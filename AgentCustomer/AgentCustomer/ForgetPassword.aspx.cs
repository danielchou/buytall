﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgentCustomer.Logic;
using AgentCustomer.Logic.BusinessLogic;
using AgentCustomer.Utility;
using AgentCustomer.Model;
using AgentCustomer.Model.Table;
using System.Net;
using System.Net.Mail;

namespace AgentCustomer
{
    public partial class ForgetPassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSendPWD_Click(object sender, EventArgs e)
        {
            var strScript = "<script> alert('請輸入正確的帳號與Email') </script>";
            var data = new LogicCustomer().GetCustDataByCustID_NO(this.txtAccount.Text.Trim());
            if (data != null)
            {
                if (this.txtEMail.Text.Trim().ToUpper() == data.EMAIL_1.ConvertToString().Trim().ToUpper())
                {
                    var strMailSubject = "Bset Leader Customer Service-密碼重設通知";
                    var strMailBody = "尊貴的客戶 ";
                    var MailTo = new List<string>
                    {
                        this.txtEMail.Text.Trim()
                    };
                    Random rnd = new Random(Guid.NewGuid().GetHashCode());
                    var strVal = rnd.Next(0, 999999).ToString().PadLeft(6, '0');

                    strMailBody += data.CUST_CNAME.ConvertToString();
                    if (data.CUST_CNAME2.ConvertToString().Trim() != string.Empty)
                    {
                        strMailBody += "/" + data.CUST_CNAME2.ConvertToString();
                    }
                    strMailBody += " 您好<br/><br/>";
                    strMailBody += "您的新密碼為：" + strVal;
                    strMailBody += "<br/>";
                    strMailBody += "請登入系統後重新設定您的新密碼<br/><br/>";
                    strMailBody += "<a href=\"https://www.bestleader-service.com/CUSTOMER_LOGIN.ASPX\">https://www.bestleader-service.com/CUSTOMER_LOGIN.ASPX<a><br/><br/><br/>";
                    strMailBody += "客戶服務部<br/>";
                    strMailBody += "Best Leader Markets PTY Ltd<br/>";

                    var obj = new LogicCustomer();

                    if (obj.ForgetPWD(data.CUST_ID == null ? 0 : data.CUST_ID.Value, strVal))
                    {
                        SendMail(MailTo, strMailSubject, strMailBody);
                        strScript = "<script> alert('新密碼已發送至您的Mail') ; ToLogin(); </script>";
                        this.Page.RegisterClientScriptBlock("Alert", strScript);
                    }
                    else
                    {
                        strScript = "<script> alert('系統錯誤，請聯絡系統管理者') ;</script>";
                        this.Page.RegisterClientScriptBlock("Alert", strScript);
                    }
                }
                else
                {
                    this.Page.RegisterClientScriptBlock("Alert", strScript);
                    return;
                }
            }
            else
            {
                this.Page.RegisterClientScriptBlock("Alert", strScript);
                return;
            }
        }


        public static void SendMail(List<string> MailTo, string MailSubject, string MailBody)
        {
            try
            {
                MailMessage msg = new MailMessage();
                msg.To.Add(string.Join(",", MailTo.ToArray()));
                //   msg.CC.Add(string.Join(",", GetConfigValueByKey("MailSystemAdmin").Split(new string[] { ";" }, StringSplitOptions.None)));

                //  Todo > Test
                //msg.To.Clear();
                //msg.CC.Clear();
                //msg.To.Add("ianHuang615@gmail.com");
                //msg.CC.Add("ianHuang615@gmail.com");

                msg.From = new MailAddress(
                    GetConfigValueByKey("MailFromAddress"),
                    "百麗客服部",
                    System.Text.Encoding.UTF8);
                msg.Subject = MailSubject;
                msg.SubjectEncoding = System.Text.Encoding.UTF8;
                msg.Body = MailBody;
                msg.IsBodyHtml = true;
                msg.BodyEncoding = System.Text.Encoding.UTF8;
                msg.Priority = MailPriority.Normal;

                //SmtpClient mySmtp = new SmtpClient("smtp.gmail.com", 587);
                //mySmtp.Credentials = new System.Net.NetworkCredential(GetConfigValueByKey("MailFromAccount"), GetConfigValueByKey("MailFromPwd"));
                //mySmtp.EnableSsl = true;

                //SmtpClient mySmtp = new SmtpClient("124.9.9.173", 25);
                SmtpClient mySmtp = new SmtpClient("168.95.4.150", 25);
                

                mySmtp.Send(msg);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetConfigValueByKey(string strKey)
        {
            return System.Web.Configuration.WebConfigurationManager.AppSettings[strKey].ConvertToString().Trim();
        }

        protected void btnRedirect_Click(object sender, EventArgs e)
        {
            this.Response.Redirect("~/Customer_Login.aspx");
        }
    }
}