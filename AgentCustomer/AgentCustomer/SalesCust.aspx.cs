﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgentCustomer.Logic;
using AgentCustomer.Logic.BusinessLogic;
using AgentCustomer.Utility;
using AgentCustomer.Model;
using AgentCustomer.Model.Table;



namespace AgentCustomer
{
    public partial class SalesCust : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Page.IsPostBack == false)
            {
                if (this.Page.Request["ID"].ConvertToString().Trim() != string.Empty)
                {
                    var strCustID = this.Page.Request["ID"].ConvertToString().Trim();

                    var data = new LogicCustomer().GetCustDataByCustID(strCustID);

                    //  2019/10/28 > Add SALES_IB_CODE > 業務幫忙進單時用的
                    data.IS_SALES_LOGIN = "Y";
                    Session["_User"] = data;
                    this.Page.Response.Redirect("~/WebPage/ContractInfo.aspx");
                }
            }
        }
    }
}