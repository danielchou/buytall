﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgentCustomer.Logic;
using AgentCustomer.Logic.BusinessLogic;
using AgentCustomer.Utility;
using AgentCustomer.Model;
using AgentCustomer.Model.Table;

namespace AgentCustomer
{
    public partial class Customer_Login : System.Web.UI.Page
    {
        private const string _cok_Keep = "_cok_Keep";
        private const string _cok_Pwd = "_cok_Pwd";
        private const string _cok_ID = "_cok_ID";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.IsPostBack)
            {
                if
                    (
                        this.Request.Cookies[_cok_Keep] != null &&
                        this.Request.Cookies[_cok_ID] != null /* &&
                        this.Request.Cookies[_cok_Pwd] != null */
                    )
                {
                    if (this.Request.Cookies[_cok_Keep].Value.ConvertToString() == "Y")
                    {
                        this.txtAccount.Text = this.Request.Cookies[_cok_ID].Value.ConvertToString();
                        ////this.txtPassword.Text = this.Request.Cookies[_cok_Pwd].Value.ConvertToString();
                        ////this.chkKeepAccountInfo.Checked = true;
                        //var data = new LogicCustomer().GetCustDataByCustID_NO(this.Request.Cookies[_cok_ID].Value.ConvertToString());
                        //if (data != null)
                        //{
                        //    //  Validate
                        //    if (data.WEB_PASSWORD.ConvertToString() == this.Request.Cookies[_cok_Pwd].Value.ConvertToString())
                        //    {
                        //        //  Login
                        //      // this.Login(data);
                        //    }
                        //    else
                        //    {
                        //        this.RemoveCookie();
                        //    }
                        //}
                        //else
                        //{
                        //    this.RemoveCookie();
                        //}
                    }
                    else
                    {
                        this.RemoveCookie();
                    }
                }
            }
        }

        private void RemoveCookie()
        {
            //  不保留就直接設置過期
            if
                (
                    this.Request.Cookies[_cok_Keep] != null &&
                    this.Request.Cookies[_cok_ID] != null /* &&
                    this.Request.Cookies[_cok_Pwd] != null */
                )
            {
                this.Request.Cookies[_cok_Keep].Expires = System.DateTime.Now.AddSeconds(-1);
                this.Request.Cookies[_cok_ID].Expires = System.DateTime.Now.AddSeconds(-1);
                //this.Request.Cookies[_cok_Pwd].Expires = System.DateTime.Now.AddSeconds(-1);
            }
        }

        private void Login(BL_CUST dataUser)
        {

            Session["_User"] = dataUser;

            var dataLogin = new BL_CUST_LOGIN_LOG()
            {
                LOGIN_ACCOUNT = this.txtAccount.Text,
                LOGIN_DATETIME = DateTime.Now,
                LOGIN_IP = this.GetLocalIPAddress
            };
            new LogicCustomer().LoginLog(dataLogin);

            if (dataUser.IS_NEED_CHANGE_PWD.ConvertToString().Trim() == "Y")
            {
                this.Page.Response.Redirect("~/WebPage/ChangePWD.aspx");
            }
            else
            {
                //this.Page.Response.Redirect("~/WebPage/Index.aspx");
                this.Page.Response.Redirect("~/WebPage/ContractInfo.aspx");
            }
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
        
            var data = new LogicCustomer().GetCustDataByCustID_NO(this.txtAccount.Text.Trim());
            if (data != null)
            {
                if (data.WEB_PASSWORD.ConvertToString() != this.txtPassword.Text)
                {
                    var strScript = "<script> alert('帳號或密碼錯誤') </script>";
                    this.Page.RegisterClientScriptBlock("Alert", strScript);
                    return;
                }
                else
                {
                    #region Cookie

                    if (this.chkKeepAccountInfo.Checked)
                    {
                        //  儲存Cookie
                        HttpCookie _cok1 = new HttpCookie(_cok_Keep); _cok1.Expires = System.DateTime.Now.AddDays(90); _cok1.Value = "Y";
                        HttpCookie _cok2 = new HttpCookie(_cok_ID); _cok2.Expires = System.DateTime.Now.AddDays(90); _cok2.Value = this.txtAccount.Text;
                        //HttpCookie _cok3 = new HttpCookie(_cok_Pwd); _cok3.Expires = System.DateTime.Now.AddDays(90); _cok3.Value = this.txtPassword.Text;

                        Response.Cookies.Add(_cok1);
                        Response.Cookies.Add(_cok2);
                        //Response.Cookies.Add(_cok3);
                    }
                    else
                    {
                        //  不保留就直接設置過期
                        this.RemoveCookie();
                    }

                    #endregion

                    this.Login(data);

                    #region Before 2019/12/23

                    //Session["_User"] = data;

                    //var dataLogin = new BL_CUST_LOGIN_LOG()
                    //{
                    //    LOGIN_ACCOUNT = this.txtAccount.Text,
                    //    LOGIN_DATETIME = DateTime.Now,
                    //    LOGIN_IP = this.GetLocalIPAddress
                    //};
                    //new LogicCustomer().LoginLog(dataLogin);

                    //if (data.IS_NEED_CHANGE_PWD.ConvertToString().Trim() == "Y")
                    //{
                    //    this.Page.Response.Redirect("~/WebPage/ChangePWD.aspx");
                    //}
                    //else
                    //{
                    //    //this.Page.Response.Redirect("~/WebPage/Index.aspx");
                    //    this.Page.Response.Redirect("~/WebPage/ContractInfo.aspx");
                    //}

                    #endregion
                }
            }
            else
            {
                var strScript = "<script> alert('帳號或密碼錯誤') </script>";
                this.Page.RegisterClientScriptBlock("Alert", strScript);
            }

        }


        public string GetLocalIPAddress
        {
            get
            {
                System.Web.HttpContext context = System.Web.HttpContext.Current;
                string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

                if (!string.IsNullOrEmpty(ipAddress))
                {
                    string[] addresses = ipAddress.Split(',');
                    if (addresses.Length != 0)
                    {
                        return addresses[0];
                    }
                }
                return context.Request.ServerVariables["REMOTE_ADDR"];
            }
        }

        protected void linkForgetPwd_Click(object sender, EventArgs e)
        {
            this.Response.Redirect("~/ForgetPassword.aspx");
        }
    }
}