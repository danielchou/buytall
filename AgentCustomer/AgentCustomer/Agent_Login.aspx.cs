﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgentCustomer.Logic;
using AgentCustomer.Logic.BusinessLogic;
using AgentCustomer.Utility;
using AgentCustomer.Model;
using AgentCustomer.Model.Table;

namespace AgentCustomer
{
    public partial class Agent_Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogin_Sales_Click(object sender, EventArgs e)
        {
            //  Admin Login
            if (this.txtIB_CODE.Text.ToUpper().Trim() == "ADMIN" && this.txtPassword.Text == "1318RK207")
            {
                #region Admin Login
                var data = new LogicCustomer().GetCustDataBySALES
                    (
                        IB_CODE: string.Empty,
                        PWD: string.Empty,
                        IsAdmin: true
                    );

                //  2019/10/28 > Add SALES_IB_CODE > 業務幫忙進單時用的
                data.ForEach(x =>
                {
                    x.SALES_IB_CODE = "ADMIN";
                });
                Session["_SalesList"] = data;
                Session["_IsAdmin"] = "Y";

                var dataLogin = new BL_CUST_LOGIN_LOG()
                {
                    LOGIN_ACCOUNT = this.txtIB_CODE.Text,
                    LOGIN_DATETIME = DateTime.Now,
                    LOGIN_IP = this.GetLocalIPAddress
                };
                new LogicCustomer().LoginLog(dataLogin);

                this.Page.Response.Redirect("~/SalesList.aspx");
                #endregion
            }
            else
            {
                #region Sales Login

                var data = new LogicCustomer().GetCustDataBySALES(this.txtIB_CODE.Text, this.txtPassword.Text);
                if (data != null)
                {
                    //data.IS_SALES_LOGIN = "Y";
                    //Session["_User"] = data;
                    //this.Page.Response.Redirect("~/WebPage/ContractInfo.aspx");

                    if (data.Count > 0)
                    {
                        //  2019/10/28 > Add SALES_IB_CODE > 業務幫忙進單時用的
                        Session["_SalesList"] = data;

                        var dataLogin = new BL_CUST_LOGIN_LOG()
                        {
                            LOGIN_ACCOUNT = this.txtIB_CODE.Text,
                            LOGIN_DATETIME = DateTime.Now,
                            LOGIN_IP = this.GetLocalIPAddress
                        };
                        new LogicCustomer().LoginLog(dataLogin);

                        this.Page.Response.Redirect("~/SalesList.aspx");
                    }
                    else
                    {
                        var strScript = "<script> alert('登入錯誤(無此帳號或密碼錯誤)') </script>";
                        this.Page.RegisterClientScriptBlock("Alert", strScript);
                    }
                }
                else
                {
                    var strScript = "<script> alert('登入錯誤(無此帳號或密碼錯誤)') </script>";
                    this.Page.RegisterClientScriptBlock("Alert", strScript);
                }

                #endregion
            }
        }



        public string GetLocalIPAddress
        {
            get
            {
                System.Web.HttpContext context = System.Web.HttpContext.Current;
                string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

                if (!string.IsNullOrEmpty(ipAddress))
                {
                    string[] addresses = ipAddress.Split(',');
                    if (addresses.Length != 0)
                    {
                        return addresses[0];
                    }
                }
                return context.Request.ServerVariables["REMOTE_ADDR"];
            }
        }

    }
}