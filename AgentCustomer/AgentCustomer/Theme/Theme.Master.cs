﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgentCustomer.Logic;
using AgentCustomer.Logic.BusinessLogic;
using AgentCustomer.Utility;
using AgentCustomer.Model;
using AgentCustomer.Model.Table;
using System.Text;
using System.Web.Script.Serialization;

namespace AgentCustomer.Theme
{
    public partial class Theme : System.Web.UI.MasterPage
    {
        private BL_CUST GetCurrentUser
        {
            get
            {
                BL_CUST _user = new BL_CUST();

                if (this.Session["_User"] != null)
                {
                    if ((this.Session["_User"] as BL_CUST) == null)
                    {
                        this.SessionOut();
                    }
                    else
                    {
                        _user = this.Session["_user"] as BL_CUST;
                    }
                }
                else
                {
                    this.SessionOut();
                }


                return _user;
            }
        }

        private void SessionOut()
        {
            //var strScript = "<script> alert('連線逾時') </script>";
            //this.Page.RegisterClientScriptBlock("Alert", strScript);
            Response.Redirect("~/Customer_Login.aspx");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (true)
            {
                if (this.Page.Request.Url.AbsoluteUri.IndexOf("Home.aspx") != -1)
                {
                    this.linkHome.CssClass = "nav-link active";
                }

                if (this.Page.Request.Url.AbsoluteUri.IndexOf("BaseInfo.aspx") != -1)
                {
                    this.linkBase.CssClass = "nav-link active";
                }

                if (this.Page.Request.Url.AbsoluteUri.IndexOf("BankInfo.aspx") != -1)
                {
                    this.linkBank.CssClass = "nav-link active";
                }

                if (this.Page.Request.Url.AbsoluteUri.IndexOf("ContractInfo.aspx") != -1)
                {
                    this.linkContract.CssClass = "nav-link active";
                }

                if (this.Page.Request.Url.AbsoluteUri.IndexOf("ContractInfo_Hist.aspx") != -1)
                {
                    this.linkContractHist.CssClass = "nav-link active";
                }
                
                if (this.Page.Request.Url.AbsoluteUri.IndexOf("ChangePWD.aspx") != -1)
                {
                    this.linkChangePwd.CssClass = "nav-link active";
                }

                if (this.Page.Request.Url.AbsoluteUri.IndexOf("Chart.aspx") != -1)
                {
                    this.linkChart.CssClass = "nav-link active";
                }

                var strLoginInfo = this.GetCurrentUser.CUST_CNAME.ConvertToString();
                if (this.GetCurrentUser.CUST_CNAME2.ConvertToString().Trim() != string.Empty)
                {
                    strLoginInfo += "/" + this.GetCurrentUser.CUST_CNAME2.ConvertToString().Trim();
                }

                lblLoginInfo.InnerHtml = "" + strLoginInfo + "&nbsp;";
            }

            if (this.Page.IsPostBack == false)
            {
                if (this.GetCurrentUser.IS_SALES_LOGIN.ConvertToString().Trim() == "Y")
                {
                    this.linkChangePwd.Visible = false;
                    this.linkLogout.Visible = false;
                }
            }
        }

        protected void linkLogout_Click(object sender, EventArgs e)
        {
            this.Session.Abandon();
            this.Page.Response.Redirect("~/Customer_Login.aspx");
        }


        protected void linkHome_Click(object sender, EventArgs e)
        {
            this.Page.Response.Redirect("~/WebPage/Home.aspx");
        }

        protected void linkBase_Click(object sender, EventArgs e)
        {
            this.Page.Response.Redirect("~/WebPage/BaseInfo.aspx");
        }

        protected void linkBank_Click(object sender, EventArgs e)
        {
            this.Page.Response.Redirect("~/WebPage/BankInfo.aspx");
        }

        protected void linkContract_Click(object sender, EventArgs e)
        {
            this.Page.Response.Redirect("~/WebPage/ContractInfo.aspx");
        }

        protected void linkContractHist_Click(object sender, EventArgs e)
        {
            this.Page.Response.Redirect("~/WebPage/ContractInfo_Hist.aspx");
        }

        protected void linkChangePwd_Click(object sender, EventArgs e)
        {
            //this.Page.Response.Redirect("~/WebPage/ChangePWD.aspx");
            this.Page.Response.Redirect("~/WebPage/ChangeCustPwd.aspx");
        }

        protected void linkChart_Click(object sender, EventArgs e)
        {
            this.Page.Response.Redirect("~/WebPage/Chart.aspx");
        }

        protected void linkDepositOut_Click(object sender, EventArgs e)
        {
            this.Page.Response.Redirect("~/WebPage/DepositOut.aspx");
        }


    }
}