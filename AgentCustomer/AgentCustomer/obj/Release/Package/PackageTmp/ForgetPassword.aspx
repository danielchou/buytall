﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ForgetPassword.aspx.cs" Inherits="AgentCustomer.ForgetPassword" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="Mobland - Mobile App Landing Page Template" />
    <meta name="keywords" content="HTML5, bootstrap, mobile, app, landing, ios, android, responsive" />

    <!-- Font -->
    <link rel="dns-prefetch" href="//fonts.googleapis.com" />
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="./Content/bootstrap.min.css" />
    <!-- Themify Icons -->
    <link rel="stylesheet" href="./Content/themify-icons.css" />
    <!-- Owl carousel -->
    <link rel="stylesheet" href="./Content/owl.carousel.min.css" />
    <!-- Main css -->
    <link href="./Content/style.css" rel="stylesheet" />
    <style>
        .form-control {
            max-width: 380px;
            display: inline-block;
        }
        [class^="ti-"],
        [class*=" ti-"] {
            padding-right: 13px;
            padding-left:13px;
        }
        *, body, html {
            font-family: 'Microsoft JhengHei';
        }

        body {
            background-image: url('./images/Login_BG.png');
            background-size: cover;
        }

        .section-title {
            /*margin-bottom: 0px;*/
        }
        .LoginLogo {
            min-width: 390px;
            max-width: 400px;
        }
    </style>
    

    <script>
        var msgAlert = "";
        var _ToLogin = ""
        
        function ToLogin() {
            _ToLogin = "Y";
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div class="section" style="max-width:480px; margin:0 auto;">
        <div class="container">
            <div class="section-title">
                <h3>忘記密碼</h3>
            </div>

            <div class="card-deck">
                <div class="card pricing popular">
                    <div class="list-group list-group-flush">
                        <div style="height:32px;">&nbsp;</div>
                        <div class="form-group">
                            <label for="<%=this.txtAccount.ClientID %>"" class="ti-user"></label>
                            <asp:TextBox ID="txtAccount" runat="server" CssClass="form-control"
                                placeholder="Please enter the account..." required></asp:TextBox>
                        </div>

                        <div class="form-group">
                            <label for="<%=this.txtEMail.ClientID %>"" class="ti-email"></label>
                            <asp:TextBox ID="txtEMail" runat="server" CssClass="form-control"
                                placeholder="Please enter the Email Address..." required></asp:TextBox>
                        </div>

                    </div>
                    <div class="card-body">
                        <asp:Button ID="btnSendPWD" runat="server" 
                            CssClass="btn btn-primary btn-lg btn-block" style="cursor:pointer;" Text="送出" OnClick="btnSendPWD_Click" />
                    </div>
                    <div style="height:18px;">&nbsp;</div>
                    <asp:Button ID="btnRedirect" runat="server" Text="" style="display:none; width:0px;" OnClick="btnRedirect_Click" />
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery and Bootstrap -->
    <script src="./scripts/jquery-3.3.1.min.js"></script>
    <script src="./scripts/bootstrap.bundle.min.js"></script>
    <!-- Plugins JS -->
    <script src="./scripts/owl.carousel.min.js"></script>
    <!-- Custom JS -->
    <script src="./scripts/script.js"></script>
        
    <script>

        $(document).ready(function () {
            //window.setInterval(sessionTimeout, 1000);

            if (msgAlert != "") {
                $("<div>" + msgAlert + "</div>").dialog({
                    height: 260,
                    width: 480,
                    modal: true,
                    resizable: true,
                    dialogClass: ""
                });
                msgAlert = "";
            }

            if (_ToLogin == "Y") {
                document.getElementById("<%=this.btnRedirect.ClientID%>").click();
            }
        });

    </script>
    </form>
</body>
</html>
