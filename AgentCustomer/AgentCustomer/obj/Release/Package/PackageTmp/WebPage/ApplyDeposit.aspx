﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Theme/Theme.Master" AutoEventWireup="true" CodeBehind="ApplyDeposit.aspx.cs" Inherits="AgentCustomer.WebPage.ApplyDeposit" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style>
        .table {
            border: unset;
        }
            .table td {
                border-bottom: unset;
                border-left: unset;
                border-right: unset;
            }

    </style>

    <script>
        var _IsSend = "";
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <div class="section light-bg" id="gallery">
        <div class="container">
            <div class="section-title" style="margin-bottom:0.5rem;" >
                <asp:LinkButton ID="linkBack" runat="server" style="float:left;" OnClick="linkBack_Click">上一頁</asp:LinkButton>
                <asp:LinkButton ID="linkApplyConfirm" runat="server" CssClass="btn btn-warning" style="float:right; margin-right:10px;" OnClick="linkApplyConfirm_Click">確定申請</asp:LinkButton>
                <asp:LinkButton ID="linkBringALL" runat="server" CssClass="btn btn-success" style="float:right; margin-right:10px; color:black;" OnClientClick="fnBringALL(); return false;" >帶入全部</asp:LinkButton>
                <h3>紅利提領申請</h3>
            </div>
            <div class="container" id="divTabs" runat="server">
                <div class="row">
                    <div class="col-md-12">
                        <nav>
                            <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">

                                <asp:LinkButton ID="linkTab_USD" runat="server" 
                                    CssClass="nav-item nav-link active" OnClick="linkTab_USD_Click">USD</asp:LinkButton>
                                <asp:LinkButton ID="linkTab_NTD" runat="server" 
                                    CssClass="nav-item nav-link" OnClick="linkTab_NTD_Click">NTD</asp:LinkButton>
                                <asp:LinkButton ID="linkTab_RMB" runat="server" 
                                    CssClass="nav-item nav-link" OnClick="linkTab_RMB_Click">RMB</asp:LinkButton>
                                <asp:LinkButton ID="linkTab_EUR" runat="server" 
                                    CssClass="nav-item nav-link" OnClick="linkTab_EUR_Click">EUR</asp:LinkButton>
                                <asp:LinkButton ID="linkTab_AUD" runat="server" 
                                    CssClass="nav-item nav-link" OnClick="linkTab_AUD_Click">AUD</asp:LinkButton>
                                

                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" role="tabpanel" >
                                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" CssClass="table rwdtable"
                                    HeaderStyle-CssClass="thead-light" OnRowDataBound="GridView1_RowDataBound" ShowFooter="true" >
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkALL" runat="server" onclick="chkAll(this);" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSelect" runat="server" onclick="chkIsAll();" />
                                            </ItemTemplate>
                                            <FooterTemplate>

                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                合約編號
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblORDER_NO" runat="server" Text='<%# Eval("ORDER_NO") %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                總計
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                幣別
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblAmountCurrency" runat="server" Text='<%# Eval("AmountCurrency") %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lbl_Amount_Currency_Total" runat="server" ></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                本金
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblAmountByCurrency" runat="server" Text='<%# Eval("AmountByCurrency","{0:#,0.0}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lbl_Amount_By_Currency_Total" runat="server" ></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                餘額
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblBALANCE" runat="server" Text='<%# Eval("BALANCE","{0:#,0.0}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lbl_BALANCE_Total" runat="server" ></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                可提領紅利
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblCanApplyAmount" runat="server" Text='<%# Eval("CanApplyAmount","{0:#,0.0}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lbl_CanApply_Amount_Total" runat="server" ></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                可提領金額(申請中)
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblApplyOnPaAmount" runat="server" Text='<%# Eval("ApplyOnPathAmount","{0:#,0.0}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lbl_ApplyOnPath_Amount_Total" runat="server" ></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                您要提領的金額
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:HyperLink ID="hypBringAmount" runat="server" CssClass="ti-share"
                                                    style="padding-right:7px; cursor:pointer; color:blue;" ToolTip="帶入金額"></asp:HyperLink>
                            
                                                <asp:TextBox ID="txtApplyAmount" runat="server" TextMode="Number" min="0" max="999999" step="1"
                                                    style="width:100%; max-width:110px;" onchange="changeAmount(this);"></asp:TextBox>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lbl_Apply_Amount_Total" runat="server" ></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                    <EmptyDataTemplate>
                                        無符合資料
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    
    <div class="modal fade" id="divSubmit2">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" style="float:left;">提領資料確認</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="float:right;">×</button>
                </div>
                <div class="modal-body">
                    <div class="section-title" style="margin-bottom:0px;">
                        <h5>您要申請出金資料如下</h5>
                    </div>
                    <div id="divApplyList" runat="server">
                        
                    </div>

                    
                    <div class="form-group">
                        <div style="background-color:#FFFA99; padding:12px;">
                            <label for="<%=this.txtPhoneValid.ClientID %>" class="ti-lock">請輸入手機收到的驗證碼</label>
                            <span style="color:red;">
                                (倒數<label style="color:red;" id="lblStart"></label>秒)
                            </span>
                            <asp:TextBox ID="txtPhoneValid" runat="server" CssClass="form-control"
                                TextMode="Password" 
                                placeholder="請輸入手機收到的驗證碼..." required></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" style="cursor:pointer;">關閉</button>
                    <asp:LinkButton runat="server" id="linkSendData" CssClass="btn btn-primary" OnClientClick="return BaforeSend();" OnClick="linkSendData_Click" >送出申請</asp:LinkButton>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    
    <asp:HiddenField ID="hidValidInput" runat="server" />

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="server">

    <script>
        var timeout;
        $(document).ready(function () {
            if (_IsSend == "Y") {
                _IsSend = "";
                ApplyConfirm();
            }
        });

        function changeAmount(obj) {
            var id = obj.id;
            var chk = id.replace("txtApplyAmount", "chkSelect");
            var idMax = id.replace("txtApplyAmount", "lblCanApplyAmount");
            var idOnPath = id.replace("txtApplyAmount", "lblApplyOnPaAmount");

            if ($.isNumeric($("#" + id).val())) {

                if (parseFloat($("#" + id).val()) > parseFloat($("#" + idMax).text().replace(",", "")) - parseFloat($("#" + idOnPath).text().replace(",", ""))) {
                    $("#" + id).val("0");
                    $("#" + chk).prop('checked', false);
                    alert("提領金額不可大於可提領紅利");
                } else {
                    if (parseFloat($("#" + id).val()) <= 0.0) {
                        $("#" + chk).prop('checked', false);
                    } else {
                        $("#" + chk).prop('checked', true);
                    }
                }
            } else {
                $("#" + chk).prop('checked', false);
            }

            chkIsAll();

            sumTotal();
        }

        function chkIsAll() {
            var isChkALL = false;
            if ($("input[name*=chkSelect]").length == $("input[name*=chkSelect]:checked").length) {
                isChkALL = true;
            } else {
                isChkALL = false;
            }

            $("input[name*=chkALL]").each(function (idx) {
                $(this).prop('checked', isChkALL);
            });
        }

        function chkAll(obj) {
            var isChkALL = false;
            if ($(obj).prop('checked') == true) {
                isChkALL = true;
            }

            $("input[name*=chkSelect]").each(function (idx) {
                $(this).prop('checked', isChkALL);
            });
        }
        

        function ApplyConfirm() {
            var start = 5 * 60;
            $("#lblStart").text(start);
            setTimeout(BtnCount, 1000); // 1s執行一次BtnCount

            //  Todo > 後端會存入Session去紀錄時間, 等電信那邊OK, 在看前端怎麼Handle, 看是要用AJAX去驗證, or 其他方式
            //  lblStart > 倒數計時


            $("#divSubmit2").modal({
                'backdrop': 'static'
            });
        }

        BtnCount = function() {
            // 啟動按鈕
            if (parseInt($("#lblStart").text()) == 0) {
                clearTimeout(timeout);

                $("#<%=this.linkSendData.ClientID%>").attr("disabled", "disabled");
                alert("請重新獲取簡訊驗證碼");
            }
            else {
                $("#lblStart").text(parseInt($("#lblStart").text()) - 1);
                setTimeout(BtnCount, 1000);
            }
        };


        function BaforeSend() {
            var result = true;
            var msg = "";

            if ($("#<%=this.txtPhoneValid.ClientID%>").val() == "") {
                msg = "請輸入簡訊驗證碼\n";
            } else {

                if ($("#<%=this.txtPhoneValid.ClientID%>").val() != "<%=this.Session["_ValidCode"].ToString() %>") {
                    msg = "請輸入正確的簡訊驗證碼\n";
                }
            }
            
            if (msg != "") {
                alert(msg);
                result = false;
            } else {
                $("#<%=this.hidValidInput.ClientID%>").val($("#<%=this.txtPhoneValid.ClientID%>").val());
            }

            return result;
        }

        function sumTotal() {
            var strCurrency = $($("span[id*=lbl_Amount_Currency_Total]")[0]).text();
            var strCurrency_sp = strCurrency.split("/");
            var iTotal = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            

            $("input[id*=txtApplyAmount]").each(function (idx) {
                var idCurrenct = this.id.replace("txtApplyAmount", "lblAmountCurrency");
                var sCurrency = $("#" + idCurrenct).text().replace(" ", "");
                if ($.isNumeric($(this).val())) {
                    var iVal = parseFloat($(this).val());
                    if (strCurrency_sp.length >= 1) {
                        if (strCurrency_sp[0] == sCurrency) {
                            iTotal[0] += iVal;
                        }
                    } if (strCurrency_sp.length >= 2) {
                        if (strCurrency_sp[1] == sCurrency) {
                            iTotal[1] += iVal;
                        }
                    } if (strCurrency_sp.length >= 3) {
                        if (strCurrency_sp[2] == sCurrency) {
                            iTotal[2] += iVal;
                        }
                    } if (strCurrency_sp.length >= 4) {
                        if (strCurrency_sp[3] == sCurrency) {
                            iTotal[3] += iVal;
                        }
                    } if (strCurrency_sp.length >= 5) {
                        if (strCurrency_sp[4] == sCurrency) {
                            iTotal[4] += iVal;
                        }
                    } if (strCurrency_sp.length >= 6) {
                        if (strCurrency_sp[5] == sCurrency) {
                            iTotal[5] += iVal;
                        }
                    } if (strCurrency_sp.length >= 7) {
                        if (strCurrency_sp[6] == sCurrency) {
                            iTotal[6] += iVal;
                        }
                    } if (strCurrency_sp.length >= 8) {
                        if (strCurrency_sp[7] == sCurrency) {
                            iTotal[7] += iVal;
                        }
                    } if (strCurrency_sp.length >= 9) {
                        if (strCurrency_sp[8] == sCurrency) {
                            iTotal[8] += iVal;
                        }
                    } if (strCurrency_sp.length >= 10) {
                        if (strCurrency_sp[9] == sCurrency) {
                            iTotal[9] += iVal;
                        }
                    }
                }
            });

            var strResult = "";
            if (strCurrency_sp.length >= 1) {
                strResult += iTotal[0];
            } if (strCurrency_sp.length >= 2) {
                strResult += " / " + iTotal[1];
            } if (strCurrency_sp.length >= 3) {
                strResult += " / " + iTotal[2];
            } if (strCurrency_sp.length >= 4) {
                strResult += " / " + iTotal[3];
            } if (strCurrency_sp.length >= 5) {
                strResult += " / " + iTotal[4];
            } if (strCurrency_sp.length >= 6) {
                strResult += " / " + iTotal[5];
            } if (strCurrency_sp.length >= 7) {
                strResult += " / " + iTotal[6];
            } if (strCurrency_sp.length >= 8) {
                strResult += " / " + iTotal[7];
            } if (strCurrency_sp.length >= 9) {
                strResult += " / " + iTotal[8];
            } if (strCurrency_sp.length >= 10) {
                strResult += " / " + iTotal[9];
            }

            $($("span[id*=lbl_Apply_Amount_Total]")[0]).text(strResult);
        }

        function fnBringALL() {

            $("input[id*=txtApplyAmount]").each(function (idx) {
                if ($("#" + this.id).attr("disabled") != "disabled") {
                    var idCanApply = this.id.replace("txtApplyAmount", "lblCanApplyAmount");
                    var idOnPath = this.id.replace("txtApplyAmount", "lblApplyOnPaAmount");
                    $(this).val($("#" + idCanApply).text().replace(" ", "").replace(",", "") - $("#" + idOnPath).text().replace(" ", "").replace(",", ""));
                    changeAmount(document.getElementById(this.id));
                }
            });
        }

    </script>

</asp:Content>

