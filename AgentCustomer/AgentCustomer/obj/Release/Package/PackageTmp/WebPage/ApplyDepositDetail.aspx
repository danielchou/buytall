﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Theme/Theme.Master" AutoEventWireup="true" CodeBehind="ApplyDepositDetail.aspx.cs" Inherits="AgentCustomer.WebPage.ApplyDepositDetail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .table {
            border: unset;
        }
            .table td {
                border-bottom: unset;
                border-left: unset;
                border-right: unset;
            }

    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="section light-bg" id="gallery">
        <div class="container">
            <div class="section-title" style="margin-bottom:0.5rem;" >
                <asp:LinkButton ID="linkBack" runat="server" style="float:left;" OnClick="linkBack_Click">上一頁</asp:LinkButton>
                <h3>紅利提領申請進度</h3>
            </div>

            
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" CssClass="table rwdtable"
                HeaderStyle-CssClass="thead-light" OnRowDataBound="GridView1_RowDataBound" ShowFooter="false" >
                <Columns>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            批號
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblBATCH_WITHDRAWAL_ID" runat="server" Text='<%# Eval("BATCH_WITHDRAWAL_ID") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            申請日期
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblAPPLY_DATE" runat="server" Text='<%# Eval("APPLY_DATE","{0:yyyy/MM/dd}") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            
                        </FooterTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField>
                        <HeaderTemplate>
                            合約編號
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblORDER_NO" runat="server" Text='<%# Eval("ORDER_NO") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            總計
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            幣別
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblCurrency" runat="server" Text='<%# Eval("Currency") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lbl_Currency_Total" runat="server" ></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            提領金額
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblACTUAL_Amount" runat="server" Text='<%# Eval("ACTUAL_Amount","{0:#,0.0}") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lbl_ACTUAL_Amount_Total" runat="server" ></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            進度狀態
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblSCHEDULE_DESC" runat="server" Text='<%# Eval("SCHEDULE_DESC") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lbl_SCHEDULE_DESC_Total" runat="server" ></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateField>

                </Columns>
                <EmptyDataTemplate>
                    無符合資料
                </EmptyDataTemplate>
            </asp:GridView>

        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="server">
</asp:Content>
