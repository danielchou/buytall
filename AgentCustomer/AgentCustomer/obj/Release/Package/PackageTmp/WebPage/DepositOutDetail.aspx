﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Theme/Theme.Master" AutoEventWireup="true" CodeBehind="DepositOutDetail.aspx.cs" Inherits="AgentCustomer.WebPage.DepositOutDetail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .card-text span {
            color: black;
            font-size: 18px;
        }
     
        button.Card-Main {
            padding: 3px 3px 3px 3px !important;
            margin: 8px !important;
        }
            button.Card-Main h3 {
                font-size: 18px !important;
            }

              
        .th-Header {
            text-align: center;
        }
        .td-Text {
            text-align: left;
        }
        .td-Numeric {
            text-align: right;
        }
        
        .td-Center {
            text-align: center;
        }

        .table th {
            font-size: 15px;
        }
        .table td {
            font-size: 14px;
        }
        .table tfoot td {
            font-weight: bold;
        }
    </style>
      
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="section light-bg" id="features">
        <div class="container">
            <div class="section-title" style="margin-bottom:0.75rem; padding-top:1.25rem;">
                <asp:LinkButton ID="linkBack" runat="server" style="float:left;" OnClick="linkBack_Click">上一頁</asp:LinkButton>
                <h3>提領紀錄明細</h3>
            </div>
            <div class="row">
                <div class="col-2"></div>
                <div class="col-8">
                    <div id="divDetail" runat="server">

                    </div>
                </div>
                <div class="col-2"></div>
            </div>
        </div>

    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="server">


</asp:Content>
