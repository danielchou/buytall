﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Theme/Theme.Master" AutoEventWireup="true" CodeBehind="ContractOrderDetail.aspx.cs" Inherits="AgentCustomer.WebPage.ContractOrderDetail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .card-text span {
            color: black;
            font-size: 20px;
        }

        .th-Header {
            text-align: center;
        }
        .td-Text {
            text-align: left;
        }
        .td-Numeric {
            text-align: right;
        }
        .td-center {
            text-align: center;
        }

        h5.bos {
            background-color: #EFEFEF;
        }
        span.box {
            min-width: 250px;
            display: inline-block;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <div class="section light-bg" id="gallery">
        <div class="container">
            <div class="section-title">
                <asp:LinkButton ID="linkBack" runat="server" style="float:left;" OnClick="linkBack_Click">上一頁</asp:LinkButton>
                <h3>您的合約明細資料</h3>
            </div>
            
            <div id="divContractData" runat="server">

            </div>

        </div>
    </div>

    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="server">
</asp:Content>
