﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Theme/Theme.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="AgentCustomer.WebPage.Home" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .card-text span {
            color: black;
            font-size: 20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <div class="container">
            <div class="section-title">
                <h3>VIP Member Use Only</h3>
            </div>
            <div class="row">
                <div class="col-12 col-lg-4">
                    <div class="card features">
                        <div class="card-body">
                            <div class="media">
                                <span class="ti-face-smile gradient-fill ti-3x mr-3"></span>
                                <div class="media-body">
                                    <h4 class="card-title">收益穩健</h4>
                                    <p class="card-text"> 百麗穩健型商品 CLIENTS AGREEMENT , 穩健固定的收益,安全合規, 是您值的信賴的理財夥伴</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-4">
                    <div class="card features">
                        <div class="card-body">
                            <div class="media">
                                <span class="ti-settings gradient-fill ti-3x mr-3"></span>
                                <div class="media-body">
                                    <h4 class="card-title">個人專屬</h4>
                                    <p class="card-text">信託客戶有皆享有個人化的專業投資建議, 並提供完整平台查詢最您的帳戶交易記錄及收益明細 </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-4">
                    <div class="card features">
                        <div class="card-body">
                            <div class="media">
                                <span class="ti-lock gradient-fill ti-3x mr-3"></span>
                                <div class="media-body">
                                    <h4 class="card-title">安全可靠</h4>
                                    <p class="card-text">百麗商品是合法合規登記在案金融商品, 註冊地在澳洲,有專業顧問及監管人,是您最可靠的夥伴</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="server">
</asp:Content>
