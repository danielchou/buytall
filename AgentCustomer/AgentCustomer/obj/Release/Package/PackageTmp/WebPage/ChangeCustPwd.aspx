﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Theme/Theme.Master" AutoEventWireup="true" CodeBehind="ChangeCustPwd.aspx.cs" Inherits="AgentCustomer.WebPage.ChangeCustPwd" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .form-control {
            max-width: 380px;
            display: inline-block;
        }
        [class^="ti-"],
        [class*=" ti-"] {
            padding-right: 13px;
            padding-left:13px;
        }
        

        *, body, html {
            font-family: 'Microsoft JhengHei';
        }
    </style>
    
    <script>
        var msgAlert = "";
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <div class="section" style="max-width:480px; margin:0 auto;">
        <div class="container">
            <div class="section-title">
                <h3>Change Password</h3>
            </div>

            <div class="card-deck">
                <div class="card pricing popular">
                    <div class="list-group list-group-flush">
                        <div style="height:32px;">&nbsp;</div>

                        <div class="form-group">
                            <label for="<%=this.txtPassword.ClientID %>"" class="ti-lock"></label>
                            <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control"
                                TextMode="Password" 
                                placeholder="Please enter the current password..." required></asp:TextBox>
                        </div>
                        
                        <div class="form-group">
                            <label for="<%=this.txtPasswordNew1.ClientID %>"" class="ti-lock"></label>
                            <asp:TextBox ID="txtPasswordNew1" runat="server" CssClass="form-control"
                                TextMode="Password" 
                                placeholder="Please enter the new password..." required></asp:TextBox>
                        </div>
                        
                        <div class="form-group">
                            <label for="<%=this.txtPasswordNew2.ClientID %>"" class="ti-lock"></label>
                            <asp:TextBox ID="txtPasswordNew2" runat="server" CssClass="form-control"
                                TextMode="Password" 
                                placeholder="Please enter the new password again..." required></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label style="color:red; margin-left:3rem;">* 密碼需為英文+數字，至少6碼</label>
                        </div>
                    </div>
                    <div class="card-body">
                        <asp:Button ID="btnChangePWD" runat="server" 
                            CssClass="btn btn-primary btn-lg btn-block" style="cursor:pointer;" 
                            Text="Change Password" OnClick="btnChangePWD_Click" />
                    </div>
                    <div style="height:18px;">&nbsp;</div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="server">
    <script>

        $(document).ready(function () {
            //window.setInterval(sessionTimeout, 1000);

            if (msgAlert != "") {
                $("<div>" + msgAlert + "</div>").dialog({
                    height: 260,
                    width: 480,
                    modal: true,
                    resizable: true,
                    dialogClass: ""
                });
                msgAlert = "";
            }
        });

    </script>
</asp:Content>
