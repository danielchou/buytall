﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Theme/Theme.Master" AutoEventWireup="true" CodeBehind="BankInfo.aspx.cs" Inherits="AgentCustomer.WebPage.BankInfo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .card-text span {
            color: black;
            font-size: 20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <div class="section light-bg" id="features">
        <div class="container">
            
            <div class="section-title" style="margin-bottom:0.75rem;">
                <h3>銀行資料</h3>
            </div>
            <asp:Repeater ID="rpData" runat="server">
                <ItemTemplate>
                   <%-- <div class="row" style="padding-top:12px;">
                        <h4 class="card-title" style="padding-right:12px;">No.</h4>
                        <p class="card-text"><asp:Label ID="Label1" runat="server" Text='<%# Container.ItemIndex + 1 %>'></asp:Label></p>
                    </div>--%>
                    
                    <div class="row">
                        <h4 class="card-title" style="padding-right:12px;">帳戶持有人姓名</h4>
                        <p class="card-text"><asp:Label ID="txtACCOUNT_CNAME" runat="server" Text='<%# Eval("ACCOUNT_CNAME") %>'></asp:Label></p>
                    </div>
                    <div class="row">
                        <h4 class="card-title" style="padding-right:12px;">幣別</h4>
                        <p class="card-text"><asp:Label ID="txtCurrency" runat="server" Text='<%# Eval("CURRENCY") %>'></asp:Label></p>
                    </div>
                    <div class="row">
                        <h4 class="card-title" style="padding-right:12px;">銀行名稱</h4>
                        <p class="card-text"><asp:Label ID="txtBANK_CNAME" runat="server" Text='<%# Eval("BANK_CNAME") %>'></asp:Label>
                            <asp:Label ID="txtBANK_ENAME" runat="server" Text='<%# Eval("BANK_ENAME") %>'></asp:Label>
                        </p>
                    </div>
                    <div class="row">
                        <h4 class="card-title" style="padding-right:12px;">分行名稱</h4>
                        <p class="card-text"><asp:Label ID="txtBRANCH_CNAME" runat="server" Text='<%# Eval("BRANCH_CNAME") %>'></asp:Label></p>
                    </div>
                    <div class="row">
                        <h4 class="card-title" style="padding-right:12px;">銀行帳戶</h4>
                        <p class="card-text"><asp:Label ID="txtACCOUNT" runat="server" Text='<%# Eval("ACCOUNT") %>'></asp:Label></p>
                    </div>
                    <div class="row">
                        <h4 class="card-title" style="padding-right:12px;">SWIFT_CODE</h4>
                        <p class="card-text"><asp:Label ID="txtSWIFT_CODE" runat="server" Text='<%# Eval("SWIFT_CODE") %>'></asp:Label></p>
                    </div>
                    <div class="row">
                        <h4 class="card-title" style="padding-right:12px;">銀行地址</h4>
                        <p class="card-text"><asp:Label ID="txtBANK_C_ADDRESS" runat="server" Text='<%# Eval("BANK_C_ADDRESS") %>'></asp:Label></p>
                    </div>
                    <div style="height:35px;">&nbsp;</div>
                </ItemTemplate>
            </asp:Repeater>



        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="server">
</asp:Content>
