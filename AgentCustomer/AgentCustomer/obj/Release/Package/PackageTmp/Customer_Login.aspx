﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Customer_Login.aspx.cs" Inherits="AgentCustomer.Customer_Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="Mobland - Mobile App Landing Page Template" />
    <meta name="keywords" content="HTML5, bootstrap, mobile, app, landing, ios, android, responsive" />

    <!-- Font -->
    <link rel="dns-prefetch" href="//fonts.googleapis.com" />
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="./Content/bootstrap.min.css" />
    <!-- Themify Icons -->
    <link rel="stylesheet" href="./Content/themify-icons.css" />
    <!-- Owl carousel -->
    <link rel="stylesheet" href="./Content/owl.carousel.min.css" />
    <!-- Main css -->
    <link href="./Content/style.css" rel="stylesheet" />

    <style>
        .form-control {
            max-width: 380px;
            display: inline-block;
        }
        [class^="ti-"],
        [class*=" ti-"] {
            padding-right: 13px;
            padding-left:13px;
        }
        *, body, html {
            font-family: 'Microsoft JhengHei';
        }

        body {
            background-image: url('./images/Login_BG.png');
            background-size: cover;
        }

        .section-title {
            /*margin-bottom: 0px;*/
        }
        .LoginLogo {
            min-width: 390px;
            max-width: 400px;
        }
    </style>
    

    <script>
        var msgAlert = "";
    </script>

</head>
<body>
    <form id="form1" runat="server">
        

    <div class="section" style="max-width:480px; margin:0 auto;">
        <div class="container">
            <div class="section-title">
                <h3><%--Login--%>
                    <img src="images/Login_Logo.png" class="LoginLogo" />
                </h3>
            </div>

            <div class="card-deck">
                <div class="card pricing popular">
                    <%--<div class="card-head">
                        <span class="price">Login</span>
                    </div>--%>
                    <div class="list-group list-group-flush">
                        <div style="height:32px;">&nbsp;<a style="color: #FF0000; font-size: 15px; font-weight: bold;">線上出金申請測試版 20190827</a></div>
                        <div class="form-group">
                            <label for="<%=this.txtAccount.ClientID %>"" class="ti-user"></label>
                            <asp:TextBox ID="txtAccount" runat="server" CssClass="form-control"
                                placeholder="Please enter the account..." required></asp:TextBox>
                        </div>

                        <div class="form-group">
                            <label for="<%=this.txtPassword.ClientID %>"" class="ti-lock"></label>
                            <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control"
                                TextMode="Password" 
                                placeholder="Please enter the password..." required></asp:TextBox>
                        </div>

                    </div>
                    <div class="card-body">
                        <asp:Button ID="btnLogin" runat="server" 
                            CssClass="btn btn-primary btn-lg btn-block" style="cursor:pointer;" Text="Login" OnClick="btnLogin_Click" />
                        <div style="text-align:right; padding-top:7px;">
                            <asp:LinkButton ID="linkForgetPwd" runat="server" OnClick="linkForgetPwd_Click">忘記密碼</asp:LinkButton>
                        </div>
                    </div>
                    <div style="height:18px;">&nbsp;</div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery and Bootstrap -->
    <script src="./scripts/jquery-3.3.1.min.js"></script>
    <script src="./scripts/bootstrap.bundle.min.js"></script>
    <!-- Plugins JS -->
    <script src="./scripts/owl.carousel.min.js"></script>
    <!-- Custom JS -->
    <script src="./scripts/script.js"></script>
        
    <script>

        $(document).ready(function () {
            //window.setInterval(sessionTimeout, 1000);

            if (msgAlert != "") {
                $("<div>" + msgAlert + "</div>").dialog({
                    height: 260,
                    width: 480,
                    modal: true,
                    resizable: true,
                    dialogClass: ""
                });
                msgAlert = "";
            }
        });

    </script>
    </form>
</body>
</html>
