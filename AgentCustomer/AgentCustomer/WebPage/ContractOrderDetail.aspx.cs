﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgentCustomer.Logic;
using AgentCustomer.Logic.BusinessLogic;
using AgentCustomer.Utility;
using AgentCustomer.Model;
using AgentCustomer.Model.Table;
using System.Text;
using System.Web.Script.Serialization;

namespace AgentCustomer.WebPage
{
    public partial class ContractOrderDetail : System.Web.UI.Page
    {
        private BL_CUST GetCurrentUser
        {
            get
            {
                BL_CUST _user = new BL_CUST();

                if (this.Session["_User"] != null)
                {
                    if ((this.Session["_User"] as BL_CUST) == null)
                    {
                        this.SessionOut();
                    }
                    else
                    {
                        _user = this.Session["_user"] as BL_CUST;
                    }
                }
                else
                {
                    this.SessionOut();
                }


                return _user;
            }
        }

        private void SessionOut()
        {
            //var strScript = "<script> alert('連線逾時') </script>";
            //this.Page.RegisterClientScriptBlock("Alert", strScript);
            Response.Redirect("~/Customer_Login.aspx");
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Page.IsPostBack == false)
            {
                this.BindCustBaseData();

            }

        }

        private void BindCustBaseData()
        {
            var dataUser = this.GetCurrentUser;
            if (dataUser != null)
            {
                this.BindContractData(dataUser.CUST_ID == null ? "" : dataUser.CUST_ID.Value.ConvertToString());
            }
        }



        private void BindContractData(string CustID)
        {
            var data = new LogicCustomer().GetContractDataByCustID(CustID);
            var dataOrderList = new LogicCustomer().GetOrderDataByORDER_NO
                (
                    data
                        .Where(x => x.ORDER_NO.ConvertToString() == this.Page.Request["No"].ConvertToString().Trim())
                        .Select(x => x.ORDER_NO.ConvertToString())
                        .ToList()
                );
            var strHtml = new StringBuilder();
            if (dataOrderList.Count > 0)
            {

                strHtml.Append("<h5 class='box'>" +
                    "<span class='box'>合約編號：" + dataOrderList[0].ORDER_NO.ConvertToString().Trim() + "</span>" +
                    "<span class='box'>幣別：" + dataOrderList[0].GetCURRENCY.ConvertToString().Trim() + "</span>" +
                    "<span class='box'>本金：" + dataOrderList[0].GetBASE_AMOUNT.ConvertToString().Trim() + "</span>" +
                    "</h5>");
                strHtml.Append("<h5 class='box'>" +
                    "<span class='box'>合約起始日：" + dataOrderList[0].GetFROM_DATE.ConvertToString().Trim() + "</span>" +
                    "<span class='box'>合約結束日：" + dataOrderList[0].GetEND_DATE.ConvertToString().Trim()+ "</span>" +
                    "</h5>");
                strHtml.Append("<h5 class='box'>" +
                    "<span class='box'>合約延長日：" + dataOrderList[0].GetEXTEND_DATE.ConvertToString().Trim() + "</span>" +
                    "<span class='box'>合約終止日：" + dataOrderList[0].GetORDER_END.ConvertToString().Trim() + "</span>" +
                    "</h5>");

                strHtml.AppendLine("<table class='table rwdtable'> ");
                strHtml.AppendLine(" <thead class='thead-light'> ");
                strHtml.AppendLine("  <tr><td data-label='總計' class='td-Numeric' colspan='8'><span style='color:GRAY;'>* 系統呈現資產餘額與對帳單餘額若有小數位尾差, 請以對帳單為準</span></td> </tr>");
                strHtml.AppendLine("  <tr>");
                //strHtml.AppendLine("   <th class='th-Header'>出/入</th> ");
                strHtml.AppendLine("   <th class='th-Header'>交易類別</th> ");
                strHtml.AppendLine("   <th class='th-Header'>派息次數</th> ");
                strHtml.AppendLine("   <th class='th-Header'>交易日</th> ");
                strHtml.AppendLine("   <th class='th-Header'>金額</th> ");
                strHtml.AppendLine("  </tr>");
                strHtml.AppendLine(" </thead>");
                strHtml.AppendLine(" <tbody>");

                dataOrderList.ForEach(x =>
                {
                    strHtml.AppendLine(" <tr>");
                    //strHtml.AppendLine("  <td data-label='出/入'>" + x.FLOW_TYPE.ConvertToString().Trim() + "</td>");
                    strHtml.AppendLine("  <td data-label='交易類別' class='td-center'>" + x.TXN_TYPE.ConvertToString().Trim() + "</td>");
                    strHtml.AppendLine("  <td data-label='派息次數' class='td-center'>" + (x.INTEREST_TIMES == null ? "&nbsp;" : x.INTEREST_TIMES.ConvertToString().Trim()) + "</td>");
                    strHtml.AppendLine("  <td data-label='交易日' class='td-center'>" + x.GetTXN_DATE.ConvertToString().Trim() + "</td>");
                    strHtml.AppendLine("  <td data-label='金額' class='td-Numeric'>" + (x.AMOUNT == null ? "" : x.AMOUNT.Value.ToString("#,0.0")) + "</td>");

                    strHtml.AppendLine(" </tr>");
                });

                strHtml.AppendLine(" </tbody>");
                strHtml.AppendLine("</table>");
                //strHtml.AppendLine("<span style='color:red;'>* 金額僅顯示至個位數</span>");
            }

            this.divContractData.InnerHtml = strHtml.ToString();
        }

        protected void linkBack_Click(object sender, EventArgs e)
        {
            this.Page.Response.Redirect("~/WebPage/ContractInfo.aspx");
        }

    }
}