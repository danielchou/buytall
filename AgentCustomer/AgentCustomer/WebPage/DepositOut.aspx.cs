﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgentCustomer.Logic;
using AgentCustomer.Logic.BusinessLogic;
using AgentCustomer.Utility;
using AgentCustomer.Model;
using AgentCustomer.Model.Table;
using System.Text;
using System.Web.Script.Serialization;


namespace AgentCustomer.WebPage
{
    public partial class DepositOut : System.Web.UI.Page
    {
        private BL_CUST GetCurrentUser
        {
            get
            {
                BL_CUST _user = new BL_CUST();

                if (this.Session["_User"] != null)
                {
                    if ((this.Session["_User"] as BL_CUST) == null)
                    {
                        this.SessionOut();
                    }
                    else
                    {
                        _user = this.Session["_user"] as BL_CUST;
                    }
                }
                else
                {
                    this.SessionOut();
                }


                return _user;
            }
        }

        private void SessionOut()
        {
            //var strScript = "<script> alert('連線逾時') </script>";
            //this.Page.RegisterClientScriptBlock("Alert", strScript);
            Response.Redirect("~/Customer_Login.aspx");
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Page.IsPostBack == false)
            {

                this.BindDepositOutData();
            }
        }

        protected void BindDepositOutData()
        {
            var dataUser = this.GetCurrentUser;
            if (dataUser != null)
            {
                var strCustID = dataUser.CUST_ID == null ? "" : dataUser.CUST_ID.Value.ConvertToString();
                var strHtml = new StringBuilder();
                var data = new LogicCustomer().GetDepositDataByCustID2(strCustID);
                var dataTemp = new LogicCustomer().GetApplyTempDataALLByCustID(strCustID);

                if (data.Count() > 0)
                {
                    #region Ver 1

                    //strHtml.AppendLine("<table class='table rwdtable' id='tb1'> ");
                    //strHtml.AppendLine(" <thead class='thead-light'> ");
                    //strHtml.AppendLine("  <tr>");
                    //strHtml.AppendLine("   <th class='th-Header'>出金申請日</th> ");
                    //strHtml.AppendLine("   <th class='th-Header'>出金金額</th> ");
                    //strHtml.AppendLine("   <th class='th-Header'>幣別</th> ");
                    //strHtml.AppendLine("   <th class='th-Header'>申請類型</th> ");
                    //strHtml.AppendLine("   <th class='th-Header'>備註</th> ");
                    //strHtml.AppendLine("  </tr>");
                    //strHtml.AppendLine(" </thead>");
                    //strHtml.AppendLine(" <tbody>");

                    ////  Loop
                    //data
                    //    .OrderByDescending(x => x.TXN_DATE)
                    //    .OrderBy(x => x.ORDER_NO)
                    //    .ToList()
                    //    .ForEach(x =>
                    //    {
                    //        strHtml.AppendLine("  <tr>");
                    //        strHtml.AppendLine("   <td data-label='出金申請日' class='td-Text'>" + x.GetTXN_DATE.ConvertToString() + "</td> ");
                    //        if (x.AMOUNT_USD != null)
                    //        {
                    //            strHtml.AppendLine("   <td data-label='出金金額' class='td-Numeric'>" + x.AMOUNT_USD.Value.ToString("#,0.0") + " </td> ");
                    //            strHtml.AppendLine("   <td data-label='幣別' class='td-Text'>" + "USD" + "</td> ");
                    //        }
                    //        else if (x.AMOUNT_NTD != null)
                    //        {
                    //            strHtml.AppendLine("   <td data-label='出金金額' class='td-Numeric'>" + x.AMOUNT_NTD.Value.ToString("#,0.0") + " </td> ");
                    //            strHtml.AppendLine("   <td data-label='幣別' class='td-Text'>" + "NTD" + "</td> ");
                    //        }
                    //        else if (x.AMOUNT_RMB != null)
                    //        {
                    //            strHtml.AppendLine("   <td data-label='出金金額' class='td-Numeric'>" + x.AMOUNT_RMB.Value.ToString("#,0.0") + " </td> ");
                    //            strHtml.AppendLine("   <td data-label='幣別' class='td-Text'>" + "RMB" + "</td> ");
                    //        }
                    //        else
                    //        {
                    //            strHtml.AppendLine("   <td data-label='出金金額' class='td-Numeric'>" + "" + " </td> ");
                    //            strHtml.AppendLine("   <td data-label='幣別' class='td-Text'>" + "" + "</td> ");
                    //        }
                    //        strHtml.AppendLine("   <td data-label='申請類型' class='td-Text'>" + "業務代填" + "</td> ");
                    //        strHtml.AppendLine("   <td data-label='備註' class='td-Text'>" + x.REMARK.ConvertToString() + "</td> ");

                    //        strHtml.AppendLine("  </tr>");
                    //    });
                    //strHtml.AppendLine(" </tbody>");
                    //strHtml.AppendLine("</table>");

                    #endregion

                    #region Ver 2

                    strHtml.AppendLine("<table class='table rwdtable' id='tb1'> ");
                    strHtml.AppendLine(" <thead class='thead-light'> ");
                    strHtml.AppendLine("  <tr>");
                    strHtml.AppendLine("   <th class='th-Header'>提領單號</th> ");
                    strHtml.AppendLine("   <th class='th-Header'>幣別</th> ");
                    strHtml.AppendLine("   <th class='th-Header'>提領申請日</th> ");
                    strHtml.AppendLine("   <th class='th-Header'>提領總額</th> ");
                    strHtml.AppendLine("   <th class='th-Header'>實際入帳金額</th> ");
                    strHtml.AppendLine("   <th class='th-Header'>提領性質</th> ");
                    strHtml.AppendLine("   <th class='th-Header'>備註</th> ");
                    strHtml.AppendLine("  </tr>");
                    strHtml.AppendLine(" </thead>");
                    strHtml.AppendLine(" <tbody>");

                    dataTemp.Select(x => new
                    {
                        x.TXN_DATE,
                        x.Currency,
                        x.BATCH_WITHDRAWAL_ID,
                        x.WITHDRAWAL_TYPE
                    }).Distinct()
                    .OrderByDescending(x => x.TXN_DATE)
                    .ToList()
                    .ForEach(x =>
                    {
                        var decAmount = dataTemp
                            .Where
                            (
                                y =>
                                    x.TXN_DATE == y.TXN_DATE &&
                                    x.Currency == y.Currency &&
                                    x.BATCH_WITHDRAWAL_ID == y.BATCH_WITHDRAWAL_ID &&
                                    x.WITHDRAWAL_TYPE == y.WITHDRAWAL_TYPE
                            )
                            .Sum(y => y.Amount == null ? 0 : y.Amount.Value);
                        var decACTUAL_Amount = dataTemp
                            .Where
                            (
                                y =>
                                    x.TXN_DATE == y.TXN_DATE &&
                                    x.Currency == y.Currency &&
                                    x.BATCH_WITHDRAWAL_ID == y.BATCH_WITHDRAWAL_ID &&
                                    x.WITHDRAWAL_TYPE == y.WITHDRAWAL_TYPE
                            )
                            .Sum(y => y.ACTUAL_Amount == null ? 0 : y.ACTUAL_Amount.Value);


                        strHtml.AppendLine("  <tr>");

                        var strBatchID = x.BATCH_WITHDRAWAL_ID == null ? "" : x.BATCH_WITHDRAWAL_ID.Value.ConvertToString();
                        strHtml.AppendLine("   <td data-label='提領單號' class='td-Text'><a href=\"\" onclick=\"OpenDatail('T" + strBatchID + "'); return false;\">" + strBatchID + "<br/>(申請中)</a></td> ");
                        strHtml.AppendLine("   <td data-label='幣別' class='td-Text'>" + x.Currency.ConvertToString() + "</td> ");
                        strHtml.AppendLine("   <td data-label='提領申請日' class='td-Text'>" + (x.TXN_DATE == null ? string.Empty : x.TXN_DATE.Value.ToString("yyyy/MM/dd")) + "</td> ");
                        strHtml.AppendLine("   <td data-label='提領總額' class='td-Numeric'>" + decAmount.ToString("#,0.0") + "</td> ");
                        strHtml.AppendLine("   <td data-label='實際入帳金額' class='td-Numeric'>" + decACTUAL_Amount.ToString("#,0.0") + "</td> ");
                        strHtml.AppendLine("   <td data-label='提領性質' class='td-Numeric'>" + (decAmount <= 0 ? "內轉合約" : "提領紅利") + "</td> ");
                        switch (x.WITHDRAWAL_TYPE.ConvertToString().ToUpper().Trim())
                        {
                            case "WEB":
                                var strURL = this.Page.Request.Url.AbsoluteUri.ConvertToString().Replace("DepositOut.aspx", "ExportPDF.aspx") +
                                    "?sType=PDF&sID=" + strBatchID;
                                var strHref = "<a target='_blank' href='" + strURL + "'>網路出金附件檔</a>";
                                strHtml.AppendLine("   <td data-label='備註' class='td-Text'>" + strHref + "</td> ");
                                break;
                            default:
                                strHtml.AppendLine("   <td data-label='備註' class='td-Text'>" + string.Empty + "</td> ");
                                break;
                        }


                        strHtml.AppendLine("  </tr>");

                    });

                    data
                        .OrderByDescending(x => x.TXN_DATE)
                        .ThenBy(x => x.ORDER_NO)
                        .ToList()
                        .ForEach(x =>
                        {
                            strHtml.AppendLine("  <tr>");

                            var strBatchID = x.BATCH_WITHDRAWAL_ID == null ? "" : x.BATCH_WITHDRAWAL_ID.Value.ConvertToString();
                            strHtml.AppendLine("   <td data-label='提領單號' class='td-Text'><a href=\"\" onclick=\"OpenDatail('" + strBatchID + "'); return false;\">" + strBatchID + "</a></td> ");
                            strHtml.AppendLine("   <td data-label='幣別' class='td-Text'>" + x.CURRENCY.ConvertToString() + "</td> ");
                            strHtml.AppendLine("   <td data-label='提領申請日' class='td-Text'>" + x.GetTXN_DATE.ConvertToString() + "</td> ");
                            strHtml.AppendLine("   <td data-label='提領總額' class='td-Numeric'>" + (x.AMOUNT == null ? "" : x.AMOUNT.Value.ToString("#,0.0")) + "</td> ");
                            strHtml.AppendLine("   <td data-label='實際入帳金額' class='td-Numeric'>" + (x.ACTUAL_AMOUNT == null ? "" : x.ACTUAL_AMOUNT.Value.ToString("#,0.0")) + "</td> ");
                            strHtml.AppendLine("   <td data-label='提領性質' class='td-Numeric'>" + ((x.AMOUNT == null ? 0 : x.AMOUNT.Value) <= 0 ? "內轉合約" : "提領紅利") + "</td> ");
                            strHtml.AppendLine("   <td data-label='備註' class='td-Text'>" + x.REMARK.ConvertToString() + "</td> ");

                            strHtml.AppendLine("  </tr>");
                        });



                    strHtml.AppendLine(" </tbody>");
                    strHtml.AppendLine("</table>");

                    #endregion
                }
                else
                {
                    strHtml.AppendLine("<h3 style='font-size:23px;'>查無提領紀錄</h3>");
                }

                this.divDepositData.InnerHtml = strHtml.ToString();
            }
        }

        protected void btnShow_Click(object sender, EventArgs e)
        {

            var strBatchID = this.hidBatchID.Value;
            this.Page.Response.Redirect("~/WebPage/DepositOutDetail.aspx?No=" + strBatchID);

        }
    }
}