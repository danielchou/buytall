﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgentCustomer.Logic;
using AgentCustomer.Logic.BusinessLogic;
using AgentCustomer.Utility;
using AgentCustomer.Model;
using AgentCustomer.Model.Table;
using System.Text;
using System.Web.Script.Serialization;

namespace AgentCustomer.WebPage
{
    public partial class BaseInfo : System.Web.UI.Page
    {
        private BL_CUST GetCurrentUser
        {
            get
            {
                BL_CUST _user = new BL_CUST();

                if (this.Session["_User"] != null)
                {
                    if ((this.Session["_User"] as BL_CUST) == null)
                    {
                        this.SessionOut();
                    }
                    else
                    {
                        _user = this.Session["_user"] as BL_CUST;
                    }
                }
                else
                {
                    this.SessionOut();
                }


                return _user;
            }
        }

        private void SessionOut()
        {
            //var strScript = "<script> alert('連線逾時') </script>";
            //this.Page.RegisterClientScriptBlock("Alert", strScript);
            Response.Redirect("~/Customer_Login.aspx");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Page.IsPostBack == false)
            {
                this.BindCustBaseData();

            }

        }


        private void BindCustBaseData()
        {
            var dataUser = this.GetCurrentUser;
            if (dataUser != null)
            {
                var dataBank = new LogicCustomer().GetBankDataByCustID(dataUser.CUST_ID == null ? "" : dataUser.CUST_ID.Value.ConvertToString());

                this.lblContractCount.Text = dataUser.ORDER_CNT == null ? "0" : dataUser.ORDER_CNT.Value.ConvertToString();
                this.lblTotalBalance.Text = dataUser.ORDER_BALANCE == null ? "$0" : dataUser.ORDER_BALANCE.Value.ToString("$#,#");

                #region 基本資料

                this.txtCUST_CNAME.Text = dataUser.CUST_CNAME.ConvertToString();
                this.divCUST_CNAME.Visible = !(dataUser.CUST_CNAME.ConvertToString().Trim() == string.Empty);
                this.txtCUST_ENAME.Text = dataUser.CUST_ENAME.ConvertToString();
                this.divCUST_ENAME.Visible = !(dataUser.CUST_ENAME.ConvertToString().Trim() == string.Empty);
                if (dataUser.SEX.ConvertToString() == "M")
                {
                    this.txtSex.Text = "男士";
                }
                else if (dataUser.SEX.ConvertToString() == "F")
                {
                    this.txtSex.Text = "女士";
                }
                else
                {
                    this.divSex.Visible = false;
                }

                if (dataUser.DATE_OF_BIRTH != null)
                {
                    this.txtBirthday.Text = dataUser.DATE_OF_BIRTH.Value.ToString("yyyy/MM/dd");
                }
                else
                {
                    this.divBirthday.Visible = false;
                }

                this.txtID_NUMBER.Text = dataUser.ID_NUMBER.ConvertToString();
                this.divID_NUMBER.Visible = !(dataUser.ID_NUMBER.ConvertToString().Trim() == string.Empty);
                this.txtPASSPORT.Text = dataUser.PASSPORT.ConvertToString();
                this.divPASSPORT.Visible = !(dataUser.PASSPORT.ConvertToString().Trim() == string.Empty);
                this.txtNATION.Text = dataUser.PASSPORT_REGION.ConvertToString();
                this.divNATION.Visible = !(dataUser.PASSPORT_REGION.ConvertToString().Trim() == string.Empty);
                this.txtPHONE.Text = dataUser.PHONE_1.ConvertToString();
                this.divPHONE.Visible = !(dataUser.PHONE_1.ConvertToString().Trim() == string.Empty);
                this.txtEMAIL.Text = dataUser.EMAIL_1.ConvertToString();
                this.divEMAIL.Visible = !(dataUser.EMAIL_1.ConvertToString().Trim() == string.Empty);
                this.txtAddress.Text = dataUser.C_ADDRESS.ConvertToString();
                this.divAddress.Visible = !(dataUser.C_ADDRESS.ConvertToString().Trim() == string.Empty);


                #region 聯名戶

                if (dataUser.CUST_CNAME2.ConvertToString().Trim() == string.Empty)
                {
                    this.divCust2.Visible = false;
                    this.divCUST_CNAME2.Visible = false;
                    this.txtCUST_CNAME2.Visible = false;
                    this.divCUST_ENAME2.Visible = false;
                    this.txtCUST_ENAME2.Visible = false;
                    this.divSex2.Visible = false;
                    this.txtSex2.Visible = false;
                    this.divBirthday2.Visible = false;
                    this.txtBirthday2.Visible = false;
                    this.divNATION2.Visible = false;
                    this.txtNATION2.Visible = false;
                    this.divPASSPORT2.Visible = false;
                    this.txtPASSPORT2.Visible = false;
                    this.divPHONE2.Visible = false;
                    this.txtPHONE2.Visible = false;
                    this.divID_NUMBER2.Visible = false;
                    this.txtID_NUMBER2.Visible = false;
                    this.divEMAIL2.Visible = false;
                    this.txtEMAIL2.Visible = false;
                    this.divAddress2.Visible = false;
                    this.txtAddress2.Visible = false;
                }
                else
                {

                    this.txtCUST_CNAME2.Text = dataUser.CUST_CNAME2.ConvertToString();
                    this.divCUST_CNAME2.Visible = !(dataUser.CUST_CNAME2.ConvertToString().Trim() == string.Empty);
                    this.txtCUST_ENAME2.Text = dataUser.CUST_ENAME2.ConvertToString();
                    this.divCUST_ENAME2.Visible = !(dataUser.CUST_ENAME2.ConvertToString().Trim() == string.Empty);
                    if (dataUser.SEX2.ConvertToString() == "M")
                    {
                        this.txtSex2.Text = "男士";
                    }
                    else if (dataUser.SEX2.ConvertToString() == "F")
                    {
                        this.txtSex2.Text = "女士";
                    }
                    else
                    {
                        this.divSex2.Visible = false;
                    }

                    if (dataUser.DATE_OF_BIRTH2 != null)
                    {
                        this.txtBirthday2.Text = dataUser.DATE_OF_BIRTH2.Value.ToString("yyyy/MM/dd");
                    }
                    else
                    {
                        this.divBirthday2.Visible = false;
                    }

                    this.txtID_NUMBER2.Text = dataUser.ID_NUMBER2.ConvertToString();
                    this.divID_NUMBER2.Visible = !(dataUser.ID_NUMBER2.ConvertToString().Trim() == string.Empty);
                    this.txtPASSPORT2.Text = dataUser.PASSPORT2.ConvertToString();
                    this.divPASSPORT2.Visible = !(dataUser.PASSPORT2.ConvertToString().Trim() == string.Empty);
                    this.txtNATION2.Text = dataUser.PASSPORT_REGION2.ConvertToString();
                    this.divNATION2.Visible = !(dataUser.PASSPORT_REGION2.ConvertToString().Trim() == string.Empty);
                    this.txtPHONE2.Text = dataUser.PHONE_2.ConvertToString();
                    this.divPHONE2.Visible = !(dataUser.PHONE_2.ConvertToString().Trim() == string.Empty);
                    this.txtEMAIL2.Text = dataUser.EMAIL_2.ConvertToString();
                    this.divEMAIL2.Visible = !(dataUser.EMAIL_2.ConvertToString().Trim() == string.Empty);
                    this.txtAddress2.Text = dataUser.C_ADDRESS2.ConvertToString();
                    this.divAddress2.Visible = !(dataUser.C_ADDRESS2.ConvertToString().Trim() == string.Empty);

                }
                #endregion

                #endregion

                #region 銀行資料
                //dataBank = dataBank.Where
                //    (
                //        x =>
                //            x.BANK_CNAME.ConvertToString().Trim() != string.Empty &&
                //            x.ACCOUNT_CNAME.ConvertToString().Trim() != string.Empty &&
                //            x.ACCOUNT.ConvertToString().Trim() != string.Empty
                //    ).ToList();
                //  2019/01/29 > 調整為判斷Account就好, 有就Show
                dataBank = dataBank.Where
                    (
                        x =>
                            x.ACCOUNT.ConvertToString().Trim() != string.Empty
                    ).ToList();
                dataBank.ForEach(x =>
                {
                    x.BANK_ENAME = x.BANK_ENAME.ConvertToString().Trim() == string.Empty ? "" : ("&nbsp;&nbsp;(" + x.BANK_ENAME.ConvertToString() + ")");
                });
                this.rpData.DataSource = dataBank;
                this.rpData.DataBind();
                for (int i = 0; i < this.rpData.Items.Count; i++)
                {
                    if (((Label)this.rpData.Items[i].FindControl("lblACCOUNT_CNAME2")).Text.Trim() == string.Empty)
                    {
                        ((System.Web.UI.HtmlControls.HtmlGenericControl)this.rpData.Items[i].FindControl("divBank2")).Visible = false;
                    }
                    else
                    {
                        ((System.Web.UI.HtmlControls.HtmlGenericControl)this.rpData.Items[i].FindControl("divBank2")).Visible = true;
                    }
                }
                #endregion
            }
        }

        protected void rpData_DataBinding(object sender, EventArgs e)
        {
            
        }
    }
}