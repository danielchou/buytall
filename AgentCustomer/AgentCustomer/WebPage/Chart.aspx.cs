﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgentCustomer.Logic;
using AgentCustomer.Logic.BusinessLogic;
using AgentCustomer.Utility;
using AgentCustomer.Model;
using AgentCustomer.Model.Table;
using System.Text;
using System.Web.Script.Serialization;


namespace AgentCustomer.WebPage
{
    public partial class Chart : System.Web.UI.Page
    {
        public string strChartData = "";

        public string strCharDate_InterestYear = "";
        public string strCharDate_InterestData = "";


        public string IsInterestYear_USD = "";
        public string strCharDate_InterestYear_USD = "";
        public string strCharDate_InterestData_USD = "";

        public string IsInterestYear_NTD = "";
        public string strCharDate_InterestYear_NTD = "";
        public string strCharDate_InterestData_NTD = "";

        public string IsInterestYear_RMB = "";
        public string strCharDate_InterestYear_RMB = "";
        public string strCharDate_InterestData_RMB = "";


        public string IsInterestYear_AUD = "";
        public string strCharDate_InterestYear_AUD = "";
        public string strCharDate_InterestData_AUD = "";


        public string IsInterestYear_EUR = "";
        public string strCharDate_InterestYear_EUR = "";
        public string strCharDate_InterestData_EUR = "";

        public string IsInterestYear_JPY = "";
        public string strCharDate_InterestYear_JPY = "";
        public string strCharDate_InterestData_JPY = "";

        public string IsInterestYear_NZD = "";
        public string strCharDate_InterestYear_NZD = "";
        public string strCharDate_InterestData_NZD = "";


        private BL_CUST GetCurrentUser
        {
            get
            {
                BL_CUST _user = new BL_CUST();

                if (this.Session["_User"] != null)
                {
                    if ((this.Session["_User"] as BL_CUST) == null)
                    {
                        this.SessionOut();
                    }
                    else
                    {
                        _user = this.Session["_user"] as BL_CUST;
                    }
                }
                else
                {
                    this.SessionOut();
                }


                return _user;
            }
        }

        private void SessionOut()
        {
            //var strScript = "<script> alert('連線逾時') </script>";
            //this.Page.RegisterClientScriptBlock("Alert", strScript);
            Response.Redirect("~/Customer_Login.aspx");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Page.IsPostBack == false)
            {
                this.BindChartData();
            }
        }


        private void BindChartData()
        {
            var dataUser = this.GetCurrentUser;
            if (dataUser != null)
            {
                var strCustID = dataUser.CUST_ID == null ? "" : dataUser.CUST_ID.Value.ConvertToString();
                var data = new LogicCustomer().GetChartDataByCustID(strCustID);
                var jsonData = string.Empty;
                decimal sumTotal = 0;
                if (data.Count() > 0)
                {
                    sumTotal = data.Sum(x => x.BALANCE == null ? 0 : x.BALANCE.Value);
                }
                data.ForEach(x =>
                {
                    jsonData += jsonData == string.Empty ? "" : ",";
                    jsonData += "['" + (x.YEAR == null ? "" : x.YEAR.ConvertToString()) + "年期', " + (x.BALANCE == null ? "0" : x.BALANCE.Value.ConvertToString()) + "]";

                    //jsonData += "[x:'" + (x.YEAR == null ? "" : x.YEAR.ConvertToString()) + "年期',y: " + (x.BALANCE == null ? "0" : x.BALANCE.Value.ConvertToString()) + "]";

                    //var percentage = x.BALANCE == null ? 0 : x.BALANCE.Value;
                    //if (percentage != 0 && sumTotal != 0)
                    //{
                    //    percentage = percentage / sumTotal * 100;
                    //}
                    //else
                    //{
                    //    percentage = 0;
                    //}
                    //jsonData += "['" + (x.YEAR == null ? "" : x.YEAR.ConvertToString()) + "年期 " + percentage.ToString("0.0") + "%', " + (x.BALANCE == null ? "0" : x.BALANCE.Value.ConvertToString()) + "]";
                });
                jsonData = "[" + jsonData + "]";
                this.strChartData = jsonData;


                var dataOrder = new LogicCustomer().GetContractDataByCustID(strCustID);
                List<BL_ORDER> dataInterest;
                var jsonDataYear = string.Empty;
                var jsonDataValue = string.Empty;
                #region USD
                dataInterest = new LogicCustomer().GetChartInterestDataByCustID(dataOrder, "USD");
                jsonDataYear = string.Empty;
                jsonDataValue = string.Empty;

                if (dataInterest != null)
                {
                    dataInterest
                        .Select(x => x.YEAR)
                        .Distinct()
                        .OrderBy(x => x)
                        .ToList()
                        .ForEach(x =>
                        {
                            jsonDataYear += jsonDataYear == string.Empty ? "" : ",";
                            jsonDataYear += x.Value;
                        });
                    jsonDataYear = "[" + jsonDataYear + "]";
                    dataInterest.ForEach(x =>
                    {
                        jsonDataValue += jsonDataValue == string.Empty ? "" : ",";
                        jsonDataValue += x.ACCUMULATE_INTEREST == null ? "null" : x.ACCUMULATE_INTEREST.Value.ConvertToString();
                    });
                    jsonDataValue = "[" + jsonDataValue + "]";
                    this.IsInterestYear_USD = "Y";
                }
                else
                {
                    this.IsInterestYear_USD = "N";
                    jsonDataYear = "[]";
                    jsonDataValue = "[]";
                }
                strCharDate_InterestYear_USD = jsonDataYear;
                strCharDate_InterestData_USD = jsonDataValue;
                #endregion

                #region NTD
                dataInterest = new LogicCustomer().GetChartInterestDataByCustID(dataOrder, "NTD");
                jsonDataYear = string.Empty;
                jsonDataValue = string.Empty;

                if (dataInterest != null)
                {
                    dataInterest
                        .Select(x => x.YEAR)
                        .Distinct()
                        .OrderBy(x => x)
                        .ToList()
                        .ForEach(x =>
                        {
                            jsonDataYear += jsonDataYear == string.Empty ? "" : ",";
                            jsonDataYear += x.Value;
                        });
                    jsonDataYear = "[" + jsonDataYear + "]";
                    dataInterest.ForEach(x =>
                    {
                        jsonDataValue += jsonDataValue == string.Empty ? "" : ",";
                        jsonDataValue += x.ACCUMULATE_INTEREST == null ? "null" : x.ACCUMULATE_INTEREST.Value.ConvertToString();
                    });
                    jsonDataValue = "[" + jsonDataValue + "]";
                    this.IsInterestYear_NTD = "Y";
                }
                else
                {
                    this.IsInterestYear_NTD = "N";
                    jsonDataYear = "[]";
                    jsonDataValue = "[]";
                }
                strCharDate_InterestYear_NTD = jsonDataYear;
                strCharDate_InterestData_NTD = jsonDataValue;
                #endregion

                #region RMB
                dataInterest = new LogicCustomer().GetChartInterestDataByCustID(dataOrder, "RMB");
                jsonDataYear = string.Empty;
                jsonDataValue = string.Empty;

                if (dataInterest != null)
                {
                    dataInterest
                        .Select(x => x.YEAR)
                        .Distinct()
                        .OrderBy(x => x)
                        .ToList()
                        .ForEach(x =>
                        {
                            jsonDataYear += jsonDataYear == string.Empty ? "" : ",";
                            jsonDataYear += x.Value;
                        });
                    jsonDataYear = "[" + jsonDataYear + "]";
                    dataInterest.ForEach(x =>
                    {
                        jsonDataValue += jsonDataValue == string.Empty ? "" : ",";
                        jsonDataValue += x.ACCUMULATE_INTEREST == null ? "null" : x.ACCUMULATE_INTEREST.Value.ConvertToString();
                    });
                    jsonDataValue = "[" + jsonDataValue + "]";
                    this.IsInterestYear_RMB = "Y";
                }
                else
                {
                    this.IsInterestYear_RMB = "N";
                    jsonDataYear = "[]";
                    jsonDataValue = "[]";
                }
                strCharDate_InterestYear_RMB = jsonDataYear;
                strCharDate_InterestData_RMB = jsonDataValue;
                #endregion


                #region AUD
                dataInterest = new LogicCustomer().GetChartInterestDataByCustID(dataOrder, "AUD");
                jsonDataYear = string.Empty;
                jsonDataValue = string.Empty;

                if (dataInterest != null)
                {
                    dataInterest
                        .Select(x => x.YEAR)
                        .Distinct()
                        .OrderBy(x => x)
                        .ToList()
                        .ForEach(x =>
                        {
                            jsonDataYear += jsonDataYear == string.Empty ? "" : ",";
                            jsonDataYear += x.Value;
                        });
                    jsonDataYear = "[" + jsonDataYear + "]";
                    dataInterest.ForEach(x =>
                    {
                        jsonDataValue += jsonDataValue == string.Empty ? "" : ",";
                        jsonDataValue += x.ACCUMULATE_INTEREST == null ? "null" : x.ACCUMULATE_INTEREST.Value.ConvertToString();
                    });
                    jsonDataValue = "[" + jsonDataValue + "]";
                    this.IsInterestYear_AUD = "Y";
                }
                else
                {
                    this.IsInterestYear_AUD = "N";
                    jsonDataYear = "[]";
                    jsonDataValue = "[]";
                }
                strCharDate_InterestYear_AUD = jsonDataYear;
                strCharDate_InterestData_AUD = jsonDataValue;
                #endregion




                #region EUR
                dataInterest = new LogicCustomer().GetChartInterestDataByCustID(dataOrder, "EUR");
                jsonDataYear = string.Empty;
                jsonDataValue = string.Empty;

                if (dataInterest != null)
                {
                    dataInterest
                        .Select(x => x.YEAR)
                        .Distinct()
                        .OrderBy(x => x)
                        .ToList()
                        .ForEach(x =>
                        {
                            jsonDataYear += jsonDataYear == string.Empty ? "" : ",";
                            jsonDataYear += x.Value;
                        });
                    jsonDataYear = "[" + jsonDataYear + "]";
                    dataInterest.ForEach(x =>
                    {
                        jsonDataValue += jsonDataValue == string.Empty ? "" : ",";
                        jsonDataValue += x.ACCUMULATE_INTEREST == null ? "null" : x.ACCUMULATE_INTEREST.Value.ConvertToString();
                    });
                    jsonDataValue = "[" + jsonDataValue + "]";
                    this.IsInterestYear_EUR = "Y";
                }
                else
                {
                    this.IsInterestYear_EUR = "N";
                    jsonDataYear = "[]";
                    jsonDataValue = "[]";
                }
                strCharDate_InterestYear_EUR = jsonDataYear;
                strCharDate_InterestData_EUR = jsonDataValue;
                #endregion


                #region JPY
                dataInterest = new LogicCustomer().GetChartInterestDataByCustID(dataOrder, "JPY");
                jsonDataYear = string.Empty;
                jsonDataValue = string.Empty;

                if (dataInterest != null)
                {
                    dataInterest
                        .Select(x => x.YEAR)
                        .Distinct()
                        .OrderBy(x => x)
                        .ToList()
                        .ForEach(x =>
                        {
                            jsonDataYear += jsonDataYear == string.Empty ? "" : ",";
                            jsonDataYear += x.Value;
                        });
                    jsonDataYear = "[" + jsonDataYear + "]";
                    dataInterest.ForEach(x =>
                    {
                        jsonDataValue += jsonDataValue == string.Empty ? "" : ",";
                        jsonDataValue += x.ACCUMULATE_INTEREST == null ? "null" : x.ACCUMULATE_INTEREST.Value.ConvertToString();
                    });
                    jsonDataValue = "[" + jsonDataValue + "]";
                    this.IsInterestYear_JPY = "Y";
                }
                else
                {
                    this.IsInterestYear_JPY = "N";
                    jsonDataYear = "[]";
                    jsonDataValue = "[]";
                }
                strCharDate_InterestYear_JPY = jsonDataYear;
                strCharDate_InterestData_JPY = jsonDataValue;
                #endregion


                #region NZD
                dataInterest = new LogicCustomer().GetChartInterestDataByCustID(dataOrder, "NZD");
                jsonDataYear = string.Empty;
                jsonDataValue = string.Empty;

                if (dataInterest != null)
                {
                    dataInterest
                        .Select(x => x.YEAR)
                        .Distinct()
                        .OrderBy(x => x)
                        .ToList()
                        .ForEach(x =>
                        {
                            jsonDataYear += jsonDataYear == string.Empty ? "" : ",";
                            jsonDataYear += x.Value;
                        });
                    jsonDataYear = "[" + jsonDataYear + "]";
                    dataInterest.ForEach(x =>
                    {
                        jsonDataValue += jsonDataValue == string.Empty ? "" : ",";
                        jsonDataValue += x.ACCUMULATE_INTEREST == null ? "null" : x.ACCUMULATE_INTEREST.Value.ConvertToString();
                    });
                    jsonDataValue = "[" + jsonDataValue + "]";
                    this.IsInterestYear_NZD = "Y";
                }
                else
                {
                    this.IsInterestYear_NZD = "N";
                    jsonDataYear = "[]";
                    jsonDataValue = "[]";
                }
                strCharDate_InterestYear_NZD = jsonDataYear;
                strCharDate_InterestData_NZD = jsonDataValue;
                #endregion

                //var dataOrder = new LogicCustomer().GetContractDataByCustID(strCustID);
                //var dataInterest = new LogicCustomer().GetChartInterestDataByCustID(dataOrder);
                //var jsonDataYear = string.Empty;
                //var jsonDataValue = string.Empty;
                //dataInterest
                //    .Select(x => x.YEAR)
                //    .Distinct()
                //    .OrderBy(x => x)
                //    .ToList()
                //    .ForEach(x =>
                //    {
                //        jsonDataYear += jsonDataYear == string.Empty ? "" : ",";
                //        jsonDataYear += x.Value;
                //    });
                //jsonDataYear = "[" + jsonDataYear + "]";
                //dataInterest.ForEach(x =>
                //{
                //    jsonDataValue += jsonDataValue == string.Empty ? "" : ",";
                //    jsonDataValue += x.ACCUMULATE_INTEREST == null ? "null" : x.ACCUMULATE_INTEREST.Value.ConvertToString();
                //});
                //jsonDataValue = "[" + jsonDataValue + "]";

                //strCharDate_InterestYear = jsonDataYear;
                //strCharDate_InterestData = jsonDataValue;
            }
        }



    }
}