﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Theme/Theme.Master" AutoEventWireup="true" CodeBehind="BaseInfo.aspx.cs" Inherits="AgentCustomer.WebPage.BaseInfo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .card-text span {
            color: black;
            font-size: 18px;
        }
     


        button.Card-Main {
            padding: 3px 3px 3px 3px !important;
            margin: 8px !important;
        }
            button.Card-Main h3 {
                font-size: 18px !important;
            }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <div class="section light-bg" id="features">
        <div class="container">
            <div class="row" style="display:none;">

                <button type="button" class="btn btn-primary Card-Main">
                    <h3 style="color: #FFFFFF; font-weight: bold;">合約數</h3>   
                    <span class="badge badge-light" style="font-size: 20px">
                        <asp:Label ID="lblContractCount" runat="server" Text=""></asp:Label>
                    </span>
                </button>

                <button type="button" class="btn btn-primary Card-Main">
                    <h3 style="color: #FFFFFF; font-weight: bold;"> 總餘額</h3>   
                    <span class="badge badge-light" style="font-size:20px">
                        <asp:Label ID="lblTotalBalance" runat="server" Text="">$10,000</asp:Label>
                    </span>
                </button>

                <button type="button" class="btn btn-primary Card-Main">
                    <h3 style="color: #FFFFFF; font-weight: bold;">本金</h3>   
                    <span class="badge badge-light" style="font-size: 20px">
                        <asp:Label ID="lblBaseAmount" runat="server" Text="">$10,000</asp:Label>
                    </span>
                </button>

                <button type="button" class="btn btn-primary Card-Main">
                    <h3 style="color: #FFFFFF; font-weight: bold;">可提領紅利</h3>   
                    <span class="badge badge-light" style="font-size: 20px">
                        <asp:Label ID="lblCanOutAmount" runat="server" Text="">$10,000</asp:Label>
                    </span>
                </button>

                <button type="button" class="btn btn-primary Card-Main">
                    <h3 style="color: #FFFFFF; font-weight: bold;">累積總派利<br /></h3>   
                    <span class="badge badge-light" style="font-size: 20px">
                        <asp:Label ID="Label1" runat="server" Text="">$10,000</asp:Label>
                    </span>
                </button>

                <button type="button" class="btn btn-primary Card-Main">
                    <h3 style="color: #FFFFFF; font-weight: bold;">累計投資年期<br /></h3>   
                    <span class="badge badge-light" style="font-size: 20px">
                        <asp:Label ID="Label2" runat="server" Text="">$10,000</asp:Label>
                    </span>
                </button>
                
            </div>
            
            <div class="section-title" style="margin-bottom:0.75rem; padding-top:1.25rem;">
                <h3>基本資料</h3>
            </div>
            <div class="row" id="divCUST_CNAME" runat="server">
                <h4 class="card-title" style="padding-right:12px;">姓名</h4>
                <p class="card-text"><asp:Label ID="txtCUST_CNAME" runat="server" Text=""></asp:Label></p>
            </div>
            <div class="row" id="divCUST_ENAME" runat="server">
                <h4 class="card-title" style="padding-right:12px;">姓名(英)</h4>
                <p class="card-text"><asp:Label ID="txtCUST_ENAME" runat="server" Text=""></asp:Label></p>
            </div>
            <div class="row" id="divSex" runat="server">
                <h4 class="card-title" style="padding-right:12px;">性別</h4>
                <p class="card-text"><asp:Label ID="txtSex" runat="server" Text=""></asp:Label></p>
            </div>
            <div class="row" id="divBirthday" runat="server">
                <h4 class="card-title" style="padding-right:12px;">出生年月日</h4>
                <p class="card-text"><asp:Label ID="txtBirthday" runat="server" Text=""></asp:Label></p>
            </div>
            <div class="row" id="divNATION" runat="server">
                <h4 class="card-title" style="padding-right:12px;">國籍</h4>
                <p class="card-text"><asp:Label ID="txtNATION" runat="server" Text=""></asp:Label></p>
            </div>
            <div class="row" id="divPASSPORT" runat="server">
                <h4 class="card-title" style="padding-right:12px;">護照號碼</h4>
                <p class="card-text"><asp:Label ID="txtPASSPORT" runat="server" Text=""></asp:Label></p>
            </div>
            <div class="row" id="divPHONE" runat="server">
                <h4 class="card-title" style="padding-right:12px;">聯絡電話</h4>
                <p class="card-text"><asp:Label ID="txtPHONE" runat="server" Text=""></asp:Label></p>
            </div>
            <div class="row" id="divID_NUMBER" runat="server">
                <h4 class="card-title" style="padding-right:12px;">身分證號碼</h4>
                <p class="card-text"><asp:Label ID="txtID_NUMBER" runat="server" Text=""></asp:Label></p>
            </div>
            <div class="row" id="divEMAIL" runat="server">
                <h4 class="card-title" style="padding-right:12px;">電子郵件信箱</h4>
                <p class="card-text"><asp:Label ID="txtEMAIL" runat="server" Text=""></asp:Label></p>
            </div>
            <div class="row" id="divAddress" runat="server">
                <h4 class="card-title" style="padding-right:12px;">居住地址</h4>
                <p class="card-text"><asp:Label ID="txtAddress" runat="server" Text=""></asp:Label></p>
            </div>

            
            <div class="section-title" style="margin-bottom:0.75rem;" id="divCust2" runat="server">
                <h5>聯名持有人</h5>
            </div>
            <div class="row" id="divCUST_CNAME2" runat="server">
                <h4 class="card-title" style="padding-right:12px;">姓名</h4>
                <p class="card-text"><asp:Label ID="txtCUST_CNAME2" runat="server" Text=""></asp:Label></p>
            </div>
            <div class="row" id="divCUST_ENAME2" runat="server">
                <h4 class="card-title" style="padding-right:12px;">姓名(英)</h4>
                <p class="card-text"><asp:Label ID="txtCUST_ENAME2" runat="server" Text=""></asp:Label></p>
            </div>
            <div class="row" id="divSex2" runat="server">
                <h4 class="card-title" style="padding-right:12px;">性別</h4>
                <p class="card-text"><asp:Label ID="txtSex2" runat="server" Text=""></asp:Label></p>
            </div>
            <div class="row" id="divBirthday2" runat="server">
                <h4 class="card-title" style="padding-right:12px;">出生年月日</h4>
                <p class="card-text"><asp:Label ID="txtBirthday2" runat="server" Text=""></asp:Label></p>
            </div>
            <div class="row" id="divNATION2" runat="server">
                <h4 class="card-title" style="padding-right:12px;">國籍</h4>
                <p class="card-text"><asp:Label ID="txtNATION2" runat="server" Text=""></asp:Label></p>
            </div>
            <div class="row" id="divPASSPORT2" runat="server">
                <h4 class="card-title" style="padding-right:12px;">護照號碼</h4>
                <p class="card-text"><asp:Label ID="txtPASSPORT2" runat="server" Text=""></asp:Label></p>
            </div>
            <div class="row" id="divPHONE2" runat="server">
                <h4 class="card-title" style="padding-right:12px;">聯絡電話</h4>
                <p class="card-text"><asp:Label ID="txtPHONE2" runat="server" Text=""></asp:Label></p>
            </div>
            <div class="row" id="divID_NUMBER2" runat="server">
                <h4 class="card-title" style="padding-right:12px;">身分證號碼</h4>
                <p class="card-text"><asp:Label ID="txtID_NUMBER2" runat="server" Text=""></asp:Label></p>
            </div>
            <div class="row" id="divEMAIL2" runat="server">
                <h4 class="card-title" style="padding-right:12px;">電子郵件信箱</h4>
                <p class="card-text"><asp:Label ID="txtEMAIL2" runat="server" Text=""></asp:Label></p>
            </div>
            <div class="row" id="divAddress2" runat="server">
                <h4 class="card-title" style="padding-right:12px;">居住地址</h4>
                <p class="card-text"><asp:Label ID="txtAddress2" runat="server" Text=""></asp:Label></p>
            </div>


            <div class="section-title" style="margin-bottom:0.75rem;">
                <h3>銀行資料</h3>
            </div>
            <asp:Repeater ID="rpData" runat="server" OnDataBinding="rpData_DataBinding">
                <ItemTemplate>
                   <%-- <div class="row" style="padding-top:12px;">
                        <h4 class="card-title" style="padding-right:12px;">No.</h4>
                        <p class="card-text"><asp:Label ID="Label1" runat="server" Text='<%# Container.ItemIndex + 1 %>'></asp:Label></p>
                    </div>--%>
                    
                    <div class="row">
                        <h4 class="card-title" style="padding-right:12px;">帳戶持有人姓名</h4>
                        <p class="card-text"><asp:Label ID="txtACCOUNT_CNAME" runat="server" Text='<%# Eval("ACCOUNT_CNAME") %>'></asp:Label>
                            &nbsp;&nbsp;&nbsp;<asp:Label ID="txtACCOUNT_ENAME" runat="server" Text='<%# Eval("ACCOUNT_ENAME") %>'></asp:Label>
                        </p>
                    </div>
                    <div class="row">
                        <h4 class="card-title" style="padding-right:12px;">幣別</h4>
                        <p class="card-text"><asp:Label ID="txtCurrency" runat="server" Text='<%# Eval("CURRENCY") %>'></asp:Label><asp:Label ID="lblACCOUNT_TYPE" runat="server" Text='<%# Eval("ACCOUNT_TYPE_DESC") %>'></asp:Label></p>
                    </div>
                    <div class="row">
                        <h4 class="card-title" style="padding-right:12px;">銀行名稱</h4>
                        <p class="card-text"><asp:Label ID="txtBANK_CNAME" runat="server" Text='<%# Eval("BANK_CNAME") %>'></asp:Label>
                            &nbsp;&nbsp;&nbsp;<asp:Label ID="txtBANK_ENAME" runat="server" Text='<%# Eval("BANK_ENAME") %>'></asp:Label>
                        </p>
                    </div>
                    <div class="row">
                        <h4 class="card-title" style="padding-right:12px;">分行名稱</h4>
                        <p class="card-text"><asp:Label ID="txtBRANCH_CNAME" runat="server" Text='<%# Eval("BRANCH_CNAME") %>'></asp:Label>
                            &nbsp;&nbsp;&nbsp;<asp:Label ID="txtBRANCH_ENAME" runat="server" Text='<%# Eval("BRANCH_ENAME") %>'></asp:Label>
                        </p>
                    </div>
                    <div class="row">
                        <h4 class="card-title" style="padding-right:12px;">銀行帳戶</h4>
                        <p class="card-text"><asp:Label ID="txtACCOUNT" runat="server" Text='<%# Eval("ACCOUNT") %>'></asp:Label></p>
                    </div>
                    <div class="row">
                        <h4 class="card-title" style="padding-right:12px;">SWIFT_CODE</h4>
                        <p class="card-text"><asp:Label ID="txtSWIFT_CODE" runat="server" Text='<%# Eval("SWIFT_CODE") %>'></asp:Label></p>
                    </div>
                    <div class="row">
                        <h4 class="card-title" style="padding-right:12px;">銀行地址</h4>
                        <p class="card-text"><asp:Label ID="txtBANK_C_ADDRESS" runat="server" Text='<%# Eval("BANK_C_ADDRESS") %>'></asp:Label>
                            &nbsp;&nbsp;&nbsp;<asp:Label ID="txtBANK_E_ADDRESS" runat="server" Text='<%# Eval("BANK_E_ADDRESS") %>'></asp:Label>
                        </p>
                    </div>
                    
                    <div id="divBank2" runat="server" visible="false" >    
                        <div class="section-title" style="margin-bottom:0.75rem;">
                            <h5>聯名持有人</h5>
                        </div>
                        <div class="row">
                            <h4 class="card-title" style="padding-right:12px;">帳戶持有人姓名</h4>
                            <p class="card-text"><asp:Label ID="lblACCOUNT_CNAME2" runat="server" Text='<%# Eval("ACCOUNT_CNAME2") %>'></asp:Label>
                                &nbsp;&nbsp;&nbsp;<asp:Label ID="lblACCOUNT_ENAME2" runat="server" Text='<%# Eval("ACCOUNT_ENAME2") %>'></asp:Label>
                            </p>
                        </div>
                        <div class="row">
                            <h4 class="card-title" style="padding-right:12px;">幣別</h4>
                            <p class="card-text"><asp:Label ID="lblCURRENCY2" runat="server" Text='<%# Eval("CURRENCY") %>'></asp:Label></p>
                        </div>
                        <div class="row">
                            <h4 class="card-title" style="padding-right:12px;">銀行名稱</h4>
                            <p class="card-text"><asp:Label ID="lblBANK_CNAME2" runat="server" Text='<%# Eval("BANK_CNAME2") %>'></asp:Label>
                                &nbsp;&nbsp;&nbsp;<asp:Label ID="lblBANK_ENAME2" runat="server" Text='<%# Eval("BANK_ENAME2") %>'></asp:Label>
                            </p>
                        </div>
                        <div class="row">
                            <h4 class="card-title" style="padding-right:12px;">分行名稱</h4>
                            <p class="card-text"><asp:Label ID="lblBRANCH_CNAME2" runat="server" Text='<%# Eval("BRANCH_CNAME2") %>'></asp:Label>
                                &nbsp;&nbsp;&nbsp;<asp:Label ID="lblBRANCH_ENAME2" runat="server" Text='<%# Eval("BRANCH_ENAME2") %>'></asp:Label>
                            </p>
                        </div>
                        <div class="row">
                            <h4 class="card-title" style="padding-right:12px;">銀行帳戶</h4>
                            <p class="card-text"><asp:Label ID="lblACCOUNT2" runat="server" Text='<%# Eval("ACCOUNT2") %>'></asp:Label></p>
                        </div>
                        <div class="row">
                            <h4 class="card-title" style="padding-right:12px;">SWIFT_CODE</h4>
                            <p class="card-text"><asp:Label ID="lblSWIFT_CODE2" runat="server" Text='<%# Eval("SWIFT_CODE2") %>'></asp:Label></p>
                        </div>
                        <div class="row">
                            <h4 class="card-title" style="padding-right:12px;">銀行地址</h4>
                            <p class="card-text"><asp:Label ID="lblBANK_C_ADDRESS2" runat="server" Text='<%# Eval("BANK_C_ADDRESS2") %>'></asp:Label>
                                &nbsp;&nbsp;&nbsp;<asp:Label ID="lblBANK_E_ADDRESS2" runat="server" Text='<%# Eval("BANK_E_ADDRESS2") %>'></asp:Label>
                            </p>
                        </div>
                    
                    </div>
                    <div style="height:35px;">&nbsp;</div>
                </ItemTemplate>
            </asp:Repeater>

        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="server">
</asp:Content>
