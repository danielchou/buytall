﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Theme/Theme.Master" AutoEventWireup="true" CodeBehind="DepositOut.aspx.cs" Inherits="AgentCustomer.WebPage.DepositOut" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        
        .th-Header {
            text-align: center;
        }
        .td-Text {
            text-align: left;
        }
        .td-Numeric {
            text-align: right;
        }
        
        .td-Center {
            text-align: center;
        }

        .table th {
            font-size: 15px;
        }
        .table td {
            font-size: 14px;
        }
        .table tfoot td {
            font-weight: bold;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="section light-bg" id="gallery">
        <div class="container">
            <div class="section-title" >
                <h3>您的提領紀錄</h3>
                
                <div id="divDepositData" runat="server">

                </div>
            </div>

        </div>

    </div>
    
        <asp:HiddenField ID="hidBatchID" runat="server" />
        <asp:HiddenField ID="hidOrderNo" runat="server" />
        <asp:Button ID="btnShow" runat="server" Text="" style="display:none;width:0;" OnClick="btnShow_Click" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="server">
    <script>

        function OpenDatail(BatchID) {
            $("#<%=this.hidBatchID.ClientID%>").val(BatchID);
            $("#<%=this.btnShow.ClientID%>").click();
        }

    </script>
</asp:Content>
