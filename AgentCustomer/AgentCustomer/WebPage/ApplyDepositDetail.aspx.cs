﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgentCustomer.Logic;
using AgentCustomer.Logic.BusinessLogic;
using AgentCustomer.Utility;
using AgentCustomer.Model;
using AgentCustomer.Model.Table;
using System.Text;
using System.Web.Script.Serialization;
using System.IO;
using System.Net;
using System.Net.Mail;

namespace AgentCustomer.WebPage
{
    public partial class ApplyDepositDetail : System.Web.UI.Page
    {
        private BL_CUST GetCurrentUser
        {
            get
            {
                BL_CUST _user = new BL_CUST();

                if (this.Session["_User"] != null)
                {
                    if ((this.Session["_User"] as BL_CUST) == null)
                    {
                        this.SessionOut();
                    }
                    else
                    {
                        _user = this.Session["_user"] as BL_CUST;
                    }
                }
                else
                {
                    this.SessionOut();
                }


                return _user;
            }
        }

        private void SessionOut()
        {
            //var strScript = "<script> alert('連線逾時') </script>";
            //this.Page.RegisterClientScriptBlock("Alert", strScript);
            Response.Redirect("~/Customer_Login.aspx");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Page.IsPostBack == false)
            {

                //  2019/10/28 > 如果是業務Login就無條件開啟
                if (this.GetCurrentUser.IS_SALES_LOGIN.ConvertToString().Trim() != "Y")
                {
                    switch (this.GetCurrentUser.IS_ONLINE_WITHDRAWAL.ConvertToString().ToUpper())
                    {
                        case "P":   //  Phone
                        case "E":   //  Email
                                    //  Nothing
                            break;
                        default:
                            this.Page.Response.Redirect("~/WebPage/ContractInfo.aspx");
                            return;
                            break;
                    }
                }
                this.BindData();
            }
        }

        protected void linkBack_Click(object sender, EventArgs e)
        {
            this.Page.Response.Redirect("~/WebPage/ContractInfo.aspx");
        }

        private void BindData()
        {
            var dataOnPath = new LogicCustomer().GetApplyTempData(this.GetCurrentUser.CUST_ID == null ? "" : this.GetCurrentUser.CUST_ID.Value.ConvertToString());

            dataOnPath = dataOnPath.OrderByDescending(x => x.APPLY_DATE).ToList();

            this.GridView1.DataSource = dataOnPath;
            this.GridView1.DataBind();
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            switch (e.Row.RowType)
            {
                case DataControlRowType.Header:
                    break;
                case DataControlRowType.Footer:
                    break;
                case DataControlRowType.DataRow:
                    #region Data Row

                    var lblBATCH_WITHDRAWAL_ID = (Label)e.Row.FindControl("lblBATCH_WITHDRAWAL_ID");
                    var aExportPDF = (System.Web.UI.HtmlControls.HtmlAnchor)e.Row.FindControl("aExportPDF");
                    var strURL = this.Page.Request.Url.AbsoluteUri.ConvertToString().Replace("ApplyDepositDetail.aspx", "ExportPDF.aspx") +
                            "?sType=PDF&sID=" + lblBATCH_WITHDRAWAL_ID.Text;
                    aExportPDF.HRef =  strURL ;

                    //  2019/10/28 > 業務代出金 > 刪除功能
                    ((LinkButton)e.Row.FindControl("linkDel")).Visible = false;
                    var data = e.Row.DataItem as BL_ACCOUNT_TXN_TEMP;
                    if (data != null)
                    {
                        //  Todo > 先卡只限業務能夠做刪除動作, 客戶不可自己刪除
                        if (this.GetCurrentUser.IS_SALES_LOGIN.ConvertToString().Trim() == "Y")
                        {
                            //  業務代申請
                            if (data.SCHEDULE.ConvertToString().Trim() == "8")
                            {
                                ((LinkButton)e.Row.FindControl("linkDel")).Visible = true;
                                ((LinkButton)e.Row.FindControl("linkDel")).CommandArgument =
                                    (data.ORDER_ID == null ? "0" : data.ORDER_ID.Value.ConvertToString()) + ";" +
                                    (data.BATCH_WITHDRAWAL_ID == null ? "0" : data.BATCH_WITHDRAWAL_ID.Value.ConvertToString());
                            }
                        }
                    }
                    #endregion
                    break;
                case DataControlRowType.Separator:
                    break;
                case DataControlRowType.Pager:
                    break;
                case DataControlRowType.EmptyDataRow:
                    break;
                default:
                    break;
            }
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName.ConvertToString())
            {
                case "DELETE_DATA":
                    var spParam = e.CommandArgument.ConvertToString().Trim().Split(new string[] { ";" }, StringSplitOptions.None);
                    if (spParam.Length >= 2)
                    {
                        //  0   ORDER_ID
                        //  1   BATCH_WITHDRAWAL_ID
                        if (spParam[0].IsNumeric() && spParam[1].IsNumeric())
                        {
                            var ORDER_ID = long.Parse(spParam[0]);
                            var BATCH_WITHDRAWAL_ID = long.Parse(spParam[1]);

                            if (new LogicCustomer().DeleteTempDepositData(ORDER_ID, BATCH_WITHDRAWAL_ID))
                            {
                                var strScript = "<script> alert('資料已刪除') </script>";
                                this.Page.RegisterClientScriptBlock("Alert", strScript);
                                this.BindData();
                            }
                            else
                            {
                                var strScript = "<script> alert('資料刪除失敗') </script>";
                                this.Page.RegisterClientScriptBlock("Alert", strScript);
                            }
                        }
                    }
                    break;
                default:
                    break;
            }
        }

    }
}