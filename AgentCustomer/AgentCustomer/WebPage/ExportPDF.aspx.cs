﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using AgentCustomer.Utility;
using AgentCustomer.Logic.BusinessLogic;

namespace AgentCustomer.WebPage
{
    public partial class ExportPDF : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.IsPostBack)
            {
                //  sTYPE=PDF&sID=101902
                //  sTYPE=PDF&sID=102460
                var strType = this.Request["sType"].ConvertToString();
                var strID = this.Request["sID"].ConvertToString();

                if (strType.ToUpper() == "PDF" && strID.IsNumeric())
                {
                    var lnID = long.Parse(strID);
                    if (lnID > 0)
                    {
                        var dataApply = new LogicCustomer().GetApplyDeposit_RequestForm_Data(lnID);

                        if (dataApply.Count() > 0)
                        {
                            var strFileName = dataApply[0].ClientName.ConvertToString()
                                .Trim()
                                .Replace("/", "")
                                .Replace(@"\", "")
                                .Replace("-", "")
                                .Replace(" ", "")
                                .Replace("?", "");

                            ReportViewer1.ProcessingMode = ProcessingMode.Local;
                            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Report/rptApplyDeposit_RequestForm.rdlc");
                            ReportDataSource datasource = new ReportDataSource("dsReport", dataApply);
                            ReportViewer1.LocalReport.DataSources.Clear();
                            ReportViewer1.LocalReport.DataSources.Add(datasource);


                            Warning[] warnings;
                            string[] streamIds;
                            string contentType;
                            string encoding;
                            string extension;

                            //Export the RDLC Report to Byte Array.
                            byte[] bytes = ReportViewer1.LocalReport.Render("PDF", null, out contentType, out encoding, out extension, out streamIds, out warnings);

                            //Download the RDLC Report in Word, Excel, PDF and Image formats.
                            Response.Clear();
                            Response.Buffer = true;
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.ContentType = contentType;
                            Response.AppendHeader("Content-Disposition", "attachment; filename=" + HttpUtility.UrlEncode(strFileName) + "." + extension);
                            Response.BinaryWrite(bytes);
                            Response.Flush();
                            Response.End();
                        }
                    }
                }

            }
        }

    }
}