﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgentCustomer.Logic;
using AgentCustomer.Logic.BusinessLogic;
using AgentCustomer.Utility;
using AgentCustomer.Model;
using AgentCustomer.Model.Table;
using System.Text;
using System.Web.Script.Serialization;

namespace AgentCustomer.WebPage
{
    public partial class ContractInfo_Hist : System.Web.UI.Page
    {

        private BL_CUST GetCurrentUser
        {
            get
            {
                BL_CUST _user = new BL_CUST();

                if (this.Session["_User"] != null)
                {
                    if ((this.Session["_User"] as BL_CUST) == null)
                    {
                        this.SessionOut();
                    }
                    else
                    {
                        _user = this.Session["_user"] as BL_CUST;
                    }
                }
                else
                {
                    this.SessionOut();
                }


                return _user;
            }
        }

        private void SessionOut()
        {
            //var strScript = "<script> alert('連線逾時') </script>";
            //this.Page.RegisterClientScriptBlock("Alert", strScript);
            Response.Redirect("~/Customer_Login.aspx");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Page.IsPostBack == false)
            {
                this.BindCustBaseData();

            }

        }

        private void BindCustBaseData()
        {
            var dataUser = this.GetCurrentUser;
            if (dataUser != null)
            {
                this.BindContractData(dataUser.CUST_ID == null ? "" : dataUser.CUST_ID.Value.ConvertToString());
            }
        }


        private void BindContractData(string CustID)
        {
            var strHtml = new StringBuilder();
            var data = new LogicCustomer().GetContractDataByCustID(CustID);
            

            data = data.Where
                (
                    x =>
                        (x.EXTEND_DATE != null ? DateTime.Parse(x.EXTEND_DATE.Value.ToString("yyyy/MM/dd")) : (x.ORDER_END == null ? DateTime.Now.AddDays(1) : x.ORDER_END.Value)) < DateTime.Parse(DateTime.Now.ToString("yyyy/MM/dd"))
                ).ToList();


            if (data.Count > 0)
            {
                var dataCurrency = data.Select
                    (
                        x =>
                            new
                            {
                                CURRENCY = x.CURRENCY,
                                SORT = x.CURRENCY.ConvertToString() == "USD" ? 1 : 2
                            }
                    ).Distinct().ToList();

                if (dataCurrency.Count() <= 1)
                {
                    strHtml.AppendLine(this.GetContractData(data));
                }
                else
                {
                    #region 多幣別
                    var strTemp = string.Empty;
                    strHtml.AppendLine("<section class='project-tab'>");
                    strHtml.AppendLine("  <div class='container'>");
                    strHtml.AppendLine("    <div class='row'>");
                    strHtml.AppendLine("      <div class='col-md-12'>");
                    strHtml.AppendLine("        <nav>");
                    strHtml.AppendLine("         <div class='nav nav-tabs nav-fill' id='nav-tab' role='tablist'> ");
                    strTemp = string.Empty;
                    dataCurrency
                        .OrderBy(x => x.SORT)
                        .ThenBy(x => x.CURRENCY)
                        .ToList()
                        .ForEach(x =>
                        {
                            strTemp = strTemp == string.Empty ? "nav-item nav-link active" : "nav-item nav-link";
                            strHtml.AppendLine("  <a class='" + strTemp + "' id='nav-" + x.CURRENCY.ConvertToString() + "-tab' data-toggle='tab' href='#nav-" + x.CURRENCY.ConvertToString() + "' role='tab' aria-controls='nav-" + x.CURRENCY.ConvertToString() + "' aria-selected='true'>" + x.CURRENCY.ConvertToString() + "</a> ");
                        });
                    strHtml.AppendLine("         </div>");
                    strHtml.AppendLine("        </nav>");


                    strHtml.AppendLine("        <div class='tab-content' id='nav-tabContent'>");
                    strTemp = string.Empty;
                    dataCurrency
                        .OrderBy(x => x.SORT)
                        .ThenBy(x => x.CURRENCY)
                        .ToList()
                        .ForEach(x =>
                        {
                            strTemp = strTemp == string.Empty ? "tab-pane fade show active" : "tab-pane fade";
                            strHtml.AppendLine("    <div class='" + strTemp + "' id='nav-" + x.CURRENCY.ConvertToString() + "' role='tabpanel' aria-labelledby='nav-" + x.CURRENCY.ConvertToString() + "-tab'> ");
                            strHtml.AppendLine(this.GetContractData(data.Where(y => y.CURRENCY == x.CURRENCY).ToList()));
                            strHtml.AppendLine("    </div>");
                        });

                    strHtml.AppendLine("        </div>");


                    strHtml.AppendLine("      </div>");
                    strHtml.AppendLine("    </div>");
                    strHtml.AppendLine("  </div>");
                    strHtml.AppendLine("</section>");

                    #endregion
                }
            }
            else
            {
                strHtml.AppendLine("<h3 style='font-size:23px;'>查無到期合約</h3>");
            }
            this.divContractData.InnerHtml = strHtml.ToString();

        }

        private string GetContractData(List<BL_ORDER> data)
        {
            var strHtml = new StringBuilder();

            strHtml.AppendLine("<table class='table rwdtable' id='tb1'> ");
            strHtml.AppendLine(" <thead class='thead-light'> ");
            strHtml.AppendLine("  <tr><td data-label='總計' class='td-Numeric' colspan='11'><span style='color:GRAY;'>* 系統呈現資產餘額與對帳單餘額若有小數位尾差, 請以對帳單為準</span></td> </tr>");
            strHtml.AppendLine("  <tr>");
            strHtml.AppendLine("   <th class='td-Text'>合約編號</th> ");
            strHtml.AppendLine("   <th class='td-Text'>幣別</th> ");
            strHtml.AppendLine("   <th class='td-Numeric'>本金(A)</th> ");
            strHtml.AppendLine("   <th class='td-Numeric'>累積紅利</th> ");
            strHtml.AppendLine("   <th class='td-Numeric'>當月紅利</th> ");
            strHtml.AppendLine("   <th class='td-Numeric'>可提領紅利(B)</th> ");
            strHtml.AppendLine("   <th class='td-Numeric'>餘額(A+B)</th> ");
            strHtml.AppendLine("   <th class='td-Text'>合約起始日</th> ");
            strHtml.AppendLine("   <th class='td-Text'>合約結束日</th> ");
            strHtml.AppendLine("   <th class='th-Header'>合約展延日</th> ");
            strHtml.AppendLine("   <th class='th-Header'>年期</th> ");
            strHtml.AppendLine("  </tr>");
            strHtml.AppendLine(" </thead>");
            strHtml.AppendLine(" <tbody>");

            decimal decTotalAmountA = 0;
            decimal decTotalAmountB = 0;
            decimal decTotalAmountAB = 0;
            decimal decTotalACCUMULATE_INTEREST = 0;
            decimal decTotalMONTHLY_INTEREST = 0;

            data
                .OrderBy(x => x.FROM_DATE)
                .ToList()
                .ForEach(x =>
                {
                    var strOrderNo = x.ORDER_NO.ConvertToString();
                    var strContractAmount = x.AmountByCurrency == null ? "" : x.AmountByCurrency.Value.ToString("#,0.0");
                    var strBalance = x.BALANCE == null ? "0" : x.BALANCE.Value.ToString("#,0.0");
                    //  可提領紅利 = 餘額 - 本金
                    var strAmountB = (x.BALANCE == null ? 0 : x.BALANCE.Value) - (x.AmountByCurrency == null ? 0 : x.AmountByCurrency.Value);
                    if (strAmountB <= 0)
                    {
                        strAmountB = 0;
                    }

                    decTotalAmountA += x.AmountByCurrency == null ? 0 : x.AmountByCurrency.Value;
                    decTotalAmountB += strAmountB;
                    decTotalAmountAB += x.BALANCE == null ? 0 : x.BALANCE.Value;
                    decTotalACCUMULATE_INTEREST += x.ACCUMULATE_INTEREST == null ? 0 : x.ACCUMULATE_INTEREST.Value;
                    decTotalMONTHLY_INTEREST += x.MONTHLY_INTEREST == null ? 0 : x.MONTHLY_INTEREST.Value;

                    strHtml.AppendLine("  <tr>");

                    strHtml.AppendLine("   <td data-label='合約編號' class='td-Text'>" + strOrderNo + "</td> ");
                    var strAmount = strContractAmount;

                    strHtml.AppendLine("   <td data-label='幣別' class='td-Text'>" + x.AmountCurrency.ConvertToString() + "</td> ");
                    strHtml.AppendLine("   <td data-label='本金(A)' class='td-Numeric'>" + strAmount + "</td> ");
                    strHtml.AppendLine("   <td data-label='累積紅利' class='td-Numeric'>" + (x.ACCUMULATE_INTEREST == null ? "" : x.ACCUMULATE_INTEREST.Value.ToString("#,0.0")) + "</td> ");
                    strHtml.AppendLine("   <td data-label='當月紅利' class='td-Numeric'>" + (x.MONTHLY_INTEREST == null ? "" : x.MONTHLY_INTEREST.Value.ToString("#,0.0")) + "</td> ");
                    strHtml.AppendLine("   <td data-label='可提領紅利(B)' class='td-Numeric'>" + strAmountB.ToString("#,0.0") + "</td> ");
                    strHtml.AppendLine("   <td data-label='餘額(A+B)' class='td-Numeric'>" + strBalance + "</td> ");
                    strHtml.AppendLine("   <td data-label='合約起始日' class='td-Text'>" + (x.FROM_DATE == null ? "" : x.FROM_DATE.Value.ToString("yyyy/MM/dd")) + "</td> ");
                    strHtml.AppendLine("   <td data-label='合約結束日' class='td-Text'>" + (x.END_DATE == null ? "" : x.END_DATE.Value.ToString("yyyy/MM/dd")) + "</td> ");
                    strHtml.AppendLine("   <td data-label='合約展延日' class='th-Header'>" + (x.EXTEND_DATE == null ? "無" : x.EXTEND_DATE.Value.ToString("yyyy/MM/dd")) + "</td> ");
                    strHtml.AppendLine("   <td data-label='年期' class='th-Header'>" + (x.YEAR == null ? "" : x.YEAR.Value.ConvertToString()) + "</td> ");


                    strHtml.AppendLine("  </tr>");
                });

            if (data.Count > 0)
            {

                strHtml.AppendLine("  <tr>");
                strHtml.AppendLine("   <td data-label='總計' class='td-Text' colspan='2'>總計</td>");
                strHtml.AppendLine("   <td data-label='本金(A)' class='td-Numeric'>" + decTotalAmountA.ToString("#,0.0") + "</td> ");
                strHtml.AppendLine("   <td data-label='累積紅利' class='td-Numeric'>" + decTotalACCUMULATE_INTEREST.ToString("#,0.0") + "</td> ");
                strHtml.AppendLine("   <td data-label='每月紅利' class='td-Numeric'>" + decTotalMONTHLY_INTEREST.ToString("#,0.0") + "</td> ");
                strHtml.AppendLine("   <td data-label='可提領紅利(B)' class='td-Numeric'>" + decTotalAmountB.ToString("#,0.0") + "</td> ");
                strHtml.AppendLine("   <td data-label='餘額(A+B)' class='td-Numeric'>" + decTotalAmountAB.ToString("#,0.0") + "</td> ");
                strHtml.AppendLine("   <td data-label='' class='td-Numeric' colspan='4'></td> ");
                strHtml.AppendLine("  </tr>");
            }

            strHtml.AppendLine(" </tbody>");
            strHtml.AppendLine("</table>");

            return strHtml.ConvertToString();
        }

        protected void btnShowDetail_Click(object sender, EventArgs e)
        {
            var strOrderNo = this.hidOrderNo.Value;
            this.Page.Response.Redirect("~/WebPage/ContractOrderDetail.aspx?No=" + strOrderNo);

        }

    }
}