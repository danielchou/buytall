﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Theme/Theme.Master" AutoEventWireup="true" CodeBehind="Chart.aspx.cs" Inherits="AgentCustomer.WebPage.Chart" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src="https://code.highcharts.com/highcharts.js"></script>
  <%--  <script src="https://code.highcharts.com/highcharts-3d.js"></script>--%>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>

    <style>
        .highcharts-exporting-group {
            display: none !important;
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    
    <div class="section light-bg" id="gallery">
        <div class="container">

            <div class="section-title">
                <%--<small>合約帳戶</small>--%>
                <h3>您的合約分析</h3>
            </div>
            
            
            <div id="chartContainer" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto">

            </div>
            
            <div style="display:block;height:35px;"></div>

            <div id="chartINTERESTContainer" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto; display:none;">

            </div>

            <div id="chartINTERESTContainerUSD" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto">

            </div>
            <br />
            <div id="chartINTERESTContainerNTD" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto">

            </div>
          <br />
            <div id="chartINTERESTContainerRMB" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto">

            </div>

               <br />
            <div id="chartINTERESTContainerAUD" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto">

            </div>

                 <br />
            <div id="chartINTERESTContainerEUR" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto">

            </div>
            
                 <br />
            <div id="chartINTERESTContainerJPY" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto">

            </div>
                 <br />
            <div id="chartINTERESTContainerNZD" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto">

            </div>

        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="server">

    <script>

        Highcharts.chart('chartContainer', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false,
                exporting: { enabled: false }
            },
            title: {
                text: '合約年期分佈',
                align: 'center',
                verticalAlign: 'middle',
                y: 40
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    dataLabels: {
                        enabled: true,
                        distance: -50,
                        style: {
                            fontWeight: 'bold',
                            color: 'white'
                        },
                        formatter : function() {
                            return '<b>' + this.point.name+'</b>' + Highcharts.numberFormat(this.percentage,2) + '%' ; 
                            //Highcharts.numberFormat(this.percentage, 2)
                        },
                        //format:"{series.data.x}  : {point.percentage:.1f}%"
                       // format:'{series.data.x}: {point.percentage:.1f}%</b>'
                    },
                    colors: ['#f24a7a', '#7a5cf5', '#63f5c4','#f24a7a'],
                    startAngle: -90,
                    endAngle: 90,
                    center: ['50%', '75%'],
                    size: '110%'
                }
            },
            series: [{
                type: 'pie',
                name: '合約年期分佈',
                
                data: <%=this.strChartData%>,
                innerSize: '50%'
            }]
        });


        if ('<%=this.IsInterestYear_USD%>' == 'Y' ) {
    
            Highcharts.chart('chartINTERESTContainerUSD', {
                chart: {
                    type: 'column',
                    exporting: { enabled: false },
                    options3d: {
                        enabled: true,
                        alpha: 10,
                        beta: 25,
                        depth: 70
                    }
                },
                title: {
                    text: '年度紅利累積分布圖 (USD)'
                },
                subtitle: {
                    text: '(含歷史合約派利)'
                },
                plotOptions: {
                    column: {
                        depth: 25
                    }
                },
                xAxis: {
                    categories: <%=this.strCharDate_InterestYear_USD%>,
                    labels: {
                        skew3d: true,
                        style: {
                            fontSize: '16px'
                        }
                    }
                },
                yAxis: {
                    title: {
                        text: null
                    }
                },
                series: [{
                    data: <%=this.strCharDate_InterestData_USD%>,
                    name: '紅利'
                }]
            });
        }else{
            $("#chartINTERESTContainerUSD").css("display","none");
        }
        
        if ('<%=this.IsInterestYear_NTD%>' == 'Y' ) {
    
            Highcharts.chart('chartINTERESTContainerNTD', {
                chart: {
                    type: 'column',
                    exporting: { enabled: false },
                    options3d: {
                        enabled: true,
                        alpha: 10,
                        beta: 25,
                        depth: 70
                    }
                },
                title: {
                    text: '年度紅利累積分布圖 (NTD)'
                },
                subtitle: {
                    text: '(含歷史合約派利)'
                },
                plotOptions: {
                    column: {
                        depth: 25
                    }
                },
                xAxis: {
                    categories: <%=this.strCharDate_InterestYear_NTD%>,
                    labels: {
                        skew3d: true,
                        style: {
                            fontSize: '16px'
                        }
                    }
                },
                yAxis: {
                    title: {
                        text: null
                    }
                },
                series: [{
                    data: <%=this.strCharDate_InterestData_NTD%>,
                    name: '紅利'
                }]
            });
        }else{
            $("#chartINTERESTContainerNTD").css("display","none");
        }
        
        if ('<%=this.IsInterestYear_RMB%>' == 'Y' ) {
    
            Highcharts.chart('chartINTERESTContainerRMB', {
                chart: {
                    type: 'column',
                    exporting: { enabled: false },
                    options3d: {
                        enabled: true,
                        alpha: 10,
                        beta: 25,
                        depth: 70
                    }
                },
                title: {
                    text: '年度紅利累積分布圖 (RMB)'
                },
                subtitle: {
                    text: '(含歷史合約派利)'
                },
                plotOptions: {
                    column: {
                        depth: 25
                    }
                },
                xAxis: {
                    categories: <%=this.strCharDate_InterestYear_RMB%>,
                    labels: {
                        skew3d: true,
                        style: {
                            fontSize: '16px'
                        }
                    }
                },
                yAxis: {
                    title: {
                        text: null
                    }
                },
                series: [{
                    data: <%=this.strCharDate_InterestData_RMB%>,
                    name: '紅利'
                }]
            });
        }else{
            $("#chartINTERESTContainerRMB").css("display","none");
        }



            if ('<%=this.IsInterestYear_AUD%>' == 'Y' ) {
    
            Highcharts.chart('chartINTERESTContainerAUD', {
                chart: {
                    type: 'column',
                    exporting: { enabled: false },
                    options3d: {
                        enabled: true,
                        alpha: 10,
                        beta: 25,
                        depth: 70
                    }
                },
                title: {
                    text: '年度紅利累積分布圖 (AUD)'
                },
                subtitle: {
                    text: '(含歷史合約派利)'
                },
                plotOptions: {
                    column: {
                        depth: 25
                    }
                },
                xAxis: {
                    categories: <%=this.strCharDate_InterestYear_AUD%>,
                    labels: {
                        skew3d: true,
                        style: {
                            fontSize: '16px'
                        }
                    }
                },
                yAxis: {
                    title: {
                        text: null
                    }
                },
                series: [{
                    data: <%=this.strCharDate_InterestData_AUD%>,
                    name: '紅利'
                }]
            });
        }else{
            $("#chartINTERESTContainerAUD").css("display","none");
        }
        <%--        Highcharts.chart('chartINTERESTContainer', {
            chart: {
                type: 'column',
                options3d: {
                    enabled: true,
                    alpha: 10,
                    beta: 25,
                    depth: 70
                }
            },
            title: {
                text: '年度紅利累積分布圖'
            },
            subtitle: {
                text: '(含歷史合約派利)'
            },
            plotOptions: {
                column: {
                    depth: 25
                }
            },
            xAxis: {
                categories: <%=this.strCharDate_InterestYear%>,
                labels: {
                    skew3d: true,
                    style: {
                        fontSize: '16px'
                    }
                }
            },
            yAxis: {
                title: {
                    text: null
                }
            },
            series: [{
                data: <%=this.strCharDate_InterestData%>,
                name: '紅利'
            }]
        });--%>


        
            if ('<%=this.IsInterestYear_JPY%>' == 'Y' ) {
    
            Highcharts.chart('chartINTERESTContainerJPY', {
                chart: {
                    type: 'column',
                    exporting: { enabled: false },
                    options3d: {
                        enabled: true,
                        alpha: 10,
                        beta: 25,
                        depth: 70
                    }
                },
                title: {
                    text: '年度紅利累積分布圖 (JPY)'
                },
                subtitle: {
                    text: '(含歷史合約派利)'
                },
                plotOptions: {
                    column: {
                        depth: 25
                    }
                },
                xAxis: {
                    categories: <%=this.strCharDate_InterestYear_JPY%>,
                    labels: {
                        skew3d: true,
                        style: {
                            fontSize: '16px'
                        }
                    }
                },
                yAxis: {
                    title: {
                        text: null
                    }
                },
                series: [{
                    data: <%=this.strCharDate_InterestData_JPY%>,
                    name: '紅利'
                }]
            });
        }else{
            $("#chartINTERESTContainerJPY").css("display","none");
            }


        
        
            if ('<%=this.IsInterestYear_NZD%>' == 'Y' ) {
    
            Highcharts.chart('chartINTERESTContainerNZD', {
                chart: {
                    type: 'column',
                    exporting: { enabled: false },
                    options3d: {
                        enabled: true,
                        alpha: 10,
                        beta: 25,
                        depth: 70
                    }
                },
                title: {
                    text: '年度紅利累積分布圖 (NZD)'
                },
                subtitle: {
                    text: '(含歷史合約派利)'
                },
                plotOptions: {
                    column: {
                        depth: 25
                    }
                },
                xAxis: {
                    categories: <%=this.strCharDate_InterestYear_NZD%>,
                    labels: {
                        skew3d: true,
                        style: {
                            fontSize: '16px'
                        }
                    }
                },
                yAxis: {
                    title: {
                        text: null
                    }
                },
                series: [{
                    data: <%=this.strCharDate_InterestData_NZD%>,
                    name: '紅利'
                }]
            });
        }else{
            $("#chartINTERESTContainerNZD").css("display","none");
            }


    </script>
</asp:Content>
