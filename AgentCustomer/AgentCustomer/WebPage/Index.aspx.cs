﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgentCustomer.Logic;
using AgentCustomer.Logic.BusinessLogic;
using AgentCustomer.Utility;
using AgentCustomer.Model;
using AgentCustomer.Model.Table;
using System.Text;
using System.Web.Script.Serialization;

namespace AgentCustomer.WebPage
{
    public partial class Index : System.Web.UI.Page
    {
        private BL_CUST GetCurrentUser
        {
            get
            {
                BL_CUST _user = new BL_CUST();

                if (this.Session["_User"] != null)
                {
                    if ((this.Session["_User"] as BL_CUST) == null)
                    {
                        this.SessionOut();
                    }
                    else
                    {
                        _user = this.Session["_user"] as BL_CUST;
                    }
                }
                else
                {
                    this.SessionOut();
                }


                return _user;
            }
        }

        private void SessionOut()
        {
            //var strScript = "<script> alert('連線逾時') </script>";
            //this.Page.RegisterClientScriptBlock("Alert", strScript);
            Response.Redirect("~/Customer_Login.aspx");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Page.IsPostBack==false)
            {
                this.BindCustBaseData();

            }

        }

        private void BindCustBaseData()
        {
            var dataUser = this.GetCurrentUser;
            if (dataUser != null)
            {
                var dataBank = new LogicCustomer().GetBankDataByCustID(dataUser.CUST_ID == null ? "" : dataUser.CUST_ID.Value.ConvertToString());

                this.lblContractCount.Text = dataUser.ORDER_CNT == null ? "0" : dataUser.ORDER_CNT.Value.ConvertToString();
                this.lblTotalBalance.Text = dataUser.ORDER_BALANCE == null ? "$0" : dataUser.ORDER_BALANCE.Value.ToString("$#,#");

                #region 基本資料

                this.txtCUST_CNAME.Text = dataUser.CUST_CNAME.ConvertToString();
                this.txtCUST_ENAME.Text = dataUser.CUST_ENAME.ConvertToString();
                if (dataUser.SEX.ConvertToString() == "M")
                {
                    this.txtSex.Text = "男士";
                }
                else if (dataUser.SEX.ConvertToString() == "F")
                {
                    this.txtSex.Text = "女士";
                }
                
                if (dataUser.DATE_OF_BIRTH != null)
                {
                    this.txtBirthday.Text = dataUser.DATE_OF_BIRTH.Value.ToString("yyyy/MM/dd");
                }
                
                this.txtID_NUMBER.Text = dataUser.ID_NUMBER.ConvertToString();
                this.txtPASSPORT.Text = dataUser.PASSPORT.ConvertToString();
                this.txtNATION.Text = dataUser.PASSPORT_REGION.ConvertToString();
                this.txtPHONE.Text = dataUser.PHONE_1.ConvertToString();
                this.txtEMAIL.Text = dataUser.EMAIL_1.ConvertToString();
                this.txtAddress.Text = dataUser.C_ADDRESS.ConvertToString();

                #endregion

                #region 銀行資料
                //if (dataBank.Count > 0)
                //{
                //    var dataBankSingle = dataBank[0];
                //    this.txtBANK_CNAME.Text = dataBankSingle.BANK_CNAME.ConvertToString();
                //    this.txtBRANCH_CNAME.Text = dataBankSingle.BRANCH_CNAME.ConvertToString();
                //    this.txtACCOUNT_CNAME.Text = dataBankSingle.ACCOUNT_CNAME.ConvertToString();
                //    this.txtACCOUNT.Text = dataBankSingle.ACCOUNT.ConvertToString();
                //    this.txtBANK_C_ADDRESS.Text = dataBankSingle.BANK_C_ADDRESS.ConvertToString();
                //    this.txtSWIFT_CODE.Text = dataBankSingle.SWIFT_CODE.ConvertToString();
                //}
                this.rpData.DataSource = dataBank;
                this.rpData.DataBind();
                #endregion

                this.BindContractData(dataUser.CUST_ID == null ? "" : dataUser.CUST_ID.Value.ConvertToString());
            }
        }


        private void BindContractData(string CustID)
        {
            var data = new LogicCustomer().GetContractDataByCustID(CustID);
            var dataOrderList = new LogicCustomer().GetOrderDataByORDER_NO
                (
                    data.Select(x => x.ORDER_NO.ConvertToString()).ToList()
                );
            var strHtml = new StringBuilder();
            strHtml.AppendLine("<table class='table rwdtable'> ");
            strHtml.AppendLine(" <thead class='thead-light'> ");
            strHtml.AppendLine("  <tr>");
            strHtml.AppendLine("   <th>合約編號</th> ");
            strHtml.AppendLine("   <th>入金金額</th> ");
            //strHtml.AppendLine("   <th>幣別</th> ");
            strHtml.AppendLine("   <th>累積紅利</th> ");
            strHtml.AppendLine("   <th>每月紅利</th> ");
            strHtml.AppendLine("   <th>合約日期</th> ");
            strHtml.AppendLine("   <th>合約結束日期</th> ");
            strHtml.AppendLine("   <th>到期剩餘天數</th> ");
            strHtml.AppendLine("   <th>狀態</th> ");
            strHtml.AppendLine("   <th>紅利提領</th> ");
            strHtml.AppendLine("  </tr>");
            strHtml.AppendLine(" </thead>");
            strHtml.AppendLine(" <tbody>");

            data.ForEach(x =>
            {
                var dataOrder = dataOrderList.Where(y => y.ORDER_NO == x.ORDER_NO).ToList();
                var strOrderNo = x.ORDER_NO.ConvertToString();
                var strContractAmount = x.AmountByCurrency == null ? "" : x.AmountByCurrency.Value.ConvertToString();
                var strBalance = x.BALANCE == null ? "0" : x.BALANCE.Value.ConvertToString();

                strHtml.AppendLine("  <tr>");

                if (dataOrder.Count > 0)
                {
                    var jsonSerialiser = new JavaScriptSerializer();
                    var json = jsonSerialiser.Serialize(dataOrder);
                    strHtml.AppendLine("   <td data-label='合約編號'><a href=\"\" onclick='openOrderDateil(" + json + ");return false;'>" + strOrderNo + "</a></td> ");
                }
                else
                {
                    strHtml.AppendLine("   <td data-label='合約編號'>" + strOrderNo + "</td> ");
                }
                var strAmount = strContractAmount;
                if (strAmount != string.Empty && x.AmountCurrency.ConvertToString().Trim() != string.Empty)
                {
                    strAmount += " (" + x.AmountCurrency.ConvertToString().Trim() + ")";
                }
                strHtml.AppendLine("   <td data-label='入金金額'>" + strAmount + "</td> ");
                //strHtml.AppendLine("   <td data-label='幣別'>" + x.AmountByCurrency.ConvertToString() + "</td> ");
                strHtml.AppendLine("   <td data-label='累積紅利'>" + (x.ACCUMULATE_INTEREST==null?"":x.ACCUMULATE_INTEREST.Value.ToString()) + "</td> ");
                strHtml.AppendLine("   <td data-label='每月紅利'>" + (x.MONTHLY_INTEREST == null ? "" : x.MONTHLY_INTEREST.Value.ToString()) + "</td> ");
                strHtml.AppendLine("   <td data-label='合約日期'>" + (x.FROM_DATE == null ? "" : x.FROM_DATE.Value.ToString("yyyy/MM/dd")) + "</td> ");
                strHtml.AppendLine("   <td data-label='合約結束日期'>" + (x.END_DATE == null ? "" : x.END_DATE.Value.ToString("yyyy/MM/dd")) + "</td> ");
                strHtml.AppendLine("   <td data-label='到期剩餘天數'>" + (x.DUE_DAYS.ConvertToString()) + "</td> ");
                strHtml.AppendLine("   <td data-label='狀態'>" + x.GetSTATUS.ConvertToString() + "</td> ");

                //  合約編號/本金/餘額
                var strDepositOut = "<a href=\"\" onclick=\"openDepositOut('"+ strOrderNo + "', '"+ strContractAmount + "', '"+ strBalance + "'); return false;\">提領</a>";
                strHtml.AppendLine("   <td data-label='紅利提領'>" + strDepositOut + "</td> ");

                strHtml.AppendLine("  </tr>");
            });
         
            strHtml.AppendLine(" </tbody>");
            strHtml.AppendLine("</table>");

            this.divContractData.InnerHtml = strHtml.ToString();

        }
    }
}