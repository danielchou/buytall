﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgentCustomer.Logic;
using AgentCustomer.Logic.BusinessLogic;
using AgentCustomer.Utility;
using AgentCustomer.Model;
using AgentCustomer.Model.Table;
using System.Text;
using System.Web.Script.Serialization;
using System.IO;
using System.Net;
using System.Net.Mail;

namespace AgentCustomer.WebPage
{
    public partial class ApplyDeposit : System.Web.UI.Page
    {
        private BL_CUST GetCurrentUser
        {
            get
            {
                BL_CUST _user = new BL_CUST();

                if (this.Session["_User"] != null)
                {
                    if ((this.Session["_User"] as BL_CUST) == null)
                    {
                        this.SessionOut();
                    }
                    else
                    {
                        _user = this.Session["_user"] as BL_CUST;
                    }
                }
                else
                {
                    this.SessionOut();
                }


                return _user;
            }
        }

        private string GetCurrency
        {
            get
            {
                return this.ViewState["_GetCurrency"].ConvertToString();
            }
            set
            {
                this.ViewState["_GetCurrency"] = value;
            }
        }

        private void SessionOut()
        {
            //var strScript = "<script> alert('連線逾時') </script>";
            //this.Page.RegisterClientScriptBlock("Alert", strScript);
            Response.Redirect("~/Customer_Login.aspx");
        }

        public string strIsFromSales
        {
            get
            {
                return this.GetCurrentUser.IS_SALES_LOGIN.ConvertToString().Trim();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Page.IsPostBack == false)
            {
                //  2019/10/28 > 如果是業務Login就無條件開啟
                if (this.GetCurrentUser.IS_SALES_LOGIN.ConvertToString().Trim() != "Y")
                {
                    switch (this.GetCurrentUser.IS_ONLINE_WITHDRAWAL.ConvertToString().ToUpper())
                    {
                        case "P":   //  Phone
                        case "E":   //  Email
                                    //  Nothing
                            break;
                        default:
                            this.Page.Response.Redirect("~/WebPage/ContractInfo.aspx");
                            return;
                            break;
                    }
                }
                else
                {
                    this.divPhoneValid.Attributes["style"] = "display:none;";
                }
                this.Session["_ValidCode"] = string.Empty;
                this.Session["_ValidTime"] = string.Empty;
                

                this.BindData();
            }
        }

        private void BindData()
        {
            var dataUser = this.GetCurrentUser;
            if (dataUser != null)
            {
                var strCustID = dataUser.CUST_ID == null ? "" : dataUser.CUST_ID.Value.ConvertToString();

                var data = new LogicCustomer().GetContractDataByCustID(strCustID, true);
                data = data.Where(x => (x.CanApplyAmount == null ? 0 : x.CanApplyAmount.Value) > 0).ToList();

                if (this.Page.IsPostBack == false)
                {
                    if (data.Select(x => x.AmountCurrency).Distinct().Count() > 0)
                    {
                        this.linkTab_USD.Visible = data.Where(x => x.AmountCurrency.ConvertToString() == "USD").Count() > 0;
                        this.linkTab_RMB.Visible = data.Where(x => x.AmountCurrency.ConvertToString() == "RMB").Count() > 0;
                        this.linkTab_NTD.Visible = data.Where(x => x.AmountCurrency.ConvertToString() == "NTD").Count() > 0;
                        this.linkTab_EUR.Visible = data.Where(x => x.AmountCurrency.ConvertToString() == "EUR").Count() > 0;
                        this.linkTab_AUD.Visible = data.Where(x => x.AmountCurrency.ConvertToString() == "AUD").Count() > 0;
                        this.linkTab_JPY.Visible = data.Where(x => x.AmountCurrency.ConvertToString() == "JPY").Count() > 0;
                        this.linkTab_NZD.Visible = data.Where(x => x.AmountCurrency.ConvertToString() == "NZD").Count() > 0;
                        if (this.linkTab_USD.Visible)
                            this.GetCurrency = "USD";
                        else if (this.linkTab_NTD.Visible)
                            this.GetCurrency = "NTD";
                        else if (this.linkTab_RMB.Visible)
                            this.GetCurrency = "RMB";
                        else if (this.linkTab_EUR.Visible)
                            this.GetCurrency = "EUR";
                        else if (this.linkTab_AUD.Visible)
                            this.GetCurrency = "AUD";
                        else if (this.linkTab_JPY.Visible)
                            this.GetCurrency = "JPY";
                        else if (this.linkTab_NZD.Visible)
                            this.GetCurrency = "NZD";
                    }
                    else
                    {
                        this.linkTab_USD.Visible = false;
                        this.linkTab_RMB.Visible = false;
                        this.linkTab_NTD.Visible = false;
                        this.linkTab_EUR.Visible = false;
                        this.linkTab_AUD.Visible = false;
                        this.linkTab_JPY.Visible = false;
                        this.linkTab_NZD.Visible = false;
                        if (data.Count() > 0)
                        {
                            this.GetCurrency = data[0].AmountCurrency.ConvertToString();
                        }
                    }
                }

                data = data.Where(x => x.AmountCurrency.ConvertToString() == this.GetCurrency).ToList();

                this.linkTab_USD.CssClass = "nav-item nav-link";
                this.linkTab_NTD.CssClass = "nav-item nav-link";
                this.linkTab_RMB.CssClass = "nav-item nav-link";
                this.linkTab_EUR.CssClass = "nav-item nav-link";
                this.linkTab_AUD.CssClass = "nav-item nav-link";
                this.linkTab_JPY.CssClass = "nav-item nav-link";
                this.linkTab_NZD.CssClass = "nav-item nav-link";
                switch (this.GetCurrency)
                {
                    case "USD":
                        this.linkTab_USD.CssClass = "nav-item nav-link active";
                        break;
                    case "NTD":
                        this.linkTab_NTD.CssClass = "nav-item nav-link active";
                        break;
                    case "RMB":
                        this.linkTab_RMB.CssClass = "nav-item nav-link active";
                        break;
                    case "EUR":
                        this.linkTab_EUR.CssClass = "nav-item nav-link active";
                        break;
                    case "AUD":
                        this.linkTab_AUD.CssClass = "nav-item nav-link active";
                        break;
                    case "JPY":
                        this.linkTab_JPY.CssClass = "nav-item nav-link active";
                        break;
                    case "NZD":
                        this.linkTab_NZD.CssClass = "nav-item nav-link active";
                        break;
                    default:
                        break;
                }

                this.GridView1.DataSource = data;
                this.GridView1.DataBind();


                var strOut = string.Empty;
                decimal decOut = 0;
                var dicCurrency = new Dictionary<string, string>();
                var dicAmount = new Dictionary<string, decimal>();
                var dicBalance = new Dictionary<string, decimal>();
                var dicCanApplyAmount = new Dictionary<string, decimal>();
                var dicApplyOnPath = new Dictionary<string, decimal>();


                data.Select(x => new
                {
                    x.AmountCurrency
                })
                .Distinct()
                .ToList()
                .ForEach(x =>
                {
                    dicCurrency.Add(x.AmountCurrency, x.AmountCurrency);
                    dicAmount.Add(x.AmountCurrency, data.Where(y => y.AmountCurrency == x.AmountCurrency).Sum(y => (y.AmountByCurrency == null ? 0 : y.AmountByCurrency.Value)));
                    dicBalance.Add(x.AmountCurrency, data.Where(y => y.AmountCurrency == x.AmountCurrency).Sum(y => (y.BALANCE == null ? 0 : y.BALANCE.Value)));
                    dicCanApplyAmount.Add(x.AmountCurrency, data.Where(y => y.AmountCurrency == x.AmountCurrency).Sum(y => (y.CanApplyAmount == null ? 0 : y.CanApplyAmount.Value)));
                    dicApplyOnPath.Add(x.AmountCurrency, data.Where(y => y.AmountCurrency == x.AmountCurrency).Sum(y => y.ApplyOnPathAmount == null ? 0 : y.ApplyOnPathAmount.Value));
                });

                var strCurrency_total = string.Empty;
                var strAmount_total = string.Empty;
                var strBalance_total = string.Empty;
                var strCanApplyAmount_total = string.Empty;
                var strApplyAmount_total = string.Empty;
                var strApplyOnPath_total = string.Empty;
                foreach (var item in dicCurrency.Values)
                {
                    strCurrency_total += strCurrency_total == string.Empty ? "" : " / ";
                    strAmount_total += strAmount_total == string.Empty ? "" : " / ";
                    strBalance_total += strBalance_total == string.Empty ? "" : " / ";
                    strCanApplyAmount_total += strCanApplyAmount_total == string.Empty ? "" : " / ";
                    strApplyAmount_total += strApplyAmount_total == string.Empty ? "" : " / ";
                    strApplyOnPath_total += strApplyOnPath_total == string.Empty ? "" : " / ";

                    strCurrency_total += item;
                    strAmount_total += dicAmount[item].ToString("#,0.0");
                    strBalance_total += dicBalance[item].ToString("#,0.0");
                    strCanApplyAmount_total += dicCanApplyAmount[item].ToString("#,0.0");
                    strApplyAmount_total += "0";
                    strApplyOnPath_total += dicApplyOnPath[item].ToString("#,0.0");
                }

                ((Label)this.GridView1.FooterRow.FindControl("lbl_Amount_Currency_Total")).Text = strCurrency_total;
                ((Label)this.GridView1.FooterRow.FindControl("lbl_Amount_By_Currency_Total")).Text = strAmount_total;
                ((Label)this.GridView1.FooterRow.FindControl("lbl_BALANCE_Total")).Text = strBalance_total;
                ((Label)this.GridView1.FooterRow.FindControl("lbl_CanApply_Amount_Total")).Text = strCanApplyAmount_total;
                ((Label)this.GridView1.FooterRow.FindControl("lbl_Apply_Amount_Total")).Text = strApplyAmount_total;
            }
        }

        protected void linkBack_Click(object sender, EventArgs e)
        {
            this.Page.Response.Redirect("~/WebPage/ContractInfo.aspx");
        }
        
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            switch (e.Row.RowType)
            {
                case DataControlRowType.DataRow:
                case DataControlRowType.Separator:
                    decimal decCanApplyAmount = 0;
                    decimal decApplyOnPath = 0;
                    decimal decTry;

                    var txtApplyAmount = (TextBox)e.Row.FindControl("txtApplyAmount");
                    var lblCanApplyAmount = (Label)e.Row.FindControl("lblCanApplyAmount");
                    var hypBringAmount = (HyperLink)e.Row.FindControl("hypBringAmount");
                    var lblApplyOnPaAmount = (Label)e.Row.FindControl("lblApplyOnPaAmount");
                    var chkSelect = (CheckBox)e.Row.FindControl("chkSelect");

                    if (decimal.TryParse(lblCanApplyAmount.Text.Replace(",", ""), out decTry))
                    {
                        decCanApplyAmount = decTry;
                    }
                    if (decimal.TryParse(lblApplyOnPaAmount.Text.Replace(",", ""), out decTry))
                    {
                        decApplyOnPath = decTry;
                    }

                    txtApplyAmount.Attributes["max"] = lblCanApplyAmount.Text.Replace(",", "").ConvertToString();
                    if (hypBringAmount.Attributes["onclick"] == null)
                    {
                        hypBringAmount.Attributes.Add("onclick", "");
                    }
                    if (decCanApplyAmount - decApplyOnPath > 0)
                    {
                        hypBringAmount.Attributes["onclick"] = "$('#" + txtApplyAmount.ClientID + "').val(" + lblCanApplyAmount.Text.Replace(",", "") + " - " + lblApplyOnPaAmount.Text.Replace(",", "") + "); changeAmount(document.getElementById('" + txtApplyAmount.ClientID + "')); return false;";
                        chkSelect.Enabled = true;
                        hypBringAmount.Enabled = true;
                        txtApplyAmount.Enabled = true;
                    }
                    else
                    {
                        chkSelect.Enabled = false;
                        hypBringAmount.Enabled = false;
                        txtApplyAmount.Enabled = false;
                    }
                    


                    this.SetCellAttr(e.Row, 0, "選取");
                    this.SetCellAttr(e.Row, 1, "合約編號");
                    this.SetCellAttr(e.Row, 2, "幣別");
                    this.SetCellAttr(e.Row, 3, "本金");
                    this.SetCellAttr(e.Row, 4, "餘額");
                    this.SetCellAttr(e.Row, 5, "可提領紅利");
                    this.SetCellAttr(e.Row, 6, "您要提領的金額");
                    
                    break;
                case DataControlRowType.Footer:

                    this.SetCellAttr(e.Row, 0, "選取");
                    this.SetCellAttr(e.Row, 1, "合約編號");
                    this.SetCellAttr(e.Row, 2, "幣別");
                    this.SetCellAttr(e.Row, 3, "本金");
                    this.SetCellAttr(e.Row, 4, "餘額");
                    this.SetCellAttr(e.Row, 5, "可提領紅利");
                    this.SetCellAttr(e.Row, 6, "您要提領的金額");

                    break;
                default:
                    break;
            }
        }

        private void SetCellAttr(GridViewRow row, int cellIndex, string dataLabel)
        {
            if (row.Cells[cellIndex].Attributes["data-label"]==null)
            {
                row.Cells[cellIndex].Attributes.Add("data-label", "");
            }
            row.Cells[cellIndex].Attributes["data-label"] = dataLabel;
        }

        protected void linkApplyConfirm_Click(object sender, EventArgs e)
        {
            var strChk = this.DataValidation();
            if (strChk != string.Empty)
            {
                var strScript = "<script> alert('" + strChk + "') </script>";
                this.Page.RegisterClientScriptBlock("Alert", strScript);
                return;
            }

            var data = this.GridView1.Rows.Cast<GridViewRow>()
                .Where(x => ((CheckBox)x.FindControl("chkSelect")).Checked == true).ToList();
            var strDataList = new StringBuilder();
            Dictionary<string, decimal> dicTotal = new Dictionary<string, decimal>();

            strDataList.AppendLine("<table class='table rwdtable' id='tb1'> ");
            strDataList.AppendLine(" <thead class='thead-light'> ");
            strDataList.AppendLine("  <tr>");
            strDataList.AppendLine("   <th>合約編號</th> ");
            strDataList.AppendLine("   <th>幣別</th> ");
            strDataList.AppendLine("   <th>提領金額</th> ");
            strDataList.AppendLine("  </tr>");
            strDataList.AppendLine(" </thead>");
            strDataList.AppendLine(" <tbody>");

            data.ForEach(x =>
            {
                decimal decTry;
                decimal decApplyAmount = 0;
                var lblAmountCurrency = (Label)x.FindControl("lblAmountCurrency");
                var txtApplyAmount = (TextBox)x.FindControl("txtApplyAmount");
                var lblORDER_NO = (Label)x.FindControl("lblORDER_NO");

                if (decimal.TryParse(txtApplyAmount.Text.Replace(",", ""), out decTry))
                {
                    decApplyAmount = decTry;
                }

                if (dicTotal.TryGetValue(lblAmountCurrency.Text, out decTry))
                {
                    dicTotal[lblAmountCurrency.Text] += decApplyAmount;
                }
                else
                {
                    dicTotal.Add(lblAmountCurrency.Text, decApplyAmount);
                }

                strDataList.AppendLine("  <tr>");
                strDataList.AppendLine("   <td data-label='合約編號'>" + lblORDER_NO.Text + "</td>");
                strDataList.AppendLine("   <td data-label='幣別'>" + lblAmountCurrency.Text + "</td> ");
                strDataList.AppendLine("   <td data-label='提領金額'>" + decApplyAmount.ToString("#,0.0") + "</td> ");
                strDataList.AppendLine("  </tr>");
            });

            strDataList.AppendLine(" </tbody>");
            strDataList.AppendLine("</table>");


            strDataList.AppendLine("<table class='table rwdtable' id='tb2' style='padding-top:15px;'> ");
            strDataList.AppendLine(" <thead class='thead-light'> ");
            strDataList.AppendLine("  <tr><td data-label='總提領金額' colspan='2'></td> </tr>");
            strDataList.AppendLine("  <tr>");
            strDataList.AppendLine("   <th>幣別</th> ");
            strDataList.AppendLine("   <th>提領總金額</th> ");
            strDataList.AppendLine("  </tr>");
            strDataList.AppendLine(" </thead>");
            strDataList.AppendLine(" <tbody>");

            foreach (var item in dicTotal)
            {
                strDataList.AppendLine("  <tr>");
                strDataList.AppendLine("   <td data-label='幣別'>" + item.Key.ConvertToString() + "</td> ");
                strDataList.AppendLine("   <td data-label='提領總金額'>" + item.Value.ToString("#,0.0") + "</td> ");
                strDataList.AppendLine("  </tr>");
            }
            strDataList.AppendLine(" </tbody>");
            strDataList.AppendLine("</table>");


            this.divApplyList.InnerHtml = strDataList.ToString();
            if (this.SendPhoneMsg())
            {
                //  簡訊發送成功才可以開啟填寫資訊
                this.Page.RegisterClientScriptBlock("Alert", "<script> _IsSend = 'Y'; </script>");
            }
            return;
        }

        private string DataValidation()
        {
            var msg = string.Empty;
            var data = this.GridView1.Rows.Cast<GridViewRow>()
                .Where(x => ((CheckBox)x.FindControl("chkSelect")).Checked == true).ToList();

            var strPhone = this.GetCurrentUser.PHONE_1.ConvertToString().Trim().Replace("-", "");
            if (strPhone.Length != 10 || strPhone.Substring(0, 2) != "09")
            {
                msg = "查無手機號碼或手機號碼格式不正確，不可進行網路出金";
            }
            else {
                if (data.Count == 0)
                {
                    msg = "請先選取要提領的合約，並填寫提領金額";
                }
                else
                {
                    data.ForEach(x =>
                    {
                        var lblCanApplyAmount = (Label)x.FindControl("lblCanApplyAmount");
                        var txtApplyAmount = (TextBox)x.FindControl("txtApplyAmount");
                        var lblORDER_NO = (Label)x.FindControl("lblORDER_NO");
                        var lblOnPath = (Label)x.FindControl("lblApplyOnPaAmount");
                        decimal decCanApplyAmount = 0;
                        decimal decApplyAmount = 0;
                        decimal decTry;
                        decimal decOnPath = 0;

                        if (decimal.TryParse(lblCanApplyAmount.Text.Replace(",", ""), out decTry))
                        {
                            decCanApplyAmount = decTry;
                        }

                        if (decimal.TryParse(txtApplyAmount.Text.Replace(",", ""), out decTry))
                        {
                            decApplyAmount = decTry;
                        }

                        if (decimal.TryParse(lblOnPath.Text.Replace(",", ""), out decTry))
                        {
                            decOnPath = decTry;
                        }

                        if (txtApplyAmount.Text.Trim() == string.Empty)
                        {
                            msg += "\n合約編號：" + lblORDER_NO.Text.Trim() + "，請輸入提領金額";
                        }
                        else
                        {
                            if (decApplyAmount > decCanApplyAmount - decOnPath)
                            {
                                msg += "\n合約編號：" + lblORDER_NO.Text.Trim() + "，提領金額不可大於可提領紅利金額";
                            }
                            else if (decApplyAmount <= 0)
                            {
                                msg += "\n合約編號：" + lblORDER_NO.Text.Trim() + "，提領金額請輸入大於0的金額";
                            }
                        }
                    });
                }
            }

            return msg;
        }

        public bool SendPhoneMsg()
        {
            var strMsg = string.Empty;
            var strResult = true;

            var strAccount = GetConfigValueByKey("smAccount");
            var strPWD = GetConfigValueByKey("smPWD");
            var strTestPhone = GetConfigValueByKey("smTestPhone");
            var strTestMail = GetConfigValueByKey("smTestMail");
            bool IsTest = false;
            var strAPI = GetConfigValueByKey("smAPI");
            var strPhone = this.GetCurrentUser.PHONE_1.ConvertToString().Trim().Replace("-", "");
            var strMail = this.GetCurrentUser.EMAIL_1.ConvertToString().Trim();
            var strValidCode = string.Empty;

            Random rd = new Random();
            for (int i = 0; i < 6; i++)
            {
                strValidCode += rd.Next(0, 9).ConvertToString();
            }

            if (strTestPhone != string.Empty)
            {
                //  有設定測試帳號, 就發送給測試帳號
                strPhone = strTestPhone;
                IsTest = true;
            }
            if (strTestMail != string.Empty)
            {
                //  有測試Mail
                strMail = strTestMail;
                IsTest = true;
            }
            //strPhone = "0933845145";  //  Todo > Test
            if (new LogicCustomer().UpdateVERIFICATIONCODE(this.GetCurrentUser.CUST_ID == null ? "0" : this.GetCurrentUser.CUST_ID.Value.ConvertToString(), strValidCode))
            {
                //  2019/10/28 > 如果是業務Login就無條件開啟
                if (this.GetCurrentUser.IS_SALES_LOGIN.ConvertToString().Trim() == "Y")
                {
                    //  Nothing > 業務送單不用卡Phone/EMail
                    this.Session["_ValidCode"] = string.Empty;
                    this.Session["_ValidTime"] = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                }
                else
                {
                    switch (this.GetCurrentUser.IS_ONLINE_WITHDRAWAL.ConvertToString().ToUpper())
                    {
                        case "P":   //  Phone
                                    //  發送簡訊
                            StringBuilder reqUrl = new StringBuilder();
                            reqUrl.Append(strAPI + "b2c/mtk/SmSend?");
                            reqUrl.Append("username=" + strAccount);
                            reqUrl.Append("&password=" + strPWD);
                            reqUrl.Append("&dstaddr=" + strPhone);
                            reqUrl.Append("&smbody=" + HttpUtility.UrlEncode((IsTest ? "(系統測試)" : "") + "您的驗證碼為:" + strValidCode, System.Text.Encoding.UTF8));
                            reqUrl.Append("&CharsetURL=UTF-8");
                            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(new Uri(reqUrl.ToString()));
                            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                            StreamReader sr = new StreamReader(response.GetResponseStream());
                            string result = sr.ReadToEnd();

                            if (result.IndexOf("statuscode=1") != -1)
                            {
                                this.Session["_ValidCode"] = strValidCode;
                                this.Session["_ValidTime"] = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                            }
                            else
                            {
                                strMsg = "簡訊發送失敗";
                            }
                            break;
                        case "E":   //  Email
                                    //  發送 Mail
                            var strMailSubject = (IsTest ? "(系統測試)" : "") + "紅利提領驗證碼";
                            //var strMailTo = "gin23157@gmail.com"; //  Todo > Test
                            var strMailTo = this.GetCurrentUser.EMAIL_1.ConvertToString();
                            if (IsTest)
                            {
                                strMailTo = strTestMail;
                            }
                            var strMailBody = "親愛的客戶 " + this.GetCurrentUser.CUST_CNAME.ConvertToString().Trim();
                            if (this.GetCurrentUser.CUST_CNAME2.ConvertToString().Trim() != string.Empty)
                            {
                                strMailBody += " / " + this.GetCurrentUser.CUST_CNAME2.ConvertToString().Trim();
                            }
                            strMailBody += "<br/></br/>";
                            strMailBody += "您的驗證碼為：" + strValidCode + "。";
                            strMailBody += "<br/>";
                            strMailBody += "客戶服務部<br/>";
                            strMailBody += "Best Leader Markets PTY Ltd<br/>";

                            SendMail(new List<string>() { strMailTo }, strMailSubject, strMailBody);

                            this.Session["_ValidCode"] = strValidCode;
                            this.Session["_ValidTime"] = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                            break;
                        default:
                            strMsg = "驗證碼發送失敗(沒設定發送方式)";
                            break;
                    }
                }
            }
            else
            {
                strMsg = "驗證碼發送失敗";
            }

            if (strMsg != string.Empty)
            {
                var strScript = "<script> alert('" + strMsg + "') </script>";
                this.Page.RegisterClientScriptBlock("Alert", strScript);
                strResult = false;
            }
            return strResult;
        }

        public static string GetConfigValueByKey(string strKey)
        {
            return System.Web.Configuration.WebConfigurationManager.AppSettings[strKey].ConvertToString().Trim();
        }

        protected void linkSendData_Click(object sender, EventArgs e)
        {
            DateTime dtNow = DateTime.Now;
            var strValidCode = this.Session["_ValidCode"].ConvertToString();
            var strValidTime = this.Session["_ValidTime"].ConvertToString();
            var strValidInput = this.hidValidInput.Value;

            //  2019/10/28 > 業務不用驗證碼確認
            if (strValidCode == strValidInput || this.GetCurrentUser.IS_SALES_LOGIN.ConvertToString().Trim() == "Y")
            {
                DateTime dtValieTime = DateTime.Parse(strValidTime);
                TimeSpan ts = dtNow - dtValieTime;
                //  2019/10/28 > 業務不用Check Time out
                if (ts.TotalSeconds >= 5 * 60 && this.GetCurrentUser.IS_SALES_LOGIN.ConvertToString().Trim() != "Y")
                {
                    this.Page.RegisterClientScriptBlock("Alert", "<script> alert('簡訊已逾期，請重新獲取新的驗證碼') </script>");
                }
                else
                {
                    //  寫入DB
                    var data = this.GridView1.Rows.Cast<GridViewRow>()
                        .Where(x => ((CheckBox)x.FindControl("chkSelect")).Checked == true).ToList();
                    var dataSave = new List<BL_ACCOUNT_TXN_TEMP>();
                    var strTempMailBody = string.Empty;
                    decimal? strBatchSN;
                    strBatchSN = new LogicCustomer().GetNextTempSN();

                    data.ForEach(x =>
                    {
                        var lblAmountCurrency = (Label)x.FindControl("lblAmountCurrency");
                        var lblCanApplyAmount = (Label)x.FindControl("lblCanApplyAmount");
                        var txtApplyAmount = (TextBox)x.FindControl("txtApplyAmount");
                        var lblORDER_NO = (Label)x.FindControl("lblORDER_NO");
                        decimal decCanApplyAmount = 0;
                        decimal decApplyAmount = 0;
                        decimal decTry;

                        if (decimal.TryParse(lblCanApplyAmount.Text.Replace(",", ""), out decTry))
                        {
                            decCanApplyAmount = decTry;
                        }

                        if (decimal.TryParse(txtApplyAmount.Text.Replace(",", ""), out decTry))
                        {
                            decApplyAmount = decTry;
                        }

                        var dataSingle = new BL_ACCOUNT_TXN_TEMP();
                        dataSingle.ORDER_NO = lblORDER_NO.Text;
                        dataSingle.CUST_ID = this.GetCurrentUser.CUST_ID;

                        switch (lblAmountCurrency.Text)
                        {
                            case "NTD":
                                dataSingle.AMOUNT_NTD = decCanApplyAmount;
                                dataSingle.ACTUAL_WITHDRAWAL_AMT_NTD = decApplyAmount;
                                break;
                            case "RMB":
                                dataSingle.AMOUNT_RMB = decCanApplyAmount;
                                dataSingle.ACTUAL_WITHDRAWAL_AMT_RMB = decApplyAmount;
                                break;
                            case "EUR":
                                dataSingle.AMOUNT_EUR = decCanApplyAmount;
                                dataSingle.ACTUAL_WITHDRAWAL_AMT_EUR = decApplyAmount;
                                break;
                            case "AUD":
                                dataSingle.AMOUNT_AUD = decCanApplyAmount;
                                dataSingle.ACTUAL_WITHDRAWAL_AMT_AUD = decApplyAmount;
                                break;
                            case "JPY":
                                dataSingle.AMOUNT_JPY = decCanApplyAmount;
                                dataSingle.ACTUAL_WITHDRAWAL_AMT_JPY = decApplyAmount;
                                break;
                            case "NZD":
                                dataSingle.AMOUNT_NZD = decCanApplyAmount;
                                dataSingle.ACTUAL_WITHDRAWAL_AMT_NZD = decApplyAmount;
                                break;
                            case "USD":
                            default:
                                dataSingle.AMOUNT_USD = decCanApplyAmount;
                                dataSingle.ACTUAL_WITHDRAWAL_AMT_USD = decApplyAmount;
                                break;
                        }
                        dataSingle.BONUS_TSF = "N";

                        dataSingle.TXN_DATE = DateTime.Now;
                        dataSingle.WITHDRAWAL_TYPE = "WEB";
                        dataSingle.APPLY_USER = this.GetCurrentUser.CUST_ID == null ? "" : this.GetCurrentUser.CUST_ID.Value.ConvertToString();
                        dataSingle.APPLY_DATE = DateTime.Now;
                        dataSingle.BATCH_WITHDRAWAL_ID = strBatchSN;

                        //  2019/10/28 > 業務代出金功能
                        if (this.GetCurrentUser.IS_SALES_LOGIN.ConvertToString().Trim() == "Y")
                        {
                            dataSingle.WEB_SUBMIT_SALES_IBCODE = this.GetCurrentUser.SALES_IB_CODE.ConvertToString();
                            dataSingle.SALES_WEB_SUBMIT_DATETIME = DateTime.Now;
                            dataSingle.IS_SALES_SUBMIT_TEMP = "Y";
                        }

                        dataSave.Add(dataSingle);

                        strTempMailBody += "<tr>";
                        strTempMailBody += "<td>" + lblORDER_NO.Text + "</td>";
                        strTempMailBody += "<td>" + lblAmountCurrency.Text + "</td>";
                        strTempMailBody += "<td>" + decApplyAmount.ToString("#,0.0") + "</td>";
                        strTempMailBody += "</tr>";
                    });

                    if (new LogicCustomer().AddNewDepositOut(dataSave, this.GetCurrentUser))
                    {
                        var strTestMail = GetConfigValueByKey("smTestMail");
                        var IsTest = false;
                        if (strTestMail != string.Empty) IsTest = true;
                        //  發送Mail
                        var strMailSubject = (IsTest ? "(系統測試)" : "") + "紅利提領申請送出通知";
                        var strMailTo = this.GetCurrentUser.EMAIL_1.ConvertToString();
                        if (IsTest)
                            strMailTo = strTestMail;
                        var strMailBody = "親愛的客戶 " + this.GetCurrentUser.CUST_CNAME.ConvertToString().Trim();
                        if (this.GetCurrentUser.CUST_CNAME2.ConvertToString().Trim() != string.Empty)
                        {
                            strMailBody += " / " + this.GetCurrentUser.CUST_CNAME2.ConvertToString().Trim();
                        }
                        strMailBody += "<br/></br/>";
                        strMailBody += "您" + DateTime.Now.ToString("yyyy/MM/dd") + "申請的出金合約資料如下<br/>";
                        strMailBody += "申請出金合約數：<span style='text-decoration: underline;'>&nbsp;&nbsp;" + data.Count().ConvertToString() + "&nbsp;&nbsp;</span><br/>";
                        //strMailBody += "本次申請出金總金額：()：<span style='text-decoration: underline;'>&nbsp;&nbsp;" + "&nbsp;&nbsp;</span><br/>";
                        strMailBody += "[本次申請出金總金額]";
                        strMailBody += "詳細合約出金明細如下<br/>";
                        //strMailBody += "<table border='1'>";
                        strMailBody += "<table style='border: 1px solid black;'>";  //  Todo > Alex > 驗證Border
                        strMailBody += "<tr><th>合約編號</th><th>幣別</th><th>出金金額</th></tr>";
                        strMailBody += strTempMailBody;
                        strMailBody += "</table>";
                        strMailBody += "<br/>";
                        strMailBody += "總提領金額:<br/>";

                        var strTotalInfo = string.Empty;
                        var decAUD = dataSave.Sum(x => x.ACTUAL_WITHDRAWAL_AMT_AUD == null ? 0 : x.ACTUAL_WITHDRAWAL_AMT_AUD.Value);
                        var decEUR = dataSave.Sum(x => x.ACTUAL_WITHDRAWAL_AMT_EUR == null ? 0 : x.ACTUAL_WITHDRAWAL_AMT_EUR.Value);
                        var decNTD = dataSave.Sum(x => x.ACTUAL_WITHDRAWAL_AMT_NTD == null ? 0 : x.ACTUAL_WITHDRAWAL_AMT_NTD.Value);
                        var decRMB = dataSave.Sum(x => x.ACTUAL_WITHDRAWAL_AMT_RMB == null ? 0 : x.ACTUAL_WITHDRAWAL_AMT_RMB.Value);
                        var decUSD = dataSave.Sum(x => x.ACTUAL_WITHDRAWAL_AMT_USD == null ? 0 : x.ACTUAL_WITHDRAWAL_AMT_USD.Value);
                        var decJPY = dataSave.Sum(x => x.ACTUAL_WITHDRAWAL_AMT_JPY == null ? 0 : x.ACTUAL_WITHDRAWAL_AMT_JPY.Value);
                        var decNZD = dataSave.Sum(x => x.ACTUAL_WITHDRAWAL_AMT_NZD == null ? 0 : x.ACTUAL_WITHDRAWAL_AMT_NZD.Value);
                        if (decAUD > 0)
                        {
                            strTotalInfo += "AUD:" + decAUD.ToString("#,0.0");
                        }
                        if (decEUR > 0)
                        {
                            strTotalInfo += "EUR:" + decEUR.ToString("#,0.0");
                        }
                        if (decNTD > 0)
                        {
                            strTotalInfo += "NTD:" + decNTD.ToString("#,0.0");
                        }
                        if (decRMB > 0)
                        {
                            strTotalInfo += "RMB:" + decRMB.ToString("#,0.0");
                        }
                        if (decUSD > 0)
                        {
                            strTotalInfo += "USD:" + decUSD.ToString("#,0.0");
                        }
                        if (decJPY > 0)
                        {
                            strTotalInfo += "JPY:" + decJPY.ToString("#,0.0");
                        }
                        if (decNZD > 0)
                        {
                            strTotalInfo += "NZD:" + decNZD.ToString("#,0.0");
                        }
                        strMailBody += "本次申請出金總金額：<span style='text-decoration: underline;'>&nbsp;&nbsp;" + strTotalInfo + "&nbsp;&nbsp;</span><br/>";

                        strMailBody += "<br/>";
                        var strURL = this.Page.Request.Url.AbsoluteUri.ConvertToString().Replace("ApplyDeposit.aspx", "ExportPDF.aspx") +
                            "?sType=PDF&sID=" + (strBatchSN == null ? "0" : strBatchSN.Value.ConvertToString());
                        strMailBody += "<a href='" + strURL + "'>下載出金申請附檔</a>";
                        strMailBody += "<br/>";
                        strMailBody += "我們已收到申請, 會盡快為您處理<br/>";
                        strMailBody += "<br/>";
                        strMailBody += "客戶服務部<br/>";
                        strMailBody += "Best Leader Markets PTY Ltd<br/>";

                        //  2019/10/28 > 業務代出金不需要發送完成Mail
                        if (this.GetCurrentUser.IS_SALES_LOGIN.ConvertToString().Trim() != "Y")
                        {
                            SendMail(new List<string>() { strMailTo }, strMailSubject, strMailBody);
                        }

                        var strDirect = this.Page.Request.Url.AbsoluteUri.ConvertToString().Replace("ApplyDeposit.aspx", "ContractInfo.aspx");
                        this.Page.RegisterClientScriptBlock("Alert", "<script> alert('申請送出完成'); this.location.href='" + strDirect + "'; </script>");
                    }

                }
            }
            else
            {
                this.Page.RegisterClientScriptBlock("Alert", "<script> alert('簡訊驗證碼輸入錯誤') </script>");
            }
        }


        public static void SendMail(List<string> MailTo, string MailSubject, string MailBody)
        {
            try
            {
                MailMessage msg = new MailMessage();
                msg.To.Add(string.Join(",", MailTo.ToArray()));
                //   msg.CC.Add(string.Join(",", GetConfigValueByKey("MailSystemAdmin").Split(new string[] { ";" }, StringSplitOptions.None)));

                ////  Todo > Test
                //msg.To.Clear();
                ////msg.CC.Clear();
        
                msg.Bcc.Add(string.Join(",", GetConfigValueByKey("MailSystemAdmin").Split(new string[] { ";" }, StringSplitOptions.None)));

                msg.From = new MailAddress(
                    GetConfigValueByKey("MailFromAddress"),
                    "百麗客服部",
                    System.Text.Encoding.UTF8);
                msg.Subject = MailSubject;
                msg.SubjectEncoding = System.Text.Encoding.UTF8;
                msg.Body = MailBody;
                msg.IsBodyHtml = true;
                msg.BodyEncoding = System.Text.Encoding.UTF8;
                msg.Priority = MailPriority.Normal;

                //SmtpClient mySmtp = new SmtpClient("smtp.gmail.com", 587);
                //mySmtp.Credentials = new System.Net.NetworkCredential(GetConfigValueByKey("MailFromAccount"), GetConfigValueByKey("MailFromPwd"));
                //mySmtp.EnableSsl = true;

                //SmtpClient mySmtp = new SmtpClient("124.9.9.173", 25);
                SmtpClient mySmtp = new SmtpClient("168.95.4.150", 25);

                mySmtp.Send(msg);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        

        protected void linkTab_AUD_Click(object sender, EventArgs e)
        {
            this.GetCurrency = "AUD";
            this.BindData();
        }

        protected void linkTab_EUR_Click(object sender, EventArgs e)
        {
            this.GetCurrency = "EUR";
            this.BindData();
        }

        protected void linkTab_RMB_Click(object sender, EventArgs e)
        {
            this.GetCurrency = "RMB";
            this.BindData();
        }

        protected void linkTab_NTD_Click(object sender, EventArgs e)
        {
            this.GetCurrency = "NTD";
            this.BindData();
        }

        protected void linkTab_USD_Click(object sender, EventArgs e)
        {
            this.GetCurrency = "USD";
            this.BindData();
        }
        protected void linkTab_JPY_Click(object sender, EventArgs e)
        {
            this.GetCurrency = "JPY";
            this.BindData();
        }

        protected void linkTab_NZD_Click(object sender, EventArgs e)
        {
            this.GetCurrency = "NZD";
            this.BindData();
        }
    }
}