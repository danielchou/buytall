﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgentCustomer.Logic;
using AgentCustomer.Logic.BusinessLogic;
using AgentCustomer.Utility;
using AgentCustomer.Model;
using AgentCustomer.Model.Table;
using System.Text;
using System.Web.Script.Serialization;

namespace AgentCustomer.WebPage
{
    public partial class DepositOutDetail : System.Web.UI.Page
    {
        private BL_CUST GetCurrentUser
        {
            get
            {
                BL_CUST _user = new BL_CUST();

                if (this.Session["_User"] != null)
                {
                    if ((this.Session["_User"] as BL_CUST) == null)
                    {
                        this.SessionOut();
                    }
                    else
                    {
                        _user = this.Session["_user"] as BL_CUST;
                    }
                }
                else
                {
                    this.SessionOut();
                }


                return _user;
            }
        }

        private void SessionOut()
        {
            //var strScript = "<script> alert('連線逾時') </script>";
            //this.Page.RegisterClientScriptBlock("Alert", strScript);
            Response.Redirect("~/Customer_Login.aspx");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Page.IsPostBack==false)
            {
                this.BindData();
            }
        }

        private void BindData()
        {
            var dataUser = this.GetCurrentUser;
            var BatchID = this.Request["no"].ConvertToString();
            var strHtml = new StringBuilder();

            if (dataUser != null)
            {
                if (BatchID.IndexOf("T") == -1)
                {
                    #region 正式資料

                    var data = new LogicCustomer().GetDepositDataDetail2(BatchID, dataUser.CUST_ID == null ? "" : dataUser.CUST_ID.Value.ConvertToString());

                    if (data.Count() > 0)
                    {

                        strHtml.AppendLine("<table class='table rwdtable' id='tb1'> ");
                        strHtml.AppendLine(" <thead class='thead-light'> ");
                        strHtml.AppendLine("  <tr>");
                        strHtml.AppendLine("   <th class='th-Header'>提領單號</th> ");
                        strHtml.AppendLine("   <th class='th-Header'>合約編號</th> ");
                        strHtml.AppendLine("   <th class='th-Header'>幣別</th> ");
                        strHtml.AppendLine("   <th class='th-Header'>提領總額</th> ");
                        strHtml.AppendLine("  </tr>");
                        strHtml.AppendLine(" </thead>");
                        strHtml.AppendLine(" <tbody>");


                        data
                            .OrderByDescending(x => x.TXN_DATE)
                            .ThenBy(x => x.ORDER_NO)
                            .ToList()

                            .ForEach(x =>
                            {
                                strHtml.AppendLine("  <tr>");

                                var strBatchID = x.BATCH_WITHDRAWAL_ID == null ? "" : x.BATCH_WITHDRAWAL_ID.Value.ConvertToString();
                                strHtml.AppendLine("   <td data-label='提領單號' class='td-Center'>" + strBatchID + "</td> ");
                                strHtml.AppendLine("   <td data-label='合約編號' class='td-Center'>" + x.ORDER_NO.ConvertToString() + "</td> ");
                                strHtml.AppendLine("   <td data-label='幣別' class='td-Center'>" + x.CURRENCY.ConvertToString() + "</td> ");
                                strHtml.AppendLine("   <td data-label='提領總額' class='td-Numeric'>" + (x.AMOUNT == null ? "" : x.AMOUNT.Value.ToString("#,0.0")) + "</td> ");

                                strHtml.AppendLine("  </tr>");
                            });
                        strHtml.AppendLine(" </tbody>");
                        strHtml.AppendLine("</table>");
                    }
                    else
                    {
                        strHtml.AppendLine("<h3 style='font-size:23px;'>查無資料</h3>");
                    }
                    #endregion
                }
                else
                {
                    #region Temp 資料
                    var dataTemp = new LogicCustomer().GetApplyTempDataALLByCustID(dataUser.CUST_ID == null ? "" : dataUser.CUST_ID.Value.ConvertToString());
                    dataTemp = dataTemp.Where(x => (x.BATCH_WITHDRAWAL_ID == null ? "" : x.BATCH_WITHDRAWAL_ID.Value.ConvertToString()) == BatchID.Substring(1, BatchID.Length - 1)).ToList();
                    
                    if (dataTemp.Count() > 0)
                    {
                        strHtml.AppendLine("<table class='table rwdtable' id='tb1'> ");
                        strHtml.AppendLine(" <thead class='thead-light'> ");
                        strHtml.AppendLine("  <tr>");
                        strHtml.AppendLine("   <th class='th-Header'>提領單號</th> ");
                        strHtml.AppendLine("   <th class='th-Header'>合約編號</th> ");
                        strHtml.AppendLine("   <th class='th-Header'>幣別</th> ");
                        strHtml.AppendLine("   <th class='th-Header'>提領總額</th> ");
                        strHtml.AppendLine("  </tr>");
                        strHtml.AppendLine(" </thead>");
                        strHtml.AppendLine(" <tbody>");


                        dataTemp
                            .OrderByDescending(x => x.TXN_DATE)
                            .ThenBy(x => x.ORDER_NO)
                            .ToList()

                            .ForEach(x =>
                            {
                                strHtml.AppendLine("  <tr>");

                                var strBatchID = x.BATCH_WITHDRAWAL_ID == null ? "" : x.BATCH_WITHDRAWAL_ID.Value.ConvertToString();
                                strHtml.AppendLine("   <td data-label='提領單號' class='td-Center'>" + strBatchID + "</td> ");
                                strHtml.AppendLine("   <td data-label='合約編號' class='td-Center'>" + x.ORDER_NO.ConvertToString() + "</td> ");
                                strHtml.AppendLine("   <td data-label='幣別' class='td-Center'>" + x.Currency.ConvertToString() + "</td> ");
                                strHtml.AppendLine("   <td data-label='提領總額' class='td-Numeric'>" + (x.Amount == null ? "" : x.Amount.Value.ToString("#,0.0")) + "</td> ");

                                strHtml.AppendLine("  </tr>");
                            });
                        strHtml.AppendLine(" </tbody>");
                        strHtml.AppendLine("</table>");
                    }
                    else
                    {
                        strHtml.AppendLine("<h3 style='font-size:23px;'>查無資料</h3>");
                    }


                    #endregion
                }

                this.divDetail.InnerHtml = strHtml.ToString();
            }
        }

        protected void linkBack_Click(object sender, EventArgs e)
        {
            this.Page.Response.Redirect("~/WebPage/DepositOut.aspx");
        }
    }
}