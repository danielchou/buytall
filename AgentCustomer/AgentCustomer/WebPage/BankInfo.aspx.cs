﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgentCustomer.Logic;
using AgentCustomer.Logic.BusinessLogic;
using AgentCustomer.Utility;
using AgentCustomer.Model;
using AgentCustomer.Model.Table;
using System.Text;
using System.Web.Script.Serialization;


namespace AgentCustomer.WebPage
{
    public partial class BankInfo : System.Web.UI.Page
    {
        private BL_CUST GetCurrentUser
        {
            get
            {
                BL_CUST _user = new BL_CUST();

                if (this.Session["_User"] != null)
                {
                    if ((this.Session["_User"] as BL_CUST) == null)
                    {
                        this.SessionOut();
                    }
                    else
                    {
                        _user = this.Session["_user"] as BL_CUST;
                    }
                }
                else
                {
                    this.SessionOut();
                }


                return _user;
            }
        }

        private void SessionOut()
        {
            //var strScript = "<script> alert('連線逾時') </script>";
            //this.Page.RegisterClientScriptBlock("Alert", strScript);
            Response.Redirect("~/Customer_Login.aspx");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Page.IsPostBack == false)
            {
                this.BindCustBaseData();

            }
        }

        private void BindCustBaseData()
        {
            var dataUser = this.GetCurrentUser;
            if (dataUser != null)
            {
                var dataBank = new LogicCustomer().GetBankDataByCustID(dataUser.CUST_ID == null ? "" : dataUser.CUST_ID.Value.ConvertToString());

                #region 銀行資料
                //dataBank.Where
                //    (
                //        x =>
                //            x.BANK_CNAME.ConvertToString().Trim() != string.Empty &&
                //            x.ACCOUNT_CNAME.ConvertToString().Trim() != string.Empty &&
                //            x.ACCOUNT.ConvertToString().Trim() != string.Empty
                //    ).ToList();
                //  2019/01/29 > 調整為判斷Account就好, 有就Show
                dataBank = dataBank.Where
                    (
                        x =>
                            x.ACCOUNT.ConvertToString().Trim() != string.Empty
                    ).ToList();
                this.rpData.DataSource = dataBank;
                this.rpData.DataBind();
                #endregion

            }
        }


    }
}