﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Theme/Theme.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="AgentCustomer.WebPage.Index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .card-text span {
            color: black;
            font-size: 20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

        <div class="container">
            <div class="section-title">
                <h3>VIP Member Use Only</h3>
            </div>
            <div class="row">
                <div class="col-12 col-lg-4">
                    <div class="card features">
                        <div class="card-body">
                            <div class="media">
                                <span class="ti-face-smile gradient-fill ti-3x mr-3"></span>
                                <div class="media-body">
                                    <h4 class="card-title">收益穩健</h4>
                                    <p class="card-text"> 百麗穩健型商品 CLIENTS AGREEMENT , 穩健固定的收益,安全合規, 是您值的信賴的理財夥伴</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-4">
                    <div class="card features">
                        <div class="card-body">
                            <div class="media">
                                <span class="ti-settings gradient-fill ti-3x mr-3"></span>
                                <div class="media-body">
                                    <h4 class="card-title">個人專屬</h4>
                                    <p class="card-text">信託客戶有皆享有個人化的專業投資建議, 並提供完整平台查詢最您的帳戶交易記錄及收益明細 </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-4">
                    <div class="card features">
                        <div class="card-body">
                            <div class="media">
                                <span class="ti-lock gradient-fill ti-3x mr-3"></span>
                                <div class="media-body">
                                    <h4 class="card-title">安全可靠</h4>
                                    <p class="card-text">百麗商品是合法合規登記在案金融商品, 註冊地在澳洲,有專業顧問及監管人,是您最可靠的夥伴</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    <div class="section light-bg" id="features">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-2" style="padding-bottom: 6px; padding-top: 6px;">
                    <button type="button" class="btn btn-primary">
                        <h3 style="color: #FFFFFF; font-weight: bold;">合約數</h3>   
                        <span class="badge badge-light" style="font-size: 20px">
                            <asp:Label ID="lblContractCount" runat="server" Text=""></asp:Label>
                        </span>
                    </button>
                </div>
              
                <div class="col-12 col-lg-2" style="padding-bottom: 6px; padding-top: 6px;">
                    <button type="button" class="btn btn-primary">
                        <h3 style="color: #FFFFFF; font-weight: bold;"> 總餘額</h3>   
                        <span class="badge badge-light" style="font-size: 20px">
                            <asp:Label ID="lblTotalBalance" runat="server" Text="">$10,000</asp:Label>
                        </span>
                    </button>
                </div>
            </div>
            
            <div class="section-title" style="margin-bottom:0.75rem;">
                <h3>基本資料</h3>
            </div>
            <div class="row">
                <h4 class="card-title" style="padding-right:12px;">姓名</h4>
                <p class="card-text"><asp:Label ID="txtCUST_CNAME" runat="server" Text=""></asp:Label></p>
            </div>
            <div class="row">
                <h4 class="card-title" style="padding-right:12px;">姓名(英)</h4>
                <p class="card-text"><asp:Label ID="txtCUST_ENAME" runat="server" Text=""></asp:Label></p>
            </div>
            <div class="row">
                <h4 class="card-title" style="padding-right:12px;">性別</h4>
                <p class="card-text"><asp:Label ID="txtSex" runat="server" Text=""></asp:Label></p>
            </div>
            <div class="row">
                <h4 class="card-title" style="padding-right:12px;">出生年月日</h4>
                <p class="card-text"><asp:Label ID="txtBirthday" runat="server" Text=""></asp:Label></p>
            </div>
            <div class="row">
                <h4 class="card-title" style="padding-right:12px;">國籍</h4>
                <p class="card-text"><asp:Label ID="txtNATION" runat="server" Text=""></asp:Label></p>
            </div>
            <div class="row">
                <h4 class="card-title" style="padding-right:12px;">護照號碼</h4>
                <p class="card-text"><asp:Label ID="txtPASSPORT" runat="server" Text=""></asp:Label></p>
            </div>
            <div class="row">
                <h4 class="card-title" style="padding-right:12px;">聯絡電話</h4>
                <p class="card-text"><asp:Label ID="txtPHONE" runat="server" Text=""></asp:Label></p>
            </div>
            <div class="row">
                <h4 class="card-title" style="padding-right:12px;">身分證號碼</h4>
                <p class="card-text"><asp:Label ID="txtID_NUMBER" runat="server" Text=""></asp:Label></p>
            </div>
            <div class="row">
                <h4 class="card-title" style="padding-right:12px;">電子郵件信箱</h4>
                <p class="card-text"><asp:Label ID="txtEMAIL" runat="server" Text=""></asp:Label></p>
            </div>
            <div class="row">
                <h4 class="card-title" style="padding-right:12px;">居住地址</h4>
                <p class="card-text"><asp:Label ID="txtAddress" runat="server" Text=""></asp:Label></p>
            </div>


            
            <div class="section-title" style="margin-bottom:0.75rem;">
                <h3>銀行資料</h3>
            </div>
            <asp:Repeater ID="rpData" runat="server">
                <ItemTemplate>
                    <div class="row" style="padding-top:12px;">
                        <h4 class="card-title" style="padding-right:12px;">No.</h4>
                        <p class="card-text"><asp:Label ID="Label1" runat="server" Text='<%# Container.ItemIndex + 1 %>'></asp:Label></p>
                    </div>
                    
                    <div class="row">
                        <h4 class="card-title" style="padding-right:12px;">幣別</h4>
                        <p class="card-text"><asp:Label ID="txtCurrency" runat="server" Text='<%# Eval("CURRENCY") %>'></asp:Label></p>
                    </div>
                    <div class="row">
                        <h4 class="card-title" style="padding-right:12px;">銀行名稱</h4>
                        <p class="card-text"><asp:Label ID="txtBANK_CNAME" runat="server" Text='<%# Eval("BANK_CNAME") %>'></asp:Label></p>
                    </div>
                    <div class="row">
                        <h4 class="card-title" style="padding-right:12px;">分行名稱</h4>
                        <p class="card-text"><asp:Label ID="txtBRANCH_CNAME" runat="server" Text='<%# Eval("BRANCH_CNAME") %>'></asp:Label></p>
                    </div>
                    <div class="row">
                        <h4 class="card-title" style="padding-right:12px;">帳戶持有人姓名</h4>
                        <p class="card-text"><asp:Label ID="txtACCOUNT_CNAME" runat="server" Text='<%# Eval("ACCOUNT_CNAME") %>'></asp:Label></p>
                    </div>
                    <div class="row">
                        <h4 class="card-title" style="padding-right:12px;">銀行帳戶</h4>
                        <p class="card-text"><asp:Label ID="txtACCOUNT" runat="server" Text='<%# Eval("ACCOUNT") %>'></asp:Label></p>
                    </div>
                    <div class="row">
                        <h4 class="card-title" style="padding-right:12px;">SWIFT_CODE</h4>
                        <p class="card-text"><asp:Label ID="txtSWIFT_CODE" runat="server" Text='<%# Eval("SWIFT_CODE") %>'></asp:Label></p>
                    </div>
                    <div class="row">
                        <h4 class="card-title" style="padding-right:12px;">銀行地址</h4>
                        <p class="card-text"><asp:Label ID="txtBANK_C_ADDRESS" runat="server" Text='<%# Eval("BANK_C_ADDRESS") %>'></asp:Label></p>
                    </div>
                </ItemTemplate>
            </asp:Repeater>



        </div>
    </div>
   

    <div class="section light-bg" id="gallery">
        <div class="container">
            <div class="section-title">
                <%--<small>合約帳戶</small>--%>
                <h3>您的合約資料</h3>
            </div>
            
            <div id="divContractData" runat="server">

            </div>

        </div>
    </div>

    
    


<%--    <div class="section" id="pricing">
        <div class="container">
            <div class="section-title">
                <small>穩健型金融商品</small>
                <h3>商品類型</h3>
            </div>

            <div class="card-deck">
                <div class="card pricing">
                    <div class="card-head">
                        <small class="text-primary">一年期</small>
                        <span class="price">10% <sub>/年</sub></span>
                    </div>
                </div>
                <div class="card pricing popular">
                    <div class="card-head">
                        <small class="text-primary">二年期</small>
                        <span class="price">11%<sub>/年</sub></span>
                    </div>
                </div>
                <div class="card pricing">
                    <div class="card-head">
                        <small class="text-primary">三年期</small>
                        <span class="price">12%<sub>/年</sub></span>
                    </div>
                </div>
            </div>
        </div>
    </div>--%>



    
    <div class="light-bg py-5" id="contact">
        <div class="container">
            
            <div class="section-title">
                <h3>聯絡我們</h3>
            </div>

            <div class="row">
                <div class="col-lg-6 text-center text-lg-left">
                    <%--<p class="mb-2"> <span class="ti-location-pin mr-2"></span> Taiwan, Taipei</p>--%>
                    <div class=" d-block d-sm-inline-block">
                        <p class="mb-2">
                            <span class="ti-email mr-2"></span> <a class="mr-4" href="mailto:bestleaderservice@gmail.com">bestleaderservice@gmail.com</a>
                        </p>
                    </div>
                    <div class="d-block d-sm-inline-block">
                        <p class="mb-0">
                            <span class="ti-headphone-alt mr-2"></span> <a href="tel:288888888">02-8888-8888</a>
                        </p>
                    </div>

                </div>
                <div class="col-lg-6">
                    <div class="social-icons">&nbsp;

                    </div>
                </div>
            </div>
            <div class="row">
                
                <div class="col-lg-10 text-center text-lg-left">
                    <asp:textbox runat="server" id="txtComment" placeholder="請輸入內容" 
                        TextMode="MultiLine" Rows="4" style="width:100%;" ></asp:textbox>
                </div>
                <div class="col-lg-2 text-center text-lg-left">
                    <a href="#" class="btn btn-light">送出</a>
                </div>
            </div>

        </div>

    </div>
    

    

    <div id="divSubmit" style="display:none; text-align:center; padding-top:13px;" title="">
        <div class="container">
            
            <div class="section-title" style="margin-bottom:0px;">
                <h3>紅利提領</h3>
            </div>

            <div class="row">
                <h4 class="card-title" style="padding-right:12px;">本金</h4>
                <p class="card-text"><asp:Label ID="lblContractAmount" runat="server" Text=""></asp:Label></p>
            </div>
            <div class="row">
                <h4 class="card-title" style="padding-right:12px;">餘額</h4>
                <p class="card-text"><asp:Label ID="lblBalance" runat="server" Text=""></asp:Label></p>
            </div>
            <div class="row">
                <h4 class="card-title" style="padding-right:12px;">提領金額</h4>
                <p class="card-text"><asp:textbox ID="txtAmountOut" runat="server" Text=""></asp:textbox></p>
            </div>

            <div id="divError" style="color:red;">

            </div>
        </div>
    </div>
    
    <div id="divSubmit2" style="display:none; text-align:center; padding-top:13px;" title="">
        <div class="container">
            
            <div class="section-title" style="margin-bottom:0px;">
                <h3>線上紅利提領申請<br />尚未開放</h3>
            </div>
        </div>
    </div>
    
    <div id="divDetail" style="display:none; text-align:center; padding-top:13px;overflow-y:auto;" title="">
        <div class="container">
            <div class="section-title" style="margin-bottom:0px;" id="divDetailData">
                
            </div>
        </div>
    </div>
    

    <asp:HiddenField ID="hidOrderNo" runat="server" />
    <asp:HiddenField ID="hidAmountOut" runat="server" />



    <footer class="my-5 text-center">
        <!-- Copyright removal is not prohibited! -->
        <p class="mb-2"><small>Copyright @2014-2019 Best Leader Markets PTY Ltd.保留所有權利,且不得轉載網站內容於任何形式媒體</small></p>
    </footer>
    


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="server">

    <script>
        function openDepositOut(strOrderNo, strContractAmount, strBalance) {
            $("#<%=this.hidOrderNo.ClientID%>").val(strOrderNo);
            $("#<%=this.lblContractAmount.ClientID%>").text(strContractAmount);
            $("#<%=this.lblBalance.ClientID%>").text(strBalance);
            $("#divError").html("").css("display", "none");
            $("#<%=this.hidAmountOut.ClientID%>").val("");
            $("#<%=this.txtAmountOut.ClientID%>").val("");
            
            $("#divSubmit2").dialog({
                height: 290,
                width: 400,
                modal: true,
                resizable: true,
                dialogClass: "",
                buttons:
                {
                    '確認': function () {
                        $(this).dialog('close');
                    }
                }
            });


            return;
            $("#divSubmit").dialog({
                height: 290,
                width: 400,
                modal: true,
                resizable: true,
                dialogClass: "",
                buttons:
                {
                    '確認': function () {
                        var amountContract = parseInt($("#<%=this.lblContractAmount.ClientID%>").text());
                        var amountBalance = parseInt($("#<%=this.lblBalance.ClientID%>").text());
                        var amountOut = parseInt($("#<%=this.txtAmountOut.ClientID%>").val());

                        console.log("amountContract:" + amountContract + ";amountBalance:" + amountBalance + ";amountOut:" + amountOut);
                        if (amountOut > amountBalance - amountContract) {
                            $("#divError").html("提領申請金額不能大於「餘額-本金」").css("display", "block");
                        } else {
                            $("#<%=this.hidAmountOut.ClientID%>").val(amountOut);
                            //  Todo > do submit
                            $(this).dialog('close');

                            $("<div>申請送出成功</div>").dialog();
                        }
                    },
                    '取消': function () {
                        $(this).dialog('close');
                    }
                }
            });
          
        }


        
        function openOrderDateil(dataOrder) {
          
            $("#divDetailData").html("");

            var dataHeader = $("<h5>")
                .html("合約編號：" + dataOrder[0].ORDER_NO + "　　幣別：" + dataOrder[0].GetCURRENCY + "　　本金：" + dataOrder[0].GetBASE_AMOUNT)
                .css("text-align", "left");
            $("#divDetailData").append(dataHeader);
            dataHeader = $("<h5>")
                .html("合約起始日：" + dataOrder[0].GetFROM_DATE + "　　合約結束日：" + dataOrder[0].GetEND_DATE)
                .css("text-align", "left");
            $("#divDetailData").append(dataHeader);

            var dataTable = $("<table>")
                .addClass("table").addClass("rwdtable")
                .append
                (
                    $("<thead>")
                        .addClass("thead-light")
                        .append
                        (
                            $("<tr>")
                                .append($("<th>").html("出/入"))
                                .append($("<th>").html("交易類別"))
                                .append($("<th>").html("派息次數"))
                                .append($("<th>").html("交易日"))
                                .append($("<th>").html("金額"))
                        )
                );
            var dataBody = $("<tbody>");
            
            $.each(dataOrder, function (i, data) {
                $(dataBody)
                    .append
                    (
                        $("<tr>")

                            .append($("<td>").html(data.FLOW_TYPE))
                            .append($("<td>").html(data.TXN_TYPE))
                            .append($("<td>").html(data.INTEREST_TIMES))
                            .append($("<td>").html(data.GetTXN_DATE))
                            .append($("<td>").html(data.AMOUNT))
                    )
            });

            $(dataTable).append(dataBody);
            $("#divDetailData").append(dataTable);

            $("#divDetail").dialog({
                height: 420,
                width: 700,
                modal: true,
                resizable: true,
                dialogClass: "",
                buttons:
                {
                    '確認': function () {
                        $(this).dialog('close');
                    },
                    '關閉': function () {
                        $(this).dialog('close');
                    }
                }
            });
          
        }
    </script>

</asp:Content>
