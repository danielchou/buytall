﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgentCustomer.Logic;
using AgentCustomer.Logic.BusinessLogic;
using AgentCustomer.Utility;
using AgentCustomer.Model;
using AgentCustomer.Model.Table;
using System.Text;
using System.Web.Script.Serialization;

namespace AgentCustomer.WebPage
{
    public partial class ContractInfo : System.Web.UI.Page
    {

        private BL_CUST GetCurrentUser
        {
            get
            {
                BL_CUST _user = new BL_CUST();

                if (this.Session["_User"] != null)
                {
                    if ((this.Session["_User"] as BL_CUST) == null)
                    {
                        this.SessionOut();
                    }
                    else
                    {
                        _user = this.Session["_user"] as BL_CUST;
                    }
                }
                else
                {
                    this.SessionOut();
                }


                return _user;
            }
        }

        private void SessionOut()
        {
            //var strScript = "<script> alert('連線逾時') </script>";
            //this.Page.RegisterClientScriptBlock("Alert", strScript);
            Response.Redirect("~/Customer_Login.aspx");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Page.IsPostBack == false)
            {
                this.BindCustBaseData();

            }

        }

        private void BindCustBaseData()
        {
            var dataUser = this.GetCurrentUser;
            if (dataUser != null)
            {
                var strCustID = dataUser.CUST_ID == null ? "" : dataUser.CUST_ID.Value.ConvertToString();

                //  合約數
                this.lblContractCount.Text = dataUser.ORDER_NUMBER == null ? "0" : dataUser.ORDER_NUMBER.Value.ConvertToString();
                ////  本金
                //this.lblBaseAmount.Text = dataUser.TOTAL_BASE_AMOUNT == null ? "0" : dataUser.TOTAL_BASE_AMOUNT.Value.ToString("#,0");
                ////  可提領紅利
                //this.lblCanOutAmount.Text = dataUser.TOTAL_AVAIL_WITHDRAWAL_AMOUNT == null ? "0" : dataUser.TOTAL_AVAIL_WITHDRAWAL_AMOUNT.Value.ToString("#,0");
                ////  總資產
                //this.lblTotalBalance.Text = dataUser.TOTAL_ASSET == null ? "0" : dataUser.TOTAL_ASSET.Value.ToString("#,0");

                ////  累積總派利
                //this.Label1.Text = dataUser.HISTORY_TOTAL_WITHDRAWAL == null ? "0" : dataUser.HISTORY_TOTAL_WITHDRAWAL.Value.ToString("#,0");
                ////  累積報酬率
                //this.Label2.Text = dataUser.INVEST_DURATION == null ? "0" : dataUser.INVEST_DURATION.Value.ToString("#,0.0");

                string[] spData;
                string strTemp;
                //  本金
                spData = dataUser.TOTAL_BASE_AMOUNT.ConvertToString().Split(new string[] { "/" }, StringSplitOptions.None);
                strTemp = string.Empty;
                if (spData.Length == 1)
                {
                    strTemp += spData[0].IsNumeric() ? double.Parse(spData[0]).ToString("#,0.0") : "0.0";
                }
                else
                {
                    for (int i = 0; i < spData.Length; i++)
                    {
                        //strTemp += strTemp == string.Empty ? "" : "<br/>";
                        strTemp += "<div style='margin:3px;'>";
                        //switch (i)
                        //{
                        //    case 0:
                        //        strTemp += "USD: ";
                        //        break;
                        //    case 1:
                        //        strTemp += "RMB: ";
                        //        break;
                        //    case 2:
                        //        strTemp += "NTD: ";
                        //        break;
                        //    default:
                        //        break;
                        //}
                        strTemp += spData[i];//.IsNumeric() ? double.Parse(spData[i]).ToString("#,0.0") : "0.0";
                        strTemp += "</div>";
                    }
                }
                this.lblBaseAmount.Text = strTemp;
                if (spData.Length > 1)
                {
                    this.lblBaseAmount.Attributes["style"] = "text-align:left !important;";
                }
                else
                {
                    this.lblBaseAmount.Attributes["style"] = "";
                }
                this.lblUnApproveAmount.Text = dataUser.PROCESSING_ORDER_AMOUNT == null ? "0" : dataUser.PROCESSING_ORDER_AMOUNT.Value.ToString("#,0.0");
                if ((dataUser.PROCESSING_ORDER_AMOUNT == null ? 0 : dataUser.PROCESSING_ORDER_AMOUNT.Value) <= 0.0m)
                {
                    this.btnUnApproveAmount.Visible = false;
                }
                else
                {
                    //  有權限的才開
                    this.btnUnApproveAmount.Visible = true;
                }


                //  可提領紅利
                spData = dataUser.TOTAL_AVAIL_WITHDRAWAL_AMOUNT.ConvertToString().Split(new string[] { "/" }, StringSplitOptions.None);
                strTemp = string.Empty;
                if (spData.Length == 1)
                {
                    strTemp += spData[0].IsNumeric() ? double.Parse(spData[0]).ToString("#,0.0") : "0.0";
                }
                else
                {
                    for (int i = 0; i < spData.Length; i++)
                    {
                        //strTemp += strTemp == string.Empty ? "" : "<br/>";
                        strTemp += "<div style='margin:3px;'>";
                        //switch (i)
                        //{
                        //    case 0:
                        //        strTemp += "USD: ";
                        //        break;
                        //    case 1:
                        //        strTemp += "RMB: ";
                        //        break;
                        //    case 2:
                        //        strTemp += "NTD: ";
                        //        break;
                        //    default:
                        //        break;
                        //}
                        strTemp += spData[i];//.IsNumeric() ? double.Parse(spData[i]).ToString("#,0.0") : "0.0";
                        strTemp += "</div>";
                    }
                }
                this.lblCanOutAmount.Text = strTemp;
                if (spData.Length > 1)
                {
                    this.lblCanOutAmount.Attributes["style"] = "text-align:left !important;";
                }
                else
                {
                    this.lblCanOutAmount.Attributes["style"] = "";
                }

                //  總資產
                spData = dataUser.TOTAL_ASSET.ConvertToString().Split(new string[] { "/" }, StringSplitOptions.None);
                strTemp = string.Empty;
                if (spData.Length == 1)
                {
                    strTemp += spData[0].IsNumeric() ? double.Parse(spData[0]).ToString("#,0.0") : "0.0";
                }
                else
                {
                    for (int i = 0; i < spData.Length; i++)
                    {
                        //strTemp += strTemp == string.Empty ? "" : "<br/>";
                        strTemp += "<div style='margin:3px;'>";
                        //switch (i)
                        //{
                        //    case 0:
                        //        strTemp += "USD: ";
                        //        break;
                        //    case 1:
                        //        strTemp += "RMB: ";
                        //        break;
                        //    case 2:
                        //        strTemp += "NTD: ";
                        //        break;
                        //    default:
                        //        break;
                        //}
                        strTemp += spData[i];//.IsNumeric() ? double.Parse(spData[i]).ToString("#,0.0") : "0.0";
                        strTemp += "</div>";
                    }
                }
                this.lblTotalBalance.Text = strTemp;
                if (spData.Length > 1)
                {
                    this.lblTotalBalance.Attributes["style"] = "text-align:left !important;";
                }
                else
                {
                    this.lblTotalBalance.Attributes["style"] = "";
                }

                //  累積總派利
                spData = dataUser.HISTORY_TOTAL_WITHDRAWAL.ConvertToString().Split(new string[] { "/" }, StringSplitOptions.None);
                strTemp = string.Empty;
                if (spData.Length == 1)
                {
                    strTemp += spData[0].IsNumeric() ? double.Parse(spData[0]).ToString("#,0.0") : "0.0";
                }
                else
                {
                    for (int i = 0; i < spData.Length; i++)
                    {
                        //strTemp += strTemp == string.Empty ? "" : "<br/>";
                        strTemp += "<div style='margin:3px;'>";
                        //switch (i)
                        //{
                        //    case 0:
                        //        strTemp += "USD: ";
                        //        break;
                        //    case 1:
                        //        strTemp += "RMB: ";
                        //        break;
                        //    case 2:
                        //        strTemp += "NTD: ";
                        //        break;
                        //    default:
                        //        break;
                        //}
                        strTemp += spData[i];//.IsNumeric() ? double.Parse(spData[i]).ToString("#,0.0") : "0.0";
                        strTemp += "</div>";
                    }
                }
                this.Label1.Text = strTemp;
                if (spData.Length > 1)
                {
                    this.Label1.Attributes["style"] = "text-align:left !important;";
                }
                else
                {
                    this.Label1.Attributes["style"] = "";
                }

                //  累計投資年期
                spData = dataUser.INVEST_DURATION.ConvertToString().Split(new string[] { "/" }, StringSplitOptions.None);
                strTemp = string.Empty;
                string[] strTemp2;
                
                if (spData.Length == 1)
                {
                    //strTemp += spData[0].IsNumeric() ? double.Parse(spData[0]).ToString("#,0.0") : "0.0";
                    strTemp2 = (spData[0].IsNumeric() ? double.Parse(spData[0]).ToString("#,0.0") : "0.0").Split(new string[] { "." }, StringSplitOptions.None);
                    strTemp += strTemp2[0] + "年";
                    if (strTemp2.Length >= 2)
                    {
                        strTemp += Math.Floor((double.Parse(strTemp2[1]) / 10.0 * 12.0)).ToString("0") + "月";
                    }
                }
                else
                {
                    for (int i = 0; i < spData.Length; i++)
                    {
                        strTemp += "<div style='margin:3px;'>";

                        strTemp2 = (spData[0].IsNumeric() ? double.Parse(spData[0]).ToString("#,0.0") : "0.0").Split(new string[] { "." }, StringSplitOptions.None);
                        //strTemp += spData[i];
                        strTemp += strTemp2[0] + "年";
                        if (strTemp2.Length >= 2)
                        {
                            strTemp += Math.Floor((double.Parse(strTemp2[1]) / 10.0 * 12.0)).ToString("0") + "月";
                        }
                        strTemp += "</div>";
                    }
                }
                this.Label2.Text = strTemp;
                if (spData.Length > 1)
                {
                    this.Label2.Attributes["style"] = "text-align:left !important;";
                }
                else
                {
                    this.Label2.Attributes["style"] = "";
                }


                //  提領紅利(申請中)
                spData = dataUser.TOTAL_APPLY_WITHDRAWAL_AMOUNT.ConvertToString().Split(new string[] { "/" }, StringSplitOptions.None);
                strTemp = string.Empty;
                if (spData.Length == 1)
                {
                    strTemp += spData[0].IsNumeric() ? double.Parse(spData[0]).ToString("#,0.0") : "0.0";
                }
                else
                {
                    for (int i = 0; i < spData.Length; i++)
                    {
                        strTemp += "<div style='margin:3px;'>";
                        strTemp += spData[i];
                        strTemp += "</div>";
                    }
                }
                this.lblCanOutAmount_OnApply.Text = strTemp;

                //  已到期未提領資產
                spData = dataUser.EXPIRED_UN_WITHDRAWAL_ASSET.ConvertToString().Split(new string[] { "/" }, StringSplitOptions.None);
                strTemp = string.Empty;
                if (spData.Length == 1)
                {
                    strTemp += spData[0].IsNumeric() ? double.Parse(spData[0]).ToString("#,0.0") : "0.0";
                }
                else
                {
                    for (int i = 0; i < spData.Length; i++)
                    {
                        strTemp += "<div style='margin:3px;'>";
                        strTemp += spData[i];
                        strTemp += "</div>";
                    }
                }
                this.lblDueAssets.Text = strTemp;
                if (spData.Length > 1)
                {
                    this.lblDueAssets.Attributes["style"] = "text-align:left !important;";
                }
                else
                {
                    this.lblDueAssets.Attributes["style"] = "";
                }



                this.BindContractData(strCustID);
            }
        }


        private void BindContractData(string CustID)
        {
            var data = new LogicCustomer().GetContractDataByCustID(CustID, true);
            var strCanOutAmount_OnApply = string.Empty; //  可提領紅利(申請中)
            var strHtml = new StringBuilder();
            //  2019/02/27 調整邏輯
            //data = data.Where(x => (x.ORDER_END == null ? DateTime.Now.AddDays(1) : x.ORDER_END.Value) >= DateTime.Now).ToList();
            bool IsCanApplyAmount = false;

            if (data.Count > 0)
            {
                var dataCurrency = data.Select
                    (
                        x =>
                        new
                        {
                            CURRENCY = x.CURRENCY,
                            SORT = x.CURRENCY.ConvertToString() == "USD" ? 1 : 2
                        }
                    ).Distinct().ToList();
                if (dataCurrency.Count() <= 1)
                {
                    strHtml.AppendLine(this.GetContractData(data, ref IsCanApplyAmount, ref strCanOutAmount_OnApply));
                }
                else
                {
                    #region 多幣別
                    var strTemp = string.Empty;
                    strHtml.AppendLine("<section class='project-tab'>");
                    strHtml.AppendLine("  <div class='container'>");
                    strHtml.AppendLine("    <div class='row'>");
                    strHtml.AppendLine("      <div class='col-md-12'>");
                    strHtml.AppendLine("        <nav>");
                    strHtml.AppendLine("         <div class='nav nav-tabs nav-fill' id='nav-tab' role='tablist'> ");
                    strTemp = string.Empty;
                    dataCurrency
                        .OrderBy(x => x.SORT)
                        .ThenBy(x => x.CURRENCY)
                        .ToList()
                        .ForEach(x =>
                        {
                            strTemp = strTemp == string.Empty ? "nav-item nav-link active" : "nav-item nav-link";
                            strHtml.AppendLine("  <a class='" + strTemp + "' id='nav-" + x.CURRENCY.ConvertToString() + "-tab' data-toggle='tab' href='#nav-" + x.CURRENCY.ConvertToString() + "' role='tab' aria-controls='nav-" + x.CURRENCY.ConvertToString() + "' aria-selected='true'>" + x.CURRENCY.ConvertToString() + "</a> ");
                        });
                    strHtml.AppendLine("         </div>");
                    strHtml.AppendLine("        </nav>");


                    strHtml.AppendLine("        <div class='tab-content' id='nav-tabContent'>");
                    strTemp = string.Empty;
                    dataCurrency
                        .OrderBy(x => x.SORT)
                        .ThenBy(x => x.CURRENCY)
                        .ToList()
                        .ForEach(x =>
                        {
                            strTemp = strTemp == string.Empty ? "tab-pane fade show active" : "tab-pane fade";
                            strHtml.AppendLine("    <div class='" + strTemp + "' id='nav-" + x.CURRENCY.ConvertToString() + "' role='tabpanel' aria-labelledby='nav-" + x.CURRENCY.ConvertToString() + "-tab'> ");
                            strHtml.AppendLine(this.GetContractData(data.Where(y => y.CURRENCY == x.CURRENCY).ToList(), ref IsCanApplyAmount, ref strCanOutAmount_OnApply));
                            strHtml.AppendLine("    </div>");
                        });

                    strHtml.AppendLine("        </div>");


                    strHtml.AppendLine("      </div>");
                    strHtml.AppendLine("    </div>");
                    strHtml.AppendLine("  </div>");
                    strHtml.AppendLine("</section>");

                    #endregion
                }
            }
            else
            {
                strHtml.AppendLine("<h3 style='font-size:23px;'>查無合約資料</h3>");
            }
            this.divContractData.InnerHtml = strHtml.ToString();
            switch (this.GetCurrentUser.IS_ONLINE_WITHDRAWAL.ConvertToString().ToUpper())
            {
                case "P":   //  Phone
                case "E":   //  Email
                    this.btnApplyAmount.Visible = true;
                    break;
                default:
                    this.btnApplyAmount.Visible = false;
                    break;
            }
            //  2019/10/28 > 如果是業務Login就無條件開啟
            if (this.GetCurrentUser.IS_SALES_LOGIN.ConvertToString().Trim() == "Y")
            {
                this.btnApplyAmount.Visible = true;
            }
            //this.btnApplyAmount.Visible = IsCanApplyAmount && this.GetCurrentUser.IS_CAN_ONLINE_WITHDRAWAL.ConvertToString() == "Y";

            //  查詢審核中的資料
            var dataOnPath = new LogicCustomer().GetApplyTempData(this.GetCurrentUser.CUST_ID == null ? "" : this.GetCurrentUser.CUST_ID.Value.ConvertToString());
            if (dataOnPath.Count() > 0)
            {
                switch (this.GetCurrentUser.IS_ONLINE_WITHDRAWAL.ConvertToString().ToUpper())
                {
                    case "P":   //  Phone
                    case "E":   //  Email
                        this.btnApplyStatus.Visible = true;
                        break;
                    default:
                        this.btnApplyStatus.Visible = false;
                        break;
                }
                //  2019/10/28 > 如果是業務Login就無條件開啟
                if (this.GetCurrentUser.IS_SALES_LOGIN.ConvertToString().Trim()=="Y")
                {
                  //  this.btnApplyStatus.Visible = true;
                }
            }
            else
            {
                this.btnApplyStatus.Visible = false;
            }
            ////this.btnApplyStatus.Visible = dataOnPath.Count() > 0 && this.GetCurrentUser.IS_CAN_ONLINE_WITHDRAWAL.ConvertToString() == "Y";
            ////  Todo > 2020/01/15 Display = none
            //this.btnApplyAmount.Visible = false;
            //this.btnApplyStatus.Visible = false;

            //  可提領紅利(申請中)
            if (this.lblCanOutAmount_OnApply.Text == string.Empty || this.lblCanOutAmount_OnApply.Text == "0.0")
            {
                this.btnCanOutAmount_OnApply.Visible = false;
            }
            else
            {
                this.btnCanOutAmount_OnApply.Visible = true;
            }
            //  已到期未提領資產
            if (this.lblDueAssets.Text == string.Empty || this.lblDueAssets.Text == "0.0")
            {
                this.btnDueAssets.Visible = false;
            }
            else
            {
                this.btnDueAssets.Visible = true;
            }

            //  提領紅利(申請中) or 已到期未提領資產, 有任一有值就不顯示和約數
            if (this.btnCanOutAmount_OnApply.Visible || this.btnDueAssets.Visible)
            {
                this.btnContractCount.Visible = false;
            }
        }

        private string GetContractData(List<BL_ORDER> data, ref bool IsCanApplyAmount, ref string strCanOutAmount_OnApply)
        {
            var strHtml = new StringBuilder();
            bool _IsCanApplyAmount = false;
            var strCurrency = string.Empty;

            var dataOrderList = new LogicCustomer().GetOrderDataByORDER_NO
                  (
                      data.Select(x => x.ORDER_NO.ConvertToString()).ToList()
                  );

            var dataOrderList_Temp = new LogicCustomer().GetOrderTempDataByORDER_NO
                  (
                      data.Select(x => x.ORDER_NO.ConvertToString()).ToList()
                  );
            var IsShowApplyOnTemp = false;
            data.ForEach(x =>
            {
                if (x.ORDER_APPLY_WITHDRAWAL_AMOUNT != null)
                {
                    if (x.ORDER_APPLY_WITHDRAWAL_AMOUNT.Value != 0)
                    {
                        IsShowApplyOnTemp = true;
                    }
                }

            });

            strHtml.AppendLine("<table class='table rwdtable' id='tb1'> ");
            strHtml.AppendLine(" <thead class='thead-light'> ");
            strHtml.AppendLine("  <tr><td data-label='總計' class='td-Numeric' colspan='11'><span style='color:GRAY;'>* 系統呈現資產餘額與對帳單餘額若有小數位尾差, 請以對帳單為準</span></td> </tr>");
            strHtml.AppendLine("  <tr>");
            strHtml.AppendLine("   <th class='td-Text'>合約編號</th> ");
            strHtml.AppendLine("   <th class='td-Text'>幣別</th> ");
            strHtml.AppendLine("   <th class='td-Numeric'>本金(A)</th> ");
            strHtml.AppendLine("   <th class='td-Numeric'>累積紅利</th> ");
            strHtml.AppendLine("   <th class='td-Numeric'>當月紅利</th> ");
            strHtml.AppendLine("   <th class='td-Numeric'>可提領紅利(B)</th> ");
            strHtml.AppendLine("   <th class='td-Numeric' style=\"display:"+(IsShowApplyOnTemp?"block":"none") +";\">可提領紅利(申請中)</th> ");
            strHtml.AppendLine("   <th class='td-Numeric'>餘額(A+B)</th> ");
            strHtml.AppendLine("   <th class='td-Text'>合約起始日</th> ");
            strHtml.AppendLine("   <th class='td-Text'>合約結束日</th> ");
            strHtml.AppendLine("   <th class='th-Header'>合約展延日</th> ");
            strHtml.AppendLine("   <th class='th-Header'>年期</th> ");
            strHtml.AppendLine("  </tr>");
            strHtml.AppendLine(" </thead>");
            strHtml.AppendLine(" <tbody>");

            decimal decTotalAmountA = 0;
            decimal decTotalAmountB = 0;
            decimal decTotalAmountB_Temp = data.Sum(x => x.ORDER_APPLY_WITHDRAWAL_AMOUNT == null ? 0 : x.ORDER_APPLY_WITHDRAWAL_AMOUNT.Value);
            decimal decTotalAmountAB = 0;
            decimal decTotalACCUMULATE_INTEREST = 0;
            decimal decTotalMONTHLY_INTEREST = 0;

            #region Set Order
            List<string> liOrder = new List<string>();
            if (this.drpOrder1.SelectedValue.ConvertToString().Trim() != string.Empty)
            {
                liOrder.Add(this.drpOrder1.SelectedValue.ConvertToString().Trim());
            }
            if (this.drpOrder2.SelectedValue.ConvertToString().Trim() != string.Empty)
            {
                liOrder.Add(this.drpOrder2.SelectedValue.ConvertToString().Trim());
            }
            if (this.drpOrder3.SelectedValue.ConvertToString().Trim() != string.Empty)
            {
                liOrder.Add(this.drpOrder3.SelectedValue.ConvertToString().Trim());
            }

            liOrder.ForEach(x =>
            {
                //switch (x)
                //{
                //    case "合約編號":
                //        data = data.OrderBy(y => y.ORDER_NO).ToList();
                //        break;
                //    case "幣別":
                //        data = data.OrderBy(y => y.AmountCurrency).ToList();
                //        break;
                //    case "本金(A)":
                //        data = data.OrderBy(y => y.AmountByCurrency).ToList();
                //        break;
                //    case "累積紅利":
                //        data = data.OrderBy(y => y.ACCUMULATE_INTEREST).ToList();
                //        break;
                //    case "當月紅利":
                //        data = data.OrderBy(y => y.MONTHLY_INTEREST).ToList();
                //        break;
                //    case "可提領紅利(B)":
                //        data = data.OrderBy(y => y.BALANCE - y.AmountByCurrency).ToList();
                //        break;
                //    case "餘額(A+B)":
                //        data = data.OrderBy(y => y.BALANCE).ToList();
                //        break;
                //    case "合約起始日":
                //        data = data.OrderBy(y => y.FROM_DATE).ToList();
                //        break;
                //    case "合約結束日":
                //        data = data.OrderBy(y => y.END_DATE).ToList();
                //        break;
                //    case "合約展延日":
                //        data = data.OrderBy(y => y.EXTEND_DATE).ToList();
                //        break;
                //    case "年期":
                //        data = data.OrderBy(y => y.YEAR).ToList();
                //        break;
                //    default:
                //        break;
                //}
            });

            if (liOrder.Count() == 0)
            {
                data = data.OrderBy(x => x.FROM_DATE).ToList();
            }
            else
            {
                switch (liOrder[0])
                {
                    case "合約編號":
                        if (liOrder.Count() > 1)
                        {
                            #region Sort 2
                            switch (liOrder[1])
                            {
                                case "合約編號":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.ORDER_NO).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.ORDER_NO).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.ORDER_NO).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.ORDER_NO).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.ORDER_NO).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.ORDER_NO).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.ORDER_NO).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.ORDER_NO).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.ORDER_NO).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.ORDER_NO).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.ORDER_NO).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.ORDER_NO).ToList();
                                    }
                                    break;
                                case "幣別":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.AmountCurrency).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.AmountCurrency).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.AmountCurrency).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.AmountCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.AmountCurrency).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.AmountCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.AmountCurrency).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.AmountCurrency).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.AmountCurrency).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.AmountCurrency).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.AmountCurrency).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.AmountCurrency).ToList();
                                    }
                                    break;
                                case "本金(A)":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.AmountByCurrency).ToList();
                                    }
                                    break;
                                case "累積紅利":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.ACCUMULATE_INTEREST).ToList();
                                    }
                                    break;
                                case "當月紅利":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.MONTHLY_INTEREST).ToList();
                                    }
                                    break;
                                case "可提領紅利(B)":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList();
                                    }
                                    break;
                                case "餘額(A+B)":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.BALANCE).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.BALANCE).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.BALANCE).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.BALANCE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.BALANCE).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.BALANCE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.BALANCE).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.BALANCE).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.BALANCE).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.BALANCE).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.BALANCE).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.BALANCE).ToList();
                                    }
                                    break;
                                case "合約起始日":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.FROM_DATE).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.FROM_DATE).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.FROM_DATE).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.FROM_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.FROM_DATE).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.FROM_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.FROM_DATE).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.FROM_DATE).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.FROM_DATE).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.FROM_DATE).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.FROM_DATE).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.FROM_DATE).ToList();
                                    }
                                    break;
                                case "合約結束日":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.END_DATE).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.END_DATE).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.END_DATE).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.END_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.END_DATE).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.END_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.END_DATE).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.END_DATE).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.END_DATE).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.END_DATE).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.END_DATE).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.END_DATE).ToList();
                                    }
                                    break;
                                case "合約展延日":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.EXTEND_DATE).ToList();
                                    }
                                    break;
                                case "年期":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.YEAR).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.YEAR).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.YEAR).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.YEAR).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.YEAR).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.YEAR).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.YEAR).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.YEAR).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.YEAR).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.YEAR).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.YEAR).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.ORDER_NO).ThenBy(x => x.YEAR).ToList();
                                    }
                                    break;
                                default:
                                    break;
                            }
                            #endregion
                        }
                        else
                        {
                            data = data.OrderBy(x => x.ORDER_NO).ToList();
                        }
                        break;
                    case "幣別":
                        if (liOrder.Count() > 1)
                        {
                            #region Sort 2
                            switch (liOrder[1])
                            {
                                case "合約編號":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.ORDER_NO).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.ORDER_NO).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.ORDER_NO).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.ORDER_NO).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.ORDER_NO).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.ORDER_NO).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.ORDER_NO).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.ORDER_NO).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.ORDER_NO).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.ORDER_NO).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.ORDER_NO).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.ORDER_NO).ToList();
                                    }
                                    break;
                                case "幣別":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.AmountCurrency).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.AmountCurrency).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.AmountCurrency).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.AmountCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.AmountCurrency).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.AmountCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.AmountCurrency).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.AmountCurrency).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.AmountCurrency).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.AmountCurrency).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.AmountCurrency).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.AmountCurrency).ToList();
                                    }
                                    break;
                                case "本金(A)":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.AmountByCurrency).ToList();
                                    }
                                    break;
                                case "累積紅利":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ToList();
                                    }
                                    break;
                                case "當月紅利":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.MONTHLY_INTEREST).ToList();
                                    }
                                    break;
                                case "可提領紅利(B)":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList();
                                    }
                                    break;
                                case "餘額(A+B)":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.BALANCE).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.BALANCE).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.BALANCE).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.BALANCE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.BALANCE).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.BALANCE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.BALANCE).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.BALANCE).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.BALANCE).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.BALANCE).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.BALANCE).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.BALANCE).ToList();
                                    }
                                    break;
                                case "合約起始日":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.FROM_DATE).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.FROM_DATE).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.FROM_DATE).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.FROM_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.FROM_DATE).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.FROM_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.FROM_DATE).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.FROM_DATE).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.FROM_DATE).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.FROM_DATE).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.FROM_DATE).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.FROM_DATE).ToList();
                                    }
                                    break;
                                case "合約結束日":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.END_DATE).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.END_DATE).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.END_DATE).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.END_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.END_DATE).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.END_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.END_DATE).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.END_DATE).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.END_DATE).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.END_DATE).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.END_DATE).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.END_DATE).ToList();
                                    }
                                    break;
                                case "合約展延日":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.EXTEND_DATE).ToList();
                                    }
                                    break;
                                case "年期":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.YEAR).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.YEAR).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.YEAR).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.YEAR).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.YEAR).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.YEAR).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.YEAR).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.YEAR).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.YEAR).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.YEAR).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.YEAR).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.AmountCurrency).ThenBy(x => x.YEAR).ToList();
                                    }
                                    break;
                                default:
                                    break;
                            }
                            #endregion
                        }
                        else
                        {
                            data = data.OrderBy(x => x.AmountCurrency).ToList();
                        }
                        break;
                    case "本金(A)":
                        if (liOrder.Count() > 1)
                        {
                            #region Sort 2
                            switch (liOrder[1])
                            {
                                case "合約編號":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.ORDER_NO).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.ORDER_NO).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.ORDER_NO).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.ORDER_NO).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.ORDER_NO).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.ORDER_NO).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.ORDER_NO).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.ORDER_NO).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.ORDER_NO).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.ORDER_NO).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.ORDER_NO).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.ORDER_NO).ToList();
                                    }
                                    break;
                                case "幣別":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.AmountCurrency).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.AmountCurrency).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.AmountCurrency).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.AmountCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.AmountCurrency).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.AmountCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.AmountCurrency).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.AmountCurrency).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.AmountCurrency).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.AmountCurrency).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.AmountCurrency).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.AmountCurrency).ToList();
                                    }
                                    break;
                                case "本金(A)":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.AmountByCurrency).ToList();
                                    }
                                    break;
                                case "累積紅利":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ToList();
                                    }
                                    break;
                                case "當月紅利":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.MONTHLY_INTEREST).ToList();
                                    }
                                    break;
                                case "可提領紅利(B)":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList();
                                    }
                                    break;
                                case "餘額(A+B)":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.BALANCE).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.BALANCE).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.BALANCE).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.BALANCE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.BALANCE).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.BALANCE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.BALANCE).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.BALANCE).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.BALANCE).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.BALANCE).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.BALANCE).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.BALANCE).ToList();
                                    }
                                    break;
                                case "合約起始日":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.FROM_DATE).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.FROM_DATE).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.FROM_DATE).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.FROM_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.FROM_DATE).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.FROM_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.FROM_DATE).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.FROM_DATE).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.FROM_DATE).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.FROM_DATE).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.FROM_DATE).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.FROM_DATE).ToList();
                                    }
                                    break;
                                case "合約結束日":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.END_DATE).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.END_DATE).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.END_DATE).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.END_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.END_DATE).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.END_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.END_DATE).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.END_DATE).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.END_DATE).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.END_DATE).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.END_DATE).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.END_DATE).ToList();
                                    }
                                    break;
                                case "合約展延日":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.EXTEND_DATE).ToList();
                                    }
                                    break;
                                case "年期":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.YEAR).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.YEAR).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.YEAR).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.YEAR).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.YEAR).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.YEAR).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.YEAR).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.YEAR).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.YEAR).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.YEAR).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.YEAR).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.AmountByCurrency).ThenBy(x => x.YEAR).ToList();
                                    }
                                    break;
                                default:
                                    break;
                            }
                            #endregion
                        }
                        else
                        {
                            data = data.OrderBy(x => x.AmountByCurrency).ToList();
                        }
                        break;
                    case "累積紅利":
                        if (liOrder.Count() > 1)
                        {
                            #region Sort 2
                            switch (liOrder[1])
                            {
                                case "合約編號":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.ORDER_NO).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.ORDER_NO).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.ORDER_NO).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.ORDER_NO).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.ORDER_NO).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.ORDER_NO).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.ORDER_NO).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.ORDER_NO).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.ORDER_NO).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.ORDER_NO).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.ORDER_NO).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.ORDER_NO).ToList();
                                    }
                                    break;
                                case "幣別":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.AmountCurrency).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.AmountCurrency).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.AmountCurrency).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.AmountCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.AmountCurrency).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.AmountCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.AmountCurrency).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.AmountCurrency).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.AmountCurrency).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.AmountCurrency).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.AmountCurrency).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.AmountCurrency).ToList();
                                    }
                                    break;
                                case "本金(A)":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.AmountByCurrency).ToList();
                                    }
                                    break;
                                case "累積紅利":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.ACCUMULATE_INTEREST).ToList();
                                    }
                                    break;
                                case "當月紅利":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.MONTHLY_INTEREST).ToList();
                                    }
                                    break;
                                case "可提領紅利(B)":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList();
                                    }
                                    break;
                                case "餘額(A+B)":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.BALANCE).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.BALANCE).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.BALANCE).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.BALANCE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.BALANCE).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.BALANCE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.BALANCE).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.BALANCE).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.BALANCE).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.BALANCE).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.BALANCE).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.BALANCE).ToList();
                                    }
                                    break;
                                case "合約起始日":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.FROM_DATE).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.FROM_DATE).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.FROM_DATE).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.FROM_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.FROM_DATE).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.FROM_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.FROM_DATE).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.FROM_DATE).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.FROM_DATE).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.FROM_DATE).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.FROM_DATE).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.FROM_DATE).ToList();
                                    }
                                    break;
                                case "合約結束日":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.END_DATE).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.END_DATE).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.END_DATE).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.END_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.END_DATE).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.END_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.END_DATE).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.END_DATE).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.END_DATE).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.END_DATE).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.END_DATE).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.END_DATE).ToList();
                                    }
                                    break;
                                case "合約展延日":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.EXTEND_DATE).ToList();
                                    }
                                    break;
                                case "年期":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.YEAR).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.YEAR).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.YEAR).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.YEAR).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.YEAR).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.YEAR).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.YEAR).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.YEAR).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.YEAR).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.YEAR).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.YEAR).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.YEAR).ToList();
                                    }
                                    break;
                                default:
                                    break;
                            }
                            #endregion
                        }
                        else
                        {
                            data = data.OrderBy(x => x.ACCUMULATE_INTEREST).ToList();
                        }
                        break;
                    case "當月紅利":
                        if (liOrder.Count() > 1)
                        {
                            #region Sort 2
                            switch (liOrder[1])
                            {
                                case "合約編號":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.ORDER_NO).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.ORDER_NO).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.ORDER_NO).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.ORDER_NO).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.ORDER_NO).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.ORDER_NO).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.ORDER_NO).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.ORDER_NO).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.ORDER_NO).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.ORDER_NO).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.ORDER_NO).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.ORDER_NO).ToList();
                                    }
                                    break;
                                case "幣別":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.AmountCurrency).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.AmountCurrency).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.AmountCurrency).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.AmountCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.AmountCurrency).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.AmountCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.AmountCurrency).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.AmountCurrency).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.AmountCurrency).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.AmountCurrency).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.AmountCurrency).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.AmountCurrency).ToList();
                                    }
                                    break;
                                case "本金(A)":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.AmountByCurrency).ToList();
                                    }
                                    break;
                                case "累積紅利":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.ACCUMULATE_INTEREST).ToList();
                                    }
                                    break;
                                case "當月紅利":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.MONTHLY_INTEREST).ToList();
                                    }
                                    break;
                                case "可提領紅利(B)":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList();
                                    }
                                    break;
                                case "餘額(A+B)":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.BALANCE).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.BALANCE).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.BALANCE).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.BALANCE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.BALANCE).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.BALANCE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.BALANCE).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.BALANCE).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.BALANCE).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.BALANCE).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.BALANCE).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.BALANCE).ToList();
                                    }
                                    break;
                                case "合約起始日":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.FROM_DATE).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.FROM_DATE).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.FROM_DATE).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.FROM_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.FROM_DATE).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.FROM_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.FROM_DATE).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.FROM_DATE).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.FROM_DATE).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.FROM_DATE).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.FROM_DATE).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.FROM_DATE).ToList();
                                    }
                                    break;
                                case "合約結束日":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.END_DATE).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.END_DATE).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.END_DATE).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.END_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.END_DATE).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.END_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.END_DATE).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.END_DATE).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.END_DATE).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.END_DATE).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.END_DATE).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.END_DATE).ToList();
                                    }
                                    break;
                                case "合約展延日":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.EXTEND_DATE).ToList();
                                    }
                                    break;
                                case "年期":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.YEAR).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.YEAR).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.YEAR).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.YEAR).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.YEAR).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.YEAR).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.YEAR).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.YEAR).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.YEAR).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.YEAR).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.YEAR).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.YEAR).ToList();
                                    }
                                    break;
                                default:
                                    break;
                            }
                            #endregion
                        }
                        else
                        {
                            data = data.OrderBy(x => x.MONTHLY_INTEREST).ToList();
                        }
                        break;
                    case "可提領紅利(B)":
                        if (liOrder.Count() > 1)
                        {
                            #region Sort 2
                            switch (liOrder[1])
                            {
                                case "合約編號":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.ORDER_NO).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.ORDER_NO).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.ORDER_NO).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.ORDER_NO).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.ORDER_NO).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.ORDER_NO).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.ORDER_NO).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.ORDER_NO).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.ORDER_NO).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.ORDER_NO).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.ORDER_NO).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.ORDER_NO).ToList();
                                    }
                                    break;
                                case "幣別":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.AmountCurrency).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.AmountCurrency).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.AmountCurrency).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.AmountCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.AmountCurrency).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.AmountCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.AmountCurrency).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.AmountCurrency).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.AmountCurrency).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.AmountCurrency).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.AmountCurrency).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.AmountCurrency).ToList();
                                    }
                                    break;
                                case "本金(A)":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.AmountByCurrency).ToList();
                                    }
                                    break;
                                case "累積紅利":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ToList();
                                    }
                                    break;
                                case "當月紅利":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.MONTHLY_INTEREST).ToList();
                                    }
                                    break;
                                case "可提領紅利(B)":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList();
                                    }
                                    break;
                                case "餘額(A+B)":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.BALANCE).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.BALANCE).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.BALANCE).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.BALANCE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.BALANCE).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.BALANCE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.BALANCE).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.BALANCE).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.BALANCE).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.BALANCE).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.BALANCE).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.BALANCE).ToList();
                                    }
                                    break;
                                case "合約起始日":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.FROM_DATE).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.FROM_DATE).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.FROM_DATE).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.FROM_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.FROM_DATE).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.FROM_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.FROM_DATE).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.FROM_DATE).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.FROM_DATE).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.FROM_DATE).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.FROM_DATE).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.FROM_DATE).ToList();
                                    }
                                    break;
                                case "合約結束日":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.END_DATE).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.END_DATE).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.END_DATE).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.END_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.END_DATE).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.END_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.END_DATE).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.END_DATE).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.END_DATE).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.END_DATE).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.END_DATE).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.END_DATE).ToList();
                                    }
                                    break;
                                case "合約展延日":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.EXTEND_DATE).ToList();
                                    }
                                    break;
                                case "年期":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.YEAR).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.YEAR).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.YEAR).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.YEAR).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.YEAR).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.YEAR).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.YEAR).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.YEAR).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.YEAR).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.YEAR).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.YEAR).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.YEAR).ToList();
                                    }
                                    break;
                                default:
                                    break;
                            }
                            #endregion
                        }
                        else
                        {
                            data = data.OrderBy(x => x.BALANCE - x.AmountByCurrency).ToList();
                        }
                        break;
                    case "餘額(A+B)":
                        if (liOrder.Count() > 1)
                        {
                            #region Sort 2
                            switch (liOrder[1])
                            {
                                case "合約編號":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.ORDER_NO).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.ORDER_NO).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.ORDER_NO).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.ORDER_NO).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.ORDER_NO).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.ORDER_NO).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.ORDER_NO).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.ORDER_NO).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.ORDER_NO).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.ORDER_NO).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.ORDER_NO).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.ORDER_NO).ToList();
                                    }
                                    break;
                                case "幣別":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.AmountCurrency).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.AmountCurrency).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.AmountCurrency).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.AmountCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.AmountCurrency).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.AmountCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.AmountCurrency).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.AmountCurrency).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.AmountCurrency).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.AmountCurrency).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.AmountCurrency).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.AmountCurrency).ToList();
                                    }
                                    break;
                                case "本金(A)":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.AmountByCurrency).ToList();
                                    }
                                    break;
                                case "累積紅利":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList();
                                    }
                                    break;
                                case "當月紅利":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.MONTHLY_INTEREST).ToList();
                                    }
                                    break;
                                case "可提領紅利(B)":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList();
                                    }
                                    break;
                                case "餘額(A+B)":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.BALANCE).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.BALANCE).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.BALANCE).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.BALANCE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.BALANCE).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.BALANCE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.BALANCE).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.BALANCE).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.BALANCE).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.BALANCE).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.BALANCE).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.BALANCE).ToList();
                                    }
                                    break;
                                case "合約起始日":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.FROM_DATE).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.FROM_DATE).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.FROM_DATE).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.FROM_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.FROM_DATE).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.FROM_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.FROM_DATE).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.FROM_DATE).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.FROM_DATE).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.FROM_DATE).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.FROM_DATE).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.FROM_DATE).ToList();
                                    }
                                    break;
                                case "合約結束日":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.END_DATE).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.END_DATE).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.END_DATE).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.END_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.END_DATE).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.END_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.END_DATE).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.END_DATE).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.END_DATE).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.END_DATE).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.END_DATE).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.END_DATE).ToList();
                                    }
                                    break;
                                case "合約展延日":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.EXTEND_DATE).ToList();
                                    }
                                    break;
                                case "年期":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.YEAR).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.YEAR).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.YEAR).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.YEAR).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.YEAR).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.YEAR).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.YEAR).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.YEAR).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.YEAR).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.YEAR).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.YEAR).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.BALANCE).ThenBy(x => x.YEAR).ToList();
                                    }
                                    break;
                                default:
                                    break;
                            }
                            #endregion
                        }
                        else
                        {
                            data = data.OrderBy(x => x.BALANCE).ToList();
                        }
                        break;
                    case "合約起始日":
                        if (liOrder.Count() > 1)
                        {
                            #region Sort 2
                            switch (liOrder[1])
                            {
                                case "合約編號":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.ORDER_NO).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.ORDER_NO).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.ORDER_NO).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.ORDER_NO).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.ORDER_NO).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.ORDER_NO).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.ORDER_NO).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.ORDER_NO).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.ORDER_NO).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.ORDER_NO).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.ORDER_NO).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.ORDER_NO).ToList();
                                    }
                                    break;
                                case "幣別":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.AmountCurrency).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.AmountCurrency).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.AmountCurrency).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.AmountCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.AmountCurrency).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.AmountCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.AmountCurrency).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.AmountCurrency).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.AmountCurrency).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.AmountCurrency).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.AmountCurrency).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.AmountCurrency).ToList();
                                    }
                                    break;
                                case "本金(A)":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.AmountByCurrency).ToList();
                                    }
                                    break;
                                case "累積紅利":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList();
                                    }
                                    break;
                                case "當月紅利":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.MONTHLY_INTEREST).ToList();
                                    }
                                    break;
                                case "可提領紅利(B)":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList();
                                    }
                                    break;
                                case "餘額(A+B)":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.BALANCE).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.BALANCE).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.BALANCE).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.BALANCE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.BALANCE).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.BALANCE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.BALANCE).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.BALANCE).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.BALANCE).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.BALANCE).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.BALANCE).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.BALANCE).ToList();
                                    }
                                    break;
                                case "合約起始日":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.FROM_DATE).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.FROM_DATE).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.FROM_DATE).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.FROM_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.FROM_DATE).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.FROM_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.FROM_DATE).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.FROM_DATE).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.FROM_DATE).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.FROM_DATE).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.FROM_DATE).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.FROM_DATE).ToList();
                                    }
                                    break;
                                case "合約結束日":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.END_DATE).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.END_DATE).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.END_DATE).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.END_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.END_DATE).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.END_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.END_DATE).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.END_DATE).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.END_DATE).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.END_DATE).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.END_DATE).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.END_DATE).ToList();
                                    }
                                    break;
                                case "合約展延日":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.EXTEND_DATE).ToList();
                                    }
                                    break;
                                case "年期":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.YEAR).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.YEAR).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.YEAR).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.YEAR).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.YEAR).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.YEAR).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.YEAR).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.YEAR).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.YEAR).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.YEAR).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.YEAR).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.FROM_DATE).ThenBy(x => x.YEAR).ToList();
                                    }
                                    break;
                                default:
                                    break;
                            }
                            #endregion
                        }
                        else
                        {
                            data = data.OrderBy(x => x.FROM_DATE).ToList();
                        }
                        break;
                    case "合約結束日":
                        if (liOrder.Count() > 1)
                        {
                            #region Sort 2
                            switch (liOrder[1])
                            {
                                case "合約編號":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.ORDER_NO).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.ORDER_NO).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.ORDER_NO).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.ORDER_NO).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.ORDER_NO).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.ORDER_NO).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.ORDER_NO).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.ORDER_NO).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.ORDER_NO).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.ORDER_NO).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.ORDER_NO).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.ORDER_NO).ToList();
                                    }
                                    break;
                                case "幣別":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.AmountCurrency).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.AmountCurrency).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.AmountCurrency).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.AmountCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.AmountCurrency).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.AmountCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.AmountCurrency).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.AmountCurrency).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.AmountCurrency).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.AmountCurrency).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.AmountCurrency).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.AmountCurrency).ToList();
                                    }
                                    break;
                                case "本金(A)":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.AmountByCurrency).ToList();
                                    }
                                    break;
                                case "累積紅利":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList();
                                    }
                                    break;
                                case "當月紅利":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.MONTHLY_INTEREST).ToList();
                                    }
                                    break;
                                case "可提領紅利(B)":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList();
                                    }
                                    break;
                                case "餘額(A+B)":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.BALANCE).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.BALANCE).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.BALANCE).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.BALANCE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.BALANCE).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.BALANCE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.BALANCE).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.BALANCE).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.BALANCE).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.BALANCE).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.BALANCE).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.BALANCE).ToList();
                                    }
                                    break;
                                case "合約起始日":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.FROM_DATE).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.FROM_DATE).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.FROM_DATE).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.FROM_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.FROM_DATE).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.FROM_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.FROM_DATE).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.FROM_DATE).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.FROM_DATE).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.FROM_DATE).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.FROM_DATE).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.FROM_DATE).ToList();
                                    }
                                    break;
                                case "合約結束日":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.END_DATE).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.END_DATE).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.END_DATE).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.END_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.END_DATE).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.END_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.END_DATE).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.END_DATE).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.END_DATE).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.END_DATE).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.END_DATE).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.END_DATE).ToList();
                                    }
                                    break;
                                case "合約展延日":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.EXTEND_DATE).ToList();
                                    }
                                    break;
                                case "年期":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.YEAR).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.YEAR).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.YEAR).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.YEAR).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.YEAR).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.YEAR).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.YEAR).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.YEAR).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.YEAR).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.YEAR).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.YEAR).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.END_DATE).ThenBy(x => x.YEAR).ToList();
                                    }
                                    break;
                                default:
                                    break;
                            }
                            #endregion
                        }
                        else
                        {
                            data = data.OrderBy(x => x.END_DATE).ToList();
                        }
                        break;
                    case "合約展延日":
                        if (liOrder.Count() > 1)
                        {
                            #region Sort 2
                            switch (liOrder[1])
                            {
                                case "合約編號":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.ORDER_NO).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.ORDER_NO).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.ORDER_NO).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.ORDER_NO).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.ORDER_NO).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.ORDER_NO).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.ORDER_NO).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.ORDER_NO).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.ORDER_NO).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.ORDER_NO).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.ORDER_NO).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.ORDER_NO).ToList();
                                    }
                                    break;
                                case "幣別":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.AmountCurrency).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.AmountCurrency).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.AmountCurrency).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.AmountCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.AmountCurrency).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.AmountCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.AmountCurrency).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.AmountCurrency).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.AmountCurrency).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.AmountCurrency).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.AmountCurrency).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.AmountCurrency).ToList();
                                    }
                                    break;
                                case "本金(A)":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.AmountByCurrency).ToList();
                                    }
                                    break;
                                case "累積紅利":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList();
                                    }
                                    break;
                                case "當月紅利":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.MONTHLY_INTEREST).ToList();
                                    }
                                    break;
                                case "可提領紅利(B)":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList();
                                    }
                                    break;
                                case "餘額(A+B)":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.BALANCE).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.BALANCE).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.BALANCE).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.BALANCE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.BALANCE).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.BALANCE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.BALANCE).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.BALANCE).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.BALANCE).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.BALANCE).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.BALANCE).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.BALANCE).ToList();
                                    }
                                    break;
                                case "合約起始日":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.FROM_DATE).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.FROM_DATE).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.FROM_DATE).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.FROM_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.FROM_DATE).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.FROM_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.FROM_DATE).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.FROM_DATE).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.FROM_DATE).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.FROM_DATE).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.FROM_DATE).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.FROM_DATE).ToList();
                                    }
                                    break;
                                case "合約結束日":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.END_DATE).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.END_DATE).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.END_DATE).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.END_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.END_DATE).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.END_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.END_DATE).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.END_DATE).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.END_DATE).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.END_DATE).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.END_DATE).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.END_DATE).ToList();
                                    }
                                    break;
                                case "合約展延日":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.EXTEND_DATE).ToList();
                                    }
                                    break;
                                case "年期":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.YEAR).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.YEAR).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.YEAR).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.YEAR).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.YEAR).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.YEAR).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.YEAR).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.YEAR).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.YEAR).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.YEAR).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.YEAR).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.EXTEND_DATE).ThenBy(x => x.YEAR).ToList();
                                    }
                                    break;
                                default:
                                    break;
                            }
                            #endregion
                        }
                        else
                        {
                            data = data.OrderBy(x => x.EXTEND_DATE).ToList();
                        }
                        break;
                    case "年期":
                        if (liOrder.Count() > 1)
                        {
                            #region Sort 2
                            switch (liOrder[1])
                            {
                                case "合約編號":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.ORDER_NO).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.ORDER_NO).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.ORDER_NO).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.ORDER_NO).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.ORDER_NO).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.ORDER_NO).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.ORDER_NO).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.ORDER_NO).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.ORDER_NO).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.ORDER_NO).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.ORDER_NO).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.YEAR).ThenBy(x => x.ORDER_NO).ToList();
                                    }
                                    break;
                                case "幣別":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.AmountCurrency).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.AmountCurrency).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.AmountCurrency).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.AmountCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.AmountCurrency).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.AmountCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.AmountCurrency).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.AmountCurrency).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.AmountCurrency).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.AmountCurrency).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.AmountCurrency).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.YEAR).ThenBy(x => x.AmountCurrency).ToList();
                                    }
                                    break;
                                case "本金(A)":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.AmountByCurrency).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.YEAR).ThenBy(x => x.AmountByCurrency).ToList();
                                    }
                                    break;
                                case "累積紅利":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.ACCUMULATE_INTEREST).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.YEAR).ThenBy(x => x.ACCUMULATE_INTEREST).ToList();
                                    }
                                    break;
                                case "當月紅利":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.MONTHLY_INTEREST).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.YEAR).ThenBy(x => x.MONTHLY_INTEREST).ToList();
                                    }
                                    break;
                                case "可提領紅利(B)":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.BALANCE - x.AmountByCurrency).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.YEAR).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList();
                                    }
                                    break;
                                case "餘額(A+B)":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.BALANCE).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.BALANCE).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.BALANCE).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.BALANCE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.BALANCE).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.BALANCE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.BALANCE).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.BALANCE).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.BALANCE).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.BALANCE).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.BALANCE).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.YEAR).ThenBy(x => x.BALANCE).ToList();
                                    }
                                    break;
                                case "合約起始日":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.FROM_DATE).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.FROM_DATE).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.FROM_DATE).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.FROM_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.FROM_DATE).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.FROM_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.FROM_DATE).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.FROM_DATE).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.FROM_DATE).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.FROM_DATE).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.FROM_DATE).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.YEAR).ThenBy(x => x.FROM_DATE).ToList();
                                    }
                                    break;
                                case "合約結束日":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.END_DATE).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.END_DATE).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.END_DATE).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.END_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.END_DATE).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.END_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.END_DATE).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.END_DATE).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.END_DATE).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.END_DATE).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.END_DATE).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.YEAR).ThenBy(x => x.END_DATE).ToList();
                                    }
                                    break;
                                case "合約展延日":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.EXTEND_DATE).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.YEAR).ThenBy(x => x.EXTEND_DATE).ToList();
                                    }
                                    break;
                                case "年期":
                                    if (liOrder.Count() > 2)
                                    {
                                        #region Sort 3
                                        switch (liOrder[2])
                                        {
                                            case "合約編號": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.YEAR).ThenBy(x => x.ORDER_NO).ToList(); break;
                                            case "幣別": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.YEAR).ThenBy(x => x.AmountCurrency).ToList(); break;
                                            case "本金(A)": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.YEAR).ThenBy(x => x.AmountByCurrency).ToList(); break;
                                            case "累積紅利": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.YEAR).ThenBy(x => x.ACCUMULATE_INTEREST).ToList(); break;
                                            case "當月紅利": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.YEAR).ThenBy(x => x.MONTHLY_INTEREST).ToList(); break;
                                            case "可提領紅利(B)": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.YEAR).ThenBy(x => x.BALANCE - x.AmountByCurrency).ToList(); break;
                                            case "餘額(A+B)": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.YEAR).ThenBy(x => x.BALANCE).ToList(); break;
                                            case "合約起始日": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.YEAR).ThenBy(x => x.FROM_DATE).ToList(); break;
                                            case "合約結束日": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.YEAR).ThenBy(x => x.END_DATE).ToList(); break;
                                            case "合約展延日": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.YEAR).ThenBy(x => x.EXTEND_DATE).ToList(); break;
                                            case "年期": data = data.OrderBy(x => x.YEAR).ThenBy(x => x.YEAR).ThenBy(x => x.YEAR).ToList(); break;
                                            default: break;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        data = data.OrderBy(x => x.YEAR).ThenBy(x => x.YEAR).ToList();
                                    }
                                    break;
                                default:
                                    break;
                            }
                            #endregion
                        }
                        else
                        {
                            data = data.OrderBy(x => x.YEAR).ToList();
                        }
                        break;
                    default:
                        break;
                }
            }

            #endregion

            data
                //.OrderBy(x => x.FROM_DATE)
                .ToList()
                .ForEach(x =>
                {
                    var dataOrder = dataOrderList.Where(y => y.ORDER_NO == x.ORDER_NO).ToList();
                    //var dataOrder_Temp = dataOrderList_Temp.Where(y => y.ORDER_NO == x.ORDER_NO).ToList();
                    var strOrderNo = x.ORDER_NO.ConvertToString();
                    var strContractAmount = x.AmountByCurrency == null ? "" : x.AmountByCurrency.Value.ToString("#,0.0");
                    var strBalance = x.BALANCE == null ? "0" : x.BALANCE.Value.ToString("#,0.0");
                    //  可提領紅利 = 餘額 - 本金
                    var strAmountB = (x.BALANCE == null ? 0 : x.BALANCE.Value) - (x.AmountByCurrency == null ? 0 : x.AmountByCurrency.Value);
                    if (strAmountB <= 0)
                    {
                        strAmountB = 0;
                    }
                    else
                    {
                        _IsCanApplyAmount = true;
                    }

                    var strBouns_Option = string.Empty;
                    switch (x.BONUS_OPTION.ConvertToString().Trim())
                    {
                        case "A":
                            strBouns_Option = "新春專案";
                            break;
                  //    case "B":
                  //        strBouns_Option = "新春專案B";
                  //          break;
                        default:

                            break;
                    }
                    if (strBouns_Option != string.Empty)
                    {
                        strBouns_Option = "<label style='color:red; display:block;font-size: 12px'>" + strBouns_Option + "</label>";
                    }
                    
                    //var strAmountB_Temp = dataOrderList_Temp.Where(y => y.ORDER_NO == x.ORDER_NO)
                    //    .ToList()
                    //    .Sum(y => y.ACTUAL_Amount == null ? 0 : y.ACTUAL_Amount.Value);
                    //decTotalAmountB_Temp += strAmountB_Temp;


                    decTotalAmountA += x.AmountByCurrency == null ? 0 : x.AmountByCurrency.Value;
                    decTotalAmountB += strAmountB;
                    decTotalAmountAB += x.BALANCE == null ? 0 : x.BALANCE.Value;
                    decTotalACCUMULATE_INTEREST += x.ACCUMULATE_INTEREST == null ? 0 : x.ACCUMULATE_INTEREST.Value;
                    decTotalMONTHLY_INTEREST += x.MONTHLY_INTEREST == null ? 0 : x.MONTHLY_INTEREST.Value;

                    strHtml.AppendLine("  <tr>");

                    if (dataOrder.Count > 0)
                    {
                        var jsonSerialiser = new JavaScriptSerializer();
                        var json = jsonSerialiser.Serialize(dataOrder);
                        strHtml.AppendLine("   <td data-label='合約編號' class='td-Text'><a href=\"\" onclick='openOrderDateil(" + json + ");return false;'>" + strOrderNo + "</a>" + strBouns_Option + "</td> ");
                    }
                    else
                    {
                        strHtml.AppendLine("   <td data-label='合約編號' class='td-Text'>" +
                                (
                                    (x.STATUS_CODE == null ? 0 : x.STATUS_CODE.Value) == 3
                                    ? "<label style='color:red; display:block;'>(審核中)</label>"
                                    : ""
                                ) +
                                strOrderNo + strBouns_Option +
                                "</td> ");
                    }


                    var strAmount = strContractAmount;
                    strHtml.AppendLine("   <td data-label='幣別' class='td-Text'>" + x.AmountCurrency.ConvertToString() + "</td> ");
                    strHtml.AppendLine("   <td data-label='本金(A)' class='td-Numeric'>" + strAmount + "</td> ");

                    strHtml.AppendLine("   <td data-label='累積紅利' class='td-Numeric'>" + (x.ACCUMULATE_INTEREST == null ? "" : x.ACCUMULATE_INTEREST.Value.ToString("#,0.0")) + "</td> ");
                    strHtml.AppendLine("   <td data-label='當月紅利' class='td-Numeric'>" + (x.MONTHLY_INTEREST == null ? "" : x.MONTHLY_INTEREST.Value.ToString("#,0.0")) + "</td> ");
                    strHtml.AppendLine("   <td data-label='可提領紅利(B)' class='td-Numeric'>" + strAmountB.ToString("#,0.0") + "</td> ");
                    //strHtml.AppendLine("   <td data-label='可提領紅利(申請中)' class='td-Numeric'>" + strAmountB_Temp.ToString("#,0.0") + "</td> ");
                    strHtml.AppendLine("   <td data-label='可提領紅利(申請中)' class='td-Numeric' style=\"display:" + (IsShowApplyOnTemp ? "block" : "none") + ";\">" + (x.ORDER_APPLY_WITHDRAWAL_AMOUNT==null?"":x.ORDER_APPLY_WITHDRAWAL_AMOUNT.Value.ToString("#,0.0")) + "</td> ");
                    strHtml.AppendLine("   <td data-label='餘額(A+B)' class='td-Numeric'>" + strBalance + "</td> ");
                    strHtml.AppendLine("   <td data-label='合約起始日' class='td-Text'>" + (x.FROM_DATE == null ? "" : x.FROM_DATE.Value.ToString("yyyy/MM/dd")) + "</td> ");
                    strHtml.AppendLine("   <td data-label='合約結束日' class='td-Text'>" + (x.END_DATE == null ? "" : x.END_DATE.Value.ToString("yyyy/MM/dd")) + "</td> ");
                    strHtml.AppendLine("   <td data-label='合約展延日' class='th-Header'>" + (x.EXTEND_DATE == null ? "無" : x.EXTEND_DATE.Value.ToString("yyyy/MM/dd")) + "</td> ");
                    strHtml.AppendLine("   <td data-label='年期' class='th-Header'>" + (x.YEAR == null ? "" : x.YEAR.Value.ConvertToString()) + "</td> ");

                    strHtml.AppendLine("  </tr>");
                    if (strCurrency == string.Empty)
                    {
                        strCurrency = x.AmountCurrency.ConvertToString();
                    }
                });

            if (data.Count > 0)
            {
                strHtml.AppendLine("  <tr>");
                strHtml.AppendLine("   <td data-label='總計' class='td-Text' colspan='2'>總計</td>");
                strHtml.AppendLine("   <td data-label='本金(A)' class='td-Numeric'>" + decTotalAmountA.ToString("#,0.0") + "</td> ");
                strHtml.AppendLine("   <td data-label='累積紅利' class='td-Numeric'>" + decTotalACCUMULATE_INTEREST.ToString("#,0.0") + "</td> ");
                strHtml.AppendLine("   <td data-label='每月紅利' class='td-Numeric'>" + decTotalMONTHLY_INTEREST.ToString("#,0.0") + "</td> ");
                strHtml.AppendLine("   <td data-label='可提領紅利(B)' class='td-Numeric'>" + decTotalAmountB.ToString("#,0.0") + "</td> ");
                strHtml.AppendLine("   <td data-label='可提領紅利(申請中)' class='td-Numeric' style=\"display:" + (IsShowApplyOnTemp ? "block" : "none") + ";\">" + decTotalAmountB_Temp.ToString("#,0.0") + "</td> ");
                strHtml.AppendLine("   <td data-label='餘額(A+B)' class='td-Numeric'>" + decTotalAmountAB.ToString("#,0.0") + "</td> ");
                strHtml.AppendLine("   <td data-label='' class='td-Numeric' colspan='4'></td> ");
                strHtml.AppendLine("  </tr>");
            }

            strHtml.AppendLine(" </tbody>");
            strHtml.AppendLine("</table>");

            if (_IsCanApplyAmount)
            {
                IsCanApplyAmount = true;
            }

            //  可提領紅利(申請中)
            if (decTotalAmountB_Temp > 0)
            {
                if (strCanOutAmount_OnApply != string.Empty)
                {
                    strCanOutAmount_OnApply += "<br/>";
                }
                if (strCurrency != string.Empty)
                {
                    strCanOutAmount_OnApply += strCurrency + ":";
                }
                strCanOutAmount_OnApply += decTotalAmountB_Temp.ToString("#,0");
            }
            return strHtml.ToString();
        }

        protected void btnShowDetail_Click(object sender, EventArgs e)
        {
            var strOrderNo = this.hidOrderNo.Value;
            this.Page.Response.Redirect("~/WebPage/ContractOrderDetail.aspx?No=" + strOrderNo);

        }

        protected void btnApplyAmount_Click(object sender, EventArgs e)
        {
            this.Page.Response.Redirect("~/WebPage/ApplyDeposit.aspx");
        }

        protected void btnApplyStatus_Click(object sender, EventArgs e)
        {
            this.Page.Response.Redirect("~/WebPage/ApplyDepositDetail.aspx");
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            var dataUser = this.GetCurrentUser;
            if (dataUser != null)
            {

                var strCustID = dataUser.CUST_ID == null ? "" : dataUser.CUST_ID.Value.ConvertToString();
                this.BindContractData(strCustID);

            }

        }
    }
}