﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgentCustomer.Logic;
using AgentCustomer.Logic.BusinessLogic;
using AgentCustomer.Utility;
using AgentCustomer.Model;
using AgentCustomer.Model.Table;
using System.Text.RegularExpressions;

namespace AgentCustomer.WebPage
{
    public partial class ChangePWD : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Page.IsPostBack == false)
            {
                if (Session["_User"] == null)
                {
                    this.Page.Response.Redirect("~/Customer_Login.aspx");
                    return;
                }
                else
                {
                    if ((Session["_User"] as BL_CUST).IS_NEED_CHANGE_PWD.ConvertToString().Trim() == "Y")
                    {
                        this.Page.RegisterClientScriptBlock("_Alert", "<script>alert('首次登入, 請先重設密碼');</script>");
                    }
                }
            }
        }

        protected void btnChangePWD_Click(object sender, EventArgs e)
        {
            if (this.txtPassword.Text != (Session["_User"] as BL_CUST).WEB_PASSWORD.ConvertToString())
            {
                var strScript = "<script> alert('密碼錯誤') </script>";
                this.Page.RegisterClientScriptBlock("Alert", strScript);
                return;
            }

            if (this.txtPasswordNew1.Text != this.txtPasswordNew2.Text)
            {
                var strScript = "<script> alert('請確認新密碼是否正確') </script>";
                this.Page.RegisterClientScriptBlock("Alert", strScript);
                return;
            }

            var hasNumber = new Regex(@"[0-9]+");
            var hasUpperChar = new Regex(@"[A-Z]+");
            var hasLowerChar = new Regex(@"[a-z]+");
            var isValidated = hasNumber.IsMatch(this.txtPasswordNew1.Text) &&
                (hasUpperChar.IsMatch(this.txtPasswordNew1.Text) || hasLowerChar.IsMatch(this.txtPasswordNew1.Text));
            //  有數字 + 任一英文字即可, 且必需 > 6位數
            if (isValidated == false || this.txtPasswordNew1.Text.Length < 6)
            {
                var strScript = "<script> alert('請確認新密碼是否符合規則(密碼需為英文+數字，至少6碼)') </script>";
                this.Page.RegisterClientScriptBlock("Alert", strScript);
                return;
            }




            if (new LogicCustomer().ChangePWD((Session["_User"] as BL_CUST).CUST_ID.Value, this.txtPasswordNew1.Text))
            {
                (Session["_User"] as BL_CUST).WEB_PASSWORD = this.txtPasswordNew1.Text;
                (Session["_User"] as BL_CUST).IS_NEED_CHANGE_PWD = "N";
                var strScript =
                    "<script>" +
                    "alert('密碼修改成功');" +
                    //" window.location.href = '" + this.Request.Url.AbsolutePath.ToLower().Replace("ChangePWD.aspx", "login.aspx") + "' ; " +
                    "</script>";
                this.Page.RegisterClientScriptBlock("Alert", strScript);
                //this.Page.Response.Redirect("~/LOGIN.aspx");
                this.Page.Response.Redirect("~/WebPage/ContractInfo.aspx");
                return;
            }
            else
            {
                var strScript = "<script> alert('更新失敗') </script>";
                this.Page.RegisterClientScriptBlock("Alert", strScript);
                return;
            }
        


        }
    }
}