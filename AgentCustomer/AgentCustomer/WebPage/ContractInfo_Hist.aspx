﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Theme/Theme.Master" AutoEventWireup="true" CodeBehind="ContractInfo_Hist.aspx.cs" Inherits="AgentCustomer.WebPage.ContractInfo_Hist" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .card-text span {
            color: black;
            font-size: 20px;
        }

        .th-Header {
            text-align: center;
        }
        .td-Text {
            text-align: left;
        }
        .td-Numeric {
            text-align: right;
        }


        button.Card-Main {
            padding: 12px 8px;
            margin: 2px;
            min-width: 120px !important;
        }
            button.Card-Main h3 {
                font-size: 18px !important;
            }


        @media (min-width: 500px) {
            button.Card-Main {
                margin: 12px !important;
            }
        }

        .table th {
            font-size: 15px;
        }
        .table td {
            font-size: 14px;
        }
        .table tfoot td {
            font-weight: bold;
        }

        
        .nav-link.active {
            border-color: #FFF #faf6fb #ce75b4 !important;
            border-bottom-width: 3px;
        }
        .tab-content {
            padding-top: 0rem;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="section light-bg" id="gallery">
        <div class="container">
            

            <div class="section-title">
                <%--<small>合約帳戶</small>--%>
                <h3>您的到期合約</h3>
            </div>
            
            <div id="divContractData" runat="server">

            </div>

        </div>
    </div>

    

    <div id="divSubmit" style="display:none; text-align:center; padding-top:13px;" title="">
        <div class="container">
            
            <div class="section-title" style="margin-bottom:0px;">
                <h3>紅利提領</h3>
            </div>

            <div class="row">
                <h4 class="card-title" style="padding-right:12px;">本金</h4>
                <p class="card-text"><asp:Label ID="lblContractAmount" runat="server" Text=""></asp:Label></p>
            </div>
            <div class="row">
                <h4 class="card-title" style="padding-right:12px;">餘額</h4>
                <p class="card-text"><asp:Label ID="lblBalance" runat="server" Text=""></asp:Label></p>
            </div>
            <div class="row">
                <h4 class="card-title" style="padding-right:12px;">提領金額</h4>
                <p class="card-text"><asp:textbox ID="txtAmountOut" runat="server" Text=""></asp:textbox></p>
            </div>

            <div id="divError" style="color:red;">

            </div>
        </div>
    </div>
    
    <div id="divSubmit2" style="display:none; text-align:center; padding-top:13px;" title="">
        <div class="container">
            
            <div class="section-title" style="margin-bottom:0px;">
                <h3>線上紅利提領申請<br />尚未開放</h3>
            </div>
        </div>
    </div>
    
    <div id="divDetail" style="display:none; text-align:center; padding-top:13px;overflow-y:auto;" title="">
        <div class="container">
            <div class="section-title" style="margin-bottom:0px;" id="divDetailData">
                
            </div>
        </div>
    </div>
    

    <asp:HiddenField ID="hidOrderNo" runat="server" />
    <asp:HiddenField ID="hidAmountOut" runat="server" />
    <asp:Button ID="btnShowDetail" runat="server" Text="" style=" width:0px; display:none;" OnClick="btnShowDetail_Click" />

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="server">
    
    <script>
        function scrolify(tblAsJQueryObject, height) {
            var oTbl = tblAsJQueryObject;

            // for very large tables you can remove the four lines below
            // and wrap the table with <div> in the mark-up and assign
            // height and overflow property  
            var oTblDiv = $("<div/>");
            oTblDiv.css('height', height);
            //oTblDiv.css('overflow', 'scroll');
            oTblDiv.css('overflow-y', 'auto');
            oTbl.wrap(oTblDiv);

            // save original width
            oTbl.attr("data-item-original-width", oTbl.width());
            oTbl.find('thead tr td').each(function () {
                $(this).attr("data-item-original-width", $(this).width());
            });
            oTbl.find('tbody tr:eq(0) td').each(function () {
                $(this).attr("data-item-original-width", $(this).width());
            });


            // clone the original table
            var newTbl = oTbl.clone();

            // remove table header from original table
            oTbl.find('thead tr').remove();
            // remove table body from new table
            newTbl.find('tbody tr').remove();

            oTbl.parent().parent().prepend(newTbl);
            newTbl.wrap("<div/>");

            // replace ORIGINAL COLUMN width				
            newTbl.width(newTbl.attr('data-item-original-width'));
            newTbl.find('thead tr td').each(function () {
                $(this).width($(this).attr("data-item-original-width"));
            });
            oTbl.width(oTbl.attr('data-item-original-width'));
            oTbl.find('tbody tr:eq(0) td').each(function () {
                $(this).width($(this).attr("data-item-original-width"));
            });
        }

        $(document).ready(function () {
            scrolify($('#tb1'), 520); // 320 is height
        });

        function openDepositOut(strOrderNo, strContractAmount, strBalance) {
            $("#<%=this.hidOrderNo.ClientID%>").val(strOrderNo);
            $("#<%=this.lblContractAmount.ClientID%>").text(strContractAmount);
            $("#<%=this.lblBalance.ClientID%>").text(strBalance);
            $("#divError").html("").css("display", "none");
            $("#<%=this.hidAmountOut.ClientID%>").val("");
            $("#<%=this.txtAmountOut.ClientID%>").val("");
            
            $("#divSubmit2").dialog({
                height: 290,
                width: 400,
                modal: true,
                resizable: true,
                dialogClass: "",
                buttons:
                {
                    '確認': function () {
                        $(this).dialog('close');
                    }
                }
            });


            return;
            $("#divSubmit").dialog({
                height: 290,
                width: 360,
                modal: true,
                resizable: true,
                dialogClass: "",
                buttons:
                {
                    '確認': function () {
                        var amountContract = parseInt($("#<%=this.lblContractAmount.ClientID%>").text());
                        var amountBalance = parseInt($("#<%=this.lblBalance.ClientID%>").text());
                        var amountOut = parseInt($("#<%=this.txtAmountOut.ClientID%>").val());

                        console.log("amountContract:" + amountContract + ";amountBalance:" + amountBalance + ";amountOut:" + amountOut);
                        if (amountOut > amountBalance - amountContract) {
                            $("#divError").html("提領申請金額不能大於「餘額-本金」").css("display", "block");
                        } else {
                            $("#<%=this.hidAmountOut.ClientID%>").val(amountOut);
                            //  Todo > do submit
                            $(this).dialog('close');

                            $("<div>申請送出成功</div>").dialog();
                        }
                    },
                    '取消': function () {
                        $(this).dialog('close');
                    }
                }
            });
          
        }


        
        function openOrderDateil(dataOrder) {
          
            var strOrderNo = dataOrder[0].ORDER_NO;
            $("#<%=this.hidOrderNo.ClientID%>").val(strOrderNo);
            $("#<%=this.btnShowDetail.ClientID%>").click();
            return;

            $("#divDetailData").html("");

            var dataHeader = $("<h5>")
              .html("合約編號：" + dataOrder[0].ORDER_NO)
              .css("text-align", "left");
            $("#divDetailData").append(dataHeader);

            dataHeader = $("<h5>")
              .html("幣別：" + dataOrder[0].GetCURRENCY)
              .css("text-align", "left");
            $("#divDetailData").append(dataHeader);

            dataHeader = $("<h5>")
              .html("本金：" + dataOrder[0].GetBASE_AMOUNT)
              .css("text-align", "left");
            $("#divDetailData").append(dataHeader);


            dataHeader = $("<h5>")
                .html("合約起始日：" + dataOrder[0].GetFROM_DATE)
                .css("text-align", "left");
            $("#divDetailData").append(dataHeader);

            dataHeader = $("<h5>")
                .html("合約結束日：" + dataOrder[0].GetEND_DATE)
                .css("text-align", "left");
            $("#divDetailData").append(dataHeader);


            var dataTable = $("<table>")
                .addClass("table").addClass("rwdtable")
                .append
                (
                    $("<thead>")
                        .addClass("thead-light")
                        .append
                        (
                            $("<tr>")
                                .append($("<th>").html("出/入"))
                                .append($("<th>").html("交易類別"))
                                .append($("<th>").html("派息次數"))
                                .append($("<th>").html("交易日"))
                                .append($("<th>").html("金額"))
                        )
                );
            var dataBody = $("<tbody>");
            
            $.each(dataOrder, function (i, data) {
                $(dataBody)
                    .append
                    (
                        $("<tr>")

                            .append($("<td>").attr("data-label", "出/入").html(data.FLOW_TYPE))
                            .append($("<td>").attr("data-label", "交易類別").html(data.TXN_TYPE))
                            .append($("<td>").attr("data-label", "派息次數").html(data.INTEREST_TIMES))
                            .append($("<td>").attr("data-label", "交易日").html(data.GetTXN_DATE))
                            .append($("<td>").attr("data-label", "金額").html(data.AMOUNT))
                    )
            });

            $(dataTable).append(dataBody);
            $("#divDetailData").append(dataTable);


            var iWidth = 360;
            if ($(window).width() <= 500 && $(window).width() >= 450) {
                iWidth = 440;
            }

            if ($(window).width() <= 600 && $(window).width() > 500) {
                iWidth = 480;
            }

            if ($(window).width() >= 600) {
                iWidth = 600;
            }

            $("#divDetail").dialog({
                height: 420,
                width: iWidth,
                modal: true,
                resizable: true,
                dialogClass: "",
                buttons:
                {
                    '確認': function () {
                        $(this).dialog('close');
                    },
                    '關閉': function () {
                        $(this).dialog('close');
                    }
                }
            });
          
        }
    </script>

</asp:Content>
