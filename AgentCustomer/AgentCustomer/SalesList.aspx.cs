﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AgentCustomer.Logic;
using AgentCustomer.Logic.BusinessLogic;
using AgentCustomer.Utility;
using AgentCustomer.Model;
using AgentCustomer.Model.Table;
using System.Text;

namespace AgentCustomer
{
    public partial class SalesList : System.Web.UI.Page
    {

        private List<BL_CUST> CustList
        {
            get
            {
                if (this.ViewState["_CustList"] == null)
                {
                    return new List<BL_CUST>();
                }
                else
                {
                    return (List<BL_CUST>)this.ViewState["_CustList"];
                }
            }
            set
            {
                this.ViewState["_CustList"] = value;
            }
        }

        private bool IsAdmin
        {
            get
            {
                if (Session["_IsAdmin"].ConvertToString().Trim() == "Y")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Page.IsPostBack == false)
            {
                if (this.Session["_SalesList"] != null)
                {
                    this.CustList = (List<BL_CUST>)this.Session["_SalesList"];
                }
                else
                {
                    this.CustList =  new List<BL_CUST>();
                }
                
                this.BindCustData();
            }
        }

        protected void BindCustData()
        {
            this.divCustList.InnerHtml = string.Empty;
            this.CustList = this.CustList.Where(x => x.CUST_ID != null).ToList();
            var strHtml = new StringBuilder();
            if (this.CustList.Count() > 0)
            {
                strHtml.AppendLine("<table class='table rwdtable' id='tb1'> ");
                strHtml.AppendLine(" <thead class='thead-light'> ");
                strHtml.AppendLine("  <tr>");
                //strHtml.AppendLine("   <th class='th-Header'>Detail</th> ");
                if (this.IsAdmin)
                {
                    strHtml.AppendLine("   <th class='th-Header'>業務姓名</th> ");
                }
                strHtml.AppendLine("   <th class='th-Header'>客戶姓名</th> ");
                strHtml.AppendLine("   <th class='th-Header'>身分證號</th> ");
                strHtml.AppendLine("   <th class='th-Header'>合約數</th> ");
                strHtml.AppendLine("  </tr>");
                strHtml.AppendLine(" </thead>");
                strHtml.AppendLine(" <tbody>");

                this.CustList
                    .OrderBy(x => x.CUST_CNAME)
                    .OrderBy(x => x.CUST_CNAME2)
                    .Where
                    (
                        x =>
                            this.txtKeyWord.Text.Trim() == string.Empty ||
                            x.CUST_CNAME.ConvertToString().ToUpper().IndexOf(this.txtKeyWord.Text.Trim().ToUpper()) != -1 ||
                            x.CUST_CNAME2.ConvertToString().ToUpper().IndexOf(this.txtKeyWord.Text.Trim().ToUpper()) != -1 ||
                            x.GetUnionID_NUMBER.ConvertToString().ToUpper().IndexOf(this.txtKeyWord.Text.Trim().ToUpper()) != -1 ||
                            (
                                //  Admin 可以同時搜尋業務姓名
                                this.IsAdmin && 
                                x.SALES_NAME.ConvertToString().ToUpper().IndexOf(this.txtKeyWord.Text.Trim().ToUpper()) != -1
                            )
                    )
                    .ToList()
                    .ForEach(x =>
                    {
                        strHtml.AppendLine("  <tr>");
                        var strUrl = this.Request.Url.AbsolutePath.Replace("SalesList.aspx", "SalesCust.aspx");
                        strUrl += "?ID=" + (x.CUST_ID == null ? "" : x.CUST_ID.Value.ConvertToString());
                        var strTarget = " window.open(\"" + strUrl + "\", \"TargetCust\", \"\", \"\"); ";

                        //strHtml.AppendLine("   <td data-label='Detail' class='td-Text'>" + "<a href='#' onclick='" + strTarget + "'>Open</a>" + "</td> ");
                        if (this.IsAdmin)
                        {
                            strHtml.AppendLine("   <td data-label='業務姓名' class='td-Text'>" + x.SALES_NAME.ConvertToString() + "</td> ");
                        }
                        strHtml.AppendLine("   <td data-label='客戶姓名' class='td-Text'>" + "<a href='#' onclick='" + strTarget + "'>" + x.GetUnionCustName.ConvertToString() + "</a>" + "</td> ");
                        strHtml.AppendLine("   <td data-label='身分證號' class='td-Text'>" + "<a href='#' onclick='" + strTarget + "'>" + x.GetUnionID_NUMBER.ConvertToString() + "</a>" + "</td> ");
                        strHtml.AppendLine("   <td data-label='合約數' class='td-Text'>" + (x.ORDER_NUMBER == null ? "0" : x.ORDER_NUMBER.Value.ConvertToString()) + "</td> ");

                        strHtml.AppendLine("  </tr>");
                    });

                strHtml.AppendLine(" </tbody>");
                strHtml.AppendLine("</table>");
            }
            else
            {
                strHtml.AppendLine("<h3 style='font-size:23px;'>查無客戶資料</h3>");
            }
            this.divCustList.InnerHtml = strHtml.ConvertToString();
            
        }



        protected void btnSearch_Click(object sender, EventArgs e)
        {
            this.BindCustData();
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            this.Session.Abandon();
            this.Page.Response.Redirect("~/Agent_Login.aspx");
        }
    }
}