﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SalesList.aspx.cs" Inherits="AgentCustomer.SalesList" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="Mobland - Mobile App Landing Page Template" />
    <meta name="keywords" content="HTML5, bootstrap, mobile, app, landing, ios, android, responsive" />

    <!-- Font -->
    <link rel="dns-prefetch" href="//fonts.googleapis.com" />
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="./Content/bootstrap.min.css" />
    <!-- Themify Icons -->
    <link rel="stylesheet" href="./Content/themify-icons.css" />
    <!-- Owl carousel -->
    <link rel="stylesheet" href="./Content/owl.carousel.min.css" />
    <!-- Main css -->
    <link href="./Content/style.css" rel="stylesheet" />

    <link href="./Content/jquery-ui-1.12.1.custom/jquery-ui.theme.css" rel="stylesheet" />

     <style>

         .th-Header {
             /*text-align: center;*/
             text-align: left;
         }
        .td-Text {
            text-align: left;
        }
        .td-Numeric {
            text-align: right;
        }
        
        .td-Center {
            text-align: center;
        }

        .table th {
            font-size: 15px;
        }
        .table td {
            font-size: 14px;
        }
        .table tfoot td {
            font-weight: bold;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    
        <div class="section light-bg" id="gallery">
            <div class="container">
                <div class="section-title" >
                    <div class="row">
  <div class="col-md-11"> <h3>您的客戶清單</h3></div>
  <div class="col-md-1">
      <asp:LinkButton ID="LinkButton1" runat="server" Font-Underline="True" OnClick="LinkButton1_Click">登出</asp:LinkButton>
  </div>
</div>
                   
                   
                    <div class="row" style="padding-top:7px; padding-bottom:7px;">
                        <div class="col-md-2">
                            關鍵字查詢：
                        </div>
                        <div class="col-md-9">
                            <asp:TextBox ID="txtKeyWord" runat="server" style="width:99%;" ></asp:TextBox>
                        </div>
                        <div class="col-md-1">
                            <asp:Button ID="btnSearch" runat="server" Text="查詢" OnClick="btnSearch_Click" />
                        </div>
                    </div>

                    <div id="divCustList" runat="server">

                    </div>
                </div>

            </div>

        </div>


        <!-- jQuery and Bootstrap -->
        
        <script src="./scripts/jquery-3.3.1.min.js"></script>
    
        <script src="./scripts/bootstrap.bundle.min.js"></script>
        <!-- Plugins JS -->
        <script src="./scripts/owl.carousel.min.js"></script>
        <!-- Custom JS -->
        <script src="./scripts/script.js"></script>
    
        <script src="./Content/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>

    </form>
</body>
</html>
