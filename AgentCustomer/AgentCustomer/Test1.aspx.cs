﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using AgentCustomer.Logic.BusinessLogic;

namespace AgentCustomer
{
    public partial class Test1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                //ReportViewer1.ProcessingMode = ProcessingMode.Local;
                //ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Report/rptApplyDeposit_RequestForm.rdlc");
                ////Customers dsCustomers = GetData("SELECT TOP 10 * FROM Customers");
                ////ReportDataSource datasource = new ReportDataSource("Customers", dsCustomers.Tables[0]);
                ////ReportViewer1.LocalReport.DataSources.Clear();
                ////ReportViewer1.LocalReport.DataSources.Add(datasource);

                //Warning[] warnings;
                //string[] streamIds;
                //string contentType;
                //string encoding;
                //string extension;

                ////Export the RDLC Report to Byte Array.
                //byte[] bytes = ReportViewer1.LocalReport.Render("PDF", null, out contentType, out encoding, out extension, out streamIds, out warnings);

                ////Download the RDLC Report in Word, Excel, PDF and Image formats.
                //Response.Clear();
                //Response.Buffer = true;
                //Response.Charset = "";
                //Response.Cache.SetCacheability(HttpCacheability.NoCache);
                //Response.ContentType = contentType;
                //Response.AppendHeader("Content-Disposition", "attachment; filename=RDLC." + extension);
                //Response.BinaryWrite(bytes);
                //Response.Flush();
                //Response.End();



                //var dataApply = new LogicCustomer().GetApplyDeposit_RequestForm_Data(101902);
                var dataApply = new LogicCustomer().GetApplyDeposit_RequestForm_Data(103125);
                //var dataApply = new LogicCustomer().GetApplyDeposit_RequestForm_Data(103123);
                
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Report/rptApplyDeposit_RequestForm.rdlc");
                ReportDataSource datasource = new ReportDataSource("dsReport", dataApply);
                ReportViewer1.LocalReport.DataSources.Clear();
                ReportViewer1.LocalReport.DataSources.Add(datasource);

                ////  Todo > 2019/09/07 VPN無法測試夾檔
                //Warning[] warnings;
                //string[] streamIds;
                //string contentType;
                //string encoding;
                //string extension;

                ////Export the RDLC Report to Byte Array.
                //byte[] bytes = ReportViewer1.LocalReport.Render("PDF", null, out contentType, out encoding, out extension, out streamIds, out warnings);

                ////Download the RDLC Report in Word, Excel, PDF and Image formats.
                //Response.Clear();
                //Response.Buffer = true;
                //Response.Charset = "";
                //Response.Cache.SetCacheability(HttpCacheability.NoCache);
                //Response.ContentType = contentType;
                //Response.AppendHeader("Content-Disposition", "attachment; filename=" + DateTime.Now.ToString("yyyyMMdd") + "." + extension);
                //Response.BinaryWrite(bytes);
                //Response.Flush();
                //Response.End();

            }
        }
    }
}