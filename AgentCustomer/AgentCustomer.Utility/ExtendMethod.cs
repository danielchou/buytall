﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgentCustomer.Utility
{
    public static class ExtendMethod
    {

        public static string ConvertToString(this object obj)
        {
            var result = obj ?? string.Empty;
            return result.ToString();
        }

        public static bool IsDate(this object obj)
        {
            DateTime dtTry;
            return DateTime.TryParse(obj.ConvertToString(), out dtTry);
        }

        public static bool IsNumeric(this object obj)
        {
            decimal decTry;
            return decimal.TryParse(obj.ConvertToString(), out decTry);
        }

        public static string ConvertToDatePickerString(this DateTime dt)
        {
            return dt.ToString("yyyy-MM-dd");
        }
        public static string ConvertToDatePickerString(this DateTime? dt)
        {
            if (dt == null)
            {
                return string.Empty;
            }
            else {
                return dt.Value.ToString("yyyy-MM-dd");
            }
        }

        /// <summary>
        /// Convert Value To Long and Check Is Valide
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="HasValue"></param>
        /// <returns></returns>
        public static long TryGetLongValue(this object obj, out bool HasValue)
        {
            HasValue = true;
            long lnTry;
            if (long.TryParse(obj.ConvertToString(), out lnTry))
            {
                return lnTry;
            }
            else
            {
                HasValue = false;
                return 0;
            }
        }

        //public static ShowMsg(System.Web.UI.Page form)

        /*

          public static TEnum GetDefaultValue<TEnum>() where TEnum : struct
    {
        Type t = typeof(TEnum);
        DefaultValueAttribute[] attributes = (DefaultValueAttribute[])t.GetCustomAttributes(typeof(DefaultValueAttribute), false);
        if (attributes != null &&
            attributes.Length > 0)
        {
            return (TEnum)attributes[0].Value;
        }
        else
        {
            return default(TEnum);
        }
    }

    */



    }
}
