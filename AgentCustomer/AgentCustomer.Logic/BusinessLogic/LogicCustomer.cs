﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;
using AgentCustomer.Data;
using AgentCustomer.Data.Class;
using AgentCustomer.Model.Data;
using AgentCustomer.Model.Table;
using AgentCustomer.Model.View;
using AgentCustomer.Utility;

namespace AgentCustomer.Logic.BusinessLogic
{
    public class LogicCustomer
    {

        public BL_CUST GetCustDataByCustEName(string CUST_ENAME)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(DataUtility.GetAgentConnectionString))
            {
                var data = new Da_BL_CUST(cn).GetCustDataByCustEName(CUST_ENAME, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public BL_CUST GetCustDataByCustID_NO(string ID_NO)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(DataUtility.GetAgentConnectionString))
            {
                var data = new Da_BL_CUST(cn).GetCustDataByCustID_NO(ID_NO, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public BL_CUST GetCustDataByCustID(string CustID)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(DataUtility.GetAgentConnectionString))
            {
                var data = new Da_BL_CUST(cn).GetCustDataByCustID(CustID, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public BL_CUST GetCustDataBySALES(string IB_CODE, string PWD, string CUST_ID)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(DataUtility.GetAgentConnectionString))
            {
                var data = new Da_BL_CUST(cn).GetCustDataBySALES(IB_CODE, PWD, CUST_ID, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public List<BL_CUST> GetCustDataBySALES(string IB_CODE, string PWD, bool IsAdmin = false)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(DataUtility.GetAgentConnectionString))
            {
                var data = new Da_BL_CUST(cn).GetCustDataBySALES(IB_CODE, PWD, IsAdmin, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        //public bool GetUnionCustomer(string account,out string cust_id)
        //{
        //    Exception exError = null;
        //    bool check = false;
        //    using (var cn = new OracleConnection(DataUtility.GetAgentConnectionString))
        //    {
        //        var data = new Da_BL_CUST(cn).GetUnionCustomer(account,  out exError);
        //        if (exError != null) throw exError;
        //        if (data == null)
        //        {

        //        }
        //        //
        //    }
        //}


        public bool ChangePWD(decimal CUST_ID, string NewPwd)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(DataUtility.GetAgentConnectionString))
            {
                var data = new Da_BL_CUST(cn).ChangePWD(CUST_ID, NewPwd, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public bool ForgetPWD(decimal CUST_ID, string NewPwd)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(DataUtility.GetAgentConnectionString))
            {
                var data = new Da_BL_CUST(cn).ForgetPWD(CUST_ID, NewPwd, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public List<BL_CUST_BANK> GetBankDataByCustID(string strCust_ID)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(DataUtility.GetAgentConnectionString))
            {
                var data = new Da_BL_CUST_BANK(cn).GetDataByCustID(strCust_ID, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }


        public List<BL_ORDER> GetContractDataByCustID(string CustID, bool IsShowUnApproveData = false)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(DataUtility.GetAgentConnectionString))
            {
                var data = new Da_BL_ORDER(cn).GetDataByCustID(CustID, IsShowUnApproveData, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public List<BL_ORDER> GetChartDataByCustID(string CustID)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(DataUtility.GetAgentConnectionString))
            {
                var data = new Da_BL_ORDER(cn).GetChartDataByCustID(CustID, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public List<BL_ORDER> GetChartInterestDataByCustID(List<BL_ORDER> dataInput, string strCurrency)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(DataUtility.GetAgentConnectionString))
            {
                var data = new Da_BL_ORDER(cn).GetChartInterestDataByCustID(dataInput, strCurrency, out exError);
                if (exError != null) throw exError;

                ////  整理
                //if (data != null)
                //{
                //   data= data
                //        .OrderByDescending(x => x.YEAR)
                //        .ToList();
                //    data.ForEach(x =>
                //    {
                //        var xx = data.Where(y => y.YEAR <= x.YEAR).Sum(y => y.ACCUMULATE_INTEREST);
                //        x.ACCUMULATE_INTEREST = xx;
                //    });

                //    data = data
                //         .OrderBy(x => x.YEAR)
                //         .ToList();
                //}
                if (data != null)
                {
                    data = data
                         .OrderBy(x => x.YEAR)
                         .ToList();
                }
                return data;
            }
        }

        public BL_ORDER_V1 GetCustAccountAmountInfo(string CustID)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(DataUtility.GetAgentConnectionString))
            {
                var data = new Da_BL_ORDER(cn).GetCustAccountAmountInfo(CustID, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public List<BL_ACCOUNT_TXN_V1> GetOrderDataByORDER_NO(List<string> ORDER_NO)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(DataUtility.GetAgentConnectionString))
            {
                var data = new Da_BL_ACCOUNT_TXN_V1(cn).GetOrderDataByORDER_NO(ORDER_NO, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public List<BL_ACCOUNT_TXN_V1> GetDepositDataByCustID(string strCustID)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(DataUtility.GetAgentConnectionString))
            {
                var data = new Da_BL_ACCOUNT_TXN_V1(cn).GetDepositData(strCustID, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public List<BL_ACCOUNT_TXN_V1> GetDepositDataByCustID2(string strCustID)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(DataUtility.GetAgentConnectionString))
            {
                var data = new Da_BL_ACCOUNT_TXN_V1(cn).GetDepositData2(strCustID, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public List<BL_ACCOUNT_TXN_V1> GetDepositDataDetail2(string strBatchID, string strCustID)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(DataUtility.GetAgentConnectionString))
            {
                var data = new Da_BL_ACCOUNT_TXN_V1(cn).GetDepositDataDetail2(strBatchID, strCustID, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }


        public bool LoginLog(BL_CUST_LOGIN_LOG data)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(DataUtility.GetAgentConnectionString))
            {
                var result = new Da_BL_CUST_LOGIN_LOG(cn).LoginLog(data, out exError);
                if (exError != null) throw exError;
                return result;
            }
        }

        public bool UpdateVERIFICATIONCODE(string strCustID, string strVERIFICATIONCODE)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(DataUtility.GetAgentConnectionString))
            {
                var result = new Da_BL_CUST(cn).UpdateVERIFICATIONCODE(strCustID, strVERIFICATIONCODE, out exError);
                if (exError != null) throw exError;
                return result;
            }
        }

        public bool AddNewDepositOut(List<BL_ACCOUNT_TXN_TEMP> data, BL_CUST CUST)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(DataUtility.GetAgentConnectionString))
            {
                var result = new Da_BL_ACCOUNT_TXN_TEMP(cn).AddNew(data, CUST, out exError);
                if (exError != null) throw exError;
                return result;
            }
        }

        public bool DeleteTempDepositData(long ORDER_ID, long BATCH_WITHDRAWAL_ID)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(DataUtility.GetAgentConnectionString))
            {
                var result = new Da_BL_ACCOUNT_TXN_TEMP(cn).DeleteTempData(ORDER_ID, BATCH_WITHDRAWAL_ID, out exError);
                if (exError != null) throw exError;
                return result;
            }
        }

        public decimal? GetNextTempSN()
        {
            Exception exError = null;
            using (var cn = new OracleConnection(DataUtility.GetAgentConnectionString))
            {
                var result = new Da_BL_ACCOUNT_TXN_TEMP(cn).GetNextSN(out exError);
                if (exError != null) throw exError;
                return result;
            }
        }

        /// <summary>
        /// 取得網路出金的申請資料
        /// </summary>
        /// <param name="strCust_ID"></param>
        /// <returns></returns>
        public List<BL_ACCOUNT_TXN_TEMP> GetApplyTempData(string strCust_ID)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(DataUtility.GetAgentConnectionString))
            {
                var result = new Da_BL_ACCOUNT_TXN_TEMP(cn).GetApplyTempData(strCust_ID, out exError);
                if (exError != null) throw exError;
                return result;
            }
        }

        public List<BL_ACCOUNT_TXN_TEMP> GetApplyTempDataALLByCustID(string strCust_ID)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(DataUtility.GetAgentConnectionString))
            {
                var data = new Da_BL_ACCOUNT_TXN_TEMP(cn).GetApplyTempDataALLByCustID(strCust_ID, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public List<BL_ApplyDeposit_RequestForm> GetApplyDeposit_RequestForm_Data(long BatchSN)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(DataUtility.GetAgentConnectionString))
            {
                var data = new Da_BL_ApplyDeposit_RequestForm(cn).GetApplyDeposit_RequestForm_Data(BatchSN, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public List<BL_ACCOUNT_TXN_TEMP> GetOrderTempDataByORDER_NO(List<string> ORDER_NO)
        {
            Exception exError = null;
            using (var cn=new OracleConnection(DataUtility.GetAgentConnectionString) )
            {
                var data = new Da_BL_ACCOUNT_TXN_TEMP(cn).GetOrderDataByORDER_NO(ORDER_NO, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }



    }
}
