﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;


namespace AgentCustomer.Data
{
    public class DataUtility
    {
        public static string GetAgentConnectionString
        {
            get
            {
                if (ConfigurationManager.ConnectionStrings["AgentConnectionString"] != null)
                {
                    return ConfigurationManager.ConnectionStrings["AgentConnectionString"].ConnectionString;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

    }
}
