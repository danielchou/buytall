﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Oracle.ManagedDataAccess.Client;
using AgentCustomer.Model;
using AgentCustomer.Model.Table;
using AgentCustomer.Utility;

namespace AgentCustomer.Data.Class
{
    public class Da_BL_CUST
    {
        private OracleConnection _cn;
        public Da_BL_CUST(OracleConnection cn)
        {
            this._cn = cn;
        }


        public BL_CUST GetCustDataByCustEName(string CUST_ENAME, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"

SELECT 
    A.CUST_ID,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.CUST_ENAME) CUST_ENAME,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.CUST_ENAME2) AS CUST_ENAME2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.CUST_CNAME) AS CUST_CNAME,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.CUST_CNAME2) AS CUST_CNAME2,
    A.PASSPORT,
    A.PASSPORT2,
    A.PASSPORT_REGION,
    A.PASSPORT_REGION2,
    A.PASSPORT_ACTIVE_END_DATE,
    A.PASSPORT_ACTIVE_END_DATE2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.ID_NUMBER) as ID_NUMBER,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.ID_NUMBER2) as ID_NUMBER2,
    A.SEX,
    A.SEX2,
    A.DATE_OF_BIRTH,
    A.DATE_OF_BIRTH2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.EMAIL_1) as EMAIL_1,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.EMAIL_2) as EMAIL_2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.EMAIL2_1) as EMAIL2_1 ,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.EMAIL2_2) as EMAIL2_2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.PHONE_1) as PHONE_1,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.PHONE_2) as PHONE_2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.PHONE2_1) as PHONE2_1,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.PHONE2_2) as PHONE2_2,
    A.POSTAL_CODE,
    A.POSTAL_CODE2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.C_ADDRESS) as C_ADDRESS,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.C_ADDRESS2) as C_ADDRESS2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.E_ADDRESS) as E_ADDRESS,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.E_ADDRESS2) as E_ADDRESS2,
    A.MAIL_TYPE_MONTHLY,
    A.MAIL_TYPE_END,
    A.CREATION_DATE,
    A.CREATED_BY,
    A.LAST_UPDATE_DATE,
    A.LAST_UPDATED_BY,
    A.CUST_CNAME_RAW,
    A.CUST_CNAME2_RAW,
    A.MT4_ACCOUNT,
    NVL(B.ORDER_CNT, 0) AS ORDER_CNT
    , D.CNAME AS SALES_NAME
    , A.WEB_PASSWORD
    , A.IS_NEED_CHANGE_PWD
FROM BL_CUST A
    LEFT JOIN
    (
        SELECT X.CUST_ID , COUNT(1) AS ORDER_CNT
        FROM BL_ORDER X
        WHERE X.STATUS_CODE = '4'
        GROUP BY X.CUST_ID
    )B
        ON A.CUST_ID = B.CUST_ID
    LEFT JOIN BL_CUST_SALES C
        ON A.CUST_ID = C.CUST_ID
            AND (C.ACTIVE IS NULL OR C.ACTIVE = 'Y' )   --AND C.ACTIVE <> 'N'  AND C.ACTIVE <> 'N'
    LEFT JOIN BL_EMP D
        ON C.IB_CODE = D.IB_CODE
WHERE BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.CUST_ENAME) = :CUST_ENAME
";
                return this._cn.Query<BL_CUST>(strSQL, new
                {
                    CUST_ENAME = CUST_ENAME
                }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }



        public BL_CUST GetCustDataByCustID_NO(string ID_NO, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"

SELECT 
    A.CUST_ID,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.CUST_ENAME) CUST_ENAME,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.CUST_ENAME2) AS CUST_ENAME2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.CUST_CNAME) AS CUST_CNAME,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.CUST_CNAME2) AS CUST_CNAME2,
    A.PASSPORT,
    A.PASSPORT2,
    A.PASSPORT_REGION,
    A.PASSPORT_REGION2,
    A.PASSPORT_ACTIVE_END_DATE,
    A.PASSPORT_ACTIVE_END_DATE2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.ID_NUMBER) as ID_NUMBER,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.ID_NUMBER2) as ID_NUMBER2,
    A.SEX,
    A.SEX2,
    A.DATE_OF_BIRTH,
    A.DATE_OF_BIRTH2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.EMAIL_1) as EMAIL_1,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.EMAIL_2) as EMAIL_2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.EMAIL2_1) as EMAIL2_1 ,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.EMAIL2_2) as EMAIL2_2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.PHONE_1) as PHONE_1,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.PHONE_2) as PHONE_2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.PHONE2_1) as PHONE2_1,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.PHONE2_2) as PHONE2_2,
    A.POSTAL_CODE,
    A.POSTAL_CODE2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.C_ADDRESS) as C_ADDRESS,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.C_ADDRESS2) as C_ADDRESS2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.E_ADDRESS) as E_ADDRESS,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.E_ADDRESS2) as E_ADDRESS2,
    A.MAIL_TYPE_MONTHLY,
    A.MAIL_TYPE_END,
    A.CREATION_DATE,
    A.CREATED_BY,
    A.LAST_UPDATE_DATE,
    A.LAST_UPDATED_BY,
    A.CUST_CNAME_RAW,
    A.CUST_CNAME2_RAW,
    A.MT4_ACCOUNT,
    NVL(B.ORDER_CNT, 0) AS ORDER_CNT
    , NVL(B.ORDER_BALANCE, 0) AS ORDER_BALANCE
    , D.CNAME AS SALES_NAME
    , A.WEB_PASSWORD
    , A.IS_NEED_CHANGE_PWD

    -- Todo > 

    ---指標:合約數
    , BL_REPORT_PKG.ORDER_NUMBER(A.CUST_ID) AS ORDER_NUMBER
    --指標:總資產(含不同幣別)
    , BL_REPORT_PKG.TOTAL_ASSET(A.CUST_ID, SYSDATE) AS TOTAL_ASSET
    --指標:本金(含不同幣別)
    , BL_REPORT_PKG.TOTAL_BASE_AMOUNT(A.CUST_ID, SYSDATE) AS TOTAL_BASE_AMOUNT
    --指標:可提領金額(含不同幣別)
    , BL_REPORT_PKG.TOTAL_AVAIL_WITHDRAWAL_AMOUNT(A.CUST_ID, SYSDATE) AS TOTAL_AVAIL_WITHDRAWAL_AMOUNT
    --指標:歷史總派利
    , BL_REPORT_PKG.HISTORY_TOTAL_WITHDRAWAL(A.CUST_ID, SYSDATE) AS HISTORY_TOTAL_WITHDRAWAL
    --指標:累積報酬率
    , BL_REPORT_PKG.INVEST_DURATION(A.CUST_ID) AS INVEST_DURATION
    --, 0 AS INVEST_DURATION  --Todo > 這有問題

    , NVL(E.ORDER_BALANCE, 0) AS ORDER_BALANCE_UNAPPROVE
    , BL_REPORT_PKG.PROCESSING_ORDER_AMOUNT(A.CUST_ID) AS PROCESSING_ORDER_AMOUNT

    --  提領紅利(申請中)
    , BL_REPORT_PKG.TOTAL_APPLY_WITHDRAWAL_AMOUNT(A.CUST_ID, SYSDATE) AS TOTAL_APPLY_WITHDRAWAL_AMOUNT
    --  已到期未提領資產
    , BL_REPORT_PKG.EXPIRED_UN_WITHDRAWAL_ASSET(A.CUST_ID, SYSDATE) AS EXPIRED_UN_WITHDRAWAL_ASSET

/*
    ---指標:合約數
    , 0 AS ORDER_NUMBER
    --指標:總資產(含不同幣別)
    , 0 AS TOTAL_ASSET
    --指標:本金(含不同幣別)
    , 0 AS TOTAL_BASE_AMOUNT
    --指標:可提領金額(含不同幣別)
    , 0 AS TOTAL_AVAIL_WITHDRAWAL_AMOUNT
    --指標:歷史總派利
    , 0 AS HISTORY_TOTAL_WITHDRAWAL
*/   

    , A.UNION_ID_NUMBER
    , A.IS_ONLINE_WITHDRAWAL
FROM BL_CUST A
    LEFT JOIN
    (
        SELECT 
            X.CUST_ID 
            , COUNT(1) AS ORDER_CNT
            , SUM( BL_INTEREST_PKG.RETURN_BALANCE(X.ORDER_ID) ) AS ORDER_BALANCE
            --, SUM( 100000 ) AS ORDER_BALANCE
        FROM BL_ORDER X
        WHERE X.STATUS_CODE = '4'
            AND (X.END_DATE IS NULL OR X.END_DATE >= SYSDATE)
        GROUP BY X.CUST_ID
    )B
        ON A.CUST_ID = B.CUST_ID
    LEFT JOIN BL_CUST_SALES C
        ON A.CUST_ID = C.CUST_ID
           AND (C.ACTIVE IS NULL OR C.ACTIVE = 'Y' )   --AND C.ACTIVE <> 'N'   AND C.ACTIVE <> 'N'
    LEFT JOIN BL_EMP D
        ON C.IB_CODE = D.IB_CODE
    LEFT JOIN
    (
        SELECT 
            X.CUST_ID 
            , COUNT(1) AS ORDER_CNT
            , SUM
                (
                    NVL(X.AMOUNT_USD, NVL(X.AMOUNT_RMB, NVL(X.AMOUNT_NTD, 0) ))
                ) AS ORDER_BALANCE
            --, SUM( 100000 ) AS ORDER_BALANCE
        FROM BL_ORDER X
        WHERE X.STATUS_CODE = '3'
            AND (X.FROM_DATE <= SYSDATE)
        GROUP BY X.CUST_ID
    )E
        ON A.CUST_ID = E.CUST_ID
WHERE 1 = 1
    AND
    (
        ( BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.ID_NUMBER) = :ID_NO AND A.CUST_CNAME2 IS NULL )
        OR ( A.UNION_ID_NUMBER = :ID_NO AND A.CUST_CNAME2 IS NOT NULL )
    )
";



                return this._cn.Query<BL_CUST>(strSQL, new
                {
                    ID_NO = ID_NO.ToUpper()
                }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }



        public BL_CUST GetCustDataByCustID(string CustID, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"

SELECT 
    A.CUST_ID,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.CUST_ENAME) CUST_ENAME,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.CUST_ENAME2) AS CUST_ENAME2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.CUST_CNAME) AS CUST_CNAME,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.CUST_CNAME2) AS CUST_CNAME2,
    A.PASSPORT,
    A.PASSPORT2,
    A.PASSPORT_REGION,
    A.PASSPORT_REGION2,
    A.PASSPORT_ACTIVE_END_DATE,
    A.PASSPORT_ACTIVE_END_DATE2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.ID_NUMBER) as ID_NUMBER,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.ID_NUMBER2) as ID_NUMBER2,
    A.SEX,
    A.SEX2,
    A.DATE_OF_BIRTH,
    A.DATE_OF_BIRTH2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.EMAIL_1) as EMAIL_1,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.EMAIL_2) as EMAIL_2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.EMAIL2_1) as EMAIL2_1 ,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.EMAIL2_2) as EMAIL2_2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.PHONE_1) as PHONE_1,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.PHONE_2) as PHONE_2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.PHONE2_1) as PHONE2_1,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.PHONE2_2) as PHONE2_2,
    A.POSTAL_CODE,
    A.POSTAL_CODE2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.C_ADDRESS) as C_ADDRESS,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.C_ADDRESS2) as C_ADDRESS2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.E_ADDRESS) as E_ADDRESS,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.E_ADDRESS2) as E_ADDRESS2,
    A.MAIL_TYPE_MONTHLY,
    A.MAIL_TYPE_END,
    A.CREATION_DATE,
    A.CREATED_BY,
    A.LAST_UPDATE_DATE,
    A.LAST_UPDATED_BY,
    A.CUST_CNAME_RAW,
    A.CUST_CNAME2_RAW,
    A.MT4_ACCOUNT,
    NVL(B.ORDER_CNT, 0) AS ORDER_CNT
    , NVL(B.ORDER_BALANCE, 0) AS ORDER_BALANCE
    , D.CNAME AS SALES_NAME
    , A.WEB_PASSWORD
    , A.IS_NEED_CHANGE_PWD

    -- Todo > 

    ---指標:合約數
    , BL_REPORT_PKG.ORDER_NUMBER(A.CUST_ID) AS ORDER_NUMBER
    --指標:總資產(含不同幣別)
    , BL_REPORT_PKG.TOTAL_ASSET(A.CUST_ID, SYSDATE) AS TOTAL_ASSET
    --指標:本金(含不同幣別)
    , BL_REPORT_PKG.TOTAL_BASE_AMOUNT(A.CUST_ID, SYSDATE) AS TOTAL_BASE_AMOUNT
    --指標:可提領金額(含不同幣別)
    , BL_REPORT_PKG.TOTAL_AVAIL_WITHDRAWAL_AMOUNT(A.CUST_ID, SYSDATE) AS TOTAL_AVAIL_WITHDRAWAL_AMOUNT
    --指標:歷史總派利
    , BL_REPORT_PKG.HISTORY_TOTAL_WITHDRAWAL(A.CUST_ID, SYSDATE) AS HISTORY_TOTAL_WITHDRAWAL
    --指標:累積報酬率
    , BL_REPORT_PKG.INVEST_DURATION(A.CUST_ID) AS INVEST_DURATION
    --, 0 AS INVEST_DURATION  --Todo > 這有問題

    , NVL(E.ORDER_BALANCE, 0) AS ORDER_BALANCE_UNAPPROVE
    , BL_REPORT_PKG.PROCESSING_ORDER_AMOUNT(A.CUST_ID) AS PROCESSING_ORDER_AMOUNT

/*
    ---指標:合約數
    , 0 AS ORDER_NUMBER
    --指標:總資產(含不同幣別)
    , 0 AS TOTAL_ASSET
    --指標:本金(含不同幣別)
    , 0 AS TOTAL_BASE_AMOUNT
    --指標:可提領金額(含不同幣別)
    , 0 AS TOTAL_AVAIL_WITHDRAWAL_AMOUNT
    --指標:歷史總派利
    , 0 AS HISTORY_TOTAL_WITHDRAWAL
*/   

    , A.UNION_ID_NUMBER
    , D.IB_CODE AS SALES_IB_CODE
FROM BL_CUST A
    LEFT JOIN
    (
        SELECT 
            X.CUST_ID 
            , COUNT(1) AS ORDER_CNT
            , SUM( BL_INTEREST_PKG.RETURN_BALANCE(X.ORDER_ID) ) AS ORDER_BALANCE
            --, SUM( 100000 ) AS ORDER_BALANCE
        FROM BL_ORDER X
        WHERE X.STATUS_CODE = '4'
            AND (X.END_DATE IS NULL OR X.END_DATE >= SYSDATE)
        GROUP BY X.CUST_ID
    )B
        ON A.CUST_ID = B.CUST_ID
    LEFT JOIN BL_CUST_SALES C
        ON A.CUST_ID = C.CUST_ID
            AND (C.ACTIVE IS NULL OR C.ACTIVE = 'Y' )   --AND C.ACTIVE <> 'N'  AND C.ACTIVE <> 'N'
    LEFT JOIN BL_EMP D
        ON C.IB_CODE = D.IB_CODE
    LEFT JOIN
    (
        SELECT 
            X.CUST_ID 
            , COUNT(1) AS ORDER_CNT
            , SUM
                (
                    NVL(X.AMOUNT_USD, NVL(X.AMOUNT_RMB, NVL(X.AMOUNT_NTD, 0) ))
                ) AS ORDER_BALANCE
            --, SUM( 100000 ) AS ORDER_BALANCE
        FROM BL_ORDER X
        WHERE X.STATUS_CODE = '3'
            AND (X.FROM_DATE <= SYSDATE)
        GROUP BY X.CUST_ID
    )E
        ON A.CUST_ID = E.CUST_ID
WHERE 1 = 1
    AND A.CUST_ID = :CUST_ID
";

                return this._cn.Query<BL_CUST>(strSQL, new
                {
                    CUST_ID = CustID
                }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }



        public BL_CUST GetCustDataBySALES(string IB_CODE,string PWD,string CUST_ID, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"

SELECT 
    A.CUST_ID,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.CUST_ENAME) CUST_ENAME,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.CUST_ENAME2) AS CUST_ENAME2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.CUST_CNAME) AS CUST_CNAME,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.CUST_CNAME2) AS CUST_CNAME2,
    A.PASSPORT,
    A.PASSPORT2,
    A.PASSPORT_REGION,
    A.PASSPORT_REGION2,
    A.PASSPORT_ACTIVE_END_DATE,
    A.PASSPORT_ACTIVE_END_DATE2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.ID_NUMBER) as ID_NUMBER,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.ID_NUMBER2) as ID_NUMBER2,
    A.SEX,
    A.SEX2,
    A.DATE_OF_BIRTH,
    A.DATE_OF_BIRTH2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.EMAIL_1) as EMAIL_1,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.EMAIL_2) as EMAIL_2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.EMAIL2_1) as EMAIL2_1 ,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.EMAIL2_2) as EMAIL2_2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.PHONE_1) as PHONE_1,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.PHONE_2) as PHONE_2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.PHONE2_1) as PHONE2_1,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.PHONE2_2) as PHONE2_2,
    A.POSTAL_CODE,
    A.POSTAL_CODE2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.C_ADDRESS) as C_ADDRESS,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.C_ADDRESS2) as C_ADDRESS2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.E_ADDRESS) as E_ADDRESS,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.E_ADDRESS2) as E_ADDRESS2,
    A.MAIL_TYPE_MONTHLY,
    A.MAIL_TYPE_END,
    A.CREATION_DATE,
    A.CREATED_BY,
    A.LAST_UPDATE_DATE,
    A.LAST_UPDATED_BY,
    A.CUST_CNAME_RAW,
    A.CUST_CNAME2_RAW,
    A.MT4_ACCOUNT,
    NVL(B.ORDER_CNT, 0) AS ORDER_CNT
    , NVL(B.ORDER_BALANCE, 0) AS ORDER_BALANCE
    , D.CNAME AS SALES_NAME
    , A.WEB_PASSWORD
    , A.IS_NEED_CHANGE_PWD

    -- Todo > 

    ---指標:合約數
    , BL_REPORT_PKG.ORDER_NUMBER(A.CUST_ID) AS ORDER_NUMBER
    --指標:總資產(含不同幣別)
    , BL_REPORT_PKG.TOTAL_ASSET(A.CUST_ID, SYSDATE) AS TOTAL_ASSET
    --指標:本金(含不同幣別)
    , BL_REPORT_PKG.TOTAL_BASE_AMOUNT(A.CUST_ID, SYSDATE) AS TOTAL_BASE_AMOUNT
    --指標:可提領金額(含不同幣別)
    , BL_REPORT_PKG.TOTAL_AVAIL_WITHDRAWAL_AMOUNT(A.CUST_ID, SYSDATE) AS TOTAL_AVAIL_WITHDRAWAL_AMOUNT
    --指標:歷史總派利
    , BL_REPORT_PKG.HISTORY_TOTAL_WITHDRAWAL(A.CUST_ID, SYSDATE) AS HISTORY_TOTAL_WITHDRAWAL
    --指標:累積報酬率
    , BL_REPORT_PKG.INVEST_DURATION(A.CUST_ID) AS INVEST_DURATION
    --, 0 AS INVEST_DURATION  --Todo > 這有問題

/*
    ---指標:合約數
    , 0 AS ORDER_NUMBER
    --指標:總資產(含不同幣別)
    , 0 AS TOTAL_ASSET
    --指標:本金(含不同幣別)
    , 0 AS TOTAL_BASE_AMOUNT
    --指標:可提領金額(含不同幣別)
    , 0 AS TOTAL_AVAIL_WITHDRAWAL_AMOUNT
    --指標:歷史總派利
    , 0 AS HISTORY_TOTAL_WITHDRAWAL
*/   

    , A.UNION_ID_NUMBER
FROM BL_CUST A
    LEFT JOIN
    (
        SELECT 
            X.CUST_ID 
            , COUNT(1) AS ORDER_CNT
            , SUM( BL_INTEREST_PKG.RETURN_BALANCE(X.ORDER_ID) ) AS ORDER_BALANCE
            --, SUM( 100000 ) AS ORDER_BALANCE
        FROM BL_ORDER X
        WHERE X.STATUS_CODE = '4'
            AND (X.END_DATE IS NULL OR X.END_DATE >= SYSDATE)
        GROUP BY X.CUST_ID
    )B
        ON A.CUST_ID = B.CUST_ID
    INNER JOIN BL_CUST_SALES C
        ON A.CUST_ID = C.CUST_ID
            AND (C.ACTIVE IS NULL OR C.ACTIVE = 'Y' )   --AND C.ACTIVE <> 'N'  AND C.ACTIVE <> 'N'
    INNER JOIN BL_EMP D
        ON C.IB_CODE = D.IB_CODE
WHERE 1 = 1 AND (C.ACTIVE IS NULL OR C.ACTIVE='Y')
    --AND BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.ID_NUMBER) = :CUST_ID
    AND
    (
        ( BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.ID_NUMBER) = :CUST_ID AND A.CUST_CNAME2 IS NULL )
        OR ( A.UNION_ID_NUMBER = :CUST_ID AND A.CUST_CNAME2 IS NOT NULL )
    )
    AND D.IB_CODE = :IB_CODE
    AND D.SALES_LOGIN_PASSWORD = :PWD
";
               
                return this._cn.Query<BL_CUST>(strSQL, new
                {
                    IB_CODE = IB_CODE,
                    PWD = PWD,
                    CUST_ID = CUST_ID
                }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }


        public List<BL_CUST> GetCustDataBySALES(string IB_CODE, string PWD, bool IsAdmin, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"

SELECT 
    A.CUST_ID,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.CUST_ENAME) CUST_ENAME,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.CUST_ENAME2) AS CUST_ENAME2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.CUST_CNAME) AS CUST_CNAME,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.CUST_CNAME2) AS CUST_CNAME2,
    A.PASSPORT,
    A.PASSPORT2,
    A.PASSPORT_REGION,
    A.PASSPORT_REGION2,
    A.PASSPORT_ACTIVE_END_DATE,
    A.PASSPORT_ACTIVE_END_DATE2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.ID_NUMBER) as ID_NUMBER,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.ID_NUMBER2) as ID_NUMBER2,
    A.SEX,
    A.SEX2,
    A.DATE_OF_BIRTH,
    A.DATE_OF_BIRTH2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.EMAIL_1) as EMAIL_1,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.EMAIL_2) as EMAIL_2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.EMAIL2_1) as EMAIL2_1 ,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.EMAIL2_2) as EMAIL2_2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.PHONE_1) as PHONE_1,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.PHONE_2) as PHONE_2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.PHONE2_1) as PHONE2_1,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.PHONE2_2) as PHONE2_2,
    A.POSTAL_CODE,
    A.POSTAL_CODE2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.C_ADDRESS) as C_ADDRESS,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.C_ADDRESS2) as C_ADDRESS2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.E_ADDRESS) as E_ADDRESS,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.E_ADDRESS2) as E_ADDRESS2,
    A.MAIL_TYPE_MONTHLY,
    A.MAIL_TYPE_END,
    A.CREATION_DATE,
    A.CREATED_BY,
    A.LAST_UPDATE_DATE,
    A.LAST_UPDATED_BY,
    A.CUST_CNAME_RAW,
    A.CUST_CNAME2_RAW,
    A.MT4_ACCOUNT,
    NVL(B.ORDER_CNT, 0) AS ORDER_CNT
    , NVL(B.ORDER_BALANCE, 0) AS ORDER_BALANCE
    , D.CNAME AS SALES_NAME
    , A.WEB_PASSWORD
    , A.IS_NEED_CHANGE_PWD

    -- Todo > 

    ---指標:合約數
    , BL_REPORT_PKG.ORDER_NUMBER(A.CUST_ID) AS ORDER_NUMBER
    --指標:總資產(含不同幣別)
    , BL_REPORT_PKG.TOTAL_ASSET(A.CUST_ID, SYSDATE) AS TOTAL_ASSET
    --指標:本金(含不同幣別)
    , BL_REPORT_PKG.TOTAL_BASE_AMOUNT(A.CUST_ID, SYSDATE) AS TOTAL_BASE_AMOUNT
    --指標:可提領金額(含不同幣別)
    , BL_REPORT_PKG.TOTAL_AVAIL_WITHDRAWAL_AMOUNT(A.CUST_ID, SYSDATE) AS TOTAL_AVAIL_WITHDRAWAL_AMOUNT
    --指標:歷史總派利
    , BL_REPORT_PKG.HISTORY_TOTAL_WITHDRAWAL(A.CUST_ID, SYSDATE) AS HISTORY_TOTAL_WITHDRAWAL
    --指標:累積報酬率
    , BL_REPORT_PKG.INVEST_DURATION(A.CUST_ID) AS INVEST_DURATION
    --, 0 AS INVEST_DURATION  --Todo > 這有問題

/*
    ---指標:合約數
    , 0 AS ORDER_NUMBER
    --指標:總資產(含不同幣別)
    , 0 AS TOTAL_ASSET
    --指標:本金(含不同幣別)
    , 0 AS TOTAL_BASE_AMOUNT
    --指標:可提領金額(含不同幣別)
    , 0 AS TOTAL_AVAIL_WITHDRAWAL_AMOUNT
    --指標:歷史總派利
    , 0 AS HISTORY_TOTAL_WITHDRAWAL
*/   

    , A.UNION_ID_NUMBER

    , D.IB_CODE AS SALES_IB_CODE
FROM BL_CUST A
    LEFT JOIN
    (
        SELECT 
            X.CUST_ID 
            , COUNT(1) AS ORDER_CNT
            , SUM( BL_INTEREST_PKG.RETURN_BALANCE(X.ORDER_ID) ) AS ORDER_BALANCE
            --, SUM( 100000 ) AS ORDER_BALANCE
        FROM BL_ORDER X
        WHERE X.STATUS_CODE = '4'
            AND (X.END_DATE IS NULL OR X.END_DATE >= SYSDATE)
        GROUP BY X.CUST_ID
    )B
        ON A.CUST_ID = B.CUST_ID
    INNER JOIN BL_CUST_SALES C
        ON A.CUST_ID = C.CUST_ID
          AND (C.ACTIVE IS NULL OR C.ACTIVE = 'Y' )   --AND C.ACTIVE <> 'N'    AND C.ACTIVE <> 'N'
    INNER JOIN BL_EMP D
        ON C.IB_CODE = D.IB_CODE
WHERE 1 = 1 AND (C.ACTIVE IS NULL OR C.ACTIVE='Y')
";

                if (IsAdmin)
                {
                    return this._cn.Query<BL_CUST>(strSQL, null).ToList();
                }
                else
                {
                    strSQL += @"
    AND D.IB_CODE = :IB_CODE
    AND D.SALES_LOGIN_PASSWORD = :PWD
";
                    return this._cn.Query<BL_CUST>(strSQL, new
                    {
                        IB_CODE = IB_CODE.ToUpper(),
                        PWD = PWD
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }


        public bool ChangePWD(decimal CUST_ID, string NewPwd, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
UPDATE BL_CUST A
    SET
        A.WEB_PASSWORD = :NEW_PWD
        , A.IS_NEED_CHANGE_PWD = 'N'
WHERE A.CUST_ID = :CUST_ID

";
                this._cn.Execute(strSQL, new
                {
                    CUST_ID = CUST_ID,
                    NEW_PWD = NewPwd
                });
                return true;
            }
            catch (Exception ex)
            {
                exError = ex;
                return false;
            }
        }

        public bool ForgetPWD(decimal CUST_ID, string NewPwd, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
UPDATE BL_CUST A
    SET
        A.WEB_PASSWORD = :NEW_PWD
        , A.IS_NEED_CHANGE_PWD = 'Y'
WHERE A.CUST_ID = :CUST_ID

";
                this._cn.Execute(strSQL, new
                {
                    CUST_ID = CUST_ID,
                    NEW_PWD = NewPwd
                });
                return true;
            }
            catch (Exception ex)
            {
                exError = ex;
                return false;
            }
        }

        public BL_CUST GetUnionCustomer(string account, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
                 select union_id_number from  BR2.BL_CUST
                 where union_id_number is not null  and  union_id_number = :acc ";
                return this._cn.Query<BL_CUST>(strSQL, new
                {
                    account = account

                }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }


        public bool UpdateVERIFICATIONCODE(string strCustID, string strVERIFICATIONCODE, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
UPDATE BL_CUST A
SET
    A.VERIFICATIONCODE = :VERIFICATIONCODE
WHERE A.CUST_ID = :CUST_ID
";
                this._cn.Execute(strSQL, new
                {
                    CUST_ID = strCustID,
                    VERIFICATIONCODE = strVERIFICATIONCODE
                });
                return true;
            }
            catch (Exception ex)
            {
                exError = ex;
                return false;
            }
        }


    }
}
