﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Oracle.ManagedDataAccess.Client;
using AgentCustomer.Model;
using AgentCustomer.Model.Table;
using AgentCustomer.Model.View;
using AgentCustomer.Utility;


namespace AgentCustomer.Data.Class
{
    public class Da_BL_CUST_LOGIN_LOG
    {
        private OracleConnection _cn;
        public Da_BL_CUST_LOGIN_LOG(OracleConnection cn)
        {
            this._cn = cn;
        }

        public bool LoginLog(BL_CUST_LOGIN_LOG data, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
INSERT INTO BL_CUST_LOGIN_LOG
(
    BL_CUST_LOG_SN
    , LOGIN_DATETIME
    , LOGIN_IP
    , LOGIN_ACCOUNT
)
SELECT 
    BL_CUST_LOGIN_LOG_S1.NEXTVAL
    , :LOGIN_DATETIME
    , :LOGIN_IP
    , :LOGIN_ACCOUNT
FROM DUAL
";
                this._cn.ExecuteScalar(strSQL, new
                {
                    LOGIN_DATETIME = data.LOGIN_DATETIME,
                    LOGIN_IP = data.LOGIN_IP,
                    LOGIN_ACCOUNT = data.LOGIN_ACCOUNT
                });
                return true;
            }
            catch (Exception ex)
            {
                exError = ex;
                return false;
            }
        }

    }
}
