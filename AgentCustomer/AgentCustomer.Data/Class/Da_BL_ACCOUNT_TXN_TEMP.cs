﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Oracle.ManagedDataAccess.Client;
using AgentCustomer.Model;
using AgentCustomer.Model.Table;
using AgentCustomer.Utility;


namespace AgentCustomer.Data.Class
{

    public class Da_BL_ACCOUNT_TXN_TEMP
    {
        private OracleConnection _cn;
        public Da_BL_ACCOUNT_TXN_TEMP(OracleConnection cn)
        {
            this._cn = cn;
        }

        public bool AddNew(List<BL_ACCOUNT_TXN_TEMP> data, BL_CUST CUST, out Exception exError)
        {
            exError = null;
            try
            {
                var param = new DynamicParameters();
                var strSQL = string.Empty;
                int i = 0;

                data.ForEach(x =>
                {
                    strSQL += @"
                INSERT INTO BL_ACCOUNT_TXN_TEMP
                (
                      ORDER_ID
                    , AMOUNT_USD
                    , AMOUNT_NTD
                    , AMOUNT_RMB
                    , AMOUNT_EUR
                    , AMOUNT_AUD
                    , AMOUNT_JPY
                    , AMOUNT_NZD
                    , APPLY_DATE
                    , CHECK_DATE
                    , APPROVE_DATE
                    , CREATION_DATE
                    , LAST_UPDATE_DATE
                    , SCHEDULE
                    , BATCH_WITHDRAWAL_ID
                    , BONUS_TSF
                    , NEW_CONTRACT_NO
                    , WITHDRAWAL_TYPE
                    , PROCESS_TYPE
                    , END_BALANCE_FLAG
                    , APPLY_USER
                    ,  TXN_DATE
                    , FLOW_TYPE
                    , TXN_TYPE
                    , TEMP_ID
                    , ACTUAL_WITHDRAWAL_AMT_USD
                    , ACTUAL_WITHDRAWAL_AMT_NTD
                    , ACTUAL_WITHDRAWAL_AMT_RMB
                    , ACTUAL_WITHDRAWAL_AMT_EUR
                    , ACTUAL_WITHDRAWAL_AMT_AUD
                    , ACTUAL_WITHDRAWAL_AMT_JPY
                    , ACTUAL_WITHDRAWAL_AMT_NZD

                    , WEB_SUBMIT_SALES_IBCODE
                    , SALES_WEB_SUBMIT_DATETIME
                    , IS_SALES_SUBMIT_TEMP
                )
                SELECT 
                      A.ORDER_ID
                    , :AMOUNT_USD" + i.ConvertToString() + @"
                    , :AMOUNT_NTD" + i.ConvertToString() + @"
                    , :AMOUNT_RMB" + i.ConvertToString() + @"
                    , :AMOUNT_EUR" + i.ConvertToString() + @"
                    , :AMOUNT_AUD" + i.ConvertToString() + @"
                    , :AMOUNT_JPY" + i.ConvertToString() + @"
                    , :AMOUNT_NZD" + i.ConvertToString() + @"
                    , :APPLY_DATE" + i.ConvertToString() + @"
                    , :CHECK_DATE" + i.ConvertToString() + @"
                    , :APPROVE_DATE" + i.ConvertToString() + @"
                    , SYSDATE
                    , SYSDATE
                    , '" + (x.IS_SALES_SUBMIT_TEMP == "Y" ? "8" : "0") + @"'   --暫存
                    , :BATCH_WITHDRAWAL_ID" + i.ConvertToString() + @"
                    , :BONUS_TSF" + i.ConvertToString() + @"
                    , :NEW_CONTRACT_NO" + i.ConvertToString() + @"
                    , :WITHDRAWAL_TYPE" + i.ConvertToString() + @"
                    , :PROCESS_TYPE" + i.ConvertToString() + @"
                    , :END_BALANCE_FLAG" + i.ConvertToString() + @"
                    , :APPLY_USER" + i.ConvertToString() + @"
                    , :TXN_DATE" + i.ConvertToString() + @"
                    , 'OUT'
                    , 'WITHDRAWAL'
                    , BL_ACCOUNT_TXN_TEMP_S1.NEXTVAL
                    , :ACTUAL_WITHDRAWAL_AMT_USD" + i.ConvertToString() + @"
                    , :ACTUAL_WITHDRAWAL_AMT_NTD" + i.ConvertToString() + @"
                    , :ACTUAL_WITHDRAWAL_AMT_RMB" + i.ConvertToString() + @"
                    , :ACTUAL_WITHDRAWAL_AMT_EUR" + i.ConvertToString() + @"
                    , :ACTUAL_WITHDRAWAL_AMT_AUD" + i.ConvertToString() + @"
                    , :ACTUAL_WITHDRAWAL_AMT_JPY" + i.ConvertToString() + @"
                    , :ACTUAL_WITHDRAWAL_AMT_NZD" + i.ConvertToString() + @"

                    , :WEB_SUBMIT_SALES_IBCODE" + i.ConvertToString() + @"
                    , :SALES_WEB_SUBMIT_DATETIME" + i.ConvertToString() + @"
                    , :IS_SALES_SUBMIT_TEMP" + i.ConvertToString() + @"
                FROM BL_ORDER A
                    INNER JOIN BL_CUST B
                        ON A.CUST_ID = B.CUST_ID
                            AND A.STATUS_CODE NOT IN (-1)
                WHERE A.CUST_ID = :CUST_ID" + i.ConvertToString() + @"
                    AND A.ORDER_NO = :ORDER_NO" + i.ConvertToString() + @"
                ; 
                ";

                    param.Add("ORDER_ID" + i.ConvertToString(), x.ORDER_ID);
                    param.Add("AMOUNT_USD" + i.ConvertToString(), x.AMOUNT_USD);
                    param.Add("AMOUNT_NTD" + i.ConvertToString(), x.AMOUNT_NTD);
                    param.Add("AMOUNT_RMB" + i.ConvertToString(), x.AMOUNT_RMB);
                    param.Add("AMOUNT_EUR" + i.ConvertToString(), x.AMOUNT_EUR);
                    param.Add("AMOUNT_AUD" + i.ConvertToString(), x.AMOUNT_AUD);
                    param.Add("AMOUNT_JPY" + i.ConvertToString(), x.AMOUNT_JPY);
                    param.Add("AMOUNT_NZD" + i.ConvertToString(), x.AMOUNT_NZD);
                    param.Add("APPLY_DATE" + i.ConvertToString(), x.APPLY_DATE);
                    param.Add("CHECK_DATE" + i.ConvertToString(), x.CHECK_DATE);
                    param.Add("APPROVE_DATE" + i.ConvertToString(), x.APPROVE_DATE);
                    param.Add("BATCH_WITHDRAWAL_ID" + i.ConvertToString(), x.BATCH_WITHDRAWAL_ID);
                    param.Add("BONUS_TSF" + i.ConvertToString(), x.BONUS_TSF);
                    param.Add("NEW_CONTRACT_NO" + i.ConvertToString(), x.NEW_CONTRACT_NO);
                    param.Add("WITHDRAWAL_TYPE" + i.ConvertToString(), x.WITHDRAWAL_TYPE);
                    param.Add("PROCESS_TYPE" + i.ConvertToString(), x.PROCESS_TYPE);
                    param.Add("END_BALANCE_FLAG" + i.ConvertToString(), x.END_BALANCE_FLAG);
                    param.Add("APPLY_USER" + i.ConvertToString(), x.APPLY_USER);
                    param.Add("TXN_DATE" + i.ConvertToString(), x.TXN_DATE);
                    param.Add("ACTUAL_WITHDRAWAL_AMT_USD" + i.ConvertToString(), x.ACTUAL_WITHDRAWAL_AMT_USD);
                    param.Add("ACTUAL_WITHDRAWAL_AMT_NTD" + i.ConvertToString(), x.ACTUAL_WITHDRAWAL_AMT_NTD);
                    param.Add("ACTUAL_WITHDRAWAL_AMT_RMB" + i.ConvertToString(), x.ACTUAL_WITHDRAWAL_AMT_RMB);
                    param.Add("ACTUAL_WITHDRAWAL_AMT_EUR" + i.ConvertToString(), x.ACTUAL_WITHDRAWAL_AMT_EUR);
                    param.Add("ACTUAL_WITHDRAWAL_AMT_AUD" + i.ConvertToString(), x.ACTUAL_WITHDRAWAL_AMT_AUD);
                    param.Add("ACTUAL_WITHDRAWAL_AMT_JPY" + i.ConvertToString(), x.ACTUAL_WITHDRAWAL_AMT_JPY);
                    param.Add("ACTUAL_WITHDRAWAL_AMT_NZD" + i.ConvertToString(), x.ACTUAL_WITHDRAWAL_AMT_NZD);
                    param.Add("CUST_ID" + i.ConvertToString(), x.CUST_ID);
                    param.Add("ORDER_NO" + i.ConvertToString(), x.ORDER_NO);
                    //  2019/10/28 > 業務代出金功能
                    param.Add("WEB_SUBMIT_SALES_IBCODE" + i.ConvertToString(), x.WEB_SUBMIT_SALES_IBCODE);
                    param.Add("SALES_WEB_SUBMIT_DATETIME" + i.ConvertToString(), x.SALES_WEB_SUBMIT_DATETIME);
                    param.Add("IS_SALES_SUBMIT_TEMP" + i.ConvertToString(), x.IS_SALES_SUBMIT_TEMP);
                    i += 1;

                });

                if (strSQL != string.Empty)
                {
                    strSQL = " BEGIN " + strSQL + " END; ";
                    this._cn.Execute(strSQL, param);
                }
                return true;
            }
            catch (Exception ex)
            {
                exError = ex;
                return false;
            }
        }


        public decimal? GetNextSN(out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
SELECT BL_ACCOUNT_TXN_BATCH_SN.NEXTVAL AS BATCH_WITHDRAWAL_ID
FROM DUAL
";
                return this._cn.Query<BL_ACCOUNT_TXN_TEMP>(strSQL, null)
                    .ToList()[0].BATCH_WITHDRAWAL_ID;
            }
            catch (Exception ex)
            {
                exError = ex;
                return 0;
            }
        }

        public List<BL_ACCOUNT_TXN_TEMP> GetApplyTempData(string strCust_ID, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
SELECT 
    A.*
    , B.ORDER_NO
FROM BL_ACCOUNT_TXN_TEMP A
    INNER JOIN BL_ORDER B
        ON A.ORDER_ID = B.ORDER_ID
            AND B.STATUS_CODE NOT IN (-1)
WHERE A.WITHDRAWAL_TYPE = 'WEB'
    AND B.CUST_ID = :CUST_ID
";
                return this._cn.Query<BL_ACCOUNT_TXN_TEMP>(strSQL, new
                {
                    CUST_ID = strCust_ID
                }).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }

        public List<BL_ACCOUNT_TXN_TEMP> GetApplyTempDataByBatchSN(long BatchSN, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
SELECT 
    A.*
    , B.ORDER_NO
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(C.CUST_ENAME) CUST_ENAME
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(C.CUST_ENAME2) AS CUST_ENAME2
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(C.CUST_CNAME) AS CUST_CNAME
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(C.CUST_CNAME2) AS CUST_CNAME2


    , D.BANK_CNAME
    , D.BANK_ENAME

    , D.ACCOUNT_CNAME AS BANK_ACCOUNT_CNAME
    , D.ACCOUNT AS BANK_ACCOUNT
    , D.SWIFT_CODE AS BANK_SWIFT_CODE

    , D.BANK_CNAME2
    , D.BANK_ENAME2

    , D.ACCOUNT_CNAME2 AS BANK_ACCOUNT_CNAME2
    , D.ACCOUNT2 AS BANK_ACCOUNT2
    , D.SWIFT_CODE2 AS BANK_SWIFT_CODE2

FROM BL_ACCOUNT_TXN_TEMP A
    INNER JOIN BL_ORDER B
        ON A.ORDER_ID = B.ORDER_ID
            AND B.STATUS_CODE NOT IN (-1)
    INNER JOIN BL_CUST C
        ON B.CUST_ID = C.CUST_ID
    LEFT JOIN BL_CUST_BANK D
        ON C.CUST_ID = D.CUST_ID
            AND 
                CASE
                    WHEN A.ACTUAL_WITHDRAWAL_AMT_USD IS NOT NULL THEN 'USD'
                    WHEN A.ACTUAL_WITHDRAWAL_AMT_NTD IS NOT NULL THEN 'NTD'
                    WHEN A.ACTUAL_WITHDRAWAL_AMT_RMB IS NOT NULL THEN 'RMB'
                    WHEN A.ACTUAL_WITHDRAWAL_AMT_EUR IS NOT NULL THEN 'EUR'
                    WHEN A.ACTUAL_WITHDRAWAL_AMT_AUD IS NOT NULL THEN 'AUD'
                    ELSE ''
                END = D.CURRENCY

WHERE A.WITHDRAWAL_TYPE = 'WEB'
    AND A.BATCH_WITHDRAWAL_ID = :BATCH_SN
    AND A.SCHEDULE NOT IN ('4', '9')
";

                return this._cn.Query<BL_ACCOUNT_TXN_TEMP>(strSQL, new
                {
                    BATCH_SN = BatchSN
                }).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }


        public List<BL_ACCOUNT_TXN_TEMP> GetApplyTempDataALLByCustID(string strCust_ID, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
SELECT 
    A.*
    , B.ORDER_NO
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(C.CUST_ENAME) CUST_ENAME
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(C.CUST_ENAME2) AS CUST_ENAME2
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(C.CUST_CNAME) AS CUST_CNAME
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(C.CUST_CNAME2) AS CUST_CNAME2


    , D.BANK_CNAME
    , D.BANK_ENAME

    , D.ACCOUNT_CNAME AS BANK_ACCOUNT_CNAME
    , D.ACCOUNT AS BANK_ACCOUNT
    , D.SWIFT_CODE AS BANK_SWIFT_CODE

    , D.BANK_CNAME2
    , D.BANK_ENAME2

    , D.ACCOUNT_CNAME2 AS BANK_ACCOUNT_CNAME2
    , D.ACCOUNT2 AS BANK_ACCOUNT2
    , D.SWIFT_CODE2 AS BANK_SWIFT_CODE2

FROM BL_ACCOUNT_TXN_TEMP A
    INNER JOIN BL_ORDER B
        ON A.ORDER_ID = B.ORDER_ID
            AND B.STATUS_CODE NOT IN (-1)
    INNER JOIN BL_CUST C
        ON B.CUST_ID = C.CUST_ID
    LEFT JOIN BL_CUST_BANK D
        ON C.CUST_ID = D.CUST_ID
            AND 
                CASE
                    WHEN A.ACTUAL_WITHDRAWAL_AMT_USD IS NOT NULL THEN 'USD'
                    WHEN A.ACTUAL_WITHDRAWAL_AMT_NTD IS NOT NULL THEN 'NTD'
                    WHEN A.ACTUAL_WITHDRAWAL_AMT_RMB IS NOT NULL THEN 'RMB'
                    WHEN A.ACTUAL_WITHDRAWAL_AMT_EUR IS NOT NULL THEN 'EUR'
                    WHEN A.ACTUAL_WITHDRAWAL_AMT_AUD IS NOT NULL THEN 'AUD'
                    ELSE ''
                END = D.CURRENCY

WHERE B.CUST_ID = :CUST_ID
    AND A.SCHEDULE NOT IN ('4', '9')
";

                return this._cn.Query<BL_ACCOUNT_TXN_TEMP>(strSQL, new
                {
                    CUST_ID = strCust_ID
                }).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }


        public bool DeleteTempData(long ORDER_ID, long BATCH_WITHDRAWAL_ID, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
DELETE BL_ACCOUNT_TXN_TEMP A
WHERE A.ORDER_ID = :ORDER_ID
    AND A.BATCH_WITHDRAWAL_ID = :BATCH_WITHDRAWAL_ID
    AND A.IS_SALES_SUBMIT_TEMP = 'Y'
";
                this._cn.Execute(strSQL, new
                {
                    ORDER_ID = ORDER_ID,
                    BATCH_WITHDRAWAL_ID = BATCH_WITHDRAWAL_ID
                });
                return true;
            }
            catch (Exception ex)
            {
                exError = ex;
                return false;
            }
        }



        public List<BL_ACCOUNT_TXN_TEMP> GetOrderDataByORDER_NO(List<string> ORDER_NO, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
SELECT 
      A.*
    , B.ORDER_NO
FROM BL_ACCOUNT_TXN_TEMP A
    INNER JOIN BL_ORDER B
        ON A.ORDER_ID = B.ORDER_ID
 WHERE B.ORDER_NO IN :ORDER_NO
    AND A.SCHEDULE IN ('1', '2', '3')

";
                return this._cn.Query<BL_ACCOUNT_TXN_TEMP>(strSQL, new
                {
                    ORDER_NO = ORDER_NO
                }).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }



    }
}
