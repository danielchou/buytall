﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Oracle.ManagedDataAccess.Client;
using AgentCustomer.Model;
using AgentCustomer.Model.Table;
using AgentCustomer.Utility;

namespace AgentCustomer.Data.Class
{
    public class Da_BL_CUST_BANK
    {
        private OracleConnection _cn;
        public Da_BL_CUST_BANK(OracleConnection cn)
        {
            this._cn = cn;
        }


        public List<BL_CUST_BANK> GetDataByCustID(string strCust_ID, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
SELECT *
FROM BL_CUST_BANK A
WHERE A.CUST_ID = :CUST_ID
";
                return this._cn.Query<BL_CUST_BANK>(strSQL, new
                {
                    CUST_ID = strCust_ID
                }).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }

    }
}
