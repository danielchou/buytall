﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Oracle.ManagedDataAccess.Client;
using AgentCustomer.Model;
using AgentCustomer.Model.Table;
using AgentCustomer.Model.View;
using AgentCustomer.Utility;


namespace AgentCustomer.Data.Class
{
    public class Da_BL_ACCOUNT_TXN_V1
    {
        private OracleConnection _cn;
        public Da_BL_ACCOUNT_TXN_V1(OracleConnection cn)
        {
            this._cn = cn;
        }




        public List<BL_ACCOUNT_TXN_V1> GetOrderDataByORDER_NO(List<string> ORDER_NO, out Exception exError)
        {
            exError = null;
            try
            {
                //                var strSQL = @"

                //SELECT 
                //          ORDER_NO
                //        , FROM_DATE     --合約起始日
                //        , END_DATE      --合約結束日
                //        , EXTEND_DATE   --合約延長日
                //        , ORDER_END     --合約終止日
                //        , CURRENCY      --幣別
                //        , BASE_AMOUNT   --本金
                //        , FLOW_TYPE     --出/入
                //        , 
                //           CASE WHEN FLOW_TYPE='IN' THEN
                //               CASE WHEN TXN_TYPE='BACKGROUND' THEN '紅利配息'
                //                    WHEN TXN_TYPE='BEGIN BALANCE' THEN '期初本金'
                //                    WHEN TXN_TYPE='END BALANCE' THEN '本利贖回' 
                //                    WHEN TXN_TYPE='INTEREST TSF' THEN '調整帳' END
                //             WHEN FLOW_TYPE='OUT' THEN 
                //               CASE WHEN TXN_TYPE='WITHDRAWAL' THEN '紅利提領' 
                //                    WHEN TXN_TYPE='INTEREST TSF' THEN '調整帳' END
                //           END AS TXN_TYPE      --交易類別
                //        , INTEREST_TIMES    --派息次數
                //        , TXN_DATE      --交易日
                //        , DECODE(CURRENCY, 'USD', AMOUNT_USD, 'NTD', AMOUNT_NTD, 'RMB', AMOUNT_RMB) AMOUNT     --金額
                //FROM BL_ACCOUNT_TXN_V1
                //WHERE ORDER_NO IN :ORDER_NO
                //    AND TXN_TYPE IN('BACKGROUND', 'BEGIN BALANCE', 'WITHDRAWAL', 'INTEREST TSF', 'END BALANCE')
                //ORDER BY TXN_DATE DESC, TXN_ID DESC
                //";
                var strSQL = @"

--客戶查詢WEB的合約交易明細(不納入HISTORY)
SELECT 
      ORDER_NO      --合約編號 
    , FROM_DATE     --合約起始日
    , END_DATE      --合約結束日
    , EXTEND_DATE   --合約延長日
    , ORDER_END     --合約終止日
    , CURRENCY      --幣別
    , BASE_AMOUNT   --本金
    ,
       CASE WHEN FLOW_TYPE = 'IN' THEN
             CASE WHEN TXN_TYPE = 'BACKGROUND' THEN '紅利配息'
                WHEN TXN_TYPE = 'BEGIN BALANCE' THEN '期初本金'
                WHEN TXN_TYPE = 'INTEREST TSF' THEN '調整帳' END
           WHEN FLOW_TYPE = 'OUT' THEN
               CASE WHEN TXN_TYPE = 'WITHDRAWAL' THEN '紅利提領'
                WHEN TXN_TYPE = 'INTEREST TSF' THEN '調整帳'
                WHEN TXN_TYPE = 'END BALANCE' THEN '本利贖回'  END
         END AS TXN_TYPE    --交易類別
    , INTEREST_TIMES        --派息次數
    , TXN_DATE              --交易日
       , DECODE(CURRENCY, 'USD', AMOUNT_USD, 'NTD', AMOUNT_NTD, 'RMB', AMOUNT_RMB,'AUD',AMOUNT_AUD,'EUR',AMOUNT_EUR,'JPY',AMOUNT_JPY) AMOUNT      --金額
    , BL_REPORT_PKG.ORDER_APPLY_WITHDRAWAL_AMOUNT(ORDER_ID, SYSDATE) AS ORDER_APPLY_WITHDRAWAL_AMOUNT
FROM BL_ACCOUNT_TXN_V1
 WHERE ORDER_NO IN :ORDER_NO
    AND TXN_TYPE IN ('BACKGROUND', 'BEGIN BALANCE', 'WITHDRAWAL', 'INTEREST TSF', 'END BALANCE') 
ORDER BY TXN_DATE DESC, TXN_ID DESC
";
              
                return this._cn.Query<BL_ACCOUNT_TXN_V1>(strSQL, new
                {
                    ORDER_NO = ORDER_NO
                }).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }

      

        public List<BL_ACCOUNT_TXN_V1> GetDepositData(string strCustID, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL =
@"
SELECT 
      A.NEW_CONTRACT_NO
    , A.BONUS_TSF
    , A.WITHDRAWAL_TYPE
    , A.TXN_DATE
    , A.BATCH_WITHDRAWAL_ID 
    
    , A.ORDER_NO
    , A.FROM_DATE
    , A.END_DATE
    , A.EXTEND_DATE
    , A.BASE_AMOUNT 
    , A.AMOUNT_USD 
    , BL_INTEREST_PKG.RETURN_BALANCE(A.ORDER_ID) AS BALANCE 
    --, 0 AS BALANCE
    , A.REMARK
    , A.BONUS_TSF 
    , A.END_BALANCE_FLAG 

    , A.NEW_CONTRACT_NO 
    , A.AMOUNT_NTD
    , A.AMOUNT_RMB
    , A.SALES
    , A.CNAME
    , A.APPLY_USER
    , A.CHECK_USER
    , A.APPROVER
FROM BL_ACCOUNT_TXN_V1 A
WHERE APPROVED_FLAG = 'Y' 
    AND TXN_TYPE IN ('WITHDRAWAL' ,'END BALANCE')
    AND A.CUST_ID = :CUST_ID
";
                return this._cn.Query<BL_ACCOUNT_TXN_V1>(strSQL, new
                {
                    CUST_ID = strCustID
                }).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }


        public List<BL_ACCOUNT_TXN_V1> GetDepositData2(string strCustID, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
SELECT 
	  A.BATCH_WITHDRAWAL_ID   --    出金單號
	, CURRENCY
	, TXN_DATE              --  出金日
    , AMOUNT                --  出金總額
	, ACTUAL_AMOUNT         --  實際提領金額
	, REMARK
	, BONUS_TSF             --  利息內轉新單
    , AMOUNT-NVL(ACTUAL_AMOUNT, 0) DEV  --  餘額
    , CUST_ID
FROM
(
    SELECT

        CNAME
        , BATCH_WITHDRAWAL_ID
        , CURRENCY
        , TRUNC(TXN_DATE) TXN_DATE
        , SUM(DECODE(CURRENCY, 'USD', AMOUNT_USD, 'AUD', AMOUNT_AUD, 'EUR', AMOUNT_EUR,'RMB', AMOUNT_RMB, 'JPY', AMOUNT_JPY)) AMOUNT
        , SUM(DECODE(CURRENCY, 'USD', ACTUAL_WITHDRAWAL_AMT_USD, 'AUD', ACTUAL_WITHDRAWAL_AMT_AUD, 'EUR', ACTUAL_WITHDRAWAL_AMT_EUR, 'RMB', ACTUAL_WITHDRAWAL_AMT_RMB, 'JPY', ACTUAL_WITHDRAWAL_AMT_JPY)) ACTUAL_AMOUNT
        , BONUS_TSF
        , REMARK
        , CUST_ID
    FROM BL_ACCOUNT_TXN_V1
    WHERE BATCH_WITHDRAWAL_ID IS NOT NULL
        AND CUST_ID = :CUST_ID
    GROUP BY BATCH_WITHDRAWAL_ID
        , CURRENCY
        , TRUNC(TXN_DATE)
        , REMARK
        , CNAME
        , BONUS_TSF
        , CUST_ID
) A
";
                return this._cn.Query<BL_ACCOUNT_TXN_V1>(strSQL, new
                {
                    CUST_ID = strCustID
                }).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }


        public List<BL_ACCOUNT_TXN_V1> GetDepositDataDetail2(string strBatchID, string strCustID, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
SELECT 
      BATCH_WITHDRAWAL_ID
    , ORDER_NO
    , CURRENCY
    , DECODE(CURRENCY,'USD', AMOUNT_USD,'AUD', AMOUNT_AUD,'EUR', AMOUNT_EUR,'JPY', AMOUNT_JPY,'RMB', AMOUNT_RMB) AMOUNT
FROM BL_ACCOUNT_TXN_V1
WHERE BATCH_WITHDRAWAL_ID = :BATCH_ID
    AND CUST_ID = :CUST_ID
";
                return this._cn.Query<BL_ACCOUNT_TXN_V1>(strSQL, new
                {
                    BATCH_ID = strBatchID,
                    CUST_ID = strCustID
                }).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }
    }
}
