﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Oracle.ManagedDataAccess.Client;
using AgentCustomer.Model;
using AgentCustomer.Model.Table;
using AgentCustomer.Model.View;
using AgentCustomer.Utility;

namespace AgentCustomer.Data.Class
{
    public class Da_BL_ORDER
    {
        private OracleConnection _cn;
        public Da_BL_ORDER(OracleConnection cn)
        {
            this._cn = cn;
        }


        public List<BL_ORDER> GetDataByCustID(string CustID, bool IsShowUnApproveData, out Exception exError)
        {
            exError = null;
            try
            {
                //  Todo > Call Package BL_INTEREST_PKG.RETURN_BALANCE
                var strSQL = @"
SELECT 
    A.*
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_ENAME)  AS CUST_ENAME
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_CNAME) AS CUST_CNAME
    , D.CNAME AS SALES_NAME
    , BL_INTEREST2_PKG.RETURN_BALANCE(a.ORDER_ID) AS BALANCE
    , BL_REPORT_PKG.ACCUMULATE_INTEREST(a.ORDER_NO, SYSDATE) AS ACCUMULATE_INTEREST
    , BL_REPORT_PKG.MONTHLY_INTEREST(a.ORDER_NO, SYSDATE) AS MONTHLY_INTEREST
    
    , NVL((
        SELECT SUM(
            NVL(X.ACTUAL_WITHDRAWAL_AMT_USD, 0) + 
            NVL(X.ACTUAL_WITHDRAWAL_AMT_NTD, 0) + 
            NVL(X.ACTUAL_WITHDRAWAL_AMT_RMB, 0) + 
            NVL(X.ACTUAL_WITHDRAWAL_AMT_EUR, 0) + 
            NVL(X.ACTUAL_WITHDRAWAL_AMT_AUD, 0) + 
            NVL(X.ACTUAL_WITHDRAWAL_AMT_JPY, 0) + 
            NVL(X.ACTUAL_WITHDRAWAL_AMT_NZD, 0)
            ) 
        FROM BL_ACCOUNT_TXN_TEMP X 
        WHERE A.ORDER_ID = X.ORDER_ID
            AND X.SCHEDULE IN ('0', '1', '3')
        ), 0) AS ApplyOnPathAmount
    , BL_REPORT_PKG.ORDER_APPLY_WITHDRAWAL_AMOUNT(A.ORDER_ID, SYSDATE) AS ORDER_APPLY_WITHDRAWAL_AMOUNT
FROM BL_ORDER A
    INNER JOIN BL_CUST B
        ON A.CUST_ID = B.CUST_ID
    LEFT JOIN BL_CUST_SALES C
        ON A.CUST_ID = C.CUST_ID
            AND C.ACTIVE <> 'N'
    LEFT JOIN BL_EMP D
        ON C.IB_CODE = D.IB_CODE
WHERE A.CUST_ID = :CUST_ID
--    AND A.STATUS_CODE = '4'
    AND A.STATUS_CODE NOT IN (-1) 
ORDER BY A.FROM_DATE, A.ORDER_NO    --  合約資料 由舊排到新的
";
                
                var data= this._cn.Query<BL_ORDER>(strSQL, new
                {
                    CUST_ID = CustID
                }).ToList();

                if (IsShowUnApproveData)
                {
                //    strSQL += @"
                //AND
                //(
                //    (A.STATUS_CODE = '4' AND A.FROM_DATE <= SYSDATE AND TO_DATE(NVL(A.EXTEND_DATE, A.END_DATE), 'YYYY/MM/DD') >= TO_DATE(SYSDATE, 'YYYY/MM/DD')) OR
                //    (A.STATUS_CODE IN ('1', '3') AND A.FROM_DATE <= SYSDATE)
                //)";
                    data = data.Where
                        (
                            x =>
                                (
                                    x.STATUS_CODE.ConvertToString() == "4" &&
                                    ((x.FROM_DATE == null ? DateTime.Parse(DateTime.Now.ToString("yyyy/MM/dd")) : DateTime.Parse(x.FROM_DATE.Value.ToString("yyyy/MM/dd"))) <= DateTime.Parse(DateTime.Now.ToString("yyyy/MM/dd"))) &&
                                    (x.EXTEND_DATE != null ? DateTime.Parse(x.EXTEND_DATE.Value.ToString("yyyy/MM/dd")) : (x.END_DATE == null ? DateTime.Parse(DateTime.Now.ToString("yyyy/MM/dd")) : DateTime.Parse(x.END_DATE.Value.ToString("yyyy/MM/dd")))) >= DateTime.Parse(DateTime.Now.ToString("yyyy/MM/dd"))
                                ) ||
                                (
                                    (x.STATUS_CODE.ConvertToString() == "1" || x.STATUS_CODE.ConvertToString() == "3") &&
                                    ((x.FROM_DATE == null ? DateTime.Parse(DateTime.Now.ToString("yyyy/MM/dd")) : DateTime.Parse(x.FROM_DATE.Value.ToString("yyyy/MM/dd"))) <= DateTime.Parse(DateTime.Now.ToString("yyyy/MM/dd"))) 
                                )
                        ).ToList();
                }
                else
                {
                    //                strSQL += @"
                    //AND
                    //(
                    //    (A.STATUS_CODE = '4')
                    //)";
                   
                    data = data.Where(x => x.STATUS_CODE.ConvertToString() == "4").ToList();
                }
                return data;

                //            if (IsShowUnApproveData)
                //            {
                //                strSQL += @"
                //AND
                //(
                //    (A.STATUS_CODE = '4' AND A.FROM_DATE <= SYSDATE AND TO_DATE(NVL(A.EXTEND_DATE, A.END_DATE), 'YYYY/MM/DD') >= TO_DATE(SYSDATE, 'YYYY/MM/DD')) OR
                //    (A.STATUS_CODE IN ('1', '3') AND A.FROM_DATE <= SYSDATE)
                //)";
                //            }
                //            else
                //            {
                //                strSQL += @"
                //AND
                //(
                //    (A.STATUS_CODE = '4')
                //)";
                //            }

                //            return this._cn.Query<BL_ORDER>(strSQL, new
                //            {
                //                CUST_ID = CustID
                //            }).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }

        public List<BL_ORDER> GetChartDataByCustID(string CustID, out Exception exError)
        {
            exError = null;
            try
            {
                //  Todo > Call Package BL_INTEREST_PKG.RETURN_BALANCE
                var strSQL = @"
SELECT 
    A.YEAR
    , SUM(BL_INTEREST_PKG.RETURN_BALANCE(a.ORDER_ID)) AS BALANCE
    --, SUM(0) AS BALANCE

FROM BL_ORDER A
WHERE A.CUST_ID = :CUST_ID
    AND A.STATUS_CODE = '4'
    AND (A.ORDER_END IS NULL OR A.ORDER_END >= SYSDATE)
GROUP BY A.YEAR
ORDER BY A.YEAR
";
                return this._cn.Query<BL_ORDER>(strSQL, new
                {
                    CUST_ID = CustID
                }).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }
        public List<BL_ORDER> GetChartInterestDataByCustID(List<BL_ORDER> dataInput, string strCurrency, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = string.Empty;
                var strSQL_Sub = string.Empty;
                dataInput = dataInput.Where(x => x.CURRENCY.ConvertToString().ToUpper() == strCurrency.ToUpper()).ToList();

                if (dataInput.Count() > 0)
                {
                    var dateS = dataInput.Min(x => x.FROM_DATE == null ? DateTime.Parse(DateTime.Now.ToString("yyyy/MM/dd")) : x.FROM_DATE.Value);
                    var dateE = dataInput.Max(x => x.ORDER_END == null ? DateTime.Parse(DateTime.Now.ToString("yyyy/MM/dd")) : x.ORDER_END.Value);
                    if (dateS != dateE)
                    {
                        for (int i = dateS.Year; i <= (dateE.Year <= DateTime.Now.Year ? dateE.Year : DateTime.Now.Year); i++)
                        {
                            strSQL_Sub += strSQL_Sub == string.Empty ? "" : " UNION ALL ";
                            strSQL_Sub += @"
                            SELECT " + (i).ToString() + @" AS YEAR,

                                BL_REPORT_PKG.ACCUMULATE_INTEREST_BY_CUST
                                (
                                    " + (dataInput[0].CUST_ID == null ? "" : dataInput[0].CUST_ID.Value.ConvertToString()) + @"
                                    , '" + strCurrency.ToUpper() + @"'
                                    , TO_DATE('" + (i).ToString() + @"/12/31', 'YYYY/MM/DD')
                                ) AS INTEREST

                            FROM DUAL
                            ";
                        }
                    }
                }

                #region Remark
                /*
                dataInput.ForEach(x =>
                {
                    var dateS = x.FROM_DATE == null ? DateTime.Parse(DateTime.Now.ToString("yyyy/MM/dd")) : x.FROM_DATE.Value;
                    var dateE = x.ORDER_END == null ? DateTime.Parse(DateTime.Now.ToString("yyyy/MM/dd")) : x.ORDER_END.Value;
                    if (dateS != dateE)
                    {
                        for (int i = dateS.Year; i <= (dateE.Year <= DateTime.Now.Year ? dateE.Year : DateTime.Now.Year); i++)
                        {
                            strSQL_Sub += strSQL_Sub == string.Empty ? "" : " UNION ALL ";
                            strSQL_Sub += @"
                            SELECT " + (i).ToString() + @" AS YEAR,

                                BL_REPORT_PKG.ACCUMULATE_INTEREST
                                (
                                    " + x.ORDER_NO.ConvertToString() + @"
                                    , TO_DATE('" + (i).ToString() + @"/12/31', 'YYYY/MM/DD')
                                ) AS INTEREST

                            FROM DUAL
                            ";
                            //                            if (dateS.Month >= 7)
                            //                            {
                            //                                strSQL_Sub += @"
                            //SELECT " + (i + 1).ToString() + @" AS YEAR,

                            //    BL_REPORT_PKG.ACCUMULATE_INTEREST
                            //    (
                            //        " + x.ORDER_NO.ConvertToString() + @"
                            //        , TO_DATE('" + (i + 1).ToString() + @"/7/1', 'YYYY/MM/DD')
                            //    ) AS INTEREST

                            //--    9 AS INTEREST
                            //FROM DUAL
                            //";
                            //                            }
                            //                            else
                            //                            {
                            //                                strSQL_Sub += @"
                            //SELECT " + (i).ToString() + @" AS YEAR,

                            //    BL_REPORT_PKG.ACCUMULATE_INTEREST
                            //    (
                            //        " + x.ORDER_NO.ConvertToString() + @"
                            //        , TO_DATE('" + (i).ToString() + @"/7/1', 'YYYY/MM/DD')
                            //    ) AS INTEREST

                            //--    9 AS INTEREST
                            //FROM DUAL
                            //";
                            //                            }
                        }
                    }
                });
                */
                #endregion


                if (strSQL_Sub != string.Empty)
                {
                    strSQL = @"
SELECT 
    T.YEAR
    , SUM(T.INTEREST) AS ACCUMULATE_INTEREST
FROM
(
" + strSQL_Sub + @"
)T
GROUP BY T.YEAR
";
                    return this._cn.Query<BL_ORDER>(strSQL).ToList();
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }



        public BL_ORDER_V1 GetCustAccountAmountInfo(string CustID, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"

SELECT ORDER_NO,ORDER_ID,CURRENCY,AMOUNT, /*本金*/

       BL_INTEREST_PKG.ORDER_ACCUMULATE_INTEREST(ORDER_ID,SYSDATE) ACC_INTEREST,   /*累計紅利*/
       BL_INTEREST_PKG.ORDER_MONTHLY_INTEREST(ORDER_ID,SYSDATE) MONTHLY_INTEREST,  /*當月紅利*/
       BL_INTEREST_PKG.RETURN_BALANCE(ORDER_ID,SYSDATE)-AMOUNT AVAIL_WITHDRAWAL_AMOUNT,        /*可提領金額*/
       BL_INTEREST_PKG.RETURN_BALANCE(ORDER_ID,SYSDATE) BALANCE,                   /*總資產*/

       --10000 AS ACC_INTEREST,   /*累計紅利*/
       --10000 AS MONTHLY_INTEREST,  /*當月紅利*/
       --10000 AS AVAIL_WITHDRAWAL_AMOUNT,        /*可提領金額*/
       --10000 AS BALANCE,                   /*總資產*/


       FROM_DATE,END_DATE,EXTEND_DATE,ORDER_END,YEAR
  FROM BL_ORDER_V1
 WHERE CUST_ID=:X_CUST_ID    AND STATUS_CODE='4'
   AND NVL(ORDER_END,SYSDATE)>=SYSDATE ";

                return this._cn.QueryFirstOrDefault<BL_ORDER_V1>(strSQL, new
                {
                    X_CUST_ID = CustID
                });
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }
    }
}
