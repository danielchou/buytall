﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Oracle.ManagedDataAccess.Client;
using AgentCustomer.Model;
using AgentCustomer.Model.Data;
using AgentCustomer.Model.Table;
using AgentCustomer.Utility;

namespace AgentCustomer.Data.Class
{
    public class Da_BL_ApplyDeposit_RequestForm
    {
        private OracleConnection _cn;
        public Da_BL_ApplyDeposit_RequestForm(OracleConnection cn)
        {
            this._cn = cn;
        }

        public List<BL_ApplyDeposit_RequestForm> GetApplyDeposit_RequestForm_Data(long BatchSN, out Exception exError)
        {
            exError = null;
            try
            {
                var data = new Da_BL_ACCOUNT_TXN_TEMP(this._cn).GetApplyTempDataByBatchSN(BatchSN, out exError);
                if (exError != null) throw exError;
                int i = 1;
                int iCurrentPage = 0;
                int iMaxPage = 0;
                var result = new List<BL_ApplyDeposit_RequestForm>();
                var SingleData = new BL_ApplyDeposit_RequestForm();

                data.ForEach(x =>
                {
                    var strOrderNo = x.ORDER_NO.ConvertToString();
                    decimal decAmount = 0;
                    if (x.ACTUAL_WITHDRAWAL_AMT_USD != null)
                        decAmount = x.ACTUAL_WITHDRAWAL_AMT_USD.Value;
                    else if (x.ACTUAL_WITHDRAWAL_AMT_NTD != null)
                        decAmount = x.ACTUAL_WITHDRAWAL_AMT_NTD.Value;
                    else if (x.ACTUAL_WITHDRAWAL_AMT_RMB != null)
                        decAmount = x.ACTUAL_WITHDRAWAL_AMT_RMB.Value;
                    else if (x.ACTUAL_WITHDRAWAL_AMT_EUR != null)
                        decAmount = x.ACTUAL_WITHDRAWAL_AMT_EUR.Value;
                    else if (x.ACTUAL_WITHDRAWAL_AMT_AUD != null)
                        decAmount = x.ACTUAL_WITHDRAWAL_AMT_AUD.Value;
                    else if (x.ACTUAL_WITHDRAWAL_AMT_JPY != null)
                        decAmount = x.ACTUAL_WITHDRAWAL_AMT_JPY.Value;

                    if (i == 1)
                    {
                        SingleData.BatchSN = BatchSN;
                        SingleData.ClientName = x.CUST_CNAME.ConvertToString().Trim();
                        SingleData.ClientName_ENG = x.CUST_ENAME.ConvertToString().Trim();
                        SingleData.Date = x.APPLY_DATE == null ? string.Empty : x.APPLY_DATE.Value.ToString("yyyy/MM/dd");
                        if (x.ACTUAL_WITHDRAWAL_AMT_USD != null)
                            SingleData.Currency = "USD";
                        else if (x.ACTUAL_WITHDRAWAL_AMT_NTD != null)
                            SingleData.Currency = "NTD";
                        else if (x.ACTUAL_WITHDRAWAL_AMT_RMB != null)
                            SingleData.Currency = "RMB";
                        else if (x.ACTUAL_WITHDRAWAL_AMT_EUR != null)
                            SingleData.Currency = "EUR";
                        else if (x.ACTUAL_WITHDRAWAL_AMT_AUD != null)
                            SingleData.Currency = "AUD";
                        else if (x.ACTUAL_WITHDRAWAL_AMT_JPY != null)
                            SingleData.Currency = "JPY";

                        SingleData.BankName = x.BANK_ENAME.ConvertToString();
                        SingleData.BankAccount = x.BANK_ACCOUNT.ConvertToString();
                        SingleData.BankSwiftCode = x.BANK_SWIFT_CODE.ConvertToString();
                    }


                    #region 各欄位金額資訊

                    switch (i)
                    {
                        case 1:
                            SingleData.OrderNo1 = strOrderNo;
                            SingleData.Amount1 = decAmount;
                            break;
                        case 2:
                            SingleData.OrderNo2 = strOrderNo;
                            SingleData.Amount2 = decAmount;
                            break;
                        case 3:
                            SingleData.OrderNo3 = strOrderNo;
                            SingleData.Amount3 = decAmount;
                            break;
                        case 4:
                            SingleData.OrderNo4 = strOrderNo;
                            SingleData.Amount4 = decAmount;
                            break;
                        case 5:
                            SingleData.OrderNo5 = strOrderNo;
                            SingleData.Amount5 = decAmount;
                            break;
                        case 6:
                            SingleData.OrderNo6 = strOrderNo;
                            SingleData.Amount6 = decAmount;
                            break;
                        case 7:
                            SingleData.OrderNo7 = strOrderNo;
                            SingleData.Amount7 = decAmount;
                            break;
                        case 8:
                            SingleData.OrderNo8 = strOrderNo;
                            SingleData.Amount8 = decAmount;
                            break;
                        case 9:
                            SingleData.OrderNo9 = strOrderNo;
                            SingleData.Amount9 = decAmount;
                            break;
                        case 10:
                            SingleData.OrderNo10 = strOrderNo;
                            SingleData.Amount10 = decAmount;
                            break;
                        case 11:
                            SingleData.OrderNo11 = strOrderNo;
                            SingleData.Amount11 = decAmount;
                            break;
                        case 12:
                            SingleData.OrderNo12 = strOrderNo;
                            SingleData.Amount12 = decAmount;
                            break;
                        case 13:
                            SingleData.OrderNo13 = strOrderNo;
                            SingleData.Amount13 = decAmount;
                            break;
                        case 14:
                            SingleData.OrderNo14 = strOrderNo;
                            SingleData.Amount14 = decAmount;
                            break;
                        case 15:
                            SingleData.OrderNo15 = strOrderNo;
                            SingleData.Amount15 = decAmount;
                            break;
                        case 16:
                            SingleData.OrderNo16 = strOrderNo;
                            SingleData.Amount16 = decAmount;
                            break;
                        case 17:
                            SingleData.OrderNo17 = strOrderNo;
                            SingleData.Amount17 = decAmount;
                            break;
                        case 18:
                            SingleData.OrderNo18 = strOrderNo;
                            SingleData.Amount18 = decAmount;
                            break;
                        case 19:
                            SingleData.OrderNo19 = strOrderNo;
                            SingleData.Amount19 = decAmount;
                            break;
                        case 20:
                            SingleData.OrderNo20 = strOrderNo;
                            SingleData.Amount20 = decAmount;
                            break;
                        default:
                            break;
                    }
                    #endregion

                    if (i == 20)
                    {
                        iCurrentPage += 1;
                        iMaxPage += 1;
                        SingleData.CurrentPage = iCurrentPage;
                        result.Add(SingleData);
                        SingleData = new BL_ApplyDeposit_RequestForm();
                        i = 0;
                    }
                    i += 1;
                });

                if (i > 1)
                {
                    iCurrentPage += 1;
                    iMaxPage += 1;
                    SingleData.CurrentPage = iCurrentPage;
                    result.Add(SingleData);
                }

                decimal decTotal = 0;
                result.ForEach(x =>
                {
                    decTotal += (x.Amount1 == null ? 0 : x.Amount1.Value);
                    decTotal += (x.Amount2 == null ? 0 : x.Amount2.Value);
                    decTotal += (x.Amount3 == null ? 0 : x.Amount3.Value);
                    decTotal += (x.Amount4 == null ? 0 : x.Amount4.Value);
                    decTotal += (x.Amount5 == null ? 0 : x.Amount5.Value);
                    decTotal += (x.Amount6 == null ? 0 : x.Amount6.Value);
                    decTotal += (x.Amount7 == null ? 0 : x.Amount7.Value);
                    decTotal += (x.Amount8 == null ? 0 : x.Amount8.Value);
                    decTotal += (x.Amount9 == null ? 0 : x.Amount9.Value);
                    decTotal += (x.Amount10 == null ? 0 : x.Amount10.Value);

                    decTotal += (x.Amount11 == null ? 0 : x.Amount11.Value);
                    decTotal += (x.Amount12 == null ? 0 : x.Amount12.Value);
                    decTotal += (x.Amount13 == null ? 0 : x.Amount13.Value);
                    decTotal += (x.Amount14 == null ? 0 : x.Amount14.Value);
                    decTotal += (x.Amount15 == null ? 0 : x.Amount15.Value);
                    decTotal += (x.Amount16 == null ? 0 : x.Amount16.Value);
                    decTotal += (x.Amount17 == null ? 0 : x.Amount17.Value);
                    decTotal += (x.Amount18 == null ? 0 : x.Amount18.Value);
                    decTotal += (x.Amount19 == null ? 0 : x.Amount19.Value);
                    decTotal += (x.Amount20 == null ? 0 : x.Amount20.Value);
                });
                result.ForEach(x =>
                {
                    x.MaxPage = iMaxPage;
                    x.AmountTotal = decTotal;
                });

                return result;
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }

        


    }
}
