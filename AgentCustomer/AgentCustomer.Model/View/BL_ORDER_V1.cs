﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgentCustomer.Model.View
{
    public class BL_ORDER_V1
    {
        public decimal? ORDER_ID { get; set; }
        public string ORDER_NO { get; set; }
        public string PARENT_ORDER_NO { get; set; }
        public decimal? CUST_ID { get; set; }
        public string CUST_CNAME { get; set; }
        public string CUST_CNAME2 { get; set; }
        public string ATTRIBUTE1 { get; set; }
        public DateTime? FROM_DATE { get; set; }
        public DateTime? END_DATE { get; set; }
        public DateTime? EXTEND_DATE { get; set; }
        public DateTime? ORDER_END { get; set; }
        public decimal? YEAR { get; set; }
        public string NOTE { get; set; }
        public string CURRENCY { get; set; }
        public decimal? AMOUNT { get; set; }
        public decimal? AMOUNT_USD { get; set; }
        public decimal? AMOUNT_NTD { get; set; }
        public decimal? AMOUNT_RMB { get; set; }
        public string IB_CODE { get; set; }
        public DateTime? CREATION_DATE { get; set; }
        public DateTime? LAST_UPDATE_DATE { get; set; }

        /// <summary>
        /// 累計紅利
        /// </summary>
        public decimal? ACC_INTEREST { get; set; }
        /// <summary>
        /// 當月紅利
        /// </summary>
        public decimal? MONTHLY_INTEREST { get; set; }
        /// <summary>
        /// 可提領金額
        /// </summary>
        public decimal? AVAIL_WITHDRAWAL_AMOUNT { get; set; }
        /// <summary>
        /// 總資產
        /// </summary>
        public decimal? BALANCE { get; set; }
      
    }




}
