﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AgentCustomer.Utility;

namespace AgentCustomer.Model.View
{
    public class BL_ACCOUNT_TXN_V1
    {
        public string ORDER_NO { get; set; }
        public DateTime? FROM_DATE { get; set; }
        public DateTime? END_DATE { get; set; }
        public DateTime? ORDER_END { get; set; }
        public DateTime? EXTEND_DATE { get; set; }
        public string CURRENCY { get; set; }
        public string SALES { get; set; }
        public decimal? BASE_AMOUNT { get; set; }
        public decimal? CUST_ID { get; set; }
        public string CNAME { get; set; }
        public decimal? ORDER_ID { get; set; }
        public decimal? BATCH_WITHDRAWAL_ID { get; set; }
        public string NEW_CONTRACT_NO { get; set; }
        public string REMARK { get; set; }
        public string BONUS_TSF { get; set; }
        public string FLOW_TYPE { get; set; }
        public string TXN_TYPE { get; set; }
        public DateTime? TXN_DATE { get; set; }
        public string WITHDRAWAL_TYPE { get; set; }
        public decimal? AMOUNT_USD { get; set; }
        public decimal? AMOUNT_NTD { get; set; }
        public decimal? AMOUNT_RMB { get; set; }
        public string APPLIED_FLAG { get; set; }
        public string APPLY_USER { get; set; }
        public DateTime? APPLY_DATE { get; set; }
        public string CHECKED_FLAG { get; set; }
        public string CHECK_USER { get; set; }
        public DateTime? CHECK_DATE { get; set; }
        public string APPROVED_FLAG { get; set; }
        public string APPROVER { get; set; }
        public DateTime? APPROVE_DATE { get; set; }
        public DateTime? CREATION_DATE { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? LAST_UPDATE_DATE { get; set; }
        public string LAST_UPDATED_BY { get; set; }
        public string ATTRIBUTE1 { get; set; }
        public string ATTRIBUTE2 { get; set; }
        public string ATTRIBUTE3 { get; set; }
        public string ATTRIBUTE4 { get; set; }
        public string ATTRIBUTE5 { get; set; }
        public decimal? TXN_ID { get; set; }
        public decimal? INTEREST_TIMES { get; set; }


        public string YEAR { get; set; }
        public string END_BALANCE_FLAG { get; set; }

        public decimal? AMOUNT { get; set; }
        public string  GetFROM_DATE
        {
            get
            {
                return this.FROM_DATE == null ? string.Empty : this.FROM_DATE.Value.ToString("yyyy/MM/dd");
            }
        }
        public string GetEND_DATE
        {
            get
            {
                return this.END_DATE == null ? string.Empty : this.END_DATE.Value.ToString("yyyy/MM/dd");
            }
        }
        public string GetORDER_END
        {
            get
            {
                return this.ORDER_END == null ? string.Empty : this.ORDER_END.Value.ToString("yyyy/MM/dd");
            }
        }
        public string GetEXTEND_DATE
        {
            get
            {
                return this.EXTEND_DATE == null ? string.Empty : this.EXTEND_DATE.Value.ToString("yyyy/MM/dd");
            }
        }
        public string GetTXN_DATE
        {
            get
            {
                return this.TXN_DATE == null ? string.Empty : this.TXN_DATE.Value.ToString("yyyy/MM/dd");
            }
        }
        public string GetAPPLY_DATE
        {
            get
            {
                return this.APPLY_DATE == null ? string.Empty : this.APPLY_DATE.Value.ToString("yyyy/MM/dd");
            }
        }

        public string GetCURRENCY
        {
            get
            {
                return this.CURRENCY.ConvertToString();
            }
        }
        public string GetBASE_AMOUNT
        {
            get
            {
                return this.BASE_AMOUNT == null ? string.Empty : this.BASE_AMOUNT.Value.ConvertToString();
            }
        }


        public decimal? ACTUAL_AMOUNT { get; set; }
        /// <summary>
        /// 餘額
        /// </summary>
        public decimal? DEV { get; set; }
        /// <summary>
        /// 提領紅利(申請中)
        /// </summary>
        public decimal? ORDER_APPLY_WITHDRAWAL_AMOUNT { get; set; }
    }




}
