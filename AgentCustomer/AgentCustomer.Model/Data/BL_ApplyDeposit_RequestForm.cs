﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgentCustomer.Model.Data
{
    public class BL_ApplyDeposit_RequestForm
    {
        public string To { get; set; }
        public string ClientName { get; set; }
        public string ClientName_ENG { get; set; }
        public string Date { get; set; }
        public string Currency { get; set; }
        public string BankName { get; set; }
        public string BankAccount { get; set; }
        public string BankSwiftCode { get; set; }
        public string Remark { get; set; }
        public long BatchSN { get; set; }

        public string OrderNo1 { get; set; }
        public decimal? Amount1 { get; set; }
        public string OrderNo2 { get; set; }
        public decimal? Amount2 { get; set; }
        public string OrderNo3 { get; set; }
        public decimal? Amount3 { get; set; }
        public string OrderNo4 { get; set; }
        public decimal? Amount4 { get; set; }
        public string OrderNo5 { get; set; }
        public decimal? Amount5 { get; set; }

        public string OrderNo6 { get; set; }
        public decimal? Amount6 { get; set; }
        public string OrderNo7 { get; set; }
        public decimal? Amount7 { get; set; }
        public string OrderNo8 { get; set; }
        public decimal? Amount8 { get; set; }
        public string OrderNo9 { get; set; }
        public decimal? Amount9 { get; set; }
        public string OrderNo10 { get; set; }
        public decimal? Amount10 { get; set; }


        public string OrderNo11 { get; set; }
        public decimal? Amount11 { get; set; }
        public string OrderNo12 { get; set; }
        public decimal? Amount12 { get; set; }
        public string OrderNo13 { get; set; }
        public decimal? Amount13 { get; set; }
        public string OrderNo14 { get; set; }
        public decimal? Amount14 { get; set; }
        public string OrderNo15 { get; set; }
        public decimal? Amount15 { get; set; }

        public string OrderNo16 { get; set; }
        public decimal? Amount16 { get; set; }
        public string OrderNo17 { get; set; }
        public decimal? Amount17 { get; set; }
        public string OrderNo18 { get; set; }
        public decimal? Amount18 { get; set; }
        public string OrderNo19 { get; set; }
        public decimal? Amount19 { get; set; }
        public string OrderNo20 { get; set; }
        public decimal? Amount20 { get; set; }

        public int CurrentPage { get; set; }
        public int MaxPage { get; set; }
        public decimal? AmountTotal { get; set; }
    }
}
