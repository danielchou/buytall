﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgentCustomer.Model.Table
{
    public class BL_CUST_LOGIN_LOG
    {
        public decimal? BL_CUST_LOG_SN { get; set; }
        public DateTime? LOGIN_DATETIME { get; set; }
        public string LOGIN_IP { get; set; }
        public string LOGIN_ACCOUNT { get; set; }
    }

}
