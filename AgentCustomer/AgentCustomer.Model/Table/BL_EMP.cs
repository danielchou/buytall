﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgentCustomer.Model.Table
{
    public class BL_EMP
    {
        public decimal? EMP_ID { get; set; }
        public string EMP_CODE { get; set; }
        public string CNAME { get; set; }
        public string ENAME { get; set; }
        public byte[] PHOTO { get; set; }
        public DateTime? HIRE_START_DATE { get; set; }
        public DateTime? HIRE_END_DATE { get; set; }
        public decimal? DEPT_ID { get; set; }
        public string ID_NUMBER { get; set; }
        public DateTime? DATE_OF_BIRTH { get; set; }
        public string SEX { get; set; }
        public string HIRE_TYPE { get; set; }
        public string EMAIL { get; set; }
        public string NATION { get; set; }
        public string TITLE { get; set; }
        public string SUPERVISOR_ID { get; set; }
        public string PHONE1 { get; set; }
        public string PHONE2 { get; set; }
        public string LOGIN_PASSWORD { get; set; }
        public string NOTE { get; set; }
        public string ATTRIBUTE1 { get; set; }
        public string ATTRIBUTE2 { get; set; }
        public string ATTRIBUTE3 { get; set; }
        public string ATTRIBUTE4 { get; set; }
        public string ATTRIBUTE5 { get; set; }
        public DateTime? CREATION_DATE { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? LAST_UPDATE_DATE { get; set; }
        public string LAST_UPDATED_BY { get; set; }
        public string INDIRECT_SALES { get; set; }
        public string IB_CODE { get; set; }


        public string DS_DEPT { get; set; }

        public string SUPERVISOR_NAME { get; set; }

        public string DEPT_VALUE { get; set; }
       

        public decimal? ROLE_SN { get; set; }


        public string ROLE_NO { get; set; }
        public string IS_USE_PROD { get; set; }
        public string IS_USE_TEST { get; set; }

    }


}
