﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgentCustomer.Model.Table
{
    public class BL_TEAM_L
    {
        public decimal? T_LINE_NUM { get; set; }
        public decimal? TEAM_NUM { get; set; }
        public decimal? LEVEL_NUM { get; set; }
        public string EMP_CODE { get; set; }
        public decimal? COMM_RATE { get; set; }
        public decimal? EXTRA_COMM_RATE { get; set; }
        public DateTime? CREATION_DATE { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? LAST_UPDATE_DATE { get; set; }
        public string LAST_UPDATED_BY { get; set; }
    }

}
