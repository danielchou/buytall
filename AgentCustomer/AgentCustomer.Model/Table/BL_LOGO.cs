﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgentCustomer.Model.Table
{
    public class BL_LOGO
    {
        public decimal? LID { get; set; }
        public byte[] LOGO { get; set; }
    }

}
