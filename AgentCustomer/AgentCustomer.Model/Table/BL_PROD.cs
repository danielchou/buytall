﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgentCustomer.Model.Table
{
    public class BL_PROD
    {
        public decimal? PROD_ID { get; set; }
        public string PROD_CNAME { get; set; }
        public string PROD_TYPE { get; set; }
        public DateTime? CREATION_DATE { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? LAST_UPDATE_DATE { get; set; }
        public string LAST_UPDATED_BY { get; set; }
    }

}
