﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AgentCustomer.Utility;

namespace AgentCustomer.Model.Table
{

    public class BL_CUST_BANK
    {
        public decimal? CUST_ID { get; set; }
        public string BANK_CNAME { get; set; }
        public string BANK_ENAME { get; set; }
        public string BRANCH_CNAME { get; set; }
        public string BRANCH_ENAME { get; set; }
        public string ACCOUNT_CNAME { get; set; }
        public string ACCOUNT { get; set; }
        public string BANK_C_ADDRESS { get; set; }
        public string BANK_E_ADDRESS { get; set; }
        public string SWIFT_CODE { get; set; }
        public DateTime? CREATION_DATE { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? LAST_UPDATE_DATE { get; set; }
        public string LAST_UPDATED_BY { get; set; }

        public string BANK_CNAME2 { get; set; }
        public string BANK_ENAME2 { get; set; }
        public string BRANCH_CNAME2 { get; set; }
        public string BRANCH_ENAME2 { get; set; }
        public string ACCOUNT_CNAME2 { get; set; }
        public string BANK_C_ADDRESS2 { get; set; }
        public string BANK_E_ADDRESS2 { get; set; }
        public string ACCOUNT2 { get; set; }
        public string SWIFT_CODE2 { get; set; }
        public decimal? BANK_ID { get; set; }
        public string CURRENCY { get; set; }
        public string ACCOUNT_ENAME { get; set; }
        public string ACCOUNT_ENAME2 { get; set; }
        public string ACCOUNT_TYPE { get; set; }

        public string ACCOUNT_TYPE_DESC
        {
            get
            {
                var strResult = string.Empty;
                if (this.CURRENCY.ConvertToString() == "RMB")
                {
                    switch (this.ACCOUNT_TYPE.ConvertToString())
                    {
                        case "DOMESTIC":
                            strResult = "(境內)";
                            break;
                        case "":
                        default:
                            strResult = "(境外)";
                            break;
                    }
                }
                return strResult;
            }
        }
    }




}
