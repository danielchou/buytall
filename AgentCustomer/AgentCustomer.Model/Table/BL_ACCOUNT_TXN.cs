﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgentCustomer.Model.Table
{
    public class BL_ACCOUNT_TXN
    {
        public decimal? ORDER_ID { get; set; }
        public string FLOW_TYPE { get; set; }
        public string TXN_TYPE { get; set; }
        public DateTime? TXN_DATE { get; set; }
        public decimal? AMOUNT_USD { get; set; }
        public decimal? AMOUNT_NTD { get; set; }
        public decimal? AMOUNT_RMB { get; set; }
        public string APPLIED_FLAG { get; set; }
        public string APPLY_USER { get; set; }
        public DateTime? APPLY_DATE { get; set; }
        public string CHECKED_FLAG { get; set; }
        public string CHECK_USER { get; set; }
        public DateTime? CHECK_DATE { get; set; }
        public string APPROVED_FLAG { get; set; }
        public string APPROVER { get; set; }
        public DateTime? APPROVE_DATE { get; set; }
        public DateTime? CREATION_DATE { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? LAST_UPDATE_DATE { get; set; }
        public string LAST_UPDATED_BY { get; set; }
        public decimal? TXN_ID { get; set; }
    }


}
