﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AgentCustomer.Utility;

namespace AgentCustomer.Model.Table
{

    public class BL_ORDER
    {
        public decimal? ORDER_ID { get; set; }
        public string ORDER_NO { get; set; }
        public decimal? PROD_ID { get; set; }
        public decimal? CUST_ID { get; set; }
        public string EMP_CODE { get; set; }
        public decimal? TEAM_NUM { get; set; }
        public string MT4_ACCOUNT { get; set; }
        public string ENTER_CURRENCY { get; set; }
        public decimal? ENTER_AMOUNT { get; set; }
        public string CURRENCY { get; set; }
        public decimal? AMOUNT { get; set; }
        public DateTime? FROM_DATE { get; set; }
        public DateTime? END_DATE { get; set; }
        public decimal? YEAR { get; set; }
        public string STATUS { get; set; }
        public string NOTE { get; set; }
        public string ATTRIBUTE1 { get; set; }
        public string ATTRIBUTE2 { get; set; }
        public string ATTRIBUTE3 { get; set; }
        public string ATTRIBUTE4 { get; set; }
        public string ATTRIBUTE5 { get; set; }
        public DateTime? CREATION_DATE { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? LAST_UPDATE_DATE { get; set; }
        public string LAST_UPDATED_BY { get; set; }
        public string IB_CODE { get; set; }
        public DateTime? ORDER_END { get; set; }
        public decimal? AMOUNT_USD { get; set; }
        public decimal? AMOUNT_NTD { get; set; }
        public decimal? AMOUNT_RMB { get; set; }

        public decimal? AMOUNT_EUR { get; set; }
        public decimal? AMOUNT_AUD { get; set; }
        public decimal? AMOUNT_JPY { get; set; }
        public decimal? AMOUNT_NZD { get; set; }

        public DateTime? EXTEND_DATE { get; set; }
        public decimal? OLD_AMOUNT_USD { get; set; }
        public decimal? OLD_AMOUNT_NTD { get; set; }
        public decimal? OLD_AMOUNT_RMB { get; set; }

        public decimal? OLD_AMOUNT_EUR { get; set; }
        public decimal? OLD_AMOUNT_AUD { get; set; }
        public decimal? OLD_AMOUNT_JPY { get; set; }
        public decimal? OLD_AMOUNT_NZD { get; set; }

        public string PARENT_ORDER_NO { get; set; }

        public decimal? INTEREST_USD { get; set; }
        public decimal? INTEREST_NTD { get; set; }
        public decimal? INTEREST_RMB { get; set; }
        public decimal? INTEREST_EUR { get; set; }
        public decimal? INTEREST_AUD { get; set; }
        public decimal? INTEREST_JPY { get; set; }
        public decimal? INTEREST_NZD { get; set; }


        public string CUST_CNAME { get; set; }
        public string CUST_CNAME2 { get; set; }
        public string CUST_ENAME { get; set; }
        public string CUST_ENAME2 { get; set; }
        public string SALES_NAME { get; set; }

        public string SALES_IB_CODE { get; set; }
        public string SALES_EMP_CODE { get; set; }
        public decimal? BALANCE { get; set; }
        public decimal? STATUS_CODE { get; set; }

        public string GetSTATUS
        {
            get
            {
                var strResult = string.Empty;

                if (this.STATUS_CODE != null)
                {
                    switch (this.STATUS_CODE.Value.ConvertToString())
                    {
                        case "1":
                            strResult = "待審核";
                            break;
                        case "4":
                            strResult = "生效合約";
                            break;
                        case "9":
                            strResult = "已退件";
                            break;
                        default:
                            break;
                    }
                }

                return strResult;
            }
        }

        /// <summary>
        /// 專案類型
        /// </summary>
        public string BONUS_OPTION { get; set; }

        public string AmountCurrency
        {
            get

            {
                var result = "USD";

                if (this.AMOUNT_USD != null)
                {
                    result = "USD";
                }
                else if (this.AMOUNT_RMB != null)
                {
                    result = "RMB";
                }
                else if (this.AMOUNT_NTD != null)
                {
                    result = "NTD";
                }
                else if (this.AMOUNT_EUR != null)
                {
                    result = "EUR";
                }
                else if (this.AMOUNT_AUD != null)
                {
                    result = "AUD";
                }
                else if (this.AMOUNT_JPY != null)
                {
                    result = "JPY";
                }
                else if (this.AMOUNT_NZD != null)
                {
                    result = "NZD";
                }
                return result;
            }
        }

        public decimal? AmountByCurrency
        {
            get
            {
                switch (this.AmountCurrency.ConvertToString())
                {
                    case "NTD":
                        return this.AMOUNT_NTD;
                        break;
                    case "RMB":
                        return this.AMOUNT_RMB;
                        break;
                    case "EUR":
                        return this.AMOUNT_EUR;
                        break;
                    case "AUD":
                        return this.AMOUNT_AUD;
                        break;
                    case "JPY":
                        return this.AMOUNT_JPY;
                        break;
                    case "NZD":
                        return this.AMOUNT_NZD;
                        break;
                    case "USD":
                    default:
                        return this.AMOUNT_USD;
                        break;
                }
            }
        }

        public int DUE_DAYS
        {
            get
            {
                int resutl = 0;
                if (this.ORDER_END != null)
                {
                    resutl = new TimeSpan(this.ORDER_END.Value.Ticks - DateTime.Now.Ticks).Days;
                }
                return resutl;
            }
        }

        public decimal? ACCUMULATE_INTEREST { get; set; }
        public decimal? MONTHLY_INTEREST { get; set; }

        /// <summary>
        /// 可提領紅利
        /// </summary>
        /// <remarks>
        /// 可提領紅利 = 餘額 - 本金
        /// </remarks>
        public decimal? CanApplyAmount
        {
            get
            {
                decimal result = 
                    (this.BALANCE == null ? 0 : this.BALANCE.Value) -
                    (this.AmountByCurrency == null ? 0 : this.AmountByCurrency.Value);
                if (result <= 0)
                {
                    result = 0;
                }
                return result;
            }
        }

        public decimal? ApplyOnPathAmount { get; set; }

        /// <summary>
        /// 提領紅利申請中
        /// </summary>
        public decimal? ORDER_APPLY_WITHDRAWAL_AMOUNT { get; set; }

    }




}
