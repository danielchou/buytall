﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgentCustomer.Model.Table
{
    public class BL_COMM_TXN
    {
        public decimal? TXN_ID { get; set; }
        public DateTime? PERIOD { get; set; }
        public decimal? EMP_ID { get; set; }
        public decimal? COMM_ID { get; set; }
        public decimal? COMM_AMOUNT { get; set; }
        public decimal? EXTRA_COMM_AMOUNT { get; set; }
        public string ISSUED_FLAG { get; set; }
    }

}
