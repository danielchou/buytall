﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AgentCustomer.Utility;

namespace AgentCustomer.Model.Table
{
    [Serializable]
    public class BL_CUST
    {
        public decimal? CUST_ID { get; set; }
        public string CUST_ENAME { get; set; }
        public string CUST_ENAME2 { get; set; }
        public string CUST_CNAME { get; set; }
        public string CUST_CNAME2 { get; set; }
        public string PASSPORT { get; set; }
        public string PASSPORT2 { get; set; }
        public string PASSPORT_REGION { get; set; }
        public string PASSPORT_REGION2 { get; set; }
        public DateTime? PASSPORT_ACTIVE_END_DATE { get; set; }
        public DateTime? PASSPORT_ACTIVE_END_DATE2 { get; set; }
        public string ID_NUMBER { get; set; }
        public string ID_NUMBER2 { get; set; }
        public string SEX { get; set; }
        public string SEX2 { get; set; }
        public DateTime? DATE_OF_BIRTH { get; set; }
        public DateTime? DATE_OF_BIRTH2 { get; set; }
        public string EMAIL_1 { get; set; }
        public string EMAIL_2 { get; set; }
        public string EMAIL2_1 { get; set; }
        public string EMAIL2_2 { get; set; }
        public string PHONE_1 { get; set; }
        public string PHONE_2 { get; set; }
        public string PHONE2_1 { get; set; }
        public string PHONE2_2 { get; set; }
        public string POSTAL_CODE { get; set; }
        public string POSTAL_CODE2 { get; set; }
        public string C_ADDRESS { get; set; }
        public string C_ADDRESS2 { get; set; }
        public string E_ADDRESS { get; set; }
        public string E_ADDRESS2 { get; set; }
        public string MAIL_TYPE_MONTHLY { get; set; }
        public string MAIL_TYPE_END { get; set; }
        public DateTime? CREATION_DATE { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? LAST_UPDATE_DATE { get; set; }
        public string LAST_UPDATED_BY { get; set; }
        public string CUST_CNAME_RAW { get; set; }
        public string CUST_CNAME2_RAW { get; set; }
        public string MT4_ACCOUNT { get; set; }
        public int? ORDER_CNT { get; set; }
        public decimal? ORDER_BALANCE { get; set; }

        public string SALES_NAME { get; set; }
        public string IB_CODE { get; set; }
        public string WEB_PASSWORD { get; set; }
        public string IS_NEED_CHANGE_PWD { get; set; }


        /// <summary>
        /// 指標:合約數
        /// </summary>
        public decimal? ORDER_NUMBER { get; set; }
        /// <summary>
        /// 指標:總資產(含不同幣別)
        /// </summary>
        public string TOTAL_ASSET { get; set; }
        /// <summary>
        /// 指標:本金(含不同幣別)
        /// </summary>
        public string TOTAL_BASE_AMOUNT { get; set; }
        /// <summary>
        /// 指標:可提領金額(含不同幣別)
        /// </summary>
        public string TOTAL_AVAIL_WITHDRAWAL_AMOUNT { get; set; }
        /// <summary>
        /// 指標:歷史總派利
        /// </summary>
        public string HISTORY_TOTAL_WITHDRAWAL { get; set; }
        /// <summary>
        /// 指標:累積報酬率
        /// </summary>
        public string INVEST_DURATION { get; set; }

        public string UNION_ID_NUMBER { get; set; }

        public string IS_SALES_LOGIN { get; set; }


        public string GetUnionCustName
        {
            get
            {
                return this.CUST_CNAME.ConvertToString() +
                    (this.CUST_CNAME2.ConvertToString() != string.Empty ? "/" + this.CUST_CNAME2.ConvertToString() : string.Empty);
            }
        }

        public string GetUnionID_NUMBER
        {
            get
            {
                return this.ID_NUMBER.ConvertToString() +
                    (this.ID_NUMBER2.ConvertToString() != string.Empty ? "/" + this.ID_NUMBER2.ConvertToString() : string.Empty);
            }
        }

        /// <summary>
        /// 審核中本金
        /// </summary>
        public decimal? ORDER_BALANCE_UNAPPROVE { get; set; }

        public decimal? PROCESSING_ORDER_AMOUNT { get; set; }

        public string VERIFICATIONCODE { get; set; }
        /// <summary>
        /// 網路出金類型
        /// P : Phone
        /// E : Email
        /// </summary>
        public string IS_ONLINE_WITHDRAWAL { get; set; }

        /// <summary>
        /// Login Sales的IB CODE
        /// </summary>
        public string SALES_IB_CODE { get; set; }

        /// <summary>
        /// 提領紅利(申請中)
        /// </summary>
        public string TOTAL_APPLY_WITHDRAWAL_AMOUNT { get; set; }
        /// <summary>
        /// 已到期未提領資產
        /// </summary>
        public string EXPIRED_UN_WITHDRAWAL_ASSET { get; set; }
    }





}
