﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Reflection;

namespace MK.Demo.Utility
{
    public static class ExtendMethods
    {
        public static void DataBindByList<T>(this ComboBox cb, List<T> data, string display, string value, T top, bool IsReSelectPreItem = false)
        {
            var preText = string.Empty;
            if (IsReSelectPreItem)
                preText = cb.Text.Trim();

            if (top != null)
            {
                data.Insert(0, top);
            }
            cb.DataSource = data;
            cb.DisplayMember = display;
            cb.ValueMember = value;

            if (IsReSelectPreItem)
                cb.SelectedIndex = cb.FindStringExact(preText);
        }

        public static string ConvertToString(this object obj)
        {
            var result = obj ?? string.Empty;
            return result.ToString();
        }

        public static bool IsDate(this object obj)
        {
            DateTime dtTry;
            return DateTime.TryParse(obj.ConvertToString(), out dtTry);
        }

        public static bool IsNumeric(this object obj)
        {
            decimal decTry;
            return decimal.TryParse(obj.ConvertToString(), out decTry);
        }

        public static string ConvertToDatePickerString(this DateTime dt)
        {
            return dt.ToString("yyyy-MM-dd");
        }
        public static string ConvertToDatePickerString(this DateTime? dt)
        {
            if (dt == null)
            {
                return string.Empty;
            }
            else {
                return dt.Value.ToString("yyyy-MM-dd");
            }
        }

        /// <summary>
        /// Convert Value To Long and Check Is Valide
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="HasValue"></param>
        /// <returns></returns>
        public static long TryGetLongValue(this object obj, out bool HasValue)
        {
            HasValue = true;
            long lnTry;
            if (long.TryParse(obj.ConvertToString(), out lnTry))
            {
                return lnTry;
            }
            else
            {
                HasValue = false;
                return 0;
            }
        }

        /// <summary>
        /// 擴充IEnumerable 可以 ForEach 
        /// </summary>
        public static void ForEach<T>(this IEnumerable<T> obj, Action<T> action)
        {
            foreach (T item in obj)
            {
                action(item);
            }
        }

        /// <summary>
        /// Bind Data Grid By List
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dgv"></param>
        /// <param name="data"></param>
        /// <param name="IsHorizontalScrollingOffset">是否要重新設定水平的Scroll Bar位置</param>
        /// <remarks>
        /// 為了讓Data Grid View能夠排序, 
        /// 但因使用DataSource = List會造成無法排序的問題
        /// 但若是將List轉換成DataTable, 會有一些問題(eg. 會有殘影要移動一下視窗才會復原...等)
        /// </remarks>
        /// 
        /// 
        public static void DataBindByList<T>(this DataGridView dgv, List<T> data, bool IsHorizontalScrollingOffset = false)
        {
            dgv.DataClean();
            DataGridViewColumnSortMode sortMode = DataGridViewColumnSortMode.NotSortable;
            DataGridViewColumn sortColumn = null;
            if (dgv.SortedColumn != null)
            {
                //取消該欄位的排序設定 = 取消整個Grid的排序(即復原回排序前的樣子)
                sortColumn = dgv.SortedColumn;
                sortMode = dgv.SortedColumn.SortMode;
                sortColumn.SortMode = DataGridViewColumnSortMode.NotSortable;
            }

            if (data.Count > 0)
            {
                PropertyInfo[] p = ((Type)data[0].GetType()).GetProperties();

                try
                {

                    data.ForEach(x =>
                                {
                                    dgv.Rows.Add();
                                    var idx = dgv.Rows.Count - 1;
                                    dgv.Columns.Cast<DataGridViewColumn>().ForEach(y =>
                                        {
                                            var pi = p.Where(z => z.Name.ToUpper() == y.DataPropertyName.ToUpper()).ToList();
                                            if (pi.Count == 1)
                                            {
                                                //為了要能夠排序, 所以Value都必須是String

                                                if (pi[0].GetValue(x, null) != null && pi[0].GetValue(x, null).GetType() == typeof(DateTime))
                                                {
                                                    //  Date Time 特別處理 [因轉換成String會造成日期被自動轉換而無法正常排序,會亂跳]
                                                    //dgv.Rows[idx].Cells[y.Name].Value = ((DateTime)pi[0].GetValue(x, null)).ToString("yyyy/MM/dd tt hh:mm");
                                                    dgv.Rows[idx].Cells[y.Name].Value = ((DateTime)pi[0].GetValue(x, null)).ToString("yyyy/MM/dd");
                                                }
                                                else
                                                {
                                                    if (pi[0].GetValue(x, null) != null)
                                                    {
                                                        if (pi[0].GetValue(x, null).GetType() == typeof(decimal))
                                                        {
                                                            dgv.Rows[idx].Cells[y.Name].Value = (decimal)pi[0].GetValue(x, null);
                                                        }
                                                        else if (pi[0].GetValue(x, null).GetType() == typeof(decimal?))
                                                        {
                                                            dgv.Rows[idx].Cells[y.Name].Value = (decimal?)pi[0].GetValue(x, null);
                                                        }
                                                        else if (pi[0].GetValue(x, null).GetType() == typeof(long))
                                                        {
                                                            dgv.Rows[idx].Cells[y.Name].Value = (long)pi[0].GetValue(x, null);
                                                        }
                                                        else if (pi[0].GetValue(x, null).GetType() == typeof(long?))
                                                        {
                                                            dgv.Rows[idx].Cells[y.Name].Value = (long?)pi[0].GetValue(x, null);
                                                        }
                                                        else if (pi[0].GetValue(x, null).GetType() == typeof(double))
                                                        {
                                                            dgv.Rows[idx].Cells[y.Name].Value = (double)pi[0].GetValue(x, null);
                                                        }
                                                        else if (pi[0].GetValue(x, null).GetType() == typeof(double?))
                                                        {
                                                            dgv.Rows[idx].Cells[y.Name].Value = (double?)pi[0].GetValue(x, null);
                                                        }
                                                        else if (pi[0].GetValue(x, null).GetType() == typeof(DateTime))
                                                        {
                                                            var dt = (DateTime)pi[0].GetValue(x, null);
                                                            dgv.Rows[idx].Cells[y.Name].Value = dt.ToString("yyyy年MM月dd日");
                                                        }
                                                        else if (pi[0].GetValue(x, null).GetType() == typeof(DateTime?))
                                                        {
                                                            var dt = (DateTime?)pi[0].GetValue(x, null);
                                                            dgv.Rows[idx].Cells[y.Name].Value = dt == null ? string.Empty : dt.Value.ToString("yyyy年MM月dd日");
                                                        }
                                                        else
                                                        {
                                                            if (pi[0].GetValue(x, null) != null)
                                                            {
                                                                var dataValue = pi[0].GetValue(x, null).ToString();
                                                                dgv.Rows[idx].Cells[y.Name].Value = dataValue;
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (pi[0].GetValue(x, null) != null)
                                                        {
                                                            var dataValue = pi[0].GetValue(x, null).ToString();
                                                            dgv.Rows[idx].Cells[y.Name].Value = dataValue;
                                                        }
                                                    }
                                                }
                                            }
                                        });
                                });

                }
                catch (Exception ex)
                {
                    //throw;
                }
            }


            if (sortColumn != null)
            {
                //恢復排序欄位的排序設定
                sortColumn.SortMode = sortMode;
            }

            if (IsHorizontalScrollingOffset)
            {
                //水平Scroll Bar位置歸0
                dgv.HorizontalScrollingOffset = 0;
            }
        }

        //public static void DataBindByList<T>(this DataGridView dgv, List<T> data, bool IsHorizontalScrollingOffset = false)
        //{
        //    dgv.DataClean();
        //    DataGridViewColumnSortMode sortMode = DataGridViewColumnSortMode.NotSortable;
        //    DataGridViewColumn sortColumn = null;
        //    if (dgv.SortedColumn != null)
        //    {
        //        //取消該欄位的排序設定 = 取消整個Grid的排序(即復原回排序前的樣子)
        //        sortColumn = dgv.SortedColumn;
        //        sortMode = dgv.SortedColumn.SortMode;
        //        sortColumn.SortMode = DataGridViewColumnSortMode.NotSortable;
        //    }

        //    if (data.Count > 0)
        //    {
        //        PropertyInfo[] p = ((Type)data[0].GetType()).GetProperties();

        //        data.ForEach(x =>
        //        {
        //            dgv.Rows.Add();
        //            var idx = dgv.Rows.Count - 1;
        //            dgv.Columns.Cast<DataGridViewColumn>().ForEach(y =>
        //            {
        //                var pi = p.Where(z => z.Name.ToUpper() == y.DataPropertyName.ToUpper()).ToList();
        //                if (pi.Count == 1)
        //                {
        //                    //為了要能夠排序, 所以Value都必須是String

        //                    if (pi[0].GetValue(x, null) != null && pi[0].GetValue(x, null).GetType() == typeof(DateTime))
        //                    {
        //                        //  Date Time 特別處理 [因轉換成String會造成日期被自動轉換而無法正常排序,會亂跳]
        //                        //dgv.Rows[idx].Cells[y.Name].Value = ((DateTime)pi[0].GetValue(x, null)).ToString("yyyy/MM/dd tt hh:mm");
        //                        dgv.Rows[idx].Cells[y.Name].Value = ((DateTime)pi[0].GetValue(x, null)).ToString("yyyy/MM/dd tt hh:mm");
        //                       // dgv.Rows[idx].Cells[y.Name].Value = ((DateTime)pi[0].GetValue(x, null)).ToString("yyyy/MM/dd");
        //                    }
        //                    else
        //                    {
        //                        var dataValue = pi[0].GetValue(x, null).ConvertToString();
        //                        dgv.Rows[idx].Cells[y.Name].Value = dataValue;
        //                    }
        //                }
        //            });
        //        });
        //    }


        //    if (sortColumn != null)
        //    {
        //        //恢復排序欄位的排序設定
        //        sortColumn.SortMode = sortMode;
        //    }

        //    if (IsHorizontalScrollingOffset)
        //    {
        //        //水平Scroll Bar位置歸0
        //        dgv.HorizontalScrollingOffset = 0;
        //    }
        //}

        public static void DataClean(this DataGridView dgv)
        {
            for (int i = dgv.Rows.Count - 1; i >= 0; i--)
            {
                dgv.Rows.RemoveAt(i);
            }
        }

        //public static string GetConfigValueByKey(string strKey)
        //{
        //    return System.Configuration.ConfigurationManager.AppSettings[strKey].ConvertToString().Trim();
        //}
        
    }
}
