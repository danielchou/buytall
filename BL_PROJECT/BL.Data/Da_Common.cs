﻿using MK.Demo.Model;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MK.Demo.Utility;
using Dapper;

namespace MK.Demo.Data
{
    public class Da_Common
    {
        private OracleConnection _cn;
        public Da_Common(OracleConnection cn)
        {
            this._cn = cn;
        }

        public int GetBALANCE(string Orderid)
        {
            try
            {
                var strSQL = @"
             select   BL_INTEREST2_PKG.RETURN_BALANCE(" + Orderid + ")  as  BALANCE  from dual ";
                var data = this._cn.Query<int>(strSQL).FirstOrDefault();

                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //return 0;   //  Ian Remark 2018/11/02
        }

        public long GetNextSeq(string SEQ_Name)
        {
            try
            {
                var strSQL = @"
SELECT " + SEQ_Name + @".NEXTVAL AS SEQ
FROM DUAL ";
                var data = this._cn.Query<Sequence>(strSQL).FirstOrDefault();

                return data.SEQ;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
