﻿using MK.Demo.Model;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MK.Demo.Utility;
using Dapper;


namespace MK.Demo.Data
{
    public class Da_IND_DISCOUNT_AMOUNT : DataRepository<IND_DISCOUNT_AMOUNT>
    {
        OracleConnection _Conn;
        public Da_IND_DISCOUNT_AMOUNT(OracleConnection conn)
         : base(conn)
        { _Conn = conn; }

        public IND_DISCOUNT_AMOUNT GetIND_DISCOUNT_AMOUNT(string strCurrency, string strAmount, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
SELECT BL_INTEREST2_PKG.IND_DISCOUNT_AMOUNT(:CURRENCY, :AMOUNT) DiscountAmount
FROM DUAL
";
                return base.GetSingleOrDefault(strSQL, new
                {
                    CURRENCY = strCurrency,
                    AMOUNT = strAmount
                }, CommandType.Text);
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }
    }


}
