﻿using MK.Demo.Model;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MK.Demo.Utility;
using Dapper;

namespace MK.Demo.Data
{
    public class Da_BL_ORDER_UPDATE_LOG : DataRepository<BL_ORDER_UPDATE_LOG>
    {
        OracleConnection _Conn;
        public Da_BL_ORDER_UPDATE_LOG(OracleConnection conn)
         : base(conn)
        { _Conn = conn; }


        public bool WriteLog(BL_ORDER_UPDATE_LOG data, string strUserName, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
INSERT INTO BL_ORDER_UPDATE_LOG
(
    BL_ORDER_UPDATE_LOG_ID, 
    ORDER_ID, 
    ORDER_NO, 
    STATUS_CODE, 
    REMARK_TEXT, 
    UPDATE_DATE, 
    UPDATED_BY
)
SELECT 
    BL_ORDER_UPDATE_LOG_S1.NEXTVAL,
    :ORDER_ID, 
    :ORDER_NO, 
    :STATUS_CODE, 
    :REMARK_TEXT, 
    SYSDATE, 
    :UPDATED_BY
FROM DUAL
";
                this._Conn.Execute(strSQL, new
                {
                    ORDER_ID = data.ORDER_ID,
                    ORDER_NO = data.ORDER_NO,
                    STATUS_CODE = data.STATUS_CODE,
                    REMARK_TEXT = data.REMARK_TEXT,
                    UPDATED_BY = strUserName
                });
                return true;
            }
            catch (Exception ex)
            {
                exError = ex;
                return false;
            }
        }

    }
}
