﻿using MK.Demo.Model;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MK.Demo.Utility;


namespace MK.Demo.Data
{
    public class Da_BL_CUST_BANK : DataRepository<BL_CUST_BANK>
    {
        public Da_BL_CUST_BANK(OracleConnection conn)
         : base(conn)
        { }

        public List<BL_CUST_BANK> GetDataByCustID(string strCust_ID, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
SELECT *
FROM BL_CUST_BANK A
WHERE A.CUST_ID = :CUST_ID
";
                return base.GetList(strSQL, new
                {
                    CUST_ID = strCust_ID
                }, CommandType.Text).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }



        public bool InsertCustBank(string CustID, BL_CUST_BANK data, out Exception exError)
        {
            exError = null;
            try
            {

                var strSQL = @"
INSERT INTO
BL_CUST_BANK
(
     CUST_ID,BANK_CNAME,BANK_CNAME2,BANK_ENAME,BANK_ENAME2,BRANCH_CNAME,BRANCH_CNAME2,BRANCH_ENAME,BRANCH_ENAME2,ACCOUNT_CNAME,ACCOUNT_CNAME2,ACCOUNT,ACCOUNT2,BANK_C_ADDRESS,BANK_C_ADDRESS2,
BANK_E_ADDRESS,BANK_E_ADDRESS2,SWIFT_CODE,SWIFT_CODE2,CREATION_DATE,CREATED_BY ,LAST_UPDATE_DATE,LAST_UPDATED_BY,BANK_ID,CURRENCY, ACCOUNT_TYPE
)
Values  ('" + CustID + "','" + data.BANK_CNAME + "','" + data.BANK_CNAME2 + "','" + data.BANK_ENAME + "','" + data.BANK_ENAME2 + "','" + data.BRANCH_CNAME + "','" + data.BRANCH_CNAME2 + "','" + data.BRANCH_ENAME + "','" + data.BRANCH_ENAME2 + "','" + data.ACCOUNT_CNAME + "','" + data.ACCOUNT_CNAME2 + "','" + data.ACCOUNT + "','" + data.ACCOUNT2 + "','" + data.BANK_C_ADDRESS + "','" + data.BANK_C_ADDRESS2 + "','" + data.BANK_E_ADDRESS + "','" + data.BANK_E_ADDRESS2 + "','" + data.SWIFT_CODE + "','" + data.SWIFT_CODE2 + "',sysdate,'" + data.CREATED_BY + "' ,sysdate,'" + data.LAST_UPDATED_BY + "'," + data.BANK_ID + ",'" + data.CURRENCY + "', '" + data.ACCOUNT_TYPE.ConvertToString() + "')";
                base.ExecuteScalar(strSQL,null, CommandType.Text);
                return true;
            }
            catch (Exception ex)
            {
                exError = ex;
                return false;
            }
        }


    }
}
