﻿using MK.Demo.Model;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MK.Demo.Utility;
using Dapper;

namespace MK.Demo.Data
{
    public class Da_BL_DEPT : DataRepository<BL_DEPT>
    {
        OracleConnection _Conn;
        public Da_BL_DEPT(OracleConnection conn)
         : base(conn)
        { _Conn = conn; }

        public List<BL_DEPT> GetData(string strORG_CODE, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
SELECT *
FROM BL_DEPT A
WHERE A.ORG_CODE = :ORG_CODE
ORDER BY A.DEPT_CODE
";
                return base.GetList(strSQL, new
                {
                    ORG_CODE = strORG_CODE
                }, CommandType.Text).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }

    }
}
