﻿using MK.Demo.Model;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MK.Demo.Utility;
using Dapper;

namespace MK.Demo.Data
{
    public class Da_BL_ACCOUNT_TXN_TEMP : DataRepository<BL_ACCOUNT_TXN_TEMP>
    {
        public Da_BL_ACCOUNT_TXN_TEMP(OracleConnection conn)
         : base(conn)
        { }


        public List<BL_ACCOUNT_TXN_TEMP> GetDepositData
            (
                string OrderNo, 
                string strCustName, 
                DateTime? dteCreateDate_S, DateTime? dteCreateDate_E,
                DateTime? dteApplyDate_S, DateTime? dteApplyDate_E,
                string BatchNo,
                string strOrg,
                out Exception exError
            )
        {
            exError = null;
            try
            {
                var strSQL = @"
SELECT A.*
    ,BL_INTEREST2_PKG.RETURN_BALANCE(a.ORDER_ID) AS BALANCE
--,0 AS BALANCE
    , B.ORDER_NO
    , B.FROM_DATE
    , B.YEAR
    --, BL_CRYPT_PKG.DECRYPT_DES_NOKEY (C.CUST_CNAME) AS CUST_CNAME
    , CASE 
        WHEN CUST_CNAME2 IS NOT NULL 
            THEN BL_CRYPT_PKG.DECRYPT_DES_NOKEY(CUST_CNAME)||'/'||BL_CRYPT_PKG.DECRYPT_DES_NOKEY(CUST_CNAME2) 
            ELSE BL_CRYPT_PKG.DECRYPT_DES_NOKEY(CUST_CNAME) 
        END CUST_CNAME
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (C.CUST_CNAME2) AS CUST_CNAME2

    , CASE 
        WHEN CUST_ENAME2 IS NOT NULL 
            THEN BL_CRYPT_PKG.DECRYPT_DES_NOKEY(CUST_ENAME)||'/'||BL_CRYPT_PKG.DECRYPT_DES_NOKEY(CUST_ENAME2) 
            ELSE BL_CRYPT_PKG.DECRYPT_DES_NOKEY(CUST_ENAME) 
        END CUST_ENAME

    , B.AMOUNT_USD AS ORDER_AMOUNT_USD
    , B.AMOUNT_NTD AS ORDER_AMOUNT_NTD
    , B.AMOUNT_RMB AS ORDER_AMOUNT_RMB
    , B.AMOUNT_EUR AS ORDER_AMOUNT_EUR
    , B.AMOUNT_AUD AS ORDER_AMOUNT_AUD
    , B.AMOUNT_JPY AS ORDER_AMOUNT_JPY
    , B.AMOUNT_NZD AS ORDER_AMOUNT_NZD
    , B.ORG_CODE
FROM BL_ACCOUNT_TXN_TEMP A
    INNER JOIN BL_ORDER B
        ON A.ORDER_ID = B.ORDER_ID
            AND B.STATUS_CODE = '4'
    INNER JOIN BL_CUST C
        ON B.CUST_ID = C.CUST_ID
WHERE 1 = 1
  AND (A.SCHEDULE <> '4'  or  (A.SCHEDULE  =  '4' and  process_type = 'SCHEDULE' ))
    AND ( UPPER(B.ORDER_NO) LIKE '%'||UPPER(:ORDER_NO)||'%' OR :ORDER_NO IS NULL )
    AND 
        ( 
            BL_CRYPT_PKG.DECRYPT_DES_NOKEY(C.CUST_CNAME) LIKE '%'||:CUST_NAME||'%' OR 
            BL_CRYPT_PKG.DECRYPT_DES_NOKEY(C.CUST_CNAME2) LIKE '%'||:CUST_NAME||'%' OR 
            :CUST_NAME IS NULL 
        )
    AND ( A.CREATION_DATE >= :CREATE_DATE_S OR :CREATE_DATE_S IS NULL )
    AND ( A.CREATION_DATE <= :CREATE_DATE_E OR :CREATE_DATE_E IS NULL )

    AND ( A.APPLY_DATE >= :APPLY_DATE_S OR :APPLY_DATE_S IS NULL )
    AND ( A.APPLY_DATE <= :APPLY_DATE_E OR :APPLY_DATE_E IS NULL )
    AND ( A.BATCH_WITHDRAWAL_ID = :BATCH_WITHDRAWAL_ID OR :BATCH_WITHDRAWAL_ID IS NULL )
";
                if (strOrg.ConvertToString() != string.Empty)
                {
                    switch (strOrg)
                    {
                        case "TW1":
                            strSQL += @"
    AND B.ORG_CODE IN ('TW1', 'IND')
";
                            break;
                        default:
                            strSQL += @"
    AND B.ORG_CODE = '" + strOrg + @"'
";
                            break;
                    }
                }
                return base.GetList(strSQL, new
                {
                    ORDER_NO = OrderNo,
                    CUST_NAME = strCustName,
                    CREATE_DATE_S = dteCreateDate_S,
                    CREATE_DATE_E = dteCreateDate_E,
                    APPLY_DATE_S = dteApplyDate_S,
                    APPLY_DATE_E = dteApplyDate_E,
                    BATCH_WITHDRAWAL_ID = BatchNo
                }, CommandType.Text).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }

        public List<BL_ACCOUNT_TXN_TEMP> GetDepositDataAll
         (
             string OrderNo,
             string strCustName,
             DateTime? dteCreateDate_S, DateTime? dteCreateDate_E,
             DateTime? dteApplyDate_S, DateTime? dteApplyDate_E,
             string BatchNo,
             out Exception exError
         )
        {
            exError = null;
            try
            {
                var strSQL = @"
SELECT A.*
    , B.ORDER_NO
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (C.CUST_CNAME) AS CUST_CNAME
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (C.CUST_CNAME2) AS CUST_CNAME2
FROM BL_ACCOUNT_TXN_TEMP A
    INNER JOIN BL_ORDER B
        ON A.ORDER_ID = B.ORDER_ID
            AND B.STATUS_CODE = '4'
    INNER JOIN BL_CUST C
        ON B.CUST_ID = C.CUST_ID
WHERE 1 = 1
    AND A.SCHEDULE <> '4'
";
                return base.GetList(strSQL, null, CommandType.Text).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }



        public List<BL_ACCOUNT_TXN_TEMP> GetDepositData_UnApprove(string strORG, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
SELECT A.*
    , BL_INTEREST2_PKG.RETURN_BALANCE(a.ORDER_ID) AS BALANCE
    , B.ORDER_NO
    , B.FROM_DATE
    , B.YEAR
    , CASE 
        WHEN CUST_CNAME2 IS NOT NULL 
            THEN BL_CRYPT_PKG.DECRYPT_DES_NOKEY(CUST_CNAME)||'/'||BL_CRYPT_PKG.DECRYPT_DES_NOKEY(CUST_CNAME2) 
            ELSE BL_CRYPT_PKG.DECRYPT_DES_NOKEY(CUST_CNAME) 
        END CUST_CNAME
    , CASE 
        WHEN CUST_ENAME2 IS NOT NULL 
            THEN BL_CRYPT_PKG.DECRYPT_DES_NOKEY(CUST_ENAME)||'/'||BL_CRYPT_PKG.DECRYPT_DES_NOKEY(CUST_ENAME2) 
            ELSE BL_CRYPT_PKG.DECRYPT_DES_NOKEY(CUST_ENAME) 
        END CUST_ENAME
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (C.CUST_CNAME2) AS CUST_CNAME2
    , B.AMOUNT_USD AS ORDER_AMOUNT_USD
    , B.AMOUNT_NTD AS ORDER_AMOUNT_NTD
    , B.AMOUNT_RMB AS ORDER_AMOUNT_RMB
    , B.AMOUNT_EUR AS ORDER_AMOUNT_EUR
    , B.AMOUNT_AUD AS ORDER_AMOUNT_AUD
    , B.AMOUNT_JPY AS ORDER_AMOUNT_JPY
    , B.AMOUNT_NZD AS ORDER_AMOUNT_NZD
    , B.ORG_CODE
FROM BL_ACCOUNT_TXN_TEMP A
    INNER JOIN BL_ORDER B
        ON A.ORDER_ID = B.ORDER_ID
            AND B.STATUS_CODE = '4'
    INNER JOIN BL_CUST C
        ON B.CUST_ID = C.CUST_ID
WHERE 1 = 1
    AND A.SCHEDULE = '3'
";
                if (strORG.ConvertToString() != string.Empty)
                {
                    switch (strORG)
                    {
                        case "TW1":
                            strSQL += @"
    AND B.ORG_CODE IN ('TW1', 'IND')
";
                            break;
                        default:
                            strSQL += @"
    AND B.ORG_CODE = '" + strORG + @"'
";
                            break;
                    }
                }

                return base.GetList(strSQL, null, CommandType.Text).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }


        public List<BL_ACCOUNT_TXN_TEMP> GetDepositData_NoTempRecord(string strORG, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
SELECT A.*
    , BL_INTEREST2_PKG.RETURN_BALANCE(a.ORDER_ID) AS BALANCE
    , B.ORDER_NO
    , B.FROM_DATE
    , B.YEAR
    , CASE 
        WHEN CUST_CNAME2 IS NOT NULL 
            THEN BL_CRYPT_PKG.DECRYPT_DES_NOKEY(CUST_CNAME)||'/'||BL_CRYPT_PKG.DECRYPT_DES_NOKEY(CUST_CNAME2) 
            ELSE BL_CRYPT_PKG.DECRYPT_DES_NOKEY(CUST_CNAME) 
        END CUST_CNAME
    , CASE 
        WHEN CUST_ENAME2 IS NOT NULL 
            THEN BL_CRYPT_PKG.DECRYPT_DES_NOKEY(CUST_ENAME)||'/'||BL_CRYPT_PKG.DECRYPT_DES_NOKEY(CUST_ENAME2) 
            ELSE BL_CRYPT_PKG.DECRYPT_DES_NOKEY(CUST_ENAME) 
        END CUST_ENAME
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (C.CUST_CNAME2) AS CUST_CNAME2
    , B.AMOUNT_USD AS ORDER_AMOUNT_USD
    , B.AMOUNT_NTD AS ORDER_AMOUNT_NTD
    , B.AMOUNT_RMB AS ORDER_AMOUNT_RMB
    , B.AMOUNT_EUR AS ORDER_AMOUNT_EUR
    , B.AMOUNT_AUD AS ORDER_AMOUNT_AUD
    , B.AMOUNT_JPY AS ORDER_AMOUNT_JPY
    , B.AMOUNT_NZD AS ORDER_AMOUNT_NZD
    , B.ORG_CODE
FROM BL_ACCOUNT_TXN_TEMP A
    INNER JOIN BL_ORDER B
        ON A.ORDER_ID = B.ORDER_ID
            AND B.STATUS_CODE = '4'
    INNER JOIN BL_CUST C
        ON B.CUST_ID = C.CUST_ID
WHERE 1 = 1
    AND A.SCHEDULE = '4'
";
                if (strORG.ConvertToString() != string.Empty)
                {
                    switch (strORG)
                    {
                        case "TW1":
                            strSQL += @"
    AND B.ORG_CODE IN ('TW1', 'IND')
";
                            break;
                        default:
                            strSQL += @"
    AND B.ORG_CODE = '" + strORG + @"'
";
                            break;
                    }
                }

                return base.GetList(strSQL, null, CommandType.Text).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }


        public List<BL_ACCOUNT_TXN_ERRLOG> GetDepositErrData(string strORG, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
SELECT A.*
    , BL_INTEREST2_PKG.RETURN_BALANCE(a.ORDER_ID) AS BALANCE
    , B.ORDER_NO
    , B.FROM_DATE
    , B.YEAR
    , CASE 
        WHEN CUST_CNAME2 IS NOT NULL 
            THEN BL_CRYPT_PKG.DECRYPT_DES_NOKEY(CUST_CNAME)||'/'||BL_CRYPT_PKG.DECRYPT_DES_NOKEY(CUST_CNAME2) 
            ELSE BL_CRYPT_PKG.DECRYPT_DES_NOKEY(CUST_CNAME) 
        END CUST_CNAME
    , CASE 
        WHEN CUST_ENAME2 IS NOT NULL 
            THEN BL_CRYPT_PKG.DECRYPT_DES_NOKEY(CUST_ENAME)||'/'||BL_CRYPT_PKG.DECRYPT_DES_NOKEY(CUST_ENAME2) 
            ELSE BL_CRYPT_PKG.DECRYPT_DES_NOKEY(CUST_ENAME) 
        END CUST_ENAME
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (C.CUST_CNAME2) AS CUST_CNAME2
    , B.AMOUNT_USD AS ORDER_AMOUNT_USD
    , B.AMOUNT_NTD AS ORDER_AMOUNT_NTD
    , B.AMOUNT_RMB AS ORDER_AMOUNT_RMB
    , B.AMOUNT_EUR AS ORDER_AMOUNT_EUR
    , B.AMOUNT_AUD AS ORDER_AMOUNT_AUD
    , B.AMOUNT_JPY AS ORDER_AMOUNT_JPY
    , B.AMOUNT_NZD AS ORDER_AMOUNT_NZD
    , B.ORG_CODE
FROM BL_ACCOUNT_TXN_ERRLOG A
    INNER JOIN BL_ORDER B
        ON A.ORDER_ID = B.ORDER_ID
            AND B.STATUS_CODE = '4'
    INNER JOIN BL_CUST C
        ON B.CUST_ID = C.CUST_ID
WHERE 1 = 1
    
";
                if (strORG.ConvertToString() != string.Empty)
                {
                    switch (strORG)
                    {
                        case "TW1":
                            strSQL += @"
    AND B.ORG_CODE IN ('TW1', 'IND')
";
                            break;
                        default:
                            strSQL += @"
    AND B.ORG_CODE = '" + strORG + @"'
";
                            break;
                    }
                }

                return base.Connection.Query<BL_ACCOUNT_TXN_ERRLOG>(strSQL, null).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }



        public List<BL_ACCOUNT_TXN_TEMP> GetDepositData(string strYear, string strMonth, out Exception exError)
        {
            exError = null;
            try
            {
                var strBaseDate_S = strYear + "/" + strMonth + "/01 00:00:00";
                var strBaseDate_E = DateTime.Parse(strBaseDate_S).AddMonths(1).ToString("yyyy/MM/dd 00:00:00");
                var strSQL = @"
SELECT
    A.*
    , B.ORDER_NO 
    , B.AMOUNT_USD AS ORDER_AMOUNT_USD 
    , B.AMOUNT_NTD AS ORDER_AMOUNT_NTD 
    , B.AMOUNT_RMB AS ORDER_AMOUNT_RMB 
    , B.AMOUNT_EUR AS ORDER_AMOUNT_EUR 
    , B.AMOUNT_AUD AS ORDER_AMOUNT_AUD
    , B.AMOUNT_JPY AS ORDER_AMOUNT_JPY
    , B.ORG_CODE
FROM BL_ACCOUNT_TXN_TEMP A
    INNER JOIN BL_ORDER B
        ON A.ORDER_ID = B.ORDER_ID
            AND B.STATUS_CODE = '4'
WHERE 
    A.APPLY_DATE >= :BaseDate_S
    AND A.APPLY_DATE < :BaseDate_E
";
                return base.GetList(strSQL, new
                {
                    BaseDate_S = DateTime.Parse(strBaseDate_S),
                    BaseDate_E = DateTime.Parse(strBaseDate_E)
                }, CommandType.Text).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }

        public bool AddNewData(BL_ACCOUNT_TXN_TEMP dataSave, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
INSERT INTO BL_ACCOUNT_TXN_TEMP
(
      ORDER_ID
    , AMOUNT_USD
    , AMOUNT_NTD
    , AMOUNT_RMB
    , AMOUNT_EUR
    , AMOUNT_AUD
    , AMOUNT_JPY
    , APPLY_DATE
    , CHECK_DATE
    , APPROVE_DATE
    , CREATION_DATE
    , LAST_UPDATE_DATE
    , SCHEDULE
    , WITHDRAWAL_TYPE
    , BONUS_TSF
    , NEW_CONTRACT_NO
    , PROCESS_TYPE
    , TEMP_ID
)
SELECT 
      :ORDER_ID
    , :AMOUNT_USD
    , :AMOUNT_NTD
    , :AMOUNT_RMB
    , :AMOUNT_EUR
    , :AMOUNT_AUD
    , :AMOUNT_JPY
    , :APPLY_DATE
    , :CHECK_DATE
    , :APPROVE_DATE
    , SYSDATE
    , SYSDATE
    , '0'   --暫存
    , :WITHDRAWAL_TYPE
    , :BONUS_TSF
    , :NEW_CONTRACT_NO
    , :PROCESS_TYPE
    , BL_ACCOUNT_TXN_TEMP_S1.NEXTVAL
FROM DUAL

";
                base.ExecuteScalar(strSQL, new
                {
                    ORDER_ID = dataSave.ORDER_ID,
                    AMOUNT_USD = dataSave.AMOUNT_USD,
                    AMOUNT_NTD = dataSave.AMOUNT_NTD,
                    AMOUNT_RMB = dataSave.AMOUNT_RMB,
                    AMOUNT_EUR = dataSave.AMOUNT_EUR,
                    AMOUNT_AUD = dataSave.AMOUNT_AUD,
                    AMOUNT_JPY = dataSave.AMOUNT_JPY,
                    APPLY_DATE = dataSave.APPLY_DATE,
                    CHECK_DATE = dataSave.CHECK_DATE,
                    APPROVE_DATE = dataSave.APPROVE_DATE,
                    WITHDRAWAL_TYPE = dataSave.WITHDRAWAL_TYPE,
                    BONUS_TSF = dataSave.BONUS_TSF,
                    NEW_CONTRACT_NO = dataSave.NEW_CONTRACT_NO,
                    PROCESS_TYPE = dataSave.PROCESS_TYPE
                }, CommandType.Text);
                return true;
            }
            catch (Exception ex)
            {
                exError = ex;
                return false;
            }
        }


        public bool AddNewDataByList(List<BL_ACCOUNT_TXN_TEMP> dataSave, out Exception exError)
        {
            exError = null;
            try
            {
                var param = new DynamicParameters();
                var strSQL = string.Empty;
                int i = 0;

                dataSave.ForEach(x =>
                {
                    strSQL += @"
                INSERT INTO BL_ACCOUNT_TXN_TEMP
                (
                      ORDER_ID
                    , AMOUNT_USD
                    , AMOUNT_NTD
                    , AMOUNT_RMB
                    , AMOUNT_EUR
                    , AMOUNT_AUD
                    , AMOUNT_JPY
                    , AMOUNT_NZD
                    , APPLY_DATE
                    , CHECK_DATE
                    , APPROVE_DATE
                    , CREATION_DATE
                    , LAST_UPDATE_DATE
                    , SCHEDULE
                    , BATCH_WITHDRAWAL_ID
                    , BONUS_TSF
                    , NEW_CONTRACT_NO
                    , WITHDRAWAL_TYPE
                    , PROCESS_TYPE
                    , END_BALANCE_FLAG
                    , APPLY_USER
                    ,  TXN_DATE
                    , FLOW_TYPE
                    , TXN_TYPE
                    , REMARK
                    , TEMP_ID
                    , ACTUAL_WITHDRAWAL_AMT_USD
                    , ACTUAL_WITHDRAWAL_AMT_NTD
                    , ACTUAL_WITHDRAWAL_AMT_RMB
                    , ACTUAL_WITHDRAWAL_AMT_EUR
                    , ACTUAL_WITHDRAWAL_AMT_AUD
                    , ACTUAL_WITHDRAWAL_AMT_JPY
                    , ACTUAL_WITHDRAWAL_AMT_NZD
                )
                SELECT 
                      :ORDER_ID" + i.ConvertToString() + @"
                    , :AMOUNT_USD" + i.ConvertToString() + @"
                    , :AMOUNT_NTD" + i.ConvertToString() + @"
                    , :AMOUNT_RMB" + i.ConvertToString() + @"
                    , :AMOUNT_EUR" + i.ConvertToString() + @"
                    , :AMOUNT_AUD" + i.ConvertToString() + @"
                    , :AMOUNT_JPY" + i.ConvertToString() + @"
                    , :AMOUNT_NZD" + i.ConvertToString() + @"
                    , :APPLY_DATE" + i.ConvertToString() + @"
                    , :CHECK_DATE" + i.ConvertToString() + @"
                    , :APPROVE_DATE" + i.ConvertToString() + @"
                    , SYSDATE
                    , SYSDATE
                    , '0'   --暫存
                    , :BATCH_WITHDRAWAL_ID" + i.ConvertToString() + @"
                    , :BONUS_TSF" + i.ConvertToString() + @"
                    , :NEW_CONTRACT_NO" + i.ConvertToString() + @"
                    , :WITHDRAWAL_TYPE" + i.ConvertToString() + @"
                    , :PROCESS_TYPE" + i.ConvertToString() + @"
                    , :END_BALANCE_FLAG" + i.ConvertToString() + @"
                    , :APPLY_USER" + i.ConvertToString() + @"
                    , :TXN_DATE" + i.ConvertToString() + @"
                    , 'OUT'
                    , 'WITHDRAWAL'
                    , :REMARK" + i.ConvertToString() + @"
                    , BL_ACCOUNT_TXN_TEMP_S1.NEXTVAL
                    , :ACTUAL_WITHDRAWAL_AMT_USD" + i.ConvertToString() + @"
                    , :ACTUAL_WITHDRAWAL_AMT_NTD" + i.ConvertToString() + @"
                    , :ACTUAL_WITHDRAWAL_AMT_RMB" + i.ConvertToString() + @"
                    , :ACTUAL_WITHDRAWAL_AMT_EUR" + i.ConvertToString() + @"
                    , :ACTUAL_WITHDRAWAL_AMT_AUD" + i.ConvertToString() + @"
                    , :ACTUAL_WITHDRAWAL_AMT_JPY" + i.ConvertToString() + @"
                    , :ACTUAL_WITHDRAWAL_AMT_NZD" + i.ConvertToString() + @"
                FROM DUAL ; 
                ";
                    param.Add("ORDER_ID" + i.ConvertToString(), x.ORDER_ID);
                    param.Add("AMOUNT_USD" + i.ConvertToString(), x.AMOUNT_USD);
                    param.Add("AMOUNT_NTD" + i.ConvertToString(), x.AMOUNT_NTD);
                    param.Add("AMOUNT_RMB" + i.ConvertToString(), x.AMOUNT_RMB);
                    param.Add("AMOUNT_EUR" + i.ConvertToString(), x.AMOUNT_EUR);
                    param.Add("AMOUNT_AUD" + i.ConvertToString(), x.AMOUNT_AUD);
                    param.Add("AMOUNT_JPY" + i.ConvertToString(), x.AMOUNT_JPY);
                    param.Add("AMOUNT_NZD" + i.ConvertToString(), x.AMOUNT_NZD);
                    param.Add("APPLY_DATE" + i.ConvertToString(), x.APPLY_DATE);
                    param.Add("CHECK_DATE" + i.ConvertToString(), x.CHECK_DATE);
                    param.Add("APPROVE_DATE" + i.ConvertToString(), x.APPROVE_DATE);
                    param.Add("BATCH_WITHDRAWAL_ID" + i.ConvertToString(), x.BATCH_WITHDRAWAL_ID);
                    param.Add("BONUS_TSF" + i.ConvertToString(), x.BONUS_TSF);
                    param.Add("NEW_CONTRACT_NO" + i.ConvertToString(), x.NEW_CONTRACT_NO);
                    param.Add("WITHDRAWAL_TYPE" + i.ConvertToString(), x.WITHDRAWAL_TYPE);
                    param.Add("PROCESS_TYPE" + i.ConvertToString(), x.PROCESS_TYPE);
                    param.Add("END_BALANCE_FLAG" + i.ConvertToString(), x.END_BALANCE_FLAG);
                    param.Add("APPLY_USER" + i.ConvertToString(), x.APPLY_USER);
                    param.Add("TXN_DATE" + i.ConvertToString(), x.TXN_DATE);
                    param.Add("REMARK" + i.ConvertToString(), x.REMARK);
                    param.Add("ACTUAL_WITHDRAWAL_AMT_USD" + i.ConvertToString(), x.ACTUAL_WITHDRAWAL_AMT_USD);
                    param.Add("ACTUAL_WITHDRAWAL_AMT_NTD" + i.ConvertToString(), x.ACTUAL_WITHDRAWAL_AMT_NTD);
                    param.Add("ACTUAL_WITHDRAWAL_AMT_RMB" + i.ConvertToString(), x.ACTUAL_WITHDRAWAL_AMT_RMB);
                    param.Add("ACTUAL_WITHDRAWAL_AMT_EUR" + i.ConvertToString(), x.ACTUAL_WITHDRAWAL_AMT_EUR);
                    param.Add("ACTUAL_WITHDRAWAL_AMT_AUD" + i.ConvertToString(), x.ACTUAL_WITHDRAWAL_AMT_AUD);
                    param.Add("ACTUAL_WITHDRAWAL_AMT_JPY" + i.ConvertToString(), x.ACTUAL_WITHDRAWAL_AMT_JPY);
                    param.Add("ACTUAL_WITHDRAWAL_AMT_NZD" + i.ConvertToString(), x.ACTUAL_WITHDRAWAL_AMT_NZD);
                    i += 1;

                });
                
                    strSQL = " BEGIN " + strSQL + " END; ";

                base.ExecuteScalar(strSQL, param, CommandType.Text);
                return true;
            }
            catch (Exception ex)
            {
                exError = ex;
                return false;
            }
        }

        public bool ResendDeposit(BL_ACCOUNT_TXN_TEMP dataSave, out Exception exError)
        {
            exError = null;
            try
            {
                //  2019/07/10 > 只要修改重送, 一律回到新單重送的狀態
                var strSQL = @"
UPDATE BL_ACCOUNT_TXN_TEMP
SET
    SCHEDULE = '0'
    , LAST_UPDATE_DATE = SYSDATE

    , AMOUNT_USD = :AMOUNT_USD
    , AMOUNT_NTD = :AMOUNT_NTD
    , AMOUNT_RMB = :AMOUNT_RMB
    , AMOUNT_EUR = :AMOUNT_EUR
    , AMOUNT_AUD = :AMOUNT_AUD
    , AMOUNT_JPY = :AMOUNT_JPY
    , AMOUNT_NZD = :AMOUNT_NZD
    , APPLY_DATE = :APPLY_DATE
    , BONUS_TSF = :BONUS_TSF
    , NEW_CONTRACT_NO = :NEW_CONTRACT_NO
    , WITHDRAWAL_TYPE = :WITHDRAWAL_TYPE
    , PROCESS_TYPE = :PROCESS_TYPE
    , END_BALANCE_FLAG = :END_BALANCE_FLAG
    , APPLY_USER = :APPLY_USER
    , TXN_DATE = :TXN_DATE
    , REMARK = :REMARK
    , ACTUAL_WITHDRAWAL_AMT_USD = :ACTUAL_WITHDRAWAL_AMT_USD
    , ACTUAL_WITHDRAWAL_AMT_NTD = :ACTUAL_WITHDRAWAL_AMT_NTD
    , ACTUAL_WITHDRAWAL_AMT_RMB = :ACTUAL_WITHDRAWAL_AMT_RMB
    , ACTUAL_WITHDRAWAL_AMT_EUR = :ACTUAL_WITHDRAWAL_AMT_EUR
    , ACTUAL_WITHDRAWAL_AMT_AUD = :ACTUAL_WITHDRAWAL_AMT_AUD
    , ACTUAL_WITHDRAWAL_AMT_JPY = :ACTUAL_WITHDRAWAL_AMT_JPY
    , ACTUAL_WITHDRAWAL_AMT_NZD = :ACTUAL_WITHDRAWAL_AMT_NZD
WHERE ORDER_ID = :ORDER_ID
    AND BATCH_WITHDRAWAL_ID = :BATCH_WITHDRAWAL_ID
";
                this.Connection.Execute(strSQL, new
                {
                    ORDER_ID = dataSave.ORDER_ID,
                    BATCH_WITHDRAWAL_ID = dataSave.BATCH_WITHDRAWAL_ID,
                    AMOUNT_USD = dataSave.AMOUNT_USD,
                    AMOUNT_NTD = dataSave.AMOUNT_NTD,
                    AMOUNT_RMB = dataSave.AMOUNT_RMB,
                    AMOUNT_EUR = dataSave.AMOUNT_EUR,
                    AMOUNT_AUD = dataSave.AMOUNT_AUD,
                    AMOUNT_JPY = dataSave.AMOUNT_JPY,
                    AMOUNT_NZD = dataSave.AMOUNT_NZD,
                    APPLY_DATE = dataSave.APPLY_DATE,
                    BONUS_TSF = dataSave.BONUS_TSF,
                    NEW_CONTRACT_NO = dataSave.NEW_CONTRACT_NO,
                    WITHDRAWAL_TYPE = dataSave.WITHDRAWAL_TYPE,
                    PROCESS_TYPE = dataSave.PROCESS_TYPE,
                    END_BALANCE_FLAG = dataSave.END_BALANCE_FLAG,
                    APPLY_USER = dataSave.APPLY_USER,
                    TXN_DATE = dataSave.TXN_DATE,
                    REMARK = dataSave.REMARK,
                    ACTUAL_WITHDRAWAL_AMT_USD = dataSave.ACTUAL_WITHDRAWAL_AMT_USD,
                    ACTUAL_WITHDRAWAL_AMT_NTD = dataSave.ACTUAL_WITHDRAWAL_AMT_NTD,
                    ACTUAL_WITHDRAWAL_AMT_RMB = dataSave.ACTUAL_WITHDRAWAL_AMT_RMB,
                    ACTUAL_WITHDRAWAL_AMT_EUR = dataSave.ACTUAL_WITHDRAWAL_AMT_EUR,
                    ACTUAL_WITHDRAWAL_AMT_AUD = dataSave.ACTUAL_WITHDRAWAL_AMT_AUD,
                    ACTUAL_WITHDRAWAL_AMT_JPY = dataSave.ACTUAL_WITHDRAWAL_AMT_JPY,
                    ACTUAL_WITHDRAWAL_AMT_NZD = dataSave.ACTUAL_WITHDRAWAL_AMT_NZD
                });
                return true;
            }
            catch (Exception ex)
            {
                exError = ex;
                return false;
            }
        }

        public bool DataSubmit(List<string> dataOrderID, string strUserName, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = string.Empty;
                var dymParam = new DynamicParameters();
                int i = 1;
                dataOrderID.ForEach(x =>
                {
                    strSQL += @"
UPDATE BL_ACCOUNT_TXN_TEMP A
SET
      A.LAST_UPDATE_DATE = SYSDATE
    , A.SCHEDULE = '4'
    , A.APPROVE_DATE = SYSDATE
    , A.APPROVED_FLAG = 'Y'
    , A.APPROVER = :USER_NAME
WHERE A.ORDER_ID = :ORDER_ID" + i.ConvertToString() + @"
;
";
                    dymParam.Add("ORDER_ID" + i.ConvertToString(), x);
                    i += 1;
                });
                if (strSQL != string.Empty)
                {
                    dymParam.Add("USER_NAME", strUserName);
                    strSQL = " BEGIN " + strSQL + " END ; ";
                    base.ExecuteScalar(strSQL, dymParam, CommandType.Text);
                }
                return true;
            }
            catch (Exception ex)
            {
                exError = ex;
                return false;
            }
        }


        public bool DataSubmit_TEMPID(List<string> dataTempID, string strUserName, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = string.Empty;
                var dymParam = new DynamicParameters();
                int i = 1;
                dataTempID.ForEach(x =>
                {
                    strSQL += @"
UPDATE BL_ACCOUNT_TXN_TEMP A
SET
      A.LAST_UPDATE_DATE = SYSDATE
    , A.SCHEDULE = '4'
    , A.APPROVE_DATE = SYSDATE
    , A.APPROVED_FLAG = 'Y'
    , A.APPROVER = :USER_NAME
WHERE A.TEMP_ID = :TEMP_ID" + i.ConvertToString() + @"
;
";
                    dymParam.Add("TEMP_ID" + i.ConvertToString(), x);
                    i += 1;
                });
                if (strSQL != string.Empty)
                {
                    dymParam.Add("USER_NAME", strUserName);
                    strSQL = " BEGIN " + strSQL + " END ; ";
                    base.ExecuteScalar(strSQL, dymParam, CommandType.Text);
                }
                return true;
            }
            catch (Exception ex)
            {
                exError = ex;
                return false;
            }
        }


        public bool DataAudit(List<string> dataTempID, string strUserName, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = string.Empty;
                var dymParam = new DynamicParameters();
                int i = 1;
                dataTempID.ForEach(x =>
                {
                    strSQL += @"
UPDATE BL_ACCOUNT_TXN_TEMP A
SET
      A.LAST_UPDATE_DATE = SYSDATE
    , A.SCHEDULE = '3'
    , A.CHECK_DATE = SYSDATE
    , A.CHECKED_FLAG  = 'Y'
    , A.CHECK_USER = :USER_NAME
WHERE A.TEMP_ID = :TEMP_ID" + i.ConvertToString() + @"
;
";
                    dymParam.Add("TEMP_ID" + i.ConvertToString(), x);
                    i += 1;
                });
                if (strSQL != string.Empty)
                {
                    dymParam.Add("USER_NAME", strUserName);
                    strSQL = " BEGIN " + strSQL + " END ; ";
                    base.ExecuteScalar(strSQL, dymParam, CommandType.Text);
                }
                return true;
            }
            catch (Exception ex)
            {
                exError = ex;
                return false;
            }
        }

        public bool DataConfirm_SalesApply(List<string> dataTempID, string strUserName, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = string.Empty;
                var dymParam = new DynamicParameters();
                int i = 1;
                dataTempID.ForEach(x =>
                {
                    strSQL += @"
UPDATE BL_ACCOUNT_TXN_TEMP A
SET
      A.LAST_UPDATE_DATE = SYSDATE
    , A.SCHEDULE = '0'
    , A.SALES_APPLY_CONFIRM_DATE = SYSDATE
    , A.SALES_APPLY_CONFIRM_FLAG  = 'Y'
    , A.SALES_APPLY_CONFIRM_USER = :USER_NAME
WHERE A.TEMP_ID = :TEMP_ID" + i.ConvertToString() + @"
;
";
                    dymParam.Add("TEMP_ID" + i.ConvertToString(), x);
                    i += 1;
                });
                if (strSQL != string.Empty)
                {
                    dymParam.Add("USER_NAME", strUserName);
                    strSQL = " BEGIN " + strSQL + " END ; ";
                    base.ExecuteScalar(strSQL, dymParam, CommandType.Text);
                }
                return true;
            }
            catch (Exception ex)
            {
                exError = ex;
                return false;
            }
        }


        public bool DataDelete(List<string> dataTempID, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = string.Empty;
                var dymParam = new DynamicParameters();
                int i = 1;
                dataTempID.ForEach(x =>
                {
                    strSQL += @"
DELETE BL_ACCOUNT_TXN_TEMP 
WHERE TEMP_ID = :TEMP_ID" + i.ConvertToString() + @"
;
";
                    dymParam.Add("TEMP_ID" + i.ConvertToString(), x);
                    i += 1;
                });
                if (strSQL != string.Empty)
                {
                    strSQL = " BEGIN " + strSQL + " END ; ";
                    base.ExecuteScalar(strSQL, dymParam, CommandType.Text);
                }
                return true;
            }
            catch (Exception ex)
            {
                exError = ex;
                return false;
            }
        }


        public List<BL_ACCOUNT_TXN_TEMP> GetDataByOrderID(List<string> dataOrderID, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
SELECT *
FROM BL_ACCOUNT_TXN_TEMP
WHERE ORDER_ID IN :ORDER_ID
";
                return base.GetList(strSQL, new
                {
                    ORDER_ID = dataOrderID
                }, CommandType.Text).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }


        public List<BL_ACCOUNT_TXN_TEMP> GetDataByTempID(List<string> dataTempID, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
SELECT *
FROM BL_ACCOUNT_TXN_TEMP
WHERE TEMP_ID IN :TEMP_ID
";
                return base.GetList(strSQL, new
                {
                    TEMP_ID = dataTempID
                }, CommandType.Text).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }

        public List<BL_ACCOUNT_TXN_TEMP> GetBatchDepositDataByListOrderID(List<decimal> liOrderID, out Exception exError)
        {
            exError = null;
            try
            {
                //  Todo > Call Package > BALANCE > BL_INTEREST_PKG.RETURN_BALANCE
                var strSQL = @"
                SELECT
                    A.ORDER_ID
                    , A.ORDER_NO
                    ,A.FROM_DATE
                    ,A.YEAR
                    , A.AMOUNT_USD AS ORDER_AMOUNT_USD
                    , A.AMOUNT_NTD AS ORDER_AMOUNT_NTD
                    , A.AMOUNT_RMB AS ORDER_AMOUNT_RMB
                    , A.AMOUNT_EUR AS ORDER_AMOUNT_EUR
                    , A.AMOUNT_AUD AS ORDER_AMOUNT_AUD
                    , A.AMOUNT_JPY AS ORDER_AMOUNT_JPY
                    , A.AMOUNT_NZD AS ORDER_AMOUNT_NZD
                    ,BL_INTEREST2_PKG.RETURN_BALANCE(a.ORDER_ID) AS BALANCE
--,0 AS BALANCE
                    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_ENAME) as CUST_ENAME
                    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_ENAME2) as CUST_ENAME2
                    --, BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_CNAME) as CUST_CNAME
                    , CASE 
                        WHEN CUST_CNAME2 IS NOT NULL 
                            THEN BL_CRYPT_PKG.DECRYPT_DES_NOKEY(CUST_CNAME)||'/'||BL_CRYPT_PKG.DECRYPT_DES_NOKEY(CUST_CNAME2) 
                        ELSE BL_CRYPT_PKG.DECRYPT_DES_NOKEY(CUST_CNAME) 
                        END CUST_CNAME
                    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_CNAME2) as  CUST_CNAME2
                    , D.CNAME AS SALES_NAME
                    , TO_CHAR(SYSDATE,'YYYY/MM/DD') AS APPLY_DATE
                    , A.CUST_ID
                    --  Todo > 2019/04/03 這個有問題
                    , BL_REPORT_PKG.MAX_INT_TIMES(A.ORDER_ID) AS PKG_INTEREST_TIMES
                    , (select count(1) from BL_ACCOUNT_TXN_TEMP X where A.ORDER_ID = X.ORDER_ID AND X.PROCESS_TYPE = 'SCHEDULE' ) AS IN_PROCESS_CNT
                    , BL_REPORT_PKG.ORDER_INTEREST_MONTHLY(A.ORDER_ID) AS ORDER_INTEREST_MONTHLY
                    , A.ORG_CODE
                FROM BL_ORDER A
                    INNER JOIN BL_CUST B
                        ON A.CUST_ID = B.CUST_ID
                    LEFT JOIN BL_CUST_SALES C
                        ON A.CUST_ID = C.CUST_ID
                            AND (C.ACTIVE IS NULL OR C.ACTIVE = 'Y' )   --AND C.ACTIVE <> 'N'
                    LEFT JOIN BL_EMP D
                        ON C.IB_CODE = D.IB_CODE
                WHERE 1 = 1              
                    AND A.ORDER_ID IN :ORDER_ID
                    AND A.STATUS_CODE = '4'
";
                return base.GetList(strSQL, new
                {
                    ORDER_ID = liOrderID
                }, CommandType.Text).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }

        public List<BL_ACCOUNT_TXN_TEMP> GetOnPathDataByOrderID(decimal? Order_Id, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
SELECT X.* , Y.ORDER_NO
    , Y.ORG_CODE
FROM BL_ACCOUNT_TXN_TEMP X 
    INNER JOIN BL_ORDER Y
        ON X.ORDER_ID = Y.ORDER_ID
WHERE X.ORDER_ID = :ORDER_ID 
    AND X.PROCESS_TYPE = 'SCHEDULE'
";
                return base.GetList(strSQL, new
                {
                    ORDER_ID = Order_Id
                }, CommandType.Text).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }

        public List<BL_ACCOUNT_TXN_TEMP> GetTempUnApproceData(List<decimal> liOrderID, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
SELECT 
      A.*
    , B.ORDER_NO
    , B.ORG_CODE
FROM BL_ACCOUNT_TXN_TEMP A
    INNER JOIN BL_ORDER B 
        ON A.ORDER_ID = B.ORDER_ID
WHERE A.ORDER_ID IN :ORDER_ID
    AND A.SCHEDULE IN ('0', '3')
";
                return base.GetList(strSQL, new
                {
                    ORDER_ID = liOrderID
                }, CommandType.Text).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }

        public BL_ACCOUNT_TXN_TEMP GetResendData(string strOrderID, string BatchID, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
SELECT 
      T.*
    , BL_INTEREST2_PKG.RETURN_BALANCE(T.ORDER_ID) AS BALANCE
    --  Todo > 2019/04/03 這個有問題
    , BL_REPORT_PKG.MAX_INT_TIMES(T.ORDER_ID) AS PKG_INTEREST_TIMES


    , A.ORDER_NO
    , A.AMOUNT_USD AS ORDER_AMOUNT_USD
    , A.AMOUNT_NTD AS ORDER_AMOUNT_NTD
    , A.AMOUNT_RMB AS ORDER_AMOUNT_RMB
    , A.AMOUNT_EUR AS ORDER_AMOUNT_EUR
    , A.AMOUNT_AUD AS ORDER_AMOUNT_AUD
    , A.AMOUNT_JPY AS ORDER_AMOUNT_JPY
    , A.FROM_DATE
    , A.YEAR
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_ENAME) as CUST_ENAME
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_ENAME2) as CUST_ENAME2
    , CASE 
        WHEN CUST_CNAME2 IS NOT NULL 
            THEN BL_CRYPT_PKG.DECRYPT_DES_NOKEY(CUST_CNAME)||'/'||BL_CRYPT_PKG.DECRYPT_DES_NOKEY(CUST_CNAME2) 
        ELSE BL_CRYPT_PKG.DECRYPT_DES_NOKEY(CUST_CNAME) 
        END CUST_CNAME
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_CNAME2) as  CUST_CNAME2
    , D.CNAME AS SALES_NAME
    , A.CUST_ID

    , BL_REPORT_PKG.ORDER_INTEREST_MONTHLY(T.ORDER_ID) AS ORDER_INTEREST_MONTHLY
    , A.ORG_CODE

FROM BL_ACCOUNT_TXN_TEMP T
    INNER JOIN BL_ORDER A
        ON T.ORDER_ID = A.ORDER_ID
    INNER JOIN BL_CUST B
        ON A.CUST_ID = B.CUST_ID
    LEFT JOIN BL_CUST_SALES C
        ON A.CUST_ID = C.CUST_ID
            AND (C.ACTIVE IS NULL OR C.ACTIVE = 'Y' )   --AND C.ACTIVE <> 'N'
    LEFT JOIN BL_EMP D
        ON C.IB_CODE = D.IB_CODE
WHERE T.ORDER_ID = :ORDER_ID
    AND T.BATCH_WITHDRAWAL_ID = :BATCH_WITHDRAWAL_ID
";
                return base.GetSingleOrDefault(strSQL, new
                {
                    ORDER_ID = strOrderID,
                    BATCH_WITHDRAWAL_ID = BatchID
                }, CommandType.Text);
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }

        public BL_ACCOUNT_TXN_ERRLOG GetDepositData_Error_ByBatchID(string strOrderID, string BatchID, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
SELECT 
      T.*
    , BL_INTEREST2_PKG.RETURN_BALANCE(T.ORDER_ID) AS BALANCE
    --  Todo > 2019/04/03 這個有問題
    , BL_REPORT_PKG.MAX_INT_TIMES(T.ORDER_ID) AS PKG_INTEREST_TIMES


    , A.ORDER_NO
    , A.AMOUNT_USD AS ORDER_AMOUNT_USD
    , A.AMOUNT_NTD AS ORDER_AMOUNT_NTD
    , A.AMOUNT_RMB AS ORDER_AMOUNT_RMB
    , A.AMOUNT_EUR AS ORDER_AMOUNT_EUR
    , A.AMOUNT_AUD AS ORDER_AMOUNT_AUD
    , A.AMOUNT_JPY AS ORDER_AMOUNT_JPY
    , A.FROM_DATE
    , A.YEAR
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_ENAME) as CUST_ENAME
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_ENAME2) as CUST_ENAME2
    , CASE 
        WHEN CUST_CNAME2 IS NOT NULL 
            THEN BL_CRYPT_PKG.DECRYPT_DES_NOKEY(CUST_CNAME)||'/'||BL_CRYPT_PKG.DECRYPT_DES_NOKEY(CUST_CNAME2) 
        ELSE BL_CRYPT_PKG.DECRYPT_DES_NOKEY(CUST_CNAME) 
        END CUST_CNAME
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_CNAME2) as  CUST_CNAME2
    , D.CNAME AS SALES_NAME
    , A.CUST_ID
    , A.ORG_CODE

FROM BL_ACCOUNT_TXN_ERRLOG T
    INNER JOIN BL_ORDER A
        ON T.ORDER_ID = A.ORDER_ID
    INNER JOIN BL_CUST B
        ON A.CUST_ID = B.CUST_ID
    LEFT JOIN BL_CUST_SALES C
        ON A.CUST_ID = C.CUST_ID
            AND (C.ACTIVE IS NULL OR C.ACTIVE = 'Y' )   --AND C.ACTIVE <> 'N'
    LEFT JOIN BL_EMP D
        ON C.IB_CODE = D.IB_CODE
WHERE T.ORDER_ID = :ORDER_ID
    AND T.BATCH_WITHDRAWAL_ID = :BATCH_WITHDRAWAL_ID
";
                return base.Connection.Query<BL_ACCOUNT_TXN_ERRLOG>(strSQL, new
                {
                    ORDER_ID = strOrderID,
                    BATCH_WITHDRAWAL_ID = BatchID
                }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }


        public bool DataReject_TEMPID(List<string> dataTempID, string strUserName, string strRejectReason, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = string.Empty;
                var dymParam = new DynamicParameters();
                int i = 1;
                dataTempID.ForEach(x =>
                {
                    strSQL += @"
UPDATE BL_ACCOUNT_TXN_TEMP A
SET
      A.LAST_UPDATE_DATE = SYSDATE
    , A.LAST_UPDATED_BY = :USER_NAME
    , A.REJECT_REASON = :REJECT_REASON
    , A.SCHEDULE = '9'
WHERE A.TEMP_ID = :TEMP_ID" + i.ConvertToString() + @"
;
";
                    dymParam.Add("TEMP_ID" + i.ConvertToString(), x);
                    i += 1;
                });
                if (strSQL != string.Empty)
                {
                    dymParam.Add("USER_NAME", strUserName);
                    dymParam.Add("REJECT_REASON", strRejectReason);
                    strSQL = " BEGIN " + strSQL + " END ; ";
                    base.ExecuteScalar(strSQL, dymParam, CommandType.Text);
                }
                return true;
            }
            catch (Exception ex)
            {
                exError = ex;
                return false;
            }
        }

        public List<BL_ACCOUNT_TXN_TEMP> GetTempDataBySchedule(decimal OrderID, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
SELECT * 
FROM BL_ACCOUNT_TXN_TEMP X 
WHERE X.ORDER_ID = :ORDER_ID 
    AND X.PROCESS_TYPE = 'SCHEDULE'
";
                return base.GetList(strSQL, new
                {
                    ORDER_ID = OrderID
                }, CommandType.Text).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;    
            }
        }

        public bool UpdateRemarkByBatchNo(string strBatchNo, string strAfterRemark, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
UPDATE BL_ACCOUNT_TXN_TEMP A
SET
    A.REMARK = :REMARK
WHERE A.BATCH_WITHDRAWAL_ID = :BATCH_WITHDRAWAL_ID
";
                base.ExecuteScalar(strSQL, new
                {
                    BATCH_WITHDRAWAL_ID = strBatchNo,
                    REMARK = strAfterRemark
                }, CommandType.Text);
                return true;
            }
            catch (Exception ex)
            {
                exError = ex;
                return false;
            }
        }

    }
}
