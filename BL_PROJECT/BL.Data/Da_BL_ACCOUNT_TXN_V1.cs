﻿using MK.Demo.Model;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MK.Demo.Utility;
using Dapper;

namespace MK.Demo.Data
{
    public class Da_BL_ACCOUNT_TXN_V1 : DataRepository<BL_ACCOUNT_TXN_V1>
    {
        public Da_BL_ACCOUNT_TXN_V1(OracleConnection conn)
         : base(conn)
        { }




        public List<BL_ACCOUNT_TXN_V1> GetDepositData
            (
                string OrderNo,
                string strCustName,
                DateTime? dteCreateDate_S, DateTime? dteCreateDate_E,
                DateTime? dteApplyDate_S, DateTime? dteApplyDate_E,
                string BatchNo,
                string strORG,
                out Exception exError
            )
        {
            exError = null;
            try
            {
                var LAST_S = DateTime.Parse(DateTime.Now.AddDays(-7).ToString("yyyy/MM/dd"));
                var strSQL =
@"
SELECT 
      A.NEW_CONTRACT_NO
    , A.BONUS_TSF
    , A.WITHDRAWAL_TYPE
    , A.TXN_DATE
    , A.BATCH_WITHDRAWAL_ID 
    
    , A.ORDER_NO
    , A.FROM_DATE
    , A.END_DATE
    , A.EXTEND_DATE
    , A.BASE_AMOUNT 
    , A.AMOUNT_USD 
    , BL_INTEREST2_PKG.RETURN_BALANCE(A.ORDER_ID) AS BALANCE 
    --, 0 AS BALANCE
    , A.REMARK
    , A.BONUS_TSF 
    , A.END_BALANCE_FLAG 

    , A.NEW_CONTRACT_NO 
    , A.AMOUNT_NTD
    , A.AMOUNT_RMB
    , A.AMOUNT_EUR
    , A.AMOUNT_AUD
    , A.AMOUNT_JPY
    , A.AMOUNT_NZD
    , A.SALES
    , A.CNAME
    , A.ENAME
    , A.APPLY_USER
    , A.CHECK_USER
    , A.APPROVER
    , A.ORG_CODE
FROM BL_ACCOUNT_TXN_V1 A
WHERE APPROVED_FLAG = 'Y' 
    AND TXN_TYPE IN ('WITHDRAWAL' ,'END BALANCE')

";
                if (strORG.ConvertToString() != string.Empty)
                {
                    switch (strORG)
                    {
                        case "TW1":
                            strSQL += @"
    AND A.ORG_CODE IN ('TW1', 'IND')
";
                            break;
                        default:
                            strSQL += @"
    AND A.ORG_CODE = '" + strORG + @"'
";
                            break;
                    }
                }

                if
                    (
                        OrderNo.ConvertToString().Trim() != string.Empty ||
                        strCustName.ConvertToString().Trim() != string.Empty ||
                        dteCreateDate_S != null ||
                        dteCreateDate_E != null ||
                        dteApplyDate_S != null ||
                        dteApplyDate_E != null ||
                        BatchNo.ConvertToString().Trim() != string.Empty
                    )
                {
                    strSQL += @"
    AND ( UPPER(A.ORDER_NO) LIKE '%'||UPPER(:ORDER_NO)||'%' OR :ORDER_NO IS NULL )
    AND ( A.CNAME LIKE '%'||:CUST_NAME||'%' OR :CUST_NAME IS NULL )
    AND ( A.BATCH_WITHDRAWAL_ID = :BATCH_WITHDRAWAL_ID OR :BATCH_WITHDRAWAL_ID IS NULL )
    AND ( A.CREATION_DATE >= :CREATE_DATE_S OR :CREATE_DATE_S IS NULL )
    AND ( A.CREATION_DATE <= :CREATE_DATE_E OR :CREATE_DATE_E IS NULL )
    AND ( A.txn_date >= :APPLY_DATE_S OR :APPLY_DATE_S IS NULL )
    AND ( A.txn_date <= :APPLY_DATE_E OR :APPLY_DATE_E IS NULL )
";
                    return base.GetList(strSQL, new
                    {
                        ORDER_NO = OrderNo,
                        CUST_NAME = strCustName,
                        BATCH_WITHDRAWAL_ID = BatchNo,
                        CREATE_DATE_S = dteCreateDate_S,
                        CREATE_DATE_E = dteCreateDate_E,
                        APPLY_DATE_S = dteApplyDate_S,
                        APPLY_DATE_E = dteApplyDate_E
                    }, CommandType.Text).ToList();
                }
                else
                {
                    strSQL += " AND TRUNC(APPROVE_DATE)>=TRUNC(SYSDATE) ";
                    return base.GetList(strSQL, null, CommandType.Text).ToList();
                }

            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }


        public BL_ACCOUNT_TXN_V1 GetDepositData_Approved_ByBatchID(string strOrderID, string BatchID, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
SELECT 
      T.*
    , BL_INTEREST2_PKG.RETURN_BALANCE(T.ORDER_ID) AS BALANCE
    --  Todo > 2019/04/03 這個有問題
    , BL_REPORT_PKG.MAX_INT_TIMES(T.ORDER_ID) AS PKG_INTEREST_TIMES


    , A.ORDER_NO
    , A.AMOUNT_USD AS ORDER_AMOUNT_USD
    , A.AMOUNT_NTD AS ORDER_AMOUNT_NTD
    , A.AMOUNT_RMB AS ORDER_AMOUNT_RMB
    , A.AMOUNT_EUR AS ORDER_AMOUNT_EUR
    , A.AMOUNT_AUD AS ORDER_AMOUNT_AUD
    , A.FROM_DATE
    , A.YEAR
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_ENAME) as CUST_ENAME
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_ENAME2) as CUST_ENAME2
    , CASE 
        WHEN CUST_CNAME2 IS NOT NULL 
            THEN BL_CRYPT_PKG.DECRYPT_DES_NOKEY(CUST_CNAME)||'/'||BL_CRYPT_PKG.DECRYPT_DES_NOKEY(CUST_CNAME2) 
        ELSE BL_CRYPT_PKG.DECRYPT_DES_NOKEY(CUST_CNAME) 
        END CUST_CNAME
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_CNAME2) as  CUST_CNAME2
    , D.CNAME AS SALES_NAME
    , A.CUST_ID
                    

FROM BL_ACCOUNT_TXN_V1 T
    INNER JOIN BL_ORDER A
        ON T.ORDER_ID = A.ORDER_ID
    INNER JOIN BL_CUST B
        ON A.CUST_ID = B.CUST_ID
    LEFT JOIN BL_CUST_SALES C
        ON A.CUST_ID = C.CUST_ID
            AND (C.ACTIVE IS NULL OR C.ACTIVE = 'Y' )   --AND C.ACTIVE <> 'N'
    LEFT JOIN BL_EMP D
        ON C.IB_CODE = D.IB_CODE
WHERE T.ORDER_ID = :ORDER_ID
    AND T.BATCH_WITHDRAWAL_ID = :BATCH_WITHDRAWAL_ID
    AND T.APPROVED_FLAG = 'Y' 
    AND T.TXN_TYPE IN ('WITHDRAWAL' ,'END BALANCE')
";
                return base.GetSingleOrDefault(strSQL, new
                {
                    ORDER_ID = strOrderID,
                    BATCH_WITHDRAWAL_ID = BatchID
                }, CommandType.Text);
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }


    }
}
