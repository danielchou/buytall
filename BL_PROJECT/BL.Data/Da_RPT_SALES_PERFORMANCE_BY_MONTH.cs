﻿using MK.Demo.Model;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MK.Demo.Utility;
using Dapper;


namespace MK.Demo.Data
{
    public class Da_RPT_SALES_PERFORMANCE_BY_MONTH : DataRepository<RPT_SALES_PERFORMANCE_BY_MONTH>
    {
        OracleConnection _Conn;
        public Da_RPT_SALES_PERFORMANCE_BY_MONTH(OracleConnection conn)
         : base(conn)
        { _Conn = conn; }


        public List<RPT_SALES_PERFORMANCE_BY_MONTH> GetReportDataByMonth(string strBaseYM,string strORG, out Exception exError)
        {
            exError = null;
            try
            {
                var P_PREV_MOH = string.Empty;
                var P_THIS_MOH = string.Empty;
                DateTime dtTty;
                var strTry = string.Empty;
                if (strBaseYM.Length == 6)
                {
                    strTry = strBaseYM.Substring(0, 4) + "/" + strBaseYM.Substring(4, 2) + "/01";
                }
                else
                {
                    strTry = strBaseYM + "01";
                }

                var strOrgScript = string.Empty;
                if (strORG.ConvertToString() != string.Empty)
                {
                    switch (strORG)
                    {
                        case "TW1":
                            strOrgScript = @"
    AND ORG_CODE IN ('TW1', 'IND')
";
                            break;
                        default:
                            strOrgScript = @"
    AND ORG_CODE = '" + strORG + @"'
";
                            break;
                    }
                }

                    if (DateTime.TryParse(strTry, out dtTty))
                {
                    P_THIS_MOH = dtTty.ToString("yyyyMM");
                    P_PREV_MOH = dtTty.AddMonths(-1).ToString("yyyyMM");
                    var strSQL = @"

SELECT 
      OV1.SALES
    , OV1.IB_CODE
    , AMOUNT_USD
    , AMOUNT_NTD
    , AMOUNT_RMB
    , AMOUNT_EUR
    , AMOUNT_AUD
    , AMOUNT_USD2
    , AMOUNT_NTD2
    , AMOUNT_RMB2
    , AMOUNT_EUR2
    , AMOUNT_AUD2
    , :P_PREV_MOH AS P_PREV_MOH
    , :P_THIS_MOH AS P_THIS_MOH
FROM 
    (
        SELECT 
              O1.ATTRIBUTE1 SALES
            , O1.IB_CODE
            , SUM(O1.AMOUNT_USD) AMOUNT_USD
            , SUM(O1.AMOUNT_NTD) AMOUNT_NTD
            , SUM(O1.AMOUNT_RMB) AMOUNT_RMB 
            , SUM(O1.AMOUNT_EUR) AMOUNT_EUR 
            , SUM(O1.AMOUNT_AUD) AMOUNT_AUD 
        FROM BL_ORDER_V1 O1 
        WHERE NVL(O1.ORDER_END, SYSDATE) >= SYSDATE 
            AND TO_CHAR(O1.FROM_DATE,'YYYYMM') <= :P_PREV_MOH "+ strOrgScript + @"
        GROUP BY 
              O1.ATTRIBUTE1  
            , O1.IB_CODE
    ) OV1,
    (
        SELECT 
              O2.ATTRIBUTE1 SALES
            , O2.IB_CODE
            , SUM(O2.AMOUNT_USD) AMOUNT_USD2
            , SUM(O2.AMOUNT_NTD) AMOUNT_NTD2
            , SUM(O2.AMOUNT_RMB) AMOUNT_RMB2 
            , SUM(O2.AMOUNT_EUR) AMOUNT_EUR2 
            , SUM(O2.AMOUNT_AUD) AMOUNT_AUD2 
        FROM BL_ORDER_V1 O2 
        WHERE NVL(O2.ORDER_END, SYSDATE) >= SYSDATE 
            AND TO_CHAR(O2.FROM_DATE, 'YYYYMM') <= :P_THIS_MOH " + strOrgScript + @"
        GROUP BY 
              O2.ATTRIBUTE1  
            , O2.IB_CODE
    ) OV2
WHERE OV1.SALES = OV2.SALES
";
                    return base.GetList(strSQL, new
                    {
                        P_PREV_MOH = P_PREV_MOH,
                        P_THIS_MOH = P_THIS_MOH
                    }, CommandType.Text).ToList();
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }


    }
}
