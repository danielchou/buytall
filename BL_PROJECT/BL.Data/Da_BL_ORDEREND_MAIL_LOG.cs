﻿using MK.Demo.Model;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MK.Demo.Utility;
using Dapper;

namespace MK.Demo.Data
{
    public class Da_BL_ORDEREND_MAIL_LOG : DataRepository<BL_ORDEREND_MAIL_LOG>
    {
        public Da_BL_ORDEREND_MAIL_LOG(OracleConnection conn)
         : base(conn)
        { }


        public bool WriteMailLog(List<BL_ORDEREND_MAIL_LOG> data, string strJobID, string strUserName, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = string.Empty;
                var param = new DynamicParameters();
                int i = 0;

                data.ForEach(x =>
                {
                    strSQL += @"
INSERT INTO BL_ORDEREND_MAIL_LOG
(
      JOB_ID
    , CUST_ID
    , ORDEREND_MONTH
    , ORDERNO_DESC
    , PDF_LOCATION
    , LAST_UPDATE_DATE
    , LAST_UPDATED_BY
    , EMAIL
    , IS_TEST
)
SELECT            
      :JOB_ID
    , :CUST_ID" + i.ConvertToString() + @"
    , :ORDEREND_MONTH" + i.ConvertToString() + @"
    , :ORDERNO_DESC" + i.ConvertToString() + @"
    , :PDF_LOCATION" + i.ConvertToString() + @"
    , SYSDATE
    , :USER_NAME
    , :EMAIL" + i.ConvertToString() + @"
    , :IS_TEST" + i.ConvertToString() + @"
FROM DUAL;
";
                    param.Add("CUST_ID" + i.ConvertToString(), x.CUST_ID);
                    param.Add("ORDEREND_MONTH" + i.ConvertToString(), x.ORDEREND_MONTH);
                    param.Add("ORDERNO_DESC" + i.ConvertToString(), x.ORDERNO_DESC);
                    param.Add("PDF_LOCATION" + i.ConvertToString(), x.PDF_LOCATION);
                    param.Add("EMAIL" + i.ConvertToString(), x.EMAIL);
                    param.Add("IS_TEST" + i.ConvertToString(), x.IS_TEST);
                    i += 1;
                });

                if (strSQL != null)
                {
                    param.Add("USER_NAME", strUserName);
                    param.Add("JOB_ID", strJobID);
                    strSQL = "Begin " + strSQL + " End; ";
                    this.Connection.Execute(strSQL, param);
                }

                return true;
            }
            catch (Exception ex)
            {
                exError = ex;
                return false;
            }
        }


        public List<BL_ORDEREND_MAIL_LOG> GetAllBatchNoByUser(string strUserName, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
SELECT 
    DISTINCT JOB_ID
FROM BL_ORDEREND_MAIL_LOG A
WHERE LAST_UPDATED_BY = :USER_NAME
ORDER BY 1 DESC
";
                return base.GetList(strSQL, new
                {
                    USER_NAME = strUserName
                }, CommandType.Text).ToList();

            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }

        public List<BL_ORDEREND_MAIL_LOG> GetDataByBatchNo(string strBatchNo, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
    SELECT 
                  A.*
                , B.MAIL_TYPE_END
                , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(B.CUST_CNAME) AS CUST_NAME
                , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(B.CUST_CNAME2) AS CUST_NAME2
                , D.IB_CODE
                , D.CNAME AS SALES_NAME
                , CASE 
                    WHEN O.ORDER_NO LIKE '%-%' THEN 'NEW' 
                    ELSE 
                        CASE
                            WHEN TO_CHAR(EXTEND_DATE,'YYYYMM') = A.ORDEREND_MONTH THEN 'OLD_END'
                            ELSE 'OLD' 
                        END
                    END ORDER_TYPE


                , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(B.EMAIL_1) AS EMAIL_1
                , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(B.EMAIL_2) AS EMAIL_2

            FROM BL_ORDEREND_MAIL_LOG A
                INNER JOIN BL_CUST B
                    ON A.CUST_ID = B.CUST_ID 
                INNER JOIN BL_ORDER_V1 O
                    ON B.CUST_ID = O.CUST_ID
                        AND A.ORDERNO_DESC = O.ORDER_NO
                LEFT JOIN BL_CUST_SALES C
                    ON A.CUST_ID = C.CUST_ID
                LEFT JOIN BL_EMP D
                    ON C.IB_CODE = D.IB_CODE
            WHERE A.JOB_ID = :BATCH_NO  AND (c.ACTIVE IS NULL OR c.ACTIVE='Y')
            ORDER BY A.CUST_ID

";
                return base.GetList(strSQL, new
                {
                    BATCH_NO = strBatchNo
                }, CommandType.Text).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }


        public bool UpdateSendFlag(List<BL_ORDEREND_MAIL_LOG> data, string strUserName, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = string.Empty;
                var param = new DynamicParameters();
                int i = 0;

                data.ForEach(x =>
                {
                    strSQL += @"
UPDATE BL_ORDEREND_MAIL_LOG
SET
      JOB_STATUS = '4'
    , JOB_DATE = SYSDATE
WHERE JOB_ID = :JOB_ID" + i.ToString() + @"
    AND CUST_ID = :CUST_ID" + i.ToString() + @"
    AND ORDERNO_DESC = :ORDERNO_DESC" + i.ToString() + @" ;
";
                    param.Add("JOB_ID" + i.ToString(), x.JOB_ID);
                    param.Add("CUST_ID" + i.ToString(), x.CUST_ID);
                    param.Add("ORDERNO_DESC" + i.ToString(), x.ORDERNO_DESC);
                    i += 1;
                });

                if (strSQL != string.Empty)
                {
                    strSQL = "Begin " + strSQL + " End;";
                    this.Connection.Execute(strSQL, param);
                }
                return true;
            }
            catch (Exception ex)
            {
                exError = ex;
                return false;
            }
        }

    }
}
