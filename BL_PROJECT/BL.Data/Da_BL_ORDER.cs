﻿using MK.Demo.Model;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MK.Demo.Utility;
using Dapper;
using System.Net.Http.Headers;

namespace MK.Demo.Data
{

    public class Da_BL_ORDER : DataRepository<BL_ORDER>
    {
        OracleConnection _Conn;
        public Da_BL_ORDER(OracleConnection conn)
         : base(conn)
        { _Conn = conn; }




        public List<BL_ORDER> GetDataByCustID(string CustID, string CustName, out Exception exError)
        {
            exError = null;
            try
            {
                //  Todo > Call Package BL_INTEREST_PKG.RETURN_BALANCE
                var param = new DynamicParameters();

                var strSQL = @"
SELECT 
    A.*
    , D.IB_CODE
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_ENAME)  AS CUST_ENAME
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_CNAME) AS CUST_CNAME
    , D.CNAME AS SALES_NAME
    ,  BL_INTEREST2_PKG.RETURN_BALANCE(a.ORDER_ID) AS BALANCE
    --, 0 AS BALANCE
FROM BL_ORDER A
    INNER JOIN BL_CUST B
        ON A.CUST_ID = B.CUST_ID
    LEFT JOIN BL_CUST_SALES C
        ON A.CUST_ID = C.CUST_ID
            AND (C.ACTIVE IS NULL OR C.ACTIVE = 'Y' )   --AND C.ACTIVE <> 'N'
    LEFT JOIN BL_EMP D
        ON C.IB_CODE = D.IB_CODE
WHERE 1 = 1
    AND A.STATUS_CODE = '4'
";
                param.Add("CUST_ID", CustID);

                if (CustName != string.Empty)
                {
                    strSQL += @"
    AND 
    (
        A.CUST_ID = :CUST_ID
        OR BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_ENAME) LIKE '%'||:CUST_NAME||'%' 
        OR BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_CNAME) LIKE '%'||:CUST_NAME||'%' 
        OR BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_ENAME2) LIKE '%'||:CUST_NAME||'%' 
        OR BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_CNAME2) LIKE '%'||:CUST_NAME||'%' 
    )
";
                    param.Add("CUST_NAME", CustName);
                }
                else
                {
                    strSQL += @"
    AND A.CUST_ID = :CUST_ID
";
                }
                return base.GetList(strSQL, param, CommandType.Text).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }

        public bool InsertOrder(string CustID, BL_ORDER data, string seqOrder, string strUserName, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
BEGIN 
INSERT INTO
BL_ORDER
(
      ORDER_ID
    , ORDER_NO
    , CUST_ID
    , EMP_CODE
    , MT4_ACCOUNT
    , FROM_DATE
    , END_DATE
    , YEAR
    , NOTE
    , CREATION_DATE
    , LAST_UPDATE_DATE
    , IB_CODE
    , ORDER_END
    , AMOUNT_USD
    , AMOUNT_NTD
    , AMOUNT_RMB
    , AMOUNT_EUR
    , AMOUNT_AUD
    , AMOUNT_JPY
    , AMOUNT_NZD
    , EXTEND_DATE
    , OLD_AMOUNT_USD
    , OLD_AMOUNT_NTD
    , OLD_AMOUNT_RMB
    , OLD_AMOUNT_EUR
    , OLD_AMOUNT_AUD
    , OLD_AMOUNT_JPY
    , OLD_AMOUNT_NZD
    , PARENT_ORDER_NO 
    , INTEREST_USD
    , INTEREST_NTD
    , INTEREST_RMB
    , INTEREST_EUR
    , INTEREST_AUD
    , INTEREST_JPY
    , INTEREST_NZD
    , STATUS_CODE
    , ENTER_CURRENCY
    , ENTER_AMOUNT
    , AMOUNT
    , CURRENCY
    , ATTRIBUTE1
    , CREATED_BY
    , LAST_UPDATED_BY
    , INTEREST_RATE
    , PROJECT
    , WITHDRAWAL_MODE
    , MULTI_ORDER_FLAG
    , IS_REMIT_TO_AUS
    , ORG_CODE
    , DISCOUNT_AMOUNT
    , BONUS_OPTION
    , INTERNAL_IB_CODE
)
SELECT 
      :ORDER_ID
    , :ORDER_NO
    , :CUST_ID
    , :EMP_CODE
    , :MT4_ACCOUNT
    , :FROM_DATE
    , :END_DATE
    , :YEAR
    , :NOTE
    , SYSDATE
    , SYSDATE
    , :IB_CODE
    , :ORDER_END
    , :AMOUNT_USD
    , :AMOUNT_NTD
    , :AMOUNT_RMB
    , :AMOUNT_EUR
    , :AMOUNT_AUD
    , :AMOUNT_JPY
    , :AMOUNT_NZD
    , :EXTEND_DATE
    , :OLD_AMOUNT_USD
    , :OLD_AMOUNT_NTD
    , :OLD_AMOUNT_RMB
    , :OLD_AMOUNT_EUR
    , :OLD_AMOUNT_AUD
    , :OLD_AMOUNT_JPY
    , :OLD_AMOUNT_NZD
    , :PARENT_ORDER_NO 
    , :INTEREST_USD
    , :INTEREST_NTD
    , :INTEREST_RMB
    , :INTEREST_EUR
    , :INTEREST_AUD
    , :INTEREST_JPY
    , :INTEREST_NZD
    , '1'
    , :ENTER_CURRENCY
    , :ENTER_AMOUNT
    , :AMOUNT
    , :CURRENCY
    , :ATTRIBUTE1
    , :USER_NAME
    , :USER_NAME
    , :INTEREST_RATE
    , :PROJECT
    , :WITHDRAWAL_MODE
    , :MULTI_ORDER_FLAG
    , :IS_REMIT_TO_AUS
    , :ORG_CODE
    , :DISCOUNT_AMOUNT
    , :BONUS_OPTION
    , :INTERNAL_IB_CODE
FROM DUAL;

INSERT INTO BL_ORDER_UPDATE_LOG
(
    BL_ORDER_UPDATE_LOG_ID, 
    ORDER_ID, 
    ORDER_NO, 
    STATUS_CODE, 
    REMARK_TEXT, 
    UPDATE_DATE, 
    UPDATED_BY
)
SELECT 
    BL_ORDER_UPDATE_LOG_S1.NEXTVAL,
    :ORDER_ID, 
    :ORDER_NO, 
    '1', 
    '新增Order > STATUS_CODE = 1', 
    SYSDATE, 
    :USER_NAME
FROM DUAL;

INSERT INTO BL_CUST_SALES
( CUST_ID, IB_CODE, ACTIVE, CREATION_DATE, CREATED_BY, LAST_UPDATE_DATE, LAST_UPDATED_BY)
SELECT 
    A.CUST_ID
    , B.IB_CODE
    , 'Y'
    , SYSDATE
    , :USER_NAME
    , SYSDATE
    , :USER_NAME
FROM BL_ORDER A
    INNER JOIN BL_EMP B
        ON A.EMP_CODE = B.EMP_CODE
WHERE NOT EXISTS
    (
        SELECT *
        FROM BL_CUST_SALES X
        WHERE A.CUST_ID = X.CUST_ID
    )
    AND A.ORDER_ID = :ORDER_ID
 ; 

UPDATE BL_CUST_SALES A
    SET
        IB_CODE = 
            (
                SELECT 
                    Y.IB_CODE
                from BL_ORDER X
                    INNER JOIN BL_EMP Y
                        ON X.EMP_CODE = Y.EMP_CODE
                WHERE X.ORDER_ID = :ORDER_ID)
        , LAST_UPDATE_DATE = SYSDATE
WHERE A.CUST_ID = (SELECT CUST_ID FROM BL_ORDER X WHERE X.ORDER_ID = :ORDER_ID)
 ;

END;
";
                base.ExecuteScalar(strSQL, new
                {
                    ORDER_ID = seqOrder,
                    ORDER_NO = data.ORDER_NO,
                    CUST_ID = CustID,
                    EMP_CODE = data.EMP_CODE,
                    MT4_ACCOUNT = data.MT4_ACCOUNT,
                    FROM_DATE = data.FROM_DATE,
                    END_DATE = data.END_DATE,
                    YEAR = data.YEAR,
                    NOTE = data.NOTE,
                    IB_CODE = data.IB_CODE,
                    ORDER_END = data.ORDER_END,
                    AMOUNT_USD = data.AMOUNT_USD,
                    AMOUNT_NTD = data.AMOUNT_NTD,
                    AMOUNT_RMB = data.AMOUNT_RMB,
                    AMOUNT_EUR = data.AMOUNT_EUR,
                    AMOUNT_AUD = data.AMOUNT_AUD,
                    AMOUNT_JPY = data.AMOUNT_JPY,
                    AMOUNT_NZD = data.AMOUNT_NZD,
                    EXTEND_DATE = data.EXTEND_DATE,
                    OLD_AMOUNT_USD = data.OLD_AMOUNT_USD,
                    OLD_AMOUNT_NTD = data.OLD_AMOUNT_NTD,
                    OLD_AMOUNT_RMB = data.OLD_AMOUNT_RMB,
                    OLD_AMOUNT_EUR = data.OLD_AMOUNT_EUR,
                    OLD_AMOUNT_AUD = data.OLD_AMOUNT_AUD,
                    OLD_AMOUNT_JPY = data.OLD_AMOUNT_JPY,
                    OLD_AMOUNT_NZD = data.OLD_AMOUNT_NZD,
                    PARENT_ORDER_NO = data.PARENT_ORDER_NO,
                    INTEREST_USD = data.INTEREST_USD,
                    INTEREST_NTD = data.INTEREST_NTD,
                    INTEREST_RMB = data.INTEREST_RMB,
                    INTEREST_EUR = data.INTEREST_EUR,
                    INTEREST_AUD = data.INTEREST_AUD,
                    INTEREST_JPY = data.INTEREST_JPY,
                    INTEREST_NZD = data.INTEREST_NZD,
                    ENTER_CURRENCY = data.ENTER_CURRENCY,
                    ENTER_AMOUNT = data.ENTER_AMOUNT,
                    AMOUNT = data.AMOUNT,
                    CURRENCY = data.CURRENCY,
                    ATTRIBUTE1 = data.ATTRIBUTE1,
                    INTEREST_RATE = data.INTEREST_RATE,
                    PROJECT = data.PROJECT,
                    WITHDRAWAL_MODE = data.WITHDRAWAL_MODE,
                    MULTI_ORDER_FLAG = data.MULTI_ORDER_FLAG,
                    IS_REMIT_TO_AUS = data.IS_REMIT_TO_AUS,
                    ORG_CODE = data.ORG_CODE,
                    DISCOUNT_AMOUNT = data.DISCOUNT_AMOUNT,
                    BONUS_OPTION = data.BONUS_OPTION,
                    INTERNAL_IB_CODE = data.INTERNAL_IB_CODE,
                    USER_NAME = strUserName
                }, CommandType.Text);
                return true;
            }
            catch (Exception ex)
            {
                exError = ex;
                return false;
            }
        }


        public List<BL_ORDER> GetUnExpiredOrderData(string strYear, string strMonth, string strCustName, out Exception exError)
        {
            exError = null;
            try
            {
                //var strBaseDate = strYear + "/" + strMonth + "/01 00:00:00";
                var strSQL = @"
                SELECT
                    A.*
                    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_ENAME) as CUST_ENAME
                    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_ENAME2) as CUST_ENAME2
                    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_CNAME) as CUST_CNAME
                    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_CNAME2) as  CUST_CNAME2
                    , D.CNAME AS SALES_NAME
                    ,BL_INTEREST2_PKG.RETURN_BALANCE(A.ORDER_ID) AS BALANCE
--, 0 AS BALANCE
                FROM BL_ORDER A
                    INNER JOIN BL_CUST B
                        ON A.CUST_ID = B.CUST_ID
                    LEFT JOIN BL_CUST_SALES C
                        ON A.CUST_ID = C.CUST_ID
                            AND (C.ACTIVE IS NULL OR C.ACTIVE = 'Y' )   --AND C.ACTIVE <> 'N'
                    LEFT JOIN BL_EMP D
                        ON C.IB_CODE = D.IB_CODE
                WHERE 1 = 1
                   AND A.ORDER_END IS NULL
                    AND A.STATUS_CODE = '4'
--                    AND ( 
--                        A.ORDER_END >= :BaseDate
--                        OR A.END_DATE >= :BaseDate
--                    )
                    AND (
                        BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_CNAME) LIKE '%'||:CNAME||'%'
                        OR BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_CNAME2) LIKE '%'||:CNAME||'%'
                        OR :CNAME IS NULL
                    )
                ";
                return base.GetList(strSQL, new
                {
                    //BaseDate = DateTime.Parse(strBaseDate),
                    CNAME = strCustName
                }, CommandType.Text).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }

        public List<BL_ORDER> GetUnExpiredOrderDatabyOrderID(string strOrderID, out Exception exError)
        {
            exError = null;
            try
            {
                //var strBaseDate = strYear + "/" + strMonth + "/01 00:00:00";
                var strSQL = @"
                SELECT
                    A.*
                    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_ENAME) as CUST_ENAME
                    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_ENAME2) as CUST_ENAME2
                    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_CNAME) as CUST_CNAME
                    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_CNAME2) as  CUST_CNAME2
                    , D.CNAME AS SALES_NAME
                    
                FROM BL_ORDER A
                    INNER JOIN BL_CUST B
                        ON A.CUST_ID = B.CUST_ID
                    LEFT JOIN BL_CUST_SALES C
                        ON A.CUST_ID = C.CUST_ID
                            AND (C.ACTIVE IS NULL OR C.ACTIVE = 'Y' )   --AND C.ACTIVE <> 'N'
                    LEFT JOIN BL_EMP D
                        ON C.IB_CODE = D.IB_CODE
                WHERE 1 = 1              
                    AND A.STATUS_CODE = '4'
                    AND   (
                        A.ORDER_NO = :OrderID
                       
                    )
                ";
                return base.GetList(strSQL, new
                {
                    OrderID = strOrderID
                }, CommandType.Text).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }


        public List<BL_ORDER> GetDatabyOrderID(string strOrderID, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
                SELECT
                    A.*
                    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_ENAME) as CUST_ENAME
                    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_ENAME2) as CUST_ENAME2
                    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_CNAME) as CUST_CNAME
                    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_CNAME2) as  CUST_CNAME2
                    --, DECODE(D.IB_CODE, NULL, E.CNAME, D.CNAME) AS SALES_NAME
                    --, DECODE(D.IB_CODE, NULL, E.IB_CODE, D.IB_CODE) AS SALES_IB_CODE
                    --, DECODE(D.IB_CODE, NULL, E.EMP_CODE, D.EMP_CODE) AS SALES_EMP_CODE

                    , DECODE(E.IB_CODE, NULL, D.CNAME, E.CNAME) AS SALES_NAME
                    , DECODE(E.IB_CODE, NULL, D.IB_CODE, E.IB_CODE) AS SALES_IB_CODE
                    , DECODE(E.IB_CODE, NULL, D.EMP_CODE, E.EMP_CODE) AS SALES_EMP_CODE

                    , F.CNAME AS SALES_NAME_INTERNAL
                    --, F.IB_CODE AS INTERNAL_IB_CODE
                    , F.EMP_CODE AS SALES_EMP_CODE_INTERNAL
                FROM BL_ORDER A
                    INNER JOIN BL_CUST B
                        ON A.CUST_ID = B.CUST_ID
                    LEFT JOIN BL_CUST_SALES C
                        ON A.CUST_ID = C.CUST_ID
                            AND (C.ACTIVE IS NULL OR C.ACTIVE = 'Y' )   --AND C.ACTIVE <> 'N'
                    LEFT JOIN BL_EMP D
                        ON C.IB_CODE = D.IB_CODE
                    LEFT JOIN BL_EMP E
                        ON A.IB_CODE = E.IB_CODE
                    LEFT JOIN BL_EMP F
                        ON A.INTERNAL_IB_CODE = F.INTERNAL_IB_CODE
                WHERE 1 = 1              
--                    AND A.STATUS_CODE = '4'
                    AND   (
                        A.ORDER_ID = :OrderID
                       
                    )
                ";
                return base.GetList(strSQL, new
                {
                    OrderID = strOrderID
                }, CommandType.Text).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }

        public List<BL_ORDER> GetDatabyOrderList( List<BL_ORDER> data, out Exception exError)
        {
            exError = null;
            try
            {
                var liOrderID = new List<string>();
                data.ForEach(x =>
                {
                    if (x.ORDER_ID != null)
                    {
                        liOrderID.Add(x.ORDER_ID.Value.ConvertToString());
                    }
                });
                var strSQL = @"
                SELECT
                    A.*
                FROM BL_ORDER A
                WHERE 1 = 1    
                    AND A.ORDER_ID IN :OrderID
                ";
                return base.GetList(strSQL, new
                {
                    OrderID = liOrderID
                }, CommandType.Text).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }


        public List<BL_ORDER> GetDataByOrderNo(string ORDER_NO, string CustName, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
SELECT 
    A.*
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_ENAME)  AS CUST_ENAME
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_CNAME) AS CUST_CNAME
    , D.CNAME AS SALES_NAME
    , BL_INTEREST2_PKG.RETURN_BALANCE(A.ORDER_ID) AS BALANCE
    --, 0 AS BALANCE
FROM BL_ORDER A
    INNER JOIN BL_CUST B
        ON A.CUST_ID = B.CUST_ID
    LEFT JOIN BL_CUST_SALES C
        ON A.CUST_ID = C.CUST_ID
            AND (C.ACTIVE IS NULL OR C.ACTIVE = 'Y' )   --AND C.ACTIVE <> 'N'
    LEFT JOIN BL_EMP D
        ON C.IB_CODE = D.IB_CODE
WHERE 1 = 1 AND A.ORDER_END IS NULL
    AND A.STATUS_CODE = '4'
    AND (UPPER(A.ORDER_NO) LIKE '%'||UPPER(:ORDER_NO)||'%' OR :ORDER_NO IS NULL)
    AND 
    (
        BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_CNAME) LIKE '%'||:CUST_NAME||'%' OR
        BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_CNAME2) LIKE '%'||:CUST_NAME||'%' OR
        :CUST_NAME IS NULL
    )
";
                return base.GetList(strSQL, new
                {
                    ORDER_NO = ORDER_NO,
                    CUST_NAME = CustName
                }, CommandType.Text).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }



        public List<BL_ORDER> GetDataByOrderNo
            (
                List<string> ORDER_NO,
                string CustName,
                DateTime? END_DATE_S,
                DateTime? END_DATE_E,
                string strCustID,
                DateTime? ORDER_END_S,
                DateTime? ORDER_END_E,
                string strORG,
                out Exception exError
            )
        {
            exError = null;
            try
            {
                var param = new DynamicParameters();

                var strSQL = @"
SELECT 
    A.*
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_ENAME)  AS CUST_ENAME
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_CNAME) AS CUST_CNAME
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_ENAME2)  AS CUST_ENAME2
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_CNAME2) AS CUST_CNAME2
    , D.CNAME AS SALES_NAME
    , BL_INTEREST2_PKG.RETURN_BALANCE(A.ORDER_ID) AS BALANCE
    --, 0 AS BALANCE
FROM BL_ORDER A
    INNER JOIN BL_CUST B
        ON A.CUST_ID = B.CUST_ID
    LEFT JOIN BL_CUST_SALES C
        ON A.CUST_ID = C.CUST_ID
            AND (C.ACTIVE IS NULL OR C.ACTIVE = 'Y' )   --AND C.ACTIVE <> 'N'
    LEFT JOIN BL_EMP D
        ON C.IB_CODE = D.IB_CODE
WHERE 1 = 1   AND  rownum <= 300
    --AND A.ORDER_END IS NULL
    AND A.STATUS_CODE = '4'
    AND 
    (
        BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_CNAME) LIKE '%'||:CUST_NAME||'%' OR
        BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_CNAME2) LIKE '%'||:CUST_NAME||'%' OR
        :CUST_NAME IS NULL
    )
    AND ( A.END_DATE >= :END_DATE_S OR :END_DATE_S IS NULL )
    AND ( A.END_DATE <= :END_DATE_E OR :END_DATE_E IS NULL )
    AND ( A.CUST_ID = :CUST_ID OR :CUST_ID IS NULL )
    AND ( A.ORDER_END >= :ORDER_END_S OR :ORDER_END_S IS NULL )
    AND ( A.ORDER_END <= :ORDER_END_E OR :ORDER_END_E IS NULL )
--ORDER BY A.LAST_UPDATE_DATE DESC
";
                var strOtherSQL = string.Empty;
                int i = 0;
                ORDER_NO.ForEach(x =>
                {
                    if (x.Trim() != string.Empty)
                    {
                        strOtherSQL += strOtherSQL == string.Empty ? "" : " OR ";
                        strOtherSQL += " UPPER(A.ORDER_NO) LIKE '%'||UPPER(:ORDER_NO" + i.ToString() + ")||'%' ";
                        param.Add("ORDER_NO" + i.ToString(), x);
                        i += 1;
                    }

                });

                if (strOtherSQL != string.Empty)
                {
                    strSQL += " AND (" + strOtherSQL + " ) ";
                }

                if (strORG.ConvertToString() != string.Empty)
                {
                    switch (strORG)
                    {
                        case "TW1":
                            strSQL += @"
    AND A.ORG_CODE IN ('TW1', 'IND')
";
                            break;
                        default:
                            strSQL += @"
    AND A.ORG_CODE = '" + strORG + @"'
";
                            break;
                    }
                }

                param.Add("CUST_NAME", CustName);
                param.Add("END_DATE_S", END_DATE_S);
                param.Add("END_DATE_E", END_DATE_E);
                param.Add("ORDER_END_S", ORDER_END_S);
                param.Add("ORDER_END_E", ORDER_END_E);
                param.Add("CUST_ID", strCustID);

                return base.GetList(strSQL, param, CommandType.Text).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }


        public List<BL_ORDER> GetDataByTransSales
            (
                List<BL_CUST> liCust,
                List<BL_EMP> liSales,
                out Exception exError
            )
        {
            exError = null;
            try
            {
                var param = new DynamicParameters();

                var strSQL = @"
SELECT 
    A.*
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_ENAME)  AS CUST_ENAME
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_CNAME) AS CUST_CNAME
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_ENAME2)  AS CUST_ENAME2
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_CNAME2) AS CUST_CNAME2
    , D.CNAME AS SALES_NAME
    , BL_INTEREST2_PKG.RETURN_BALANCE(A.ORDER_ID) AS BALANCE
FROM BL_ORDER A
    INNER JOIN BL_CUST B
        ON A.CUST_ID = B.CUST_ID
    LEFT JOIN BL_CUST_SALES C
        ON A.CUST_ID = C.CUST_ID
            AND (C.ACTIVE IS NULL OR C.ACTIVE = 'Y' )
    LEFT JOIN BL_EMP D
        ON C.IB_CODE = D.IB_CODE
WHERE 1 = 1   AND  rownum <= 300
    AND A.STATUS_CODE = '4'
";
                if (liCust.Count() > 0)
                {
                    strSQL += @"
    AND A.CUST_ID IN :CUST_ID
";
                    param.Add("CUST_ID", liCust.Select(x => x.CUST_ID).ToList());
                }

                if (liSales.Count()>0)
                {
                    strSQL += @"
    AND D.IB_CODE IN :IB_CODE
";
                    param.Add("IB_CODE", liSales.Select(x => x.IB_CODE).ToList());
                }
                
                return base.GetList(strSQL, param, CommandType.Text).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }


        public List<BL_ORDER> GetUnApproveDataByOrderNo(string ORDER_NO, string CustName, string strApproveStatus, string strOrg, out Exception exError)
        {
            exError = null;
            try
            {
                var strSchedule = string.Empty;
                switch (strApproveStatus)
                {
                    case "待查核":
                        strSchedule = "1";
                        break;
                    case "待審核":
                        strSchedule = "3";
                        break;
                    case "已退件":
                        strSchedule = "9";
                        break;
                    case "已作廢":
                        strSchedule = "-1";
                        break;
                    default:
                        break;
                }
                
                var strSQL = @"
SELECT 
    A.*
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_ENAME)  AS CUST_ENAME
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_CNAME) AS CUST_CNAME
    , a.attribute1 AS SALES_NAME
    , BL_INTEREST2_PKG.RETURN_BALANCE(a.ORDER_ID) AS BALANCE
    --, 0 AS BALANCE

    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(B.EMAIL_1) as EMAIL_1
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(B.EMAIL_2) as EMAIL_2
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(B.EMAIL2_1) as EMAIL2_1
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(B.EMAIL2_2) as EMAIL2_2
    , D.EMAIL as SALES_EMAIL
FROM BL_ORDER A
    INNER JOIN BL_CUST B
        ON A.CUST_ID = B.CUST_ID
    LEFT JOIN BL_CUST_SALES C
        ON A.CUST_ID = C.CUST_ID
            AND (C.ACTIVE IS NULL OR C.ACTIVE = 'Y' )   --AND C.ACTIVE <> 'N'
    LEFT JOIN BL_EMP D
        ON C.IB_CODE = D.IB_CODE
WHERE 1 = 1
    AND A.STATUS_CODE NOT IN ('4')
    AND (UPPER(A.ORDER_NO) LIKE '%'||UPPER(:ORDER_NO)||'%' OR :ORDER_NO IS NULL)
    AND 
    (
        BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_CNAME) LIKE '%'||:CUST_NAME||'%' OR
        BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_CNAME2) LIKE '%'||:CUST_NAME||'%' OR
        :CUST_NAME IS NULL
    )
";
                if (strSchedule != string.Empty)
                {
                    strSQL += @"
    AND A.STATUS_CODE = " + strSchedule;
                }
                if (strOrg != string.Empty)
                {
                    switch (strOrg)
                    {
                        case "TW1":
                            strSQL += @"
    AND A.ORG_CODE IN ('TW1', 'IND')
";
                            break;
                        default:
                            strSQL += @"
    AND A.ORG_CODE = '" + strOrg + @"'
";
                            break;
                    }
                }
                strSQL += @"
ORDER BY A.STATUS_CODE

";

                return base.GetList(strSQL, new
                {
                    ORDER_NO = ORDER_NO,
                    CUST_NAME = CustName
                }, CommandType.Text).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }



        public List<BL_ORDER> GetUnApproveDataByOrderNo_NewCustomer(string ORDER_NO, string CustName, string strApproveStatus, string strOrg, out Exception exError)
        {
            exError = null;
            try
            {
                var strSchedule = string.Empty;
                switch (strApproveStatus)
                {
                    case "待查核":
                        strSchedule = "1";
                        break;
                    case "待審核":
                        strSchedule = "3";
                        break;
                    case "已退件":
                        strSchedule = "9";
                        break;
                    case "已作廢":
                        strSchedule = "-1";
                        break;
                    default:
                        break;
                }

                var strSQL = @"
SELECT 
    A.*
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_ENAME)  AS CUST_ENAME
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_CNAME) AS CUST_CNAME
    , a.attribute1 AS SALES_NAME
    , BL_INTEREST2_PKG.RETURN_BALANCE(a.ORDER_ID) AS BALANCE
    --, 0 AS BALANCE

    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(B.EMAIL_1) as EMAIL_1
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(B.EMAIL_2) as EMAIL_2
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(B.EMAIL2_1) as EMAIL2_1
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(B.EMAIL2_2) as EMAIL2_2
    , D.EMAIL as SALES_EMAIL
FROM BL_ORDER A
    INNER JOIN BL_CUST B ON A.CUST_ID = B.CUST_ID
    LEFT JOIN BL_CUST_SALES C ON A.CUST_ID = C.CUST_ID AND (C.ACTIVE IS NULL OR C.ACTIVE = 'Y' )   --AND C.ACTIVE <> 'N'
    LEFT JOIN BL_EMP D ON C.IB_CODE = D.IB_CODE
WHERE 1 = 1
    AND A.STATUS_CODE NOT IN ('4')
    AND B.is_old_customer='N'
    AND (UPPER(A.ORDER_NO) LIKE '%'||UPPER(:ORDER_NO)||'%' OR :ORDER_NO IS NULL)
    AND 
    (
        BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_CNAME) LIKE '%'||:CUST_NAME||'%' OR
        BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_CNAME2) LIKE '%'||:CUST_NAME||'%' OR
        :CUST_NAME IS NULL
    )
";
                if (strSchedule != string.Empty)
                {
                    strSQL += @"AND A.STATUS_CODE = " + strSchedule;
                }

                if (strOrg != string.Empty)
                {
                    switch (strOrg)
                    {
                        case "TW1":
                            strSQL += @" AND A.ORG_CODE IN ('TW1') "; break;
                        default:
                            strSQL += @" AND A.ORG_CODE = '" + strOrg + @"'"; break;
                    }
                }
                strSQL += @" ORDER BY A.STATUS_CODE ";

                return base.GetList(strSQL, new
                {
                    ORDER_NO = ORDER_NO,
                    CUST_NAME = CustName
                }, CommandType.Text).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }

        public bool InsertCustBankData(string strCustID, BL_CUST_BANK data)
        {
            try
            {

                var strSQL = @"
INSERT INTO
BL_CUST_BANK
(
     CUST_ID,BANK_CNAME,BANK_CNAME2,BANK_ENAME,BANK_ENAME2,BRANCH_CNAME,BRANCH_CNAME2,BRANCH_ENAME,BRANCH_ENAME2,ACCOUNT_CNAME,ACCOUNT_CNAME2,ACCOUNT,ACCOUNT2,BANK_C_ADDRESS,BANK_C_ADDRESS2,
BANK_E_ADDRESS,BANK_E_ADDRESS2,SWIFT_CODE,SWIFT_CODE2,CREATION_DATE,CREATED_BY ,LAST_UPDATE_DATE,LAST_UPDATED_BY,BANK_ID
)
Values  ('" + strCustID + "','" + data.BANK_CNAME + "','" + data.BANK_CNAME2 + "','" + data.BANK_ENAME + "','" + data.BANK_ENAME2 + "','" + data.BRANCH_CNAME + "','" + data.BRANCH_CNAME2 + "','" + data.BRANCH_ENAME + "','" + data.BRANCH_ENAME2 + "','" + data.ACCOUNT_CNAME + "','" + data.ACCOUNT_CNAME2 + "','" + data.ACCOUNT + "','" + data.ACCOUNT2 + "','" + data.BANK_C_ADDRESS + "','" + data.BANK_C_ADDRESS2 + "','" + data.BANK_E_ADDRESS + "','" + data.BANK_E_ADDRESS2 + "','" + data.SWIFT_CODE + "','" + data.SWIFT_CODE2 + "',sysdate(),'" + data.CREATED_BY + "' ,sysdate(),'" + data.LAST_UPDATED_BY + "',BL_CUST_BANK_S1.nextval)";
                base.ExecuteScalar(strSQL, null, CommandType.Text);
                return true;
            }
            catch (Exception ex)
            {
             
                return false;
            }
        }


        public bool UpdateOrderData
            (
                string strOrderID,
                string strCustID,
                BL_CUST dataCust,
                BL_CUST_BANK dataBank,
                BL_CUST_BANK dataBank2,
                BL_CUST_BANK dataBank3,
                BL_CUST_BANK dataBank3_2,
                BL_CUST_BANK dataBank4,
                BL_CUST_BANK dataBank5,
                BL_CUST_BANK dataBank6,
                BL_CUST_BANK dataBank7,
                BL_ORDER dataOrder,
                string strUserName,
                bool IsResend,
                bool IsRejectByHKtoResend,
                out Exception exError
            )
        {
            exError = null;
            try
            {

                var strSQL = string.Empty;

                if (IsResend)
                {
                    #region 修改重送
                    strSQL = @"
BEGIN

    
    UPDATE BL_ORDER
        SET
            EMP_CODE = :EMP_CODE
            , IB_CODE = :IB_CODE
            , INTERNAL_IB_CODE = :INTERNAL_IB_CODE
            , EXTEND_DATE = :EXTEND_DATE
            , ORDER_END = :ORDER_END
            , LAST_UPDATE_DATE = SYSDATE
            , LAST_UPDATED_BY = :USER_NAME
            , STATUS_CODE = '" + (IsRejectByHKtoResend ? "3" : "1") + @"'


            , MT4_ACCOUNT = :MT4_ACCOUNT
            , FROM_DATE = :FROM_DATE
            , END_DATE = :END_DATE
            , YEAR = :YEAR
            , NOTE = :NOTE
            , AMOUNT_USD = :AMOUNT_USD
            , AMOUNT_NTD = :AMOUNT_NTD
            , AMOUNT_RMB = :AMOUNT_RMB
            , AMOUNT_EUR = :AMOUNT_EUR
            , AMOUNT_AUD = :AMOUNT_AUD
            , AMOUNT_JPY = :AMOUNT_JPY
            , AMOUNT_NZD = :AMOUNT_NZD
            , OLD_AMOUNT_USD = :OLD_AMOUNT_USD
            , OLD_AMOUNT_NTD = :OLD_AMOUNT_NTD
            , OLD_AMOUNT_RMB = :OLD_AMOUNT_RMB
            , OLD_AMOUNT_EUR = :OLD_AMOUNT_EUR
            , OLD_AMOUNT_AUD = :OLD_AMOUNT_AUD
            , OLD_AMOUNT_JPY = :OLD_AMOUNT_JPY
            , OLD_AMOUNT_NZD = :OLD_AMOUNT_NZD
            , PARENT_ORDER_NO = :PARENT_ORDER_NO
            , INTEREST_USD = :INTEREST_USD
            , INTEREST_NTD = :INTEREST_NTD
            , INTEREST_RMB = :INTEREST_RMB
            , INTEREST_EUR = :INTEREST_EUR
            , INTEREST_AUD = :INTEREST_AUD
            , INTEREST_JPY = :INTEREST_JPY
            , INTEREST_NZD = :INTEREST_NZD
            , ENTER_CURRENCY = :ENTER_CURRENCY
            , ENTER_AMOUNT = :ENTER_AMOUNT
            , AMOUNT = :AMOUNT
            , CURRENCY = :CURRENCY
            , ATTRIBUTE1 = :ATTRIBUTE1
            , INTEREST_RATE = :INTEREST_RATE
            , PROJECT = :PROJECT
            , ORDER_NO = :ORDER_NO
            , WITHDRAWAL_MODE = :WITHDRAWAL_MODE
            , MULTI_ORDER_FLAG = :MULTI_ORDER_FLAG
            , IS_REMIT_TO_AUS = :IS_REMIT_TO_AUS
            , ORG_CODE = :ORG_CODE
            , DISCOUNT_AMOUNT = :DISCOUNT_AMOUNT
            , BONUS_OPTION = :BONUS_OPTION
    WHERE ORDER_ID = :ORDER_ID;


    INSERT INTO BL_ORDER_UPDATE_LOG
    (
        BL_ORDER_UPDATE_LOG_ID, 
        ORDER_ID, 
        ORDER_NO, 
        STATUS_CODE, 
        REMARK_TEXT, 
        UPDATE_DATE, 
        UPDATED_BY
    )
    SELECT 
        BL_ORDER_UPDATE_LOG_S1.NEXTVAL,
        :ORDER_ID, 
        :ORDER_NO, 
        '" + (IsRejectByHKtoResend ? "3" : "1") + @"', 
        '" + (IsRejectByHKtoResend ? "修改重送(By香港退件) > STATUS_CODE = 3" : "修改重送(By行政退件) > STATUS_CODE = 1") + @"', 
        SYSDATE, 
        :USER_NAME
    FROM DUAL;


    UPDATE BL_CUST_SALES
    SET 
          IB_CODE = :IB_CODE
        , LAST_UPDATE_DATE = SYSDATE
        , LAST_UPDATED_BY = :USER_NAME
    WHERE CUST_ID = :CUST_ID;

END;
";
                    base.ExecuteScalar(strSQL, new
                    {
                        CUST_ID = strCustID,
                        ORDER_ID = strOrderID,
                        EMP_CODE = dataOrder.EMP_CODE,
                        IB_CODE = dataOrder.IB_CODE,
                        INTERNAL_IB_CODE=dataOrder.INTERNAL_IB_CODE,
                        EXTEND_DATE = dataOrder.EXTEND_DATE,
                        ORDER_END = dataOrder.ORDER_END,

                        USER_NAME = strUserName,
                        MT4_ACCOUNT = dataOrder.MT4_ACCOUNT,
                        FROM_DATE = dataOrder.FROM_DATE,
                        END_DATE = dataOrder.END_DATE,
                        YEAR = dataOrder.YEAR,
                        NOTE = dataOrder.NOTE,
                        AMOUNT_USD = dataOrder.AMOUNT_USD,
                        AMOUNT_NTD = dataOrder.AMOUNT_NTD,
                        AMOUNT_RMB = dataOrder.AMOUNT_RMB,
                        AMOUNT_EUR = dataOrder.AMOUNT_EUR,
                        AMOUNT_AUD = dataOrder.AMOUNT_AUD,
                        AMOUNT_JPY = dataOrder.AMOUNT_JPY,
                        AMOUNT_NZD = dataOrder.AMOUNT_NZD,
                        OLD_AMOUNT_USD = dataOrder.OLD_AMOUNT_USD,
                        OLD_AMOUNT_NTD = dataOrder.OLD_AMOUNT_NTD,
                        OLD_AMOUNT_RMB = dataOrder.OLD_AMOUNT_RMB,
                        OLD_AMOUNT_EUR = dataOrder.OLD_AMOUNT_EUR,
                        OLD_AMOUNT_AUD = dataOrder.OLD_AMOUNT_AUD,
                        OLD_AMOUNT_JPY = dataOrder.OLD_AMOUNT_JPY,
                        OLD_AMOUNT_NZD = dataOrder.OLD_AMOUNT_NZD,
                        PARENT_ORDER_NO = dataOrder.PARENT_ORDER_NO,
                        INTEREST_USD = dataOrder.INTEREST_USD,
                        INTEREST_NTD = dataOrder.INTEREST_NTD,
                        INTEREST_RMB = dataOrder.INTEREST_RMB,
                        INTEREST_EUR = dataOrder.INTEREST_EUR,
                        INTEREST_AUD = dataOrder.INTEREST_AUD,
                        INTEREST_JPY = dataOrder.INTEREST_JPY,
                        INTEREST_NZD = dataOrder.INTEREST_NZD,
                        ENTER_CURRENCY = dataOrder.ENTER_CURRENCY,
                        ENTER_AMOUNT = dataOrder.ENTER_AMOUNT,
                        AMOUNT = dataOrder.AMOUNT,
                        CURRENCY = dataOrder.CURRENCY,
                        ATTRIBUTE1 = dataOrder.ATTRIBUTE1,
                        INTEREST_RATE = dataOrder.INTEREST_RATE,
                        PROJECT = dataOrder.PROJECT,
                        WITHDRAWAL_MODE = dataOrder.WITHDRAWAL_MODE,
                        MULTI_ORDER_FLAG = dataOrder.MULTI_ORDER_FLAG,
                        IS_REMIT_TO_AUS = dataOrder.IS_REMIT_TO_AUS,
                        ORG_CODE = dataOrder.ORG_CODE,
                        DISCOUNT_AMOUNT = dataOrder.DISCOUNT_AMOUNT,
                        BONUS_OPTION = dataOrder.BONUS_OPTION,
                        ORDER_NO = dataOrder.ORDER_NO
                    }, CommandType.Text);

                    #endregion
                }
                else
                {
                    #region 一般資料修改

                    strSQL = @"
BEGIN

    UPDATE BL_CUST
        SET
            PHONE_1 = BL_CRYPT_PKG.ENCRYPT_DES_NOKEY(:PHONE_1)
            , PHONE_2 = BL_CRYPT_PKG.ENCRYPT_DES_NOKEY(:PHONE_2)
            , EMAIL_1 = BL_CRYPT_PKG.ENCRYPT_DES_NOKEY(:EMAIL_1)
            , EMAIL_2 = BL_CRYPT_PKG.ENCRYPT_DES_NOKEY(:EMAIL_2)
            , C_ADDRESS = BL_CRYPT_PKG.ENCRYPT_DES_NOKEY(:C_ADDRESS)
            , C_ADDRESS2 = BL_CRYPT_PKG.ENCRYPT_DES_NOKEY(:C_ADDRESS2)
            , MAIL_TYPE_MONTHLY = :MAIL_TYPE_MONTHLY
            , MAIL_TYPE_END = :MAIL_TYPE_END
            , POSTAL_CODE = :POSTAL_CODE
            , POSTAL_CODE2 = :POSTAL_CODE2

            , LAST_UPDATE_DATE = SYSDATE
            , LAST_UPDATED_BY = :USER_NAME


            , CUST_CNAME = BL_CRYPT_PKG.ENCRYPT_DES_NOKEY(:CUST_CNAME)
            , CUST_ENAME = BL_CRYPT_PKG.ENCRYPT_DES_NOKEY(:CUST_ENAME)
            , CUST_CNAME2 = BL_CRYPT_PKG.ENCRYPT_DES_NOKEY(:CUST_CNAME2)
            , CUST_ENAME2 = BL_CRYPT_PKG.ENCRYPT_DES_NOKEY(:CUST_ENAME2)
            , ID_NUMBER = BL_CRYPT_PKG.ENCRYPT_DES_NOKEY(:ID_NUMBER)
            , ID_NUMBER2 = BL_CRYPT_PKG.ENCRYPT_DES_NOKEY(:ID_NUMBER2)
            , PASSPORT = :PASSPORT
            , PASSPORT2 = :PASSPORT2

            , SEX = :SEX
            , SEX2 = :SEX2
            , DATE_OF_BIRTH = :DATE_OF_BIRTH
            , DATE_OF_BIRTH2 = :DATE_OF_BIRTH2
            , PASSPORT_REGION = :PASSPORT_REGION
            , PASSPORT_REGION2 = :PASSPORT_REGION2
            , IS_ONLINE_WITHDRAWAL = :IS_ONLINE_WITHDRAWAL

   WHERE CUST_ID = :CUST_ID;

    
    UPDATE BL_CUST_BANK
        SET
         BANK_CNAME = :BANK_CNAME
      
        , BANK_ENAME = :BANK_ENAME
        , BRANCH_CNAME = :BRANCH_CNAME
        , BRANCH_ENAME = :BRANCH_ENAME
        , ACCOUNT_CNAME = :ACCOUNT_CNAME
        , ACCOUNT_ENAME = :ACCOUNT_ENAME
        , ACCOUNT = :ACCOUNT
        , BANK_C_ADDRESS = :BANK_C_ADDRESS
        , BANK_E_ADDRESS = :BANK_E_ADDRESS
        , SWIFT_CODE = :SWIFT_CODE
        , LAST_UPDATE_DATE = SYSDATE

        , BANK_CNAME2 = :BANK_CNAME2
        , BANK_ENAME2 = :BANK_ENAME2
        , BRANCH_CNAME2 = :BRANCH_CNAME2
        , BRANCH_ENAME2 = :BRANCH_ENAME2
        , ACCOUNT_CNAME2 = :ACCOUNT_CNAME2
        , ACCOUNT_ENAME2 = :ACCOUNT_ENAME2
        , ACCOUNT2 = :ACCOUNT2
        , BANK_C_ADDRESS2 = :BANK_C_ADDRESS2
        , BANK_E_ADDRESS2 = :BANK_E_ADDRESS2
        , SWIFT_CODE2 = :SWIFT_CODE2
        , LAST_UPDATED_BY = :USER_NAME
        , CURRENCY = :CURRENCY
    WHERE CUST_ID = :CUST_ID
        AND BANK_ID = :BANK_ID;

    
    UPDATE BL_CUST_BANK
        SET
         BANK_CNAME = :BANK_CNAME_2
      
        , BANK_ENAME = :BANK_ENAME_2
        , BRANCH_CNAME = :BRANCH_CNAME_2
        , BRANCH_ENAME = :BRANCH_ENAME_2
        , ACCOUNT_CNAME = :ACCOUNT_CNAME_2
        , ACCOUNT_ENAME = :ACCOUNT_ENAME_2
        , ACCOUNT = :ACCOUNT_2
        , BANK_C_ADDRESS = :BANK_C_ADDRESS_2
        , BANK_E_ADDRESS = :BANK_E_ADDRESS_2
        , SWIFT_CODE = :SWIFT_CODE_2
        , LAST_UPDATE_DATE = SYSDATE

        , BANK_CNAME2 = :BANK_CNAME2_2
        , BANK_ENAME2 = :BANK_ENAME2_2
        , BRANCH_CNAME2 = :BRANCH_CNAME2_2
        , BRANCH_ENAME2 = :BRANCH_ENAME2_2
        , ACCOUNT_CNAME2 = :ACCOUNT_CNAME2_2
        , ACCOUNT_ENAME2 = :ACCOUNT_ENAME2_2
        , ACCOUNT2 = :ACCOUNT2_2
        , BANK_C_ADDRESS2 = :BANK_C_ADDRESS2_2
        , BANK_E_ADDRESS2 = :BANK_E_ADDRESS2_2
        , SWIFT_CODE2 = :SWIFT_CODE2_2
        , LAST_UPDATED_BY = :USER_NAME
        , CURRENCY = :CURRENCY_2
    WHERE CUST_ID = :CUST_ID
        AND BANK_ID = :BANK_ID_2;


    UPDATE BL_CUST_BANK
        SET
         BANK_CNAME = :BANK_CNAME_3
      
        , BANK_ENAME = :BANK_ENAME_3
        , BRANCH_CNAME = :BRANCH_CNAME_3
        , BRANCH_ENAME = :BRANCH_ENAME_3
        , ACCOUNT_CNAME = :ACCOUNT_CNAME_3
        , ACCOUNT_ENAME = :ACCOUNT_ENAME_3
        , ACCOUNT = :ACCOUNT_3
        , BANK_C_ADDRESS = :BANK_C_ADDRESS_3
        , BANK_E_ADDRESS = :BANK_E_ADDRESS_3
        , SWIFT_CODE = :SWIFT_CODE_3
        , LAST_UPDATE_DATE = SYSDATE

        , BANK_CNAME2 = :BANK_CNAME2_3
        , BANK_ENAME2 = :BANK_ENAME2_3
        , BRANCH_CNAME2 = :BRANCH_CNAME2_3
        , BRANCH_ENAME2 = :BRANCH_ENAME2_3
        , ACCOUNT_CNAME2 = :ACCOUNT_CNAME2_3
        , ACCOUNT_ENAME2 = :ACCOUNT_ENAME2_3
        , ACCOUNT2 = :ACCOUNT2_3
        , BANK_C_ADDRESS2 = :BANK_C_ADDRESS2_3
        , BANK_E_ADDRESS2 = :BANK_E_ADDRESS2_3
        , SWIFT_CODE2 = :SWIFT_CODE2_3
        , LAST_UPDATED_BY = :USER_NAME
        , CURRENCY = :CURRENCY_3
        , ACCOUNT_TYPE = :ACCOUNT_TYPE_3
    WHERE CUST_ID = :CUST_ID
        AND BANK_ID = :BANK_ID_3;

    UPDATE BL_CUST_BANK
        SET
         BANK_CNAME = :BANK_CNAME_3_2
      
        , BANK_ENAME = :BANK_ENAME_3_2
        , BRANCH_CNAME = :BRANCH_CNAME_3_2
        , BRANCH_ENAME = :BRANCH_ENAME_3_2
        , ACCOUNT_CNAME = :ACCOUNT_CNAME_3_2
        , ACCOUNT_ENAME = :ACCOUNT_ENAME_3_2
        , ACCOUNT = :ACCOUNT_3_2
        , BANK_C_ADDRESS = :BANK_C_ADDRESS_3_2
        , BANK_E_ADDRESS = :BANK_E_ADDRESS_3_2
        , SWIFT_CODE = :SWIFT_CODE_3_2
        , LAST_UPDATE_DATE = SYSDATE

        , BANK_CNAME2 = :BANK_CNAME2_3_2
        , BANK_ENAME2 = :BANK_ENAME2_3_2
        , BRANCH_CNAME2 = :BRANCH_CNAME2_3_2
        , BRANCH_ENAME2 = :BRANCH_ENAME2_3_2
        , ACCOUNT_CNAME2 = :ACCOUNT_CNAME2_3_2
        , ACCOUNT_ENAME2 = :ACCOUNT_ENAME2_3_2
        , ACCOUNT2 = :ACCOUNT2_3_2
        , BANK_C_ADDRESS2 = :BANK_C_ADDRESS2_3_2
        , BANK_E_ADDRESS2 = :BANK_E_ADDRESS2_3_2
        , SWIFT_CODE2 = :SWIFT_CODE2_3_2
        , LAST_UPDATED_BY = :USER_NAME
        , CURRENCY = :CURRENCY_3_2
        , ACCOUNT_TYPE = :ACCOUNT_TYPE_3_2
    WHERE CUST_ID = :CUST_ID
        AND BANK_ID = :BANK_ID_3_2;


    UPDATE BL_CUST_BANK
        SET
         BANK_CNAME = :BANK_CNAME_4
      
        , BANK_ENAME = :BANK_ENAME_4
        , BRANCH_CNAME = :BRANCH_CNAME_4
        , BRANCH_ENAME = :BRANCH_ENAME_4
        , ACCOUNT_CNAME = :ACCOUNT_CNAME_4
        , ACCOUNT_ENAME = :ACCOUNT_ENAME_4
        , ACCOUNT = :ACCOUNT_4
        , BANK_C_ADDRESS = :BANK_C_ADDRESS_4
        , BANK_E_ADDRESS = :BANK_E_ADDRESS_4
        , SWIFT_CODE = :SWIFT_CODE_4
        , LAST_UPDATE_DATE = SYSDATE

        , BANK_CNAME2 = :BANK_CNAME2_4
        , BANK_ENAME2 = :BANK_ENAME2_4
        , BRANCH_CNAME2 = :BRANCH_CNAME2_4
        , BRANCH_ENAME2 = :BRANCH_ENAME2_4
        , ACCOUNT_CNAME2 = :ACCOUNT_CNAME2_4
        , ACCOUNT_ENAME2 = :ACCOUNT_ENAME2_4
        , ACCOUNT2 = :ACCOUNT2_4
        , BANK_C_ADDRESS2 = :BANK_C_ADDRESS2_4
        , BANK_E_ADDRESS2 = :BANK_E_ADDRESS2_4
        , SWIFT_CODE2 = :SWIFT_CODE2_4
        , LAST_UPDATED_BY = :USER_NAME
        , CURRENCY = :CURRENCY_4
    WHERE CUST_ID = :CUST_ID
        AND BANK_ID = :BANK_ID_4;


    UPDATE BL_CUST_BANK
        SET
         BANK_CNAME = :BANK_CNAME_5
      
        , BANK_ENAME = :BANK_ENAME_5
        , BRANCH_CNAME = :BRANCH_CNAME_5
        , BRANCH_ENAME = :BRANCH_ENAME_5
        , ACCOUNT_CNAME = :ACCOUNT_CNAME_5
        , ACCOUNT_ENAME = :ACCOUNT_ENAME_5
        , ACCOUNT = :ACCOUNT_5
        , BANK_C_ADDRESS = :BANK_C_ADDRESS_5
        , BANK_E_ADDRESS = :BANK_E_ADDRESS_5
        , SWIFT_CODE = :SWIFT_CODE_5
        , LAST_UPDATE_DATE = SYSDATE

        , BANK_CNAME2 = :BANK_CNAME2_5
        , BANK_ENAME2 = :BANK_ENAME2_5
        , BRANCH_CNAME2 = :BRANCH_CNAME2_5
        , BRANCH_ENAME2 = :BRANCH_ENAME2_5
        , ACCOUNT_CNAME2 = :ACCOUNT_CNAME2_5
        , ACCOUNT_ENAME2 = :ACCOUNT_ENAME2_5
        , ACCOUNT2 = :ACCOUNT2_5
        , BANK_C_ADDRESS2 = :BANK_C_ADDRESS2_5
        , BANK_E_ADDRESS2 = :BANK_E_ADDRESS2_5
        , SWIFT_CODE2 = :SWIFT_CODE2_5
        , LAST_UPDATED_BY = :USER_NAME
        , CURRENCY = :CURRENCY_5
    WHERE CUST_ID = :CUST_ID
        AND BANK_ID = :BANK_ID_5;


    UPDATE BL_CUST_BANK
        SET
         BANK_CNAME = :BANK_CNAME_6
      
        , BANK_ENAME = :BANK_ENAME_6
        , BRANCH_CNAME = :BRANCH_CNAME_6
        , BRANCH_ENAME = :BRANCH_ENAME_6
        , ACCOUNT_CNAME = :ACCOUNT_CNAME_6
        , ACCOUNT_ENAME = :ACCOUNT_ENAME_6
        , ACCOUNT = :ACCOUNT_6
        , BANK_C_ADDRESS = :BANK_C_ADDRESS_6
        , BANK_E_ADDRESS = :BANK_E_ADDRESS_6
        , SWIFT_CODE = :SWIFT_CODE_6
        , LAST_UPDATE_DATE = SYSDATE

        , BANK_CNAME2 = :BANK_CNAME2_6
        , BANK_ENAME2 = :BANK_ENAME2_6
        , BRANCH_CNAME2 = :BRANCH_CNAME2_6
        , BRANCH_ENAME2 = :BRANCH_ENAME2_6
        , ACCOUNT_CNAME2 = :ACCOUNT_CNAME2_6
        , ACCOUNT_ENAME2 = :ACCOUNT_ENAME2_6
        , ACCOUNT2 = :ACCOUNT2_6
        , BANK_C_ADDRESS2 = :BANK_C_ADDRESS2_6
        , BANK_E_ADDRESS2 = :BANK_E_ADDRESS2_6
        , SWIFT_CODE2 = :SWIFT_CODE2_6
        , LAST_UPDATED_BY = :USER_NAME
        , CURRENCY = :CURRENCY_6
    WHERE CUST_ID = :CUST_ID
        AND BANK_ID = :BANK_ID_6;


    UPDATE BL_CUST_BANK
        SET
         BANK_CNAME = :BANK_CNAME_7
      
        , BANK_ENAME = :BANK_ENAME_7
        , BRANCH_CNAME = :BRANCH_CNAME_7
        , BRANCH_ENAME = :BRANCH_ENAME_7
        , ACCOUNT_CNAME = :ACCOUNT_CNAME_7
        , ACCOUNT_ENAME = :ACCOUNT_ENAME_7
        , ACCOUNT = :ACCOUNT_7
        , BANK_C_ADDRESS = :BANK_C_ADDRESS_7
        , BANK_E_ADDRESS = :BANK_E_ADDRESS_7
        , SWIFT_CODE = :SWIFT_CODE_7
        , LAST_UPDATE_DATE = SYSDATE

        , BANK_CNAME2 = :BANK_CNAME2_7
        , BANK_ENAME2 = :BANK_ENAME2_7
        , BRANCH_CNAME2 = :BRANCH_CNAME2_7
        , BRANCH_ENAME2 = :BRANCH_ENAME2_7
        , ACCOUNT_CNAME2 = :ACCOUNT_CNAME2_7
        , ACCOUNT_ENAME2 = :ACCOUNT_ENAME2_7
        , ACCOUNT2 = :ACCOUNT2_7
        , BANK_C_ADDRESS2 = :BANK_C_ADDRESS2_7
        , BANK_E_ADDRESS2 = :BANK_E_ADDRESS2_7
        , SWIFT_CODE2 = :SWIFT_CODE2_7
        , LAST_UPDATED_BY = :USER_NAME
        , CURRENCY = :CURRENCY_7
    WHERE CUST_ID = :CUST_ID
        AND BANK_ID = :BANK_ID_7;

    
    UPDATE BL_ORDER
        SET
            EMP_CODE = :EMP_CODE
            , IB_CODE = :IB_CODE
            , INTERNAL_IB_CODE = :INTERNAL_IB_CODE
            , ATTRIBUTE1 = :ATTRIBUTE1
            , EXTEND_DATE = :EXTEND_DATE
            , ORDER_END = :ORDER_END
            , LAST_UPDATE_DATE = SYSDATE
            , LAST_UPDATED_BY = :USER_NAME
            , ORDER_NO = :ORDER_NO
            , MULTI_ORDER_FLAG = :MULTI_ORDER_FLAG
            , IS_REMIT_TO_AUS = :IS_REMIT_TO_AUS
            , ORG_CODE = :ORG_CODE
            , BONUS_OPTION = :BONUS_OPTION
    WHERE ORDER_ID = :ORDER_ID;

    UPDATE BL_CUST_SALES
    SET 
          IB_CODE = :IB_CODE
        , LAST_UPDATE_DATE = SYSDATE
        , LAST_UPDATED_BY = :USER_NAME
    WHERE CUST_ID = :CUST_ID;

END;
";

                    base.ExecuteScalar(strSQL, new
                    {
                        CUST_ID = strCustID,
                        PHONE_1 = dataCust.PHONE_1,
                        PHONE_2 = dataCust.PHONE_2,
                        EMAIL_1 = dataCust.EMAIL_1,
                        EMAIL_2 = dataCust.EMAIL_2,
                        C_ADDRESS = dataCust.C_ADDRESS,
                        C_ADDRESS2 = dataCust.C_ADDRESS2,
                        MAIL_TYPE_MONTHLY = dataCust.MAIL_TYPE_MONTHLY,
                        MAIL_TYPE_END = dataCust.MAIL_TYPE_END,
                        POSTAL_CODE = dataCust.POSTAL_CODE,
                        POSTAL_CODE2 = dataCust.POSTAL_CODE2,

                        CUST_CNAME = dataCust.CUST_CNAME,
                        CUST_ENAME = dataCust.CUST_ENAME,
                        CUST_CNAME2 = dataCust.CUST_CNAME2,
                        CUST_ENAME2 = dataCust.CUST_ENAME2,
                        ID_NUMBER = dataCust.ID_NUMBER,
                        ID_NUMBER2 = dataCust.ID_NUMBER2,
                        PASSPORT = dataCust.PASSPORT,
                        PASSPORT2 = dataCust.PASSPORT2,

                        SEX = dataCust.SEX,
                        SEX2 = dataCust.SEX2,
                        DATE_OF_BIRTH = dataCust.DATE_OF_BIRTH,
                        DATE_OF_BIRTH2 = dataCust.DATE_OF_BIRTH2,
                        PASSPORT_REGION = dataCust.PASSPORT_REGION,
                        PASSPORT_REGION2 = dataCust.PASSPORT_REGION2,
                        IS_ONLINE_WITHDRAWAL = dataCust.IS_ONLINE_WITHDRAWAL,


                        BANK_CNAME = dataBank.BANK_CNAME,
                        BANK_ENAME = dataBank.BANK_ENAME,
                        BRANCH_CNAME = dataBank.BRANCH_CNAME,
                        BRANCH_ENAME = dataBank.BRANCH_ENAME,
                        ACCOUNT_CNAME = dataBank.ACCOUNT_CNAME,
                        ACCOUNT_ENAME = dataBank.ACCOUNT_ENAME,
                        ACCOUNT = dataBank.ACCOUNT,
                        BANK_C_ADDRESS = dataBank.BANK_C_ADDRESS,
                        BANK_E_ADDRESS = dataBank.BANK_E_ADDRESS,
                        SWIFT_CODE = dataBank.SWIFT_CODE,

                        BANK_CNAME2 = dataBank.BANK_CNAME2,
                        BANK_ENAME2 = dataBank.BANK_ENAME2,
                        BRANCH_CNAME2 = dataBank.BRANCH_CNAME2,
                        BRANCH_ENAME2 = dataBank.BRANCH_ENAME2,
                        ACCOUNT_CNAME2 = dataBank.ACCOUNT_CNAME2,
                        ACCOUNT_ENAME2 = dataBank.ACCOUNT_ENAME2,
                        ACCOUNT2 = dataBank.ACCOUNT2,
                        BANK_C_ADDRESS2 = dataBank.BANK_C_ADDRESS2,
                        BANK_E_ADDRESS2 = dataBank.BANK_E_ADDRESS2,
                        SWIFT_CODE2 = dataBank.SWIFT_CODE2,
                        BANK_ID = dataBank.BANK_ID,
                        CURRENCY = dataBank.CURRENCY,

                        BANK_CNAME_2 = dataBank2.BANK_CNAME,
                        BANK_ENAME_2 = dataBank2.BANK_ENAME,
                        BRANCH_CNAME_2 = dataBank2.BRANCH_CNAME,
                        BRANCH_ENAME_2 = dataBank2.BRANCH_ENAME,
                        ACCOUNT_CNAME_2 = dataBank2.ACCOUNT_CNAME,
                        ACCOUNT_ENAME_2 = dataBank2.ACCOUNT_ENAME,
                        ACCOUNT_2 = dataBank2.ACCOUNT,
                        BANK_C_ADDRESS_2 = dataBank2.BANK_C_ADDRESS,
                        BANK_E_ADDRESS_2 = dataBank2.BANK_E_ADDRESS,
                        SWIFT_CODE_2 = dataBank2.SWIFT_CODE,

                        BANK_CNAME2_2 = dataBank2.BANK_CNAME2,
                        BANK_ENAME2_2 = dataBank2.BANK_ENAME2,
                        BRANCH_CNAME2_2 = dataBank2.BRANCH_CNAME2,
                        BRANCH_ENAME2_2 = dataBank2.BRANCH_ENAME2,
                        ACCOUNT_CNAME2_2 = dataBank2.ACCOUNT_CNAME2,
                        ACCOUNT_ENAME2_2 = dataBank2.ACCOUNT_ENAME2,
                        ACCOUNT2_2 = dataBank2.ACCOUNT2,
                        BANK_C_ADDRESS2_2 = dataBank2.BANK_C_ADDRESS2,
                        BANK_E_ADDRESS2_2 = dataBank2.BANK_E_ADDRESS2,
                        SWIFT_CODE2_2 = dataBank2.SWIFT_CODE2,
                        BANK_ID_2 = dataBank2.BANK_ID,
                        CURRENCY_2 = dataBank2.CURRENCY,


                        BANK_CNAME_3 = dataBank3.BANK_CNAME,
                        BANK_ENAME_3 = dataBank3.BANK_ENAME,
                        BRANCH_CNAME_3 = dataBank3.BRANCH_CNAME,
                        BRANCH_ENAME_3 = dataBank3.BRANCH_ENAME,
                        ACCOUNT_CNAME_3 = dataBank3.ACCOUNT_CNAME,
                        ACCOUNT_ENAME_3 = dataBank3.ACCOUNT_ENAME,
                        ACCOUNT_3 = dataBank3.ACCOUNT,
                        BANK_C_ADDRESS_3 = dataBank3.BANK_C_ADDRESS,
                        BANK_E_ADDRESS_3 = dataBank3.BANK_E_ADDRESS,
                        SWIFT_CODE_3 = dataBank3.SWIFT_CODE,

                        BANK_CNAME2_3 = dataBank3.BANK_CNAME2,
                        BANK_ENAME2_3 = dataBank3.BANK_ENAME2,
                        BRANCH_CNAME2_3 = dataBank3.BRANCH_CNAME2,
                        BRANCH_ENAME2_3 = dataBank3.BRANCH_ENAME2,
                        ACCOUNT_CNAME2_3 = dataBank3.ACCOUNT_CNAME2,
                        ACCOUNT_ENAME2_3 = dataBank3.ACCOUNT_ENAME2,
                        ACCOUNT2_3 = dataBank3.ACCOUNT2,
                        BANK_C_ADDRESS2_3 = dataBank3.BANK_C_ADDRESS2,
                        BANK_E_ADDRESS2_3 = dataBank3.BANK_E_ADDRESS2,
                        SWIFT_CODE2_3 = dataBank3.SWIFT_CODE2,
                        BANK_ID_3 = dataBank3.BANK_ID,
                        CURRENCY_3 = dataBank3.CURRENCY,
                        ACCOUNT_TYPE_3 = dataBank3.ACCOUNT_TYPE,


                        BANK_CNAME_3_2 = dataBank3_2.BANK_CNAME,
                        BANK_ENAME_3_2 = dataBank3_2.BANK_ENAME,
                        BRANCH_CNAME_3_2 = dataBank3_2.BRANCH_CNAME,
                        BRANCH_ENAME_3_2 = dataBank3_2.BRANCH_ENAME,
                        ACCOUNT_CNAME_3_2 = dataBank3_2.ACCOUNT_CNAME,
                        ACCOUNT_ENAME_3_2 = dataBank3_2.ACCOUNT_ENAME,
                        ACCOUNT_3_2 = dataBank3_2.ACCOUNT,
                        BANK_C_ADDRESS_3_2 = dataBank3_2.BANK_C_ADDRESS,
                        BANK_E_ADDRESS_3_2 = dataBank3_2.BANK_E_ADDRESS,
                        SWIFT_CODE_3_2 = dataBank3_2.SWIFT_CODE,

                        BANK_CNAME2_3_2 = dataBank3_2.BANK_CNAME2,
                        BANK_ENAME2_3_2 = dataBank3_2.BANK_ENAME2,
                        BRANCH_CNAME2_3_2 = dataBank3_2.BRANCH_CNAME2,
                        BRANCH_ENAME2_3_2 = dataBank3_2.BRANCH_ENAME2,
                        ACCOUNT_CNAME2_3_2 = dataBank3_2.ACCOUNT_CNAME2,
                        ACCOUNT_ENAME2_3_2 = dataBank3_2.ACCOUNT_ENAME2,
                        ACCOUNT2_3_2 = dataBank3_2.ACCOUNT2,
                        BANK_C_ADDRESS2_3_2 = dataBank3_2.BANK_C_ADDRESS2,
                        BANK_E_ADDRESS2_3_2 = dataBank3_2.BANK_E_ADDRESS2,
                        SWIFT_CODE2_3_2 = dataBank3_2.SWIFT_CODE2,
                        BANK_ID_3_2 = dataBank3_2.BANK_ID,
                        CURRENCY_3_2 = dataBank3_2.CURRENCY,
                        ACCOUNT_TYPE_3_2 = dataBank3_2.ACCOUNT_TYPE,


                        BANK_CNAME_4 = dataBank4.BANK_CNAME,
                        BANK_ENAME_4 = dataBank4.BANK_ENAME,
                        BRANCH_CNAME_4 = dataBank4.BRANCH_CNAME,
                        BRANCH_ENAME_4 = dataBank4.BRANCH_ENAME,
                        ACCOUNT_CNAME_4 = dataBank4.ACCOUNT_CNAME,
                        ACCOUNT_ENAME_4 = dataBank4.ACCOUNT_ENAME,
                        ACCOUNT_4 = dataBank4.ACCOUNT,
                        BANK_C_ADDRESS_4 = dataBank4.BANK_C_ADDRESS,
                        BANK_E_ADDRESS_4 = dataBank4.BANK_E_ADDRESS,
                        SWIFT_CODE_4 = dataBank4.SWIFT_CODE,

                        BANK_CNAME2_4 = dataBank4.BANK_CNAME2,
                        BANK_ENAME2_4 = dataBank4.BANK_ENAME2,
                        BRANCH_CNAME2_4 = dataBank4.BRANCH_CNAME2,
                        BRANCH_ENAME2_4 = dataBank4.BRANCH_ENAME2,
                        ACCOUNT_CNAME2_4 = dataBank4.ACCOUNT_CNAME2,
                        ACCOUNT_ENAME2_4 = dataBank4.ACCOUNT_ENAME2,
                        ACCOUNT2_4 = dataBank4.ACCOUNT2,
                        BANK_C_ADDRESS2_4 = dataBank4.BANK_C_ADDRESS2,
                        BANK_E_ADDRESS2_4 = dataBank4.BANK_E_ADDRESS2,
                        SWIFT_CODE2_4 = dataBank4.SWIFT_CODE2,
                        BANK_ID_4 = dataBank4.BANK_ID,
                        CURRENCY_4 = dataBank4.CURRENCY,


                        BANK_CNAME_5 = dataBank5.BANK_CNAME,
                        BANK_ENAME_5 = dataBank5.BANK_ENAME,
                        BRANCH_CNAME_5 = dataBank5.BRANCH_CNAME,
                        BRANCH_ENAME_5 = dataBank5.BRANCH_ENAME,
                        ACCOUNT_CNAME_5 = dataBank5.ACCOUNT_CNAME,
                        ACCOUNT_ENAME_5 = dataBank5.ACCOUNT_ENAME,
                        ACCOUNT_5 = dataBank5.ACCOUNT,
                        BANK_C_ADDRESS_5 = dataBank5.BANK_C_ADDRESS,
                        BANK_E_ADDRESS_5 = dataBank5.BANK_E_ADDRESS,
                        SWIFT_CODE_5 = dataBank5.SWIFT_CODE,

                        BANK_CNAME2_5 = dataBank5.BANK_CNAME2,
                        BANK_ENAME2_5 = dataBank5.BANK_ENAME2,
                        BRANCH_CNAME2_5 = dataBank5.BRANCH_CNAME2,
                        BRANCH_ENAME2_5 = dataBank5.BRANCH_ENAME2,
                        ACCOUNT_CNAME2_5 = dataBank5.ACCOUNT_CNAME2,
                        ACCOUNT_ENAME2_5 = dataBank5.ACCOUNT_ENAME2,
                        ACCOUNT2_5 = dataBank5.ACCOUNT2,
                        BANK_C_ADDRESS2_5 = dataBank5.BANK_C_ADDRESS2,
                        BANK_E_ADDRESS2_5 = dataBank5.BANK_E_ADDRESS2,
                        SWIFT_CODE2_5 = dataBank5.SWIFT_CODE2,
                        BANK_ID_5 = dataBank5.BANK_ID,
                        CURRENCY_5 = dataBank5.CURRENCY,


                        BANK_CNAME_6 = dataBank6.BANK_CNAME,
                        BANK_ENAME_6 = dataBank6.BANK_ENAME,
                        BRANCH_CNAME_6 = dataBank6.BRANCH_CNAME,
                        BRANCH_ENAME_6 = dataBank6.BRANCH_ENAME,
                        ACCOUNT_CNAME_6 = dataBank6.ACCOUNT_CNAME,
                        ACCOUNT_ENAME_6 = dataBank6.ACCOUNT_ENAME,
                        ACCOUNT_6 = dataBank6.ACCOUNT,
                        BANK_C_ADDRESS_6 = dataBank6.BANK_C_ADDRESS,
                        BANK_E_ADDRESS_6 = dataBank6.BANK_E_ADDRESS,
                        SWIFT_CODE_6 = dataBank6.SWIFT_CODE,

                        BANK_CNAME2_6 = dataBank6.BANK_CNAME2,
                        BANK_ENAME2_6 = dataBank6.BANK_ENAME2,
                        BRANCH_CNAME2_6 = dataBank6.BRANCH_CNAME2,
                        BRANCH_ENAME2_6 = dataBank6.BRANCH_ENAME2,
                        ACCOUNT_CNAME2_6 = dataBank6.ACCOUNT_CNAME2,
                        ACCOUNT_ENAME2_6 = dataBank6.ACCOUNT_ENAME2,
                        ACCOUNT2_6 = dataBank6.ACCOUNT2,
                        BANK_C_ADDRESS2_6 = dataBank6.BANK_C_ADDRESS2,
                        BANK_E_ADDRESS2_6 = dataBank6.BANK_E_ADDRESS2,
                        SWIFT_CODE2_6 = dataBank6.SWIFT_CODE2,
                        BANK_ID_6 = dataBank6.BANK_ID,
                        CURRENCY_6 = dataBank6.CURRENCY,


                        BANK_CNAME_7 = dataBank7.BANK_CNAME,
                        BANK_ENAME_7 = dataBank7.BANK_ENAME,
                        BRANCH_CNAME_7 = dataBank7.BRANCH_CNAME,
                        BRANCH_ENAME_7 = dataBank7.BRANCH_ENAME,
                        ACCOUNT_CNAME_7 = dataBank7.ACCOUNT_CNAME,
                        ACCOUNT_ENAME_7 = dataBank7.ACCOUNT_ENAME,
                        ACCOUNT_7 = dataBank7.ACCOUNT,
                        BANK_C_ADDRESS_7 = dataBank7.BANK_C_ADDRESS,
                        BANK_E_ADDRESS_7 = dataBank7.BANK_E_ADDRESS,
                        SWIFT_CODE_7 = dataBank7.SWIFT_CODE,

                        BANK_CNAME2_7 = dataBank7.BANK_CNAME2,
                        BANK_ENAME2_7 = dataBank7.BANK_ENAME2,
                        BRANCH_CNAME2_7 = dataBank7.BRANCH_CNAME2,
                        BRANCH_ENAME2_7 = dataBank7.BRANCH_ENAME2,
                        ACCOUNT_CNAME2_7 = dataBank7.ACCOUNT_CNAME2,
                        ACCOUNT_ENAME2_7 = dataBank7.ACCOUNT_ENAME2,
                        ACCOUNT2_7 = dataBank7.ACCOUNT2,
                        BANK_C_ADDRESS2_7 = dataBank7.BANK_C_ADDRESS2,
                        BANK_E_ADDRESS2_7 = dataBank7.BANK_E_ADDRESS2,
                        SWIFT_CODE2_7 = dataBank7.SWIFT_CODE2,
                        BANK_ID_7 = dataBank7.BANK_ID,
                        CURRENCY_7 = dataBank7.CURRENCY,


                        ORDER_ID = strOrderID,
                        EMP_CODE = dataOrder.EMP_CODE,
                        IB_CODE = dataOrder.IB_CODE,
                        INTERNAL_IB_CODE=dataOrder.INTERNAL_IB_CODE,
                        ATTRIBUTE1 = dataOrder.ATTRIBUTE1,
                        EXTEND_DATE = dataOrder.EXTEND_DATE,
                        ORDER_END = dataOrder.ORDER_END,
                        ORDER_NO = dataOrder.ORDER_NO,
                        MULTI_ORDER_FLAG = dataOrder.MULTI_ORDER_FLAG,
                        IS_REMIT_TO_AUS = dataOrder.IS_REMIT_TO_AUS,
                        ORG_CODE = dataOrder.ORG_CODE,
                        BONUS_OPTION = dataOrder.BONUS_OPTION,

                        USER_NAME = strUserName
                    }, CommandType.Text);
                    #endregion
                }

                return true;
            }
            catch (Exception ex)
            {
                exError = ex;
                return false;
            }
        }



        public bool BatchAuditOrder(List<BL_ORDER> dataOrder,string strUserName, out Exception exError)
        {
            exError = null;
            try
            {
                var liOrderID = new List<string>();
                dataOrder.ForEach(x =>
                {
                    if (x.ORDER_ID != null)
                    {
                        liOrderID.Add(x.ORDER_ID.Value.ConvertToString());
                    }
                });
                var strSQL = @"
BEGIN 
    UPDATE BL_ORDER A
        SET
              A.STATUS_CODE = '3'
            , LAST_UPDATE_DATE = SYSDATE
            , LAST_UPDATED_BY = :USER_NAME
    WHERE A.ORDER_ID IN :ORDER_ID;

    INSERT INTO BL_ORDER_UPDATE_LOG
    (
        BL_ORDER_UPDATE_LOG_ID, 
        ORDER_ID, 
        ORDER_NO, 
        STATUS_CODE, 
        REMARK_TEXT, 
        UPDATE_DATE, 
        UPDATED_BY
    )
    SELECT 
        BL_ORDER_UPDATE_LOG_S1.NEXTVAL,
        A.ORDER_ID, 
        A.ORDER_NO, 
        '3', 
        '行政查核 > STATUS_CODE = 3', 
        SYSDATE, 
        :USER_NAME
    FROM BL_ORDER A
    WHERE A.ORDER_ID IN :ORDER_ID ;
END;
";
                base.ExecuteScalar(strSQL, new
                {
                    ORDER_ID = liOrderID,
                    USER_NAME = strUserName
                }, CommandType.Text);

                return true;
            }
            catch (Exception ex)
            {
                // log  
                exError = ex;
                return false;
            }
        }

        public bool BatchApproveOrder(List<BL_ORDER> dataOrder, string strUserName, out Exception exError)
        {
            exError = null;
            try
            {
                var liOrderID = new List<string>();
                dataOrder.ForEach(x =>
                {
                    if (x.ORDER_ID != null)
                    {
                        liOrderID.Add(x.ORDER_ID.Value.ConvertToString());
                    }
                });
                //                var strSQL = @"
                //UPDATE BL_ORDER A
                //    SET
                //        A.STATUS_CODE = '4'
                //WHERE A.ORDER_ID IN :ORDER_ID

                //";
                //                base.ExecuteScalar(strSQL, new
                //                {
                //                    ORDER_ID = liOrderID
                //                }, CommandType.Text);
                var param = new DynamicParameters();
                int i = 0;
                var strSQL = @"
BEGIN

    UPDATE BL_ORDER A
        SET
            A.STATUS_CODE = '4'
            , LAST_UPDATE_DATE = SYSDATE
            , LAST_UPDATED_BY = :USER_NAME
    WHERE A.ORDER_ID IN :ORDER_ID ;

    INSERT INTO BL_ORDER_UPDATE_LOG
    (
        BL_ORDER_UPDATE_LOG_ID, 
        ORDER_ID, 
        ORDER_NO, 
        STATUS_CODE, 
        REMARK_TEXT, 
        UPDATE_DATE, 
        UPDATED_BY
    )
    SELECT 
        BL_ORDER_UPDATE_LOG_S1.NEXTVAL,
        A.ORDER_ID, 
        A.ORDER_NO, 
        '4', 
        '香港審批 > STATUS_CODE = 4', 
        SYSDATE, 
        :USER_NAME
    FROM BL_ORDER A
    WHERE A.ORDER_ID IN :ORDER_ID ;
";
                liOrderID.ForEach(x =>
                {
                    //  2019/07/10 > 取消這段, 這個新增客戶已經寫過, 不再Insert
//                    strSQL += @"
//INSERT INTO BL_CUST_SALES
//( CUST_ID, IB_CODE, ACTIVE, CREATION_DATE, CREATED_BY, LAST_UPDATE_DATE, LAST_UPDATED_BY)
//SELECT 
//    A.CUST_ID
//    , B.IB_CODE
//    , 'Y'
//    , SYSDATE
//    , :USER_NAME
//    , SYSDATE
//    , :USER_NAME
//FROM BL_ORDER A
//    INNER JOIN BL_EMP B
//        ON A.EMP_CODE = B.EMP_CODE
//WHERE NOT EXISTS
//    (
//        SELECT *
//        FROM BL_CUST_SALES X
//        WHERE A.CUST_ID = X.CUST_ID
//    )
//    AND A.ORDER_ID = :ORDER_ID" + i.ConvertToString() + @"
// ; ";

                    strSQL += @"
UPDATE BL_CUST_SALES A
    SET
        IB_CODE = 
            (
                SELECT 
                    Y.IB_CODE
                from BL_ORDER X
                    INNER JOIN BL_EMP Y
                        ON X.EMP_CODE = Y.EMP_CODE
                WHERE X.ORDER_ID = :ORDER_ID" + i.ConvertToString() + @"
            )
        , LAST_UPDATE_DATE = SYSDATE
WHERE A.CUST_ID = (SELECT CUST_ID FROM BL_ORDER X WHERE X.ORDER_ID = :ORDER_ID" + i.ConvertToString() + @")
 ; ";

                    param.Add("ORDER_ID" + i.ConvertToString(), x);
                    i += 1;
                });

                param.Add("ORDER_ID", liOrderID);
                param.Add("USER_NAME", strUserName);

                strSQL += " END ;";
                base.ExecuteScalar(strSQL, param, CommandType.Text);
                return true;
            }
            catch (Exception ex)
            {
                exError = ex;
                return false;
            }
        }



        public bool BatchRejectOrder(List<BL_ORDER> dataOrder,string strRejectReason,string strUserName, out Exception exError)
        {
            exError = null;
            try
            {

                var liOrderID = new List<string>();
                dataOrder.ForEach(x =>
                {
                    if (x.ORDER_ID != null)
                    {
                        liOrderID.Add(x.ORDER_ID.Value.ConvertToString());
                    }
                });
                var strSQL = @"
BEGIN
    UPDATE BL_ORDER A
        SET
            A.STATUS_CODE = '9'
            , A.REJECT_REASON = :REJECT_REASON
            , LAST_UPDATE_DATE = SYSDATE
            , LAST_UPDATED_BY = :USER_NAME
    WHERE A.ORDER_ID IN :ORDER_ID ;


    INSERT INTO BL_ORDER_UPDATE_LOG
    (
        BL_ORDER_UPDATE_LOG_ID, 
        ORDER_ID, 
        ORDER_NO, 
        STATUS_CODE, 
        REMARK_TEXT, 
        UPDATE_DATE, 
        UPDATED_BY
    )
    SELECT 
        BL_ORDER_UPDATE_LOG_S1.NEXTVAL,
        A.ORDER_ID, 
        A.ORDER_NO, 
        '9', 
        '退件 > STATUS_CODE = 9', 
        SYSDATE, 
        :USER_NAME
    FROM BL_ORDER A
    WHERE A.ORDER_ID IN :ORDER_ID ;

END;
                ";
                base.ExecuteScalar(strSQL, new
                {
                    ORDER_ID = liOrderID,
                    REJECT_REASON = strRejectReason,
                    USER_NAME = strUserName
                }, CommandType.Text);

                return true;
            }
            catch (Exception ex)
            {
                exError = ex;
                return false;
            }
        }


        public List<BL_ORDER> GetExpiredDataByYM(string strYear, string strMonth, string strORG, out Exception exError)
        {
            exError = null;
            try
            {
                var strYM = strYear + strMonth;
                var strSQL = @"
select 
    A.* 
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_ENAME)  AS CUST_ENAME
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY (B.CUST_CNAME) AS CUST_CNAME
    , D.CNAME AS SALES_NAME
    ,BL_INTEREST2_PKG.RETURN_BALANCE(a.ORDER_ID) AS BALANCE
    --, 0 AS BALANCE
from BL_ORDER A
    INNER JOIN BL_CUST B
        ON A.CUST_ID = B.CUST_ID
    LEFT JOIN BL_CUST_SALES C
        ON A.CUST_ID = C.CUST_ID
            AND (C.ACTIVE IS NULL OR C.ACTIVE = 'Y' )   --AND C.ACTIVE <> 'N'
    LEFT JOIN BL_EMP D
        ON C.IB_CODE = D.IB_CODE

WHERE TO_CHAR(A.END_DATE, 'YYYYMM') = :YM
    AND A.STATUS_CODE NOT IN (-1)
";
                if (strORG.ConvertToString() != string.Empty)
                {
                    switch (strORG)
                    {
                        case "TW1":
                            strSQL += @"
    AND A.ORG_CODE IN ('TW1', 'IND')
";
                            break;
                        default:
                            strSQL += @"
    AND A.ORG_CODE = '" + strORG + @"'
";
                            break;
                    }
                }

                return base.GetList(strSQL, new
                {
                    YM = strYM
                }, CommandType.Text).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }



        public bool TransOrder(string ORDER_ID, string CUST_ID_ori, string CUST_ID_new, string USER_NAME, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
UPDATE BL_ORDER A
    SET
        A.CUST_ID = :CUST_ID_NEW
        , A.ORIGIN_CUST_ID = :CUST_ID_ORI
        , A.TRANSFER_DATE = SYSDATE
        , LAST_UPDATE_DATE = SYSDATE
        , LAST_UPDATED_BY = :USER_NAME
WHERE A.ORDER_ID = :ORDER_ID
";
                base.ExecuteScalar(strSQL, new
                {
                    CUST_ID_NEW = CUST_ID_new,
                    CUST_ID_ORI = CUST_ID_ori,
                    USER_NAME = USER_NAME,
                    ORDER_ID = ORDER_ID
                }, CommandType.Text);
                return true;
            }
            catch (Exception ex)
            {
                exError = ex;
                return false;
            }
        }



        public bool BatchVoidOrder(List<BL_ORDER> dataOrder, string strUserName, out Exception exError)
        {
            exError = null;
            try
            {

                var liOrderID = new List<string>();
                dataOrder.ForEach(x =>
                {
                    if (x.ORDER_ID != null)
                    {
                        liOrderID.Add(x.ORDER_ID.Value.ConvertToString());
                    }
                });
                var strSQL = @"
BEGIN
    UPDATE BL_ORDER A
        SET
            A.STATUS_CODE = '-1'
            , LAST_UPDATE_DATE = SYSDATE
            , LAST_UPDATED_BY = :USER_NAME
    WHERE A.ORDER_ID IN :ORDER_ID ;


    INSERT INTO BL_ORDER_UPDATE_LOG
    (
        BL_ORDER_UPDATE_LOG_ID, 
        ORDER_ID, 
        ORDER_NO, 
        STATUS_CODE, 
        REMARK_TEXT, 
        UPDATE_DATE, 
        UPDATED_BY
    )
    SELECT 
        BL_ORDER_UPDATE_LOG_S1.NEXTVAL,
        A.ORDER_ID, 
        A.ORDER_NO, 
        '-1', 
        'Void > STATUS_CODE = -1', 
        SYSDATE, 
        :USER_NAME
    FROM BL_ORDER A
    WHERE A.ORDER_ID IN :ORDER_ID ;

END;
                ";
                base.ExecuteScalar(strSQL, new
                {
                    ORDER_ID = liOrderID,
                    USER_NAME = strUserName
                }, CommandType.Text);

                return true;
            }
            catch (Exception ex)
            {
                exError = ex;
                return false;
            }
        }







    }




    class Employee

    {



        private string name;



        public string Name

        {

            get { return name; }

            set { name = value; }

        }

        private string address;



        public string Address

        {

            get { return address; }

            set { address = value; }

        }

        private string ssn;



        public string Ssn

        {

            get { return ssn; }

            set { ssn = value; }

        }

        private Int16 age;



        public Int16 Age

        {

            get { return age; }

            set { age = value; }

        }



        public Employee(string EmpName, string EmpAddress, string EmpSsn, Int16 EmpAge)

        {

            this.name = EmpName;

            this.address = EmpAddress;

            this.ssn = EmpSsn;

            this.age = EmpAge;

        }

    }


    
class Company

    {

        private List<Employee> m_employees;



        public Company()

        {

            m_employees = new List<Employee>();

            m_employees.Add(new Employee("Mahesh Chand", "112 New Road, Chadds Ford, PA", "123-21-1212", 30));

            m_employees.Add(new Employee("Jack Mohita", "Pear Lane, New York 23231", "878-12-2334", 23));

            m_employees.Add(new Employee("Renee Singer", "Near medow, Philadelphia, PA", "980-00-2320", 20));

        }

        public List<Employee> GetEmployees()

        {

            return m_employees;

        }

    }


}
