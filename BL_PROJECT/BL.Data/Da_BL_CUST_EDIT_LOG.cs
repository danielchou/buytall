﻿using MK.Demo.Model;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MK.Demo.Utility;
using Dapper;

namespace MK.Demo.Data
{
    public class Da_BL_CUST_EDIT_LOG : DataRepository<BL_CUST_EDIT_LOG>
    {
        OracleConnection _Conn;
        public Da_BL_CUST_EDIT_LOG(OracleConnection conn)
         : base(conn)
        { _Conn = conn; }


        public BL_CUST_LOG GetCustLogData(string strCustID, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
SELECT 
        A.CUST_ID
        , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.CUST_ENAME) AS CUST_ENAME
        , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.CUST_ENAME2) AS CUST_ENAME2
        , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.CUST_CNAME) AS CUST_CNAME
        , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.CUST_CNAME2) AS CUST_CNAME2
        , A.PASSPORT
        , A.PASSPORT2
        , A.PASSPORT_REGION
        , A.PASSPORT_REGION2
        , A.PASSPORT_ACTIVE_END_DATE
        , A.PASSPORT_ACTIVE_END_DATE2
        , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.ID_NUMBER) AS ID_NUMBER
        , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.ID_NUMBER2) AS ID_NUMBER2
        , A.SEX
        , A.SEX2
        , A.DATE_OF_BIRTH
        , A.DATE_OF_BIRTH2
        , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.EMAIL_1) AS EMAIL_1
        , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.EMAIL_2) AS EMAIL_2
        , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.EMAIL2_1) AS EMAIL2_1
        , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.EMAIL2_2) AS EMAIL2_2
        , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.PHONE_1) AS PHONE_1
        , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.PHONE_2) AS PHONE_2
        , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.PHONE2_1) AS PHONE2_1
        , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.PHONE2_2) AS PHONE2_2
        , A.POSTAL_CODE
        , A.POSTAL_CODE2
        , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.C_ADDRESS) AS C_ADDRESS
        , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.C_ADDRESS2) AS C_ADDRESS2
        , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.E_ADDRESS) AS E_ADDRESS
        , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.E_ADDRESS2) AS E_ADDRESS2
        , A.MAIL_TYPE_MONTHLY
        , A.MAIL_TYPE_END
        , A.CREATION_DATE
        , A.CREATED_BY
        , A.LAST_UPDATE_DATE
        , A.LAST_UPDATED_BY
        , A.CUST_CNAME_RAW
        , A.CUST_CNAME2_RAW
        , A.MT4_ACCOUNT
        , A.WEB_PASSWORD
        , A.IS_NEED_CHANGE_PWD
        , A.UNION_ID_NUMBER
        , A.ORG_CODE
FROM BL_CUST A
WHERE A.CUST_ID = :CUST_ID
";
                return this._Conn.Query<BL_CUST_LOG>(strSQL, new
                {
                    CUST_ID = strCustID
                }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }

        public List<BL_CUST_BANK_LOG> GetCustBankLogData(string strCust_ID, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
SELECT 
        A.CUST_ID
        , A.BANK_CNAME
        , A.BANK_CNAME2
        , A.BANK_ENAME
        , A.BANK_ENAME2
        , A.BRANCH_CNAME
        , A.BRANCH_CNAME2
        , A.BRANCH_ENAME
        , A.BRANCH_ENAME2
        , A.ACCOUNT_CNAME
        , A.ACCOUNT_CNAME2
        , A.ACCOUNT_ENAME
        , A.ACCOUNT_ENAME2
        , A.ACCOUNT
        , A.ACCOUNT2
        , A.BANK_C_ADDRESS
        , A.BANK_C_ADDRESS2
        , A.BANK_E_ADDRESS
        , A.BANK_E_ADDRESS2
        , A.SWIFT_CODE
        , A.SWIFT_CODE2
        , A.CREATION_DATE
        , A.CREATED_BY
        , A.LAST_UPDATE_DATE
        , A.LAST_UPDATED_BY
        , A.BANK_ID
        , A.CURRENCY
FROM BL_CUST_BANK A
WHERE A.CUST_ID = :CUST_ID
";
                return this._Conn.Query<BL_CUST_BANK_LOG>(strSQL, new
                {
                    CUST_ID = strCust_ID
                }).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }

        public bool WriteLog(List<BL_CUST_EDIT_LOG> data, out Exception exError)
        {
            exError = null;

            return true;    //  Todo > Alex > Oracle Max Data Length = 4000, 但只要銀行資料超過一筆, 基本上長度就不夠了

            try
            {
                var strSQL = string.Empty;
                var arrParam = new DynamicParameters();
                int i = 0;

                data.ForEach(x =>
                {
                    strSQL += @"
INSERT INTO BL_CUST_EDIT_LOG
(
    BL_CUST_EDIT_LOG_S1
    , EDIT_BY
    , EDIT_DATE
    , EDIT_OLD_DESC
    , EDIT_NEW_DESC
)
SELECT
    BL_CUST_EDIT_LOG_S1.NEXTVAL
    , :EDIT_BY" + i.ConvertToString() + @"
    , SYSDATE
    , :EDIT_OLD_DESC" + i.ConvertToString() + @"
    , :EDIT_NEW_DESC" + i.ConvertToString() + @"
FROM DUAL;
";
                    arrParam.Add("EDIT_BY" + i.ConvertToString(), x.EDIT_BY);
                    arrParam.Add("EDIT_OLD_DESC" + i.ConvertToString(), x.EDIT_OLD_DESC);
                    arrParam.Add("EDIT_NEW_DESC" + i.ConvertToString(), x.EDIT_NEW_DESC);
                    i += 1;
                });

                if (strSQL != string.Empty)
                {
                    strSQL = " BEGIN " + strSQL + " END; ";
                    base.ExecuteScalar(strSQL, arrParam, CommandType.Text);
                }
                return true;
            }
            catch (Exception ex)
            {
                exError = ex;
                return false;
            }
        }

        public List<BL_CUST_EDIT_LOG> GetLogDataByCustID(string strCust_ID, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
SELECT *
FROM BL_CUST_EDIT_LOG A
WHERE A.EDIT_OLD_DESC LIKE '%'||:CUST_ID||'%'
ORDER BY A.EDIT_DATE DESC
";
                return base.GetList(strSQL, new
                {
                    CUST_ID = "\"CUST_ID\":" + strCust_ID + ".0,"
                }, CommandType.Text).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }


    }
}
