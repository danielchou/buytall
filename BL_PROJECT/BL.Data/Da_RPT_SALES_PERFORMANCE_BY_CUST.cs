﻿using MK.Demo.Model;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MK.Demo.Utility;
using Dapper;

namespace MK.Demo.Data
{
    public class Da_RPT_SALES_PERFORMANCE_BY_CUST : DataRepository<RPT_SALES_PERFORMANCE_BY_CUST>
    {
        OracleConnection _Conn;
        public Da_RPT_SALES_PERFORMANCE_BY_CUST(OracleConnection conn)
         : base(conn)
        { _Conn = conn; }


        public List<RPT_SALES_PERFORMANCE_BY_CUST> GetReportDataByRange(DateTime Date_S, DateTime Date_E,string strORG, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
SELECT 
      ATTRIBUTE1 SALES
    , TO_CHAR(FROM_DATE,'W') WEEK
    , ORDER_NO
    , CASE WHEN CUST_CNAME2 IS NOT NULL THEN CUST_CNAME||'/'||CUST_CNAME2 ELSE CUST_CNAME END CNAME
    , CASE WHEN CUST_CNAME2 IS NOT NULL THEN CUST_ENAME||'/'||CUST_ENAME2 ELSE CUST_ENAME2 END ENAME
    , CURRENCY
    , CASE 
        WHEN AMOUNT_USD IS NOT NULL THEN AMOUNT_USD 
        WHEN AMOUNT_NTD IS NOT NULL THEN AMOUNT_NTD
        WHEN AMOUNT_RMB IS NOT NULL THEN AMOUNT_RMB 
        WHEN AMOUNT_EUR IS NOT NULL THEN AMOUNT_EUR 
        WHEN AMOUNT_AUD IS NOT NULL THEN AMOUNT_AUD 
        WHEN AMOUNT_JPY IS NOT NULL THEN AMOUNT_JPY
        END AMOUNT
    , FROM_DATE
    , END_DATE
    , YEAR
    , CASE 
        WHEN AMOUNT_USD IS NOT NULL THEN NVL(AMOUNT_USD,0) -NVL(OLD_AMOUNT_USD,0) 
        WHEN AMOUNT_NTD IS NOT NULL THEN NVL(AMOUNT_NTD,0) -NVL(OLD_AMOUNT_NTD,0) 
        WHEN AMOUNT_RMB IS NOT NULL THEN NVL(AMOUNT_RMB,0) -NVL(OLD_AMOUNT_RMB,0)  
        WHEN AMOUNT_EUR IS NOT NULL THEN NVL(AMOUNT_EUR,0) -NVL(OLD_AMOUNT_EUR,0)  
        WHEN AMOUNT_AUD IS NOT NULL THEN NVL(AMOUNT_AUD,0) -NVL(OLD_AMOUNT_AUD,0)  
        WHEN AMOUNT_JPY IS NOT NULL THEN NVL(AMOUNT_JPY,0) -NVL(OLD_AMOUNT_JPY,0)  
        END AMOUNT2
    , :P_DATE_FROM AS DATE_S
    , :P_DATE_TO AS DATE_E
FROM BL_ORDER_V1 
WHERE 
    FROM_DATE BETWEEN :P_DATE_FROM AND :P_DATE_TO
";
                if (strORG.ConvertToString() != string.Empty)
                {
                    switch (strORG)
                    {
                        case "TW1":
                            strSQL += @"
    AND ORG_CODE IN ('TW1', 'IND')
";
                            break;
                        default:
                            strSQL += @"
    AND ORG_CODE = '" + strORG + @"'
";
                            break;
                    }
                }
                strSQL += @"

ORDER BY ATTRIBUTE1, FROM_DATE
";
                return base.GetList(strSQL, new
                {
                    P_DATE_FROM = Date_S,
                    P_DATE_TO = Date_E
                }, CommandType.Text).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }

    }
}
