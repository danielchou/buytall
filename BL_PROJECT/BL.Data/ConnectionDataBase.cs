﻿using MK.Demo.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MK.Demo.Data
{
    public abstract class ConnectionDataBase
    {
        protected bool IsTestMode = false;
        private string _connectionString;
        public string ConnectionString
        {
      
            //get { return _connectionString; }
            //set { _connectionString = value; }
            get
            {
                var strConnectionString = string.Empty;
                if (this.IsTestMode)
                {
                    strConnectionString = ConfigurationManager.ConnectionStrings["Oracle_Test"].ConnectionString;
                }
                else
                {
                    strConnectionString = ConfigurationManager.ConnectionStrings["Oracle_Release"].ConnectionString;
                }
                return strConnectionString;
            }
        }

        public ConnectionDataBase()
        {
            //if (this.IsTestMode)
            //{
            //    this.ConnectionString = ConfigurationManager.ConnectionStrings["Oracle_Test"].ConnectionString;
            //}
            //else
            //{
            //    this.ConnectionString = ConfigurationManager.ConnectionStrings["Oracle_Release"].ConnectionString;
            //}
           


            //if (SettingVars.G_IS_TEST_MODE == true)
            //{
            //    this.ConnectionStringSQL = ConfigurationManager.ConnectionStrings["Oracle_Test"].ConnectionString;
            //}
            //else
            //{
            //    this.ConnectionStringSQL = ConfigurationManager.ConnectionStrings["Oracle_Release"].ConnectionString;
            //}
        }


    }
}
