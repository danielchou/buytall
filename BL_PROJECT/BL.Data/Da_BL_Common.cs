﻿using MK.Demo.Model;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MK.Demo.Utility;
using Dapper;

namespace MK.Demo.Data
{
    public class Da_BL_Common : DataRepository<BL_Common>
    {
        OracleConnection _Conn;
        public Da_BL_Common(OracleConnection conn)
         : base(conn)
        { _Conn = conn; }

        public bool ExecNTD_Payment_Job(string strUserName, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
BEGIN
    BL_INTEREST2_PKG.INTEREST_JOB_DAILY('NTD');

    INSERT INTO BL_PROCESS_LOG
    ( BL_PROCESS_LOG_S1, CREATE_BY, CREATE_DATE, PROCESS_NAME, PROCESS_DESC )
    SELECT 
        BL_PROCESS_LOG_S1.NEXTVAL
        , :USER_NAME
        , SYSDATE
        , 'INTEREST_JOB_DAILY NTD'
        , '執行台幣提領執行作業'
    FROM DUAL ; 

END;
";
                this._Conn.Execute(strSQL, new
                {
                    USER_NAME = strUserName
                });
                return true;
            }
            catch (Exception ex)
            {
                exError = ex;
                return false;
            }
        }


    }
}
