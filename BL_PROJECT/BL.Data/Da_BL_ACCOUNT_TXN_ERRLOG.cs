﻿using MK.Demo.Model;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MK.Demo.Utility;
using Dapper;

namespace MK.Demo.Data
{
    public class Da_BL_ACCOUNT_TXN_ERRLOG : DataRepository<BL_ACCOUNT_TXN_ERRLOG>
    {
        OracleConnection _Conn;
        public Da_BL_ACCOUNT_TXN_ERRLOG(OracleConnection conn)
         : base(conn)
        { _Conn = conn; }

        public bool WriteErrorLog(long ERROR_ID, BL_ACCOUNT_TXN_TEMP data, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
INSERT INTO BL_ACCOUNT_TXN_ERRLOG
(
      ERROR_ID
    , ORDER_ID
    , BATCH_WITHDRAWAL_ID
    , TXN_TYPE
    , AMOUNT_USD
    , AMOUNT_RMB
    , AMOUNT_NTD
    , AMOUNT_EUR
    , AMOUNT_AUD
    , WITHDRAWAL_TYPE
    , REMARK
    , BONUS_TSF
    , NEW_CONTRACT_NO
    , END_BALANCE_FLAG
    , APPLY_USER
    , CHECK_USER
    , APPROVER
    , CREATION_DATE
)
SELECT 
      :ERROR_ID
    , :ORDER_ID
    , :BATCH_WITHDRAWAL_ID
    , :TXN_TYPE
    , :AMOUNT_USD
    , :AMOUNT_RMB
    , :AMOUNT_NTD
    , :AMOUNT_EUR
    , :AMOUNT_AUD
    , :WITHDRAWAL_TYPE
    , :REMARK
    , :BONUS_TSF
    , :NEW_CONTRACT_NO
    , :END_BALANCE_FLAG
    , :APPLY_USER
    , :CHECK_USER
    , :APPROVER
    , :CREATION_DATE
FROM DUAL
";

                base.ExecuteScalar(strSQL, new
                {
                    ERROR_ID = ERROR_ID,
                    ORDER_ID = data.ORDER_ID,
                    BATCH_WITHDRAWAL_ID = data.BATCH_WITHDRAWAL_ID,
                    TXN_TYPE = data.TXN_TYPE,
                    AMOUNT_USD = data.AMOUNT_USD,
                    AMOUNT_RMB = data.AMOUNT_RMB,
                    AMOUNT_NTD = data.AMOUNT_NTD,
                    AMOUNT_EUR = data.AMOUNT_EUR,
                    AMOUNT_AUD = data.AMOUNT_AUD,
                    WITHDRAWAL_TYPE = data.WITHDRAWAL_TYPE,
                    REMARK = data.REMARK,
                    BONUS_TSF = data.BONUS_TSF,
                    NEW_CONTRACT_NO = data.NEW_CONTRACT_NO,
                    END_BALANCE_FLAG = data.END_BALANCE_FLAG,
                    APPLY_USER = data.APPLY_USER,
                    CHECK_USER = data.CHECK_USER,
                    APPROVER = data.APPROVER,
                    CREATION_DATE = data.CREATION_DATE
                }, CommandType.Text);
                return true;
            }
            catch (Exception ex)
            {
                exError = ex;
                return false;
            }
        }


    }
}
