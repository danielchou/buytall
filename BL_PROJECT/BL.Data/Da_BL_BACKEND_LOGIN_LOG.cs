﻿using MK.Demo.Model;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MK.Demo.Utility;
using Dapper;

namespace MK.Demo.Data
{
    public class Da_BL_BACKEND_LOGIN_LOG : DataRepository<BL_BACKEND_LOGIN_LOG>
    {
        OracleConnection _Conn;
        public Da_BL_BACKEND_LOGIN_LOG(OracleConnection conn)
         : base(conn)
        { _Conn = conn; }

        public bool WriteLog(long LogSN, string strAccout, string strIPAddress, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
INSERT INTO BL_BACKEND_LOGIN_LOG
(
      BL_BACKEND_LOGIN_LOG_SN
    , LOGIN_DATETIME
    , LOGIN_IP
    , LOGIN_ACCOUNT
)
SELECT
      :LOG_SN
    , SYSDATE
    , :IP_ADDRESS
    , :ACCOUNT
FROM DUAL
";
                this._Conn.Execute(strSQL, new
                {
                    LOG_SN = LogSN,
                    IP_ADDRESS = strIPAddress,
                    ACCOUNT = strAccout
                });
                return true;
            }
            catch (Exception ex)
            {
                exError = ex;
                return false;
            }
        }

        public List<BL_BACKEND_LOGIN_LOG> GetMSG_CHECK_DATA(string strAccount, decimal MaxMsgSN, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
SELECT *
FROM BL_BACKEND_LOGIN_LOG A
WHERE A.LOGIN_ACCOUNT = :ACCOUNT
    AND A.BL_MSG_DATA_SN = :MAX_SN
";
                return base.GetList(strSQL, new
                {
                    ACCOUNT = strAccount,
                    MAX_SN = MaxMsgSN
                }, CommandType.Text).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }

        public bool UpdateMSG_CheckLog(decimal MsgSN, long LogSN, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
UPDATE BL_BACKEND_LOGIN_LOG A
SET
    A.BL_MSG_DATA_SN = :MSG_SN
WHERE A.BL_BACKEND_LOGIN_LOG_SN = :LOG_SN
";
                base.Connection.Execute(strSQL, new
                {
                    MSG_SN = MsgSN,
                    LOG_SN = LogSN
                });
                return true;
            }
            catch (Exception ex)
            {
                exError = ex;
                return false;
            }
        }


    }
}
