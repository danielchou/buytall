﻿using MK.Demo.Model;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MK.Demo.Utility;
using Dapper;

namespace MK.Demo.Data
{
    public class Da_BL_PERIOD : DataRepository<BL_PERIOD>
    {
        public Da_BL_PERIOD(OracleConnection conn)
         : base(conn)
        { }


        public BL_PERIOD GetMaxDate(out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
SELECT 
      MAX(YEAR) AS YEAR
    , MAX(MON) AS MON
FROM BL_PERIOD A
WHERE A.STATUS = 'CLOSED'
";
                return base.GetSingleOrDefault(strSQL, null, CommandType.Text);
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }

        public List<BL_PERIOD> GetDataByYear(string strYear, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
SELECT *
FROM BL_PERIOD A
WHERE A.YEAR = :YEAR
";
                return base.GetList(strSQL, new
                {
                    YEAR = strYear
                }, CommandType.Text).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }


        public bool UpdateData(List<BL_PERIOD> dataSave, string strUserName, out Exception exError)
        {
            exError = null;
            try
            {
                var param = new DynamicParameters();
                var strSQL = string.Empty;
                int i = 0;

                dataSave.ForEach(x =>
                {
                    strSQL += @"
    UPDATE BL_PERIOD
    SET
          STATUS = :STATUS" + i.ConvertToString() + @"
        , LAST_UPDATE_DATE = SYSDATE
        , LAST_UPDATED_BY = :USER_NAME
    WHERE 1 = 1
        AND YEAR = :YEAR" + i.ConvertToString() + @"
        AND MON = :MON" + i.ConvertToString() + @"
    ;
";
                    param.Add("STATUS" + i.ConvertToString(), x.STATUS);
                    param.Add("YEAR" + i.ConvertToString(), x.YEAR);
                    param.Add("MON" + i.ConvertToString(), x.MON);
                    i += 1;
                });

                if (strSQL != string.Empty)
                {
                    strSQL = "BEGIN " + strSQL + " END; ";
                    param.Add("USER_NAME", strUserName);
                    this.Connection.Execute(strSQL, param);
                }

                return true;
            }
            catch (Exception ex)
            {
                exError = ex;
                return false;
            }
        }


    }
}
