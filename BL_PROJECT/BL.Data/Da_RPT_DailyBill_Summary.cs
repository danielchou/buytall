﻿using MK.Demo.Model;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MK.Demo.Utility;
using Dapper;


namespace MK.Demo.Data
{
    public class Da_RPT_DailyBill_Summary : DataRepository<RPT_DailyBill_Summary>
    {
        OracleConnection _Conn;
        public Da_RPT_DailyBill_Summary(OracleConnection conn)
         : base(conn)
        { _Conn = conn; }

        public List<RPT_DailyBill_Summary> GetReportData(DateTime txt_date,string strORG, out Exception exError)
        {
            exError = null;
            try
            {
                var strScriptORG = string.Empty;
                if (strORG.ConvertToString() != string.Empty)
                {
                    switch (strORG)
                    {
                        case "TW1":
                            strScriptORG = @"
    AND T.ORG_CODE IN ('TW1', 'IND')
    AND C.ORG_CODE IN ('TW1', 'IND')
";
                            break;
                        default:
                            strScriptORG = @"
    AND T.ORG_CODE = '" + strORG + @"'
    AND C.ORG_CODE = '" + strORG + @"'
";
                            break;
                    }
                }
                var strSQL = @"
SELECT 
      TRUNC(A.TXN_DATE) AS TXN_DATE
    , A.ORDER_NO
    , CUST_ENAME
    , CASE WHEN ACTUAL_AMOUNT>0 THEN '打款' ELSE '內轉新約' END AS TYPE
    , CASE WHEN ACTUAL_AMOUNT>0 THEN BANK_ENAME ELSE NULL END AS BANK_ENAME
    , CASE WHEN ACTUAL_AMOUNT>0 THEN BRANCH_ENAME ELSE NULL END AS BRANCH_ENAME
    , CASE WHEN ACTUAL_AMOUNT>0 THEN ACCOUNT_CNAME ELSE NULL END AS ACCOUNT_CNAME
    , CASE WHEN ACTUAL_AMOUNT>0 THEN SWIFT_CODE ELSE NULL END AS SWIFT_CODE
    , CASE WHEN ACTUAL_AMOUNT>0 THEN ACCOUNT ELSE NULL END AS ACCOUNT
    , CNAME
    , REMARK
    , BASE_AMOUNT AS BASE_AMOUNT
    , CASE WHEN ACTUAL_AMOUNT > BASE_AMOUNT THEN BASE_AMOUNT ELSE 0 END AS BASE_AMOUNT2
    , CASE WHEN ACTUAL_AMOUNT > BASE_AMOUNT THEN ACTUAL_AMOUNT-BASE_AMOUNT ELSE ACTUAL_AMOUNT END AS ACTUAL_AMOUNT2 
    , ACTUAL_AMOUNT AS ACTUAL_AMOUNT
    , AMOUNT AS TOTAL_AMOUNT
    , TXN_ID
    , A.BATCH_WITHDRAWAL_ID
    , BONUS_TSF
    , END_BALANCE_FLAG
FROM 
(
    SELECT 
          T.CNAME
        , C.CUST_ENAME
        , T.TXN_DATE
        , BATCH_WITHDRAWAL_ID
        , ORDER_NO
        , T.BASE_AMOUNT
        , B.BANK_ENAME
        , B.BRANCH_ENAME
        , B.ACCOUNT
        , SWIFT_CODE
        , B.ACCOUNT_CNAME
        , B.CURRENCY
        , T.REMARK
        , BONUS_TSF
        , T.END_BALANCE_FLAG
        , DECODE(T.CURRENCY, 'USD', AMOUNT_USD, 'RMB', AMOUNT_RMB, 'NTD', AMOUNT_NTD, 'EUR', AMOUNT_EUR, 'AUD', AMOUNT_AUD, 'JPY', AMOUNT_JPY) AMOUNT
        , DECODE(T.CURRENCY, 'USD', ACTUAL_WITHDRAWAL_AMT_USD, 'RMB', ACTUAL_WITHDRAWAL_AMT_RMB, 'NTD', ACTUAL_WITHDRAWAL_AMT_NTD, 'EUR', ACTUAL_WITHDRAWAL_AMT_EUR, 'AUD', ACTUAL_WITHDRAWAL_AMT_AUD, 'JPY', ACTUAL_WITHDRAWAL_AMT_JPY) AS ACTUAL_AMOUNT
        , TXN_ID
    FROM BL_ACCOUNT_TXN_V1 T
            , BL_CUST_BANK B
            , BL_CUST_V1 C
    WHERE T.CUST_ID = B.CUST_ID
        AND C.CUST_ID = B.CUST_ID
        AND T.CURRENCY = B.CURRENCY
        AND T.TXN_TYPE = 'WITHDRAWAL'
        AND TRUNC(TXN_DATE) = TRUNC(:TXN_DATE) "+ strScriptORG + @"
    ORDER BY TXN_DATE DESC
) A

";
                return base.GetList(strSQL, new
                {
                    TXN_DATE = txt_date
                }, CommandType.Text).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }

        }

    }
}
