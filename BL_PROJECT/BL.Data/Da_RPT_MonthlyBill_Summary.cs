﻿using MK.Demo.Model;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MK.Demo.Utility;
using Dapper;

namespace MK.Demo.Data
{
    public class Da_RPT_MonthlyBill_Summary : DataRepository<RPT_MonthlyBill_Summary>
    {
        OracleConnection _Conn;
        public Da_RPT_MonthlyBill_Summary(OracleConnection conn)
         : base(conn)
        { _Conn = conn; }


        public List<RPT_MonthlyBill_Summary> GetRptData(DateTime OrderEndDate,string strORG, out Exception exError)
        {
            exError = null;
            try
            {
                var strYYMM = OrderEndDate.ToString("yyyy/MM");
                var strYYMMDD = DateTime.Parse(OrderEndDate.ToString("yyyy/MM/01")).AddDays(-1).ToString("yyyy/MM/dd");

                var strYYMMDD_thisEnd = DateTime.Parse(OrderEndDate.ToString("yyyy/MM/01")).AddMonths(1).AddDays(-1).ToString("yyyyMMdd HHmmss");
                var strYYMMDD_prevEnd = DateTime.Parse(OrderEndDate.ToString("yyyy/MM/01")).AddDays(-1).ToString("yyyyMMdd HHmmss");

                var strSQL = @"
SELECT 
        O.ORDER_NO,
        CASE WHEN C.CUST_CNAME2 IS NULL THEN C.CUST_CNAME ELSE C.CUST_CNAME||'/'||C.CUST_CNAME2 END CUST_CNAME,
        BL_INTEREST2_PKG.CAL_INTEREST_TIMES(O.ORDER_ID) AS CAL_INTEREST_TIMES,
        BL_REPORT_PKG.MONTHLY_INTEREST(O.ORDER_NO, TO_DATE(:YYMMDD_THIS, 'YYYYMMDD HH24:MI:SS')) AS MONTHLY_INTEREST,
        BL_INTEREST2_PKG.WITHDRAWAL_DATE(O.ORDER_ID, :YYMM_THIS) AS WITHDRAWAL_DATE,
        BL_INTEREST2_PKG.WITHDRAWAL_AMOUNT2(O.ORDER_ID, :YYMM_THIS) AS WITHDRAWAL_AMOUNT2,
        BL_INTEREST2_PKG.WITHDRAWAL_AMOUNT(O.ORDER_ID, :YYMM_THIS) AS WITHDRAWAL_AMOUNT,
        BL_INTEREST2_PKG.MONTHLY_BALANCE(O.ORDER_ID, TO_DATE(:YYMMDD_PREV, 'YYYYMMDD HH24:MI:SS')) AS MONTHLY_BALANCE, 
        BL_INTEREST2_PKG.RETURN_BALANCE(O.ORDER_ID, TO_DATE(:YYMMDD_THIS, 'YYYYMMDD HH24:MI:SS')) AS RETURN_BALANCE,
        MT4_ACCOUNT,O.CURRENCY,O.AMOUNT,FROM_DATE,END_DATE,EXTEND_DATE,YEAR,POSTAL_CODE,ADDR,IB_CODE
  FROM BL_ORDER_V1 O, BL_CUST_V1 C
  WHERE O.CUST_ID = C.CUST_ID
   AND STATUS_CODE = '4' AND NVL(ORDER_END, SYSDATE) >= :ORDER_END
  ";

                if (strORG.ConvertToString() != string.Empty)
                {
                    switch (strORG)
                    {
                        case "TW1":
                            strSQL += @"
    AND O.ORG_CODE IN ('TW1', 'IND')
    AND C.ORG_CODE IN ('TW1', 'IND')
";
                            break;
                        default:
                            strSQL += @"
    AND O.ORG_CODE = '" + strORG + @"'
    AND C.ORG_CODE = '" + strORG + @"'
";
                            break;
                    }
                }
                return base.GetList(strSQL, new
                {
                    ORDER_END = OrderEndDate,
                    YYMM_THIS = strYYMM,
                    YYMMDD_THIS = strYYMMDD_thisEnd,
                    YYMMDD_PREV = strYYMMDD_prevEnd
                }, CommandType.Text).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }



    }
}
