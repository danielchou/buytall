﻿using MK.Demo.Model;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MK.Demo.Utility;
using Dapper;


namespace MK.Demo.Data
{
    public class Da_BL_MSG_DATA : DataRepository<BL_MSG_DATA>
    {
        OracleConnection _Conn;
        public Da_BL_MSG_DATA(OracleConnection conn)
         : base(conn)
        { _Conn = conn; }

        public List<BL_MSG_DATA> GetSystemVersionData(out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
SELECT *
FROM BL_MSG_DATA A
WHERE A.END_DATE >= SYSDATE
    AND A.START_DATE <= SYSDATE
ORDER BY A.START_DATE DESC
";


                return base.GetList(strSQL, null, CommandType.Text).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }

    }
}
