﻿using MK.Demo.Model;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MK.Demo.Utility;
using Dapper;


namespace MK.Demo.Data
{
    public class Da_BL_BILL_MAIL_LOG : DataRepository<BL_BILL_MAIL_LOG>
    {
        OracleConnection _Conn;
        public Da_BL_BILL_MAIL_LOG(OracleConnection conn)
         : base(conn)
        { _Conn = conn; }


        public bool WriteMailLog(List<BL_BILL_MAIL_LOG> data, string strJobID, string strUserName, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = string.Empty;
                var param = new DynamicParameters();
                int i = 0;

                data.ForEach(x =>
                {
                    strSQL += @"
INSERT INTO BL_BILL_MAIL_LOG
(
  JOB_ID
  , CUST_ID
  , PREV_DATE
  , THIS_DATE
  , TXN_DATE
  , PDF_LOCATION
  , JOB_STATUS
  , LAST_UPDATE_DATE
  , LAST_UPDATED_BY
  , EMAIL
  , IS_TEST
)
SELECT            
    :JOB_ID
  , :CUST_ID" + i.ToString() + @"
  , :PREV_DATE" + i.ToString() + @"
  , :THIS_DATE" + i.ToString() + @"
  , :TXN_DATE" + i.ToString() + @"
  , :PDF_LOCATION" + i.ToString() + @"
  , '1'
  , SYSDATE
  , :USER_NAME
  , :EMAIL" + i.ToString() + @"
  , :IS_TEST" + i.ToString() + @"
FROM DUAL;
";
                    param.Add("CUST_ID" + i.ToString(), x.CUST_ID);
                    param.Add("PREV_DATE" + i.ToString(), x.PREV_DATE);
                    param.Add("THIS_DATE" + i.ToString(), x.THIS_DATE);
                    param.Add("TXN_DATE" + i.ToString(), x.TXN_DATE);
                    param.Add("PDF_LOCATION" + i.ToString(), x.PDF_LOCATION);
                    param.Add("EMAIL" + i.ToString(), x.EMAIL);
                    param.Add("IS_TEST" + i.ToString(), x.IS_TEST);
                    i += 1;
                });

                if (strSQL != null)
                {
                    param.Add("USER_NAME", strUserName);
                    param.Add("JOB_ID", strJobID);
                    strSQL = "Begin " + strSQL + " End; ";
                    this.Connection.Execute(strSQL, param);
                }

                return true;
            }
            catch (Exception ex)
            {
                exError = ex;
                return false;
            }
        }

        public List<BL_BILL_MAIL_LOG> GetAllBatchNoByUser(string strUserName, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
SELECT 
    DISTINCT JOB_ID
FROM BL_BILL_MAIL_LOG A
WHERE LAST_UPDATED_BY = :USER_NAME
ORDER BY 1 DESC
";
                return base.GetList(strSQL, new
                {
                    USER_NAME = strUserName
                }, CommandType.Text).ToList();

            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }

        public List<BL_BILL_MAIL_LOG> GetDataByBatchNo(string strBatchNo, out Exception exError)
        {
            //SELECT 
            //      A.*
            //    , B.MAIL_TYPE_END
            //    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(B.CUST_CNAME) AS CUST_NAME
            //    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(B.CUST_CNAME2) AS CUST_NAME2
            //    , D.IB_CODE
            //    , D.CNAME AS SALES_NAME
            //FROM BL_BILL_MAIL_LOG A
            //    INNER JOIN BL_CUST B
            //        ON A.CUST_ID = B.CUST_ID
            //    LEFT JOIN BL_CUST_SALES C
            //        ON A.CUST_ID = C.CUST_ID
            //    LEFT JOIN BL_EMP D
            //        ON C.IB_CODE = D.IB_CODE
            //WHERE A.JOB_ID = :BATCH_NO
            //ORDER BY A.CUST_ID
            exError = null;
            try
            {
                var strSQL = @"
    SELECT 
                  A.*
                , B.MAIL_TYPE_END
                , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(B.CUST_CNAME) AS CUST_NAME
                , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(B.CUST_CNAME2) AS CUST_NAME2
                , D.IB_CODE
                , D.CNAME AS SALES_NAME
            FROM BL_BILL_MAIL_LOG A
                INNER JOIN BL_CUST B
                    ON A.CUST_ID = B.CUST_ID 
                LEFT JOIN BL_CUST_SALES C
                    ON A.CUST_ID = C.CUST_ID
                LEFT JOIN BL_EMP D
                    ON C.IB_CODE = D.IB_CODE
            WHERE A.JOB_ID = :BATCH_NO  AND (c.ACTIVE IS NULL OR c.ACTIVE='Y')
            ORDER BY A.CUST_ID

";
                return base.GetList(strSQL, new
                {
                    BATCH_NO = strBatchNo
                }, CommandType.Text).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }

        public bool UpdateSendFlag(List<BL_BILL_MAIL_LOG> data, string strUserName, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = string.Empty;
                var param = new DynamicParameters();
                int i = 0;

                data.ForEach(x =>
                {
                    strSQL += @"
UPDATE BL_BILL_MAIL_LOG
SET
      JOB_STATUS = '4'
    , JOB_DATE = SYSDATE
WHERE JOB_ID = :JOB_ID" + i.ToString() + @"
    AND CUST_ID = :CUST_ID" + i.ToString() + @" ;
";
                    param.Add("JOB_ID" + i.ToString(), x.JOB_ID);
                    param.Add("CUST_ID" + i.ToString(), x.CUST_ID);
                    i += 1;
                });

                if (strSQL != string.Empty)
                {
                    strSQL = "Begin " + strSQL + " End;";
                    this.Connection.Execute(strSQL, param);
                }
                return true;
            }
            catch (Exception ex)
            {
                exError = ex;
                return false;
            }
        }

        public List<BL_BILL_MAIL_LOG> GetSalesInfoByBatchNo(List<string> liBatchNo, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
SELECT 
      A.CUST_ID
    , C.IB_CODE
    , C.CNAME AS SALES_NAME
FROM BL_BILL_MAIL_LOG A
    INNER JOIN BL_CUST_SALES B
        ON A.CUST_ID = B.CUST_ID
    INNER JOIN BL_EMP C
        ON B.IB_CODE = C.IB_CODE
WHERE A.JOB_ID IN :BATCH_NO
";
                return base.GetList(strSQL, new
                {
                    BATCH_NO = liBatchNo
                }, CommandType.Text).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }

    }
}
