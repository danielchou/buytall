﻿using MK.Demo.Model;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MK.Demo.Utility;
using Dapper;

namespace MK.Demo.Data
{
    public class Da_BL_ORDER_Expiry : DataRepository<BL_ORDER_Expiry>
    {
        public Da_BL_ORDER_Expiry(OracleConnection conn)
         : base(conn)
        { }

        public List<BL_ORDER_Expiry> GetData(DateTime dt,string strORG, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"

SELECT 
      CASE 
        WHEN O.ORDER_NO LIKE '%-%' THEN 'NEW' 
        ELSE 
            CASE
                WHEN TO_CHAR(EXTEND_DATE,'YYYYMM') = :YYYYMM THEN 'OLD_END'
                ELSE 'OLD' 
            END
        END ORDER_TYPE
    , O.ORDER_NO
    , CASE WHEN O.CUST_CNAME2 IS NOT NULL THEN O.CUST_CNAME||'/'||O.CUST_CNAME2 ELSE O.CUST_CNAME END CNAME
    , CASE WHEN O.CUST_ENAME2 IS NOT NULL THEN O.CUST_ENAME||'/'||O.CUST_ENAME2 ELSE O.CUST_ENAME END ENAME
    , O.ATTRIBUTE1 SALES
    , O.IB_CODE
    , O.FROM_DATE
    , O.END_DATE
    , O.EXTEND_DATE
    , O.ORDER_END
    , NVL(O.ORDER_END,NVL(O.EXTEND_DATE,O.END_DATE)) NOTICE_DATE /*通知日期*/
    , O.INTEREST_RATE
    , O.CUST_ID
    , O.YEAR
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(C.PHONE_1) AS PHONE
    , C.POSTAL_CODE AS POSTAL_CODE
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(C.C_ADDRESS) AS C_ADDRESS
    , :YYYYMM AS ORDEREND_MONTH

    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(C.CUST_ENAME) AS CUST_ENAME
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(C.CUST_ENAME2) AS CUST_ENAME2
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(C.CUST_CNAME) AS CUST_CNAME
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(C.CUST_CNAME2) AS CUST_CNAME2
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(C.EMAIL_1) AS EMAIL_1
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(C.EMAIL_2) AS EMAIL_2
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(C.ID_NUMBER) AS ID_NUMBER
    , C.UNION_ID_NUMBER

FROM BL_ORDER_V1 O
    INNER JOIN BL_CUST C
        ON O.CUST_ID = C.CUST_ID
WHERE NVL(TO_CHAR(ORDER_END,'YYYYMM'),NVL(TO_CHAR(EXTEND_DATE,'YYYYMM'),TO_CHAR(END_DATE,'YYYYMM'))) = :YYYYMM
";


                if (strORG.ConvertToString() != string.Empty)
                {
                    switch (strORG)
                    {
                        case "TW1":
                            strSQL += @"
    AND O.ORG_CODE IN ('TW1', 'IND')
    AND C.ORG_CODE IN ('TW1', 'IND')
";
                            break;
                        default:
                            strSQL += @"
    AND O.ORG_CODE = '" + strORG + @"'
    AND C.ORG_CODE = '" + strORG + @"'
";
                            break;
                    }
                }


                strSQL += @"
ORDER BY END_DATE
";
                return base.GetList(strSQL, new
                {
                    YYYYMM = dt.ToString("yyyyMM")
                }, CommandType.Text).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }

    }
}
