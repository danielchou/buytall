﻿using MK.Demo.Model;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MK.Demo.Utility;
using Dapper;


namespace MK.Demo.Data
{
    public class Da_BL_PROCESS_LOG : DataRepository<BL_PROCESS_LOG>
    {
        OracleConnection _Conn;
        public Da_BL_PROCESS_LOG(OracleConnection conn)
         : base(conn)
        { _Conn = conn; }


        public BL_PROCESS_LOG GetLastInfo(out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
SELECT *
FROM BL_PROCESS_LOG A
WHERE A.BL_PROCESS_LOG_S1 = 
    ( SELECT MAX(BL_PROCESS_LOG_S1) FROM BL_PROCESS_LOG X )
";
                return base.GetSingleOrDefault(strSQL, null, CommandType.Text);
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }


    }
}
