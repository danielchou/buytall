﻿using MK.Demo.Model;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MK.Demo.Utility;
using Dapper;

namespace MK.Demo.Data
{
    public class Da_BL_RPT_MONTHLY_BILL : DataRepository<BL_RPT_MONTHLY_BILL>
    {
        OracleConnection _Conn;
        public Da_BL_RPT_MONTHLY_BILL(OracleConnection conn)
         : base(conn)
        { _Conn = conn; }


        public List<BL_RPT_MONTHLY_BILL> GetDataByBatchReportSN(long ReportSN, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
SELECT *
FROM BL_RPT_MONTHLY_BILL A
WHERE A.BL_RPT_MONTHLY_BILL_SN = :BL_RPT_MONTHLY_BILL_SN
";
                return base.GetList(strSQL, new
                {
                    BL_RPT_MONTHLY_BILL_SN = ReportSN
                }, CommandType.Text).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }

        public bool InsertReportData(long BL_RPT_MONTHLY_BILL_SN, DateTime strRepoetEndDate, List<long> liCustID, out Exception exError)
        {
            exError = null;
            try
            {
                var dateS = DateTime.Parse(strRepoetEndDate.ToString("yyyy/MM/01"));
                var dateE = DateTime.Parse(strRepoetEndDate.ToString("yyyy/MM/01")).AddMonths(1);

                var strYYMM = strRepoetEndDate.ToString("yyyy/MM");
                var strTXN_DATE = strRepoetEndDate.ToString("yyyy/MM");
                var strYYMMDD = DateTime.Parse(strRepoetEndDate.ToString("yyyy/MM/01")).AddDays(-1).ToString("yyyy/MM/dd");

                
                var strSQL = string.Empty;
                strSQL = @"


INSERT INTO BL_RPT_MONTHLY_BILL

SELECT  
      :BL_RPT_MONTHLY_BILL_SN
    , T.CNAME   --客戶名稱
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(CUST_ENAME)    --客戶名稱(英)
    , E.IB_CODE
    , POSTAL_CODE
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(C_ADDRESS) ADDRESS
    , ORDER_NO  --合約編號
    , FROM_DATE 
    , END_DATE
    , EXTEND_DATE
    , ORDER_END
    , CURRENCY
    , BASE_AMOUNT   --本金
    , BL_INTEREST2_PKG.RETURN_BALANCE(T.ORDER_ID, TO_DATE(:YYYYMMDD, 'YYYY/MM/DD')) - BASE_AMOUNT --上月累計餘額
    , BL_INTEREST2_PKG.WITHDRAWAL_AMOUNT(T.ORDER_ID, :YYYYMM)  --紅利提領
    , BL_INTEREST2_PKG.WITHDRAWAL_DATE(T.ORDER_ID, :YYYYMM)    --紅利日期
    , MAX(INTEREST_TIMES)   --配息次數
    , SUM(DECODE(FLOW_TYPE, 'IN', AMOUNT_USD, 'OUT', -AMOUNT_USD)) AMOUNT_USD
    , SUM(DECODE(FLOW_TYPE, 'IN', AMOUNT_NTD, 'OUT', -AMOUNT_NTD)) AMOUNT_NTD
    , SUM(DECODE(FLOW_TYPE, 'IN', AMOUNT_RMB, 'OUT', -AMOUNT_RMB)) AMOUNT_RMB

FROM BL_ACCOUNT_TXN_V1 T
    INNER JOIN BL_CUST C
        ON C.CUST_ID = T.CUST_ID
    LEFT JOIN BL_CUST_SALES D
        ON C.CUST_ID = D.CUST_ID
            AND (D.ACTIVE IS NULL OR D.ACTIVE = 'Y' )   --AND D.ACTIVE <> 'N'
    LEFT JOIN BL_EMP E
        ON D.IB_CODE = E.IB_CODE
WHERE 1 = 1
    AND C.CUST_ID IN :CUST_ID
    AND TXN_TYPE <> 'HISTORY'
    AND :TXN_DATE BETWEEN 
        TO_CHAR(FROM_DATE,'YYYY/MM') 
        AND NVL(TO_CHAR(EXTEND_DATE,'YYYY/MM'),TO_CHAR(END_DATE,'YYYY/MM') ) 
GROUP BY 
      T.CNAME
    , ORDER_NO
    , FROM_DATE 
    , END_DATE
    , EXTEND_DATE
    , ORDER_END
    , CURRENCY
    , BASE_AMOUNT
    , BL_CRYPT_PKG.DECRYPT_DES_NOKEY(CUST_ENAME)
    , POSTAL_CODE,BL_CRYPT_PKG.DECRYPT_DES_NOKEY(C.C_ADDRESS) 
    , BL_INTEREST2_PKG.RETURN_BALANCE(T.ORDER_ID, TO_DATE(:YYYYMMDD, 'YYYY/MM/DD')) - BASE_AMOUNT 
    , BL_INTEREST2_PKG.WITHDRAWAL_AMOUNT(T.ORDER_ID, :YYYYMM) 
    , BL_INTEREST2_PKG.WITHDRAWAL_DATE(T.ORDER_ID, :YYYYMM)
    , E.IB_CODE

ORDER BY T.CNAME, FROM_DATE ASC, ORDER_NO DESC
";

                base.ExecuteScalar(strSQL, new
                {
                    BL_RPT_MONTHLY_BILL_SN = BL_RPT_MONTHLY_BILL_SN,
                    CUST_ID = liCustID,
                    YYYYMMDD = strYYMMDD,
                    YYYYMM = strYYMM,
                    TXN_DATE = strTXN_DATE
                }, CommandType.Text);

                return true;
            }
            catch (Exception ex)
            {
                exError = ex;
                return false;
            }
        }


        public List<BL_RPT_MONTHLY_BILL> GetReportData(DateTime strRepoetEndDate, List<long> liCustID, out Exception exError)
        {
            exError = null;
            try
            {
                var strTXN_DATE = strRepoetEndDate.ToString("yyyy/MM");
                var strYYMMDD_thisEnd = DateTime.Parse(strRepoetEndDate.ToString("yyyy/MM/01")).AddMonths(1).AddDays(-1).ToString("yyyy/MM/dd");
                var strYYMMDD_prevEnd = DateTime.Parse(strRepoetEndDate.ToString("yyyy/MM/01")).AddDays(-1).ToString("yyyy/MM/dd");
                //    --  , BL_INTEREST2_PKG.WITHDRAWAL_AMOUNT(T.ORDER_ID, :TXN_DATE) AS INTEREST_OUT  --紅利提領
                var strSQL = @"
SELECT    
                      decode(T.CUST_CNAME2,null,T.CUST_CNAME,  T.CUST_CNAME ||' / ' || T.CUST_CNAME2) AS  CUST_CNAME--客戶名稱           --20191021
                    , decode(T.CUST_ENAME2,null,T.CUST_ENAME,  T.CUST_ENAME ||' / ' || T.CUST_ENAME2) AS  CUST_ENAME --客戶名稱(英)      --20191021
                    , T.IB_CODE
                    , C.POSTAL_CODE
                    , C.ADDR AS ADDRESS
                    , ORDER_NO --合約編號
                    , ORDER_NO2
                    , FROM_DATE 
                    , END_DATE
                    , EXTEND_DATE
                    , ORDER_END
                    , CURRENCY
                    , AMOUNT --本金
                    , BL_INTEREST2_PKG.MONTHLY_BALANCE(T.ORDER_ID, TO_DATE(:YYMMDD_PREV, 'YYYY/MM/DD')) --20191021 marked   +BL_INTEREST2_PKG.INITIAL_INTEREST_IN_MONTH(T.ORDER_ID , :X_MONTH /* YYYY/MM */  )  
                                                                                                    AS PRE_MONTH_BALANCE --上月累計餘額
                    , decode(BL_INTEREST2_PKG.WITHDRAWAL_AMOUNT(T.ORDER_ID, :TXN_DATE),0,'-',BL_INTEREST2_PKG.WITHDRAWAL_AMOUNT(T.ORDER_ID, :TXN_DATE)) AS INTEREST_OUT  --紅利提領
                    , BL_INTEREST2_PKG.WITHDRAWAL_DATE(T.ORDER_ID, :TXN_DATE) AS INTEREST_DATE   --紅利日期
                    , BL_INTEREST2_PKG.INTEREST_TIMES(T.ORDER_ID, TO_DATE(:YYMMDD_THIS, 'YYYY/MM/DD')) AS INTEREST_TIMES --配息次數
                    , BL_INTEREST2_PKG.ORDER_MONTHLY_INTEREST(T.ORDER_ID, TO_DATE(:YYMMDD_THIS, 'YYYY/MM/DD')) AS NEW_INT --紅利        
                    , BL_INTEREST2_PKG.RETURN_BALANCE(T.ORDER_ID, TO_DATE(:YYMMDD_THIS, 'YYYY/MM/DD')) AS END_BALANCE --小結  --, BL_REPORT_PKG.REPORT_NOTE(T.ORDER_ID, :TXN_DATE) 
                    , BL_REPORT_PKG.REPORT_NOTE(T.ORDER_ID,substr(:TXN_DATE, 1, 4)||substr(:TXN_DATE, 6, 2)) AS SPECIAL_MEMO --20191209 舊轉新memo
                FROM BL_ORDER_V1 T
                        , BL_CUST_V2 C
                WHERE C.CUST_ID = T.CUST_ID
                  AND C.CUST_ID IN (SELECT CUST_ID
                                      FROM BL_ORDER_V1
                                     WHERE NVL (TO_CHAR(ORDER_END,'YYYYMM'), TO_CHAR(SYSDATE,'YYYY/MM')) >= :TXN_DATE
                                   )
                    AND T.STATUS_CODE IN ('1','3','4')
                    AND C.CUST_ID IN :CUST_ID   
                    --當月到期的合約不SHOW在對帳單
                    AND :TXN_DATE BETWEEN TO_CHAR(FROM_DATE,'YYYY/MM') AND NVL(TO_CHAR(ADD_MONTHS(ORDER_END,-1), 'YYYY/MM'),NVL(TO_CHAR(ADD_MONTHS(EXTEND_DATE,-1), 'YYYY/MM'), TO_CHAR(ADD_MONTHS(END_DATE,-1), 'YYYY/MM') ))
UNION
     SELECT    
                      decode(T.CUST_CNAME2,null,T.CUST_CNAME,  T.CUST_CNAME ||' / ' || T.CUST_CNAME2) AS  CUST_CNAME--客戶名稱          --20191021 
                    , decode(T.CUST_ENAME2,null,T.CUST_ENAME,  T.CUST_ENAME ||' / ' || T.CUST_ENAME2) AS  CUST_ENAME --客戶名稱(英)    --20191021
                    , T.IB_CODE
                    , C.POSTAL_CODE
                    , C.ADDR AS ADDRESS
                    , ORDER_NO --合約編號
                    , ORDER_NO2
                    , FROM_DATE 
                    , END_DATE
                    , EXTEND_DATE
                    , ORDER_END
                    , CURRENCY
                    , AMOUNT --本金
                    , BL_INTEREST2_PKG.MONTHLY_BALANCE(T.ORDER_ID, TO_DATE(:YYMMDD_PREV, 'YYYY/MM/DD')) --20191021 marked +BL_INTEREST2_PKG.INITIAL_INTEREST_IN_MONTH(T.ORDER_ID , :X_MONTH /* YYYY/MM */  )  
                                                                                                                                                        AS PRE_MONTH_BALANCE --上月累計餘額
                    , decode(BL_INTEREST2_PKG.WITHDRAWAL_AMOUNT(T.ORDER_ID, :TXN_DATE),0,'-',BL_INTEREST2_PKG.WITHDRAWAL_AMOUNT(T.ORDER_ID, :TXN_DATE)) AS INTEREST_OUT  --紅利提領
                    , BL_INTEREST2_PKG.WITHDRAWAL_DATE(T.ORDER_ID, :TXN_DATE) AS INTEREST_DATE   --紅利日期
                    , BL_INTEREST2_PKG.INTEREST_TIMES(T.ORDER_ID, TO_DATE(:YYMMDD_THIS, 'YYYY/MM/DD')) AS INTEREST_TIMES --配息次數
                    , BL_INTEREST2_PKG.ORDER_MONTHLY_INTEREST(T.ORDER_ID, TO_DATE(:YYMMDD_THIS, 'YYYY/MM/DD')) AS NEW_INT --紅利        
                    , BL_INTEREST2_PKG.RETURN_BALANCE(T.ORDER_ID, TO_DATE(:YYMMDD_THIS, 'YYYY/MM/DD')) AS END_BALANCE --小結
                    , '' AS SPECIAL_MEMO --20191209 舊轉新memo
                FROM BL_ORDER_V1 T
                        , BL_CUST_V2 C
                WHERE C.CUST_ID = T.CUST_ID
                  AND C.CUST_ID IN (SELECT CUST_ID
                                      FROM BL_ORDER_V1
                                     WHERE TRUNC(ORDER_END)=LAST_DAY(TO_DATE(:TXN_DATE,'YYYY/MM'))
                                   )
                  AND T.ORDER_ID IN (SELECT ORDER_ID
                                      FROM BL_ORDER_V1
                                     WHERE TRUNC(ORDER_END)=LAST_DAY(TO_DATE(:TXN_DATE,'YYYY/MM'))
                                   )  
                    AND T.STATUS_CODE IN ('1','3','4')
                    AND C.CUST_ID IN :CUST_ID
                    --當月最後一天到期合約且有轉新約,要呈現出來
                    AND EXISTS (SELECT 1 FROM  BL_ORDER_V1 WHERE PARENT_ORDER_NO=T.ORDER_NO)  
                    order by FROM_DATE
                ";
                //  --    AND :TXN_DATE BETWEEN TO_CHAR(FROM_DATE,'YYYY/MM') AND NVL(TO_CHAR(ADD_MONTHS(EXTEND_DATE,-1), 'YYYY/MM'), TO_CHAR(ADD_MONTHS(END_DATE,-1), 'YYYY/MM') ) 
                return base.GetList(strSQL, new
                {
                    CUST_ID = liCustID,
                    YYMMDD_PREV = strYYMMDD_prevEnd,
                    YYMMDD_THIS = strYYMMDD_thisEnd,
                    TXN_DATE = strTXN_DATE
                }, CommandType.Text).ToList();

            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }

    }
}
