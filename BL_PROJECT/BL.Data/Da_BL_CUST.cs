﻿using MK.Demo.Model;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MK.Demo.Utility;
using Dapper;

namespace MK.Demo.Data
{
    public class Da_BL_CUST : DataRepository<BL_CUST>
    {
        public Da_BL_CUST(OracleConnection conn)
         : base(conn)
        { }



        public List<BL_CUST> GetData(string strCustID, QueryCust _Condition,string strORG, out Exception exError)
        {
            exError = null;
            try
            {
                if (_Condition == null)
                {
                    _Condition = new QueryCust();
                }
                var strSQL = @"
SELECT 
     A.CUST_ID,
  BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.CUST_ENAME) CUST_ENAME,
   BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.CUST_ENAME2) AS CUST_ENAME2,
  BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.CUST_CNAME) AS CUST_CNAME,
   BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.CUST_CNAME2) AS CUST_CNAME2,
   A.PASSPORT,
   A.PASSPORT2,
   A.PASSPORT_REGION,
   A.PASSPORT_REGION2,
  A.PASSPORT_ACTIVE_END_DATE,
  A.PASSPORT_ACTIVE_END_DATE2,
   BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.ID_NUMBER) as ID_NUMBER,
 BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.ID_NUMBER2) as ID_NUMBER2,
  A.SEX,
  A.SEX2,
  A.DATE_OF_BIRTH,
  A.DATE_OF_BIRTH2,
  BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.EMAIL_1) as EMAIL_1,
  BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.EMAIL_2) as EMAIL_2,
  BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.EMAIL2_1) as EMAIL2_1 ,
  BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.EMAIL2_2) as EMAIL2_2,
  BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.PHONE_1) as PHONE_1,
  BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.PHONE_2) as PHONE_2,
  BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.PHONE2_1) as PHONE2_1,
  BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.PHONE2_2) as PHONE2_2,
  A.POSTAL_CODE,
  A.POSTAL_CODE2,
  BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.C_ADDRESS) as C_ADDRESS,
  BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.C_ADDRESS2) as C_ADDRESS2,
  BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.E_ADDRESS) as E_ADDRESS,
   BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.E_ADDRESS2) as E_ADDRESS2,
  A.MAIL_TYPE_MONTHLY,
  A.MAIL_TYPE_END,
  A.CREATION_DATE,
  A.CREATED_BY,
  A.LAST_UPDATE_DATE,
  A.LAST_UPDATED_BY,
  A.CUST_CNAME_RAW,
  A.CUST_CNAME2_RAW,
  A.MT4_ACCOUNT,
    NVL(B.ORDER_CNT, 0) AS ORDER_CNT
    , D.CNAME AS SALES_NAME
    , A.WEB_PASSWORD
    , A.IS_ONLINE_WITHDRAWAL
    , A.ORG_CODE

    , A.VERIFICATIONCODE
    , A.WELCOME_FLAG
    , A.IS_ONLINE_WITHDRAWAL_CHECK
    , A.IS_ONLINE_MAIL_GUID
    , A.VERIFICATIONCODE_E
    , A.VERIFICATIONCODE_P
FROM BL_CUST A
    LEFT JOIN
    (
        SELECT X.CUST_ID , COUNT(1) AS ORDER_CNT
        FROM BL_ORDER X
        WHERE X.STATUS_CODE = '4'
        GROUP BY X.CUST_ID
    )B
        ON A.CUST_ID = B.CUST_ID
    LEFT JOIN BL_CUST_SALES C
        ON A.CUST_ID = C.CUST_ID
            AND (C.ACTIVE IS NULL OR C.ACTIVE = 'Y' )   --AND C.ACTIVE <> 'N'
    LEFT JOIN BL_EMP D
        ON C.IB_CODE = D.IB_CODE
WHERE   1 = 1   AND   rownum <= 600
    --and a.CUST_ID IN (SELECT CUST_ID FROM BL_ORDER_V1 WHERE NVL(ORDER_END,SYSDATE)>=SYSDATE) 
    AND (A.CUST_ID = :CUST_ID OR :CUST_ID IS NULL)
    AND 
    (
        BL_CRYPT_PKG.DECRYPT_DES_NOKEY (A.CUST_ENAME) LIKE '%'||:CUST_NAME||'%' 
        OR BL_CRYPT_PKG.DECRYPT_DES_NOKEY (A.CUST_CNAME) LIKE '%'||:CUST_NAME||'%'
        OR BL_CRYPT_PKG.DECRYPT_DES_NOKEY (A.CUST_ENAME2) LIKE '%'||:CUST_NAME||'%' 
        OR BL_CRYPT_PKG.DECRYPT_DES_NOKEY (A.CUST_CNAME2) LIKE '%'||:CUST_NAME||'%'
        OR :CUST_NAME IS NULL
    )
    AND ( UPPER(BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.ID_NUMBER)) LIKE '%'||:ID_NUMBER||'%' OR :ID_NUMBER IS NULL )
    AND ( UPPER(BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.ID_NUMBER2)) LIKE '%'||:ID_NUMBER2||'%' OR :ID_NUMBER2 IS NULL )
    AND ( A.PASSPORT LIKE '%'||:PASSPORT||'%' OR :PASSPORT IS NULL )
    AND ( D.CNAME LIKE '%'||:SALES_NAME||'%' OR :SALES_NAME IS NULL )
";
                if (_Condition.CONTRACT_COUNT != null && _Condition.CONTRACT_COUNT_CONDITION.ConvertToString() != string.Empty)
                {
                    strSQL += @"
    AND NVL(B.ORDER_CNT, 0) " + _Condition.CONTRACT_COUNT_CONDITION.ConvertToString() + " " + _Condition.CONTRACT_COUNT.Value.ConvertToString();
                }

                if (_Condition.IschkUnSendMail.ConvertToString() == "Y")
                {
                    strSQL += @"
    AND A.WEB_PASSWORD IS NULL
";
                }
                if (_Condition.IsIncOrderEnd.ConvertToString() == "Y")
                {
                    //  不排除有Order End的條件
                }
                else
                {
                    strSQL += @"
    and a.CUST_ID IN (SELECT CUST_ID FROM BL_ORDER_V1 WHERE NVL(ORDER_END,SYSDATE)>=SYSDATE) 
";
                }

                if (strORG.ConvertToString() != string.Empty)
                {
                    switch (strORG)
                    {
                        case "TW1":
                            strSQL += @"
    AND A.ORG_CODE IN ('TW1', 'IND')
";
                            break;
                        default:
                            strSQL += @"
    AND A.ORG_CODE = '" + strORG + @"'
";
                            break;
                    }
                }

                return base.GetList(strSQL, new
                {
                    CUST_ID = strCustID,
                    CUST_NAME = _Condition.CUST_NAME.ConvertToString(),
                    ID_NUMBER = _Condition.ID_NUMBER.ConvertToString(),
                    ID_NUMBER2 = _Condition.ID_NUMBER2.ConvertToString(),
                    PASSPORT = _Condition.PASSPORT.ConvertToString(),
                    SALES_NAME = _Condition.SALES.ConvertToString()

                }, CommandType.Text).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }



        public List<BL_CUST> GetData(List<string> CustID, out Exception exError)
        {
            exError = null;
            try
            {
                
                var strSQL = @"
SELECT 
    A.CUST_ID,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.CUST_ENAME) CUST_ENAME,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.CUST_ENAME2) AS CUST_ENAME2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.CUST_CNAME) AS CUST_CNAME,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.CUST_CNAME2) AS CUST_CNAME2,
    A.PASSPORT,
    A.PASSPORT2,
    A.PASSPORT_REGION,
    A.PASSPORT_REGION2,
    A.PASSPORT_ACTIVE_END_DATE,
    A.PASSPORT_ACTIVE_END_DATE2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.ID_NUMBER) as ID_NUMBER,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.ID_NUMBER2) as ID_NUMBER2,
    A.SEX,
    A.SEX2,
    A.DATE_OF_BIRTH,
    A.DATE_OF_BIRTH2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.EMAIL_1) as EMAIL_1,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.EMAIL_2) as EMAIL_2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.EMAIL2_1) as EMAIL2_1 ,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.EMAIL2_2) as EMAIL2_2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.PHONE_1) as PHONE_1,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.PHONE_2) as PHONE_2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.PHONE2_1) as PHONE2_1,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.PHONE2_2) as PHONE2_2,
    A.POSTAL_CODE,
    A.POSTAL_CODE2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.C_ADDRESS) as C_ADDRESS,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.C_ADDRESS2) as C_ADDRESS2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.E_ADDRESS) as E_ADDRESS,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.E_ADDRESS2) as E_ADDRESS2,
    A.MAIL_TYPE_MONTHLY,
    A.MAIL_TYPE_END,
    A.CREATION_DATE,
    A.CREATED_BY,
    A.LAST_UPDATE_DATE,
    A.LAST_UPDATED_BY,
    A.CUST_CNAME_RAW,
    A.CUST_CNAME2_RAW,
    A.MT4_ACCOUNT,
    NVL(B.ORDER_CNT, 0) AS ORDER_CNT
    , D.CNAME AS SALES_NAME
    , A.WEB_PASSWORD
    , A.UNION_ID_NUMBER
    , A.ORG_CODE
FROM BL_CUST A
    LEFT JOIN
    (
        SELECT X.CUST_ID , COUNT(1) AS ORDER_CNT
        FROM BL_ORDER X
        WHERE X.STATUS_CODE = '4'
        GROUP BY X.CUST_ID
    )B
        ON A.CUST_ID = B.CUST_ID
    LEFT JOIN BL_CUST_SALES C
        ON A.CUST_ID = C.CUST_ID
            AND (C.ACTIVE IS NULL OR C.ACTIVE = 'Y' )   --AND C.ACTIVE <> 'N'
    LEFT JOIN BL_EMP D
        ON C.IB_CODE = D.IB_CODE
WHERE 1 = 1
    AND A.CUST_ID IN :CUST_ID

";
              
                return base.GetList(strSQL, new
                {
                    CUST_ID = CustID
                }, CommandType.Text).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }
        
        /// <summary>
        /// 取得可線上申請出金的資料
        /// </summary>
        /// <param name="exError"></param>
        /// <returns></returns>
        public List<BL_CUST> GetData_CanOnlineDeposit(out Exception exError)
        {
            exError = null;
            try
            {

                var strSQL = @"
SELECT 
    A.CUST_ID,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.CUST_ENAME) CUST_ENAME,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.CUST_ENAME2) AS CUST_ENAME2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.CUST_CNAME) AS CUST_CNAME,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.CUST_CNAME2) AS CUST_CNAME2,
    A.PASSPORT,
    A.PASSPORT2,
    A.PASSPORT_REGION,
    A.PASSPORT_REGION2,
    A.PASSPORT_ACTIVE_END_DATE,
    A.PASSPORT_ACTIVE_END_DATE2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.ID_NUMBER) as ID_NUMBER,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.ID_NUMBER2) as ID_NUMBER2,
    A.SEX,
    A.SEX2,
    A.DATE_OF_BIRTH,
    A.DATE_OF_BIRTH2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.EMAIL_1) as EMAIL_1,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.EMAIL_2) as EMAIL_2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.EMAIL2_1) as EMAIL2_1 ,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.EMAIL2_2) as EMAIL2_2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.PHONE_1) as PHONE_1,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.PHONE_2) as PHONE_2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.PHONE2_1) as PHONE2_1,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.PHONE2_2) as PHONE2_2,
    A.POSTAL_CODE,
    A.POSTAL_CODE2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.C_ADDRESS) as C_ADDRESS,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.C_ADDRESS2) as C_ADDRESS2,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.E_ADDRESS) as E_ADDRESS,
    BL_CRYPT_PKG.DECRYPT_DES_NOKEY(A.E_ADDRESS2) as E_ADDRESS2,
    A.MAIL_TYPE_MONTHLY,
    A.MAIL_TYPE_END,
    A.CREATION_DATE,
    A.CREATED_BY,
    A.LAST_UPDATE_DATE,
    A.LAST_UPDATED_BY,
    A.CUST_CNAME_RAW,
    A.CUST_CNAME2_RAW,
    A.MT4_ACCOUNT,
    NVL(B.ORDER_CNT, 0) AS ORDER_CNT
    , D.CNAME AS SALES_NAME
    , D.EMAIL AS SALES_EMAIL
    , A.WEB_PASSWORD
    , A.UNION_ID_NUMBER
    , A.ORG_CODE

    , A.IS_ONLINE_WITHDRAWAL
    , A.VERIFICATIONCODE
    , A.WELCOME_FLAG
    , A.IS_ONLINE_WITHDRAWAL_CHECK
    , A.IS_ONLINE_MAIL_GUID
    , A.VERIFICATIONCODE_E
    , A.VERIFICATIONCODE_P
FROM BL_CUST A
    LEFT JOIN
    (
        SELECT X.CUST_ID , COUNT(1) AS ORDER_CNT
        FROM BL_ORDER X
        WHERE X.STATUS_CODE = '4'
        GROUP BY X.CUST_ID
    )B
        ON A.CUST_ID = B.CUST_ID
    LEFT JOIN BL_CUST_SALES C
        ON A.CUST_ID = C.CUST_ID
            AND (C.ACTIVE IS NULL OR C.ACTIVE = 'Y' )   --AND C.ACTIVE <> 'N'
    LEFT JOIN BL_EMP D
        ON C.IB_CODE = D.IB_CODE
WHERE 1 = 1
    AND A.IS_ONLINE_WITHDRAWAL = 'Y'

";

                return base.GetList(strSQL, null, CommandType.Text).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }


        public List<BL_CUST> GetDataByID_Number(string strID_Number, string strID_Number2, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
SELECT *
FROM BL_CUST
WHERE BL_CRYPT_PKG.DECRYPT_DES_NOKEY(ID_NUMBER) = :ID_NUMBER
    AND 
    ( 
        BL_CRYPT_PKG.DECRYPT_DES_NOKEY(ID_NUMBER2) = :ID_NUMBER2 OR 
        (
            BL_CRYPT_PKG.DECRYPT_DES_NOKEY(ID_NUMBER2) IS NULL AND 
            :ID_NUMBER2 IS NULL
        ) 
    )
";
                return base.GetList(strSQL, new
                {
                    ID_NUMBER = strID_Number,
                    ID_NUMBER2 = strID_Number2
                }, CommandType.Text).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }

        /// <summary>
        /// 更改客戶 新客戶改為舊客戶...
        /// </summary>
        /// <param name="strCustID"></param>
        /// <returns></returns>
        public bool UpdateCustomer_IsOldCustomer(string strCustID, out Exception exError)
        {
            exError = null;

            try
            {
                var strSQL = @"
BEGIN
    update BL_Cust set is_old_customer='Y' where cust_id={0};
END;
";
                strSQL = string.Format(strSQL, strCustID);

                base.ExecuteScalar(strSQL, null, CommandType.Text);
                return true;
            }
            catch (Exception ex)
            {
                exError = ex;
                return false;
            }
        }

        public bool UpdateContractData(BL_CUST dataCust, string strCustID, out Exception exError)
        {
            exError = null;
            try
            {

                var strSQL = @"
BEGIN 

    UPDATE BL_CUST 
    SET
          MAIL_TYPE_MONTHLY = :MAIL_TYPE_MONTHLY
        , MAIL_TYPE_END = :MAIL_TYPE_END
    WHERE CUST_ID = :CUST_ID;
END ;
";
                base.ExecuteScalar(strSQL, new
                {
                    CUST_ID = strCustID,
                    MAIL_TYPE_MONTHLY = dataCust.MAIL_TYPE_MONTHLY,
                    MAIL_TYPE_END = dataCust.MAIL_TYPE_END
                }, CommandType.Text);

                return true;
            }
            catch (Exception ex)
            {
                exError = ex;
                return false;
            }
        }


        public bool UpdateCustData
            (
                string strCustID,
                BL_CUST dataCust,
                BL_CUST_BANK dataBank,
                BL_CUST_BANK dataBank2,
                BL_CUST_BANK dataBank3,
                BL_CUST_BANK dataBank3_2,
                BL_CUST_BANK dataBank4,
                BL_CUST_BANK dataBank5,
                BL_CUST_BANK dataBank6,
                BL_CUST_BANK dataBank7,
                string strUserName,
                out Exception exError
            )
        {
            exError = null;
            try
            {

                var strSQL = @"
BEGIN

    UPDATE BL_CUST
        SET
              PHONE_1 = BL_CRYPT_PKG.ENCRYPT_DES_NOKEY(:PHONE_1)
            , PHONE_2 = BL_CRYPT_PKG.ENCRYPT_DES_NOKEY(:PHONE_2)
            , EMAIL_1 = BL_CRYPT_PKG.ENCRYPT_DES_NOKEY(:EMAIL_1)
            , EMAIL_2 = BL_CRYPT_PKG.ENCRYPT_DES_NOKEY(:EMAIL_2)
            , C_ADDRESS = BL_CRYPT_PKG.ENCRYPT_DES_NOKEY(:C_ADDRESS)
            , C_ADDRESS2 = BL_CRYPT_PKG.ENCRYPT_DES_NOKEY(:C_ADDRESS2)
            , MAIL_TYPE_MONTHLY = :MAIL_TYPE_MONTHLY
            , MAIL_TYPE_END = :MAIL_TYPE_END
            , POSTAL_CODE = :POSTAL_CODE
            , POSTAL_CODE2 = :POSTAL_CODE2

            , LAST_UPDATE_DATE = SYSDATE
            , LAST_UPDATED_BY = :USER_NAME


            , CUST_CNAME = BL_CRYPT_PKG.ENCRYPT_DES_NOKEY(:CUST_CNAME)
            , CUST_ENAME = BL_CRYPT_PKG.ENCRYPT_DES_NOKEY(:CUST_ENAME)
            , CUST_CNAME2 = BL_CRYPT_PKG.ENCRYPT_DES_NOKEY(:CUST_CNAME2)
            , CUST_ENAME2 = BL_CRYPT_PKG.ENCRYPT_DES_NOKEY(:CUST_ENAME2)
            , ID_NUMBER = BL_CRYPT_PKG.ENCRYPT_DES_NOKEY(:ID_NUMBER)
            , ID_NUMBER2 = BL_CRYPT_PKG.ENCRYPT_DES_NOKEY(:ID_NUMBER2)
            , PASSPORT = :PASSPORT
            , PASSPORT2 = :PASSPORT2

            , SEX = :SEX
            , SEX2 = :SEX2
            , DATE_OF_BIRTH = :DATE_OF_BIRTH
            , DATE_OF_BIRTH2 = :DATE_OF_BIRTH2
            , PASSPORT_REGION = :PASSPORT_REGION
            , PASSPORT_REGION2 = :PASSPORT_REGION2
            , IS_ONLINE_WITHDRAWAL = :IS_ONLINE_WITHDRAWAL
   WHERE CUST_ID = :CUST_ID;

    
    UPDATE BL_CUST_BANK
        SET
          BANK_CNAME = :BANK_CNAME
      
        , BANK_ENAME = :BANK_ENAME
        , BRANCH_CNAME = :BRANCH_CNAME
        , BRANCH_ENAME = :BRANCH_ENAME
        , ACCOUNT_CNAME = :ACCOUNT_CNAME
        , ACCOUNT_ENAME = :ACCOUNT_ENAME
        , ACCOUNT = :ACCOUNT
        , BANK_C_ADDRESS = :BANK_C_ADDRESS
        , BANK_E_ADDRESS = :BANK_E_ADDRESS
        , SWIFT_CODE = :SWIFT_CODE
        , LAST_UPDATE_DATE = SYSDATE
        , LAST_UPDATED_BY = :USER_NAME

        , BANK_CNAME2 = :BANK_CNAME2
        , BANK_ENAME2 = :BANK_ENAME2
        , BRANCH_CNAME2 = :BRANCH_CNAME2
        , BRANCH_ENAME2 = :BRANCH_ENAME2
        , ACCOUNT_CNAME2 = :ACCOUNT_CNAME2
        , ACCOUNT_ENAME2 = :ACCOUNT_ENAME2
        , ACCOUNT2 = :ACCOUNT2
        , BANK_C_ADDRESS2 = :BANK_C_ADDRESS2
        , BANK_E_ADDRESS2 = :BANK_E_ADDRESS2
        , SWIFT_CODE2 = :SWIFT_CODE2
        , CURRENCY = :CURRENCY
    WHERE CUST_ID = :CUST_ID
        AND BANK_ID = :BANK_ID;

    
    
    UPDATE BL_CUST_BANK
        SET
          BANK_CNAME = :BANK_CNAME_2
      
        , BANK_ENAME = :BANK_ENAME_2
        , BRANCH_CNAME = :BRANCH_CNAME_2
        , BRANCH_ENAME = :BRANCH_ENAME_2
        , ACCOUNT_CNAME = :ACCOUNT_CNAME_2
        , ACCOUNT_ENAME = :ACCOUNT_ENAME_2
        , ACCOUNT = :ACCOUNT_2
        , BANK_C_ADDRESS = :BANK_C_ADDRESS_2
        , BANK_E_ADDRESS = :BANK_E_ADDRESS_2
        , SWIFT_CODE = :SWIFT_CODE_2
        , LAST_UPDATE_DATE = SYSDATE
        , LAST_UPDATED_BY = :USER_NAME

        , BANK_CNAME2 = :BANK_CNAME2_2
        , BANK_ENAME2 = :BANK_ENAME2_2
        , BRANCH_CNAME2 = :BRANCH_CNAME2_2
        , BRANCH_ENAME2 = :BRANCH_ENAME2_2
        , ACCOUNT_CNAME2 = :ACCOUNT_CNAME2_2
        , ACCOUNT_ENAME2 = :ACCOUNT_ENAME2_2
        , ACCOUNT2 = :ACCOUNT2_2
        , BANK_C_ADDRESS2 = :BANK_C_ADDRESS2_2
        , BANK_E_ADDRESS2 = :BANK_E_ADDRESS2_2
        , SWIFT_CODE2 = :SWIFT_CODE2_2
        , CURRENCY = :CURRENCY_2
    WHERE CUST_ID = :CUST_ID
        AND BANK_ID = :BANK_ID_2;


    UPDATE BL_CUST_BANK
        SET
          BANK_CNAME = :BANK_CNAME_3
      
        , BANK_ENAME = :BANK_ENAME_3
        , BRANCH_CNAME = :BRANCH_CNAME_3
        , BRANCH_ENAME = :BRANCH_ENAME_3
        , ACCOUNT_CNAME = :ACCOUNT_CNAME_3
        , ACCOUNT_ENAME = :ACCOUNT_ENAME_3
        , ACCOUNT = :ACCOUNT_3
        , BANK_C_ADDRESS = :BANK_C_ADDRESS_3
        , BANK_E_ADDRESS = :BANK_E_ADDRESS_3
        , SWIFT_CODE = :SWIFT_CODE_3
        , LAST_UPDATE_DATE = SYSDATE
        , LAST_UPDATED_BY = :USER_NAME

        , BANK_CNAME2 = :BANK_CNAME2_3
        , BANK_ENAME2 = :BANK_ENAME2_3
        , BRANCH_CNAME2 = :BRANCH_CNAME2_3
        , BRANCH_ENAME2 = :BRANCH_ENAME2_3
        , ACCOUNT_CNAME2 = :ACCOUNT_CNAME2_3
        , ACCOUNT_ENAME2 = :ACCOUNT_ENAME2_3
        , ACCOUNT2 = :ACCOUNT2_3
        , BANK_C_ADDRESS2 = :BANK_C_ADDRESS2_3
        , BANK_E_ADDRESS2 = :BANK_E_ADDRESS2_3
        , SWIFT_CODE2 = :SWIFT_CODE2_3
        , CURRENCY = :CURRENCY_3
        , ACCOUNT_TYPE = :ACCOUNT_TYPE_3
    WHERE CUST_ID = :CUST_ID
        AND BANK_ID = :BANK_ID_3;

    UPDATE BL_CUST_BANK
        SET
          BANK_CNAME = :BANK_CNAME_3_2
      
        , BANK_ENAME = :BANK_ENAME_3_2
        , BRANCH_CNAME = :BRANCH_CNAME_3_2
        , BRANCH_ENAME = :BRANCH_ENAME_3_2
        , ACCOUNT_CNAME = :ACCOUNT_CNAME_3_2
        , ACCOUNT_ENAME = :ACCOUNT_ENAME_3_2
        , ACCOUNT = :ACCOUNT_3_2
        , BANK_C_ADDRESS = :BANK_C_ADDRESS_3_2
        , BANK_E_ADDRESS = :BANK_E_ADDRESS_3_2
        , SWIFT_CODE = :SWIFT_CODE_3_2
        , LAST_UPDATE_DATE = SYSDATE
        , LAST_UPDATED_BY = :USER_NAME

        , BANK_CNAME2 = :BANK_CNAME2_3_2
        , BANK_ENAME2 = :BANK_ENAME2_3_2
        , BRANCH_CNAME2 = :BRANCH_CNAME2_3_2
        , BRANCH_ENAME2 = :BRANCH_ENAME2_3_2
        , ACCOUNT_CNAME2 = :ACCOUNT_CNAME2_3_2
        , ACCOUNT_ENAME2 = :ACCOUNT_ENAME2_3_2
        , ACCOUNT2 = :ACCOUNT2_3_2
        , BANK_C_ADDRESS2 = :BANK_C_ADDRESS2_3_2
        , BANK_E_ADDRESS2 = :BANK_E_ADDRESS2_3_2
        , SWIFT_CODE2 = :SWIFT_CODE2_3_2
        , CURRENCY = :CURRENCY_3_2
        , ACCOUNT_TYPE = :ACCOUNT_TYPE_3_2
    WHERE CUST_ID = :CUST_ID
        AND BANK_ID = :BANK_ID_3_2;

    UPDATE BL_CUST_BANK
        SET
          BANK_CNAME = :BANK_CNAME_4
      
        , BANK_ENAME = :BANK_ENAME_4
        , BRANCH_CNAME = :BRANCH_CNAME_4
        , BRANCH_ENAME = :BRANCH_ENAME_4
        , ACCOUNT_CNAME = :ACCOUNT_CNAME_4
        , ACCOUNT_ENAME = :ACCOUNT_ENAME_4
        , ACCOUNT = :ACCOUNT_4
        , BANK_C_ADDRESS = :BANK_C_ADDRESS_4
        , BANK_E_ADDRESS = :BANK_E_ADDRESS_4
        , SWIFT_CODE = :SWIFT_CODE_4
        , LAST_UPDATE_DATE = SYSDATE
        , LAST_UPDATED_BY = :USER_NAME

        , BANK_CNAME2 = :BANK_CNAME2_4
        , BANK_ENAME2 = :BANK_ENAME2_4
        , BRANCH_CNAME2 = :BRANCH_CNAME2_4
        , BRANCH_ENAME2 = :BRANCH_ENAME2_4
        , ACCOUNT_CNAME2 = :ACCOUNT_CNAME2_4
        , ACCOUNT_ENAME2 = :ACCOUNT_ENAME2_4
        , ACCOUNT2 = :ACCOUNT2_4
        , BANK_C_ADDRESS2 = :BANK_C_ADDRESS2_4
        , BANK_E_ADDRESS2 = :BANK_E_ADDRESS2_4
        , SWIFT_CODE2 = :SWIFT_CODE2_4
        , CURRENCY = :CURRENCY_4
    WHERE CUST_ID = :CUST_ID
        AND BANK_ID = :BANK_ID_4;

    UPDATE BL_CUST_BANK
        SET
          BANK_CNAME = :BANK_CNAME_5
      
        , BANK_ENAME = :BANK_ENAME_5
        , BRANCH_CNAME = :BRANCH_CNAME_5
        , BRANCH_ENAME = :BRANCH_ENAME_5
        , ACCOUNT_CNAME = :ACCOUNT_CNAME_5
        , ACCOUNT_ENAME = :ACCOUNT_ENAME_5
        , ACCOUNT = :ACCOUNT_5
        , BANK_C_ADDRESS = :BANK_C_ADDRESS_5
        , BANK_E_ADDRESS = :BANK_E_ADDRESS_5
        , SWIFT_CODE = :SWIFT_CODE_5
        , LAST_UPDATE_DATE = SYSDATE
        , LAST_UPDATED_BY = :USER_NAME

        , BANK_CNAME2 = :BANK_CNAME2_5
        , BANK_ENAME2 = :BANK_ENAME2_5
        , BRANCH_CNAME2 = :BRANCH_CNAME2_5
        , BRANCH_ENAME2 = :BRANCH_ENAME2_5
        , ACCOUNT_CNAME2 = :ACCOUNT_CNAME2_5
        , ACCOUNT_ENAME2 = :ACCOUNT_ENAME2_5
        , ACCOUNT2 = :ACCOUNT2_5
        , BANK_C_ADDRESS2 = :BANK_C_ADDRESS2_5
        , BANK_E_ADDRESS2 = :BANK_E_ADDRESS2_5
        , SWIFT_CODE2 = :SWIFT_CODE2_5
        , CURRENCY = :CURRENCY_5
    WHERE CUST_ID = :CUST_ID
        AND BANK_ID = :BANK_ID_5;

    UPDATE BL_CUST_BANK
        SET
          BANK_CNAME = :BANK_CNAME_6
      
        , BANK_ENAME = :BANK_ENAME_6
        , BRANCH_CNAME = :BRANCH_CNAME_6
        , BRANCH_ENAME = :BRANCH_ENAME_6
        , ACCOUNT_CNAME = :ACCOUNT_CNAME_6
        , ACCOUNT_ENAME = :ACCOUNT_ENAME_6
        , ACCOUNT = :ACCOUNT_6
        , BANK_C_ADDRESS = :BANK_C_ADDRESS_6
        , BANK_E_ADDRESS = :BANK_E_ADDRESS_6
        , SWIFT_CODE = :SWIFT_CODE_6
        , LAST_UPDATE_DATE = SYSDATE
        , LAST_UPDATED_BY = :USER_NAME

        , BANK_CNAME2 = :BANK_CNAME2_6
        , BANK_ENAME2 = :BANK_ENAME2_6
        , BRANCH_CNAME2 = :BRANCH_CNAME2_6
        , BRANCH_ENAME2 = :BRANCH_ENAME2_6
        , ACCOUNT_CNAME2 = :ACCOUNT_CNAME2_6
        , ACCOUNT_ENAME2 = :ACCOUNT_ENAME2_6
        , ACCOUNT2 = :ACCOUNT2_6
        , BANK_C_ADDRESS2 = :BANK_C_ADDRESS2_6
        , BANK_E_ADDRESS2 = :BANK_E_ADDRESS2_6
        , SWIFT_CODE2 = :SWIFT_CODE2_6
        , CURRENCY = :CURRENCY_6
    WHERE CUST_ID = :CUST_ID
        AND BANK_ID = :BANK_ID_6;

    UPDATE BL_CUST_BANK
        SET
          BANK_CNAME = :BANK_CNAME_7
      
        , BANK_ENAME = :BANK_ENAME_7
        , BRANCH_CNAME = :BRANCH_CNAME_7
        , BRANCH_ENAME = :BRANCH_ENAME_7
        , ACCOUNT_CNAME = :ACCOUNT_CNAME_7
        , ACCOUNT_ENAME = :ACCOUNT_ENAME_7
        , ACCOUNT = :ACCOUNT_7
        , BANK_C_ADDRESS = :BANK_C_ADDRESS_7
        , BANK_E_ADDRESS = :BANK_E_ADDRESS_7
        , SWIFT_CODE = :SWIFT_CODE_7
        , LAST_UPDATE_DATE = SYSDATE
        , LAST_UPDATED_BY = :USER_NAME

        , BANK_CNAME2 = :BANK_CNAME2_7
        , BANK_ENAME2 = :BANK_ENAME2_7
        , BRANCH_CNAME2 = :BRANCH_CNAME2_7
        , BRANCH_ENAME2 = :BRANCH_ENAME2_7
        , ACCOUNT_CNAME2 = :ACCOUNT_CNAME2_7
        , ACCOUNT_ENAME2 = :ACCOUNT_ENAME2_7
        , ACCOUNT2 = :ACCOUNT2_7
        , BANK_C_ADDRESS2 = :BANK_C_ADDRESS2_7
        , BANK_E_ADDRESS2 = :BANK_E_ADDRESS2_7
        , SWIFT_CODE2 = :SWIFT_CODE2_7
        , CURRENCY = :CURRENCY_7
    WHERE CUST_ID = :CUST_ID
        AND BANK_ID = :BANK_ID_7;

END;
";

                base.ExecuteScalar(strSQL, new
                {
                    CUST_ID = strCustID,
                    PHONE_1 = dataCust.PHONE_1,
                    PHONE_2 = dataCust.PHONE_2,
                    EMAIL_1 = dataCust.EMAIL_1,
                    EMAIL_2 = dataCust.EMAIL_2,
                    C_ADDRESS = dataCust.C_ADDRESS,
                    C_ADDRESS2 = dataCust.C_ADDRESS2,
                    MAIL_TYPE_MONTHLY = dataCust.MAIL_TYPE_MONTHLY,
                    MAIL_TYPE_END = dataCust.MAIL_TYPE_END,
                    POSTAL_CODE = dataCust.POSTAL_CODE,
                    POSTAL_CODE2 = dataCust.POSTAL_CODE2,

                    CUST_CNAME = dataCust.CUST_CNAME,
                    CUST_ENAME = dataCust.CUST_ENAME,
                    CUST_CNAME2 = dataCust.CUST_CNAME2,
                    CUST_ENAME2 = dataCust.CUST_ENAME2,
                    ID_NUMBER = dataCust.ID_NUMBER,
                    ID_NUMBER2 = dataCust.ID_NUMBER2,
                    PASSPORT = dataCust.PASSPORT,
                    PASSPORT2 = dataCust.PASSPORT2,

                    SEX = dataCust.SEX,
                    SEX2 = dataCust.SEX2,
                    DATE_OF_BIRTH = dataCust.DATE_OF_BIRTH,
                    DATE_OF_BIRTH2 = dataCust.DATE_OF_BIRTH2,
                    PASSPORT_REGION = dataCust.PASSPORT_REGION,
                    PASSPORT_REGION2 = dataCust.PASSPORT_REGION2,
                    IS_ONLINE_WITHDRAWAL = dataCust.IS_ONLINE_WITHDRAWAL,


                    BANK_CNAME = dataBank.BANK_CNAME,
                    BANK_ENAME = dataBank.BANK_ENAME,
                    BRANCH_CNAME = dataBank.BRANCH_CNAME,
                    BRANCH_ENAME = dataBank.BRANCH_ENAME,
                    ACCOUNT_CNAME = dataBank.ACCOUNT_CNAME,
                    ACCOUNT_ENAME = dataBank.ACCOUNT_ENAME,
                    ACCOUNT = dataBank.ACCOUNT,
                    BANK_C_ADDRESS = dataBank.BANK_C_ADDRESS,
                    BANK_E_ADDRESS = dataBank.BANK_E_ADDRESS,
                    SWIFT_CODE = dataBank.SWIFT_CODE,

                    BANK_CNAME2 = dataBank.BANK_CNAME2,
                    BANK_ENAME2 = dataBank.BANK_ENAME2,
                    BRANCH_CNAME2 = dataBank.BRANCH_CNAME2,
                    BRANCH_ENAME2 = dataBank.BRANCH_ENAME2,
                    ACCOUNT_CNAME2 = dataBank.ACCOUNT_CNAME2,
                    ACCOUNT_ENAME2 = dataBank.ACCOUNT_ENAME2,
                    ACCOUNT2 = dataBank.ACCOUNT2,
                    BANK_C_ADDRESS2 = dataBank.BANK_C_ADDRESS2,
                    BANK_E_ADDRESS2 = dataBank.BANK_E_ADDRESS2,
                    SWIFT_CODE2 = dataBank.SWIFT_CODE2,
                    BANK_ID = dataBank.BANK_ID,
                    CURRENCY = dataBank.CURRENCY,

                    BANK_CNAME_2 = dataBank2.BANK_CNAME,
                    BANK_ENAME_2 = dataBank2.BANK_ENAME,
                    BRANCH_CNAME_2 = dataBank2.BRANCH_CNAME,
                    BRANCH_ENAME_2 = dataBank2.BRANCH_ENAME,
                    ACCOUNT_CNAME_2 = dataBank2.ACCOUNT_CNAME,
                    ACCOUNT_ENAME_2 = dataBank2.ACCOUNT_ENAME,
                    ACCOUNT_2 = dataBank2.ACCOUNT,
                    BANK_C_ADDRESS_2 = dataBank2.BANK_C_ADDRESS,
                    BANK_E_ADDRESS_2 = dataBank2.BANK_E_ADDRESS,
                    SWIFT_CODE_2 = dataBank2.SWIFT_CODE,

                    BANK_CNAME2_2 = dataBank2.BANK_CNAME2,
                    BANK_ENAME2_2 = dataBank2.BANK_ENAME2,
                    BRANCH_CNAME2_2 = dataBank2.BRANCH_CNAME2,
                    BRANCH_ENAME2_2 = dataBank2.BRANCH_ENAME2,
                    ACCOUNT_CNAME2_2 = dataBank2.ACCOUNT_CNAME2,
                    ACCOUNT_ENAME2_2 = dataBank2.ACCOUNT_ENAME2,
                    ACCOUNT2_2 = dataBank2.ACCOUNT2,
                    BANK_C_ADDRESS2_2 = dataBank2.BANK_C_ADDRESS2,
                    BANK_E_ADDRESS2_2 = dataBank2.BANK_E_ADDRESS2,
                    SWIFT_CODE2_2 = dataBank2.SWIFT_CODE2,
                    BANK_ID_2 = dataBank2.BANK_ID,
                    CURRENCY_2 = dataBank2.CURRENCY,


                    BANK_CNAME_3 = dataBank3.BANK_CNAME,
                    BANK_ENAME_3 = dataBank3.BANK_ENAME,
                    BRANCH_CNAME_3 = dataBank3.BRANCH_CNAME,
                    BRANCH_ENAME_3 = dataBank3.BRANCH_ENAME,
                    ACCOUNT_CNAME_3 = dataBank3.ACCOUNT_CNAME,
                    ACCOUNT_ENAME_3 = dataBank3.ACCOUNT_ENAME,
                    ACCOUNT_3 = dataBank3.ACCOUNT,
                    BANK_C_ADDRESS_3 = dataBank3.BANK_C_ADDRESS,
                    BANK_E_ADDRESS_3 = dataBank3.BANK_E_ADDRESS,
                    SWIFT_CODE_3 = dataBank3.SWIFT_CODE,

                    BANK_CNAME2_3 = dataBank3.BANK_CNAME2,
                    BANK_ENAME2_3 = dataBank3.BANK_ENAME2,
                    BRANCH_CNAME2_3 = dataBank3.BRANCH_CNAME2,
                    BRANCH_ENAME2_3 = dataBank3.BRANCH_ENAME2,
                    ACCOUNT_CNAME2_3 = dataBank3.ACCOUNT_CNAME2,
                    ACCOUNT_ENAME2_3 = dataBank3.ACCOUNT_ENAME2,
                    ACCOUNT2_3 = dataBank3.ACCOUNT2,
                    BANK_C_ADDRESS2_3 = dataBank3.BANK_C_ADDRESS2,
                    BANK_E_ADDRESS2_3 = dataBank3.BANK_E_ADDRESS2,
                    SWIFT_CODE2_3 = dataBank3.SWIFT_CODE2,
                    BANK_ID_3 = dataBank3.BANK_ID,
                    CURRENCY_3 = dataBank3.CURRENCY,
                    ACCOUNT_TYPE_3 = dataBank3.ACCOUNT_TYPE,

                    BANK_CNAME_3_2 = dataBank3_2.BANK_CNAME,
                    BANK_ENAME_3_2 = dataBank3_2.BANK_ENAME,
                    BRANCH_CNAME_3_2 = dataBank3_2.BRANCH_CNAME,
                    BRANCH_ENAME_3_2 = dataBank3_2.BRANCH_ENAME,
                    ACCOUNT_CNAME_3_2 = dataBank3_2.ACCOUNT_CNAME,
                    ACCOUNT_ENAME_3_2 = dataBank3_2.ACCOUNT_ENAME,
                    ACCOUNT_3_2 = dataBank3_2.ACCOUNT,
                    BANK_C_ADDRESS_3_2 = dataBank3_2.BANK_C_ADDRESS,
                    BANK_E_ADDRESS_3_2 = dataBank3_2.BANK_E_ADDRESS,
                    SWIFT_CODE_3_2 = dataBank3_2.SWIFT_CODE,

                    BANK_CNAME2_3_2 = dataBank3_2.BANK_CNAME2,
                    BANK_ENAME2_3_2 = dataBank3_2.BANK_ENAME2,
                    BRANCH_CNAME2_3_2 = dataBank3_2.BRANCH_CNAME2,
                    BRANCH_ENAME2_3_2 = dataBank3_2.BRANCH_ENAME2,
                    ACCOUNT_CNAME2_3_2 = dataBank3_2.ACCOUNT_CNAME2,
                    ACCOUNT_ENAME2_3_2 = dataBank3_2.ACCOUNT_ENAME2,
                    ACCOUNT2_3_2 = dataBank3_2.ACCOUNT2,
                    BANK_C_ADDRESS2_3_2 = dataBank3_2.BANK_C_ADDRESS2,
                    BANK_E_ADDRESS2_3_2 = dataBank3_2.BANK_E_ADDRESS2,
                    SWIFT_CODE2_3_2 = dataBank3_2.SWIFT_CODE2,
                    BANK_ID_3_2 = dataBank3_2.BANK_ID,
                    CURRENCY_3_2 = dataBank3_2.CURRENCY,
                    ACCOUNT_TYPE_3_2 = dataBank3_2.ACCOUNT_TYPE,

                    BANK_CNAME_4 = dataBank4.BANK_CNAME,
                    BANK_ENAME_4 = dataBank4.BANK_ENAME,
                    BRANCH_CNAME_4 = dataBank4.BRANCH_CNAME,
                    BRANCH_ENAME_4 = dataBank4.BRANCH_ENAME,
                    ACCOUNT_CNAME_4 = dataBank4.ACCOUNT_CNAME,
                    ACCOUNT_ENAME_4 = dataBank4.ACCOUNT_ENAME,
                    ACCOUNT_4 = dataBank4.ACCOUNT,
                    BANK_C_ADDRESS_4 = dataBank4.BANK_C_ADDRESS,
                    BANK_E_ADDRESS_4 = dataBank4.BANK_E_ADDRESS,
                    SWIFT_CODE_4 = dataBank4.SWIFT_CODE,

                    BANK_CNAME2_4 = dataBank4.BANK_CNAME2,
                    BANK_ENAME2_4 = dataBank4.BANK_ENAME2,
                    BRANCH_CNAME2_4 = dataBank4.BRANCH_CNAME2,
                    BRANCH_ENAME2_4 = dataBank4.BRANCH_ENAME2,
                    ACCOUNT_CNAME2_4 = dataBank4.ACCOUNT_CNAME2,
                    ACCOUNT_ENAME2_4 = dataBank4.ACCOUNT_ENAME2,
                    ACCOUNT2_4 = dataBank4.ACCOUNT2,
                    BANK_C_ADDRESS2_4 = dataBank4.BANK_C_ADDRESS2,
                    BANK_E_ADDRESS2_4 = dataBank4.BANK_E_ADDRESS2,
                    SWIFT_CODE2_4 = dataBank4.SWIFT_CODE2,
                    BANK_ID_4 = dataBank4.BANK_ID,
                    CURRENCY_4 = dataBank4.CURRENCY,


                    BANK_CNAME_5 = dataBank5.BANK_CNAME,
                    BANK_ENAME_5 = dataBank5.BANK_ENAME,
                    BRANCH_CNAME_5 = dataBank5.BRANCH_CNAME,
                    BRANCH_ENAME_5 = dataBank5.BRANCH_ENAME,
                    ACCOUNT_CNAME_5 = dataBank5.ACCOUNT_CNAME,
                    ACCOUNT_ENAME_5 = dataBank5.ACCOUNT_ENAME,
                    ACCOUNT_5 = dataBank5.ACCOUNT,
                    BANK_C_ADDRESS_5 = dataBank5.BANK_C_ADDRESS,
                    BANK_E_ADDRESS_5 = dataBank5.BANK_E_ADDRESS,
                    SWIFT_CODE_5 = dataBank5.SWIFT_CODE,

                    BANK_CNAME2_5 = dataBank5.BANK_CNAME2,
                    BANK_ENAME2_5 = dataBank5.BANK_ENAME2,
                    BRANCH_CNAME2_5 = dataBank5.BRANCH_CNAME2,
                    BRANCH_ENAME2_5 = dataBank5.BRANCH_ENAME2,
                    ACCOUNT_CNAME2_5 = dataBank5.ACCOUNT_CNAME2,
                    ACCOUNT_ENAME2_5 = dataBank5.ACCOUNT_ENAME2,
                    ACCOUNT2_5 = dataBank5.ACCOUNT2,
                    BANK_C_ADDRESS2_5 = dataBank5.BANK_C_ADDRESS2,
                    BANK_E_ADDRESS2_5 = dataBank5.BANK_E_ADDRESS2,
                    SWIFT_CODE2_5 = dataBank5.SWIFT_CODE2,
                    BANK_ID_5 = dataBank5.BANK_ID,
                    CURRENCY_5 = dataBank5.CURRENCY,


                    BANK_CNAME_6 = dataBank6.BANK_CNAME,
                    BANK_ENAME_6 = dataBank6.BANK_ENAME,
                    BRANCH_CNAME_6 = dataBank6.BRANCH_CNAME,
                    BRANCH_ENAME_6 = dataBank6.BRANCH_ENAME,
                    ACCOUNT_CNAME_6 = dataBank6.ACCOUNT_CNAME,
                    ACCOUNT_ENAME_6 = dataBank6.ACCOUNT_ENAME,
                    ACCOUNT_6 = dataBank6.ACCOUNT,
                    BANK_C_ADDRESS_6 = dataBank6.BANK_C_ADDRESS,
                    BANK_E_ADDRESS_6 = dataBank6.BANK_E_ADDRESS,
                    SWIFT_CODE_6 = dataBank6.SWIFT_CODE,

                    BANK_CNAME2_6 = dataBank6.BANK_CNAME2,
                    BANK_ENAME2_6 = dataBank6.BANK_ENAME2,
                    BRANCH_CNAME2_6 = dataBank6.BRANCH_CNAME2,
                    BRANCH_ENAME2_6 = dataBank6.BRANCH_ENAME2,
                    ACCOUNT_CNAME2_6 = dataBank6.ACCOUNT_CNAME2,
                    ACCOUNT_ENAME2_6 = dataBank6.ACCOUNT_ENAME2,
                    ACCOUNT2_6 = dataBank6.ACCOUNT2,
                    BANK_C_ADDRESS2_6 = dataBank6.BANK_C_ADDRESS2,
                    BANK_E_ADDRESS2_6 = dataBank6.BANK_E_ADDRESS2,
                    SWIFT_CODE2_6 = dataBank6.SWIFT_CODE2,
                    BANK_ID_6 = dataBank6.BANK_ID,
                    CURRENCY_6 = dataBank6.CURRENCY,


                    BANK_CNAME_7 = dataBank7.BANK_CNAME,
                    BANK_ENAME_7 = dataBank7.BANK_ENAME,
                    BRANCH_CNAME_7 = dataBank7.BRANCH_CNAME,
                    BRANCH_ENAME_7 = dataBank7.BRANCH_ENAME,
                    ACCOUNT_CNAME_7 = dataBank7.ACCOUNT_CNAME,
                    ACCOUNT_ENAME_7 = dataBank7.ACCOUNT_ENAME,
                    ACCOUNT_7 = dataBank7.ACCOUNT,
                    BANK_C_ADDRESS_7 = dataBank7.BANK_C_ADDRESS,
                    BANK_E_ADDRESS_7 = dataBank7.BANK_E_ADDRESS,
                    SWIFT_CODE_7 = dataBank7.SWIFT_CODE,

                    BANK_CNAME2_7 = dataBank7.BANK_CNAME2,
                    BANK_ENAME2_7 = dataBank7.BANK_ENAME2,
                    BRANCH_CNAME2_7 = dataBank7.BRANCH_CNAME2,
                    BRANCH_ENAME2_7 = dataBank7.BRANCH_ENAME2,
                    ACCOUNT_CNAME2_7 = dataBank7.ACCOUNT_CNAME2,
                    ACCOUNT_ENAME2_7 = dataBank7.ACCOUNT_ENAME2,
                    ACCOUNT2_7 = dataBank7.ACCOUNT2,
                    BANK_C_ADDRESS2_7 = dataBank7.BANK_C_ADDRESS2,
                    BANK_E_ADDRESS2_7 = dataBank7.BANK_E_ADDRESS2,
                    SWIFT_CODE2_7 = dataBank7.SWIFT_CODE2,
                    BANK_ID_7 = dataBank7.BANK_ID,
                    CURRENCY_7 = dataBank7.CURRENCY,


                    USER_NAME = strUserName
                }, CommandType.Text);
                return true;
            }
            catch (Exception ex)
            {
                exError = ex;
                return false;
            }
        }





        //        public void UpdateContractData(BL_CUST dataCust, BL_CUST_BANK dataBank, string strCustID, out Exception exError)
        //        {
        //            exError = null;
        //            try
        //            {
        //                var strSQL = @"
        //BEGIN 

        //    UPDATE BL_CUST A
        //    SET
        //          A.CUST_CNAME = :CUST_CNAME
        //        , A.CUST_CNAME2 = :CUST_CNAME2
        //        , A.ID_NUMBER = :ID_NUMBER
        //        , A.ID_NUMBER2 = :ID_NUMBER2
        //        , A.PHONE_1 = :PHONE_1
        //        , A.PHONE_2 = :PHONE_2
        //        , A.EMAIL_1 = :EMAIL_1
        //        , A.EMAIL_2 = :EMAIL_2
        //        , A.C_ADDRESS = :C_ADDRESS
        //        , A.C_ADDRESS2 = :C_ADDRESS2
        //        , A.SEX = :SEX
        //        , A.SEX2 = :SEX2
        //        , A.DATE_OF_BIRTH = :DATE_OF_BIRTH
        //        , A.DATE_OF_BIRTH2 = :DATE_OF_BIRTH2
        //    WHERE A.CUST_ID = :CUST_ID ;

        //    UPDATE BL_CUST_BANK A
        //    SET
        //          A.BANK_CNAME = :BANK_CNAME
        //        , A.BRANCH_CNAME = :BRANCH_CNAME
        //        , A.ACCOUNT_CNAME = :ACCOUNT_CNAME
        //        , A.ACCOUNT = :ACCOUNT
        //        , A.BANK_C_ADDRESS = :BANK_C_ADDRESS
        //        , A.SWIFT_CODE = :SWIFT_CODE
        //    WHERE A.CUST_ID = :CUST_ID ;

        //END ;
        //";
        //                base.ExecuteScalar(strSQL, new
        //                {
        //                    CUST_ID = strCustID,
        //                    CUST_CNAME = dataCust.CUST_CNAME,
        //                    CUST_CNAME2 = dataCust.CUST_CNAME2,
        //                    ID_NUMBER = dataCust.ID_NUMBER,
        //                    ID_NUMBER2 = dataCust.ID_NUMBER2,
        //                    PHONE_1 = dataCust.PHONE_1,
        //                    PHONE_2 = dataCust.PHONE_2,
        //                    EMAIL_1 = dataCust.EMAIL_1,
        //                    EMAIL_2 = dataCust.EMAIL_2,
        //                    C_ADDRESS = dataCust.C_ADDRESS,
        //                    C_ADDRESS2 = dataCust.C_ADDRESS2,
        //                    SEX = dataCust.SEX,
        //                    SEX2 = dataCust.SEX2,
        //                    DATE_OF_BIRTH = dataCust.DATE_OF_BIRTH,
        //                    DATE_OF_BIRTH2 = dataCust.DATE_OF_BIRTH2,

        //                    BANK_CNAME = dataBank.BANK_CNAME,
        //                    BRANCH_CNAME = dataBank.BRANCH_CNAME,
        //                    ACCOUNT_CNAME = dataBank.ACCOUNT_CNAME,
        //                    ACCOUNT = dataBank.ACCOUNT,
        //                    BANK_C_ADDRESS = dataBank.BANK_C_ADDRESS,
        //                    SWIFT_CODE = dataBank.SWIFT_CODE
        //                }, CommandType.Text);

        //            }
        //            catch (Exception ex)
        //            {
        //                exError = ex;
        //            }
        //        }


        public void InsertContractData
            (
                BL_CUST dataCust,
                BL_CUST_BANK dataBank,
                BL_CUST_BANK dataBank2,
                BL_CUST_BANK dataBank3,
                BL_CUST_BANK dataBank3_2,
                BL_CUST_BANK dataBank4,
                BL_CUST_BANK dataBank5,
                BL_CUST_BANK dataBank6,
                BL_CUST_BANK dataBank7,
                string strCustID,
                string strBankID,
                string strBankID2,
                string strBankID3,
                string strBankID3_2,
                string strBankID4,
                string strBankID5,
                string strBankID6,
                string strBankID7,
                string strUserName,
                out Exception exError
            )
        {
            exError = null;
            try
            {
                var strSQL = @"
BEGIN 

    INSERT INTO BL_CUST 
    (
          CUST_ID
        , CUST_CNAME
        , CUST_ENAME
        , CUST_CNAME2
        , CUST_ENAME2
        , ID_NUMBER
        , ID_NUMBER2
        , PASSPORT
        , PASSPORT2
        , PHONE_1
        , PHONE_2
        , EMAIL_1
        , EMAIL_2
        , C_ADDRESS
        , C_ADDRESS2
        , SEX
        , SEX2
        , DATE_OF_BIRTH
        , DATE_OF_BIRTH2
        , MAIL_TYPE_MONTHLY
        , MAIL_TYPE_END
        , CREATION_DATE
        , LAST_UPDATE_DATE
        , PASSPORT_REGION
        , PASSPORT_REGION2
        , CREATED_BY
        , LAST_UPDATED_BY
        , POSTAL_CODE
        , POSTAL_CODE2
        , IS_ONLINE_WITHDRAWAL
        , ORG_CODE
    )
    SELECT 
          :CUST_ID
        , BL_CRYPT_PKG.ENCRYPT_DES_NOKEY(:CUST_CNAME)
        , BL_CRYPT_PKG.ENCRYPT_DES_NOKEY(:CUST_ENAME)
        , BL_CRYPT_PKG.ENCRYPT_DES_NOKEY(:CUST_CNAME2)
        , BL_CRYPT_PKG.ENCRYPT_DES_NOKEY(:CUST_ENAME2)
        , BL_CRYPT_PKG.ENCRYPT_DES_NOKEY(:ID_NUMBER)
        , BL_CRYPT_PKG.ENCRYPT_DES_NOKEY(:ID_NUMBER2)
        , :PASSPORT
        , :PASSPORT2
        , BL_CRYPT_PKG.ENCRYPT_DES_NOKEY(:PHONE_1)
        , BL_CRYPT_PKG.ENCRYPT_DES_NOKEY(:PHONE_2)
        , BL_CRYPT_PKG.ENCRYPT_DES_NOKEY(:EMAIL_1)
        , BL_CRYPT_PKG.ENCRYPT_DES_NOKEY(:EMAIL_2)
        , BL_CRYPT_PKG.ENCRYPT_DES_NOKEY(:C_ADDRESS)
        , BL_CRYPT_PKG.ENCRYPT_DES_NOKEY(:C_ADDRESS2)
        , :SEX
        , :SEX2
        , :DATE_OF_BIRTH
        , :DATE_OF_BIRTH2
        , :MAIL_TYPE_MONTHLY
        , :MAIL_TYPE_END
        , SYSDATE
        , SYSDATE
        , :PASSPORT_REGION
        , :PASSPORT_REGION2
        , :USER_NAME
        , :USER_NAME
        , :POSTAL_CODE
        , :POSTAL_CODE2
        , :IS_ONLINE_WITHDRAWAL
        , :ORG_CODE_CUST
    FROM DUAL;

    INSERT INTO BL_CUST_BANK
    (
          CUST_ID
        , BANK_CNAME
        , BANK_ENAME
        , BRANCH_CNAME
        , BRANCH_ENAME
        , ACCOUNT_CNAME
        , ACCOUNT
        , BANK_C_ADDRESS
        , BANK_E_ADDRESS
        , SWIFT_CODE
        , CREATION_DATE
        , LAST_UPDATE_DATE
        , BANK_ID
        , ACCOUNT_ENAME

        , BANK_CNAME2
        , BANK_ENAME2
        , BRANCH_CNAME2
        , BRANCH_ENAME2
        , ACCOUNT_CNAME2
        , ACCOUNT2
        , BANK_C_ADDRESS2
        , BANK_E_ADDRESS2
        , SWIFT_CODE2
        , CREATED_BY
        , LAST_UPDATED_BY
        , CURRENCY
        , ACCOUNT_ENAME2
    )
    SELECT
          :CUST_ID
        , :BANK_CNAME
        , :BANK_ENAME
        , :BRANCH_CNAME
        , :BRANCH_ENAME
        , :ACCOUNT_CNAME
        , :ACCOUNT
        , :BANK_C_ADDRESS
        , :BANK_E_ADDRESS
        , :SWIFT_CODE
        , SYSDATE
        , SYSDATE
        ,  :BANK_ID
        , :ACCOUNT_ENAME

        , :BANK_CNAME2
        , :BANK_ENAME2
        , :BRANCH_CNAME2
        , :BRANCH_ENAME2
        , :ACCOUNT_CNAME2
        , :ACCOUNT2
        , :BANK_C_ADDRESS2
        , :BANK_E_ADDRESS2
        , :SWIFT_CODE2
        , :USER_NAME
        , :USER_NAME
        , :CURRENCY
        , :ACCOUNT_ENAME2
    FROM DUAL;


    INSERT INTO BL_CUST_BANK
    (
          CUST_ID
        , BANK_CNAME
        , BANK_ENAME
        , BRANCH_CNAME
        , BRANCH_ENAME
        , ACCOUNT_CNAME
        , ACCOUNT
        , BANK_C_ADDRESS
        , BANK_E_ADDRESS
        , SWIFT_CODE
        , CREATION_DATE
        , LAST_UPDATE_DATE
        , BANK_ID
        , ACCOUNT_ENAME

        , BANK_CNAME2
        , BANK_ENAME2
        , BRANCH_CNAME2
        , BRANCH_ENAME2
        , ACCOUNT_CNAME2
        , ACCOUNT2
        , BANK_C_ADDRESS2
        , BANK_E_ADDRESS2
        , SWIFT_CODE2
        , CREATED_BY
        , LAST_UPDATED_BY
        , CURRENCY
        , ACCOUNT_ENAME2
    )
    SELECT
          :CUST_ID
        , :BANK_CNAME_2
        , :BANK_ENAME_2
        , :BRANCH_CNAME_2
        , :BRANCH_ENAME_2
        , :ACCOUNT_CNAME_2
        , :ACCOUNT_2
        , :BANK_C_ADDRESS_2
        , :BANK_E_ADDRESS_2
        , :SWIFT_CODE_2
        , SYSDATE
        , SYSDATE
        ,  :BANK_ID_2
        , :ACCOUNT_ENAME_2

        , :BANK_CNAME2_2
        , :BANK_ENAME2_2
        , :BRANCH_CNAME2_2
        , :BRANCH_ENAME2_2
        , :ACCOUNT_CNAME2_2
        , :ACCOUNT2_2
        , :BANK_C_ADDRESS2_2
        , :BANK_E_ADDRESS2_2
        , :SWIFT_CODE2_2
        , :USER_NAME
        , :USER_NAME
        , :CURRENCY_2
        , :ACCOUNT_ENAME2_2
    FROM DUAL;



    INSERT INTO BL_CUST_BANK
    (
          CUST_ID
        , BANK_CNAME
        , BANK_ENAME
        , BRANCH_CNAME
        , BRANCH_ENAME
        , ACCOUNT_CNAME
        , ACCOUNT
        , BANK_C_ADDRESS
        , BANK_E_ADDRESS
        , SWIFT_CODE
        , CREATION_DATE
        , LAST_UPDATE_DATE
        , BANK_ID
        , ACCOUNT_ENAME

        , BANK_CNAME2
        , BANK_ENAME2
        , BRANCH_CNAME2
        , BRANCH_ENAME2
        , ACCOUNT_CNAME2
        , ACCOUNT2
        , BANK_C_ADDRESS2
        , BANK_E_ADDRESS2
        , SWIFT_CODE2
        , CREATED_BY
        , LAST_UPDATED_BY
        , CURRENCY
        , ACCOUNT_ENAME2
        , ACCOUNT_TYPE
    )
    SELECT
          :CUST_ID
        , :BANK_CNAME_3
        , :BANK_ENAME_3
        , :BRANCH_CNAME_3
        , :BRANCH_ENAME_3
        , :ACCOUNT_CNAME_3
        , :ACCOUNT_3
        , :BANK_C_ADDRESS_3
        , :BANK_E_ADDRESS_3
        , :SWIFT_CODE_3
        , SYSDATE
        , SYSDATE
        ,  :BANK_ID_3
        , :ACCOUNT_ENAME_3

        , :BANK_CNAME2_3
        , :BANK_ENAME2_3
        , :BRANCH_CNAME2_3
        , :BRANCH_ENAME2_3
        , :ACCOUNT_CNAME2_3
        , :ACCOUNT2_3
        , :BANK_C_ADDRESS2_3
        , :BANK_E_ADDRESS2_3
        , :SWIFT_CODE2_3
        , :USER_NAME
        , :USER_NAME
        , :CURRENCY_3
        , :ACCOUNT_ENAME2_3
        , :ACCOUNT_TYPE3
    FROM DUAL;

    INSERT INTO BL_CUST_BANK
    (
          CUST_ID
        , BANK_CNAME
        , BANK_ENAME
        , BRANCH_CNAME
        , BRANCH_ENAME
        , ACCOUNT_CNAME
        , ACCOUNT
        , BANK_C_ADDRESS
        , BANK_E_ADDRESS
        , SWIFT_CODE
        , CREATION_DATE
        , LAST_UPDATE_DATE
        , BANK_ID
        , ACCOUNT_ENAME

        , BANK_CNAME2
        , BANK_ENAME2
        , BRANCH_CNAME2
        , BRANCH_ENAME2
        , ACCOUNT_CNAME2
        , ACCOUNT2
        , BANK_C_ADDRESS2
        , BANK_E_ADDRESS2
        , SWIFT_CODE2
        , CREATED_BY
        , LAST_UPDATED_BY
        , CURRENCY
        , ACCOUNT_ENAME2
        , ACCOUNT_TYPE
    )
    SELECT
          :CUST_ID
        , :BANK_CNAME_3_2
        , :BANK_ENAME_3_2
        , :BRANCH_CNAME_3_2
        , :BRANCH_ENAME_3_2
        , :ACCOUNT_CNAME_3_2
        , :ACCOUNT_3_2
        , :BANK_C_ADDRESS_3_2
        , :BANK_E_ADDRESS_3_2
        , :SWIFT_CODE_3_2
        , SYSDATE
        , SYSDATE
        , :BANK_ID_3_2
        , :ACCOUNT_ENAME_3_2

        , :BANK_CNAME2_3_2
        , :BANK_ENAME2_3_2
        , :BRANCH_CNAME2_3_2
        , :BRANCH_ENAME2_3_2
        , :ACCOUNT_CNAME2_3_2
        , :ACCOUNT2_3_2
        , :BANK_C_ADDRESS2_3_2
        , :BANK_E_ADDRESS2_3_2
        , :SWIFT_CODE2_3_2
        , :USER_NAME
        , :USER_NAME
        , :CURRENCY_3_2
        , :ACCOUNT_ENAME2_3_2
        , :ACCOUNT_TYPE3_2
    FROM DUAL;



    INSERT INTO BL_CUST_BANK
    (
          CUST_ID
        , BANK_CNAME
        , BANK_ENAME
        , BRANCH_CNAME
        , BRANCH_ENAME
        , ACCOUNT_CNAME
        , ACCOUNT
        , BANK_C_ADDRESS
        , BANK_E_ADDRESS
        , SWIFT_CODE
        , CREATION_DATE
        , LAST_UPDATE_DATE
        , BANK_ID
        , ACCOUNT_ENAME

        , BANK_CNAME2
        , BANK_ENAME2
        , BRANCH_CNAME2
        , BRANCH_ENAME2
        , ACCOUNT_CNAME2
        , ACCOUNT2
        , BANK_C_ADDRESS2
        , BANK_E_ADDRESS2
        , SWIFT_CODE2
        , CREATED_BY
        , LAST_UPDATED_BY
        , CURRENCY
        , ACCOUNT_ENAME2
    )
    SELECT
          :CUST_ID
        , :BANK_CNAME_4
        , :BANK_ENAME_4
        , :BRANCH_CNAME_4
        , :BRANCH_ENAME_4
        , :ACCOUNT_CNAME_4
        , :ACCOUNT_4
        , :BANK_C_ADDRESS_4
        , :BANK_E_ADDRESS_4
        , :SWIFT_CODE_4
        , SYSDATE
        , SYSDATE
        ,  :BANK_ID_4
        , :ACCOUNT_ENAME_4

        , :BANK_CNAME2_4
        , :BANK_ENAME2_4
        , :BRANCH_CNAME2_4
        , :BRANCH_ENAME2_4
        , :ACCOUNT_CNAME2_4
        , :ACCOUNT2_4
        , :BANK_C_ADDRESS2_4
        , :BANK_E_ADDRESS2_4
        , :SWIFT_CODE2_4
        , :USER_NAME
        , :USER_NAME
        , :CURRENCY_4
        , :ACCOUNT_ENAME2_4
    FROM DUAL;

    INSERT INTO BL_CUST_BANK
    (
          CUST_ID
        , BANK_CNAME
        , BANK_ENAME
        , BRANCH_CNAME
        , BRANCH_ENAME
        , ACCOUNT_CNAME
        , ACCOUNT
        , BANK_C_ADDRESS
        , BANK_E_ADDRESS
        , SWIFT_CODE
        , CREATION_DATE
        , LAST_UPDATE_DATE
        , BANK_ID
        , ACCOUNT_ENAME

        , BANK_CNAME2
        , BANK_ENAME2
        , BRANCH_CNAME2
        , BRANCH_ENAME2
        , ACCOUNT_CNAME2
        , ACCOUNT2
        , BANK_C_ADDRESS2
        , BANK_E_ADDRESS2
        , SWIFT_CODE2
        , CREATED_BY
        , LAST_UPDATED_BY
        , CURRENCY
        , ACCOUNT_ENAME2
    )
    SELECT
          :CUST_ID
        , :BANK_CNAME_5
        , :BANK_ENAME_5
        , :BRANCH_CNAME_5
        , :BRANCH_ENAME_5
        , :ACCOUNT_CNAME_5
        , :ACCOUNT_5
        , :BANK_C_ADDRESS_5
        , :BANK_E_ADDRESS_5
        , :SWIFT_CODE_5
        , SYSDATE
        , SYSDATE
        ,  :BANK_ID_5
        , :ACCOUNT_ENAME_5

        , :BANK_CNAME2_5
        , :BANK_ENAME2_5
        , :BRANCH_CNAME2_5
        , :BRANCH_ENAME2_5
        , :ACCOUNT_CNAME2_5
        , :ACCOUNT2_5
        , :BANK_C_ADDRESS2_5
        , :BANK_E_ADDRESS2_5
        , :SWIFT_CODE2_5
        , :USER_NAME
        , :USER_NAME
        , :CURRENCY_5
        , :ACCOUNT_ENAME2_5
    FROM DUAL;

    INSERT INTO BL_CUST_BANK
    (
          CUST_ID
        , BANK_CNAME
        , BANK_ENAME
        , BRANCH_CNAME
        , BRANCH_ENAME
        , ACCOUNT_CNAME
        , ACCOUNT
        , BANK_C_ADDRESS
        , BANK_E_ADDRESS
        , SWIFT_CODE
        , CREATION_DATE
        , LAST_UPDATE_DATE
        , BANK_ID
        , ACCOUNT_ENAME

        , BANK_CNAME2
        , BANK_ENAME2
        , BRANCH_CNAME2
        , BRANCH_ENAME2
        , ACCOUNT_CNAME2
        , ACCOUNT2
        , BANK_C_ADDRESS2
        , BANK_E_ADDRESS2
        , SWIFT_CODE2
        , CREATED_BY
        , LAST_UPDATED_BY
        , CURRENCY
        , ACCOUNT_ENAME2
    )
    SELECT
          :CUST_ID
        , :BANK_CNAME_6
        , :BANK_ENAME_6
        , :BRANCH_CNAME_6
        , :BRANCH_ENAME_6
        , :ACCOUNT_CNAME_6
        , :ACCOUNT_6
        , :BANK_C_ADDRESS_6
        , :BANK_E_ADDRESS_6
        , :SWIFT_CODE_6
        , SYSDATE
        , SYSDATE
        ,  :BANK_ID_6
        , :ACCOUNT_ENAME_6

        , :BANK_CNAME2_6
        , :BANK_ENAME2_6
        , :BRANCH_CNAME2_6
        , :BRANCH_ENAME2_6
        , :ACCOUNT_CNAME2_6
        , :ACCOUNT2_6
        , :BANK_C_ADDRESS2_6
        , :BANK_E_ADDRESS2_6
        , :SWIFT_CODE2_6
        , :USER_NAME
        , :USER_NAME
        , :CURRENCY_6
        , :ACCOUNT_ENAME2_6
    FROM DUAL;

    INSERT INTO BL_CUST_BANK
    (
          CUST_ID
        , BANK_CNAME
        , BANK_ENAME
        , BRANCH_CNAME
        , BRANCH_ENAME
        , ACCOUNT_CNAME
        , ACCOUNT
        , BANK_C_ADDRESS
        , BANK_E_ADDRESS
        , SWIFT_CODE
        , CREATION_DATE
        , LAST_UPDATE_DATE
        , BANK_ID
        , ACCOUNT_ENAME

        , BANK_CNAME2
        , BANK_ENAME2
        , BRANCH_CNAME2
        , BRANCH_ENAME2
        , ACCOUNT_CNAME2
        , ACCOUNT2
        , BANK_C_ADDRESS2
        , BANK_E_ADDRESS2
        , SWIFT_CODE2
        , CREATED_BY
        , LAST_UPDATED_BY
        , CURRENCY
        , ACCOUNT_ENAME2
    )
    SELECT
          :CUST_ID
        , :BANK_CNAME_7
        , :BANK_ENAME_7
        , :BRANCH_CNAME_7
        , :BRANCH_ENAME_7
        , :ACCOUNT_CNAME_7
        , :ACCOUNT_7
        , :BANK_C_ADDRESS_7
        , :BANK_E_ADDRESS_7
        , :SWIFT_CODE_7
        , SYSDATE
        , SYSDATE
        ,  :BANK_ID_7
        , :ACCOUNT_ENAME_7

        , :BANK_CNAME2_7
        , :BANK_ENAME2_7
        , :BRANCH_CNAME2_7
        , :BRANCH_ENAME2_7
        , :ACCOUNT_CNAME2_7
        , :ACCOUNT2_7
        , :BANK_C_ADDRESS2_7
        , :BANK_E_ADDRESS2_7
        , :SWIFT_CODE2_7
        , :USER_NAME
        , :USER_NAME
        , :CURRENCY_7
        , :ACCOUNT_ENAME2_7
    FROM DUAL;
END ;
";
                base.ExecuteScalar(strSQL, new
                {
                    CUST_ID = strCustID,
                    CUST_CNAME = dataCust.CUST_CNAME,
                    CUST_ENAME = dataCust.CUST_ENAME,
                    CUST_CNAME2 = dataCust.CUST_CNAME2,
                    CUST_ENAME2 = dataCust.CUST_ENAME2,
                    ID_NUMBER = dataCust.ID_NUMBER,
                    ID_NUMBER2 = dataCust.ID_NUMBER2,
                    PASSPORT = dataCust.PASSPORT,
                    PASSPORT2 = dataCust.PASSPORT2,
                    PHONE_1 = dataCust.PHONE_1,
                    PHONE_2 = dataCust.PHONE_2,
                    EMAIL_1 = dataCust.EMAIL_1,
                    EMAIL_2 = dataCust.EMAIL_2,
                    C_ADDRESS = dataCust.C_ADDRESS,
                    C_ADDRESS2 = dataCust.C_ADDRESS2,
                    SEX = dataCust.SEX,
                    SEX2 = dataCust.SEX2,
                    DATE_OF_BIRTH = dataCust.DATE_OF_BIRTH,
                    DATE_OF_BIRTH2 = dataCust.DATE_OF_BIRTH2,
                    MAIL_TYPE_MONTHLY = dataCust.MAIL_TYPE_MONTHLY,
                    MAIL_TYPE_END = dataCust.MAIL_TYPE_END,
                    PASSPORT_REGION = dataCust.PASSPORT_REGION,
                    PASSPORT_REGION2 = dataCust.PASSPORT_REGION2,
                    POSTAL_CODE = dataCust.POSTAL_CODE,
                    POSTAL_CODE2 = dataCust.POSTAL_CODE2,
                    IS_ONLINE_WITHDRAWAL = dataCust.IS_ONLINE_WITHDRAWAL,
                    ORG_CODE_CUST = dataCust.ORG_CODE,

                    BANK_CNAME = dataBank.BANK_CNAME,
                    BANK_ENAME = dataBank.BANK_ENAME,
                    BRANCH_CNAME = dataBank.BRANCH_CNAME,
                    BRANCH_ENAME = dataBank.BRANCH_ENAME,
                    ACCOUNT_CNAME = dataBank.ACCOUNT_CNAME,
                    ACCOUNT = dataBank.ACCOUNT,
                    BANK_C_ADDRESS = dataBank.BANK_C_ADDRESS,
                    BANK_E_ADDRESS = dataBank.BANK_E_ADDRESS,
                    SWIFT_CODE = dataBank.SWIFT_CODE,
                    BANK_ID = strBankID,
                    ACCOUNT_ENAME = dataBank.ACCOUNT_ENAME,

                    BANK_CNAME2 = dataBank.BANK_CNAME2,
                    BANK_ENAME2 = dataBank.BANK_ENAME2,
                    BRANCH_CNAME2 = dataBank.BRANCH_CNAME2,
                    BRANCH_ENAME2 = dataBank.BRANCH_ENAME2,
                    ACCOUNT_CNAME2 = dataBank.ACCOUNT_CNAME2,
                    ACCOUNT2 = dataBank.ACCOUNT2,
                    BANK_C_ADDRESS2 = dataBank.BANK_C_ADDRESS2,
                    BANK_E_ADDRESS2 = dataBank.BANK_E_ADDRESS2,
                    SWIFT_CODE2 = dataBank.SWIFT_CODE2,
                    CURRENCY = dataBank.CURRENCY,
                    ACCOUNT_ENAME2 = dataBank.ACCOUNT_ENAME2,

                    BANK_CNAME_2 = dataBank2.BANK_CNAME,
                    BANK_ENAME_2 = dataBank2.BANK_ENAME,
                    BRANCH_CNAME_2 = dataBank2.BRANCH_CNAME,
                    BRANCH_ENAME_2 = dataBank2.BRANCH_ENAME,
                    ACCOUNT_CNAME_2 = dataBank2.ACCOUNT_CNAME,
                    ACCOUNT_2 = dataBank2.ACCOUNT,
                    BANK_C_ADDRESS_2 = dataBank2.BANK_C_ADDRESS,
                    BANK_E_ADDRESS_2 = dataBank2.BANK_E_ADDRESS,
                    SWIFT_CODE_2 = dataBank2.SWIFT_CODE,
                    BANK_ID_2 = strBankID2,
                    ACCOUNT_ENAME_2 = dataBank2.ACCOUNT_ENAME,

                    BANK_CNAME2_2 = dataBank2.BANK_CNAME2,
                    BANK_ENAME2_2 = dataBank2.BANK_ENAME2,
                    BRANCH_CNAME2_2 = dataBank2.BRANCH_CNAME2,
                    BRANCH_ENAME2_2 = dataBank2.BRANCH_ENAME2,
                    ACCOUNT_CNAME2_2 = dataBank2.ACCOUNT_CNAME2,
                    ACCOUNT2_2 = dataBank2.ACCOUNT2,
                    BANK_C_ADDRESS2_2 = dataBank2.BANK_C_ADDRESS2,
                    BANK_E_ADDRESS2_2 = dataBank2.BANK_E_ADDRESS2,
                    SWIFT_CODE2_2 = dataBank2.SWIFT_CODE2,
                    CURRENCY_2 = dataBank2.CURRENCY,
                    ACCOUNT_ENAME2_2 = dataBank2.ACCOUNT_ENAME2,



                    BANK_CNAME_3 = dataBank3.BANK_CNAME,
                    BANK_ENAME_3 = dataBank3.BANK_ENAME,
                    BRANCH_CNAME_3 = dataBank3.BRANCH_CNAME,
                    BRANCH_ENAME_3 = dataBank3.BRANCH_ENAME,
                    ACCOUNT_CNAME_3 = dataBank3.ACCOUNT_CNAME,
                    ACCOUNT_3 = dataBank3.ACCOUNT,
                    BANK_C_ADDRESS_3 = dataBank3.BANK_C_ADDRESS,
                    BANK_E_ADDRESS_3 = dataBank3.BANK_E_ADDRESS,
                    SWIFT_CODE_3 = dataBank3.SWIFT_CODE,
                    BANK_ID_3 = strBankID3,
                    ACCOUNT_ENAME_3 = dataBank3.ACCOUNT_ENAME,

                    BANK_CNAME2_3 = dataBank3.BANK_CNAME2,
                    BANK_ENAME2_3 = dataBank3.BANK_ENAME2,
                    BRANCH_CNAME2_3 = dataBank3.BRANCH_CNAME2,
                    BRANCH_ENAME2_3 = dataBank3.BRANCH_ENAME2,
                    ACCOUNT_CNAME2_3 = dataBank3.ACCOUNT_CNAME2,
                    ACCOUNT2_3 = dataBank3.ACCOUNT2,
                    BANK_C_ADDRESS2_3 = dataBank3.BANK_C_ADDRESS2,
                    BANK_E_ADDRESS2_3 = dataBank3.BANK_E_ADDRESS2,
                    SWIFT_CODE2_3 = dataBank3.SWIFT_CODE2,
                    CURRENCY_3 = dataBank3.CURRENCY,
                    ACCOUNT_ENAME2_3 = dataBank3.ACCOUNT_ENAME2,
                    ACCOUNT_TYPE3 = dataBank3.ACCOUNT_TYPE,

                    BANK_CNAME_3_2 = dataBank3_2.BANK_CNAME,
                    BANK_ENAME_3_2 = dataBank3_2.BANK_ENAME,
                    BRANCH_CNAME_3_2 = dataBank3_2.BRANCH_CNAME,
                    BRANCH_ENAME_3_2 = dataBank3_2.BRANCH_ENAME,
                    ACCOUNT_CNAME_3_2 = dataBank3_2.ACCOUNT_CNAME,
                    ACCOUNT_3_2 = dataBank3_2.ACCOUNT,
                    BANK_C_ADDRESS_3_2 = dataBank3_2.BANK_C_ADDRESS,
                    BANK_E_ADDRESS_3_2 = dataBank3_2.BANK_E_ADDRESS,
                    SWIFT_CODE_3_2 = dataBank3_2.SWIFT_CODE,
                    BANK_ID_3_2 = strBankID3_2,
                    ACCOUNT_ENAME_3_2 = dataBank3_2.ACCOUNT_ENAME,

                    BANK_CNAME2_3_2 = dataBank3_2.BANK_CNAME2,
                    BANK_ENAME2_3_2 = dataBank3_2.BANK_ENAME2,
                    BRANCH_CNAME2_3_2 = dataBank3_2.BRANCH_CNAME2,
                    BRANCH_ENAME2_3_2 = dataBank3_2.BRANCH_ENAME2,
                    ACCOUNT_CNAME2_3_2 = dataBank3_2.ACCOUNT_CNAME2,
                    ACCOUNT2_3_2 = dataBank3_2.ACCOUNT2,
                    BANK_C_ADDRESS2_3_2 = dataBank3_2.BANK_C_ADDRESS2,
                    BANK_E_ADDRESS2_3_2 = dataBank3_2.BANK_E_ADDRESS2,
                    SWIFT_CODE2_3_2 = dataBank3_2.SWIFT_CODE2,
                    CURRENCY_3_2 = dataBank3_2.CURRENCY,
                    ACCOUNT_ENAME2_3_2 = dataBank3_2.ACCOUNT_ENAME2,
                    ACCOUNT_TYPE3_2 = dataBank3_2.ACCOUNT_TYPE,

                    BANK_CNAME_4 = dataBank4.BANK_CNAME,
                    BANK_ENAME_4 = dataBank4.BANK_ENAME,
                    BRANCH_CNAME_4 = dataBank4.BRANCH_CNAME,
                    BRANCH_ENAME_4 = dataBank4.BRANCH_ENAME,
                    ACCOUNT_CNAME_4 = dataBank4.ACCOUNT_CNAME,
                    ACCOUNT_4 = dataBank4.ACCOUNT,
                    BANK_C_ADDRESS_4 = dataBank4.BANK_C_ADDRESS,
                    BANK_E_ADDRESS_4 = dataBank4.BANK_E_ADDRESS,
                    SWIFT_CODE_4 = dataBank4.SWIFT_CODE,
                    BANK_ID_4 = strBankID4,
                    ACCOUNT_ENAME_4 = dataBank4.ACCOUNT_ENAME,

                    BANK_CNAME2_4 = dataBank4.BANK_CNAME2,
                    BANK_ENAME2_4 = dataBank4.BANK_ENAME2,
                    BRANCH_CNAME2_4 = dataBank4.BRANCH_CNAME2,
                    BRANCH_ENAME2_4 = dataBank4.BRANCH_ENAME2,
                    ACCOUNT_CNAME2_4 = dataBank4.ACCOUNT_CNAME2,
                    ACCOUNT2_4 = dataBank4.ACCOUNT2,
                    BANK_C_ADDRESS2_4 = dataBank4.BANK_C_ADDRESS2,
                    BANK_E_ADDRESS2_4 = dataBank4.BANK_E_ADDRESS2,
                    SWIFT_CODE2_4 = dataBank4.SWIFT_CODE2,
                    CURRENCY_4 = dataBank4.CURRENCY,
                    ACCOUNT_ENAME2_4 = dataBank4.ACCOUNT_ENAME2,


                    BANK_CNAME_5 = dataBank5.BANK_CNAME,
                    BANK_ENAME_5 = dataBank5.BANK_ENAME,
                    BRANCH_CNAME_5 = dataBank5.BRANCH_CNAME,
                    BRANCH_ENAME_5 = dataBank5.BRANCH_ENAME,
                    ACCOUNT_CNAME_5 = dataBank5.ACCOUNT_CNAME,
                    ACCOUNT_5 = dataBank5.ACCOUNT,
                    BANK_C_ADDRESS_5 = dataBank5.BANK_C_ADDRESS,
                    BANK_E_ADDRESS_5 = dataBank5.BANK_E_ADDRESS,
                    SWIFT_CODE_5 = dataBank5.SWIFT_CODE,
                    BANK_ID_5 = strBankID5,
                    ACCOUNT_ENAME_5 = dataBank5.ACCOUNT_ENAME,

                    BANK_CNAME2_5 = dataBank5.BANK_CNAME2,
                    BANK_ENAME2_5 = dataBank5.BANK_ENAME2,
                    BRANCH_CNAME2_5 = dataBank5.BRANCH_CNAME2,
                    BRANCH_ENAME2_5 = dataBank5.BRANCH_ENAME2,
                    ACCOUNT_CNAME2_5 = dataBank5.ACCOUNT_CNAME2,
                    ACCOUNT2_5 = dataBank5.ACCOUNT2,
                    BANK_C_ADDRESS2_5 = dataBank5.BANK_C_ADDRESS2,
                    BANK_E_ADDRESS2_5 = dataBank5.BANK_E_ADDRESS2,
                    SWIFT_CODE2_5 = dataBank5.SWIFT_CODE2,
                    CURRENCY_5 = dataBank5.CURRENCY,
                    ACCOUNT_ENAME2_5 = dataBank5.ACCOUNT_ENAME2,


                    BANK_CNAME_6 = dataBank6.BANK_CNAME,
                    BANK_ENAME_6 = dataBank6.BANK_ENAME,
                    BRANCH_CNAME_6 = dataBank6.BRANCH_CNAME,
                    BRANCH_ENAME_6 = dataBank6.BRANCH_ENAME,
                    ACCOUNT_CNAME_6 = dataBank6.ACCOUNT_CNAME,
                    ACCOUNT_6 = dataBank6.ACCOUNT,
                    BANK_C_ADDRESS_6 = dataBank6.BANK_C_ADDRESS,
                    BANK_E_ADDRESS_6 = dataBank6.BANK_E_ADDRESS,
                    SWIFT_CODE_6 = dataBank6.SWIFT_CODE,
                    BANK_ID_6 = strBankID6,
                    ACCOUNT_ENAME_6 = dataBank6.ACCOUNT_ENAME,

                    BANK_CNAME2_6 = dataBank6.BANK_CNAME2,
                    BANK_ENAME2_6 = dataBank6.BANK_ENAME2,
                    BRANCH_CNAME2_6 = dataBank6.BRANCH_CNAME2,
                    BRANCH_ENAME2_6 = dataBank6.BRANCH_ENAME2,
                    ACCOUNT_CNAME2_6 = dataBank6.ACCOUNT_CNAME2,
                    ACCOUNT2_6 = dataBank6.ACCOUNT2,
                    BANK_C_ADDRESS2_6 = dataBank6.BANK_C_ADDRESS2,
                    BANK_E_ADDRESS2_6 = dataBank6.BANK_E_ADDRESS2,
                    SWIFT_CODE2_6 = dataBank6.SWIFT_CODE2,
                    CURRENCY_6 = dataBank6.CURRENCY,
                    ACCOUNT_ENAME2_6 = dataBank6.ACCOUNT_ENAME2,


                    BANK_CNAME_7 = dataBank7.BANK_CNAME,
                    BANK_ENAME_7 = dataBank7.BANK_ENAME,
                    BRANCH_CNAME_7 = dataBank7.BRANCH_CNAME,
                    BRANCH_ENAME_7 = dataBank7.BRANCH_ENAME,
                    ACCOUNT_CNAME_7 = dataBank7.ACCOUNT_CNAME,
                    ACCOUNT_7 = dataBank7.ACCOUNT,
                    BANK_C_ADDRESS_7 = dataBank7.BANK_C_ADDRESS,
                    BANK_E_ADDRESS_7 = dataBank7.BANK_E_ADDRESS,
                    SWIFT_CODE_7 = dataBank7.SWIFT_CODE,
                    BANK_ID_7 = strBankID7,
                    ACCOUNT_ENAME_7 = dataBank7.ACCOUNT_ENAME,

                    BANK_CNAME2_7 = dataBank7.BANK_CNAME2,
                    BANK_ENAME2_7 = dataBank7.BANK_ENAME2,
                    BRANCH_CNAME2_7 = dataBank7.BRANCH_CNAME2,
                    BRANCH_ENAME2_7 = dataBank7.BRANCH_ENAME2,
                    ACCOUNT_CNAME2_7 = dataBank7.ACCOUNT_CNAME2,
                    ACCOUNT2_7 = dataBank7.ACCOUNT2,
                    BANK_C_ADDRESS2_7 = dataBank7.BANK_C_ADDRESS2,
                    BANK_E_ADDRESS2_7 = dataBank7.BANK_E_ADDRESS2,
                    SWIFT_CODE2_7 = dataBank7.SWIFT_CODE2,
                    CURRENCY_7 = dataBank7.CURRENCY,
                    ACCOUNT_ENAME2_7 = dataBank7.ACCOUNT_ENAME2,


                    USER_NAME = strUserName
                }, CommandType.Text);

            }
            catch (Exception ex)
            {
                exError = ex;
            }
        }
        public List<BL_CUST> GetCustComboData_active(string yyyymm,out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
SELECT 
    A.CUST_ID,
   A.CUST_ENAME CUST_ENAME,
 A.CUST_ENAME2 AS CUST_ENAME2,
  A.CUST_CNAME AS CUST_CNAME,
    A.CUST_CNAME2 AS CUST_CNAME2,    
    A.EMAIL_1 as EMAIL_1,
  A.EMAIL_2  as EMAIL_2,
   A.EMAIL2_1   as EMAIL2_1 ,
    A.EMAIL2_2 as EMAIL2_2,
    A.PHONE_1  as PHONE_1,
    A.PHONE_2 as PHONE_2,
   A.PHONE2_1 as PHONE2_1,
  A.PHONE2_2 as PHONE2_2,
    D.CNAME AS SALES_NAME,
    D.IB_CODE,

 A.ID_NUMBER AS ID_NUMBER,
  A.ID_NUMBER2 AS ID_NUMBER2,
    A.UNION_ID_NUMBER
    , A.MAIL_TYPE_END
FROM BL_CUST_v2  A
    LEFT JOIN
    (
        SELECT X.CUST_ID , COUNT(1) AS ORDER_CNT
        FROM BL_ORDER X
        WHERE X.STATUS_CODE  IN ('1','3','4')  AND :TXN_DATE  BETWEEN TO_CHAR(x.FROM_DATE,'YYYY/MM') AND NVL(TO_CHAR(x.EXTEND_DATE,'YYYY/MM'),TO_CHAR(x.END_DATE,'YYYY/MM') )
        GROUP BY X.CUST_ID
    )B
        ON A.CUST_ID = B.CUST_ID
    LEFT JOIN BL_CUST_SALES C
        ON A.CUST_ID = C.CUST_ID
            AND (C.ACTIVE IS NULL OR C.ACTIVE = 'Y' )   --AND C.ACTIVE <> 'N'
    LEFT JOIN BL_EMP D
        ON C.IB_CODE = D.IB_CODE
WHERE 1 = 1
ORDER BY CUST_CNAME

";
                return base.GetList(strSQL, new
                {
                    TXN_DATE = yyyymm
                }, CommandType.Text).ToList();

                //return base.GetList(strSQL, null, CommandType.Text).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }
        public List<BL_CUST> GetCustComboData(string yyyymm,string strORG, out Exception exError)
        {
           exError = null;
            try
            {
                var strSQL = @"
SELECT 
    A.CUST_ID,
   A.CUST_ENAME CUST_ENAME,
 A.CUST_ENAME2 AS CUST_ENAME2,
  A.CUST_CNAME AS CUST_CNAME,
    A.CUST_CNAME2 AS CUST_CNAME2,    
    A.EMAIL_1 as EMAIL_1,
  A.EMAIL_2  as EMAIL_2,
   A.EMAIL2_1   as EMAIL2_1 ,
    A.EMAIL2_2 as EMAIL2_2,
    A.PHONE_1  as PHONE_1,
    A.PHONE_2 as PHONE_2,
   A.PHONE2_1 as PHONE2_1,
  A.PHONE2_2 as PHONE2_2,
    D.CNAME AS SALES_NAME,
    D.IB_CODE,
 A.ID_NUMBER AS ID_NUMBER,
  A.ID_NUMBER2 AS ID_NUMBER2,
    A.UNION_ID_NUMBER
    , A.MAIL_TYPE_END
FROM BL_CUST_V2  A
    LEFT JOIN
    (
        SELECT X.CUST_ID , COUNT(1) AS ORDER_CNT
        FROM BL_ORDER X
        WHERE X.STATUS_CODE  IN ('1','3','4') -- AND :TXN_DATE BETWEEN TO_CHAR(x.FROM_DATE,'YYYY/MM') AND NVL(TO_CHAR(x.EXTEND_DATE,'YYYY/MM'),TO_CHAR(x.END_DATE,'YYYY/MM') )
        GROUP BY X.CUST_ID
    )B
        ON A.CUST_ID = B.CUST_ID
    LEFT JOIN BL_CUST_SALES C
        ON A.CUST_ID = C.CUST_ID
            AND (C.ACTIVE IS NULL OR C.ACTIVE = 'Y' )   --AND C.ACTIVE <> 'N'
    LEFT JOIN BL_EMP D
        ON C.IB_CODE = D.IB_CODE
WHERE 1 = 1 --and cust_cname like   '鄭國%'
  AND A.CUST_ID IN 
        (
            SELECT CUST_ID
            FROM BL_ORDER_V1
            WHERE :TXN_DATE BETWEEN 
                    TO_CHAR(FROM_DATE,'YYYY/MM') AND 
                    NVL(TO_CHAR(ADD_MONTHS(EXTEND_DATE,-1)   ,'YYYY/MM'),TO_CHAR( ADD_MONTHS(END_DATE,-1) ,'YYYY/MM') )  
                    --NVL (TO_CHAR(ADD_MONTHS(ORDER_END,-1),'YYYY/MM'), TO_CHAR(SYSDATE,'YYYY/MM')) >= :TXN_DATE
        )
";
                if (strORG.ConvertToString() != string.Empty)
                {
                    switch (strORG)
                    {
                        case "TW1":
                            strSQL += @"
    AND A.ORG_CODE IN ('TW1', 'IND')
";
                            break;
                        default:
                            strSQL += @"
    AND A.ORG_CODE = '" + strORG + @"'
";
                            break;
                    }
                }

                    strSQL += @"

ORDER BY CUST_CNAME
";
                //                var strSQL = @"
                ////SELECT 
                ////    A.CUST_ID,
                ////   A.CUST_ENAME CUST_ENAME,
                //// A.CUST_ENAME2 AS CUST_ENAME2,
                ////  A.CUST_CNAME AS CUST_CNAME,
                ////    A.CUST_CNAME2 AS CUST_CNAME2,    
                ////    A.EMAIL_1 as EMAIL_1,
                ////  A.EMAIL_2  as EMAIL_2,
                ////   A.EMAIL2_1   as EMAIL2_1 ,
                ////    A.EMAIL2_2 as EMAIL2_2,
                ////    A.PHONE_1  as PHONE_1,
                ////    A.PHONE_2 as PHONE_2,
                ////   A.PHONE2_1 as PHONE2_1,
                ////  A.PHONE2_2 as PHONE2_2,
                ////    D.CNAME AS SALES_NAME,
                ////    D.IB_CODE,

                //// A.ID_NUMBER AS ID_NUMBER,
                ////  A.ID_NUMBER2 AS ID_NUMBER2,
                ////    A.UNION_ID_NUMBER
                ////    , A.MAIL_TYPE_END
                ////FROM BL_CUST_v1  A
                ////    LEFT JOIN
                ////    (
                ////        SELECT X.CUST_ID , COUNT(1) AS ORDER_CNT
                ////        FROM BL_ORDER X
                ////        WHERE X.STATUS_CODE  IN ('1','3','4') -- AND :TXN_DATE BETWEEN TO_CHAR(x.FROM_DATE,'YYYY/MM') AND NVL(TO_CHAR(x.EXTEND_DATE,'YYYY/MM'),TO_CHAR(x.END_DATE,'YYYY/MM') )
                ////        GROUP BY X.CUST_ID
                ////    )B
                ////        ON A.CUST_ID = B.CUST_ID
                ////    LEFT JOIN BL_CUST_SALES C
                ////        ON A.CUST_ID = C.CUST_ID
                ////            AND (C.ACTIVE IS NULL OR C.ACTIVE = 'Y' )   --AND C.ACTIVE <> 'N'
                ////    LEFT JOIN BL_EMP D
                ////        ON C.IB_CODE = D.IB_CODE
                ////WHERE 1 = 1
                ////ORDER BY CUST_CNAME

                //";
                //return base.GetList(strSQL, null, CommandType.Text).ToList();
                return base.GetList(strSQL, new
                {
                    TXN_DATE = yyyymm
                }, CommandType.Text).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }


        public List<BL_CUST> GetCustComboData_All(string strORG, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
                SELECT 
                    A.CUST_ID,
                   A.CUST_ENAME CUST_ENAME,
                 A.CUST_ENAME2 AS CUST_ENAME2,
                  A.CUST_CNAME AS CUST_CNAME,
                    A.CUST_CNAME2 AS CUST_CNAME2,    
                    A.EMAIL_1 as EMAIL_1,
                  A.EMAIL_2  as EMAIL_2,
                   A.EMAIL2_1   as EMAIL2_1 ,
                    A.EMAIL2_2 as EMAIL2_2,
                    A.PHONE_1  as PHONE_1,
                    A.PHONE_2 as PHONE_2,
                   A.PHONE2_1 as PHONE2_1,
                  A.PHONE2_2 as PHONE2_2,
                    D.CNAME AS SALES_NAME,
                    D.IB_CODE,

                 A.ID_NUMBER AS ID_NUMBER,
                  A.ID_NUMBER2 AS ID_NUMBER2,
                    A.UNION_ID_NUMBER
                    , A.MAIL_TYPE_END
                FROM BL_CUST_v1  A
                    LEFT JOIN
                    (
                        SELECT X.CUST_ID , COUNT(1) AS ORDER_CNT
                        FROM BL_ORDER X
                        WHERE X.STATUS_CODE  IN ('1','3','4') -- AND :TXN_DATE BETWEEN TO_CHAR(x.FROM_DATE,'YYYY/MM') AND NVL(TO_CHAR(x.EXTEND_DATE,'YYYY/MM'),TO_CHAR(x.END_DATE,'YYYY/MM') )
                        GROUP BY X.CUST_ID
                    )B
                        ON A.CUST_ID = B.CUST_ID
                    LEFT JOIN BL_CUST_SALES C
                        ON A.CUST_ID = C.CUST_ID
                            AND (C.ACTIVE IS NULL OR C.ACTIVE = 'Y' )   --AND C.ACTIVE <> 'N'
                    LEFT JOIN BL_EMP D
                        ON C.IB_CODE = D.IB_CODE
                WHERE 1 = 1
                ";

                if (strORG.ConvertToString() != string.Empty)
                {
                    switch (strORG)
                    {
                        case "TW1":
                            strSQL += @"
    AND A.ORG_CODE IN ('TW1', 'IND')
";
                            break;
                        default:
                            strSQL += @"
    AND A.ORG_CODE = '" + strORG + @"'
";
                            break;
                    }
                }


                strSQL += @"

                ORDER BY CUST_CNAME
";
                return base.GetList(strSQL, null, CommandType.Text).ToList();
                //return base.GetList(strSQL, new
                //{
                //    TXN_DATE = yyyymm
                //}, CommandType.Text).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }



        public bool UpdateWebPwd(List<BL_CUST> data, out Exception exError)
        {
            exError = null;
            try
            {
                var param = new DynamicParameters();
                var strSQL = string.Empty;
                int i = 0;
                data.ForEach(x =>
                {
                    i += 1;
                    strSQL += @"
UPDATE BL_CUST A
    SET
        A.WEB_PASSWORD = :WEB_PASSWORD" + i.ConvertToString() + @"
        , A.IS_NEED_CHANGE_PWD = 'Y'
WHERE A.CUST_ID = :CUST_ID" + i.ConvertToString() + @" 
;
";
                    param.Add("WEB_PASSWORD" + i.ConvertToString(), x.WEB_PASSWORD);
                    param.Add("CUST_ID" + i.ConvertToString(), x.CUST_ID);
                });

                if (strSQL != string.Empty)
                {
                    strSQL = " BEGIN " + strSQL + " END; ";
                    base.ExecuteScalar(strSQL, param, CommandType.Text);
                }
                return true;
            }
            catch (Exception ex)
            {
                exError = ex;
                return false;
            }
        }

        public bool UpdateWebGU_ID(List<BL_CUST> data, out Exception exError)
        {
            exError = null;
            try
            {
                var param = new DynamicParameters();
                var strSQL = string.Empty;
                int i = 0;
                data.ForEach(x =>
                {
                    i += 1;
                    strSQL += @"
UPDATE BL_CUST A
    SET
        A.IS_ONLINE_MAIL_GUID = :IS_ONLINE_MAIL_GUID" + i.ConvertToString() + @"
WHERE A.CUST_ID = :CUST_ID" + i.ConvertToString() + @" 
;
";
                    param.Add("IS_ONLINE_MAIL_GUID" + i.ConvertToString(), x.IS_ONLINE_MAIL_GUID);
                    param.Add("CUST_ID" + i.ConvertToString(), x.CUST_ID);
                });

                if (strSQL != string.Empty)
                {
                    strSQL = " BEGIN " + strSQL + " END; ";
                    base.ExecuteScalar(strSQL, param, CommandType.Text);
                }
                return true;
            }
            catch (Exception ex)
            {
                exError = ex;
                return false;
            }
        }


        public string GetNewUnionIdNumber(out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
SELECT 
    BL_INTEREST2_PKG.RETURN_NEW_UNION_ID_NUMBER AS UNION_ID_NUMBER
FROM DUAL
";
                var data = base.GetSingleOrDefault(strSQL, null, CommandType.Text);
                if (data != null)
                {
                    return data.UNION_ID_NUMBER.ConvertToString();
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }

        public bool UpdateUnionIdNumber(BL_CUST data, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
UPDATE BL_CUST A
SET
    A.UNION_ID_NUMBER = :UNION_ID_NUMBER
WHERE A.CUST_ID = :CUST_ID
";
                base.ExecuteScalar(strSQL, new
                {
                    CUST_ID = data.CUST_ID,
                    UNION_ID_NUMBER = data.UNION_ID_NUMBER
                }, CommandType.Text);

                return true;
            }
            catch (Exception ex)
            {
                exError = ex;
                return false;
            }
        }


    }
}
