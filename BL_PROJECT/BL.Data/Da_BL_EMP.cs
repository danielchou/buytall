﻿using MK.Demo.Model;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MK.Demo.Data
{

    public class Da_BL_EMP : DataRepository<BL_EMP>
    {
        public Da_BL_EMP(OracleConnection conn)
         : base(conn)
        { }
        public List<BL_EMP> SelectBL_EMP(string strORG_CODE, out Exception exError)
        {
            exError = null;
            List<BL_EMP> data = new List<BL_EMP>();
            try
            {
                string sql = @"
select * 
from  BL_EMP  
WHERE (ORG_CODE = :ORG_CODE OR :ORG_CODE IS NULL )
    ";
                data = base.GetList(sql, new
                {
                    ORG_CODE = strORG_CODE
                }, CommandType.Text).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
            }
            return data;
        }


        public string GetEmpCode(out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
SELECT 
    MAX(TO_NUMBER(EMP_CODE)) + 1 AS EMP_CODE
FROM BL_EMP 
WHERE EMP_CODE NOT LIKE 'IS%'
";
                var data = base.GetSingleOrDefault(strSQL, null, CommandType.Text);
                if (data != null)
                {
                    return data.EMP_CODE;
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception ex)
            {
                exError = ex;
                return string.Empty;
            }
        }

        public List<BL_EMP> SelectBL_EMP
            (
                string EmpCode,
                string EmpName,
                string IB_CODE,
                string ID_NO,
                DateTime? HIRE_DATE_S,
                DateTime? HIRE_DATE_E,
                DateTime? QUIT_DATE_S,
                DateTime? QUIT_DATE_E,
                string ORG_CODE,
                out Exception exError
            )
        {
            exError = null;
            try
            {
                var strSQL = @"
SELECT 
    A.* 
    , B.CNAME AS SUPERVISOR_NAME
    , C.DEPT_NAME AS DEPT_NAME
    , D.CNAME AS PARENT_SALES_NAME
    , E.CNAME AS PARENT_INTERNAL_SALES_NAME
FROM BL_EMP A
    LEFT JOIN BL_EMP B
        ON A.SUPERVISOR_ID = B.EMP_ID
    LEFT JOIN BL_DEPT C
        ON A.DS_DEPT = C.DEPT_CODE
    LEFT JOIN BL_EMP D
        ON A.PARENT_IB_CODE = D.IB_CODE
    LEFT JOIN BL_EMP E
        ON A.PARENT_INTERNAL_IB_CODE = E.INTERNAL_IB_CODE
WHERE 1 = 1
    AND ( UPPER(A.EMP_CODE) LIKE '%'||:EMP_CODE||'%' OR :EMP_CODE IS NULL )
    AND ( UPPER(A.CNAME) LIKE '%'||:EMP_NAME||'%' OR UPPER(A.ENAME) LIKE '%'||:EMP_NAME||'%' OR :EMP_NAME IS NULL )
    AND ( UPPER(A.IB_CODE) LIKE '%'||:IB_CODE||'%' OR :IB_CODE IS NULL )
    AND ( UPPER(A.ID_NUMBER) LIKE '%'||:ID_NUMBER||'%' OR :ID_NUMBER IS NULL )
    AND ( A.HIRE_START_DATE >= :HIRE_DATE_S OR :HIRE_DATE_S IS NULL )
    AND ( A.HIRE_START_DATE <= :HIRE_DATE_E OR :HIRE_DATE_E IS NULL )
    AND ( A.HIRE_END_DATE >= :QUIT_DATE_S OR :QUIT_DATE_S IS NULL )
    AND ( A.HIRE_END_DATE <= :QUIT_DATE_E OR :QUIT_DATE_E IS NULL )
    AND ( A.ORG_CODE = :ORG_CODE OR :ORG_CODE IS NULL )
";
                var data = base.GetList(strSQL, new
                {
                    EMP_CODE = EmpCode,
                    EMP_NAME = EmpName,
                    IB_CODE = IB_CODE,
                    ID_NUMBER = ID_NO,
                    HIRE_DATE_S = HIRE_DATE_S,
                    HIRE_DATE_E = HIRE_DATE_E,
                    QUIT_DATE_S = QUIT_DATE_S,
                    QUIT_DATE_E = QUIT_DATE_E,
                    ORG_CODE = ORG_CODE
                }, CommandType.Text).ToList();
                return data;
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }

        public BL_EMP SelectBL_EMPByEMP_ID
            (
                string EMP_ID,
                out Exception exError
            )
        {
            exError = null;
            try
            {
                var strSQL = @"
SELECT 
    A.* 
    , B.CNAME AS SUPERVISOR_NAME
    , C.DEPT_NAME AS DEPT_NAME
    , D.CNAME AS PARENT_SALES_NAME
    , E.CNAME AS PARENT_INTERNAL_SALES_NAME
FROM BL_EMP A
    LEFT JOIN BL_EMP B
        ON A.SUPERVISOR_ID = B.EMP_ID
    LEFT JOIN BL_DEPT C
        ON A.DS_DEPT = C.DEPT_CODE
    LEFT JOIN BL_EMP D
        ON A.PARENT_IB_CODE = D.IB_CODE
    LEFT JOIN BL_EMP E
        ON A.PARENT_INTERNAL_IB_CODE = E.INTERNAL_IB_CODE
WHERE 1 = 1
    AND A.EMP_ID = :EMP_ID
";
                var data = base.GetSingleOrDefault(strSQL, new
                {
                    EMP_ID = EMP_ID
                }, CommandType.Text);
                return data;
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }




        public bool AddNewEmp(BL_EMP dataSave, long EMP_ID, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
INSERT INTO BL_EMP
(
    EMP_ID,
    EMP_CODE,
    CNAME,
    ENAME,
    HIRE_START_DATE,
    HIRE_END_DATE,
    ID_NUMBER,
    DATE_OF_BIRTH,
    SEX,
    EMAIL,
    TITLE,
    SUPERVISOR_ID,
    CREATION_DATE,
    CREATED_BY,
    LAST_UPDATE_DATE,
    LAST_UPDATED_BY,
    IB_CODE,
    DS_DEPT,
    ROLE_NO,
    IS_USE_PROD,
    IS_USE_TEST,
    LOGIN_PASSWORD,
    SALES_LOGIN_PASSWORD,
    ORG_CODE,
    INTERNAL_IB_CODE,
    PARENT_IB_CODE,
    PARENT_INTERNAL_IB_CODE
)
SELECT 
    :EMP_ID,
    :EMP_CODE,
    :CNAME,
    :ENAME,
    :HIRE_START_DATE,
    :HIRE_END_DATE,
    :ID_NUMBER,
    :DATE_OF_BIRTH,
    :SEX,
    :EMAIL,
    :TITLE,
    :SUPERVISOR_ID,
    SYSDATE,
    :CREATED_BY,
    SYSDATE,
    :LAST_UPDATED_BY,
    :IB_CODE,
    :DEPT_VALUE,
    :ROLE_NO,
    :IS_USE_PROD,
    :IS_USE_TEST,
    :LOGIN_PASSWORD,
    :SALES_LOGIN_PASSWORD,
    :ORG_CODE,
    :INTERNAL_IB_CODE,
    :PARENT_IB_CODE,
    :PARENT_INTERNAL_IB_CODE
FROM DUAL
";
                base.ExecuteScalar(strSQL, new
                {
                    EMP_ID = EMP_ID,
                    EMP_CODE = dataSave.EMP_CODE,
                    CNAME = dataSave.CNAME,
                    ENAME = dataSave.ENAME,
                    HIRE_START_DATE = dataSave.HIRE_START_DATE,
                    HIRE_END_DATE = dataSave.HIRE_END_DATE,
                    ID_NUMBER = dataSave.ID_NUMBER,
                    DATE_OF_BIRTH = dataSave.DATE_OF_BIRTH,
                    SEX = dataSave.SEX,
                    EMAIL = dataSave.EMAIL,
                    TITLE = dataSave.TITLE,
                    SUPERVISOR_ID = dataSave.SUPERVISOR_ID,
                    CREATED_BY = dataSave.CREATED_BY,
                    LAST_UPDATED_BY = dataSave.LAST_UPDATED_BY,
                    IB_CODE = dataSave.IB_CODE,
                    DEPT_VALUE = dataSave.DEPT_VALUE,
                    ROLE_NO = dataSave.ROLE_NO,
                    IS_USE_PROD = dataSave.IS_USE_PROD,
                    IS_USE_TEST = dataSave.IS_USE_TEST,
                    LOGIN_PASSWORD = dataSave.LOGIN_PASSWORD,
                    SALES_LOGIN_PASSWORD = dataSave.SALES_LOGIN_PASSWORD,
                    ORG_CODE = dataSave.ORG_CODE,
                    INTERNAL_IB_CODE = dataSave.INTERNAL_IB_CODE,
                    PARENT_IB_CODE = dataSave.PARENT_IB_CODE,
                    PARENT_INTERNAL_IB_CODE = dataSave.PARENT_INTERNAL_IB_CODE
                }, CommandType.Text);

                return true;
            }
            catch (Exception ex)
            {
                exError = ex;
                return false;
            }
        }


        public bool UpdateEmp(BL_EMP dataSave, string EMP_ID, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
UPDATE BL_EMP
SET
    EMP_CODE = :EMP_CODE,
    CNAME = :CNAME,
    ENAME = :ENAME,
    HIRE_START_DATE = :HIRE_START_DATE,
    HIRE_END_DATE = :HIRE_END_DATE,
    ID_NUMBER = :ID_NUMBER,
    DATE_OF_BIRTH = :DATE_OF_BIRTH,
    SEX = :SEX,
    EMAIL = :EMAIL,
    TITLE = :TITLE,
    SUPERVISOR_ID = :SUPERVISOR_ID,
    LAST_UPDATE_DATE = SYSDATE,
    LAST_UPDATED_BY = :LAST_UPDATED_BY,
    IB_CODE = :IB_CODE,
    DS_DEPT = :DEPT_VALUE,
    ROLE_NO = :ROLE_NO,
    IS_USE_PROD = :IS_USE_PROD,
    IS_USE_TEST = :IS_USE_TEST,
    LOGIN_PASSWORD = :LOGIN_PASSWORD,
    SALES_LOGIN_PASSWORD = :SALES_LOGIN_PASSWORD,
    INTERNAL_IB_CODE = :INTERNAL_IB_CODE,
    PARENT_IB_CODE = :PARENT_IB_CODE,
    PARENT_INTERNAL_IB_CODE = :PARENT_INTERNAL_IB_CODE

WHERE EMP_ID = :EMP_ID
";
                base.ExecuteScalar(strSQL, new
                {
                    EMP_ID = EMP_ID,
                    EMP_CODE = dataSave.EMP_CODE,
                    CNAME = dataSave.CNAME,
                    ENAME = dataSave.ENAME,
                    HIRE_START_DATE = dataSave.HIRE_START_DATE,
                    HIRE_END_DATE = dataSave.HIRE_END_DATE,
                    ID_NUMBER = dataSave.ID_NUMBER,
                    DATE_OF_BIRTH = dataSave.DATE_OF_BIRTH,
                    SEX = dataSave.SEX,
                    EMAIL = dataSave.EMAIL,
                    TITLE = dataSave.TITLE,
                    SUPERVISOR_ID = dataSave.SUPERVISOR_ID,
                    CREATED_BY = dataSave.CREATED_BY,
                    LAST_UPDATED_BY = dataSave.LAST_UPDATED_BY,
                    IB_CODE = dataSave.IB_CODE,
                    DEPT_VALUE = dataSave.DEPT_VALUE,
                    ROLE_NO = dataSave.ROLE_NO,
                    IS_USE_PROD = dataSave.IS_USE_PROD,
                    IS_USE_TEST = dataSave.IS_USE_TEST,
                    LOGIN_PASSWORD = dataSave.LOGIN_PASSWORD,
                    SALES_LOGIN_PASSWORD = dataSave.SALES_LOGIN_PASSWORD,
                    INTERNAL_IB_CODE = dataSave.INTERNAL_IB_CODE,
                    PARENT_IB_CODE = dataSave.PARENT_IB_CODE,
                    PARENT_INTERNAL_IB_CODE = dataSave.PARENT_INTERNAL_IB_CODE
                }, CommandType.Text);

                return true;
            }
            catch (Exception ex)
            {
                exError = ex;
                return false;
            }
        }


        public bool DeleteBL_EMPByEMP_ID(string EMP_ID, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
DELETE BL_EMP
WHERE EMP_ID = :EMP_ID
";
                base.ExecuteScalar(strSQL, new
                {
                    EMP_ID = EMP_ID
                }, CommandType.Text);
                return true;
            }
            catch (Exception ex)
            {
                exError = ex;
                return false;
            }
        }

        public List<BL_EMP> SelectEmpByAccount(string AccountName, out Exception exError)
        {
            exError = null;
            try
            {
                var strSQL = @"
SELECT *
FROM br3.BL_EMP A
WHERE UPPER(A.ENAME) = UPPER('"+ AccountName + "') ";
                return base.GetList(strSQL,null, CommandType.Text).ToList();
            }
            catch (Exception ex)
            {
                exError = ex;
                return null;
            }
        }

    }
}
