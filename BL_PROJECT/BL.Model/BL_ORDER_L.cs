﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MK.Demo.Model
{
    public class BL_ORDER_L
    {
        public decimal? ORDER_L_ID { get; set; }
        public decimal? ORDER_ID { get; set; }
        public decimal? TEAM_NUM { get; set; }
        public string EMP_CODE { get; set; }
        public decimal? COMM_RATE { get; set; }
        public decimal? EXTRA_COMM_RATE { get; set; }
        public DateTime? CREATION_DATE { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? LAST_UPDATE_DATE { get; set; }
        public string LAST_UPDATED_BY { get; set; }
        public decimal? COMM_AMOUNT { get; set; }
    }




}
