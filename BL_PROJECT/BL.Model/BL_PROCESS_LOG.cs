﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MK.Demo.Model
{
    public class BL_PROCESS_LOG
    {
        public long BL_PROCESS_LOG_S1 { get; set; }
        public string CREATE_BY { get; set; }
        public DateTime? CREATE_DATE { get; set; }
        public string PROCESS_NAME { get; set; }
        public string PROCESS_DESC { get; set; }

    }
}
