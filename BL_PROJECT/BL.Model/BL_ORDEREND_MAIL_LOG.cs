﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MK.Demo.Utility;

namespace MK.Demo.Model
{
    public class BL_ORDEREND_MAIL_LOG
    {

        public string JOB_ID { get; set; }
        public decimal? CUST_ID { get; set; }
        public string ORDEREND_MONTH { get; set; }
        public string ORDERNO_DESC { get; set; }
        public string PDF_LOCATION { get; set; }
        public string JOB_STATUS { get; set; }
        public DateTime? JOB_DATE { get; set; }
        public DateTime? LAST_UPDATE_DATE { get; set; }
        public string LAST_UPDATED_BY { get; set; }
        public string EMAIL { get; set; }
        public string IS_TEST { get; set; }



        public string FILE_NAME
        {
            get
            {
                var result = string.Empty;
                var sp = this.PDF_LOCATION.ConvertToString().Split(new string[] { "\\" }, StringSplitOptions.None);
                if (sp.Length > 1)
                {
                    result = sp[sp.Length - 1];
                }
                return result;
            }
        }
        public string FILE_PATH
        {
            get
            {
                var result = string.Empty;
                var sp = this.PDF_LOCATION.ConvertToString().Split(new string[] { "\\" }, StringSplitOptions.None);
                if (sp.Length > 1)
                {
                    for (int i = 0; i < sp.Length - 1; i++)
                    {
                        result += result == string.Empty ? "" : "\\";
                        result += sp[i];
                    }
                }
                return result;
            }
        }

        public string JOB_STATUS_DESC
        {
            get
            {
                var result = string.Empty;
                switch (this.JOB_STATUS.ConvertToString())
                {
                    case "4":
                        result = "已發送";
                        break;
                    default:
                        break;
                }
                return result;
            }
        }

        public string ORDER_TYPE { get; set; }
        public string ORDER_TYPE_DESC
        {
            get
            {
                var result = string.Empty;
                switch (this.ORDER_TYPE.ConvertToString())
                {
                    case "NEW":
                        result = "新合約";
                        break;
                    case "OLD":
                        result = "舊合約-可續約";
                        break;
                    case "OLD_END":
                        result = "舊合約-不可續約";
                        break;
                    default:
                        break;
                }
                return result;
            }
        }

        public string MAIL_TYPE_END { get; set; }
        public string CUST_NAME { get; set; }
        public string CUST_NAME2 { get; set; }
        public string IB_CODE { get; set; }
        public string SALES_NAME { get; set; }

        
        public string EMAIL_1 { get; set; }
        public string EMAIL_2 { get; set; }
    }
}
