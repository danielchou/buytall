﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MK.Demo.Model
{
    public class BL_TEAM_H
    {
        public decimal? TEAM_NUM { get; set; }
        public string TEAM_NAME { get; set; }
        public decimal? PROD_ID { get; set; }
        public string STATUS { get; set; }
        public DateTime? CREATION_DATE { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? LAST_UPDATE_DATE { get; set; }
        public string LAST_UPDATED_BY { get; set; }
    }




}
