﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MK.Demo.Utility;

namespace MK.Demo.Model
{
    public class BL_ORDER
    {
        public decimal? ORDER_ID { get; set; }
        //public string  ORDER_NO { get; set; }
        /// <summary>
        /// 合約編號(Production改為數值型態)
        /// 2019/04/17 > 又改回文字
        /// </summary>
        public string ORDER_NO { get; set; }
        public decimal? PROD_ID { get; set; }
        public decimal? CUST_ID { get; set; }
        public string EMP_CODE { get; set; }
        public decimal? TEAM_NUM { get; set; }
        public string MT4_ACCOUNT { get; set; }
        public string ENTER_CURRENCY { get; set; }
        public decimal? ENTER_AMOUNT { get; set; }
        public string CURRENCY { get; set; }
        public decimal? AMOUNT { get; set; }
        public DateTime? FROM_DATE { get; set; }
        public DateTime? END_DATE { get; set; }
        public decimal? YEAR { get; set; }
        public string STATUS { get; set; }
        public string NOTE { get; set; }
        public string ATTRIBUTE1 { get; set; }
        public string ATTRIBUTE2 { get; set; }
        public string ATTRIBUTE3 { get; set; }
        public string ATTRIBUTE4 { get; set; }
        public string ATTRIBUTE5 { get; set; }
        public DateTime? CREATION_DATE { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? LAST_UPDATE_DATE { get; set; }
        public string LAST_UPDATED_BY { get; set; }
        public string IB_CODE { get; set; }
        public string INTERNAL_IB_CODE { get; set; }
        public DateTime? ORDER_END { get; set; }
        public decimal? AMOUNT_USD { get; set; }
        public decimal? AMOUNT_NTD { get; set; }
        public decimal? AMOUNT_RMB { get; set; }
        public decimal? AMOUNT_EUR { get; set; }
        public decimal? AMOUNT_AUD { get; set; }
        public decimal? AMOUNT_JPY { get; set; }
        public decimal? AMOUNT_NZD { get; set; }
        public DateTime? EXTEND_DATE { get; set; }
        public decimal? OLD_AMOUNT_USD { get; set; }
        public decimal? OLD_AMOUNT_NTD { get; set; }
        public decimal? OLD_AMOUNT_RMB { get; set; }
        public decimal? OLD_AMOUNT_EUR { get; set; }
        public decimal? OLD_AMOUNT_AUD { get; set; }
        public decimal? OLD_AMOUNT_JPY { get; set; }
        public decimal? OLD_AMOUNT_NZD { get; set; }
        public string PARENT_ORDER_NO { get; set; }

        public decimal? INTEREST_USD { get; set; }
        public decimal? INTEREST_NTD { get; set; }
        public decimal? INTEREST_RMB { get; set; }
        public decimal? INTEREST_EUR { get; set; }
        public decimal? INTEREST_AUD { get; set; }
        public decimal? INTEREST_JPY { get; set; }
        public decimal? INTEREST_NZD { get; set; }

        public string CUST_CNAME { get; set; }
        public string CUST_CNAME2 { get; set; }
        public string CUST_ENAME { get; set; }
        public string CUST_ENAME2 { get; set; }
        public string SALES_NAME { get; set; }

        public string SALES_IB_CODE { get; set; }
        public string SALES_EMP_CODE { get; set; }


        public string SALES_NAME_INTERNAL { get; set; }
        public string SALES_IB_CODE_INTERNAL { get; set; }
        public string SALES_EMP_CODE_INTERNAL { get; set; }

        public decimal? BALANCE { get; set; }
        public decimal? STATUS_CODE { get; set; }

        public string GetSTATUS
        {
            get
            {
                var strResult = string.Empty;

                if (this.STATUS_CODE != null)
                {
                    switch (this.STATUS_CODE.Value.ConvertToString())
                    {
                        case "1":
                            strResult = "待查核";
                            break;
                        case "3":
                            strResult = "待審批";
                            break;
                        case "4":
                            strResult = "審批通過";
                            break;
                        case "9":
                            strResult = "已退件";
                            break;
                        case "-1":
                            strResult = "已作廢";
                            break;
                        default:
                            break;
                    }
                }

                return strResult;
            }
        }

        public decimal? INTEREST_RATE { get; set; }

        public string PROJECT { get; set; }


        public DateTime? TRANSFER_DATE { get; set; }
        public decimal? ORIGIN_CUST_ID { get; set; }

        public string REJECT_REASON { get; set; }
        public string WITHDRAWAL_MODE { get; set; }

        public string MULTI_ORDER_FLAG { get; set; }
        public string IS_REMIT_TO_AUS { get; set; }
        public string ORG_CODE { get; set; }

        public string EMAIL_1 { get; set; }
        public string EMAIL_2 { get; set; }
        public string EMAIL2_1 { get; set; }
        public string EMAIL2_2 { get; set; }
        public string SALES_EMAIL { get; set; }

        public decimal? DISCOUNT_AMOUNT { get; set; }
        public string BONUS_OPTION { get; set; }

        
    }




}
