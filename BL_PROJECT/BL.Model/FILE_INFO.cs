﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MK.Demo.Model
{
    public class FILE_INFO
    {
        public string FILE_NAME { get; set; }
        public string FILE_PATH { get; set; }
        public string EMAIL { get; set; }
        public string CUST_NAME { get; set; }
        public string CUST_ID { get; set; }
        public string DATA_YM { get; set; }
        public string MAIL_TYPE_END { get; set; }
    }
}
