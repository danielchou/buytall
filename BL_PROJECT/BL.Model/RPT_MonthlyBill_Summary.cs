﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MK.Demo.Model
{
    public class RPT_MonthlyBill_Summary
    {
        public string ORDER_NO { get; set; }
        public string CUST_CNAME { get; set; }
        /// <summary>
        /// 派利次數
        /// </summary>
        public decimal? CAL_INTEREST_TIMES { get; set; }
        /// <summary>
        /// 當月派利
        /// </summary>
        public string MONTHLY_INTEREST { get; set; }
        /// <summary>
        /// 提領日期
        /// </summary>
        public string WITHDRAWAL_DATE { get; set; }
        /// <summary>
        /// 當月提領
        /// </summary>
        public string WITHDRAWAL_AMOUNT2 { get; set; }
        /// <summary>
        /// 當月提領(加總)
        /// </summary>
        public string WITHDRAWAL_AMOUNT { get; set; }
        /// <summary>
        /// 上月餘額
        /// </summary>
        public string MONTHLY_BALANCE { get; set; }
        /// <summary>
        /// 合計
        /// </summary>
        public string RETURN_BALANCE { get; set; }

        public string MT4_ACCOUNT { get; set; }
        public string CURRENCY { get; set; }
        public decimal? AMOUNT { get; set; }
        public DateTime? FROM_DATE { get; set; }

        public DateTime? END_DATE { get; set; }
        public DateTime? EXTEND_DATE { get; set; }
        public string YEAR { get; set; }
        public string POSTAL_CODE { get; set; }
        public string ADDR { get; set; }
        public string IB_CODE { get; set; }



    }
}
