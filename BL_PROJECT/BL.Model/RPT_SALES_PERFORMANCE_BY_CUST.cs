﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MK.Demo.Model
{
    /// <summary>
    /// 業績報表業務員
    /// </summary>
    public class RPT_SALES_PERFORMANCE_BY_CUST
    {
        public string SALES { get; set; }
        public string WEEK { get; set; }
        public string ORDER_NO { get; set; }
        public string CNAME { get; set; }
        public string ENAME { get; set; }
        public string CURRENCY { get; set; }
        public decimal? AMOUNT { get; set; }
        public DateTime? FROM_DATE { get; set; }
        public DateTime? END_DATE { get; set; }
        public string YEAR { get; set; }
        public decimal? AMOUNT2 { get; set; }
        
        public DateTime? DATE_S { get; set; }
        public DateTime? DATE_E { get; set; }
    }
}
