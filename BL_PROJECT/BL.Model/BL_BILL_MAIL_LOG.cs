﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MK.Demo.Utility;

namespace MK.Demo.Model
{
    public class BL_BILL_MAIL_LOG
    {
        public string JOB_ID { get; set; }
        public decimal? CUST_ID { get; set; }
        public string PREV_DATE { get; set; }
        public string THIS_DATE { get; set; }
        public DateTime? TXN_DATE { get; set; }
        public string PDF_LOCATION { get; set; }
        public string JOB_STATUS { get; set; }
        public DateTime? JOB_DATE { get; set; }
        public DateTime? LAST_UPDATE_DATE { get; set; }
        public string LAST_UPDATED_BY { get; set; }
        public string EMAIL { get; set; }
        public string IS_TEST { get; set; }





        public string FILE_NAME
        {
            get
            {
                var result = string.Empty;
                var sp = this.PDF_LOCATION.ConvertToString().Split(new string[] { "\\" }, StringSplitOptions.None);
                if (sp.Length > 1)
                {
                    result = sp[sp.Length - 1];
                }
                return result;
            }
        }
        public string FILE_PATH
        {
            get
            {
                var result = string.Empty;
                var sp = this.PDF_LOCATION.ConvertToString().Split(new string[] { "\\" }, StringSplitOptions.None);
                if (sp.Length > 1)
                {
                    for (int i = 0; i < sp.Length - 1; i++)
                    {
                        result += result == string.Empty ? "" : "\\";
                        result += sp[i];
                    }
                }
                return result;
            }
        }

        public string DATA_YM
        {
            get
            {
                return this.TXN_DATE == null ? string.Empty : this.TXN_DATE.Value.ToString("yyyyMM");
            }
        }
        public string MAIL_TYPE_END { get; set; }
        public string CUST_NAME { get; set; }
        public string CUST_NAME2 { get; set; }
        public string JOB_STATUS_DESC
        {
            get
            {
                var result = string.Empty;
                switch (this.JOB_STATUS.ConvertToString())
                {
                    case "4":
                        result = "已發送";
                        break;
                    default:
                        break;
                }
                return result;
            }
        }


        public string IB_CODE { get; set; }
        public string SALES_NAME { get; set; }
    }
}
