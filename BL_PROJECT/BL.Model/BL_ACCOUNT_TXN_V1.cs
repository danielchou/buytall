﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MK.Demo.Model
{
    public class BL_ACCOUNT_TXN_V1
    {
        public string ORDER_NO { get; set; }
        public DateTime? FROM_DATE { get; set; }
        public DateTime? END_DATE { get; set; }
        public DateTime? ORDER_END { get; set; }
        public DateTime? EXTEND_DATE { get; set; }
        public string CURRENCY { get; set; }
        public string SALES { get; set; }
        public decimal? BASE_AMOUNT { get; set; }

        public decimal? BALANCE { get; set; }
        
        public decimal? CUST_ID { get; set; }
        public string CNAME { get; set; }
        public string ENAME { get; set; }
        public decimal? ORDER_ID { get; set; }
        public decimal? BATCH_WITHDRAWAL_ID { get; set; }
        public string NEW_CONTRACT_NO { get; set; }
        public string REMARK { get; set; }
        public string BONUS_TSF { get; set; }
        public string FLOW_TYPE { get; set; }
        public string TXN_TYPE { get; set; }
        public DateTime? TXN_DATE { get; set; }
        public string WITHDRAWAL_TYPE { get; set; }
        public decimal? AMOUNT_USD { get; set; }
        public decimal? AMOUNT_NTD { get; set; }
        public decimal? AMOUNT_RMB { get; set; }
        public decimal? AMOUNT_EUR { get; set; }
        public decimal? AMOUNT_AUD { get; set; }
        public decimal? AMOUNT_JPY { get; set; }
        public decimal? AMOUNT_NZD { get; set; }
        public string APPLIED_FLAG { get; set; }
        public string APPLY_USER { get; set; }
        public DateTime? APPLY_DATE { get; set; }
        public string CHECKED_FLAG { get; set; }
        public string CHECK_USER { get; set; }
        public DateTime? CHECK_DATE { get; set; }
        public string APPROVED_FLAG { get; set; }
        public string APPROVER { get; set; }
        public DateTime? APPROVE_DATE { get; set; }
        public DateTime? CREATION_DATE { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? LAST_UPDATE_DATE { get; set; }
        public string LAST_UPDATED_BY { get; set; }
        public string ATTRIBUTE1 { get; set; }
        public string ATTRIBUTE2 { get; set; }
        public string ATTRIBUTE3 { get; set; }
        public string ATTRIBUTE4 { get; set; }
        public string ATTRIBUTE5 { get; set; }
        public decimal? TXN_ID { get; set; }
        public decimal? INTEREST_TIMES { get; set; }

        public string YEAR { get; set; }
        public string END_BALANCE_FLAG { get; set; }



        public string CUST_ENAME { get; set; }
        public string CUST_ENAME2 { get; set; }
        public string CUST_CNAME { get; set; }
        public string CUST_CNAME2 { get; set; }
        public string SALES_NAME { get; set; }
        public decimal? Amount
        {
            get
            {
                decimal? result = (decimal?)null;
                if (this.AMOUNT_USD != null)
                {
                    result = this.AMOUNT_USD;
                }
                if (this.AMOUNT_NTD != null)
                {
                    result = this.AMOUNT_NTD;
                }
                if (this.AMOUNT_RMB != null)
                {
                    result = this.AMOUNT_RMB;
                }
                if (this.AMOUNT_EUR != null)
                {
                    result = this.AMOUNT_EUR;
                }
                if (this.AMOUNT_AUD != null)
                {
                    result = this.AMOUNT_AUD;
                }
                if (this.AMOUNT_JPY != null)
                {
                    result = this.AMOUNT_JPY;
                }
                if (this.AMOUNT_NZD != null)
                {
                    result = this.AMOUNT_NZD;
                }
                return result;
            }
        }


        public string ORDER_Currency
        {
            get
            {
                var result = string.Empty;
                if (this.AMOUNT_USD != null)
                {
                    result = "USD";
                }
                if (this.AMOUNT_NTD != null)
                {
                    result = "NTD";
                }
                if (this.AMOUNT_RMB != null)
                {
                    result = "RMB";
                }
                if (this.AMOUNT_EUR != null)
                {
                    result = "EUR";
                }
                if (this.AMOUNT_AUD != null)
                {
                    result = "AUD";
                }
                if (this.AMOUNT_JPY != null)
                {
                    result = "JPY";
                }
                if (this.AMOUNT_NZD != null)
                {
                    result = "NZD";
                }
                return result;
            }
        }

        public decimal? ORDER_Amount
        {
            get
            {
                decimal? result = (decimal?)null;
                if (this.AMOUNT_USD != null)
                {
                    result = this.AMOUNT_USD;
                }
                if (this.AMOUNT_NTD != null)
                {
                    result = this.AMOUNT_NTD;
                }
                if (this.AMOUNT_RMB != null)
                {
                    result = this.AMOUNT_RMB;
                }
                if (this.AMOUNT_EUR != null)
                {
                    result = this.AMOUNT_EUR;
                }
                if (this.AMOUNT_AUD != null)
                {
                    result = this.AMOUNT_AUD;
                }
                if (this.AMOUNT_JPY != null)
                {
                    result = this.AMOUNT_JPY;
                }
                if (this.AMOUNT_NZD != null)
                {
                    result = this.AMOUNT_NZD;
                }
                return result;
            }
        }

        public decimal? ACTUAL_Amount
        {
            get
            {
                decimal? result = (decimal?)null;
                if (this.AMOUNT_USD != null)
                {
                    result = this.AMOUNT_USD;
                }
                if (this.AMOUNT_NTD != null)
                {
                    result = this.AMOUNT_NTD;
                }
                if (this.AMOUNT_RMB != null)
                {
                    result = this.AMOUNT_RMB;
                }
                if (this.AMOUNT_EUR != null)
                {
                    result = this.AMOUNT_EUR;
                }
                if (this.AMOUNT_AUD != null)
                {
                    result = this.AMOUNT_AUD;
                }
                if (this.AMOUNT_JPY != null)
                {
                    result = this.AMOUNT_JPY;
                }
                if (this.AMOUNT_NZD != null)
                {
                    result = this.AMOUNT_NZD;
                }
                return result;
            }
        }

        public string ORG_CODE { get; set; }
    }




}
