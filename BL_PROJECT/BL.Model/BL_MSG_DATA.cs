﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MK.Demo.Model
{
    public class BL_MSG_DATA
    {
        public decimal BL_MSG_DATA_SN { get; set; }
        public decimal VER_NO { get; set; }
        public string MSG_DATA { get; set; }
        public DateTime? START_DATE { get; set; }
        public DateTime? END_DATE { get; set; }
        public DateTime? CREATION_DATE { get; set; }

    }
}
