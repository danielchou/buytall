﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MK.Demo.Model
{
    /// <summary>
    /// 業績存量比較表(月份)
    /// </summary>
    public class RPT_SALES_PERFORMANCE_BY_MONTH
    {
        public string SALES { get; set; }
        public string IB_CODE { get; set; }
        public decimal? AMOUNT_USD { get; set; }
        public decimal? AMOUNT_NTD { get; set; }
        public decimal? AMOUNT_RMB { get; set; }
        public decimal? AMOUNT_EUR { get; set; }
        public decimal? AMOUNT_AUD { get; set; }
        public decimal? AMOUNT_USD2 { get; set; }
        public decimal? AMOUNT_NTD2 { get; set; }
        public decimal? AMOUNT_RMB2 { get; set; }
        public decimal? AMOUNT_EUR2 { get; set; }
        public decimal? AMOUNT_AUD2 { get; set; }
        public string P_PREV_MOH { get; set; }
        public string P_THIS_MOH { get; set; }
    }

    
}
