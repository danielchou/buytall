﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MK.Demo.Model
{
    /// <summary>
    /// Log 用的Data Model
    /// </summary>
    /// <remarks>
    /// 這會轉換成Json Format寫進DB
    /// </remarks>
    public class BL_CUST_LOG
    {
        public decimal? CUST_ID { get; set; }
        public string CUST_ENAME { get; set; }
        public string CUST_ENAME2 { get; set; }
        public string CUST_CNAME { get; set; }
        public string CUST_CNAME2 { get; set; }
        public string PASSPORT { get; set; }
        public string PASSPORT2 { get; set; }
        public string PASSPORT_REGION { get; set; }
        public string PASSPORT_REGION2 { get; set; }
        public DateTime? PASSPORT_ACTIVE_END_DATE { get; set; }
        public DateTime? PASSPORT_ACTIVE_END_DATE2 { get; set; }
        public string ID_NUMBER { get; set; }
        public string ID_NUMBER2 { get; set; }
        public string SEX { get; set; }
        public string SEX2 { get; set; }
        public DateTime? DATE_OF_BIRTH { get; set; }
        public DateTime? DATE_OF_BIRTH2 { get; set; }
        public string EMAIL_1 { get; set; }
        public string EMAIL_2 { get; set; }
        public string EMAIL2_1 { get; set; }
        public string EMAIL2_2 { get; set; }
        public string PHONE_1 { get; set; }
        public string PHONE_2 { get; set; }
        public string PHONE2_1 { get; set; }
        public string PHONE2_2 { get; set; }
        public string POSTAL_CODE { get; set; }
        public string POSTAL_CODE2 { get; set; }
        public string C_ADDRESS { get; set; }
        public string C_ADDRESS2 { get; set; }
        public string E_ADDRESS { get; set; }
        public string E_ADDRESS2 { get; set; }
        public string MAIL_TYPE_MONTHLY { get; set; }
        public string MAIL_TYPE_END { get; set; }
        public DateTime? CREATION_DATE { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? LAST_UPDATE_DATE { get; set; }
        public string LAST_UPDATED_BY { get; set; }
        public string CUST_CNAME_RAW { get; set; }
        public string CUST_CNAME2_RAW { get; set; }
        public string MT4_ACCOUNT { get; set; }
        public string WEB_PASSWORD { get; set; }
        public string IS_NEED_CHANGE_PWD { get; set; }
        public string UNION_ID_NUMBER { get; set; }
        public string ORG_CODE { get; set; }
    }
}

