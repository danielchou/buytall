﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MK.Demo.Utility;

namespace MK.Demo.Model
{
    public class BL_ACCOUNT_TXN_TEMP
    {
        public decimal? ORDER_ID { get; set; }
        public string FLOW_TYPE { get; set; }
        public string TXN_TYPE { get; set; }
        public DateTime? TXN_DATE { get; set; }
        public decimal? AMOUNT_USD { get; set; }
        public decimal? AMOUNT_NTD { get; set; }
        public decimal? AMOUNT_RMB { get; set; }
        public decimal? AMOUNT_EUR { get; set; }
        public decimal? AMOUNT_AUD { get; set; }
        public decimal? AMOUNT_JPY { get; set; }
        public decimal? AMOUNT_NZD { get; set; }
        public string APPLIED_FLAG { get; set; }
        public string APPLY_USER { get; set; }
        public DateTime? APPLY_DATE { get; set; }
        public string CHECKED_FLAG { get; set; }
        public string CHECK_USER { get; set; }
        public DateTime? CHECK_DATE { get; set; }
        public string APPROVED_FLAG { get; set; }
        public string APPROVER { get; set; }
        public DateTime? APPROVE_DATE { get; set; }
        public DateTime? CREATION_DATE { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? LAST_UPDATE_DATE { get; set; }
        public string LAST_UPDATED_BY { get; set; }
        public string ATTRIBUTE1 { get; set; }
        public string ATTRIBUTE2 { get; set; }
        public string ATTRIBUTE3 { get; set; }
        public string ATTRIBUTE4 { get; set; }
        public string ATTRIBUTE5 { get; set; }
        public decimal? TXN_ID { get; set; }
        public decimal? INTEREST_TIMES { get; set; }
        public string WITHDRAWAL_TYPE { get; set; }
        public string SCHEDULE { get; set; }

        public string PROCESS_TYPE { get; set; }
        public DateTime? FROM_DATE { get; set; }
        public string YEAR { get; set; }

        public decimal? branch { get; set; }
        public string END_BALANCE_FLAG  { get; set; }


        public string   NEW_CONTRACT_NO { get; set; }
        public  string REMARK { get; set; }

        public string CUST_ENAME { get; set; }
        public string CUST_ENAME2 { get; set; }
        public string SCHEDULE_DESC
        {
            get
            {
                var result = string.Empty;
                switch (this.SCHEDULE.ConvertToString().Trim())
                {
                    case "0":
                        result = "待查核(資料暫存)";
                        break;
                    case "3":
                        result = "待審批(查核通過)";
                        break;
                    case "4":
                        result = "已審批(申請送出)";
                        break;
                    case "9":
                        result = "已退件";
                        break;
                    case "8":
                        result = "待確認(業務暫存)";
                        break;
                    default:
                        break;
                }
              

                return result;
            }
        }

        public string Currency
        {
            get
            {
                var result = string.Empty;
                if (this.AMOUNT_USD != null)
                {
                    result = "USD";
                }
                if (this.AMOUNT_NTD != null)
                {
                    result = "NTD";
                }
                if (this.AMOUNT_RMB != null)
                {
                    result = "RMB";
                }
                if (this.AMOUNT_EUR != null)
                {
                    result = "EUR";
                }
                if (this.AMOUNT_AUD != null)
                {
                    result = "AUD";
                }
                if (this.AMOUNT_JPY != null)
                {
                    result = "JPY";
                }
                if (this.AMOUNT_NZD != null)
                {
                    result = "NZD";
                }

                return result;
            }
        }
        public decimal? Amount
        {
            get
            {
                decimal? result = (decimal?)null;
                if (this.AMOUNT_USD != null)
                {
                    result = this.AMOUNT_USD;
                }
                if (this.AMOUNT_NTD != null)
                {
                    result = this.AMOUNT_NTD;
                }
                if (this.AMOUNT_RMB != null)
                {
                    result = this.AMOUNT_RMB;
                }
                if (this.AMOUNT_EUR != null)
                {
                    result = this.AMOUNT_EUR;
                }
                if (this.AMOUNT_AUD != null)
                {
                    result = this.AMOUNT_AUD;
                }
                if (this.AMOUNT_JPY != null)
                {
                    result = this.AMOUNT_JPY;
                }
                if (this.AMOUNT_NZD != null)
                {
                    result = this.AMOUNT_NZD;
                }
                return result;
            }
        }
        public string ORDER_NO { get; set; }
        public decimal? ORDER_AMOUNT_USD { get; set; }
        public decimal? ORDER_AMOUNT_NTD { get; set; }
        public decimal? ORDER_AMOUNT_RMB { get; set; }
        public decimal? ORDER_AMOUNT_EUR { get; set; }
        public decimal? ORDER_AMOUNT_AUD { get; set; }
        public decimal? ORDER_AMOUNT_JPY { get; set; }
        public decimal? ORDER_AMOUNT_NZD { get; set; }


        public string ORDER_Currency
        {
            get
            {
                var result = string.Empty;
                if (this.ORDER_AMOUNT_USD != null)
                {
                    result = "USD";
                }
                if (this.ORDER_AMOUNT_NTD != null)
                {
                    result = "NTD";
                }
                if (this.ORDER_AMOUNT_RMB != null)
                {
                    result = "RMB";
                }
                if (this.ORDER_AMOUNT_EUR != null)
                {
                    result = "EUR";
                }
                if (this.ORDER_AMOUNT_AUD != null)
                {
                    result = "AUD";
                }
                if (this.ORDER_AMOUNT_JPY!= null)
                {
                    result = "JPY";
                }
                if (this.ORDER_AMOUNT_NZD != null)
                {
                    result = "NZD";
                }

                return result;
            }
        }

        public decimal? ORDER_Amount
        {
            get
            {
                decimal? result = (decimal?)null;
                if (this.ORDER_AMOUNT_USD != null)
                {
                    result = this.ORDER_AMOUNT_USD;
                }
                if (this.ORDER_AMOUNT_NTD != null)
                {
                    result = this.ORDER_AMOUNT_NTD;
                }
                if (this.ORDER_AMOUNT_RMB != null)
                {
                    result = this.ORDER_AMOUNT_RMB;
                }
                if (this.ORDER_AMOUNT_EUR != null)
                {
                    result = this.ORDER_AMOUNT_EUR;
                }
                if (this.ORDER_AMOUNT_AUD != null)
                {
                    result = this.ORDER_AMOUNT_AUD;
                }
                if (this.ORDER_AMOUNT_JPY != null)
                {
                    result = this.ORDER_AMOUNT_JPY;
                }
                if (this.ORDER_AMOUNT_NZD != null)
                {
                    result = this.ORDER_AMOUNT_NZD;
                }
                return result;
            }
        }


        public string CUST_CNAME { get; set; }
        public string CUST_CNAME2 { get; set; }

        public decimal? BALANCE { get; set; }

        public decimal? CUST_ID { get; set; }
        public decimal? BATCH_WITHDRAWAL_ID { get; set; }
    

        /// <summary>
        /// 是否餘額結清
        /// </summary>
        public string IS_ALL_BALANCE_OUT { get; set; }

        public string BONUS_TSF { get; set; }
     
        public decimal? TEMP_ID { get; set; }

        /// <summary>
        /// 實際提領金額(USD)
        /// </summary>
        public decimal? ACTUAL_WITHDRAWAL_AMT_USD { get; set; }
        /// <summary>
        /// 實際提領金額(NTD)
        /// </summary>
        public decimal? ACTUAL_WITHDRAWAL_AMT_NTD { get; set; }
        /// <summary>
        /// 實際提領金額(RMB)
        /// </summary>
        public decimal? ACTUAL_WITHDRAWAL_AMT_RMB { get; set; }
        /// <summary>
        /// 實際提領金額(EUR)
        /// </summary>
        public decimal? ACTUAL_WITHDRAWAL_AMT_EUR { get; set; }
        /// <summary>
        /// 實際提領金額(AUD)
        /// </summary>
        public decimal? ACTUAL_WITHDRAWAL_AMT_AUD { get; set; }
        /// <summary>
        /// 實際提領金額(JPY)
        /// </summary>
        public decimal? ACTUAL_WITHDRAWAL_AMT_JPY { get; set; }
        /// <summary>
        /// 實際提領金額(NZD)
        /// </summary>
        public decimal? ACTUAL_WITHDRAWAL_AMT_NZD { get; set; }

        public decimal? ACTUAL_Amount
        {
            get
            {
                decimal? result = (decimal?)null;
                if (this.ACTUAL_WITHDRAWAL_AMT_USD != null)
                {
                    result = this.ACTUAL_WITHDRAWAL_AMT_USD;
                }
                if (this.ACTUAL_WITHDRAWAL_AMT_NTD != null)
                {
                    result = this.ACTUAL_WITHDRAWAL_AMT_NTD;
                }
                if (this.ACTUAL_WITHDRAWAL_AMT_RMB != null)
                {
                    result = this.ACTUAL_WITHDRAWAL_AMT_RMB;
                }
                if (this.ACTUAL_WITHDRAWAL_AMT_EUR != null)
                {
                    result = this.ACTUAL_WITHDRAWAL_AMT_EUR;
                }
                if (this.ACTUAL_WITHDRAWAL_AMT_AUD != null)
                {
                    result = this.ACTUAL_WITHDRAWAL_AMT_AUD;
                }
                if (this.ACTUAL_WITHDRAWAL_AMT_JPY != null)
                {
                    result = this.ACTUAL_WITHDRAWAL_AMT_JPY;
                }
                if (this.ACTUAL_WITHDRAWAL_AMT_NZD != null)
                {
                    result = this.ACTUAL_WITHDRAWAL_AMT_NZD;
                }
                return result;
            }
        }

        public decimal? PKG_INTEREST_TIMES { get; set; }

        public string REJECT_REASON { get; set; }

        public int? IN_PROCESS_CNT { get; set; }

        public decimal? OnPathAmount { get; set; }

        public decimal? ORDER_INTEREST_MONTHLY { get; set; }

        public string ORG_CODE { get; set; }
    }


}
