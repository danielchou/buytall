﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MK.Demo.Model
{
    public class BL_CUST_EDIT_LOG
    {

        public long BL_CUST_EDIT_LOG_S1 { get; set; }
        public string EDIT_BY { get; set; }
        public DateTime? EDIT_DATE { get; set; }

        public string EDIT_OLD_DESC { get; set; }

        public string EDIT_NEW_DESC { get; set; }

        public string EDIT_OLD_DESC_SHOW
        {
            get
            {
                return this.EDIT_OLD_DESC.Replace(",\"", "," + Environment.NewLine + "\"");
            }
        }

        public string EDIT_NEW_DESC_SHOW
        {
            get
            {
                return this.EDIT_NEW_DESC.Replace(",\"", "," + Environment.NewLine + "\"");
            }
        }


    }
}
