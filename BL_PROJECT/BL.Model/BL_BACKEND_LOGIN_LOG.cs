﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MK.Demo.Model
{
    public class BL_BACKEND_LOGIN_LOG
    {
        public long BL_BACKEND_LOGIN_LOG_SN
        {
            get; set;
        }
        public DateTime? LOGIN_DATETIME
        {
            get; set;
        }
        public string LOGIN_IP
        {
            get; set;
        }
        public string LOGIN_ACCOUNT
        {
            get; set;
        }
        public long? BL_MSG_DATA_SN
        {
            get; set;
        }

    }
}
