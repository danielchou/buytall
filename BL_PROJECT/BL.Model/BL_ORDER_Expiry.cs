﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MK.Demo.Model
{
    /// <summary>
    /// 合約到期資訊
    /// </summary>
    public class BL_ORDER_Expiry
    {
        public string ORDER_TYPE { get; set; }
        public string ORDER_NO { get; set; }
        public string CNAME { get; set; }
        public string ENAME { get; set; }
        public string SALES { get; set; }
        public string IB_CODE { get; set; }
        public DateTime? FROM_DATE { get; set; }
        public DateTime? END_DATE { get; set; }
        public DateTime? EXTEND_DATE { get; set; }
        public DateTime? ORDER_END { get; set; }
        /// <summary>
        /// 通知日期
        /// </summary>
        public DateTime? NOTICE_DATE { get; set; }

        public decimal? INTEREST_RATE { get; set; }
        public decimal? CUST_ID { get; set; }
        public decimal? YEAR { get; set; }

        public string PHONE { get; set; }
        public string POSTAL_CODE { get; set; }
        public string C_ADDRESS { get; set; }

        public string ORDEREND_MONTH { get; set; }


        public string CUST_ENAME { get; set; }
        public string CUST_ENAME2 { get; set; }
        public string CUST_CNAME { get; set; }
        public string CUST_CNAME2 { get; set; }
        public string EMAIL_1 { get; set; }
        public string EMAIL_2 { get; set; }

        public string ID_NUMBER { get; set; }
        public string UNION_ID_NUMBER { get; set; }
    }
}
