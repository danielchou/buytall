﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MK.Demo.Model
{
    public class BL_RPT_MONTHLY_BILL
    {
        public decimal? BL_RPT_MONTHLY_BILL_SN { get; set; }
        public string CUST_CNAME { get; set; }
        public string CUST_ENAME { get; set; }
        public string IB_CODE { get; set; }
        public string POSTAL_CODE { get; set; }
        public string ADDRESS { get; set; }
        public string ORDER_NO { get; set; }
        public string ORDER_NO2 { get; set; }
        public DateTime? FROM_DATE { get; set; }
        public DateTime? END_DATE { get; set; }
        public DateTime? EXTEND_DATE { get; set; }
        public DateTime? ORDER_END { get; set; }
        public string CURRENCY { get; set; }
        public decimal? AMOUNT { get; set; }
        public decimal? PRE_MONTH_BALANCE { get; set; }
        public decimal? INTEREST_OUT { get; set; }
        public string INTEREST_DATE { get; set; }
        public decimal? INTEREST_TIMES { get; set; }
        public decimal? BALANCE_USD { get; set; }
        public decimal? BALANCE_NTD { get; set; }
        public decimal? BALANCE_RMB { get; set; }

        public decimal? NEW_INT { get; set; }
        public decimal? END_BALANCE { get; set; }

        public string IB_CODE_COMBINE { get; set; }

        /// <summary>
        /// 用來判斷分頁條件
        /// </summary>
        /// <remarks>
        /// Add On 2019/06/28
        /// </remarks>
        public string DATA_GROUP { get; set; }

        public string Is_Last { get; set; }
        public string SPECIAL_MEMO { get; set; }
    }


}
