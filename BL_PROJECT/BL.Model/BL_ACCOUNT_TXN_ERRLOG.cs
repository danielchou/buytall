﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MK.Demo.Model
{
    public class BL_ACCOUNT_TXN_ERRLOG
    {
        public decimal? ERROR_ID { get; set; }
        public decimal? ORDER_ID { get; set; }
        public decimal? BATCH_WITHDRAWAL_ID { get; set; }
        public string TXN_TYPE { get; set; }
        public decimal? AMOUNT_USD { get; set; }
        public decimal? AMOUNT_RMB { get; set; }
        public decimal? AMOUNT_NTD { get; set; }
        public decimal? AMOUNT_EUR { get; set; }
        public decimal? AMOUNT_AUD { get; set; }
        public decimal? AMOUNT_JPY { get; set; }
        public decimal? AMOUNT_NZD { get; set; }
        public string WITHDRAWAL_TYPE { get; set; }
        public string REMARK { get; set; }
        public string BONUS_TSF { get; set; }
        public string NEW_CONTRACT_NO { get; set; }
        public string END_BALANCE_FLAG { get; set; }
        public string APPLY_USER { get; set; }
        public string CHECK_USER { get; set; }
        public string APPROVER { get; set; }
        public DateTime? CREATION_DATE { get; set; }



        public decimal? BALANCE { get; set; }
        public string ORDER_NO { get; set; }
        public DateTime? FROM_DATE { get; set; }
        public int? YEAR { get; set; }
        public string CUST_CNAME { get; set; }
        public string CUST_CNAME2 { get; set; }
        public string CUST_ENAME { get; set; }
        public decimal? ORDER_AMOUNT_USD { get; set; }
        public decimal? ORDER_AMOUNT_NTD { get; set; }
        public decimal? ORDER_AMOUNT_RMB { get; set; }
        public decimal? ORDER_AMOUNT_EUR { get; set; }
        public decimal? ORDER_AMOUNT_AUD { get; set; }
        public decimal? ORDER_AMOUNT_JPY { get; set; }
        public decimal? ORDER_AMOUNT_NZD { get; set; }


        public string Currency
        {
            get
            {
                var result = string.Empty;
                if (this.AMOUNT_USD != null)
                {
                    result = "USD";
                }
                if (this.AMOUNT_NTD != null)
                {
                    result = "NTD";
                }
                if (this.AMOUNT_RMB != null)
                {
                    result = "RMB";
                }
                if (this.AMOUNT_EUR != null)
                {
                    result = "EUR";
                }
                if (this.AMOUNT_AUD != null)
                {
                    result = "AUD";
                }
                if (this.AMOUNT_JPY != null)
                {
                    result = "JPY";
                }
                if (this.AMOUNT_NZD != null)
                {
                    result = "NZD";
                }
                return result;
            }
        }
        public decimal? Amount
        {
            get
            {
                decimal? result = (decimal?)null;
                if (this.AMOUNT_USD != null)
                {
                    result = this.AMOUNT_USD;
                }
                if (this.AMOUNT_NTD != null)
                {
                    result = this.AMOUNT_NTD;
                }
                if (this.AMOUNT_RMB != null)
                {
                    result = this.AMOUNT_RMB;
                }
                if (this.AMOUNT_EUR != null)
                {
                    result = this.AMOUNT_EUR;
                }
                if (this.AMOUNT_AUD != null)
                {
                    result = this.AMOUNT_AUD;
                }
                if (this.AMOUNT_JPY != null)
                {
                    result = this.AMOUNT_JPY;
                }
                if (this.AMOUNT_NZD != null)
                {
                    result = this.AMOUNT_NZD;
                }
                return result;
            }
        }

        public string CUST_ENAME2 { get; set; }
        public decimal? CUST_ID { get; set; }

        public string ORDER_Currency
        {
            get
            {
                var result = string.Empty;
                if (this.ORDER_AMOUNT_USD != null)
                {
                    result = "USD";
                }
                if (this.ORDER_AMOUNT_NTD != null)
                {
                    result = "NTD";
                }
                if (this.ORDER_AMOUNT_RMB != null)
                {
                    result = "RMB";
                }
                if (this.ORDER_AMOUNT_EUR != null)
                {
                    result = "EUR";
                }
                if (this.ORDER_AMOUNT_AUD != null)
                {
                    result = "AUD";
                }
                if (this.ORDER_AMOUNT_JPY != null)
                {
                    result = "JPY";
                }
                if (this.ORDER_AMOUNT_NZD != null)
                {
                    result = "NZD";
                }
                return result;
            }
        }

        public decimal? ORDER_Amount
        {
            get
            {
                decimal? result = (decimal?)null;
                if (this.ORDER_AMOUNT_USD != null)
                {
                    result = this.ORDER_AMOUNT_USD;
                }
                if (this.ORDER_AMOUNT_NTD != null)
                {
                    result = this.ORDER_AMOUNT_NTD;
                }
                if (this.ORDER_AMOUNT_RMB != null)
                {
                    result = this.ORDER_AMOUNT_RMB;
                }
                if (this.ORDER_AMOUNT_EUR != null)
                {
                    result = this.ORDER_AMOUNT_EUR;
                }
                if (this.ORDER_AMOUNT_AUD != null)
                {
                    result = this.ORDER_AMOUNT_AUD;
                }
                if (this.ORDER_AMOUNT_JPY != null)
                {
                    result = this.ORDER_AMOUNT_JPY;
                }
                if (this.ORDER_AMOUNT_NZD != null)
                {
                    result = this.ORDER_AMOUNT_NZD;
                }
                return result;
            }
        }


        public decimal? ACTUAL_Amount
        {
            get
            {
                decimal? result = (decimal?)null;
                if (this.AMOUNT_USD != null)
                {
                    result = this.AMOUNT_USD;
                }
                if (this.AMOUNT_NTD != null)
                {
                    result = this.AMOUNT_NTD;
                }
                if (this.AMOUNT_RMB != null)
                {
                    result = this.AMOUNT_RMB;
                }
                if (this.AMOUNT_EUR != null)
                {
                    result = this.AMOUNT_EUR;
                }
                if (this.AMOUNT_AUD != null)
                {
                    result = this.AMOUNT_AUD;
                }
                if (this.AMOUNT_JPY != null)
                {
                    result = this.AMOUNT_JPY;
                }
                if (this.AMOUNT_NZD != null)
                {
                    result = this.AMOUNT_NZD;
                }
                return result;
            }
        }

        public string ORG_CODE { get; set; }

    }


}
