﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MK.Demo.Model
{
   public class QueryCust
    {
        /// <summary>
        /// 客戶姓名
        /// </summary>
        public string CUST_NAME { get; set; }
        /// <summary>
        /// 身分證號
        /// </summary>
        public string ID_NUMBER { get; set; }
        /// <summary>
        /// 統一編號
        /// </summary>
        public string ID_NUMBER2 { get; set; }
        /// <summary>
        /// 護照號碼
        /// </summary>
        public string PASSPORT { get; set; }
        /// <summary>
        /// 所屬業務
        /// </summary>
        public string SALES { get; set; }
        /// <summary>
        /// 合約數量
        /// </summary>
        public int? CONTRACT_COUNT { get; set; }
        /// <summary>
        /// 合約數量條件
        /// </summary>
        /// <remarks>
        /// - 大於
        /// - 小於
        /// - 等於
        /// - 大於等於
        /// - 小於等於
        /// </remarks>
        public string CONTRACT_COUNT_CONDITION { get; set; }

        /// <summary>
        /// 是否要Show出尚未發送過信件之客戶
        /// </summary>
        public string IschkUnSendMail { get; set; }

        /// <summary>
        /// 是否要查詢有Order End的資料
        /// </summary>
        public string IsIncOrderEnd { get; set; }
    }
}
