﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MK.Demo.Model
{
    public class ComboBoxData<T>
    {
        public string Display { get; set; }

        public T Value { get; set; }
    }
}
