﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MK.Demo.Model
{
    public class BL_ACCOUNT_TXN
    {
        public decimal? ORDER_ID { get; set; }
        public string FLOW_TYPE { get; set; }
        public string TXN_TYPE { get; set; }
        public DateTime? TXN_DATE { get; set; }
        public decimal? AMOUNT_USD { get; set; }
        public decimal? AMOUNT_NTD { get; set; }
        public decimal? AMOUNT_RMB { get; set; }
        public decimal? AMOUNT_EUR { get; set; }
        public decimal? AMOUNT_AUD { get; set; }
        public string APPLIED_FLAG { get; set; }
        public string APPLY_USER { get; set; }
        public DateTime? APPLY_DATE { get; set; }
        public string CHECKED_FLAG { get; set; }
        public string CHECK_USER { get; set; }
        public DateTime? CHECK_DATE { get; set; }
        public string APPROVED_FLAG { get; set; }
        public string APPROVER { get; set; }
        public DateTime? APPROVE_DATE { get; set; }
        public DateTime? CREATION_DATE { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? LAST_UPDATE_DATE { get; set; }
        public string LAST_UPDATED_BY { get; set; }
        public string ATTRIBUTE1 { get; set; }
        public string ATTRIBUTE2 { get; set; }
        public string ATTRIBUTE3 { get; set; }
        public string ATTRIBUTE4 { get; set; }
        public string ATTRIBUTE5 { get; set; }
        public decimal? TXN_ID { get; set; }
        public decimal? INTEREST_TIMES { get; set; }
        public string WITHDRAWAL_TYPE { get; set; }
        public decimal? BATCH_WITHDRAWAL_ID { get; set; }
        public string REMARK { get; set; }
        public string BONUS_TSF { get; set; }
        public string NEW_CONTRACT_NO { get; set; }

    }




}
