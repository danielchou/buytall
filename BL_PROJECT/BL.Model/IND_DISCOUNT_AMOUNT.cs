﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MK.Demo.Model
{
    public class IND_DISCOUNT_AMOUNT
    {
        public decimal? DiscountAmount { get; set; }
    }
}
