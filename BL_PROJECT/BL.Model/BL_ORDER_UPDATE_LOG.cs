﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MK.Demo.Model
{
    public class BL_ORDER_UPDATE_LOG
    {
        public decimal? BL_ORDER_UPDATE_LOG_ID { get; set; }
        public decimal? ORDER_ID { get; set; }
        public string ORDER_NO { get; set; }
        public decimal? STATUS_CODE { get; set; }
        public string REMARK_TEXT { get; set; }
        public DateTime? UPDATE_DATE { get; set; }
        public string UPDATED_BY { get; set; }

    }
}
