﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MK.Demo.Model
{
    public class RPT_DailyBill_Summary
    {
        public DateTime TXN_DATE { get; set; }
        public string ORDER_NO { get; set; }
        public string CUST_ENAME { get; set; }
        public string TYPE { get; set; }

        public string BANK_ENAME { get; set; }
        public string BRANCH_ENAME { get; set; }
        public string ACCOUNT_CNAME { get; set; }
        public string SWIFT_CODE { get; set; }
        public string ACCOUNT { get; set; }
        public string CNAME { get; set; }
        public string REMARK { get; set; }
        /// <summary>
        /// 本金
        /// </summary>
        public decimal? BASE_AMOUNT { get; set; }
        /// <summary>
        /// 本金提領
        /// </summary>
        public decimal? BASE_AMOUNT2 { get; set; }
        /// <summary>
        /// 紅利提領
        /// </summary>
        public decimal? ACTUAL_AMOUNT2 { get; set; }
        /// <summary>
        /// 總提領
        /// </summary>
        public decimal? ACTUAL_AMOUNT { get; set; }
        public decimal? TOTAL_AMOUNT { get; set; }
        public string TXN_ID { get; set; }
        public string BATCH_WITHDRAWAL_ID { get; set; }
        public string BONUS_TSF { get; set; }
        public string END_BALANCE_FLAG { get; set; }

    }
}
