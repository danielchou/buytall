﻿namespace MK_DEMO.Destop
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {

                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.lblTestMode = new System.Windows.Forms.Label();
            this.lb_login_user = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnTransContractToNewSales = new System.Windows.Forms.Button();
            this.btnAddNewContract = new System.Windows.Forms.Button();
            this.btnQuery = new System.Windows.Forms.Button();
            this.picLoder = new System.Windows.Forms.PictureBox();
            this.dgv_header = new System.Windows.Forms.DataGridView();
            this.check = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.CUST_CNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUST_CNAME2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUST_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUST_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Sales_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PASSPORT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID_NUMBER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID_NUMBER2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ORDER_CNT = new System.Windows.Forms.DataGridViewLinkColumn();
            this.AddContract = new System.Windows.Forms.DataGridViewLinkColumn();
            this.ViewDetail = new System.Windows.Forms.DataGridViewLinkColumn();
            this.IsIncOrderEnd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolScriptCust = new System.Windows.Forms.ToolStripButton();
            this.toolScriptContract = new System.Windows.Forms.ToolStripButton();
            this.toolScriptDeposit = new System.Windows.Forms.ToolStripButton();
            this.toolStripContractApprove = new System.Windows.Forms.ToolStripButton();
            this.toolScriptManager = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolScriptAddTxn = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripEmpManager = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripAlertToCustomer = new System.Windows.Forms.ToolStripMenuItem();
            this.toolMenu_BL_PERIOD_Management = new System.Windows.Forms.ToolStripMenuItem();
            this.toolMenu_NTD_Payment_Job = new System.Windows.Forms.ToolStripMenuItem();
            this.toolScriptReport = new System.Windows.Forms.ToolStripDropDownButton();
            this.每月新合約統計報表ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.每月出金統計表ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.每月新客戶列表ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripRPTMonthlyBill = new System.Windows.Forms.ToolStripMenuItem();
            this.本月到期合約清單ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.月對帳總表ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.每日出金表ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolMenu_OrderExpiry = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.ToolStripAlertToCustomer_DepositOnLine = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLoder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_header)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // lblTestMode
            // 
            this.lblTestMode.AutoSize = true;
            this.lblTestMode.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lblTestMode.ForeColor = System.Drawing.Color.Red;
            this.lblTestMode.Location = new System.Drawing.Point(287, 0);
            this.lblTestMode.Name = "lblTestMode";
            this.lblTestMode.Size = new System.Drawing.Size(74, 21);
            this.lblTestMode.TabIndex = 6;
            this.lblTestMode.Text = "測試環境";
            this.lblTestMode.Visible = false;
            // 
            // lb_login_user
            // 
            this.lb_login_user.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lb_login_user.AutoSize = true;
            this.lb_login_user.Font = new System.Drawing.Font("微軟正黑體", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lb_login_user.ForeColor = System.Drawing.Color.Blue;
            this.lb_login_user.Location = new System.Drawing.Point(840, 3);
            this.lb_login_user.Name = "lb_login_user";
            this.lb_login_user.Size = new System.Drawing.Size(214, 19);
            this.lb_login_user.TabIndex = 5;
            this.lb_login_user.Text = "HI ~ Alex Chan  歡迎登入系統 ";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.tabControl1.Location = new System.Drawing.Point(3, -2);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1090, 675);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.pictureBox1);
            this.tabPage2.Controls.Add(this.groupBox3);
            this.tabPage2.Controls.Add(this.toolStrip1);
            this.tabPage2.Location = new System.Drawing.Point(4, 28);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPage2.Size = new System.Drawing.Size(1082, 643);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "主功能畫面";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(1026, -1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(45, 36);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 15;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.checkBox1);
            this.groupBox3.Controls.Add(this.panel1);
            this.groupBox3.Controls.Add(this.picLoder);
            this.groupBox3.Controls.Add(this.dgv_header);
            this.groupBox3.Font = new System.Drawing.Font("微軟正黑體", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(5, 34);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1072, 606);
            this.groupBox3.TabIndex = 14;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "客戶清單";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(38, 26);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(15, 14);
            this.checkBox1.TabIndex = 115;
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.panel1.Controls.Add(this.btnTransContractToNewSales);
            this.panel1.Controls.Add(this.btnAddNewContract);
            this.panel1.Controls.Add(this.btnQuery);
            this.panel1.Location = new System.Drawing.Point(6, 567);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1060, 33);
            this.panel1.TabIndex = 114;
            // 
            // btnTransContractToNewSales
            // 
            this.btnTransContractToNewSales.BackColor = System.Drawing.Color.Gainsboro;
            this.btnTransContractToNewSales.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnTransContractToNewSales.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnTransContractToNewSales.Font = new System.Drawing.Font("微軟正黑體", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnTransContractToNewSales.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTransContractToNewSales.Location = new System.Drawing.Point(279, 2);
            this.btnTransContractToNewSales.Name = "btnTransContractToNewSales";
            this.btnTransContractToNewSales.Size = new System.Drawing.Size(155, 29);
            this.btnTransContractToNewSales.TabIndex = 110;
            this.btnTransContractToNewSales.Text = "合約轉業務";
            this.btnTransContractToNewSales.UseVisualStyleBackColor = false;
            this.btnTransContractToNewSales.Click += new System.EventHandler(this.btnTransContractToNewSales_Click);
            // 
            // btnAddNewContract
            // 
            this.btnAddNewContract.BackColor = System.Drawing.Color.Gainsboro;
            this.btnAddNewContract.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnAddNewContract.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAddNewContract.Font = new System.Drawing.Font("微軟正黑體", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnAddNewContract.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddNewContract.Location = new System.Drawing.Point(115, 2);
            this.btnAddNewContract.Name = "btnAddNewContract";
            this.btnAddNewContract.Size = new System.Drawing.Size(155, 29);
            this.btnAddNewContract.TabIndex = 109;
            this.btnAddNewContract.Text = "新增新客戶合約";
            this.btnAddNewContract.UseVisualStyleBackColor = false;
            this.btnAddNewContract.Click += new System.EventHandler(this.btnAddNewContract_Click);
            // 
            // btnQuery
            // 
            this.btnQuery.BackColor = System.Drawing.Color.Gainsboro;
            this.btnQuery.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnQuery.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnQuery.Font = new System.Drawing.Font("微軟正黑體", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnQuery.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnQuery.Location = new System.Drawing.Point(2, 2);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(105, 29);
            this.btnQuery.TabIndex = 108;
            this.btnQuery.Text = "查詢";
            this.btnQuery.UseVisualStyleBackColor = false;
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // picLoder
            // 
            this.picLoder.BackColor = System.Drawing.Color.Transparent;
            this.picLoder.Image = ((System.Drawing.Image)(resources.GetObject("picLoder.Image")));
            this.picLoder.Location = new System.Drawing.Point(442, 249);
            this.picLoder.Name = "picLoder";
            this.picLoder.Size = new System.Drawing.Size(174, 19);
            this.picLoder.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picLoder.TabIndex = 113;
            this.picLoder.TabStop = false;
            this.picLoder.Visible = false;
            // 
            // dgv_header
            // 
            this.dgv_header.AllowUserToAddRows = false;
            this.dgv_header.AllowUserToDeleteRows = false;
            this.dgv_header.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_header.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_header.ColumnHeadersHeight = 25;
            this.dgv_header.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv_header.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.check,
            this.CUST_CNAME,
            this.CUST_CNAME2,
            this.CURRENCY,
            this.CUST_NO,
            this.CUST_ID,
            this.Sales_Name,
            this.PASSPORT,
            this.ID_NUMBER,
            this.ID_NUMBER2,
            this.ORDER_CNT,
            this.AddContract,
            this.ViewDetail,
            this.IsIncOrderEnd});
            this.dgv_header.Location = new System.Drawing.Point(6, 20);
            this.dgv_header.Name = "dgv_header";
            this.dgv_header.ReadOnly = true;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("微軟正黑體", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.RoyalBlue;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Yellow;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_header.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_header.RowHeadersWidth = 25;
            this.dgv_header.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgv_header.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(226)))), ((int)(((byte)(239)))));
            this.dgv_header.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_header.RowTemplate.Height = 24;
            this.dgv_header.Size = new System.Drawing.Size(1060, 543);
            this.dgv_header.TabIndex = 0;
            this.dgv_header.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_header_CellClick);
            this.dgv_header.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgv_header_DataBindingComplete);
            // 
            // check
            // 
            this.check.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.check.FillWeight = 25F;
            this.check.HeaderText = "";
            this.check.MinimumWidth = 25;
            this.check.Name = "check";
            this.check.ReadOnly = true;
            this.check.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.check.Width = 25;
            // 
            // CUST_CNAME
            // 
            this.CUST_CNAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.CUST_CNAME.DataPropertyName = "CUST_CNAME";
            this.CUST_CNAME.HeaderText = "主要客戶姓名";
            this.CUST_CNAME.Name = "CUST_CNAME";
            this.CUST_CNAME.ReadOnly = true;
            this.CUST_CNAME.Width = 110;
            // 
            // CUST_CNAME2
            // 
            this.CUST_CNAME2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.CUST_CNAME2.DataPropertyName = "CUST_CNAME2";
            this.CUST_CNAME2.HeaderText = "次要客戶姓名";
            this.CUST_CNAME2.Name = "CUST_CNAME2";
            this.CUST_CNAME2.ReadOnly = true;
            this.CUST_CNAME2.Width = 110;
            // 
            // CURRENCY
            // 
            this.CURRENCY.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.CURRENCY.HeaderText = "幣別";
            this.CURRENCY.Name = "CURRENCY";
            this.CURRENCY.ReadOnly = true;
            this.CURRENCY.Visible = false;
            this.CURRENCY.Width = 90;
            // 
            // CUST_NO
            // 
            this.CUST_NO.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.CUST_NO.HeaderText = "客戶編號";
            this.CUST_NO.Name = "CUST_NO";
            this.CUST_NO.ReadOnly = true;
            this.CUST_NO.Visible = false;
            this.CUST_NO.Width = 90;
            // 
            // CUST_ID
            // 
            this.CUST_ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.CUST_ID.DataPropertyName = "CUST_ID";
            this.CUST_ID.HeaderText = "客戶ID";
            this.CUST_ID.Name = "CUST_ID";
            this.CUST_ID.ReadOnly = true;
            this.CUST_ID.Width = 90;
            // 
            // Sales_Name
            // 
            this.Sales_Name.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Sales_Name.DataPropertyName = "SALES_NAME";
            this.Sales_Name.HeaderText = "所屬業務";
            this.Sales_Name.Name = "Sales_Name";
            this.Sales_Name.ReadOnly = true;
            this.Sales_Name.Width = 90;
            // 
            // PASSPORT
            // 
            this.PASSPORT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.PASSPORT.DataPropertyName = "PASSPORT";
            this.PASSPORT.HeaderText = "護照號碼";
            this.PASSPORT.Name = "PASSPORT";
            this.PASSPORT.ReadOnly = true;
            this.PASSPORT.Width = 110;
            // 
            // ID_NUMBER
            // 
            this.ID_NUMBER.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ID_NUMBER.DataPropertyName = "ID_NUMBER";
            this.ID_NUMBER.FillWeight = 110F;
            this.ID_NUMBER.HeaderText = "身分證字號";
            this.ID_NUMBER.Name = "ID_NUMBER";
            this.ID_NUMBER.ReadOnly = true;
            this.ID_NUMBER.Width = 110;
            // 
            // ID_NUMBER2
            // 
            this.ID_NUMBER2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ID_NUMBER2.DataPropertyName = "ID_NUMBER2";
            this.ID_NUMBER2.FillWeight = 110F;
            this.ID_NUMBER2.HeaderText = "公司統編";
            this.ID_NUMBER2.Name = "ID_NUMBER2";
            this.ID_NUMBER2.ReadOnly = true;
            this.ID_NUMBER2.Width = 110;
            // 
            // ORDER_CNT
            // 
            this.ORDER_CNT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ORDER_CNT.DataPropertyName = "ORDER_CNT";
            this.ORDER_CNT.FillWeight = 90F;
            this.ORDER_CNT.HeaderText = "合約數目";
            this.ORDER_CNT.Name = "ORDER_CNT";
            this.ORDER_CNT.ReadOnly = true;
            this.ORDER_CNT.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ORDER_CNT.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ORDER_CNT.Width = 80;
            // 
            // AddContract
            // 
            this.AddContract.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.AddContract.HeaderText = "新增合約";
            this.AddContract.Name = "AddContract";
            this.AddContract.ReadOnly = true;
            this.AddContract.Text = "新增合約";
            this.AddContract.ToolTipText = "新增合約";
            this.AddContract.Width = 110;
            // 
            // ViewDetail
            // 
            this.ViewDetail.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ViewDetail.HeaderText = "詳細";
            this.ViewDetail.Name = "ViewDetail";
            this.ViewDetail.ReadOnly = true;
            this.ViewDetail.Text = "客人詳細資料";
            this.ViewDetail.ToolTipText = "客人詳細資料";
            this.ViewDetail.Width = 110;
            // 
            // IsIncOrderEnd
            // 
            this.IsIncOrderEnd.DataPropertyName = "IsIncOrderEnd";
            this.IsIncOrderEnd.HeaderText = "is_orderend";
            this.IsIncOrderEnd.MinimumWidth = 100;
            this.IsIncOrderEnd.Name = "IsIncOrderEnd";
            this.IsIncOrderEnd.ReadOnly = true;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Font = new System.Drawing.Font("微軟正黑體", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolScriptCust,
            this.toolScriptContract,
            this.toolScriptDeposit,
            this.toolStripContractApprove,
            this.toolScriptManager,
            this.toolScriptReport,
            this.toolStripButton1,
            this.toolStripButton2,
            this.toolStripButton3});
            this.toolStrip1.Location = new System.Drawing.Point(3, 4);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Padding = new System.Windows.Forms.Padding(2);
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.toolStrip1.Size = new System.Drawing.Size(1076, 31);
            this.toolStrip1.TabIndex = 13;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolScriptCust
            // 
            this.toolScriptCust.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.toolScriptCust.Image = ((System.Drawing.Image)(resources.GetObject("toolScriptCust.Image")));
            this.toolScriptCust.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolScriptCust.Name = "toolScriptCust";
            this.toolScriptCust.Size = new System.Drawing.Size(93, 24);
            this.toolScriptCust.Text = "客戶查詢";
            this.toolScriptCust.ToolTipText = "客戶查詢";
            this.toolScriptCust.Click += new System.EventHandler(this.toolScriptCust_Click);
            // 
            // toolScriptContract
            // 
            this.toolScriptContract.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.toolScriptContract.Image = ((System.Drawing.Image)(resources.GetObject("toolScriptContract.Image")));
            this.toolScriptContract.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolScriptContract.Name = "toolScriptContract";
            this.toolScriptContract.Size = new System.Drawing.Size(93, 24);
            this.toolScriptContract.Text = "合約查詢";
            this.toolScriptContract.Click += new System.EventHandler(this.toolScriptContract_Click);
            // 
            // toolScriptDeposit
            // 
            this.toolScriptDeposit.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Bold);
            this.toolScriptDeposit.Image = ((System.Drawing.Image)(resources.GetObject("toolScriptDeposit.Image")));
            this.toolScriptDeposit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolScriptDeposit.Margin = new System.Windows.Forms.Padding(5, 1, 0, 2);
            this.toolScriptDeposit.Name = "toolScriptDeposit";
            this.toolScriptDeposit.Size = new System.Drawing.Size(93, 24);
            this.toolScriptDeposit.Text = "出金作業";
            this.toolScriptDeposit.Click += new System.EventHandler(this.toolScriptDeposit_Click);
            // 
            // toolStripContractApprove
            // 
            this.toolStripContractApprove.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.toolStripContractApprove.Image = ((System.Drawing.Image)(resources.GetObject("toolStripContractApprove.Image")));
            this.toolStripContractApprove.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripContractApprove.Name = "toolStripContractApprove";
            this.toolStripContractApprove.Size = new System.Drawing.Size(108, 24);
            this.toolStripContractApprove.Text = "待審核合約";
            this.toolStripContractApprove.ToolTipText = "待審核合約";
            this.toolStripContractApprove.Click += new System.EventHandler(this.toolStripContractApprove_Click);
            // 
            // toolScriptManager
            // 
            this.toolScriptManager.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolScriptAddTxn,
            this.ToolStripEmpManager,
            this.ToolStripAlertToCustomer,
            this.toolMenu_BL_PERIOD_Management,
            this.toolMenu_NTD_Payment_Job,
            this.ToolStripAlertToCustomer_DepositOnLine});
            this.toolScriptManager.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.toolScriptManager.Image = ((System.Drawing.Image)(resources.GetObject("toolScriptManager.Image")));
            this.toolScriptManager.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolScriptManager.Name = "toolScriptManager";
            this.toolScriptManager.Size = new System.Drawing.Size(102, 24);
            this.toolScriptManager.Text = "設定管理";
            // 
            // toolScriptAddTxn
            // 
            this.toolScriptAddTxn.Name = "toolScriptAddTxn";
            this.toolScriptAddTxn.Size = new System.Drawing.Size(264, 24);
            this.toolScriptAddTxn.Text = "調整帳作業";
            this.toolScriptAddTxn.Click += new System.EventHandler(this.toolScriptEmpManager_Click);
            // 
            // ToolStripEmpManager
            // 
            this.ToolStripEmpManager.Name = "ToolStripEmpManager";
            this.ToolStripEmpManager.Size = new System.Drawing.Size(264, 24);
            this.ToolStripEmpManager.Text = "員工基本資料管理";
            this.ToolStripEmpManager.Click += new System.EventHandler(this.ToolStripEmpManager_Click);
            // 
            // ToolStripAlertToCustomer
            // 
            this.ToolStripAlertToCustomer.Name = "ToolStripAlertToCustomer";
            this.ToolStripAlertToCustomer.Size = new System.Drawing.Size(264, 24);
            this.ToolStripAlertToCustomer.Text = "客人WebApp登入資訊通知";
            this.ToolStripAlertToCustomer.Click += new System.EventHandler(this.ToolStripAlertToCustomer_Click);
            // 
            // toolMenu_BL_PERIOD_Management
            // 
            this.toolMenu_BL_PERIOD_Management.Name = "toolMenu_BL_PERIOD_Management";
            this.toolMenu_BL_PERIOD_Management.Size = new System.Drawing.Size(264, 24);
            this.toolMenu_BL_PERIOD_Management.Text = "開關帳月份設定";
            this.toolMenu_BL_PERIOD_Management.Click += new System.EventHandler(this.toolMenu_BL_PERIOD_Management_Click);
            // 
            // toolMenu_NTD_Payment_Job
            // 
            this.toolMenu_NTD_Payment_Job.Name = "toolMenu_NTD_Payment_Job";
            this.toolMenu_NTD_Payment_Job.Size = new System.Drawing.Size(264, 24);
            this.toolMenu_NTD_Payment_Job.Text = "台幣提領執行作業";
            this.toolMenu_NTD_Payment_Job.Click += new System.EventHandler(this.toolMenu_NTD_Payment_Job_Click);
            // 
            // toolScriptReport
            // 
            this.toolScriptReport.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.每月新合約統計報表ToolStripMenuItem,
            this.每月出金統計表ToolStripMenuItem,
            this.每月新客戶列表ToolStripMenuItem,
            this.toolStripRPTMonthlyBill,
            this.本月到期合約清單ToolStripMenuItem,
            this.月對帳總表ToolStripMenuItem,
            this.每日出金表ToolStripMenuItem,
            this.toolMenu_OrderExpiry});
            this.toolScriptReport.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.toolScriptReport.Image = ((System.Drawing.Image)(resources.GetObject("toolScriptReport.Image")));
            this.toolScriptReport.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolScriptReport.Margin = new System.Windows.Forms.Padding(5, 1, 0, 2);
            this.toolScriptReport.Name = "toolScriptReport";
            this.toolScriptReport.Size = new System.Drawing.Size(102, 24);
            this.toolScriptReport.Text = "報表統計";
            this.toolScriptReport.ToolTipText = "報表";
            // 
            // 每月新合約統計報表ToolStripMenuItem
            // 
            this.每月新合約統計報表ToolStripMenuItem.Font = new System.Drawing.Font("微軟正黑體", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.每月新合約統計報表ToolStripMenuItem.Name = "每月新合約統計報表ToolStripMenuItem";
            this.每月新合約統計報表ToolStripMenuItem.Size = new System.Drawing.Size(227, 24);
            this.每月新合約統計報表ToolStripMenuItem.Text = "業績報表 (業務員)";
            this.每月新合約統計報表ToolStripMenuItem.Click += new System.EventHandler(this.每月新合約統計報表ToolStripMenuItem_Click);
            // 
            // 每月出金統計表ToolStripMenuItem
            // 
            this.每月出金統計表ToolStripMenuItem.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.每月出金統計表ToolStripMenuItem.Name = "每月出金統計表ToolStripMenuItem";
            this.每月出金統計表ToolStripMenuItem.Size = new System.Drawing.Size(227, 24);
            this.每月出金統計表ToolStripMenuItem.Text = "業績報表 (團隊)";
            this.每月出金統計表ToolStripMenuItem.Click += new System.EventHandler(this.每月出金統計表ToolStripMenuItem_Click);
            // 
            // 每月新客戶列表ToolStripMenuItem
            // 
            this.每月新客戶列表ToolStripMenuItem.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.每月新客戶列表ToolStripMenuItem.Name = "每月新客戶列表ToolStripMenuItem";
            this.每月新客戶列表ToolStripMenuItem.Size = new System.Drawing.Size(227, 24);
            this.每月新客戶列表ToolStripMenuItem.Text = "業績存量比較表 (月份)";
            this.每月新客戶列表ToolStripMenuItem.Click += new System.EventHandler(this.每月新客戶列表ToolStripMenuItem_Click);
            // 
            // toolStripRPTMonthlyBill
            // 
            this.toolStripRPTMonthlyBill.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.toolStripRPTMonthlyBill.Name = "toolStripRPTMonthlyBill";
            this.toolStripRPTMonthlyBill.Size = new System.Drawing.Size(227, 24);
            this.toolStripRPTMonthlyBill.Text = "月對帳單 (客戶)";
            this.toolStripRPTMonthlyBill.Click += new System.EventHandler(this.toolStripRPTMonthlyBill_Click);
            // 
            // 本月到期合約清單ToolStripMenuItem
            // 
            this.本月到期合約清單ToolStripMenuItem.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.本月到期合約清單ToolStripMenuItem.Name = "本月到期合約清單ToolStripMenuItem";
            this.本月到期合約清單ToolStripMenuItem.Size = new System.Drawing.Size(227, 24);
            this.本月到期合約清單ToolStripMenuItem.Text = "到期合約清單";
            this.本月到期合約清單ToolStripMenuItem.Click += new System.EventHandler(this.本月到期合約清單ToolStripMenuItem_Click);
            // 
            // 月對帳總表ToolStripMenuItem
            // 
            this.月對帳總表ToolStripMenuItem.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.月對帳總表ToolStripMenuItem.Name = "月對帳總表ToolStripMenuItem";
            this.月對帳總表ToolStripMenuItem.Size = new System.Drawing.Size(227, 24);
            this.月對帳總表ToolStripMenuItem.Text = "月對帳總表";
            this.月對帳總表ToolStripMenuItem.Click += new System.EventHandler(this.月對帳總表ToolStripMenuItem_Click);
            // 
            // 每日出金表ToolStripMenuItem
            // 
            this.每日出金表ToolStripMenuItem.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.每日出金表ToolStripMenuItem.Name = "每日出金表ToolStripMenuItem";
            this.每日出金表ToolStripMenuItem.Size = new System.Drawing.Size(227, 24);
            this.每日出金表ToolStripMenuItem.Text = "每日出金表";
            this.每日出金表ToolStripMenuItem.Click += new System.EventHandler(this.每日出金表ToolStripMenuItem_Click);
            // 
            // toolMenu_OrderExpiry
            // 
            this.toolMenu_OrderExpiry.Name = "toolMenu_OrderExpiry";
            this.toolMenu_OrderExpiry.Size = new System.Drawing.Size(227, 24);
            this.toolMenu_OrderExpiry.Text = "到期通知書";
            this.toolMenu_OrderExpiry.Click += new System.EventHandler(this.toolMenu_OrderExpiry_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Font = new System.Drawing.Font("微軟正黑體", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.toolStripButton1.ForeColor = System.Drawing.Color.Blue;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(125, 24);
            this.toolStripButton1.Text = "合約審批(HK)";
            this.toolStripButton1.ToolTipText = "合約審批(HK)";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.Font = new System.Drawing.Font("微軟正黑體", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.toolStripButton2.ForeColor = System.Drawing.Color.Blue;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(125, 24);
            this.toolStripButton2.Text = "出金審批(HK)";
            this.toolStripButton2.ToolTipText = "出金審批(HK)";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.Font = new System.Drawing.Font("微軟正黑體", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.toolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton3.Image")));
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(108, 24);
            this.toolStripButton3.Text = "調整帳作業";
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // ToolStripAlertToCustomer_DepositOnLine
            // 
            this.ToolStripAlertToCustomer_DepositOnLine.Name = "ToolStripAlertToCustomer_DepositOnLine";
            this.ToolStripAlertToCustomer_DepositOnLine.Size = new System.Drawing.Size(264, 24);
            this.ToolStripAlertToCustomer_DepositOnLine.Text = "線上出金通知信";
            this.ToolStripAlertToCustomer_DepositOnLine.Click += new System.EventHandler(this.ToolStripAlertToCustomer_DepositOnLine_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1094, 675);
            this.Controls.Add(this.lblTestMode);
            this.Controls.Add(this.lb_login_user);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("微軟正黑體", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "穩健型商品後台 Ver 20200409";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Main_FormClosed);
            this.Load += new System.EventHandler(this.Main_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picLoder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_header)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label lb_login_user;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolScriptCust;
        private System.Windows.Forms.ToolStripButton toolScriptDeposit;
        private System.Windows.Forms.ToolStripDropDownButton toolScriptReport;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.PictureBox picLoder;
        private System.Windows.Forms.DataGridView dgv_header;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnQuery;
        private System.Windows.Forms.ToolStripDropDownButton toolScriptManager;
        private System.Windows.Forms.ToolStripMenuItem toolScriptAddTxn;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Button btnAddNewContract;
        private System.Windows.Forms.ToolStripButton toolScriptContract;
        private System.Windows.Forms.ToolStripMenuItem 每月新合約統計報表ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 每月出金統計表ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 每月新客戶列表ToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripContractApprove;
        private System.Windows.Forms.ToolStripMenuItem 本月到期合約清單ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripRPTMonthlyBill;
        private System.Windows.Forms.ToolStripMenuItem ToolStripEmpManager;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.Label lblTestMode;
        private System.Windows.Forms.ToolStripMenuItem ToolStripAlertToCustomer;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.ToolStripMenuItem 月對帳總表ToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolStripMenuItem 每日出金表ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolMenu_BL_PERIOD_Management;
        private System.Windows.Forms.ToolStripMenuItem toolMenu_NTD_Payment_Job;
        private System.Windows.Forms.ToolStripMenuItem toolMenu_OrderExpiry;
        private System.Windows.Forms.Button btnTransContractToNewSales;
        private System.Windows.Forms.DataGridViewCheckBoxColumn check;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUST_CNAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUST_CNAME2;
        private System.Windows.Forms.DataGridViewTextBoxColumn CURRENCY;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUST_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUST_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Sales_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn PASSPORT;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID_NUMBER;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID_NUMBER2;
        private System.Windows.Forms.DataGridViewLinkColumn ORDER_CNT;
        private System.Windows.Forms.DataGridViewLinkColumn AddContract;
        private System.Windows.Forms.DataGridViewLinkColumn ViewDetail;
        private System.Windows.Forms.DataGridViewTextBoxColumn IsIncOrderEnd;
        private System.Windows.Forms.ToolStripMenuItem ToolStripAlertToCustomer_DepositOnLine;
    }
}