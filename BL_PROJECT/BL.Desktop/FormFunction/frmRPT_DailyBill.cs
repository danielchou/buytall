﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MK.Demo.Utility;
using Microsoft.Reporting.WinForms;
using MK.Demo.Logic;
using System.IO;
using MK.Demo.Model;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.xml;
using iTextSharp.text.html;

namespace MK_DEMO.Destop.FormFunction
{
    public partial class frmRPT_DailyBill : Form
    {
        private List<string> liCustID = new List<string>();
        private List<BL_CUST> liCust_ALL = new List<BL_CUST>();
        public frmRPT_DailyBill()
        {
            InitializeComponent();
        }

        private void frmRPT_DailyBill_Load(object sender, EventArgs e)
        {
            this.InitComboBox();
            //this.BindReport();
        }

        private void InitComboBox()
        {
            this.dteReportEndDate.Value = DateTime.Now;
            this.dteReportEndDate.Text = string.Empty;
            this.dteReportEndDate.Format = DateTimePickerFormat.Custom;
            this.dteReportEndDate.CustomFormat = " ";
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.BindReport();
        }


        private void BindReport()
        {
            var strORG = Global.ORG_CODE;
            var strRepoetEndDate = DateTime.Now;
            if (this.dteReportEndDate.Text.IsDate()) strRepoetEndDate = this.dteReportEndDate.Value;

            var data = new logicCust(Global.IsTestMode).GetDailyBillReportData(strRepoetEndDate, strORG);


            string exeFolder = Application.StartupPath;
            string reportPath = System.IO.Path.Combine(exeFolder, @"RPT\RPT_DAILY_BILL.rdlc");

            var binding = new BindingSource();
            binding.DataSource = data;
            this.reportViewer2.Reset();

            this.reportViewer2.LocalReport.ReportPath = reportPath;
            this.reportViewer2.LocalReport.DataSources.Add(new ReportDataSource("dsReport", binding));
            this.reportViewer2.RefreshReport();

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dteReportEndDate_ValueChanged(object sender, EventArgs e)
        {
            this.dteReportEndDate.Format = DateTimePickerFormat.Long;
        }



      
    }



}
