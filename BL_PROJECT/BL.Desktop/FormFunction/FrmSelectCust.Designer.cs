﻿namespace MK_DEMO.Destop.FormFunction
{
    partial class FrmSelectCust
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.dgv_header = new System.Windows.Forms.DataGridView();
            this.check = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.CUST_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IB_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SALES_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUST_CNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUST_ENAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUST_CNAME2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUST_ENAME2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MAIL_TYPE_END = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID_NUMBER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID_NUMBER2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UNION_ID_NUMBER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EMAIL_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EMAIL_2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.txtIB_CODE = new System.Windows.Forms.TextBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.txtIB_CODE2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.btnSearch = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_header)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSubmit
            // 
            this.btnSubmit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSubmit.Location = new System.Drawing.Point(159, 359);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(147, 48);
            this.btnSubmit.TabIndex = 9;
            this.btnSubmit.Text = "確定";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(312, 359);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(147, 48);
            this.btnClose.TabIndex = 8;
            this.btnClose.Text = "關閉";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // dgv_header
            // 
            this.dgv_header.AllowUserToAddRows = false;
            this.dgv_header.AllowUserToDeleteRows = false;
            this.dgv_header.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_header.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_header.ColumnHeadersHeight = 25;
            this.dgv_header.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv_header.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.check,
            this.CUST_ID,
            this.IB_CODE,
            this.SALES_NAME,
            this.CUST_CNAME,
            this.CUST_ENAME,
            this.CUST_CNAME2,
            this.CUST_ENAME2,
            this.MAIL_TYPE_END,
            this.ID_NUMBER,
            this.ID_NUMBER2,
            this.UNION_ID_NUMBER,
            this.EMAIL_1,
            this.EMAIL_2});
            this.dgv_header.Location = new System.Drawing.Point(12, 37);
            this.dgv_header.Name = "dgv_header";
            this.dgv_header.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("微軟正黑體", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.RoyalBlue;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Yellow;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_header.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_header.RowHeadersWidth = 25;
            this.dgv_header.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgv_header.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(226)))), ((int)(((byte)(239)))));
            this.dgv_header.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_header.RowTemplate.Height = 24;
            this.dgv_header.Size = new System.Drawing.Size(797, 316);
            this.dgv_header.TabIndex = 10;
            this.dgv_header.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_header_CellClick);
            // 
            // check
            // 
            this.check.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.check.FillWeight = 25F;
            this.check.HeaderText = "";
            this.check.MinimumWidth = 25;
            this.check.Name = "check";
            this.check.ReadOnly = true;
            this.check.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.check.Width = 25;
            // 
            // CUST_ID
            // 
            this.CUST_ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.CUST_ID.DataPropertyName = "CUST_ID";
            this.CUST_ID.HeaderText = "CUST_ID";
            this.CUST_ID.Name = "CUST_ID";
            this.CUST_ID.ReadOnly = true;
            this.CUST_ID.Width = 80;
            // 
            // IB_CODE
            // 
            this.IB_CODE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.IB_CODE.DataPropertyName = "IB_CODE";
            this.IB_CODE.HeaderText = "IB_CODE";
            this.IB_CODE.Name = "IB_CODE";
            this.IB_CODE.ReadOnly = true;
            this.IB_CODE.Width = 90;
            // 
            // SALES_NAME
            // 
            this.SALES_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.SALES_NAME.DataPropertyName = "SALES_NAME";
            this.SALES_NAME.HeaderText = "業務";
            this.SALES_NAME.Name = "SALES_NAME";
            this.SALES_NAME.ReadOnly = true;
            this.SALES_NAME.Width = 105;
            // 
            // CUST_CNAME
            // 
            this.CUST_CNAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.CUST_CNAME.DataPropertyName = "CUST_CNAME";
            this.CUST_CNAME.HeaderText = "客戶";
            this.CUST_CNAME.Name = "CUST_CNAME";
            this.CUST_CNAME.ReadOnly = true;
            this.CUST_CNAME.Width = 115;
            // 
            // CUST_ENAME
            // 
            this.CUST_ENAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.CUST_ENAME.DataPropertyName = "CUST_ENAME";
            this.CUST_ENAME.HeaderText = "客戶(英)";
            this.CUST_ENAME.Name = "CUST_ENAME";
            this.CUST_ENAME.ReadOnly = true;
            this.CUST_ENAME.Width = 220;
            // 
            // CUST_CNAME2
            // 
            this.CUST_CNAME2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.CUST_CNAME2.DataPropertyName = "CUST_CNAME2";
            this.CUST_CNAME2.HeaderText = "聯名客戶";
            this.CUST_CNAME2.Name = "CUST_CNAME2";
            this.CUST_CNAME2.ReadOnly = true;
            this.CUST_CNAME2.Width = 115;
            // 
            // CUST_ENAME2
            // 
            this.CUST_ENAME2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.CUST_ENAME2.DataPropertyName = "CUST_ENAME2";
            this.CUST_ENAME2.HeaderText = "聯名客戶(英)";
            this.CUST_ENAME2.Name = "CUST_ENAME2";
            this.CUST_ENAME2.ReadOnly = true;
            this.CUST_ENAME2.Width = 115;
            // 
            // MAIL_TYPE_END
            // 
            this.MAIL_TYPE_END.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.MAIL_TYPE_END.DataPropertyName = "MAIL_TYPE_END";
            this.MAIL_TYPE_END.HeaderText = "寄送方式";
            this.MAIL_TYPE_END.Name = "MAIL_TYPE_END";
            this.MAIL_TYPE_END.ReadOnly = true;
            this.MAIL_TYPE_END.Width = 130;
            // 
            // ID_NUMBER
            // 
            this.ID_NUMBER.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ID_NUMBER.DataPropertyName = "ID_NUMBER";
            this.ID_NUMBER.HeaderText = "ID_NUMBER";
            this.ID_NUMBER.Name = "ID_NUMBER";
            this.ID_NUMBER.ReadOnly = true;
            this.ID_NUMBER.Visible = false;
            // 
            // ID_NUMBER2
            // 
            this.ID_NUMBER2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ID_NUMBER2.DataPropertyName = "ID_NUMBER2";
            this.ID_NUMBER2.HeaderText = "ID_NUMBER2";
            this.ID_NUMBER2.Name = "ID_NUMBER2";
            this.ID_NUMBER2.ReadOnly = true;
            this.ID_NUMBER2.Visible = false;
            // 
            // UNION_ID_NUMBER
            // 
            this.UNION_ID_NUMBER.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.UNION_ID_NUMBER.DataPropertyName = "UNION_ID_NUMBER";
            this.UNION_ID_NUMBER.HeaderText = "UNION_ID_NUMBER";
            this.UNION_ID_NUMBER.Name = "UNION_ID_NUMBER";
            this.UNION_ID_NUMBER.ReadOnly = true;
            this.UNION_ID_NUMBER.Visible = false;
            // 
            // EMAIL_1
            // 
            this.EMAIL_1.DataPropertyName = "EMAIL_1";
            this.EMAIL_1.HeaderText = "EMAIL_1";
            this.EMAIL_1.Name = "EMAIL_1";
            this.EMAIL_1.ReadOnly = true;
            this.EMAIL_1.Visible = false;
            // 
            // EMAIL_2
            // 
            this.EMAIL_2.DataPropertyName = "EMAIL_2";
            this.EMAIL_2.HeaderText = "EMAIL_2";
            this.EMAIL_2.Name = "EMAIL_2";
            this.EMAIL_2.ReadOnly = true;
            this.EMAIL_2.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.label1.Location = new System.Drawing.Point(17, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 19);
            this.label1.TabIndex = 11;
            this.label1.Text = "客戶";
            // 
            // txtIB_CODE
            // 
            this.txtIB_CODE.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.txtIB_CODE.Location = new System.Drawing.Point(59, 4);
            this.txtIB_CODE.Name = "txtIB_CODE";
            this.txtIB_CODE.Size = new System.Drawing.Size(127, 27);
            this.txtIB_CODE.TabIndex = 12;
            this.txtIB_CODE.TextChanged += new System.EventHandler(this.txtIB_CODE_TextChanged);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(41, 42);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(15, 14);
            this.checkBox1.TabIndex = 13;
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // txtIB_CODE2
            // 
            this.txtIB_CODE2.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.txtIB_CODE2.Location = new System.Drawing.Point(308, 4);
            this.txtIB_CODE2.Name = "txtIB_CODE2";
            this.txtIB_CODE2.Size = new System.Drawing.Size(127, 27);
            this.txtIB_CODE2.TabIndex = 15;
            this.txtIB_CODE2.TextChanged += new System.EventHandler(this.txtIB_CODE2_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.label2.Location = new System.Drawing.Point(219, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 19);
            this.label2.TabIndex = 14;
            this.label2.Text = "IB_CODE";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.label3.Location = new System.Drawing.Point(458, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 19);
            this.label3.TabIndex = 16;
            this.label3.Text = "寄送方式";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "",
            "業務人員親送",
            "電子郵件",
            "其他"});
            this.comboBox1.Location = new System.Drawing.Point(534, 7);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 25);
            this.comboBox1.TabIndex = 17;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(671, 4);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(138, 28);
            this.btnSearch.TabIndex = 18;
            this.btnSearch.Text = "搜尋";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // FrmSelectCust
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.ClientSize = new System.Drawing.Size(821, 419);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtIB_CODE2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.txtIB_CODE);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgv_header);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.btnClose);
            this.Font = new System.Drawing.Font("微軟正黑體", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Name = "FrmSelectCust";
            this.Text = "選取客戶";
            this.Load += new System.EventHandler(this.FrmSelectCust_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_header)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.DataGridView dgv_header;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtIB_CODE;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.TextBox txtIB_CODE2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn check;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUST_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn IB_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn SALES_NAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUST_CNAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUST_ENAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUST_CNAME2;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUST_ENAME2;
        private System.Windows.Forms.DataGridViewTextBoxColumn MAIL_TYPE_END;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID_NUMBER;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID_NUMBER2;
        private System.Windows.Forms.DataGridViewTextBoxColumn UNION_ID_NUMBER;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMAIL_1;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMAIL_2;
        private System.Windows.Forms.Button btnSearch;
    }
}
