﻿using MK.Demo.Logic;
using MK.Demo.Model;
using MK_DEMO.Destop.FormFunction;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MK.Demo.Utility;
using static MK.Demo.Data.enCommon;

namespace MK_DEMO.Destop.FormFunction
{
    public partial class FrmSelectCust : MK_DEMO.Destop.BaseForm
    {
        public string strDisplay = "";
        public List<string> liCustID = new List<string>();
        public List<BL_CUST> liCust = new List<BL_CUST>();
        public enFormStatus status;
        public string strExcCustID = "";
        public bool _IsShowSendType = false;
        public string _yyyymm = "";

        public FrmSelectCust(List<string> CustID, string yyyymm = "", bool IsShowSendType = false)
        {
            InitializeComponent();
            this.status = enFormStatus.Cancel;

            this.dgv_header.AutoGenerateColumns = false;
            this.dgv_header.MultiSelect = false;
            this.dgv_header.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            this.liCustID = CustID;
            this._IsShowSendType = IsShowSendType;
            this.label3.Visible = this._IsShowSendType;
            this.comboBox1.Visible = this._IsShowSendType;
            _yyyymm = yyyymm;
        }

        public FrmSelectCust(List<string> CustID, string excCustID) : this(CustID)
        {
            strExcCustID = excCustID;
        }

        private void FrmSelectCust_Load(object sender, EventArgs e)
        {
            this.BindData();
            if (this.liCustID.Count > 0)
            {
                Parallel.ForEach(this.dgv_header.Rows.Cast<DataGridViewRow>(), x =>
                {
                    if (this.liCustID.Where(y => y.Trim() == x.Cells["CUST_ID"].Value.ConvertToString().Trim()).ToList().Count > 0)
                    {
                        x.Cells["check"].Value = true;
                    }
                });
            }

            //if (Global.str_UserName == "ADMIN")
            //{
            //    //  Todo > For Test
            //    this.dgv_header.Columns["ID_NUMBER"].Visible = true;
            //    this.dgv_header.Columns["ID_NUMBER2"].Visible = true;
            //    this.dgv_header.Columns["UNION_ID_NUMBER"].Visible = true;
            //}
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.status = enFormStatus.Cancel;
            this.Close();
        }

        private void BindData()
        {
            var strORG = Global.ORG_CODE;
            List<BL_CUST> data = new List<BL_CUST>();
            if (_yyyymm == string.Empty)
            {
                data = base.Cust.GetCustComboData_All(strORG);
            }
            else
            {
                data = base.Cust.GetCustComboData(_yyyymm, strORG);
            }
            //}
            //else
            //{
            //    data = base.Cust.GetCustComboData_Active(_yyyymm);
            //}

            if (this.strExcCustID != string.Empty)
            {
                data = data.Where(x => (x.CUST_ID == null ? "" : x.CUST_ID.Value.ConvertToString()) != this.strExcCustID).ToList();
            }
            //this.dgv_header.DataSource = data;
            this.dgv_header.DataBindByList(data);
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            var selection = this.dgv_header.Rows.Cast<DataGridViewRow>().Where(x => (bool?)x.Cells["check"].Value == true).ToList();
            if (selection.Count > 0)
            {
                this.liCustID = new List<string>();
                this.liCust = new List<BL_CUST>();
                this.strDisplay = string.Empty;
                selection.ForEach(x =>
                {
                    if (x.Cells["CUST_ID"].Value.ConvertToString().Trim() != string.Empty)
                    {
                        strDisplay += strDisplay == string.Empty ? "" : ",";
                        strDisplay += x.Cells["CUST_CNAME"].Value.ConvertToString().Trim();
                        this.liCustID.Add(x.Cells["CUST_ID"].Value.ConvertToString().Trim());
                        this.liCust.Add(new BL_CUST()
                        {
                            CUST_ID = x.Cells["CUST_ID"].Value.ConvertToString().Trim().IsNumeric() ?
                                decimal.Parse(x.Cells["CUST_ID"].Value.ConvertToString().Trim()) :
                                0,
                            CUST_CNAME = x.Cells["CUST_CNAME"].Value.ConvertToString().Trim(),
                            CUST_CNAME2 = x.Cells["CUST_CNAME2"].Value.ConvertToString().Trim(),
                            ID_NUMBER = x.Cells["ID_NUMBER"].Value.ConvertToString().Trim(),
                            ID_NUMBER2 = x.Cells["ID_NUMBER2"].Value.ConvertToString().Trim(),
                            UNION_ID_NUMBER = x.Cells["UNION_ID_NUMBER"].Value.ConvertToString().Trim(),
                            EMAIL_1 = x.Cells["EMAIL_1"].Value.ConvertToString().Trim(),
                            EMAIL_2 = x.Cells["EMAIL_2"].Value.ConvertToString().Trim(),
                            MAIL_TYPE_END = x.Cells["MAIL_TYPE_END"].Value.ConvertToString().Trim(),
                            IB_CODE = x.Cells["IB_CODE"].Value.ConvertToString().Trim(),
                            SALES_NAME = x.Cells["SALES_NAME"].Value.ConvertToString().Trim()
                        });
                    }
                });


                if (strExcCustID != string.Empty)
                {
                    if (selection.Count != 1)
                    {
                        MessageBox.Show("請單選");
                        return;
                    }
                    else
                    {
                        this.status = enFormStatus.OK;
                        this.Close();
                    }
                }
                else
                {
                    this.status = enFormStatus.OK;
                    this.Close();
                }
            }
            else
            {
                MessageBox.Show("請選取客戶");
            }
            
        }

        private void dgv_header_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                if ((bool?)this.dgv_header.Rows[e.RowIndex].Cells["check"].Value == true)
                {
                    this.dgv_header.Rows[e.RowIndex].Cells["check"].Value = false;
                }
                else
                {
                    this.dgv_header.Rows[e.RowIndex].Cells["check"].Value = true;
                }
            }
        }

        private void txtIB_CODE_TextChanged(object sender, EventArgs e)
        {
            //  2020/03/22 取消Auto Search
            //this.BindGrid();
        }

        private void BindGrid()
        {
            var strORG = Global.ORG_CODE;

            var data = base.Cust.GetCustComboData(_yyyymm, strORG);
            if (this.strExcCustID != string.Empty)
            {
                data = data.Where(x => (x.CUST_ID == null ? "" : x.CUST_ID.Value.ConvertToString()) != this.strExcCustID).ToList();
            }

            if (this.txtIB_CODE.Text.Trim() != string.Empty)
            {
                data = data
                    .Where
                    (
                        x =>
                            x.CUST_CNAME.ConvertToString().Trim().IndexOf(this.txtIB_CODE.Text.Trim()) != -1 ||
                            x.CUST_CNAME2.ConvertToString().Trim().IndexOf(this.txtIB_CODE.Text.Trim()) != -1 ||
                            x.CUST_ENAME.ConvertToString().Trim().IndexOf(this.txtIB_CODE.Text.Trim()) != -1 ||
                            x.CUST_ENAME2.ConvertToString().Trim().IndexOf(this.txtIB_CODE.Text.Trim()) != -1
                    ).ToList();
            }

            if (this.txtIB_CODE2.Text.Trim() != string.Empty)
            {
                data = data
                    .Where
                    (
                        x =>
                            x.IB_CODE.ConvertToString().Trim().IndexOf(this.txtIB_CODE2.Text.Trim()) != -1
                    ).ToList();
            }

            if (this.comboBox1.Text != string.Empty)
            {
                if (this.comboBox1.Text == "其他")
                {
                    data = data.Where(x => x.MAIL_TYPE_END.ConvertToString() != "業務人員親送"
                        && x.MAIL_TYPE_END.ConvertToString() != "電子郵件").ToList();
                }
                else
                {
                    data = data.Where(x => x.MAIL_TYPE_END.ConvertToString() == this.comboBox1.Text).ToList();
                }
            }

            //this.dgv_header.DataSource = data;
            this.dgv_header.DataBindByList(data);

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            Parallel.ForEach(this.dgv_header.Rows.Cast<DataGridViewRow>(), x =>
            {
                x.Cells["check"].Value = this.checkBox1.Checked;
            });
        }

        private void txtIB_CODE2_TextChanged(object sender, EventArgs e)
        {
            //  2020/03/22 取消Auto Search
            this.BindGrid();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //  2020/03/22 取消Auto Search
            this.BindGrid();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.BindGrid();
        }
    }
}
