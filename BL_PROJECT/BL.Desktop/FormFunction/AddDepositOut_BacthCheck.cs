﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using MK.Demo.Utility;
using MK.Demo.Model;
using MK.Demo.Logic;
using System.Threading.Tasks;
using System.Linq;
using static MK.Demo.Data.enCommon;

namespace MK_DEMO.Destop.FormFunction
{
    public partial class AddDepositOut_BatchCheck : MK_DEMO.Destop.BaseForm
    {
        public enFormStatus status;

        private List<decimal> liOrderID = new List<decimal>();
        private List<BL_ACCOUNT_TXN_TEMP> liData = new List<BL_ACCOUNT_TXN_TEMP>();
        private bool IsFirstLoad = true;
        public AddDepositOut_BatchCheck(List<BL_ACCOUNT_TXN_TEMP> chkData)
        {
            InitializeComponent();


            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            this.liData = chkData;

        }
        
        private void AddDepositOut_BatchCheck_Load(object sender, EventArgs e)
        {
            this.dataGridView1.DataBindByList(this.liData);
            IsFirstLoad = false;
        }

        
        private void btnPanelSubmit_Click(object sender, EventArgs e)
        {
            status = enFormStatus.OK;
            this.Close();
        }

        private void btnPanelClose_Click(object sender, EventArgs e)
        {
            status = enFormStatus.Cancel;
            this.Close();
        }


    }
}
