﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using MK.Demo.Utility;
using MK.Demo.Model;
using MK.Demo.Logic;
using System.Threading.Tasks;
using System.Linq;
using static MK.Demo.Data.enCommon;

namespace MK_DEMO.Destop.FormFunction
{
    public partial class AddDepositOut_Batch : MK_DEMO.Destop.BaseForm
    {
        public enFormStatus status;

        private List<decimal> liOrderID = new List<decimal>();
        private List<BL_ACCOUNT_TXN_TEMP> liData = new List<BL_ACCOUNT_TXN_TEMP>();
        private bool IsFirstLoad = true;

        private bool _IsResend = false;
        private string _OrderID = "";
        private string _BatchID = "";

        public AddDepositOut_Batch(List<decimal> selectionOrderID)
        {
            InitializeComponent();

            this.liOrderID = selectionOrderID;

            //this.dteAPPLY_DATE.Value = DateTime.Now;
            //this.dteAPPLY_DATE.Text = string.Empty;
            //this.dteAPPLY_DATE.Format = DateTimePickerFormat.Custom;
            //this.dteAPPLY_DATE.CustomFormat = " ";

            this.dteAPPROVE_DATE.Value = DateTime.Now;
            this.dteAPPROVE_DATE.Text = string.Empty;
            this.dteAPPROVE_DATE.Format = DateTimePickerFormat.Custom;
            this.dteAPPROVE_DATE.CustomFormat = " ";


            this.dteCHECK_DATE.Value = DateTime.Now;
            this.dteCHECK_DATE.Text = string.Empty;
            this.dteCHECK_DATE.Format = DateTimePickerFormat.Custom;
            this.dteCHECK_DATE.CustomFormat = " ";

            this.dgv_header.AutoGenerateColumns = false;
            this.dgv_header.MultiSelect = false;
            this.dgv_header.SelectionMode = DataGridViewSelectionMode.FullRowSelect;


            this.liData = base.Cust.GetBatchDepositDataByListOrderID(this.liOrderID);
            var strCheck = string.Empty;
            //  Get Batch SN
            var strBatchSN = base.Cust.GetNextSEQ("BL_ACCOUNT_TXN_BATCH_SN");
            this.liData.ForEach(x =>
            {
                x.BATCH_WITHDRAWAL_ID = strBatchSN;
                x.APPLY_DATE = DateTime.Now;

                //  Init 都是 利息出金
                switch (x.ORDER_Currency.ConvertToString().ToUpper().Trim())
                {
                    case "USD":
                        x.AMOUNT_USD = x.BALANCE - x.ORDER_Amount;
                        break;
                    case "NTD":
                        x.AMOUNT_NTD = x.BALANCE - x.ORDER_Amount;
                        break;
                    case "RMB":
                        x.AMOUNT_RMB = x.BALANCE - x.ORDER_Amount;
                        break;
                    case "EUR":
                        x.AMOUNT_EUR = x.BALANCE - x.ORDER_Amount;
                        break;
                    case "AUD":
                        x.AMOUNT_AUD = x.BALANCE - x.ORDER_Amount;
                        break;
                    case "JPY":
                        x.AMOUNT_JPY = x.BALANCE - x.ORDER_Amount;
                        break;
                    case "NZD":
                        x.AMOUNT_NZD = x.BALANCE - x.ORDER_Amount;
                        break;
                    default:
                        break;
                }

                x.OnPathAmount = 0;

                if ((x.IN_PROCESS_CNT == null ? 0 : x.IN_PROCESS_CNT.Value) > 0)
                {
                    var dataExists = base.Cust.GetTemp_OnPathDataByOrderID(x.ORDER_ID);
                    dataExists.ForEach(y =>
                    {
                        if (x.ORDER_NO.ConvertToString() != y.ORDER_NO.ConvertToString())
                        {
                            strCheck += strCheck == string.Empty ? "" : Environment.NewLine;
                            strCheck += "ORDER NO:" + y.ORDER_NO.ConvertToString() + "，";
                            strCheck += "BATCH NO:" + (y.BATCH_WITHDRAWAL_ID == null ? "0" : y.BATCH_WITHDRAWAL_ID.Value.ConvertToString()) + "，";
                            strCheck += "出金日期:" + (y.APPLY_DATE == null ? "" : y.APPLY_DATE.Value.ToString("yyyy/MM/dd")) + "，";
                            strCheck += "出金金額:" + (y.ACTUAL_Amount == null ? "0" : y.ACTUAL_Amount.Value.ConvertToString());
                            //x.OnPathAmount += y.Amount == null ? 0 : y.Amount.Value;
                        }
                    });
                }
            });

            if (strCheck != string.Empty)
            {
                MessageBox.Show("請注意! 此合約已等待出金資料" + Environment.NewLine + strCheck);
            }

        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strOrderID"></param>
        /// <param name="strBatchID"></param>
        /// <param name="IsResend">是否是修改重送的</param>
        /// <remarks>For 修改重送使用</remarks>
        public AddDepositOut_Batch(string strOrderID, string strBatchID, bool IsResend)
        {
            InitializeComponent();

            this._IsResend = IsResend;
            this._OrderID = strOrderID;
            this._BatchID = strBatchID;

            this.dteAPPROVE_DATE.Value = DateTime.Now;
            this.dteAPPROVE_DATE.Text = string.Empty;
            this.dteAPPROVE_DATE.Format = DateTimePickerFormat.Custom;
            this.dteAPPROVE_DATE.CustomFormat = " ";


            this.dteCHECK_DATE.Value = DateTime.Now;
            this.dteCHECK_DATE.Text = string.Empty;
            this.dteCHECK_DATE.Format = DateTimePickerFormat.Custom;
            this.dteCHECK_DATE.CustomFormat = " ";

            this.dgv_header.AutoGenerateColumns = false;
            this.dgv_header.MultiSelect = false;
            this.dgv_header.SelectionMode = DataGridViewSelectionMode.FullRowSelect;


            this.liData = new List<BL_ACCOUNT_TXN_TEMP>() {
                base.Cust.GetBatchDeposit_ResendData(strOrderID,strBatchID)
            };
            this.liData.ForEach(x =>
            {
                //  Init 都是 利息出金
                switch (x.ORDER_Currency.ConvertToString().ToUpper().Trim())
                {
                    case "USD":
                        x.AMOUNT_USD = x.BALANCE - x.ORDER_Amount;
                        break;
                    case "NTD":
                        x.AMOUNT_NTD = x.BALANCE - x.ORDER_Amount;
                        break;
                    case "RMB":
                        x.AMOUNT_RMB = x.BALANCE - x.ORDER_Amount;
                        break;
                    case "EUR":
                        x.AMOUNT_EUR = x.BALANCE - x.ORDER_Amount;
                        break;
                    case "AUD":
                        x.AMOUNT_AUD = x.BALANCE - x.ORDER_Amount;
                        break;
                    case "JPY":
                        x.AMOUNT_JPY = x.BALANCE - x.ORDER_Amount;
                        break;
                    case "NZD":
                        x.AMOUNT_NZD = x.BALANCE - x.ORDER_Amount;
                        break;
                    default:
                        break;
                }

                this.dteAPPLY_DATE.Value = x.APPLY_DATE == null ? DateTime.Now : x.APPLY_DATE.Value;
                this.checkBox2.Checked = x.BONUS_TSF.ConvertToString() == "Y";
                this.txtRemark.Text = x.REMARK.ConvertToString();
                this.textBox1.Text = x.NEW_CONTRACT_NO.ConvertToString();
                if (x.WITHDRAWAL_TYPE.ConvertToString() == "EXTERNAL")
                {
                    this.comboBox1.SelectedIndex = this.comboBox1.FindStringExact("Y");
                }
                else
                {
                    this.comboBox1.SelectedIndex = this.comboBox1.FindStringExact("N");
                }
            });

            if (IsResend)
            {
                this.btnSubmit.Text = "修改重送";
            }
        }



        private void AddDepositOut_Batch_Load(object sender, EventArgs e)
        {
            if (this._IsResend == false)
            {
                var dataExists = base.Cust.GetDepositTempUnApproceData(this.liOrderID);
                var strMsgExists = string.Empty;

                dataExists
                    .Select(x => new { x.ORDER_NO })
                    .Distinct()
                    .ForEach(x =>
                    {
                        strMsgExists += strMsgExists == string.Empty ? "" : "、";
                        strMsgExists += x.ORDER_NO.ConvertToString();
                    });
                dataExists.ForEach(x =>
                {
                    this.liData
                        .Where(y => x.ORDER_ID == y.ORDER_ID)
                        .ForEach(y =>
                        {
                            y.OnPathAmount += x.Amount == null ? 0 : x.Amount.Value;
                        });
                });

                if (strMsgExists != string.Empty)
                {
                    strMsgExists = "以下合約目前已有出金資料在審核中，請問是否仍要繼續作業?" + Environment.NewLine + "Order No:" + strMsgExists;
                    if (MessageBox.Show(strMsgExists, "系統訊息", MessageBoxButtons.YesNo) == DialogResult.No)
                    {
                        this.Close();
                    }
                }
            }

            this.BindToGrid();
            this.dgv_header_CellClick(this.dgv_header, new DataGridViewCellEventArgs(0, 0));

            if (this.liData.Count() > 0)
            {
                this.BindBankInfo(this.liData[0].CUST_ID == null ? "" : this.liData[0].CUST_ID.Value.ConvertToString());
                this.txtBatchID.Text = this.liData[0].BATCH_WITHDRAWAL_ID == null ? "" : this.liData[0].BATCH_WITHDRAWAL_ID.Value.ConvertToString();

            }
            IsFirstLoad = false;
        }

        private void BindToGrid()
        {
            //this.dgv_header.DataSource = null;
            this.dgv_header.DataSource = this.liData;
            this.SetSeq();
            //this.dgv_header.Update();


            Parallel.ForEach(this.dgv_header.Rows.Cast<DataGridViewRow>(), x =>
            {
                x.Cells["RowDel"].Value = "刪除";

                //x.Cells["AmountOut"].ReadOnly = false;

                //if (x.Cells["AmountOut"].Value.ConvertToString().Trim() != string.Empty)
                //{
                //    x.DefaultCellStyle.BackColor = Color.LightYellow;
                //}
                //else
                //{
                //    x.DefaultCellStyle.BackColor = Color.White;
                //}
            });
            this.setSummary();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            status = enFormStatus.Cancel;
            this.Close();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            //  1. 將畫面資料先儲存一次
            this.UpdateSingleDataToGrid();
            this.liData.ForEach(x =>
            {
                if (this.checkBox2.Checked)
                {
                    x.BONUS_TSF = "Y";
                }
                else
                {
                    x.BONUS_TSF = "N";
                }
                x.REMARK = this.txtRemark.Text;
                x.NEW_CONTRACT_NO = this.textBox1.Text;
                x.WITHDRAWAL_TYPE = this.comboBox1.Text;

                x.PROCESS_TYPE = string.Empty;
                if (DateTime.Parse(this.dteAPPLY_DATE.Value.ToString("yyyy/MM/dd")) > DateTime.Parse(DateTime.Now.ToString("yyyy/MM/dd")))
                {
                    x.PROCESS_TYPE = "SCHEDULE";
                }

                x.TXN_DATE = this.dteAPPLY_DATE.Value;
                //   EXTERNAL - 客戶出金, INTERNAL - 行政代出金,
                var type = comboBox1.SelectedText == "Y" ? "EXTERNAL" : "INTERNAL";
                x.WITHDRAWAL_TYPE = type;


                //x.END_BALANCE_FLAG = x.END_BALANCE_FLAG.ConvertToString().Trim() == "餘額結清" ? "Y" : (x.END_BALANCE_FLAG.ConvertToString().Trim() == "派利出金" ? "N" : "");
            });
            var dataSave = this.liData;
            dataSave = dataSave.Where(x => x.Amount != null).ToList();


            //  2. Data Validation
            if (this.DataValidation())
            {
                var IsSubmit = false;
                var frm = new AddDepositOut_BatchCheck(dataSave)
                { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
                frm.ShowDialog();
                if (frm.status == enFormStatus.OK)
                {
                    IsSubmit = true;
                }
                else
                {
                    IsSubmit = false;
                }
                frm.Dispose();
                frm = null;

                if (IsSubmit)
                {
                    if (this._IsResend)
                    {
                        if (base.Cust.ResendDeposit(dataSave[0]))
                        {
                            MessageBox.Show("資料重送成功");
                            status = enFormStatus.OK;
                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show("資料重送失敗");
                        }
                    }
                    else
                    {
                        if (base.Cust.AddNewDataByList(dataSave))
                        {
                            MessageBox.Show("資料儲存成功");
                            status = enFormStatus.OK;
                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show("資料儲存失敗");
                        }
                    }
                }
            }
        }


        private BL_ACCOUNT_TXN_TEMP GetSaveData
        {
            get
            {
                var data = new BL_ACCOUNT_TXN_TEMP();
                decimal decTry;
                DateTime dtTry;
                if (decimal.TryParse(this.txtParentOrderNo.Tag.ConvertToString(), out decTry)) data.ORDER_ID = decTry;
                if (decimal.TryParse(this.txtAMOUNT_USD.Text, out decTry)) data.AMOUNT_USD = decTry;
                if (decimal.TryParse(this.txtAMOUNT_NTD.Text, out decTry)) data.AMOUNT_NTD = decTry;
                if (decimal.TryParse(this.txtAMOUNT_RMB.Text, out decTry)) data.AMOUNT_RMB = decTry;
                if (decimal.TryParse(this.txtAMOUNT_EUR.Text, out decTry)) data.AMOUNT_EUR = decTry;
                if (decimal.TryParse(this.txtAMOUNT_AUD.Text, out decTry)) data.AMOUNT_AUD = decTry;
                if (decimal.TryParse(this.txtAMOUNT_JPY.Text, out decTry)) data.AMOUNT_JPY = decTry;
                if (decimal.TryParse(this.txtAMOUNT_NZD.Text, out decTry)) data.AMOUNT_NZD = decTry;
                //if (DateTime.TryParse(this.dteAPPLY_DATE.Text, out dtTry)) data.APPLY_DATE = dtTry;
                if (DateTime.TryParse(this.dteCHECK_DATE.Text, out dtTry)) data.CHECK_DATE = dtTry;
                if (DateTime.TryParse(this.dteAPPROVE_DATE.Text, out dtTry)) data.APPROVE_DATE = dtTry;
                
                return data;
            }
        }

        private bool DataValidation()
        {
            var msg = string.Empty;
            var result = true;

            //if (this.txtParentOrderNo.Text.Trim() == string.Empty)
            //{
            //    msg += "請選擇「Order No」" + Environment.NewLine;
            //}

            //var data = base.Cust.GetUnExpiredOrderDatabyOrderID(this.txtParentOrderNo.Text.Trim());
            //if (data.Count == 0)
            //{
            //    msg += "此「Order No」不存在，請重新輸入" + Environment.NewLine;
            //}

            Parallel.ForEach(this.dgv_header.Rows.Cast<DataGridViewRow>(), x =>
            {
                decimal decAmount;
                decimal decOutAmount;
                decimal decBALANCE;

                if (x.Cells["END_BALANCE_FLAG"].Value.ConvertToString().ToUpper().Trim() != "Y")
                {
                    if (x.Cells["AmountOut"].Value.ConvertToString() == string.Empty)
                    {
                        msg += "- 客戶:" + x.Cells["CUST_NAME"].Value.ConvertToString() + ", 可出金申請不可為0" + Environment.NewLine;
                    }
                    if
                        (
                            decimal.TryParse(x.Cells["ORDER_Amount"].Value.ConvertToString().Trim(), out decAmount) &&
                            decimal.TryParse(x.Cells["AmountOut"].Value.ConvertToString().Trim(), out decOutAmount) &&
                            decimal.TryParse(x.Cells["BALANCE"].Value.ConvertToString().Trim(), out decBALANCE)
                        )
                    {
                        if (DateTime.Parse(this.dteAPPLY_DATE.Value.ToString("yyyy/MM/dd")) > DateTime.Parse(DateTime.Now.ToString("yyyy/MM/dd")))
                        {
                            //  出金時間為未來, 一樣可以有1%的空間 [Add By 2018/11/30]
                            //  1%全部調整為1.2% [Modify By 2019/04/15]
                            if (double.Parse(decOutAmount.ConvertToString()) > (double.Parse(decBALANCE.ConvertToString()) - double.Parse(decAmount.ConvertToString())) + (double.Parse(decAmount.ConvertToString()) * 0.012))
                            {
                                msg += "- 客戶:" + x.Cells["CUST_NAME"].Value.ConvertToString() + ", 可出金申請不能大於 餘額 - 本金 + (本金 * 1%) " + Environment.NewLine;
                            }
                        }
                        else
                        {
                            if (decOutAmount > decBALANCE - decAmount)
                            {
                                msg += "- 客戶:" + x.Cells["CUST_NAME"].Value.ConvertToString() + ", 可出金申請不能大於 餘額 – 本金" + Environment.NewLine;
                            }
                        }
                    }
                    else
                    {
                        //  不卡控不合規則的資料, 後面直接判斷, 沒提領金額不寫入Temp
                        //msg += "- 客戶:" + x.Cells["CUST_NAME"].Value.ConvertToString() + ", 出金申請不能大於 餘額 – 本金" + Environment.NewLine;
                    }
                }
                else
                {
                    if (DateTime.Parse(this.dteAPPLY_DATE.Value.ToString("yyyy/MM/dd")) > DateTime.Parse(DateTime.Now.ToString("yyyy/MM/dd")))
                    {
                        //  若申請日為未來時間, 則提領金額 可以等於 餘額 + (本金 * 0.01)
                        //  1%全部調整為1.2% [Modify By 2019/04/15]
                        if
                            (
                                decimal.TryParse(x.Cells["ORDER_Amount"].Value.ConvertToString().Trim(), out decAmount) &&
                                decimal.TryParse(x.Cells["AmountOut"].Value.ConvertToString().Trim(), out decOutAmount) &&
                                decimal.TryParse(x.Cells["BALANCE"].Value.ConvertToString().Trim(), out decBALANCE)
                            )
                        {
                            if (double.Parse(decOutAmount.ConvertToString()) > double.Parse(decBALANCE.ConvertToString()) + (double.Parse(decAmount.ConvertToString()) * 0.012))
                            {
                                msg += "- 客戶:" + x.Cells["CUST_NAME"].Value.ConvertToString() + ", 可出金申請不能大於 餘額 + 本金 * 1% " + Environment.NewLine;
                            }
                        }

                    }
                }


                if (x.Cells["END_BALANCE_FLAG"].Value.ConvertToString().ToUpper().Trim() != "Y")
                {
                    if (x.Cells["ACTUAL_AmountOut"].Value.ConvertToString() == string.Empty)
                    {
                        msg += "- 客戶:" + x.Cells["CUST_NAME"].Value.ConvertToString() + ", 總出金申請不可為0" + Environment.NewLine;
                    }
                    if
                        (
                            decimal.TryParse(x.Cells["ORDER_Amount"].Value.ConvertToString().Trim(), out decAmount) &&
                            decimal.TryParse(x.Cells["ACTUAL_AmountOut"].Value.ConvertToString().Trim(), out decOutAmount) &&
                            decimal.TryParse(x.Cells["BALANCE"].Value.ConvertToString().Trim(), out decBALANCE)
                        )
                    {
                        if (DateTime.Parse(this.dteAPPLY_DATE.Value.ToString("yyyy/MM/dd")) > DateTime.Parse(DateTime.Now.ToString("yyyy/MM/dd")))
                        {
                            //  出金時間為未來, 一樣可以有1%的空間 [Add By 2018/11/30]
                            //  1%全部調整為1.2% [Modify By 2019/04/15]
                            if (double.Parse(decOutAmount.ConvertToString()) > (double.Parse(decBALANCE.ConvertToString()) - double.Parse(decAmount.ConvertToString())) + (double.Parse(decAmount.ConvertToString()) * 0.012))
                            {
                                msg += "- 客戶:" + x.Cells["CUST_NAME"].Value.ConvertToString() + ", 總出金申請不能大於 餘額 - 本金 + (本金 * 1%) " + Environment.NewLine;
                            }
                        }
                        else
                        {
                            if (decOutAmount > decBALANCE - decAmount)
                            {
                                msg += "- 客戶:" + x.Cells["CUST_NAME"].Value.ConvertToString() + ", 總出金申請不能大於 餘額 – 本金" + Environment.NewLine;
                            }
                        }
                    }
                    else
                    {
                        //  不卡控不合規則的資料, 後面直接判斷, 沒提領金額不寫入Temp
                        //msg += "- 客戶:" + x.Cells["CUST_NAME"].Value.ConvertToString() + ", 出金申請不能大於 餘額 – 本金" + Environment.NewLine;
                    }
                }
                else
                {
                    if (DateTime.Parse(this.dteAPPLY_DATE.Value.ToString("yyyy/MM/dd")) > DateTime.Parse(DateTime.Now.ToString("yyyy/MM/dd")))
                    {
                        //  若申請日為未來時間, 則提領金額 可以等於 餘額 + (本金 * 0.01)
                        //  1%全部調整為1.2% [Modify By 2019/04/15]
                        if
                            (
                                decimal.TryParse(x.Cells["ORDER_Amount"].Value.ConvertToString().Trim(), out decAmount) &&
                                decimal.TryParse(x.Cells["ACTUAL_AmountOut"].Value.ConvertToString().Trim(), out decOutAmount) &&
                                decimal.TryParse(x.Cells["BALANCE"].Value.ConvertToString().Trim(), out decBALANCE)
                            )
                        {
                            if (double.Parse(decOutAmount.ConvertToString()) > double.Parse(decBALANCE.ConvertToString()) + (double.Parse(decAmount.ConvertToString()) * 0.012))
                            {
                                msg += "- 客戶:" + x.Cells["CUST_NAME"].Value.ConvertToString() + ", 總出金申請不能大於 餘額 + 本金 * 1% " + Environment.NewLine;
                            }
                        }

                    }
                }
            });


            if (msg != string.Empty)
            {
                MessageBox.Show(msg);
                result = false;
            }
            return result;
        }

        private void btnBringOrder_Click(object sender, EventArgs e)
        {
            //  批次這裡不做事
            return;
            var frm = new frmContractDetail_Deposit("", "")
            { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
            frm.ShowDialog();
            if (frm.status == MK.Demo.Data.enCommon.enFormStatus.OK)
            {
                this.txtParentOrderNo.Text = frm.strORDER_NO.ConvertToString();
                this.txtParentOrderNo.Tag = frm.strORDER_ID.ConvertToString();
                this.txtAmount_NTD_Order.Text = frm.strAMOUNT_NTD.ConvertToString();
                this.txtAmount_RMB_Order.Text = frm.strAMOUNT_RMB.ConvertToString();
                this.txtAmount_USD_Order.Text = frm.strAMOUNT_USD.ConvertToString();
                this.txtAmount_EUR_Order.Text = frm.strAMOUNT_EUR.ConvertToString();
                this.txtAmount_AUD_Order.Text = frm.strAMOUNT_AUD.ConvertToString();
                this.txtAmount_JPY_Order.Text = frm.strAMOUNT_JPY.ConvertToString();
                this.txtAmount_NZD_Order.Text = frm.strAMOUNT_NZD.ConvertToString();
                //--------------------------------------------------------------
                //get BALANCE
                var bl = new logicCust(Global.IsTestMode).GetBALANCE(frm.strORDER_ID.ConvertToString());
                this.tb_BALANCE.Text = bl.ToString();

                if (this.txtAmount_NTD_Order.Text.Trim() != string.Empty)
                {
                    //this.txtAMOUNT_NTD.Enabled = true;
                    this.txtAMOUNT_NTD.Enabled = false;
                    this.txtAMOUNT_RMB.Enabled = false;
                    this.txtAMOUNT_USD.Enabled = false;
                    this.txtAMOUNT_EUR.Enabled = false;
                    this.txtAMOUNT_AUD.Enabled = false;
                    this.txtAMOUNT_JPY.Enabled = false;
                    this.txtAMOUNT_NZD.Enabled = false;
                    this.lblCurrenct.Text = "NTD";
                }

                if (this.txtAmount_RMB_Order.Text.Trim() != string.Empty)
                {
                    this.txtAMOUNT_NTD.Enabled = false;
                    //this.txtAMOUNT_RMB.Enabled = true;
                    this.txtAMOUNT_RMB.Enabled = false;
                    this.txtAMOUNT_USD.Enabled = false;
                    this.txtAMOUNT_EUR.Enabled = false;
                    this.txtAMOUNT_AUD.Enabled = false;
                    this.txtAMOUNT_JPY.Enabled = false;
                    this.txtAMOUNT_NZD.Enabled = false;
                    this.lblCurrenct.Text = "RMB";
                }

                if (this.txtAmount_USD_Order.Text.Trim() != string.Empty)
                {
                    this.txtAMOUNT_NTD.Enabled = false;
                    this.txtAMOUNT_RMB.Enabled = false;
                    //this.txtAMOUNT_USD.Enabled = true;
                    this.txtAMOUNT_USD.Enabled = false;
                    this.txtAMOUNT_EUR.Enabled = false;
                    this.txtAMOUNT_AUD.Enabled = false;
                    this.txtAMOUNT_JPY.Enabled = false;
                    this.txtAMOUNT_NZD.Enabled = false;
                    this.lblCurrenct.Text = "USD";
                }

                if (this.txtAmount_EUR_Order.Text.Trim() != string.Empty)
                {
                    this.txtAMOUNT_NTD.Enabled = false;
                    this.txtAMOUNT_RMB.Enabled = false;
                    //this.txtAMOUNT_USD.Enabled = true;
                    this.txtAMOUNT_USD.Enabled = false;
                    this.txtAMOUNT_EUR.Enabled = false;
                    this.txtAMOUNT_AUD.Enabled = false;
                    this.txtAMOUNT_JPY.Enabled = false;
                    this.txtAMOUNT_NZD.Enabled = false;
                    this.lblCurrenct.Text = "EUR";
                }

                if (this.txtAmount_AUD_Order.Text.Trim() != string.Empty)
                {
                    this.txtAMOUNT_NTD.Enabled = false;
                    this.txtAMOUNT_RMB.Enabled = false;
                    //this.txtAMOUNT_USD.Enabled = true;
                    this.txtAMOUNT_USD.Enabled = false;
                    this.txtAMOUNT_EUR.Enabled = false;
                    this.txtAMOUNT_AUD.Enabled = false;
                    this.txtAMOUNT_JPY.Enabled = false;
                    this.txtAMOUNT_NZD.Enabled = false;
                    this.lblCurrenct.Text = "AUD";
                }
                if (this.txtAmount_JPY_Order.Text.Trim() != string.Empty)
                {
                    this.txtAMOUNT_NTD.Enabled = false;
                    this.txtAMOUNT_RMB.Enabled = false;
                    this.txtAMOUNT_USD.Enabled = false;
                    this.txtAMOUNT_EUR.Enabled = false;
                    this.txtAMOUNT_AUD.Enabled = false;
                    this.txtAMOUNT_JPY.Enabled = false;
                    this.lblCurrenct.Text = "JPY";
                }
                if (this.txtAmount_NZD_Order.Text.Trim() != string.Empty)
                {
                    this.txtAMOUNT_NTD.Enabled = false;
                    this.txtAMOUNT_RMB.Enabled = false;
                    this.txtAMOUNT_USD.Enabled = false;
                    this.txtAMOUNT_EUR.Enabled = false;
                    this.txtAMOUNT_AUD.Enabled = false;
                    this.txtAMOUNT_JPY.Enabled = false;
                    this.lblCurrenct.Text = "NZD";
                }
            }
            frm.Dispose();
            frm = null;
        }

        private void dte_ValueChanged(object sender, EventArgs e)
        {
            if (this.IsFirstLoad == false)
            {
                (sender as DateTimePicker).Format = DateTimePickerFormat.Long;
                this.UpdateSingleDataToGrid();
            }
        }

        private void txtParentOrderNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            //  批次這裡不做事
            return;
            if (e.KeyChar == (char)Keys.Tab)
            {

            }
        }

        private void txtParentOrderNo_KeyDown(object sender, KeyEventArgs e)
        {
            //  批次這裡不做事
            return;
            if (e.KeyCode == Keys.Tab || e.KeyCode == Keys.Enter)
            {
                this.SetOrderNoDafuleValue(this.txtParentOrderNo.Text.Trim());
            }
        }

        private void SetOrderNoDafuleValue(string strOrderNo, bool IsShowError = true)
        {
            this.txtAMOUNT_NTD.Text = string.Empty;
            this.txtAMOUNT_RMB.Text = string.Empty;
            this.txtAMOUNT_USD.Text = string.Empty;
            this.txtAMOUNT_EUR.Text = string.Empty;
            this.txtAMOUNT_AUD.Text = string.Empty;
            this.txtAMOUNT_JPY.Text = string.Empty;
            this.txtAMOUNT_NZD.Text = string.Empty;

            //  Do Search
            var data = base.Cust.GetUnExpiredOrderDatabyOrderID(strOrderNo);
            if (data.Count > 0)
            {
                var dataSingle = data[0];


                this.txtParentOrderNo.Text = dataSingle.ORDER_NO.ConvertToString();
                this.txtParentOrderNo.Tag = dataSingle.ORDER_ID == null ? "" : dataSingle.ORDER_ID.Value.ConvertToString();
                this.txtAmount_NTD_Order.Text = dataSingle.AMOUNT_NTD == null ? "" : dataSingle.AMOUNT_NTD.Value.ConvertToString();
                this.txtAmount_RMB_Order.Text = dataSingle.AMOUNT_RMB == null ? "" : dataSingle.AMOUNT_RMB.Value.ConvertToString();
                this.txtAmount_USD_Order.Text = dataSingle.AMOUNT_USD == null ? "" : dataSingle.AMOUNT_USD.Value.ConvertToString();
                this.txtAmount_EUR_Order.Text = dataSingle.AMOUNT_EUR == null ? "" : dataSingle.AMOUNT_EUR.Value.ConvertToString();
                this.txtAmount_AUD_Order.Text = dataSingle.AMOUNT_AUD == null ? "" : dataSingle.AMOUNT_AUD.Value.ConvertToString();
                this.txtAmount_JPY_Order.Text = dataSingle.AMOUNT_JPY == null ? "" : dataSingle.AMOUNT_JPY.Value.ConvertToString();
                this.txtAmount_NZD_Order.Text = dataSingle.AMOUNT_NZD == null ? "" : dataSingle.AMOUNT_NZD.Value.ConvertToString();

                var bl = new logicCust(Global.IsTestMode).GetBALANCE(this.txtParentOrderNo.Tag.ConvertToString());
                this.tb_BALANCE.Text = bl.ToString();


                if (this.txtAmount_NTD_Order.Text.Trim() != string.Empty)
                {
                    //this.txtAMOUNT_NTD.Enabled = true;
                    this.txtAMOUNT_NTD.Enabled = false;
                    this.txtAMOUNT_RMB.Enabled = false;
                    this.txtAMOUNT_USD.Enabled = false;
                    this.txtAMOUNT_EUR.Enabled = false;
                    this.txtAMOUNT_AUD.Enabled = false;
                    this.txtAMOUNT_JPY.Enabled = false;
                    this.txtAMOUNT_NZD.Enabled = false;
                    this.lblCurrenct.Text = "NTD";
                }

                if (this.txtAmount_RMB_Order.Text.Trim() != string.Empty)
                {
                    this.txtAMOUNT_NTD.Enabled = false;
                    //this.txtAMOUNT_RMB.Enabled = true;
                    this.txtAMOUNT_RMB.Enabled = false;
                    this.txtAMOUNT_USD.Enabled = false;
                    this.txtAMOUNT_EUR.Enabled = false;
                    this.txtAMOUNT_AUD.Enabled = false;
                    this.txtAMOUNT_JPY.Enabled = false;
                    this.txtAMOUNT_NZD.Enabled = false;
                    this.lblCurrenct.Text = "RMB";
                }

                if (this.txtAmount_USD_Order.Text.Trim() != string.Empty)
                {
                    this.txtAMOUNT_NTD.Enabled = false;
                    this.txtAMOUNT_RMB.Enabled = false;
                    //this.txtAMOUNT_USD.Enabled = true;
                    this.txtAMOUNT_USD.Enabled = false;
                    this.txtAMOUNT_EUR.Enabled = false;
                    this.txtAMOUNT_AUD.Enabled = false;
                    this.txtAMOUNT_JPY.Enabled = false;
                    this.txtAMOUNT_NZD.Enabled = false;
                    this.lblCurrenct.Text = "USD";
                }

                if (this.txtAmount_EUR_Order.Text.Trim() != string.Empty)
                {
                    this.txtAMOUNT_NTD.Enabled = false;
                    this.txtAMOUNT_RMB.Enabled = false;
                    //this.txtAMOUNT_USD.Enabled = true;
                    this.txtAMOUNT_USD.Enabled = false;
                    this.txtAMOUNT_EUR.Enabled = false;
                    this.txtAMOUNT_AUD.Enabled = false;
                    this.txtAMOUNT_JPY.Enabled = false;
                    this.txtAMOUNT_NZD.Enabled = false;
                    this.lblCurrenct.Text = "EUR";
                }

                if (this.txtAmount_AUD_Order.Text.Trim() != string.Empty)
                {
                    this.txtAMOUNT_NTD.Enabled = false;
                    this.txtAMOUNT_RMB.Enabled = false;
                    //this.txtAMOUNT_USD.Enabled = true;
                    this.txtAMOUNT_USD.Enabled = false;
                    this.txtAMOUNT_EUR.Enabled = false;
                    this.txtAMOUNT_AUD.Enabled = false;
                    this.txtAMOUNT_JPY.Enabled = false;
                    this.txtAMOUNT_NZD.Enabled = false;
                    this.lblCurrenct.Text = "AUD";
                }
                if (this.txtAmount_JPY_Order.Text.Trim() != string.Empty)
                {
                    this.txtAMOUNT_NTD.Enabled = false;
                    this.txtAMOUNT_RMB.Enabled = false;
                    this.txtAMOUNT_USD.Enabled = false;
                    this.txtAMOUNT_EUR.Enabled = false;
                    this.txtAMOUNT_AUD.Enabled = false;
                    this.txtAMOUNT_JPY.Enabled = false;
                    this.txtAMOUNT_NZD.Enabled = false;
                    this.lblCurrenct.Text = "JPY";
                }
                if (this.txtAmount_NZD_Order.Text.Trim() != string.Empty)
                {
                    this.txtAMOUNT_NTD.Enabled = false;
                    this.txtAMOUNT_RMB.Enabled = false;
                    this.txtAMOUNT_USD.Enabled = false;
                    this.txtAMOUNT_EUR.Enabled = false;
                    this.txtAMOUNT_AUD.Enabled = false;
                    this.txtAMOUNT_JPY.Enabled = false;
                    this.txtAMOUNT_NZD.Enabled = false;
                    this.lblCurrenct.Text = "NZD";
                }
            }
            else
            {
                //  Clean
                this.txtParentOrderNo.Text = string.Empty;
                this.txtParentOrderNo.Tag = string.Empty;
                this.txtAmount_NTD_Order.Text = string.Empty;
                this.txtAmount_RMB_Order.Text = string.Empty;
                this.txtAmount_USD_Order.Text = string.Empty;
                this.txtAmount_EUR_Order.Text = string.Empty;
                this.txtAmount_AUD_Order.Text = string.Empty;
                this.txtAmount_JPY_Order.Text = string.Empty;
                this.txtAmount_NZD_Order.Text = string.Empty;
                //--------------------------------------------------------------
                this.tb_BALANCE.Text = string.Empty;

                //this.txtAMOUNT_NTD.Enabled = true;
                //this.txtAMOUNT_RMB.Enabled = true;
                //this.txtAMOUNT_USD.Enabled = true;
                this.txtAMOUNT_NTD.Enabled = false;
                this.txtAMOUNT_RMB.Enabled = false;
                this.txtAMOUNT_USD.Enabled = false;
                this.txtAMOUNT_EUR.Enabled = false;
                this.txtAMOUNT_AUD.Enabled = false;
                this.txtAMOUNT_JPY.Enabled = false;
                this.txtAMOUNT_NZD.Enabled = false;
                this.lblCurrenct.Text = string.Empty;
                if (IsShowError)
                {
                    MessageBox.Show("此「Order No」不存在，請重新輸入");
                }
            }

        }

        private void UpdateSingleDataToGrid()
        {
            var dataCurrentRow = this.dgv_header.Rows.Cast<DataGridViewRow>()
                            .Where(x => x.Cells["ORDER_ID"].Value.ConvertToString() == this.txtParentOrderNo.Tag.ConvertToString())
                            .ToList();
            if (dataCurrentRow.Count > 0)
            {
                this.liData.Where(x => (x.ORDER_ID == null ? "" : x.ORDER_ID.Value.ConvertToString()) == this.txtParentOrderNo.Tag.ConvertToString())
                    .ToList()
                    .ForEach(x =>
                    {
                        x.AMOUNT_NTD = this.txtAMOUNT_NTD.Text.Trim().IsNumeric() ? decimal.Parse(this.txtAMOUNT_NTD.Text.Trim()) : (decimal?)null;
                        x.AMOUNT_RMB = this.txtAMOUNT_RMB.Text.Trim().IsNumeric() ? decimal.Parse(this.txtAMOUNT_RMB.Text.Trim()) : (decimal?)null;
                        x.AMOUNT_USD = this.txtAMOUNT_USD.Text.Trim().IsNumeric() ? decimal.Parse(this.txtAMOUNT_USD.Text.Trim()) : (decimal?)null;
                        x.AMOUNT_EUR = this.txtAMOUNT_EUR.Text.Trim().IsNumeric() ? decimal.Parse(this.txtAMOUNT_EUR.Text.Trim()) : (decimal?)null;
                        x.AMOUNT_AUD = this.txtAMOUNT_AUD.Text.Trim().IsNumeric() ? decimal.Parse(this.txtAMOUNT_AUD.Text.Trim()) : (decimal?)null;
                        x.AMOUNT_JPY = this.txtAMOUNT_JPY.Text.Trim().IsNumeric() ? decimal.Parse(this.txtAMOUNT_JPY.Text.Trim()) : (decimal?)null;
                        x.AMOUNT_NZD = this.txtAMOUNT_NZD.Text.Trim().IsNumeric() ? decimal.Parse(this.txtAMOUNT_NZD.Text.Trim()) : (decimal?)null;

                        //if (this.dteAPPLY_DATE.Text.IsDate())
                        //{
                        //    x.APPLY_DATE = DateTime.Parse(this.dteAPPLY_DATE.Text);
                        //}
                        //else
                        //{
                        //    x.APPLY_DATE = (DateTime?)null;
                        //}

                        if (this.dteAPPROVE_DATE.Text.IsDate())
                        {
                            x.APPROVE_DATE = DateTime.Parse(this.dteAPPROVE_DATE.Text);
                        }
                        else
                        {
                            x.APPROVE_DATE = (DateTime?)null;
                        }
                        if (this.dteCHECK_DATE.Text.IsDate())
                        {
                            x.CHECK_DATE = DateTime.Parse(this.dteCHECK_DATE.Text);
                        }
                        else
                        {
                            x.CHECK_DATE = (DateTime?)null;
                        }

                        x.IS_ALL_BALANCE_OUT = this.checkBox1.Checked ? "Y" : "N";

                        switch (x.END_BALANCE_FLAG.ConvertToString().Trim().ToUpper())
                        {
                            case "Y":
                            case "N":

                                break;
                            case "餘額結清":
                                x.END_BALANCE_FLAG = "Y";
                                break;
                            case "派利出金":
                                x.END_BALANCE_FLAG = "N";
                                break;
                            case "利息出金":
                                x.END_BALANCE_FLAG = string.Empty;
                                break;
                            default:
                                break;
                        }
                    });
            }

            this.liData.ForEach(x =>
            {
                x.APPLY_DATE = DateTime.Parse(this.dteAPPLY_DATE.Text);
                x.APPLY_USER = Global.str_UserName;
          
            });

            Parallel.ForEach(this.dgv_header.Rows.Cast<DataGridViewRow>(), x =>
            {
                x.Cells["APPLY_DATE"].Value = this.dteAPPLY_DATE.Value.ToString("yyyy/MM/dd");
            });


            //this.dgv_header.DataSource = null;
            //this.dgv_header.DataSource = this.liData;
            this.BindToGrid();
            //this.dgv_header.Rows.Cast<DataGridViewRow>()
            //    .Where(x => x.Cells["ORDER_ID"].Value.ConvertToString() == this.txtParentOrderNo.Tag.ConvertToString())
            //    .ToList()
            //    .ForEach(x =>
            //    {
            //        x.Cells["check"].Value = true;
            //    });

        }

        private void dgv_header_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //this.checkBox1.Checked = false;
            if (e.RowIndex != -1 && this.dgv_header.Rows.Count > 0)
            {
                this.UpdateSingleDataToGrid();  //  更新目前畫面上資料回系統

                Parallel.ForEach(this.dgv_header.Rows.Cast<DataGridViewRow>(), x =>
                {
                    x.Cells["check"].Value = false;
                });

                this.dgv_header.Rows[e.RowIndex].Cells["check"].Value = true;

                if (this.dgv_header.Columns[e.ColumnIndex].Name == "RowDel")
                {
                    //  刪除
                    this.liData = this.liData.Where(x => x.ORDER_NO != this.dgv_header.Rows[e.RowIndex].Cells["ORDER_NO"].Value.ConvertToString().Trim()).ToList();
                    this.BindToGrid();
                    this.dgv_header_CellClick(this.dgv_header, new DataGridViewCellEventArgs(0, 0));
                }
                var data = this.liData;
                if (e.RowIndex + 1 <= this.liData.Count)
                {
                    data = this.liData.Where(x => x.ORDER_ID == decimal.Parse(this.dgv_header.Rows[e.RowIndex].Cells["ORDER_ID"].Value.ConvertToString())).ToList();
                }

                if (data.Count > 0)
                {
                    var dataSingle = data[0];

                    this.txtParentOrderNo.Text = dataSingle.ORDER_NO.ConvertToString();
                    this.txtParentOrderNo.Tag = dataSingle.ORDER_ID == null ? "" : dataSingle.ORDER_ID.Value.ConvertToString();

                    this.txtAmount_NTD_Order.Text = dataSingle.ORDER_AMOUNT_NTD == null ? "" : dataSingle.ORDER_AMOUNT_NTD.Value.ConvertToString();
                    this.txtAmount_RMB_Order.Text = dataSingle.ORDER_AMOUNT_RMB == null ? "" : dataSingle.ORDER_AMOUNT_RMB.Value.ConvertToString();
                    this.txtAmount_USD_Order.Text = dataSingle.ORDER_AMOUNT_USD == null ? "" : dataSingle.ORDER_AMOUNT_USD.Value.ConvertToString();
                    this.txtAmount_EUR_Order.Text = dataSingle.ORDER_AMOUNT_EUR == null ? "" : dataSingle.ORDER_AMOUNT_EUR.Value.ConvertToString();
                    this.txtAmount_AUD_Order.Text = dataSingle.ORDER_AMOUNT_AUD == null ? "" : dataSingle.ORDER_AMOUNT_AUD.Value.ConvertToString();
                    this.txtAmount_JPY_Order.Text = dataSingle.ORDER_AMOUNT_JPY == null ? "" : dataSingle.ORDER_AMOUNT_JPY.Value.ConvertToString();
                    this.txtAmount_NZD_Order.Text = dataSingle.ORDER_AMOUNT_NZD == null ? "" : dataSingle.ORDER_AMOUNT_NZD.Value.ConvertToString();

                    this.tb_BALANCE.Text = dataSingle.BALANCE == null ? "" : dataSingle.BALANCE.Value.ConvertToString();
                    this.txtInterestTimes.Text = dataSingle.PKG_INTEREST_TIMES == null ? "" : dataSingle.PKG_INTEREST_TIMES.Value.ConvertToString();
                    if (this.txtAmount_NTD_Order.Text.Trim() != string.Empty)
                    {
                        //this.txtAMOUNT_NTD.Enabled = true;
                        this.txtAMOUNT_NTD.Enabled = false;
                        this.txtAMOUNT_RMB.Enabled = false;
                        this.txtAMOUNT_USD.Enabled = false;
                        this.txtAMOUNT_EUR.Enabled = false;
                        this.txtAMOUNT_AUD.Enabled = false;
                        this.txtAMOUNT_JPY.Enabled = false;
                        this.txtAMOUNT_NZD.Enabled = false;
                        this.lblCurrenct.Text = "NTD";
                    }

                    if (this.txtAmount_RMB_Order.Text.Trim() != string.Empty)
                    {
                        this.txtAMOUNT_NTD.Enabled = false;
                        //this.txtAMOUNT_RMB.Enabled = true;
                        this.txtAMOUNT_RMB.Enabled = false;
                        this.txtAMOUNT_USD.Enabled = false;
                        this.txtAMOUNT_EUR.Enabled = false;
                        this.txtAMOUNT_AUD.Enabled = false;
                        this.txtAMOUNT_JPY.Enabled = false;
                        this.txtAMOUNT_NZD.Enabled = false;
                        this.lblCurrenct.Text = "RMB";
                    }

                    if (this.txtAmount_USD_Order.Text.Trim() != string.Empty)
                    {
                        this.txtAMOUNT_NTD.Enabled = false;
                        this.txtAMOUNT_RMB.Enabled = false;
                        //this.txtAMOUNT_USD.Enabled = true;
                        this.txtAMOUNT_USD.Enabled = false;
                        this.txtAMOUNT_EUR.Enabled = false;
                        this.txtAMOUNT_AUD.Enabled = false;
                        this.txtAMOUNT_JPY.Enabled = false;
                        this.txtAMOUNT_NZD.Enabled = false;
                        this.lblCurrenct.Text = "USD";
                    }

                    if (this.txtAmount_EUR_Order.Text.Trim() != string.Empty)
                    {
                        this.txtAMOUNT_NTD.Enabled = false;
                        this.txtAMOUNT_RMB.Enabled = false;
                        //this.txtAMOUNT_USD.Enabled = true;
                        this.txtAMOUNT_USD.Enabled = false;
                        this.txtAMOUNT_EUR.Enabled = false;
                        this.txtAMOUNT_AUD.Enabled = false;
                        this.txtAMOUNT_JPY.Enabled = false;
                        this.txtAMOUNT_NZD.Enabled = false;
                        this.lblCurrenct.Text = "EUR";
                    }

                    if (this.txtAmount_AUD_Order.Text.Trim() != string.Empty)
                    {
                        this.txtAMOUNT_NTD.Enabled = false;
                        this.txtAMOUNT_RMB.Enabled = false;
                        //this.txtAMOUNT_USD.Enabled = true;
                        this.txtAMOUNT_USD.Enabled = false;
                        this.txtAMOUNT_EUR.Enabled = false;
                        this.txtAMOUNT_AUD.Enabled = false;
                        this.txtAMOUNT_JPY.Enabled = false;
                        this.txtAMOUNT_NZD.Enabled = false;
                        this.lblCurrenct.Text = "AUD";
                    }
                    if (this.txtAmount_JPY_Order.Text.Trim() != string.Empty)
                    {
                        this.txtAMOUNT_NTD.Enabled = false;
                        this.txtAMOUNT_RMB.Enabled = false;
                        this.txtAMOUNT_USD.Enabled = false;
                        this.txtAMOUNT_EUR.Enabled = false;
                        this.txtAMOUNT_AUD.Enabled = false;
                        this.txtAMOUNT_JPY.Enabled = false;
                        this.txtAMOUNT_NZD.Enabled = false;
                        this.lblCurrenct.Text = "JPY";
                    }
                    if (this.txtAmount_NZD_Order.Text.Trim() != string.Empty)
                    {
                        this.txtAMOUNT_NTD.Enabled = false;
                        this.txtAMOUNT_RMB.Enabled = false;
                        this.txtAMOUNT_USD.Enabled = false;
                        this.txtAMOUNT_EUR.Enabled = false;
                        this.txtAMOUNT_AUD.Enabled = false;
                        this.txtAMOUNT_JPY.Enabled = false;
                        this.txtAMOUNT_NZD.Enabled = false;
                        this.lblCurrenct.Text = "NZD";
                    }

                    this.txtAMOUNT_NTD.Text = dataSingle.AMOUNT_NTD == null ? "" : dataSingle.AMOUNT_NTD.Value.ConvertToString();
                    this.txtAMOUNT_RMB.Text = dataSingle.AMOUNT_RMB == null ? "" : dataSingle.AMOUNT_RMB.Value.ConvertToString();
                    this.txtAMOUNT_USD.Text = dataSingle.AMOUNT_USD == null ? "" : dataSingle.AMOUNT_USD.Value.ConvertToString();
                    this.txtAMOUNT_EUR.Text = dataSingle.AMOUNT_EUR == null ? "" : dataSingle.AMOUNT_EUR.Value.ConvertToString();
                    this.txtAMOUNT_AUD.Text = dataSingle.AMOUNT_AUD == null ? "" : dataSingle.AMOUNT_AUD.Value.ConvertToString();
                    this.txtAMOUNT_JPY.Text = dataSingle.AMOUNT_JPY == null ? "" : dataSingle.AMOUNT_JPY.Value.ConvertToString();
                    this.txtAMOUNT_NZD.Text = dataSingle.AMOUNT_NZD == null ? "" : dataSingle.AMOUNT_NZD.Value.ConvertToString();

                    //this.dteAPPLY_DATE.Value = DateTime.Now;
                    //this.dteAPPLY_DATE.Text = string.Empty;
                    //this.dteAPPLY_DATE.Format = DateTimePickerFormat.Custom;
                    //this.dteAPPLY_DATE.CustomFormat = " ";

                    this.dteAPPROVE_DATE.Value = DateTime.Now;
                    this.dteAPPROVE_DATE.Text = string.Empty;
                    this.dteAPPROVE_DATE.Format = DateTimePickerFormat.Custom;
                    this.dteAPPROVE_DATE.CustomFormat = " ";


                    this.dteCHECK_DATE.Value = DateTime.Now;
                    this.dteCHECK_DATE.Text = string.Empty;
                    this.dteCHECK_DATE.Format = DateTimePickerFormat.Custom;
                    this.dteCHECK_DATE.CustomFormat = " ";


                    //if (dataSingle.APPLY_DATE != null)
                    //{
                    //    this.dteAPPLY_DATE.Format = DateTimePickerFormat.Long;
                    //    this.dteAPPLY_DATE.Value = dataSingle.APPLY_DATE.Value;
                    //    this.dteAPPLY_DATE.Text = dataSingle.APPLY_DATE.Value.ToString("yyyy/MM/dd");
                    //}

                    if (dataSingle.APPROVE_DATE != null)
                    {
                        this.dteAPPROVE_DATE.Format = DateTimePickerFormat.Long;
                        this.dteAPPROVE_DATE.Value = dataSingle.APPROVE_DATE.Value;
                        this.dteAPPROVE_DATE.Text = dataSingle.APPROVE_DATE.Value.ToString("yyyy/MM/dd");
                    }

                    if (dataSingle.CHECK_DATE != null)
                    {
                        this.dteCHECK_DATE.Format = DateTimePickerFormat.Long;
                        this.dteCHECK_DATE.Value = dataSingle.CHECK_DATE.Value;
                        this.dteCHECK_DATE.Text = dataSingle.CHECK_DATE.Value.ToString("yyyy/MM/dd");
                    }
                    //if (dataSingle.IS_ALL_BALANCE_OUT.ConvertToString().Trim() == "Y")
                    //{
                    //    this.checkBox1.Checked = true;
                    //}
                    //else
                    //{
                    //    this.checkBox1.Checked = false;
                    //}
                    //this.checkBox1_CheckedChanged(this.checkBox1, new EventArgs());

                    //  Bind 銀行資料
                    //this.BindBankInfo(this.dgv_header.Rows[e.RowIndex].Cells["CUST_ID"].Value.ConvertToString());
                    if (this.dgv_header.Columns[e.ColumnIndex].Name == "AmountOut" ||
                        this.dgv_header.Columns[e.ColumnIndex].Name == "ACTUAL_AmountOut")
                    {
                        //this.dgv_header.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly = false;
                        //this.dgv_header.BeginEdit(true);

                        bool IsRebind = false;
                        var frm = new frmDepositOut_BatchAmount(this.liData) { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
                        frm.ShowDialog();
                        if (frm.status == enFormStatus.OK)
                        {
                            var dataUpdate = frm.dataOut;
                            dataUpdate.ForEach(x =>
                            {
                                this.liData
                                    .Where(y => y.ORDER_ID == x.ORDER_ID)
                                    .ForEach(y =>
                                    {
                                        y.AMOUNT_NTD = x.AMOUNT_NTD;
                                        y.AMOUNT_RMB = x.AMOUNT_RMB;
                                        y.AMOUNT_USD = x.AMOUNT_USD;
                                        y.AMOUNT_EUR = x.AMOUNT_EUR;
                                        y.AMOUNT_AUD = x.AMOUNT_AUD;
                                        y.AMOUNT_JPY = x.AMOUNT_JPY;
                                        y.AMOUNT_NZD = x.AMOUNT_NZD;
                                        y.END_BALANCE_FLAG = x.END_BALANCE_FLAG;
                                        y.ACTUAL_WITHDRAWAL_AMT_NTD = x.ACTUAL_WITHDRAWAL_AMT_NTD;
                                        y.ACTUAL_WITHDRAWAL_AMT_RMB = x.ACTUAL_WITHDRAWAL_AMT_RMB;
                                        y.ACTUAL_WITHDRAWAL_AMT_USD = x.ACTUAL_WITHDRAWAL_AMT_USD;
                                        y.ACTUAL_WITHDRAWAL_AMT_EUR = x.ACTUAL_WITHDRAWAL_AMT_EUR;
                                        y.ACTUAL_WITHDRAWAL_AMT_AUD = x.ACTUAL_WITHDRAWAL_AMT_AUD;
                                        y.ACTUAL_WITHDRAWAL_AMT_JPY = x.ACTUAL_WITHDRAWAL_AMT_JPY;
                                        y.ACTUAL_WITHDRAWAL_AMT_NZD = x.ACTUAL_WITHDRAWAL_AMT_NZD;
                                    });
                                IsRebind = true;
                            });
                        }
                        frm.Dispose();
                        frm = null;
                        if (IsRebind)
                        {
                            this.dgv_header.DataSource = null;
                            this.dgv_header.DataSource = this.liData;
                            this.liData.Where(x => (x.ORDER_ID == null ? "" : x.ORDER_ID.Value.ConvertToString()) == this.txtParentOrderNo.Tag.ConvertToString())
                                .ToList()
                                .ForEach(x =>
                                {
                                    this.txtAMOUNT_NTD.Text = x.AMOUNT_NTD == null ? "" : x.AMOUNT_NTD.Value.ConvertToString();
                                    this.txtAMOUNT_RMB.Text = x.AMOUNT_RMB == null ? "" : x.AMOUNT_RMB.Value.ConvertToString();
                                    this.txtAMOUNT_USD.Text = x.AMOUNT_USD == null ? "" : x.AMOUNT_USD.Value.ConvertToString();
                                    this.txtAMOUNT_EUR.Text = x.AMOUNT_EUR == null ? "" : x.AMOUNT_EUR.Value.ConvertToString();
                                    this.txtAMOUNT_AUD.Text = x.AMOUNT_AUD == null ? "" : x.AMOUNT_AUD.Value.ConvertToString();
                                    this.txtAMOUNT_JPY.Text = x.AMOUNT_JPY == null ? "" : x.AMOUNT_JPY.Value.ConvertToString();
                                    this.txtAMOUNT_NZD.Text = x.AMOUNT_NZD == null ? "" : x.AMOUNT_NZD.Value.ConvertToString();
                                });
                            this.BindToGrid();
                            if (this.checkBox2.Checked)
                            {
                                this.SetRemarkByNewContract();
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show("資料不存在");
                }

            }
        }


        private void BindBankInfo(string strCustID)
        {
            var dataBank = base.Cust.GetBankDataByCustID(strCustID);
            //  帶入該筆幣別的銀行資料
            dataBank = dataBank.Where(x => x.CURRENCY.ConvertToString().ToUpper() == this.liData[0].ORDER_Currency.ConvertToString().Trim().ToUpper()).ToList();
            if (dataBank.Count > 0)
            {
                var dataBankSingle = dataBank[0];
                
                this.txtBANK_CNAME.Text = dataBankSingle.BANK_CNAME.ConvertToString();
                if (dataBankSingle.BANK_CNAME2.ConvertToString().Trim() != string.Empty)
                {
                    this.txtBANK_CNAME.Text += "/" + dataBankSingle.BANK_CNAME2.ConvertToString().Trim();
                }
                this.txt_Bankename.Text = dataBankSingle.BANK_ENAME.ConvertToString();
                if (dataBankSingle.BANK_ENAME2.ConvertToString().Trim() != string.Empty)
                {
                    this.txt_Bankename.Text += "/" + dataBankSingle.BANK_ENAME2.ConvertToString().Trim();
                }
                this.txtBRANCH_CNAME.Text = dataBankSingle.BRANCH_CNAME.ConvertToString();
                if (dataBankSingle.BRANCH_CNAME2.ConvertToString().Trim() != string.Empty)
                {
                    this.txtBRANCH_CNAME.Text += "/" + dataBankSingle.BRANCH_CNAME2.ConvertToString().Trim();
                }
                //this.txtACCOUNT_CNAME.Text = dataBankSingle.ACCOUNT_CNAME.ConvertToString();
                this.txtACCOUNT.Text = dataBankSingle.ACCOUNT.ConvertToString();
                if (dataBankSingle.ACCOUNT2.ConvertToString().Trim() != string.Empty)
                {
                    this.txtACCOUNT.Text += "/" + dataBankSingle.ACCOUNT2.ConvertToString().Trim();
                }
                this.txtRemark.Text = dataBankSingle.BANK_C_ADDRESS.ConvertToString();
                this.txtSWIFT_CODE.Text = dataBankSingle.SWIFT_CODE.ConvertToString();
                if (dataBankSingle.SWIFT_CODE2.ConvertToString().Trim() != string.Empty)
                {
                    this.txtSWIFT_CODE.Text += "/" + dataBankSingle.SWIFT_CODE2.ConvertToString().Trim();
                }
                this.checkBox2_CheckedChanged(this.checkBox2, new EventArgs());
            }

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (this.checkBox1.Checked)
            {
                //if (this.txtAMOUNT_USD.Enabled)
                //{
                //    this.txtAMOUNT_USD.Text = this.tb_BALANCE.Text;
                //}
                //else if (this.txtAMOUNT_NTD.Enabled)
                //{
                //    this.txtAMOUNT_NTD.Text = this.tb_BALANCE.Text;
                //}
                //else if (this.txtAMOUNT_RMB.Enabled)
                //{
                //    this.txtAMOUNT_RMB.Text = this.tb_BALANCE.Text;
                //}

                switch (this.lblCurrenct.Text.ToUpper().Trim())
                {
                    case "NTD":
                        this.txtAMOUNT_NTD.Text = this.tb_BALANCE.Text;
                        break;
                    case "USD":
                        this.txtAMOUNT_USD.Text = this.tb_BALANCE.Text;
                        break;
                    case "RMB":
                        this.txtAMOUNT_RMB.Text = this.tb_BALANCE.Text;
                        break;
                    case "EUR":
                        this.txtAMOUNT_EUR.Text = this.tb_BALANCE.Text;
                        break;
                    case "AUD":
                        this.txtAMOUNT_AUD.Text = this.tb_BALANCE.Text;
                        break;
                    case "JPY":
                        this.txtAMOUNT_JPY.Text = this.tb_BALANCE.Text;
                        break;
                    case "NZD":
                        this.txtAMOUNT_NZD.Text = this.tb_BALANCE.Text;
                        break;
                    default:
                        break;
                }
                //  手動更新回畫面
                this.dgv_header.Rows.Cast<DataGridViewRow>()
                    .Where(x => x.Cells["ORDER_NO"].Value.ConvertToString().Trim() == this.txtParentOrderNo.Text.Trim())
                    .ForEach(x =>
                    {
                        x.Cells["AmountOut"].Value = this.tb_BALANCE.Text;
                    });
                this.UpdateSingleDataToGrid();

                if (this.checkBox2.Checked)
                {
                    this.SetRemarkByNewContract();
                }
            }
            else
            {

            }
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {

            if (this.checkBox2.Checked)
            {
                this.txtRemark.Enabled = true;
                this.SetRemarkByNewContract();
            }
            else
            {
                this.txtRemark.Enabled = false;
                this.textBox1.Text = string.Empty;
                Parallel.ForEach(this.dgv_header.Rows.Cast<DataGridViewRow>(), x =>
                {
                    x.Cells["IsTransToNewForm"].Value = string.Empty;
                    x.Cells["NewOrderNo"].Value = string.Empty;
                });
                this.txtRemark.Text = this.txt_Bankename.Text + " / " + this.txtACCOUNT.Text + " / " + this.txtSWIFT_CODE.Text;
            }
        }

        private void SetRemarkByNewContract()
        {
            double dbTry;
            double TotalAmount = 0;
            var strCurrency = "USD";
            Parallel.ForEach(this.dgv_header.Rows.Cast<DataGridViewRow>(), x =>
            {
                //  2019/08/06 改為抓取[提領金額], 不抓取[實際提領金額]
                //if (double.TryParse(x.Cells["ACTUAL_AmountOut"].Value.ConvertToString(), out dbTry))
                if (double.TryParse(x.Cells["AmountOut"].Value.ConvertToString(), out dbTry))
                {
                    TotalAmount += dbTry;
                }
                x.Cells["IsTransToNewForm"].Value = "Y";
                x.Cells["NewOrderNo"].Value = this.textBox1.Text;
                strCurrency = x.Cells["ORDER_Currency"].Value.ConvertToString();
            });
            this.txtRemark.Text = "新合約號碼:" + this.textBox1.Text + ", 內轉紅利 " + strCurrency + " " + TotalAmount;


            this.setSummary();
        }

        private void setSummary()
        {
            double dbTry;
            double TotalAmount1 = 0;
            double TotalAmount2 = 0;
            double TotalAmount2ACTUAL = 0;
            double TotalAmount3 = 0;

            Parallel.ForEach(this.dgv_header.Rows.Cast<DataGridViewRow>(), x =>
            {
                this.txtCustomer.Text = x.Cells["CUST_NAME"].Value.ConvertToString();
                this.txtCustomer_en.Text = x.Cells["cust_en"].Value.ConvertToString();
                this.txtCustomer_en2.Text = x.Cells["cust_en2"].Value.ConvertToString();


                if (double.TryParse(x.Cells["ORDER_Amount"].Value.ConvertToString(), out dbTry))
                {
                    TotalAmount1 += dbTry;
                }
                if (double.TryParse(x.Cells["AmountOut"].Value.ConvertToString(), out dbTry))
                {
                    TotalAmount2 += dbTry;
                }
                if (double.TryParse(x.Cells["ACTUAL_AmountOut"].Value.ConvertToString(), out dbTry))
                {
                    TotalAmount2ACTUAL += dbTry;
                }
                if (double.TryParse(x.Cells["BALANCE"].Value.ConvertToString(), out dbTry))
                {
                    TotalAmount3 += dbTry;
                }
            });

            this.txtAmount1.Text = TotalAmount1.ToString("#,0.00");
            this.txtAmount2.Text = TotalAmount2.ToString("#,0.00");
            this.txtAmount2ACTUAL.Text = TotalAmount2ACTUAL.ToString("#,0.00");
            this.txtAmount3.Text = TotalAmount3.ToString("#,0.00");

            this.txtCustomer_en2.Visible = this.txtCustomer_en2.Text.Trim() != string.Empty;
        }

        private void txtSWIFT_CODE_TextChanged(object sender, EventArgs e)
        {
            this.SetRemarkByNewContract();
        }

        private void dgv_header_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            var txt = e.Control as TextBox;
            e.Control.TextChanged -= Txt_TextChanged;
            e.Control.TextChanged += Txt_TextChanged;

            if (txt != null)
            {
                txt.TextChanged -= Txt_TextChanged;
                txt.TextChanged += Txt_TextChanged;

            }
        }

        private void Txt_TextChanged(object sender, EventArgs e)
        {
            //  Note : 2018/11/27 這裡不會進來了, 改為Dialog處理
            //var txt = sender as TextBox;
            //if (txt != null)
            //{
            //    if (txt.Text.IsNumeric())
            //    {
            //        if (this.txtAMOUNT_USD.Enabled)
            //        {
            //            this.txtAMOUNT_USD.Text = txt.Text;
            //        }
            //        if (this.txtAMOUNT_NTD.Enabled)
            //        {
            //            this.txtAMOUNT_NTD.Text = txt.Text;
            //        }
            //        if (this.txtAMOUNT_RMB.Enabled)
            //        {
            //            this.txtAMOUNT_RMB.Text = txt.Text;
            //        }
            //        this.UpdateSingleDataToGrid();
            //    }

            //}
        }

        private void dgv_header_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1 && e.ColumnIndex != -1)
            {
                //if (this.dgv_header.Columns[e.ColumnIndex].Name == "AmountOut")
                //{
                //    //this.dgv_header.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly = false;
                //    DataGridViewCellStyle style = new DataGridViewCellStyle();
                //    style.BackColor = Color.Yellow;
                //    this.dgv_header.CurrentCell.Style.ApplyStyle(style);
                //}
            }

        }

        private void dgv_header_CellLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1 && e.ColumnIndex != -1)
            {
                if (this.dgv_header.Columns[e.ColumnIndex].Name == "AmountOut" ||
                    this.dgv_header.Columns[e.ColumnIndex].Name == "ACTUAL_AmountOut")
                {
                    //this.dgv_header.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly = false;
                    DataGridViewCellStyle style = new DataGridViewCellStyle();
                    style.BackColor = Color.White;
                    this.dgv_header.CurrentCell.Style.ApplyStyle(style);
                }
                if (this.checkBox2.Checked)
                {
                    this.SetRemarkByNewContract();
                }
            }
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            this.SetRemarkByNewContract();
        }

        private void dgv_header_KeyDown(object sender, KeyEventArgs e)
        {
            if (this.dgv_header.CurrentCell != null)
            {
                //  Todo > 不Work
                //var rowIndex = this.dgv_header.CurrentCell.RowIndex;
                //var cellIndex = this.dgv_header.CurrentCell.ColumnIndex;
                //if (e.KeyCode == Keys.Enter)
                //{
                //    if (rowIndex + 1 <= this.dgv_header.Rows.Count - 1)
                //    {
                        
                //        //this.dgv_header.CurrentCell.Selected = false;
                //        //this.dgv_header.BeginEdit(false);
                //        //this.dgv_header.CurrentCell = this.dgv_header.Rows[rowIndex + 1].Cells[cellIndex];
                //        //this.dgv_header.BeginEdit(true);
                //        //this.dgv_header_CellEnter(this.dgv_header, new DataGridViewCellEventArgs(this.dgv_header.CurrentCell.ColumnIndex, this.dgv_header.CurrentCell.RowIndex));

                //        //this.dgv_header_CellLeave(this.dgv_header, new DataGridViewCellEventArgs(cellIndex, rowIndex));
                //        //this.dgv_header_CellClick(this.dgv_header, new DataGridViewCellEventArgs(this.dgv_header.CurrentCell.ColumnIndex, this.dgv_header.CurrentCell.RowIndex));




                //    }
                //}


                //int totalRows = this.dgv_header.Rows.Count;
                //int rowIndex = this.dgv_header.CurrentCell.OwningRow.Index;
                //if (rowIndex == 0)
                //    return;
                //int colIndex = this.dgv_header.CurrentCell.OwningColumn.Index;
                //DataGridViewRow selectedRow = this.dgv_header.Rows[rowIndex];
                //this.dgv_header.ClearSelection();

                //switch (e.KeyCode)
                //{
                //    case Keys.Down:
                //        //if (rowIndex + 1 <= this.dgv_header.Rows.Count - 1)
                //        //{
                //        //    //this.dgv_header.BeginEdit(false);
                //        //    //this.dgv_header.ClearSelection();
                //        //    //this.dgv_header_CellEnter(this.dgv_header, new DataGridViewCellEventArgs(cellIndex, rowIndex + 1));
                //        //    //this.dgv_header_CellClick(this.dgv_header, new DataGridViewCellEventArgs(cellIndex, rowIndex + 1));

                //        //    //this.dgv_header_CellLeave(this.dgv_header, new DataGridViewCellEventArgs(cellIndex, rowIndex));
                //        //    //this.dgv_header_CellEnter(this.dgv_header, new DataGridViewCellEventArgs(cellIndex, rowIndex + 1));


                //        //}

                //        if (rowIndex + 1 <= this.dgv_header.Rows.Count - 1)
                //        {
                //            this.dgv_header.Rows[rowIndex + 1].Cells[colIndex].Selected = true;
                //            //this.dgv_header.FirstDisplayedScrollingRowIndex = rowIndex + 1;
                //            this.dgv_header_CellEnter(this.dgv_header, new DataGridViewCellEventArgs(colIndex, rowIndex + 1));
                //        }
                //        break;
                //    case Keys.Up:
                //        if (rowIndex - 1 >= 0 && this.dgv_header.Rows.Count > 0)
                //        {
                //            this.dgv_header.Rows[rowIndex - 1].Cells[colIndex].Selected = true;
                //            //this.dgv_header.FirstDisplayedScrollingRowIndex = rowIndex - 1;

                //            this.dgv_header_CellEnter(this.dgv_header, new DataGridViewCellEventArgs(colIndex, rowIndex - 1));
                //        }
                //        //if (rowIndex - 1 >= 0 && this.dgv_header.Rows.Count > 0)
                //        //{
                //        //    //this.dgv_header.BeginEdit(false);
                //        //    //this.dgv_header.ClearSelection();
                //        //    //this.dgv_header_CellEnter(this.dgv_header, new DataGridViewCellEventArgs(cellIndex, rowIndex - 1));
                //        //    //this.dgv_header_CellClick(this.dgv_header, new DataGridViewCellEventArgs(cellIndex, rowIndex - 1));

                //        //    //this.dgv_header_CellLeave(this.dgv_header, new DataGridViewCellEventArgs(cellIndex, rowIndex));
                //        //    //this.dgv_header_CellEnter(this.dgv_header, new DataGridViewCellEventArgs(cellIndex, rowIndex - 1));


                //        //}
                //        break;
                //    default:
                //        break;
                //}

                //if (this.dgv_header.Columns[cellIndex].Name == "AmountOut")
                //{

                //    switch (e.KeyCode)
                //    {
                //        case Keys.Down:
                //            this.dgv_header_CellLeave(this.dgv_header, new DataGridViewCellEventArgs(cellIndex, rowIndex));
                //            if (rowIndex + 1 <= this.dgv_header.Rows.Count - 1)
                //            {
                //                this.dgv_header_CellClick(this.dgv_header, new DataGridViewCellEventArgs(cellIndex, rowIndex + 1));
                //                //this.dgv_header_CellEnter(this.dgv_header, new DataGridViewCellEventArgs(cellIndex, rowIndex + 1));
                //                //this.dgv_header.CurrentCell = this.dgv_header.Rows[rowIndex + 1].Cells[cellIndex];
                //            }
                //            break;
                //        case Keys.Up:
                //            this.dgv_header_CellLeave(this.dgv_header, new DataGridViewCellEventArgs(cellIndex, rowIndex));
                //            if (rowIndex - 1 >= 0 && this.dgv_header.Rows.Count > 0)
                //            {
                //                this.dgv_header_CellClick(this.dgv_header, new DataGridViewCellEventArgs(cellIndex, rowIndex - 1));
                //                //this.dgv_header_CellEnter(this.dgv_header, new DataGridViewCellEventArgs(cellIndex, rowIndex - 1));
                //                //this.dgv_header.CurrentCell = this.dgv_header.Rows[rowIndex - 1].Cells[cellIndex];
                //            }
                //            break;
                //        default:
                //            break;
                //    }
                //}
            }
        }

        private void txtAMOUNT_USD_TextChanged(object sender, EventArgs e)
        {
            //this.dgv_header.Rows.Cast<DataGridViewRow>()
            //    .Where(x => (bool?)x.Cells["check"].Value == true)
            //    .ToList()
            //    .ForEach(x =>
            //    {
            //        switch (x.Cells["ORDER_Currency"].Value.ConvertToString().Trim().ToUpper())
            //        {
            //            case "NTD":
            //                x.Cells["AmountOut"].Value = this.txtAMOUNT_NTD.Text.Trim();
            //                break;
            //            case "USD":
            //                x.Cells["AmountOut"].Value = this.txtAMOUNT_USD.Text.Trim();
            //                break;
            //            case "RMB":
            //                x.Cells["AmountOut"].Value = this.txtAMOUNT_RMB.Text.Trim();
            //                break;
            //            default:
            //                break;
            //        }
            //    });
            this.UpdateSingleDataToGrid();  //  更新目前畫面上資料回系統

        }

        private void dgv_header_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            //string columnsName = this.dgv_header.Columns[e.ColumnIndex].Name;
            //switch (columnsName)
            //{
            //    case "END_BALANCE_FLAG":
            //        if (e.Value != null)
            //        {
            //            if (e.Value.ToString() == "Y")
            //            {
            //                e.Value = "餘額結清";
            //            }

            //            if (e.Value.ToString() == "N" || e.Value.ToString() == string.Empty)
            //            { e.Value = "派利出金"; }

            //        }
            //        else
            //        {

            //            e.Value = "利息出金";
            //        }


            //        break;

            //}
        }

        private void dgv_header_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            this.dgv_header.Rows.Cast<DataGridViewRow>()
                .ForEach(x =>
                {
                    switch (x.Cells["END_BALANCE_FLAG"].Value.ConvertToString().Trim().ToUpper())
                    {
                        case "Y":
                            x.Cells["END_BALANCE_FLAG2"].Value = "餘額結清";
                            break;
                        case "N":
                            x.Cells["END_BALANCE_FLAG2"].Value = "派利出金";
                            break;
                        default:
                            x.Cells["END_BALANCE_FLAG2"].Value = "利息出金";
                            break;
                    }
                });
         

        }

        private void SetSeq()
        {
            int i = 1;
            this.dgv_header.Rows.Cast<DataGridViewRow>().ForEach(x =>
            {
                x.Cells["No"].Value = i.ToString();
                i += 1;
            });
        }

        private void dgv_header_Sorted(object sender, EventArgs e)
        {
            this.SetSeq();
        }

        private void dgv_header_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
