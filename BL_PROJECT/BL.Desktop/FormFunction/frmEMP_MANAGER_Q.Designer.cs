﻿namespace MK_DEMO.Destop.FormFunction
{
    partial class frmEMP_MANAGER_Q
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEMP_MANAGER_Q));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnClose = new System.Windows.Forms.Button();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.dteHIRE_DATE_E = new System.Windows.Forms.DateTimePicker();
            this.dteHIRE_DATE_S = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.txtEmpName = new System.Windows.Forms.TextBox();
            this.txtEmpCode = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnCleanCondition = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.picLoder = new System.Windows.Forms.PictureBox();
            this.dgv_header = new System.Windows.Forms.DataGridView();
            this.txtIB_CODE = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtID_NO = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dteQUIT_DATE_E = new System.Windows.Forms.DateTimePicker();
            this.dteQUIT_DATE_S = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.btnAddNew = new System.Windows.Forms.Button();
            this.check = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.DoModify = new System.Windows.Forms.DataGridViewLinkColumn();
            this.DoDel = new System.Windows.Forms.DataGridViewLinkColumn();
            this.EMP_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SALES_LOGIN_PASSWORD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HIRE_START_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HIRE_END_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID_NUMBER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DATE_OF_BIRTH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TITLE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IB_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SEX = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EMAIL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SUPERVISOR_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SUPERVISOR_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DEPT_VALUE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DEPT_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EMP_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INTERNAL_IB_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PARENT_INTERNAL_SALES_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PARENT_IB_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.picLoder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_header)).BeginInit();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(1079, 578);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(110, 36);
            this.btnClose.TabIndex = 144;
            this.btnClose.Text = "關閉";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // dteHIRE_DATE_E
            // 
            this.dteHIRE_DATE_E.Location = new System.Drawing.Point(696, 5);
            this.dteHIRE_DATE_E.Name = "dteHIRE_DATE_E";
            this.dteHIRE_DATE_E.Size = new System.Drawing.Size(175, 27);
            this.dteHIRE_DATE_E.TabIndex = 158;
            this.dteHIRE_DATE_E.ValueChanged += new System.EventHandler(this.dte_ValueChanged);
            // 
            // dteHIRE_DATE_S
            // 
            this.dteHIRE_DATE_S.Location = new System.Drawing.Point(512, 5);
            this.dteHIRE_DATE_S.Name = "dteHIRE_DATE_S";
            this.dteHIRE_DATE_S.Size = new System.Drawing.Size(171, 27);
            this.dteHIRE_DATE_S.TabIndex = 157;
            this.dteHIRE_DATE_S.ValueChanged += new System.EventHandler(this.dte_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(453, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 19);
            this.label2.TabIndex = 156;
            this.label2.Text = "到職日";
            // 
            // txtEmpName
            // 
            this.txtEmpName.Location = new System.Drawing.Point(311, 5);
            this.txtEmpName.Name = "txtEmpName";
            this.txtEmpName.Size = new System.Drawing.Size(132, 27);
            this.txtEmpName.TabIndex = 152;
            // 
            // txtEmpCode
            // 
            this.txtEmpCode.Location = new System.Drawing.Point(102, 5);
            this.txtEmpCode.Name = "txtEmpCode";
            this.txtEmpCode.Size = new System.Drawing.Size(132, 27);
            this.txtEmpCode.TabIndex = 151;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(240, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 19);
            this.label4.TabIndex = 150;
            this.label4.Text = "員工姓名";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 19);
            this.label5.TabIndex = 149;
            this.label5.Text = "EMP CODE";
            // 
            // btnCleanCondition
            // 
            this.btnCleanCondition.Location = new System.Drawing.Point(1032, 36);
            this.btnCleanCondition.Name = "btnCleanCondition";
            this.btnCleanCondition.Size = new System.Drawing.Size(157, 29);
            this.btnCleanCondition.TabIndex = 148;
            this.btnCleanCondition.Text = "清除查詢條件";
            this.btnCleanCondition.UseVisualStyleBackColor = true;
            this.btnCleanCondition.Click += new System.EventHandler(this.btnCleanCondition_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(1032, 4);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(157, 29);
            this.btnSearch.TabIndex = 147;
            this.btnSearch.Text = "查詢";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // picLoder
            // 
            this.picLoder.BackColor = System.Drawing.Color.Transparent;
            this.picLoder.Image = ((System.Drawing.Image)(resources.GetObject("picLoder.Image")));
            this.picLoder.Location = new System.Drawing.Point(474, 247);
            this.picLoder.Name = "picLoder";
            this.picLoder.Size = new System.Drawing.Size(174, 19);
            this.picLoder.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picLoder.TabIndex = 146;
            this.picLoder.TabStop = false;
            this.picLoder.Visible = false;
            // 
            // dgv_header
            // 
            this.dgv_header.AllowUserToAddRows = false;
            this.dgv_header.AllowUserToDeleteRows = false;
            this.dgv_header.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_header.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_header.ColumnHeadersHeight = 25;
            this.dgv_header.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv_header.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.check,
            this.DoModify,
            this.DoDel,
            this.EMP_CODE,
            this.CNAME,
            this.ENAME,
            this.SALES_LOGIN_PASSWORD,
            this.HIRE_START_DATE,
            this.HIRE_END_DATE,
            this.ID_NUMBER,
            this.DATE_OF_BIRTH,
            this.TITLE,
            this.IB_CODE,
            this.SEX,
            this.EMAIL,
            this.SUPERVISOR_ID,
            this.SUPERVISOR_NAME,
            this.DEPT_VALUE,
            this.DEPT_NAME,
            this.EMP_ID,
            this.INTERNAL_IB_CODE,
            this.PARENT_INTERNAL_SALES_NAME,
            this.PARENT_IB_CODE});
            this.dgv_header.Location = new System.Drawing.Point(9, 71);
            this.dgv_header.Name = "dgv_header";
            this.dgv_header.ReadOnly = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("微軟正黑體", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.RoyalBlue;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Yellow;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_header.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgv_header.RowHeadersWidth = 25;
            this.dgv_header.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgv_header.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(226)))), ((int)(((byte)(239)))));
            this.dgv_header.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_header.RowTemplate.Height = 24;
            this.dgv_header.Size = new System.Drawing.Size(1186, 501);
            this.dgv_header.TabIndex = 145;
            this.dgv_header.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_header_CellClick);
            // 
            // txtIB_CODE
            // 
            this.txtIB_CODE.Location = new System.Drawing.Point(102, 37);
            this.txtIB_CODE.Name = "txtIB_CODE";
            this.txtIB_CODE.Size = new System.Drawing.Size(132, 27);
            this.txtIB_CODE.TabIndex = 160;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 19);
            this.label1.TabIndex = 159;
            this.label1.Text = "IB CODE";
            // 
            // txtID_NO
            // 
            this.txtID_NO.Location = new System.Drawing.Point(311, 37);
            this.txtID_NO.Name = "txtID_NO";
            this.txtID_NO.Size = new System.Drawing.Size(132, 27);
            this.txtID_NO.TabIndex = 162;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(240, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 19);
            this.label3.TabIndex = 161;
            this.label3.Text = "身分證號";
            // 
            // dteQUIT_DATE_E
            // 
            this.dteQUIT_DATE_E.Location = new System.Drawing.Point(696, 37);
            this.dteQUIT_DATE_E.Name = "dteQUIT_DATE_E";
            this.dteQUIT_DATE_E.Size = new System.Drawing.Size(175, 27);
            this.dteQUIT_DATE_E.TabIndex = 165;
            this.dteQUIT_DATE_E.ValueChanged += new System.EventHandler(this.dte_ValueChanged);
            // 
            // dteQUIT_DATE_S
            // 
            this.dteQUIT_DATE_S.Location = new System.Drawing.Point(512, 37);
            this.dteQUIT_DATE_S.Name = "dteQUIT_DATE_S";
            this.dteQUIT_DATE_S.Size = new System.Drawing.Size(171, 27);
            this.dteQUIT_DATE_S.TabIndex = 164;
            this.dteQUIT_DATE_S.ValueChanged += new System.EventHandler(this.dte_ValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(453, 41);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 19);
            this.label6.TabIndex = 163;
            this.label6.Text = "離職日";
            // 
            // btnAddNew
            // 
            this.btnAddNew.Location = new System.Drawing.Point(886, 4);
            this.btnAddNew.Name = "btnAddNew";
            this.btnAddNew.Size = new System.Drawing.Size(140, 29);
            this.btnAddNew.TabIndex = 166;
            this.btnAddNew.Text = "新增員工";
            this.btnAddNew.UseVisualStyleBackColor = true;
            this.btnAddNew.Click += new System.EventHandler(this.btnAddNew_Click);
            // 
            // check
            // 
            this.check.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.check.FillWeight = 25F;
            this.check.HeaderText = "選";
            this.check.MinimumWidth = 25;
            this.check.Name = "check";
            this.check.ReadOnly = true;
            this.check.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.check.Width = 25;
            // 
            // DoModify
            // 
            this.DoModify.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.DoModify.HeaderText = "修改";
            this.DoModify.Name = "DoModify";
            this.DoModify.ReadOnly = true;
            this.DoModify.Width = 80;
            // 
            // DoDel
            // 
            this.DoDel.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.DoDel.HeaderText = "刪除";
            this.DoDel.Name = "DoDel";
            this.DoDel.ReadOnly = true;
            this.DoDel.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.DoDel.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.DoDel.Width = 80;
            // 
            // EMP_CODE
            // 
            this.EMP_CODE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.EMP_CODE.DataPropertyName = "EMP_CODE";
            this.EMP_CODE.HeaderText = "EMP_CODE";
            this.EMP_CODE.Name = "EMP_CODE";
            this.EMP_CODE.ReadOnly = true;
            this.EMP_CODE.Width = 95;
            // 
            // CNAME
            // 
            this.CNAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.CNAME.DataPropertyName = "CNAME";
            this.CNAME.HeaderText = "姓名";
            this.CNAME.Name = "CNAME";
            this.CNAME.ReadOnly = true;
            this.CNAME.Width = 110;
            // 
            // ENAME
            // 
            this.ENAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ENAME.DataPropertyName = "ENAME";
            this.ENAME.HeaderText = "姓名(英)";
            this.ENAME.Name = "ENAME";
            this.ENAME.ReadOnly = true;
            this.ENAME.Width = 220;
            // 
            // SALES_LOGIN_PASSWORD
            // 
            this.SALES_LOGIN_PASSWORD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.SALES_LOGIN_PASSWORD.DataPropertyName = "SALES_LOGIN_PASSWORD";
            this.SALES_LOGIN_PASSWORD.HeaderText = "業務密碼(Web)";
            this.SALES_LOGIN_PASSWORD.Name = "SALES_LOGIN_PASSWORD";
            this.SALES_LOGIN_PASSWORD.ReadOnly = true;
            this.SALES_LOGIN_PASSWORD.Width = 125;
            // 
            // HIRE_START_DATE
            // 
            this.HIRE_START_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.HIRE_START_DATE.DataPropertyName = "HIRE_START_DATE";
            dataGridViewCellStyle1.Format = "d";
            dataGridViewCellStyle1.NullValue = null;
            this.HIRE_START_DATE.DefaultCellStyle = dataGridViewCellStyle1;
            this.HIRE_START_DATE.HeaderText = "到職日";
            this.HIRE_START_DATE.Name = "HIRE_START_DATE";
            this.HIRE_START_DATE.ReadOnly = true;
            this.HIRE_START_DATE.Width = 120;
            // 
            // HIRE_END_DATE
            // 
            this.HIRE_END_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.HIRE_END_DATE.DataPropertyName = "HIRE_END_DATE";
            dataGridViewCellStyle2.Format = "d";
            dataGridViewCellStyle2.NullValue = null;
            this.HIRE_END_DATE.DefaultCellStyle = dataGridViewCellStyle2;
            this.HIRE_END_DATE.HeaderText = "離職日";
            this.HIRE_END_DATE.Name = "HIRE_END_DATE";
            this.HIRE_END_DATE.ReadOnly = true;
            this.HIRE_END_DATE.Width = 120;
            // 
            // ID_NUMBER
            // 
            this.ID_NUMBER.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ID_NUMBER.DataPropertyName = "ID_NUMBER";
            this.ID_NUMBER.HeaderText = "身分證號";
            this.ID_NUMBER.Name = "ID_NUMBER";
            this.ID_NUMBER.ReadOnly = true;
            this.ID_NUMBER.Width = 120;
            // 
            // DATE_OF_BIRTH
            // 
            this.DATE_OF_BIRTH.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.DATE_OF_BIRTH.DataPropertyName = "DATE_OF_BIRTH";
            dataGridViewCellStyle3.Format = "d";
            dataGridViewCellStyle3.NullValue = null;
            this.DATE_OF_BIRTH.DefaultCellStyle = dataGridViewCellStyle3;
            this.DATE_OF_BIRTH.HeaderText = "生日";
            this.DATE_OF_BIRTH.Name = "DATE_OF_BIRTH";
            this.DATE_OF_BIRTH.ReadOnly = true;
            this.DATE_OF_BIRTH.Width = 120;
            // 
            // TITLE
            // 
            this.TITLE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.TITLE.DataPropertyName = "TITLE";
            this.TITLE.HeaderText = "職稱";
            this.TITLE.Name = "TITLE";
            this.TITLE.ReadOnly = true;
            this.TITLE.Width = 90;
            // 
            // IB_CODE
            // 
            this.IB_CODE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.IB_CODE.DataPropertyName = "IB_CODE";
            this.IB_CODE.HeaderText = "IB_CODE";
            this.IB_CODE.Name = "IB_CODE";
            this.IB_CODE.ReadOnly = true;
            this.IB_CODE.Width = 110;
            // 
            // SEX
            // 
            this.SEX.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.SEX.DataPropertyName = "SEX";
            this.SEX.HeaderText = "性別";
            this.SEX.Name = "SEX";
            this.SEX.ReadOnly = true;
            this.SEX.Width = 80;
            // 
            // EMAIL
            // 
            this.EMAIL.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.EMAIL.DataPropertyName = "EMAIL";
            this.EMAIL.HeaderText = "EMAIL";
            this.EMAIL.Name = "EMAIL";
            this.EMAIL.ReadOnly = true;
            this.EMAIL.Width = 150;
            // 
            // SUPERVISOR_ID
            // 
            this.SUPERVISOR_ID.DataPropertyName = "SUPERVISOR_ID";
            this.SUPERVISOR_ID.HeaderText = "SUPERVISOR_ID";
            this.SUPERVISOR_ID.Name = "SUPERVISOR_ID";
            this.SUPERVISOR_ID.ReadOnly = true;
            this.SUPERVISOR_ID.Visible = false;
            // 
            // SUPERVISOR_NAME
            // 
            this.SUPERVISOR_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.SUPERVISOR_NAME.DataPropertyName = "SUPERVISOR_NAME";
            this.SUPERVISOR_NAME.HeaderText = "直屬主管";
            this.SUPERVISOR_NAME.Name = "SUPERVISOR_NAME";
            this.SUPERVISOR_NAME.ReadOnly = true;
            this.SUPERVISOR_NAME.Width = 110;
            // 
            // DEPT_VALUE
            // 
            this.DEPT_VALUE.DataPropertyName = "DEPT_VALUE";
            this.DEPT_VALUE.HeaderText = "DEPT_ID";
            this.DEPT_VALUE.Name = "DEPT_VALUE";
            this.DEPT_VALUE.ReadOnly = true;
            this.DEPT_VALUE.Visible = false;
            // 
            // DEPT_NAME
            // 
            this.DEPT_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.DEPT_NAME.DataPropertyName = "DS_DEPT";
            this.DEPT_NAME.HeaderText = "部門";
            this.DEPT_NAME.Name = "DEPT_NAME";
            this.DEPT_NAME.ReadOnly = true;
            this.DEPT_NAME.Width = 120;
            // 
            // EMP_ID
            // 
            this.EMP_ID.DataPropertyName = "EMP_ID";
            this.EMP_ID.HeaderText = "EMP_ID";
            this.EMP_ID.Name = "EMP_ID";
            this.EMP_ID.ReadOnly = true;
            this.EMP_ID.Visible = false;
            // 
            // INTERNAL_IB_CODE
            // 
            this.INTERNAL_IB_CODE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.INTERNAL_IB_CODE.DataPropertyName = "INTERNAL_IB_CODE";
            this.INTERNAL_IB_CODE.HeaderText = "外圍業務(IB_CODE2)";
            this.INTERNAL_IB_CODE.Name = "INTERNAL_IB_CODE";
            this.INTERNAL_IB_CODE.ReadOnly = true;
            this.INTERNAL_IB_CODE.Width = 170;
            // 
            // PARENT_INTERNAL_SALES_NAME
            // 
            this.PARENT_INTERNAL_SALES_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.PARENT_INTERNAL_SALES_NAME.DataPropertyName = "PARENT_INTERNAL_SALES_NAME";
            this.PARENT_INTERNAL_SALES_NAME.HeaderText = "外圍主管(IB_CODE2)";
            this.PARENT_INTERNAL_SALES_NAME.Name = "PARENT_INTERNAL_SALES_NAME";
            this.PARENT_INTERNAL_SALES_NAME.ReadOnly = true;
            this.PARENT_INTERNAL_SALES_NAME.Width = 170;
            // 
            // PARENT_IB_CODE
            // 
            this.PARENT_IB_CODE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.PARENT_IB_CODE.DataPropertyName = "PARENT_SALES_NAME";
            this.PARENT_IB_CODE.HeaderText = "直屬業務(IB_CODE)";
            this.PARENT_IB_CODE.Name = "PARENT_IB_CODE";
            this.PARENT_IB_CODE.ReadOnly = true;
            this.PARENT_IB_CODE.Width = 170;
            // 
            // frmEMP_MANAGER_Q
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.ClientSize = new System.Drawing.Size(1205, 622);
            this.Controls.Add(this.btnAddNew);
            this.Controls.Add(this.dteQUIT_DATE_E);
            this.Controls.Add(this.dteQUIT_DATE_S);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtID_NO);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtIB_CODE);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.dteHIRE_DATE_E);
            this.Controls.Add(this.dteHIRE_DATE_S);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtEmpName);
            this.Controls.Add(this.txtEmpCode);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnCleanCondition);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.picLoder);
            this.Controls.Add(this.dgv_header);
            this.Font = new System.Drawing.Font("微軟正黑體", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Name = "frmEMP_MANAGER_Q";
            this.Text = "員工管理";
            this.Load += new System.EventHandler(this.frmEMP_MANAGER_Q_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picLoder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_header)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnClose;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.DateTimePicker dteHIRE_DATE_E;
        private System.Windows.Forms.DateTimePicker dteHIRE_DATE_S;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtEmpName;
        private System.Windows.Forms.TextBox txtEmpCode;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnCleanCondition;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.PictureBox picLoder;
        private System.Windows.Forms.DataGridView dgv_header;
        private System.Windows.Forms.TextBox txtIB_CODE;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtID_NO;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dteQUIT_DATE_E;
        private System.Windows.Forms.DateTimePicker dteQUIT_DATE_S;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnAddNew;
        private System.Windows.Forms.DataGridViewCheckBoxColumn check;
        private System.Windows.Forms.DataGridViewLinkColumn DoModify;
        private System.Windows.Forms.DataGridViewLinkColumn DoDel;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMP_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn CNAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn SALES_LOGIN_PASSWORD;
        private System.Windows.Forms.DataGridViewTextBoxColumn HIRE_START_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn HIRE_END_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID_NUMBER;
        private System.Windows.Forms.DataGridViewTextBoxColumn DATE_OF_BIRTH;
        private System.Windows.Forms.DataGridViewTextBoxColumn TITLE;
        private System.Windows.Forms.DataGridViewTextBoxColumn IB_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn SEX;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMAIL;
        private System.Windows.Forms.DataGridViewTextBoxColumn SUPERVISOR_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn SUPERVISOR_NAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn DEPT_VALUE;
        private System.Windows.Forms.DataGridViewTextBoxColumn DEPT_NAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMP_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn INTERNAL_IB_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn PARENT_INTERNAL_SALES_NAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn PARENT_IB_CODE;
    }
}
