﻿using MK.Demo.Logic;
using MK.Demo.Model;
using MK_DEMO.Destop.FormFunction;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MK.Demo.Utility;

namespace MK_DEMO.Destop.FormFunction
{
    public partial class frmNTD_Payment_Job : MK_DEMO.Destop.BaseForm
    {
        public frmNTD_Payment_Job()
        {
            InitializeComponent();

        }

        private void frmNTD_Payment_Job_Load(object sender, EventArgs e)
        {
            this.BindExecuteInfo();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDoJob_Click(object sender, EventArgs e)
        {
            if (base.Cust.ExecNTD_Payment_Job(Global.str_UserName))
            {
                this.BindExecuteInfo();
                MessageBox.Show("台幣提領執行作業-執行成功");
            }
            else
            {
                MessageBox.Show("台幣提領執行作業-執行失敗");
            }
        }

        private void BindExecuteInfo()
        {
            this.lblLastExecuteInfo1.Text = string.Empty;
            this.lblLastExecuteInfo2.Text = string.Empty;
            var data = base.Cust.GetLastExecuteNTD_Payment_Info();
            if (data != null)
            {
                this.lblLastExecuteInfo1.Text = "最後執行日期：" + (data.CREATE_DATE == null ? string.Empty : data.CREATE_DATE.Value.ToString("yyyy/MM/dd HH:mm"));
                this.lblLastExecuteInfo2.Text = "最後執行人員：" + data.CREATE_BY.ConvertToString();
            }

        }

    }
}
