﻿namespace MK_DEMO.Destop.FormFunction
{
    partial class frmContractDetail_Deposit
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmContractDetail_Deposit));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.check = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ORDER_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUST_CNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUST_ENAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SALES = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MT4_ACCOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT_USD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT_NTD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT_RMB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FROM_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.END_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.YEAR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtCustName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dgv_header = new System.Windows.Forms.DataGridView();
            this.btnOK = new System.Windows.Forms.Button();
            this.picLoder = new System.Windows.Forms.PictureBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.Qcheck = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.QORDER_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QCUST_CNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QCUST_ENAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QSALES_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QMT4_ACCOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QAMOUNT_USD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QAMOUNT_NTD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QAMOUNT_RMB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QAMOUNT_EUR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QAMOUNT_AUD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QAMOUNT_JPY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QAMOUNT_NZD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QFROM_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QEND_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QYEAR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QEXTEND_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QORDER_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_header)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLoder)).BeginInit();
            this.SuspendLayout();
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // check
            // 
            this.check.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.check.FillWeight = 25F;
            this.check.HeaderText = "選";
            this.check.MinimumWidth = 25;
            this.check.Name = "check";
            this.check.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.check.Width = 25;
            // 
            // ORDER_NO
            // 
            this.ORDER_NO.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ORDER_NO.DataPropertyName = "ORDER_NO";
            this.ORDER_NO.HeaderText = "合約編號";
            this.ORDER_NO.Name = "ORDER_NO";
            this.ORDER_NO.Width = 90;
            // 
            // CUST_CNAME
            // 
            this.CUST_CNAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.CUST_CNAME.DataPropertyName = "CUST_CNAME";
            this.CUST_CNAME.HeaderText = "姓名";
            this.CUST_CNAME.Name = "CUST_CNAME";
            this.CUST_CNAME.Width = 90;
            // 
            // CUST_ENAME
            // 
            this.CUST_ENAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.CUST_ENAME.DataPropertyName = "CUST_ENAME";
            this.CUST_ENAME.HeaderText = "英文姓名";
            this.CUST_ENAME.Name = "CUST_ENAME";
            this.CUST_ENAME.Width = 90;
            // 
            // SALES
            // 
            this.SALES.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.SALES.DataPropertyName = "SALES_NAME";
            this.SALES.HeaderText = "業務";
            this.SALES.Name = "SALES";
            this.SALES.Width = 90;
            // 
            // MT4_ACCOUNT
            // 
            this.MT4_ACCOUNT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.MT4_ACCOUNT.DataPropertyName = "MT4_ACCOUNT";
            this.MT4_ACCOUNT.HeaderText = "MT4交易帳戶";
            this.MT4_ACCOUNT.Name = "MT4_ACCOUNT";
            this.MT4_ACCOUNT.Width = 90;
            // 
            // AMOUNT_USD
            // 
            this.AMOUNT_USD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.AMOUNT_USD.DataPropertyName = "AMOUNT_USD";
            this.AMOUNT_USD.HeaderText = "入金金額(美元)";
            this.AMOUNT_USD.Name = "AMOUNT_USD";
            this.AMOUNT_USD.Width = 90;
            // 
            // AMOUNT_NTD
            // 
            this.AMOUNT_NTD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.AMOUNT_NTD.DataPropertyName = "AMOUNT_NTD";
            this.AMOUNT_NTD.HeaderText = "入金金額(新台幣)";
            this.AMOUNT_NTD.Name = "AMOUNT_NTD";
            this.AMOUNT_NTD.Width = 90;
            // 
            // AMOUNT_RMB
            // 
            this.AMOUNT_RMB.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.AMOUNT_RMB.DataPropertyName = "AMOUNT_RMB";
            this.AMOUNT_RMB.HeaderText = "入金金額(人民幣)";
            this.AMOUNT_RMB.Name = "AMOUNT_RMB";
            this.AMOUNT_RMB.Width = 90;
            // 
            // FROM_DATE
            // 
            this.FROM_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.FROM_DATE.DataPropertyName = "FROM_DATE";
            this.FROM_DATE.HeaderText = "合約日期";
            this.FROM_DATE.Name = "FROM_DATE";
            this.FROM_DATE.Width = 90;
            // 
            // END_DATE
            // 
            this.END_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.END_DATE.DataPropertyName = "END_DATE";
            this.END_DATE.HeaderText = "合約結束日期";
            this.END_DATE.Name = "END_DATE";
            this.END_DATE.Width = 90;
            // 
            // YEAR
            // 
            this.YEAR.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.YEAR.DataPropertyName = "YEAR";
            this.YEAR.HeaderText = "年期";
            this.YEAR.Name = "YEAR";
            this.YEAR.Width = 90;
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.btnSearch.Location = new System.Drawing.Point(230, 6);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(112, 25);
            this.btnSearch.TabIndex = 119;
            this.btnSearch.Text = "查詢";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtCustName
            // 
            this.txtCustName.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.txtCustName.Location = new System.Drawing.Point(82, 6);
            this.txtCustName.Name = "txtCustName";
            this.txtCustName.Size = new System.Drawing.Size(142, 27);
            this.txtCustName.TabIndex = 118;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 19);
            this.label1.TabIndex = 117;
            this.label1.Text = "客戶姓名";
            // 
            // dgv_header
            // 
            this.dgv_header.AllowUserToAddRows = false;
            this.dgv_header.AllowUserToDeleteRows = false;
            this.dgv_header.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_header.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_header.ColumnHeadersHeight = 25;
            this.dgv_header.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv_header.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Qcheck,
            this.QORDER_NO,
            this.QCUST_CNAME,
            this.QCUST_ENAME,
            this.QSALES_NAME,
            this.QMT4_ACCOUNT,
            this.Column1,
            this.QAMOUNT_USD,
            this.QAMOUNT_NTD,
            this.QAMOUNT_RMB,
            this.QAMOUNT_EUR,
            this.QAMOUNT_AUD,
            this.QAMOUNT_JPY,
            this.QAMOUNT_NZD,
            this.QFROM_DATE,
            this.QEND_DATE,
            this.QYEAR,
            this.QEXTEND_DATE,
            this.QORDER_ID});
            this.dgv_header.Location = new System.Drawing.Point(12, 37);
            this.dgv_header.Name = "dgv_header";
            this.dgv_header.ReadOnly = true;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("微軟正黑體", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.RoyalBlue;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Yellow;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_header.RowHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dgv_header.RowHeadersWidth = 25;
            this.dgv_header.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgv_header.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(226)))), ((int)(((byte)(239)))));
            this.dgv_header.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_header.RowTemplate.Height = 24;
            this.dgv_header.Size = new System.Drawing.Size(779, 304);
            this.dgv_header.TabIndex = 116;
            this.dgv_header.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_header_CellClick);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.btnOK.Location = new System.Drawing.Point(565, 347);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(110, 48);
            this.btnOK.TabIndex = 115;
            this.btnOK.Text = "確定";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // picLoder
            // 
            this.picLoder.BackColor = System.Drawing.Color.Transparent;
            this.picLoder.Image = ((System.Drawing.Image)(resources.GetObject("picLoder.Image")));
            this.picLoder.Location = new System.Drawing.Point(314, 194);
            this.picLoder.Name = "picLoder";
            this.picLoder.Size = new System.Drawing.Size(174, 19);
            this.picLoder.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picLoder.TabIndex = 114;
            this.picLoder.TabStop = false;
            this.picLoder.Visible = false;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.btnClose.Location = new System.Drawing.Point(681, 347);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(110, 48);
            this.btnClose.TabIndex = 5;
            this.btnClose.Text = "關閉";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // Qcheck
            // 
            this.Qcheck.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Qcheck.FillWeight = 25F;
            this.Qcheck.HeaderText = "選";
            this.Qcheck.MinimumWidth = 25;
            this.Qcheck.Name = "Qcheck";
            this.Qcheck.ReadOnly = true;
            this.Qcheck.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Qcheck.Width = 25;
            // 
            // QORDER_NO
            // 
            this.QORDER_NO.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.QORDER_NO.DataPropertyName = "ORDER_NO";
            this.QORDER_NO.HeaderText = "合約編號";
            this.QORDER_NO.Name = "QORDER_NO";
            this.QORDER_NO.ReadOnly = true;
            this.QORDER_NO.Width = 90;
            // 
            // QCUST_CNAME
            // 
            this.QCUST_CNAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.QCUST_CNAME.DataPropertyName = "CUST_CNAME";
            this.QCUST_CNAME.HeaderText = "姓名";
            this.QCUST_CNAME.Name = "QCUST_CNAME";
            this.QCUST_CNAME.ReadOnly = true;
            this.QCUST_CNAME.Width = 130;
            // 
            // QCUST_ENAME
            // 
            this.QCUST_ENAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.QCUST_ENAME.DataPropertyName = "CUST_ENAME";
            this.QCUST_ENAME.HeaderText = "英文姓名";
            this.QCUST_ENAME.Name = "QCUST_ENAME";
            this.QCUST_ENAME.ReadOnly = true;
            this.QCUST_ENAME.Width = 220;
            // 
            // QSALES_NAME
            // 
            this.QSALES_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.QSALES_NAME.DataPropertyName = "SALES_NAME";
            this.QSALES_NAME.HeaderText = "業務";
            this.QSALES_NAME.Name = "QSALES_NAME";
            this.QSALES_NAME.ReadOnly = true;
            this.QSALES_NAME.Width = 130;
            // 
            // QMT4_ACCOUNT
            // 
            this.QMT4_ACCOUNT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.QMT4_ACCOUNT.DataPropertyName = "MT4_ACCOUNT";
            this.QMT4_ACCOUNT.HeaderText = "MT4交易帳戶";
            this.QMT4_ACCOUNT.Name = "QMT4_ACCOUNT";
            this.QMT4_ACCOUNT.ReadOnly = true;
            this.QMT4_ACCOUNT.Width = 150;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Column1.DataPropertyName = "balance";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle1.Format = "N2";
            dataGridViewCellStyle1.NullValue = null;
            this.Column1.DefaultCellStyle = dataGridViewCellStyle1;
            this.Column1.HeaderText = "餘額(美金)";
            this.Column1.MinimumWidth = 100;
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // QAMOUNT_USD
            // 
            this.QAMOUNT_USD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.QAMOUNT_USD.DataPropertyName = "AMOUNT_USD";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "N2";
            dataGridViewCellStyle2.NullValue = null;
            this.QAMOUNT_USD.DefaultCellStyle = dataGridViewCellStyle2;
            this.QAMOUNT_USD.HeaderText = "入金金額(美元)";
            this.QAMOUNT_USD.Name = "QAMOUNT_USD";
            this.QAMOUNT_USD.ReadOnly = true;
            this.QAMOUNT_USD.Width = 150;
            // 
            // QAMOUNT_NTD
            // 
            this.QAMOUNT_NTD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.QAMOUNT_NTD.DataPropertyName = "AMOUNT_NTD";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = null;
            this.QAMOUNT_NTD.DefaultCellStyle = dataGridViewCellStyle3;
            this.QAMOUNT_NTD.HeaderText = "入金金額(新台幣)";
            this.QAMOUNT_NTD.Name = "QAMOUNT_NTD";
            this.QAMOUNT_NTD.ReadOnly = true;
            this.QAMOUNT_NTD.Width = 150;
            // 
            // QAMOUNT_RMB
            // 
            this.QAMOUNT_RMB.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.QAMOUNT_RMB.DataPropertyName = "AMOUNT_RMB";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N2";
            dataGridViewCellStyle4.NullValue = null;
            this.QAMOUNT_RMB.DefaultCellStyle = dataGridViewCellStyle4;
            this.QAMOUNT_RMB.HeaderText = "入金金額(人民幣)";
            this.QAMOUNT_RMB.Name = "QAMOUNT_RMB";
            this.QAMOUNT_RMB.ReadOnly = true;
            this.QAMOUNT_RMB.Width = 150;
            // 
            // QAMOUNT_EUR
            // 
            this.QAMOUNT_EUR.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.QAMOUNT_EUR.DataPropertyName = "AMOUNT_EUR";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "N2";
            this.QAMOUNT_EUR.DefaultCellStyle = dataGridViewCellStyle5;
            this.QAMOUNT_EUR.HeaderText = "入金金額(歐元)";
            this.QAMOUNT_EUR.Name = "QAMOUNT_EUR";
            this.QAMOUNT_EUR.ReadOnly = true;
            this.QAMOUNT_EUR.Width = 150;
            // 
            // QAMOUNT_AUD
            // 
            this.QAMOUNT_AUD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.QAMOUNT_AUD.DataPropertyName = "AMOUNT_AUD";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "N2";
            this.QAMOUNT_AUD.DefaultCellStyle = dataGridViewCellStyle6;
            this.QAMOUNT_AUD.HeaderText = "入金金額(澳幣)";
            this.QAMOUNT_AUD.Name = "QAMOUNT_AUD";
            this.QAMOUNT_AUD.ReadOnly = true;
            this.QAMOUNT_AUD.Width = 150;
            // 
            // QAMOUNT_JPY
            // 
            this.QAMOUNT_JPY.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.QAMOUNT_JPY.DataPropertyName = "AMOUNT_JPY";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Format = "N2";
            this.QAMOUNT_JPY.DefaultCellStyle = dataGridViewCellStyle7;
            this.QAMOUNT_JPY.HeaderText = "入金金額(日幣)";
            this.QAMOUNT_JPY.Name = "QAMOUNT_JPY";
            this.QAMOUNT_JPY.ReadOnly = true;
            this.QAMOUNT_JPY.Width = 150;
            // 
            // QAMOUNT_NZD
            // 
            this.QAMOUNT_NZD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.QAMOUNT_NZD.DataPropertyName = "AMOUNT_NZD";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.Format = "N2";
            this.QAMOUNT_NZD.DefaultCellStyle = dataGridViewCellStyle8;
            this.QAMOUNT_NZD.HeaderText = "入金金額(紐幣)";
            this.QAMOUNT_NZD.Name = "QAMOUNT_NZD";
            this.QAMOUNT_NZD.ReadOnly = true;
            this.QAMOUNT_NZD.Width = 150;
            // 
            // QFROM_DATE
            // 
            this.QFROM_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.QFROM_DATE.DataPropertyName = "FROM_DATE";
            this.QFROM_DATE.HeaderText = "合約日期";
            this.QFROM_DATE.Name = "QFROM_DATE";
            this.QFROM_DATE.ReadOnly = true;
            this.QFROM_DATE.Width = 130;
            // 
            // QEND_DATE
            // 
            this.QEND_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.QEND_DATE.DataPropertyName = "END_DATE";
            this.QEND_DATE.HeaderText = "合約結束日期";
            this.QEND_DATE.Name = "QEND_DATE";
            this.QEND_DATE.ReadOnly = true;
            this.QEND_DATE.Width = 150;
            // 
            // QYEAR
            // 
            this.QYEAR.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.QYEAR.DataPropertyName = "YEAR";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.QYEAR.DefaultCellStyle = dataGridViewCellStyle9;
            this.QYEAR.HeaderText = "年期";
            this.QYEAR.Name = "QYEAR";
            this.QYEAR.ReadOnly = true;
            this.QYEAR.Width = 90;
            // 
            // QEXTEND_DATE
            // 
            this.QEXTEND_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.QEXTEND_DATE.DataPropertyName = "EXTEND_DATE";
            this.QEXTEND_DATE.HeaderText = "出金日期";
            this.QEXTEND_DATE.Name = "QEXTEND_DATE";
            this.QEXTEND_DATE.ReadOnly = true;
            this.QEXTEND_DATE.Width = 130;
            // 
            // QORDER_ID
            // 
            this.QORDER_ID.DataPropertyName = "ORDER_ID";
            this.QORDER_ID.HeaderText = "ORDER_ID";
            this.QORDER_ID.Name = "QORDER_ID";
            this.QORDER_ID.ReadOnly = true;
            this.QORDER_ID.Visible = false;
            // 
            // frmContractDetail_Deposit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.ClientSize = new System.Drawing.Size(803, 407);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.txtCustName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgv_header);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.picLoder);
            this.Controls.Add(this.btnClose);
            this.Font = new System.Drawing.Font("微軟正黑體", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Name = "frmContractDetail_Deposit";
            this.Text = "合約資料";
            this.Load += new System.EventHandler(this.frmContractDetail_Deposit_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_header)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLoder)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.PictureBox picLoder;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.DataGridViewCheckBoxColumn check;
        private System.Windows.Forms.DataGridViewTextBoxColumn ORDER_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUST_CNAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUST_ENAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn SALES;
        private System.Windows.Forms.DataGridViewTextBoxColumn MT4_ACCOUNT;
        private System.Windows.Forms.DataGridViewTextBoxColumn AMOUNT_USD;
        private System.Windows.Forms.DataGridViewTextBoxColumn AMOUNT_NTD;
        private System.Windows.Forms.DataGridViewTextBoxColumn AMOUNT_RMB;
        private System.Windows.Forms.DataGridViewTextBoxColumn FROM_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn END_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn YEAR;
        private System.Windows.Forms.DataGridView dgv_header;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCustName;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Qcheck;
        private System.Windows.Forms.DataGridViewTextBoxColumn QORDER_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn QCUST_CNAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn QCUST_ENAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn QSALES_NAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn QMT4_ACCOUNT;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn QAMOUNT_USD;
        private System.Windows.Forms.DataGridViewTextBoxColumn QAMOUNT_NTD;
        private System.Windows.Forms.DataGridViewTextBoxColumn QAMOUNT_RMB;
        private System.Windows.Forms.DataGridViewTextBoxColumn QAMOUNT_EUR;
        private System.Windows.Forms.DataGridViewTextBoxColumn QAMOUNT_AUD;
        private System.Windows.Forms.DataGridViewTextBoxColumn QAMOUNT_JPY;
        private System.Windows.Forms.DataGridViewTextBoxColumn QAMOUNT_NZD;
        private System.Windows.Forms.DataGridViewTextBoxColumn QFROM_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn QEND_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn QYEAR;
        private System.Windows.Forms.DataGridViewTextBoxColumn QEXTEND_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn QORDER_ID;
    }
}
