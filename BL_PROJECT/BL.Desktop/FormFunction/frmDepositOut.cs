﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using MK.Demo.Utility;
using MK.Demo.Model;
using MK.Demo.Logic;
using System.Threading.Tasks;
using System.Linq;

namespace MK_DEMO.Destop.FormFunction
{
    public partial class frmDepositOut : MK_DEMO.Destop.BaseForm
    {
        private string Order_No = "";
        private bool IsShow_Add_Form = false;

        public frmDepositOut()
        {
            InitializeComponent();

            this.dgv_header.AutoGenerateColumns = false;
            this.dgv_header.MultiSelect = false;
            this.dgv_header.SelectionMode = DataGridViewSelectionMode.FullRowSelect;


            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            this.dgvUnApprove.AutoGenerateColumns = false;
            this.dgvUnApprove.MultiSelect = false;
            this.dgvUnApprove.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            this.dgvErrDeposit.AutoGenerateColumns = false;
            this.dgvErrDeposit.MultiSelect = false;
            this.dgvErrDeposit.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            this.dgvNoTempRecord.AutoGenerateColumns = false;
            this.dgvNoTempRecord.MultiSelect = false;
            this.dgvNoTempRecord.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        public frmDepositOut(string strOrderNo, bool IsShowAddFrm = false) : this()
        {
            this.Order_No = strOrderNo;
            this.IsShow_Add_Form = IsShowAddFrm;
        }

        private void frmDepositOut_Load(object sender, EventArgs e)
        {
            this.CleanCondition();



            if (Global.str_UserName == "JENNIFERCHEN" || Global.str_UserName == "ADMIN" || Global.UserRole.ConvertToString() == "40")
            {
                //btnSubmit.Enabled = true;
                //  2019/03/12 : 只有HK可以有Approve的權限
                btnSubmit.Enabled = false;
                this.button1.Visible = true;
                this.btnComfirm_SalesApply.Visible = true;
            }
            else
            {
                btnSubmit.Enabled = false;
            }

            if (Global.str_UserName == "HK_ADMIN" || Global.UserRole.ConvertToString() == "50")
            {
                this.comboBox1.SelectedIndex = 2;

                this.btnSubmit.Visible = true;
                this.btnSubmit.Enabled = true;
                this.button1.Visible = false;
                this.btnComfirm_SalesApply.Visible = false;
                this.btnDelete.Visible = false;
                this.btnAddNew.Visible = false;
                this.btnReject.Visible = true;
            }
            else
            {

                this.btnReject.Visible = false;
            }

            //  2019/07/05 > 行政人員 and Admin也要有退件功能
            if
                (
                    Global.str_UserName == "ADMIN" ||
                    Global.UserRole.ConvertToString() == "40" ||    //  Admin
                    Global.UserRole.ConvertToString() == "20"   //行政人員
                )
            {
                this.btnReject.Visible = true;
            }

            if (IsShow_Add_Form)
            {
                var frm = new AddDepositOut(string.Empty, string.Empty, this.Order_No) { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
                var IsNeedCloesThisForm = false;
                frm.ShowDialog();
                if (frm.status == MK.Demo.Data.enCommon.enFormStatus.OK)
                {
                    this.BindData();
                }
                else
                {
                    IsNeedCloesThisForm = true;
                }
                frm.Dispose();
                frm = null;

                if (IsNeedCloesThisForm)
                {
                    this.Close();
                }
            }

            if (
                    !(
                      Global.str_UserName.ToUpper() == "ADMIN" ||
                      Global.UserRole.ConvertToString() == "40" ||
                      Global.UserRole.ConvertToString() == "20"   //  行政查核人員
                    )
                )
            {
                //  2019/03/23 非行政人員, 沒有[出金待審批資料] and [ERROR資料] Tab
                this.tabControl1.TabPages.RemoveAt(4);  //  No Temp Record Tab
                this.tabControl1.TabPages.RemoveAt(3);  //  ERROR資料 Tab
                this.tabControl1.TabPages.RemoveAt(2);  //  出金待審批資料 Tab
            }

            this.BindData();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.BindData();
        }


        private void BindData()
        {
            var strORG = Global.ORG_CODE;

            DateTime dtTry;

            string OrderNo = this.txtOrderNo.Text.Trim();
            string strCustName = this.txtCustomerName.Text.Trim();
            string strBatchNo = this.txtBatchNo.Text.Trim();
            DateTime? dtCreateDate_S = (DateTime?)null;
            DateTime? dtCreateDate_E = (DateTime?)null;
            DateTime? dtApplyDate_S = (DateTime?)null;
            DateTime? dtApplyDate_E = (DateTime?)null;

            if (DateTime.TryParse(this.dteCreateDate_S.Text.Trim(), out dtTry)) dtCreateDate_S = dtTry;
            if (DateTime.TryParse(this.dteCreateDate_E.Text.Trim(), out dtTry)) dtCreateDate_E = dtTry;

            if (DateTime.TryParse(this.dteAPPLY_DATE_S.Text.Trim(), out dtTry)) dtApplyDate_S = dtTry;
            if (DateTime.TryParse(this.dteAPPLY_DATE_E.Text.Trim(), out dtTry)) dtApplyDate_E = dtTry;

            var data = base.Cust.GetDepositData(OrderNo, strCustName, dtCreateDate_S, dtCreateDate_E, dtApplyDate_S, dtApplyDate_E, strBatchNo, strORG);
            //this.dgv_header.DataSource = data;
            if (this.comboBox1.Text.Trim() != string.Empty)
            {
                data = data.Where(p => p.SCHEDULE_DESC == this.comboBox1.Text.Trim()).ToList();
            }
            
            this.dgv_header.DataBindByList(data);
            this.dgv_header.Rows.Cast<DataGridViewRow>()
                .ForEach(x =>
                {
                    x.Cells["ModifyRemark"].Value = "整批號調整remark";
                });
            if (data.Count > 0)
            {
                this.BindDetailData(this.dgv_header, 0);
            }

            var dataApproved = base.Cust.GetDepositData_Approved(OrderNo, strCustName, dtCreateDate_S, dtCreateDate_E, dtApplyDate_S, dtApplyDate_E, strBatchNo, strORG);
            this.dataGridView1.DataBindByList(dataApproved);

            if (data.Count == 0 && dataApproved.Count == 0)
            {
                MessageBox.Show("查無資料");
            }
            var decTotalAmount = data.Sum(x => x.Amount);
            var decTotalActAmount = data.Sum(x => x.ACTUAL_Amount);
            this.txtTotalAmount.Text = decTotalAmount == null ? "" : decTotalAmount.Value.ConvertToString();
            this.txtTotalACTAmount.Text = decTotalActAmount == null ? "" : decTotalActAmount.Value.ConvertToString();

            this.tabPage1.Text = "待出金資料 [" + data.Count().ConvertToString() + "]";
            this.tabPage2.Text = "已出金查詢 [" + dataApproved.Count().ConvertToString() + "]";
            this.BindAuditTabPage();
        }

        private void BindAuditTabPage()
        {
            if (
                    Global.str_UserName.ToUpper() == "ADMIN" ||
                    Global.UserRole.ConvertToString() == "40" ||
                    Global.UserRole.ConvertToString() == "20"   //  行政查核人員
                )
            {
                var strORG = Global.ORG_CODE;


                //  1. 已經行政確認, 但香港尚未Approve的資料
                var dataUnApprove = base.Cust.GetDepositData_UnApprove(strORG);
                this.dgvUnApprove.DataBindByList(dataUnApprove);
                this.tabPage3.Text = "出金待審批資料[" + dataUnApprove.Count().ConvertToString() + "]";
                //if (dataUnApprove.Count() > 0)
                //{
                //    this.BindDetailData(this.dgvUnApprove, 0);
                //}
                //  2. Error Table 資料
                var dataErr = base.Cust.GetDepositErrData(strORG);
                dataErr = dataErr.OrderByDescending(x => x.CREATION_DATE).ToList();
                this.dgvErrDeposit.DataBindByList(dataErr);
                this.tabPage4.Text = "ERROR資料[" + dataErr.Count().ConvertToString() + "]";
                //  3. No Temp Record
                var dataNoTempRecord = base.Cust.GetDepositData_NoTempRecord(strORG);
                dataNoTempRecord = dataNoTempRecord
                    .OrderByDescending(x => x.LAST_UPDATE_DATE)
                    .ThenByDescending(x => x.BATCH_WITHDRAWAL_ID)
                    .ToList();
                this.dgvNoTempRecord.DataBindByList(dataNoTempRecord);
                this.tabPage7.Text = "Temp Record [" + dataNoTempRecord.Count().ConvertToString() + "]";
                //if (dataNoTempRecord.Count() > 0)
                //{
                //    this.BindDetailData(this.dgvNoTempRecord, 0);
                //}
            }
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            //  Check勾選項目
            var selection = this.dgv_header.Rows.Cast<DataGridViewRow>()
                .Where(x => (bool?)x.Cells["check2"].Value == true)
                .ToList();
          //  selection = selection.Where(x => x.Cells["PROCESS_TYPE"].Value.ConvertToString().Trim() == string.Empty).ToList();
            if (selection.Count == 0)
            {
                MessageBox.Show("請選取要送出的表單");
            }
            else
            {
                if (selection.Where(x => x.Cells["SCHEDULE"].Value.ConvertToString() == "3").ToList().Count() == selection.Count())
                {
                    
                    //  Do Submit
                    List<string> dataOrderID = new List<string>();
                    selection.ForEach(x =>
                    {
                        dataOrderID.Add(x.Cells["TEMP_ID"].Value.ConvertToString());
                    });

                    //  Do Submit
                    if (base.Cust.DataSubmit_ACCOUNT_TXN_TEMP(dataOrderID, Global.str_UserName))

                    {
                        MessageBox.Show("資料已送出");
                        this.BindData();
                    }
                }
                else
                {

                    MessageBox.Show("請選取待審批的資料");
                }

            }
        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            //var strYear = this.cboYear.Text.ConvertToString();
            //var strMonth = this.cboMonth.Text.ConvertToString();
            //int intTry1, intTry2;
            //DateTime dtTry;
            //var IsValidate = false;
            ////if (int.TryParse(strYear, out intTry1) && int.TryParse(strMonth, out intTry2))
            ////{
            ////    //  只能填寫 >= 目前年月的資料
            ////    if (DateTime.TryParse(strYear + "/" + strMonth + "/01", out dtTry))
            ////    {
            ////        if (dtTry >= new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1))
            ////        {
            ////            IsValidate = true;
            ////        }
            ////    }
            ////}
            ////  Todo > 這個卡控先跳過, 待確認(要卡哪個時間日期欄位)
            //IsValidate = true;

            //if (IsValidate)
            //{
            //    //  開From填寫, 填寫完直接Rebind
            //    var frm = new AddDepositOut(strYear, strMonth) { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
            //    frm.ShowDialog();
            //    if (frm.status == MK.Demo.Data.enCommon.enFormStatus.OK)
            //    {
            //        this.BindData();
            //    }
            //    frm.Dispose();
            //    frm = null;
            //}
            //else
            //{
            //    MessageBox.Show("不符合可以新增資料的年月" + Environment.NewLine + "僅能新增大於等於目前系統年月的資料");
            //}
            //  開From填寫, 填寫完直接Rebind


            var frm = new AddDepositOut(string.Empty, string.Empty) { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
            frm.ShowDialog();
            if (frm.status == MK.Demo.Data.enCommon.enFormStatus.OK)
            {
                this.BindData();
            }
            frm.Dispose();
            frm = null;
        }

        private void dgv_header_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                //Parallel.ForEach(this.dgv_header.Rows.Cast<DataGridViewRow>(), x =>
                //{
                //    x.Cells["check2"].Value = false;
                //});

                //if (this.dgv_header.Rows[e.RowIndex].Cells["SCHEDULE"].Value.ConvertToString().Trim() == "0")
                //{

                //    this.dgv_header.Rows[e.RowIndex].Cells["check2"].Value = true;
                //}

                switch (this.dgv_header.Rows[e.RowIndex].Cells["SCHEDULE"].Value.ConvertToString())
                {
                    case "0":
                    case "3":
                        if ((bool?)this.dgv_header.Rows[e.RowIndex].Cells["check2"].Value == true)
                        {
                            this.dgv_header.Rows[e.RowIndex].Cells["check2"].Value = false;
                        }
                        else
                        {
                            this.dgv_header.Rows[e.RowIndex].Cells["check2"].Value = true;
                        }
                        break;
                    default:
                        this.dgv_header.Rows[e.RowIndex].Cells["check2"].Value = false;
                        break;
                }

            }
        }

        private void dgv_header_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            //this.dgv_header.Rows.Cast<DataGridViewRow>()
            //    .Where(x => x.Cells["SCHEDULE"].Value.ConvertToString() != "0") //不是暫存的資料
            //    .ToList()
            //    .ForEach(x =>
            //    {
            //        x.Cells["check2"].ReadOnly = true;
            //    });
        }

        private void btnCleanCondition_Click(object sender, EventArgs e)
        {
            this.CleanCondition();
        }

        private void dte_ValueChanged(object sender, EventArgs e)
        {
            (sender as DateTimePicker).Format = DateTimePickerFormat.Long;
        }

        private void CleanCondition()
        {
            this.dteCreateDate_S.Value = DateTime.Now;
            this.dteCreateDate_S.Text = string.Empty;
            this.dteCreateDate_S.Format = DateTimePickerFormat.Custom;
            this.dteCreateDate_S.CustomFormat = " ";

            this.dteCreateDate_E.Value = DateTime.Now;
            this.dteCreateDate_E.Text = string.Empty;
            this.dteCreateDate_E.Format = DateTimePickerFormat.Custom;
            this.dteCreateDate_E.CustomFormat = " ";


            this.dteAPPLY_DATE_S.Value = DateTime.Now;
            this.dteAPPLY_DATE_S.Text = string.Empty;
            this.dteAPPLY_DATE_S.Format = DateTimePickerFormat.Custom;
            this.dteAPPLY_DATE_S.CustomFormat = " ";

            this.dteAPPLY_DATE_E.Value = DateTime.Now;
            this.dteAPPLY_DATE_E.Text = string.Empty;
            this.dteAPPLY_DATE_E.Format = DateTimePickerFormat.Custom;
            this.dteAPPLY_DATE_E.CustomFormat = " ";


            this.txtOrderNo.Text = string.Empty;
            this.txtCustomerName.Text = string.Empty;
            this.txtBatchNo.Text = string.Empty;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            //  刪除選取的

            //  Check勾選項目
            var selection = this.dgv_header.Rows.Cast<DataGridViewRow>()
                .Where(x => (bool?)x.Cells["check2"].Value == true)
                .ToList();
            if (selection.Count == 0)
            {
                MessageBox.Show("請選取要刪除的表單");
            }
            else
            {
                //  Do Submit
                List<string> dataOrderID = new List<string>();
                selection.ForEach(x =>
                {
                    dataOrderID.Add(x.Cells["TEMP_ID"].Value.ConvertToString());
                });

                if (base.Cust.DataDelete_ACCOUNT_TXN_TEMP(dataOrderID))
                {
                    MessageBox.Show("資料刪除成功");
                    this.BindData();
                }

            }
        }

        private void chkSelect_CheckedChanged(object sender, EventArgs e)
        {
            Parallel.ForEach(this.dgv_header.Rows.Cast<DataGridViewRow>(), x =>
            {
                if (this.chkSelect.Checked == false)
                {
                    x.Cells["check2"].Value = false;
                }
                else
                {
                    //if (x.Cells["SCHEDULE"].Value.ConvertToString() == "0")
                    //{
                    //    x.Cells["check2"].Value = true;
                    //}
                    //else
                    //{
                    //    x.Cells["check2"].Value = false;
                    //}
                    x.Cells["check2"].Value = true;
                }
            });


        }

        private void button1_Click(object sender, EventArgs e)
        {
            //  Check勾選項目
            var selection = this.dgv_header.Rows.Cast<DataGridViewRow>()
                .Where(x => (bool?)x.Cells["check2"].Value == true)
                .ToList();
            if (selection.Count == 0)
            {
                MessageBox.Show("請選取要送出的表單");
            }
            else
            {
                if (selection.Where(x => x.Cells["SCHEDULE"].Value.ConvertToString() == "0" ).ToList().Count() == selection.Count())
                {
                    //  Do Submit
                    List<string> dataOrderID = new List<string>();
                    selection.ForEach(x =>
                    {
                        dataOrderID.Add(x.Cells["TEMP_ID"].Value.ConvertToString());
                    });

                    if (base.Cust.DataAudit(dataOrderID, Global.str_UserName))
                    {
                        MessageBox.Show("資料已送出");
                        this.BindData();
                    }
                }
                else
                {
                    MessageBox.Show("請選取待查核的資料");
                }
            }
        }

        private void dgv_header_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgv_header_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {

                if ((bool?)this.dgv_header.Rows[e.RowIndex].Cells["check2"].Value == true)
                {
                    this.dgv_header.Rows[e.RowIndex].Cells["check2"].Value = false;
                }
                else
                {
                    this.dgv_header.Rows[e.RowIndex].Cells["check2"].Value = true;
                }

                //  Bind Detail Data
                this.BindDetailData(this.dgv_header, e.RowIndex);


                if (this.dgv_header.Columns[e.ColumnIndex].Name == "SCHEDULE_DESC")
                {
                    if (this.dgv_header.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ConvertToString() == "已退件")
                    {
                        if (Global.str_UserName != "HK_ADMIN" && Global.UserRole.ConvertToString() != "50")
                        {
                            //  開啟編輯模式
                            var frm = new AddDepositOut_Batch
                                (
                                    strOrderID: this.dgv_header.Rows[e.RowIndex].Cells["ORDER_ID"].Value.ConvertToString(),
                                    strBatchID: this.dgv_header.Rows[e.RowIndex].Cells["Batch_No"].Value.ConvertToString(),
                                    IsResend: true
                                )
                            { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
                            frm.ShowDialog();

                            frm.Dispose();
                            frm = null;

                            this.BindData();
                        }
                    }
                }

                if (this.dgv_header.Columns[e.ColumnIndex].Name == "ModifyRemark")
                {
                    //  整批號調整remark
                    var strBeforeRemark = this.dgv_header.Rows[e.RowIndex].Cells["remark"].Value.ConvertToString();
                    var strBatchNo = this.dgv_header.Rows[e.RowIndex].Cells["Batch_No"].Value.ConvertToString();
                    var IsNeedRebind = false;
                    var frm = new FrmDeposit_ChangeRemark(strBatchNo, strBeforeRemark)
                    { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
                    frm.ShowDialog();
                    if (frm.status == MK.Demo.Data.enCommon.enFormStatus.OK)
                    {
                        IsNeedRebind = true;
                    }
                    frm.Dispose();
                    frm = null;
                    if (IsNeedRebind)
                    {
                        this.BindData();
                    }
                }

            }
        }

        private void dgv_header_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            string columnsName = this.dgv_header.Columns[e.ColumnIndex].Name;
            switch (columnsName)
            {
                case "IsTransToNewForm":
                    if (e.Value != null)
                    {
                        if (e.Value.ToString() == "N")
                        { e.Value = string.Empty; }

                    }


                    break;

                case "type":
                    if (e.Value != null)
                    {
                        if (e.Value.ToString() == "Y")
                        {
                            e.Value = "餘額結清";
                        }

                        if (e.Value.ToString() == "N")
                        { e.Value = "派利出金"; }

                    }
                    else
                    {
                      
                         e.Value = "利息出金"; 
                    }


                    break;

            }
        }

        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            string columnsName = this.dgv_header.Columns[e.ColumnIndex].Name;
            switch (columnsName)
            {
              
                

                case "type":
                    if (e.Value != null)
                    {
                        if (e.Value.ToString() == "Y")
                        {
                            e.Value = "餘額結清";
                        }

                        if (e.Value.ToString() == "N" || e.Value.ToString() == "")
                        { e.Value = "派利出金"; }

                    }
                    else
                    {

                        e.Value = "利息出金";
                    }


                    break;

            }
        }

        private void btnReject_Click(object sender, EventArgs e)
        {
            var selection = this.dgv_header.Rows.Cast<DataGridViewRow>()
            .Where(x => (bool?)x.Cells["check2"].Value == true)
            .ToList();
            //  selection = selection.Where(x => x.Cells["PROCESS_TYPE"].Value.ConvertToString().Trim() == string.Empty).ToList();
            if (selection.Count == 0)
            {
                MessageBox.Show("請選取要送出的表單");
            }
            else
            {
                if (Global.str_UserName == "HK_ADMIN" || Global.UserRole.ConvertToString() == "50")
                {

                    if (selection.Where(x => x.Cells["SCHEDULE"].Value.ConvertToString() == "3").ToList().Count() == selection.Count())
                    {

                        var strRejectReason = string.Empty;
                        if (Global.str_UserName == "HK_ADMIN" || Global.UserRole.ConvertToString() == "50")
                        {
                            DialogResult dResult;

                            while (true)
                            {
                                dResult = InputBox("請輸入退件原因", "退件原因", ref strRejectReason);
                                if (dResult == DialogResult.OK)
                                {
                                    if (strRejectReason != string.Empty)
                                    {
                                        break;
                                    }
                                }
                                else
                                {
                                    return;
                                }
                            }
                            if (strRejectReason == string.Empty)
                            {
                                return;
                            }
                        }

                        List<string> dataOrderID = new List<string>();
                        selection.ForEach(x =>
                        {
                            dataOrderID.Add(x.Cells["TEMP_ID"].Value.ConvertToString());
                        });


                        if (base.Cust.ACCOUNT_TXT_TEMP_Reject_TEMPID(dataOrderID, Global.str_UserName, strRejectReason))
                        {


                            if (Global.str_UserName == "HK_ADMIN" || Global.UserRole.ConvertToString() == "50")
                            {
                                var strMailSubject = "出金審批退件通知";
                                var strMailTo = GetConfigValueByKey("MailAudit").Split(new string[] { ";" }, StringSplitOptions.None).ToList();
                                var strMailBody = @"
Dear <br/><br/>
　　出金審批退件通知, <br/>
　　退件原因：" + strRejectReason + @"<br/>
　　以下Order No為被退件之資料<br/>
<table>
<tr><th>Order No</th><th>Batch No</th></tr>
";
                                selection.ForEach(x =>
                                {
                                    strMailBody += @"
<tr><td>" + x.Cells["ORDERNO"].Value.ConvertToString() + " </td><td>" + x.Cells["Batch_No"].Value.ConvertToString() + " </td></tr>";
                                });
                                strMailBody += "</table>";

                                SendMail(strMailTo, strMailSubject, strMailBody);
                            }


                            MessageBox.Show("退件完成");
                            this.BindData();
                        }

                    }
                    else
                    {
                        MessageBox.Show("請選取待審批的資料進行退件作業");
                    }

                }


                //  2019/07/05 > 行政人員 and Admin也要有退件功能
                if
                    (
                        Global.str_UserName == "ADMIN" ||
                        Global.UserRole.ConvertToString() == "40" ||    //  Admin
                        Global.UserRole.ConvertToString() == "20"   //行政人員
                    )
                {


                    if (selection.Where(x => x.Cells["SCHEDULE"].Value.ConvertToString() == "3"|| x.Cells["SCHEDULE"].Value.ConvertToString() == "0").ToList().Count() == selection.Count())
                    {

                        var strRejectReason = string.Empty;

                        List<string> dataOrderID = new List<string>();
                        selection.ForEach(x =>
                        {
                            dataOrderID.Add(x.Cells["TEMP_ID"].Value.ConvertToString());
                        });


                        if (base.Cust.ACCOUNT_TXT_TEMP_Reject_TEMPID(dataOrderID, Global.str_UserName, strRejectReason))
                        {
                            
                            MessageBox.Show("退件完成");
                            this.BindData();
                        }

                    }
                    else
                    {
                        MessageBox.Show("請選取待審批或待查核的資料進行退件作業");
                    }

                }

            }
        }



        public static DialogResult InputBox(string title, string promptText, ref string value)
        {
            Form form = new Form();
            Label label = new Label();
            TextBox textBox = new TextBox();
            Button buttonOk = new Button();
            Button buttonCancel = new Button();

            form.Text = title;
            form.Font = new Font("微軟正黑體", 11);
            label.Text = promptText;
            textBox.Text = value;
            textBox.Multiline = true;

            buttonOk.Text = "OK";
            buttonCancel.Text = "Cancel";
            buttonOk.DialogResult = DialogResult.OK;
            buttonCancel.DialogResult = DialogResult.Cancel;

            label.SetBounds(9, 12, 372, 13);
            textBox.SetBounds(12, 36, 372, 80);
            buttonOk.SetBounds(228, 172, 75, 23);
            buttonCancel.SetBounds(309, 172, 75, 23);

            label.AutoSize = true;
            textBox.Anchor = textBox.Anchor | AnchorStyles.Right;
            buttonOk.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            buttonCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;

            form.ClientSize = new Size(396, 207);
            form.Controls.AddRange(new Control[] { label, textBox, buttonOk, buttonCancel });
            form.ClientSize = new Size(Math.Max(300, label.Right + 10), form.ClientSize.Height);
            form.FormBorderStyle = FormBorderStyle.FixedDialog;
            form.StartPosition = FormStartPosition.CenterScreen;
            form.MinimizeBox = false;
            form.MaximizeBox = false;
            //form.AcceptButton = buttonOk;
            //form.CancelButton = buttonCancel;

            DialogResult dialogResult = form.ShowDialog();
            value = textBox.Text;
            return dialogResult;
        }

        private void BindDetailData(DataGridView dgv, int idx = 0)
        {
            var strNameOrderID = "ORDER_ID";
            var strNameBatchID = "Batch_No";
            switch (dgv.Name)
            {
                case "dgvUnApprove":
                    strNameOrderID = "dataGridViewTextBoxColumn38";
                    strNameBatchID = "dataGridViewTextBoxColumn5";
                    break;
                case "dgvNoTempRecord":
                    strNameOrderID = "dataGridViewTextBoxColumn79";
                    strNameBatchID = "dataGridViewTextBoxColumn47";
                    break;
                default:
                    break;
            }
            var dataSingle = base.Cust.GetBatchDeposit_ResendData
                 (
                     strOrderID: dgv.Rows[idx].Cells[strNameOrderID].Value.ConvertToString(),
                     BatchID: dgv.Rows[idx].Cells[strNameBatchID].Value.ConvertToString()
                 );
            this.txtAPPLY_DATE.Text = string.Empty;
            this.txtRemark.Text = string.Empty;
            this.checkBox2.Checked = false;
            this.textBox1.Text = string.Empty;
            this.comboBox2.Text = string.Empty;
            this.txtCustomer_en2.Text = string.Empty;
            this.txtAmount2ACTUAL.Text = string.Empty;
            this.txtCustomer_en.Text = string.Empty;
            this.txtAmount3.Text = string.Empty;
            this.txtAmount2.Text = string.Empty;
            this.txtAmount1.Text = string.Empty;
            this.txtCustomer.Text = string.Empty;

            this.txtACCOUNT.Text = string.Empty;
            this.txtSWIFT_CODE.Text = string.Empty;
            this.txtBRANCH_CNAME.Text = string.Empty;
            this.txt_Bankename.Text = string.Empty;
            this.txtBANK_CNAME.Text = string.Empty;

            if (dataSingle != null)
            {
                this.txtCustomer.Text = dataSingle.CUST_CNAME.ConvertToString();
                this.txtCustomer_en.Text = dataSingle.CUST_ENAME.ConvertToString();
                this.txtCustomer_en2.Text = dataSingle.CUST_ENAME2.ConvertToString();

                this.txtAmount1.Text = dataSingle.ORDER_Amount == null ? "0" : dataSingle.ORDER_Amount.Value.ToString("#,0.00");
                this.txtAmount2.Text = dataSingle.Amount == null ? "0" : dataSingle.Amount.Value.ToString("#,0.00");
                this.txtAmount2ACTUAL.Text = dataSingle.ACTUAL_Amount == null ? "0" : dataSingle.ACTUAL_Amount.Value.ToString("#,0.00");
                this.txtAmount3.Text = dataSingle.BALANCE == null ? "0" : dataSingle.BALANCE.Value.ToString("#,0.00");

                this.txtAPPLY_DATE.Text = dataSingle.APPLY_DATE == null ? "" : dataSingle.APPLY_DATE.Value.ToString("yyyy/MM/dd");
                this.checkBox2.Checked = dataSingle.BONUS_TSF.ConvertToString() == "Y"; //  內轉新單
                this.comboBox2.Text = dataSingle.WITHDRAWAL_TYPE.ConvertToString() == "WEB" ? "Y" : "";

                this.textBox1.Text = dataSingle.NEW_CONTRACT_NO.ConvertToString();  //  新單合約編號
                this.txtRemark.Text = dataSingle.REMARK.ConvertToString();

                //  銀行資訊
                var dataBank = base.Cust.GetBankDataByCustID(dataSingle.CUST_ID == null ? "" : dataSingle.CUST_ID.Value.ConvertToString());
                dataBank = dataBank.Where(x => x.CURRENCY.ConvertToString().ToUpper() == dataSingle.ORDER_Currency.ConvertToString().Trim().ToUpper()).ToList();
                if (dataBank.Count > 0)
                {
                    var dataBankSingle = dataBank[0];

                    this.txtBANK_CNAME.Text = dataBankSingle.BANK_CNAME.ConvertToString();
                    if (dataBankSingle.BANK_CNAME2.ConvertToString().Trim() != string.Empty)
                    {
                        this.txtBANK_CNAME.Text += "/" + dataBankSingle.BANK_CNAME2.ConvertToString().Trim();
                    }
                    this.txt_Bankename.Text = dataBankSingle.BANK_ENAME.ConvertToString();
                    if (dataBankSingle.BANK_ENAME2.ConvertToString().Trim() != string.Empty)
                    {
                        this.txt_Bankename.Text += "/" + dataBankSingle.BANK_ENAME2.ConvertToString().Trim();
                    }
                    this.txtBRANCH_CNAME.Text = dataBankSingle.BRANCH_CNAME.ConvertToString();
                    if (dataBankSingle.BRANCH_CNAME2.ConvertToString().Trim() != string.Empty)
                    {
                        this.txtBRANCH_CNAME.Text += "/" + dataBankSingle.BRANCH_CNAME2.ConvertToString().Trim();
                    }
                    this.txtACCOUNT.Text = dataBankSingle.ACCOUNT.ConvertToString();
                    if (dataBankSingle.ACCOUNT2.ConvertToString().Trim() != string.Empty)
                    {
                        this.txtACCOUNT.Text += "/" + dataBankSingle.ACCOUNT2.ConvertToString().Trim();
                    }
                    this.txtSWIFT_CODE.Text = dataBankSingle.SWIFT_CODE.ConvertToString();
                    if (dataBankSingle.SWIFT_CODE2.ConvertToString().Trim() != string.Empty)
                    {
                        this.txtSWIFT_CODE.Text += "/" + dataBankSingle.SWIFT_CODE2.ConvertToString().Trim();
                    }
                }
            }
            this.txtCustomer_en2.Visible = this.txtCustomer_en2.Text.Trim() != string.Empty;

        }

        private void BindDetailData_Approved(DataGridView dgv, int idx = 0)
        {
            var strNameOrderID = "dataGridViewTextBoxColumn18";
            var strNameBatchID = "dataGridViewTextBoxColumn1";
            var dataSingle = base.Cust.GetDepositData_Approved_ByBatchID
                 (
                     strOrderID: dgv.Rows[idx].Cells[strNameOrderID].Value.ConvertToString(),
                     BatchID: dgv.Rows[idx].Cells[strNameBatchID].Value.ConvertToString()
                 );
            this.txtAPPLY_DATE.Text = string.Empty;
            this.txtRemark.Text = string.Empty;
            this.checkBox2.Checked = false;
            this.textBox1.Text = string.Empty;
            this.comboBox2.Text = string.Empty;
            this.txtCustomer_en2.Text = string.Empty;
            this.txtAmount2ACTUAL.Text = string.Empty;
            this.txtCustomer_en.Text = string.Empty;
            this.txtAmount3.Text = string.Empty;
            this.txtAmount2.Text = string.Empty;
            this.txtAmount1.Text = string.Empty;
            this.txtCustomer.Text = string.Empty;

            this.txtACCOUNT.Text = string.Empty;
            this.txtSWIFT_CODE.Text = string.Empty;
            this.txtBRANCH_CNAME.Text = string.Empty;
            this.txt_Bankename.Text = string.Empty;
            this.txtBANK_CNAME.Text = string.Empty;

            if (dataSingle != null)
            {
                this.txtCustomer.Text = dataSingle.CUST_CNAME.ConvertToString();
                this.txtCustomer_en.Text = dataSingle.CUST_ENAME.ConvertToString();
                this.txtCustomer_en2.Text = dataSingle.CUST_ENAME2.ConvertToString();

                this.txtAmount1.Text = dataSingle.ORDER_Amount == null ? "0" : dataSingle.ORDER_Amount.Value.ToString("#,0.00");
                this.txtAmount2.Text = dataSingle.Amount == null ? "0" : dataSingle.Amount.Value.ToString("#,0.00");
                this.txtAmount2ACTUAL.Text = dataSingle.ACTUAL_Amount == null ? "0" : dataSingle.ACTUAL_Amount.Value.ToString("#,0.00");
                this.txtAmount3.Text = dataSingle.BALANCE == null ? "0" : dataSingle.BALANCE.Value.ToString("#,0.00");

                this.txtAPPLY_DATE.Text = dataSingle.APPLY_DATE == null ? "" : dataSingle.APPLY_DATE.Value.ToString("yyyy/MM/dd");
                this.checkBox2.Checked = dataSingle.BONUS_TSF.ConvertToString() == "Y"; //  內轉新單
                this.comboBox2.Text = dataSingle.WITHDRAWAL_TYPE.ConvertToString() == "WEB" ? "Y" : "";

                this.textBox1.Text = dataSingle.NEW_CONTRACT_NO.ConvertToString();  //  新單合約編號
                this.txtRemark.Text = dataSingle.REMARK.ConvertToString();

                //  銀行資訊
                var dataBank = base.Cust.GetBankDataByCustID(dataSingle.CUST_ID == null ? "" : dataSingle.CUST_ID.Value.ConvertToString());
                dataBank = dataBank.Where(x => x.CURRENCY.ConvertToString().ToUpper() == dataSingle.ORDER_Currency.ConvertToString().Trim().ToUpper()).ToList();
                if (dataBank.Count > 0)
                {
                    var dataBankSingle = dataBank[0];

                    this.txtBANK_CNAME.Text = dataBankSingle.BANK_CNAME.ConvertToString();
                    if (dataBankSingle.BANK_CNAME2.ConvertToString().Trim() != string.Empty)
                    {
                        this.txtBANK_CNAME.Text += "/" + dataBankSingle.BANK_CNAME2.ConvertToString().Trim();
                    }
                    this.txt_Bankename.Text = dataBankSingle.BANK_ENAME.ConvertToString();
                    if (dataBankSingle.BANK_ENAME2.ConvertToString().Trim() != string.Empty)
                    {
                        this.txt_Bankename.Text += "/" + dataBankSingle.BANK_ENAME2.ConvertToString().Trim();
                    }
                    this.txtBRANCH_CNAME.Text = dataBankSingle.BRANCH_CNAME.ConvertToString();
                    if (dataBankSingle.BRANCH_CNAME2.ConvertToString().Trim() != string.Empty)
                    {
                        this.txtBRANCH_CNAME.Text += "/" + dataBankSingle.BRANCH_CNAME2.ConvertToString().Trim();
                    }
                    this.txtACCOUNT.Text = dataBankSingle.ACCOUNT.ConvertToString();
                    if (dataBankSingle.ACCOUNT2.ConvertToString().Trim() != string.Empty)
                    {
                        this.txtACCOUNT.Text += "/" + dataBankSingle.ACCOUNT2.ConvertToString().Trim();
                    }
                    this.txtSWIFT_CODE.Text = dataBankSingle.SWIFT_CODE.ConvertToString();
                    if (dataBankSingle.SWIFT_CODE2.ConvertToString().Trim() != string.Empty)
                    {
                        this.txtSWIFT_CODE.Text += "/" + dataBankSingle.SWIFT_CODE2.ConvertToString().Trim();
                    }
                }
            }
            this.txtCustomer_en2.Visible = this.txtCustomer_en2.Text.Trim() != string.Empty;

        }

        private void BindDetailData_Error(DataGridView dgv, int idx = 0)
        {
            var strNameOrderID = "dataGridViewTextBoxColumn61";
            var strNameBatchID = "dataGridViewTextBoxColumn43";
            var dataSingle = base.Cust.GetDepositData_Error_ByBatchID
                 (
                     strOrderID: dgv.Rows[idx].Cells[strNameOrderID].Value.ConvertToString(),
                     BatchID: dgv.Rows[idx].Cells[strNameBatchID].Value.ConvertToString()
                 );
            this.txtAPPLY_DATE.Text = string.Empty;
            this.txtRemark.Text = string.Empty;
            this.checkBox2.Checked = false;
            this.textBox1.Text = string.Empty;
            this.comboBox2.Text = string.Empty;
            this.txtCustomer_en2.Text = string.Empty;
            this.txtAmount2ACTUAL.Text = string.Empty;
            this.txtCustomer_en.Text = string.Empty;
            this.txtAmount3.Text = string.Empty;
            this.txtAmount2.Text = string.Empty;
            this.txtAmount1.Text = string.Empty;
            this.txtCustomer.Text = string.Empty;

            this.txtACCOUNT.Text = string.Empty;
            this.txtSWIFT_CODE.Text = string.Empty;
            this.txtBRANCH_CNAME.Text = string.Empty;
            this.txt_Bankename.Text = string.Empty;
            this.txtBANK_CNAME.Text = string.Empty;

            if (dataSingle != null)
            {
                this.txtCustomer.Text = dataSingle.CUST_CNAME.ConvertToString();
                this.txtCustomer_en.Text = dataSingle.CUST_ENAME.ConvertToString();
                this.txtCustomer_en2.Text = dataSingle.CUST_ENAME2.ConvertToString();

                this.txtAmount1.Text = dataSingle.ORDER_Amount == null ? "0" : dataSingle.ORDER_Amount.Value.ToString("#,0.00");
                this.txtAmount2.Text = dataSingle.Amount == null ? "0" : dataSingle.Amount.Value.ToString("#,0.00");
                this.txtAmount2ACTUAL.Text = dataSingle.ACTUAL_Amount == null ? "0" : dataSingle.ACTUAL_Amount.Value.ToString("#,0.00");
                this.txtAmount3.Text = dataSingle.BALANCE == null ? "0" : dataSingle.BALANCE.Value.ToString("#,0.00");

                this.txtAPPLY_DATE.Text = dataSingle.CREATION_DATE == null ? "" : dataSingle.CREATION_DATE.Value.ToString("yyyy/MM/dd");
                this.checkBox2.Checked = dataSingle.BONUS_TSF.ConvertToString() == "Y"; //  內轉新單
                this.comboBox2.Text = dataSingle.WITHDRAWAL_TYPE.ConvertToString() == "WEB" ? "Y" : "";

                this.textBox1.Text = dataSingle.NEW_CONTRACT_NO.ConvertToString();  //  新單合約編號
                this.txtRemark.Text = dataSingle.REMARK.ConvertToString();

                //  銀行資訊
                var dataBank = base.Cust.GetBankDataByCustID(dataSingle.CUST_ID == null ? "" : dataSingle.CUST_ID.Value.ConvertToString());
                dataBank = dataBank.Where(x => x.CURRENCY.ConvertToString().ToUpper() == dataSingle.ORDER_Currency.ConvertToString().Trim().ToUpper()).ToList();
                if (dataBank.Count > 0)
                {
                    var dataBankSingle = dataBank[0];

                    this.txtBANK_CNAME.Text = dataBankSingle.BANK_CNAME.ConvertToString();
                    if (dataBankSingle.BANK_CNAME2.ConvertToString().Trim() != string.Empty)
                    {
                        this.txtBANK_CNAME.Text += "/" + dataBankSingle.BANK_CNAME2.ConvertToString().Trim();
                    }
                    this.txt_Bankename.Text = dataBankSingle.BANK_ENAME.ConvertToString();
                    if (dataBankSingle.BANK_ENAME2.ConvertToString().Trim() != string.Empty)
                    {
                        this.txt_Bankename.Text += "/" + dataBankSingle.BANK_ENAME2.ConvertToString().Trim();
                    }
                    this.txtBRANCH_CNAME.Text = dataBankSingle.BRANCH_CNAME.ConvertToString();
                    if (dataBankSingle.BRANCH_CNAME2.ConvertToString().Trim() != string.Empty)
                    {
                        this.txtBRANCH_CNAME.Text += "/" + dataBankSingle.BRANCH_CNAME2.ConvertToString().Trim();
                    }
                    this.txtACCOUNT.Text = dataBankSingle.ACCOUNT.ConvertToString();
                    if (dataBankSingle.ACCOUNT2.ConvertToString().Trim() != string.Empty)
                    {
                        this.txtACCOUNT.Text += "/" + dataBankSingle.ACCOUNT2.ConvertToString().Trim();
                    }
                    this.txtSWIFT_CODE.Text = dataBankSingle.SWIFT_CODE.ConvertToString();
                    if (dataBankSingle.SWIFT_CODE2.ConvertToString().Trim() != string.Empty)
                    {
                        this.txtSWIFT_CODE.Text += "/" + dataBankSingle.SWIFT_CODE2.ConvertToString().Trim();
                    }
                }
            }
            this.txtCustomer_en2.Visible = this.txtCustomer_en2.Text.Trim() != string.Empty;

        }


        private void txtAmount3_TextChanged(object sender, EventArgs e)
        {

        }

        private void label18_Click(object sender, EventArgs e)
        {

        }

        private void dgvNoTempRecord_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            string columnsName = this.dgvNoTempRecord.Columns[e.ColumnIndex].Name;
            switch (columnsName)
            {
                case "dataGridViewTextBoxColumn75":
                    if (e.Value != null)
                    {
                        if (e.Value.ToString() == "N")
                        { e.Value = string.Empty; }
                    }
                    break;
                case "dataGridViewTextBoxColumn50":
                    if (e.Value != null)
                    {
                        if (e.Value.ToString() == "Y")
                        {
                            e.Value = "餘額結清";
                        }
                        if (e.Value.ToString() == "N")
                        { e.Value = "派利出金"; }
                    }
                    else
                    {
                        e.Value = "利息出金";
                    }
                    break;

            }
        }

        private void btnComfirm_SalesApply_Click(object sender, EventArgs e)
        {
            //  Check勾選項目
            var selection = this.dgv_header.Rows.Cast<DataGridViewRow>()
                .Where(x => (bool?)x.Cells["check2"].Value == true)
                .ToList();
            if (selection.Count == 0)
            {
                MessageBox.Show("請選取要送出的表單");
            }
            else
            {
                if (selection.Where(x => x.Cells["SCHEDULE"].Value.ConvertToString() == "8").ToList().Count() == selection.Count())
                {
                    //  Do Submit
                    List<string> dataOrderID = new List<string>();
                    selection.ForEach(x =>
                    {
                        dataOrderID.Add(x.Cells["TEMP_ID"].Value.ConvertToString());
                    });

                    if (base.Cust.DataConfirm_SalesApply(dataOrderID, Global.str_UserName))
                    {
                        MessageBox.Show("資料已送出");
                        this.BindData();
                    }
                }
                else
                {
                    MessageBox.Show("請選取待確認的資料");
                }
            }
        }

        private void dgvUnApprove_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                //  Bind Detail Data
                this.BindDetailData(this.dgvUnApprove, e.RowIndex);
            }
        }

        private void dgvNoTempRecord_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                //  Bind Detail Data
                this.BindDetailData(this.dgvNoTempRecord, e.RowIndex);
            }
        }

        private void dgvErrDeposit_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                this.BindDetailData_Error(this.dgvErrDeposit, e.RowIndex);
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {

                this.BindDetailData_Approved(this.dataGridView1, e.RowIndex);
            }
        }

        private void btnBatchModifyRemark_Click(object sender, EventArgs e)
        {
            //  Check勾選項目
            var selection = this.dgv_header.Rows.Cast<DataGridViewRow>()
                .Where(x => (bool?)x.Cells["check2"].Value == true)
                .ToList();

            if (selection.Count() == 1)
            {
                var strBeforeRemark = selection[0].Cells["remark"].Value.ConvertToString();
                var strBatchNo = selection[0].Cells["Batch_No"].Value.ConvertToString();
                var IsNeedRebind = false;
                var frm = new FrmDeposit_ChangeRemark(strBatchNo, strBeforeRemark)
                { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
                frm.ShowDialog();
                if (frm.status == MK.Demo.Data.enCommon.enFormStatus.OK)
                {
                    IsNeedRebind = true;
                }
                frm.Dispose();
                frm = null;
                if (IsNeedRebind)
                {
                    this.BindData();
                }
            }
            else
            {
                MessageBox.Show("請單選待出金資料");
            }
            
        }
    }
}
