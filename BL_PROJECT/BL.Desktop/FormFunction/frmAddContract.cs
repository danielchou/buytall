﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using MK.Demo.Utility;
using MK.Demo.Model;
using MK.Demo.Logic;
using System.Threading.Tasks;
using System.Linq;
using Newtonsoft.Json;

namespace MK_DEMO.Destop.FormFunction
{
    public partial class frmAddContract : MK_DEMO.Destop.BaseForm
    {
        private string strCustID;
        private string strOrderID = "";
        private bool IsViewMode = false;
        //   private bool isFirstLoad = true;
        private bool IsOnlyCust = false;
        /// <summary>
        /// 修改重送
        /// </summary>
        private bool IsResend = false;

        private bool IsRejectByHK = false;
        /// <summary>
        /// 行政人員修改模式
        /// </summary>
        private bool IsAuditModifyMode = false;

        private bool IsOrderVoid = false;

        public bool IsOrderend = false;


        public frmAddContract(string strCustID, bool IsOnlyCust = false)
        {
            InitializeComponent();

            this.strCustID = strCustID;

            this.dteBirthday.Value = DateTime.Now;
            this.dteBirthday.Text = string.Empty;
            this.dteBirthday.Format = DateTimePickerFormat.Custom;
            this.dteBirthday.CustomFormat = " ";

            this.dteBirthday2.Value = DateTime.Now;
            this.dteBirthday2.Text = string.Empty;
            this.dteBirthday2.Format = DateTimePickerFormat.Custom;
            this.dteBirthday2.CustomFormat = " ";

            //this.dteFromDate.Value = DateTime.Now;
            //this.dteFromDate.Text = string.Empty;
            //this.dteFromDate.Format = DateTimePickerFormat.Custom;
            //this.dteFromDate.CustomFormat = " ";
            this.dteFromDate.Value = DateTime.Now;
            this.dteFromDate.Format = DateTimePickerFormat.Custom;

            //this.dteEndDate.Value = DateTime.Now;
            //this.dteEndDate.Text = string.Empty;
            //this.dteEndDate.Format = DateTimePickerFormat.Custom;
            //this.dteEndDate.CustomFormat = " ";

            this.dteExtendDate.Value = DateTime.Now;
            this.dteExtendDate.Text = string.Empty;
            this.dteExtendDate.Format = DateTimePickerFormat.Custom;
            this.dteExtendDate.CustomFormat = " ";

            this.dteOrderEnd.Value = DateTime.Now;
            this.dteOrderEnd.Text = string.Empty;
            this.dteOrderEnd.Format = DateTimePickerFormat.Custom;
            this.dteOrderEnd.CustomFormat = " ";

            this.lbl2020Condition.Text = string.Empty;  //  Default Empty

            this.SetModifyALLStatus(false); //  Default 不可編輯
            this.IsOnlyCust = IsOnlyCust;
            if (this.IsOnlyCust)
            {
                this.IsViewMode = false;
                this.tabControl1.TabPages.RemoveAt(2);
                this.btnSubmit.Visible = false;
                this.btnComfirmModify.Visible = true;
                this.SetModifyALLStatus(true);
                this.panContractInfo.Visible = true;
                this.Text = "客戶詳細資訊";
            }

            if (Global.str_UserName == "HK_ADMIN" || Global.UserRole.ConvertToString() == "50")
            {
                this.IsViewMode = true;
            }

            //  Add on 2020/03/22
            if (DateTime.Now >= new DateTime(2020, 04, 02, 12, 00, 00))
            {
                this.rdoBonusA.Enabled = false;
            }
            if (DateTime.Now >= new DateTime(2020, 06, 30, 12, 00, 00))
            {
                this.rdoBonusB.Visible = false;
            }
          
        }




        public frmAddContract(string strCustID, string strOrderID, bool IsViewMode = false, bool IsResend = false) : this(strCustID)
        {
            this.strOrderID = strOrderID;
            this.IsViewMode = IsViewMode;
            this.IsResend = IsResend;
        }

        private void frmAddContract_Load(object sender, EventArgs e)
        {
            if (this.strCustID.Trim() != string.Empty)
            {
                this.InitBind();
                this.btnBringParentOrder.Visible = true;
                this.CheckFromDateEvent();
                var dataHist = base.Cust.GetLogDataByCustID(this.strCustID);
                if (dataHist.Count() > 0)
                {
                    this.btnModifyHist.Visible = true;
                }
                else
                {
                    this.btnModifyHist.Visible = false;
                }
            }
            else
            {
                this.btnBringParentOrder.Visible = false;
                this.txtParentOrderNo.Enabled = false;
                this.CheckFromDateEvent();
                this.btnModifyHist.Visible = false;
            }

            if (this.strOrderID != string.Empty)
            {
                //  帶入訂單資料(可修改特定欄位資料)
                if (IsViewMode == false)
                {
                    this.Text = "修改合約";
                    if (this.IsResend)
                    {
                        this.Text = "修改合約 - 修改重送";
                        this.btnSubmit.Visible = false;
                        this.btnComfirmModify.Visible = true;
                        this.SetModifyALLStatus(false);
                        this.btnComfirmModify.Text = "確定修改-修改重送";

                        this.InitModifyData();

                        #region Set Readonly

                        this.txtCUST_CNAME.Enabled = false;
                        this.rdoWoman.Enabled = false;
                        this.rdoMan.Enabled = false;
                        this.dteBirthday.Enabled = false;
                        this.txtPASSPORT.Enabled = false;
                        this.txtNATION.Enabled = false;
                        this.txtPHONE.Enabled = false;
                        this.txtEMAIL.Enabled = false;
                        this.txtAddress.Enabled = false;
                        this.txtPOSTAL_CODE.Enabled = false;

                        this.txtID_NUMBER.Enabled = false;
                        this.txtCUST_ENAME.Enabled = false;
                        this.txtCUST_ENAME2.Enabled = false;
                        this.txtID_NUMBER2.Enabled = false;
                        this.rdoWoman2.Enabled = false;
                        this.rdoMan2.Enabled = false;
                        this.txtAddress2.Enabled = false;
                        this.txtEMAIL2.Enabled = false;
                        this.txtPHONE2.Enabled = false;
                        this.txtNATION2.Enabled = false;
                        this.txtPASSPORT2.Enabled = false;
                        this.dteBirthday2.Enabled = false;
                        this.txtCUST_CNAME2.Enabled = false;
                        this.txtPOSTAL_CODE2.Enabled = false;


                        this.txtBRANCH_CNAME.Enabled = false;
                        this.txtBRANCH_ENAME.Enabled = false;
                        this.txtBANK_CNAME.Enabled = false;
                        this.txtBANK_ENAME.Enabled = false;
                        this.txtACCOUNT_CNAME.Enabled = false;
                        this.txtACCOUNT_ENAME.Enabled = false;
                        this.txtSWIFT_CODE.Enabled = false;
                        this.txtACCOUNT.Enabled = false;
                        this.txtBANK_C_ADDRESS.Enabled = false;
                        this.txtBANK_E_ADDRESS.Enabled = false;
                        this.btnImportToUSD1.Enabled = false;

                        this.txtBRANCH_CNAME2.Enabled = false;
                        this.txtBRANCH_ENAME2.Enabled = false;
                        this.txtBANK_CNAME2.Enabled = false;
                        this.txtBANK_ENAME2.Enabled = false;
                        this.txtACCOUNT_CNAME2.Enabled = false;
                        this.txtACCOUNT_ENAME2.Enabled = false;
                        this.txtSWIFT_CODE2.Enabled = false;
                        this.txtACCOUNT2.Enabled = false;
                        this.txtBANK_C_ADDRESS2.Enabled = false;
                        this.txtBANK_E_ADDRESS2.Enabled = false;
                        this.btnImportToUSD2.Enabled = false;


                        this.txtBRANCH_CNAME_2.Enabled = false;
                        this.txtBRANCH_ENAME_2.Enabled = false;
                        this.txtBANK_CNAME_2.Enabled = false;
                        this.txtBANK_ENAME_2.Enabled = false;
                        this.txtACCOUNT_CNAME_2.Enabled = false;
                        this.txtACCOUNT_ENAME_2.Enabled = false;
                        this.txtSWIFT_CODE_2.Enabled = false;
                        this.txtACCOUNT_2.Enabled = false;
                        this.txtBANK_C_ADDRESS_2.Enabled = false;
                        this.txtBANK_E_ADDRESS_2.Enabled = false;
                        this.btnImportToNTD1.Enabled = false;

                        this.txtBRANCH_CNAME2_2.Enabled = false;
                        this.txtBRANCH_ENAME2_2.Enabled = false;
                        this.txtBANK_CNAME2_2.Enabled = false;
                        this.txtBANK_ENAME2_2.Enabled = false;
                        this.txtACCOUNT_CNAME2_2.Enabled = false;
                        this.txtACCOUNT_ENAME2_2.Enabled = false;
                        this.txtSWIFT_CODE2_2.Enabled = false;
                        this.txtACCOUNT2_2.Enabled = false;
                        this.txtBANK_C_ADDRESS2_2.Enabled = false;
                        this.txtBANK_E_ADDRESS2_2.Enabled = false;
                        this.btnImportToNTD2.Enabled = false;


                        this.txtBRANCH_CNAME_3.Enabled = false;
                        this.txtBRANCH_ENAME_3.Enabled = false;
                        this.txtBANK_CNAME_3.Enabled = false;
                        this.txtBANK_ENAME_3.Enabled = false;
                        this.txtACCOUNT_CNAME_3.Enabled = false;
                        this.txtACCOUNT_ENAME_3.Enabled = false;
                        this.txtSWIFT_CODE_3.Enabled = false;
                        this.txtACCOUNT_3.Enabled = false;
                        this.txtBANK_C_ADDRESS_3.Enabled = false;
                        this.txtBANK_E_ADDRESS_3.Enabled = false;
                        this.btnImportToRMB1.Enabled = false;

                        this.txtBRANCH_CNAME2_3.Enabled = false;
                        this.txtBRANCH_ENAME2_3.Enabled = false;
                        this.txtBANK_CNAME2_3.Enabled = false;
                        this.txtBANK_ENAME2_3.Enabled = false;
                        this.txtACCOUNT_CNAME2_3.Enabled = false;
                        this.txtACCOUNT_ENAME2_3.Enabled = false;
                        this.txtSWIFT_CODE2_3.Enabled = false;
                        this.txtACCOUNT2_3.Enabled = false;
                        this.txtBANK_C_ADDRESS2_3.Enabled = false;
                        this.txtBANK_E_ADDRESS2_3.Enabled = false;
                        this.btnImportToRMB2.Enabled = false;

                        this.txtBRANCH_CNAME_3_2.Enabled = false;
                        this.txtBRANCH_ENAME_3_2.Enabled = false;
                        this.txtBANK_CNAME_3_2.Enabled = false;
                        this.txtBANK_ENAME_3_2.Enabled = false;
                        this.txtACCOUNT_CNAME_3_2.Enabled = false;
                        this.txtACCOUNT_ENAME_3_2.Enabled = false;
                        this.txtSWIFT_CODE_3_2.Enabled = false;
                        this.txtACCOUNT_3_2.Enabled = false;
                        this.txtBANK_C_ADDRESS_3_2.Enabled = false;
                        this.txtBANK_E_ADDRESS_3_2.Enabled = false;
                        this.btnImportToRMB1_2.Enabled = false;

                        this.txtBRANCH_CNAME2_3_2.Enabled = false;
                        this.txtBRANCH_ENAME2_3_2.Enabled = false;
                        this.txtBANK_CNAME2_3_2.Enabled = false;
                        this.txtBANK_ENAME2_3_2.Enabled = false;
                        this.txtACCOUNT_CNAME2_3_2.Enabled = false;
                        this.txtACCOUNT_ENAME2_3_2.Enabled = false;
                        this.txtSWIFT_CODE2_3_2.Enabled = false;
                        this.txtACCOUNT2_3_2.Enabled = false;
                        this.txtBANK_C_ADDRESS2_3_2.Enabled = false;
                        this.txtBANK_E_ADDRESS2_3_2.Enabled = false;
                        this.btnImportToRMB2_2.Enabled = false;


                        this.txtBRANCH_CNAME_4.Enabled = false;
                        this.txtBRANCH_ENAME_4.Enabled = false;
                        this.txtBANK_CNAME_4.Enabled = false;
                        this.txtBANK_ENAME_4.Enabled = false;
                        this.txtACCOUNT_CNAME_4.Enabled = false;
                        this.txtACCOUNT_ENAME_4.Enabled = false;
                        this.txtSWIFT_CODE_4.Enabled = false;
                        this.txtACCOUNT_4.Enabled = false;
                        this.txtBANK_C_ADDRESS_4.Enabled = false;
                        this.txtBANK_E_ADDRESS_4.Enabled = false;
                        this.btnImportToEUR1.Enabled = false;

                        this.txtBRANCH_CNAME2_4.Enabled = false;
                        this.txtBRANCH_ENAME2_4.Enabled = false;
                        this.txtBANK_CNAME2_4.Enabled = false;
                        this.txtBANK_ENAME2_4.Enabled = false;
                        this.txtACCOUNT_CNAME2_4.Enabled = false;
                        this.txtACCOUNT_ENAME2_4.Enabled = false;
                        this.txtSWIFT_CODE2_4.Enabled = false;
                        this.txtACCOUNT2_4.Enabled = false;
                        this.txtBANK_C_ADDRESS2_4.Enabled = false;
                        this.txtBANK_E_ADDRESS2_4.Enabled = false;
                        this.btnImportToEUR2.Enabled = false;


                        this.txtBRANCH_CNAME_5.Enabled = false;
                        this.txtBRANCH_ENAME_5.Enabled = false;
                        this.txtBANK_CNAME_5.Enabled = false;
                        this.txtBANK_ENAME_5.Enabled = false;
                        this.txtACCOUNT_CNAME_5.Enabled = false;
                        this.txtACCOUNT_ENAME_5.Enabled = false;
                        this.txtSWIFT_CODE_5.Enabled = false;
                        this.txtACCOUNT_5.Enabled = false;
                        this.txtBANK_C_ADDRESS_5.Enabled = false;
                        this.txtBANK_E_ADDRESS_5.Enabled = false;
                        this.btnImportToAUD1.Enabled = false;

                        this.txtBRANCH_CNAME2_5.Enabled = false;
                        this.txtBRANCH_ENAME2_5.Enabled = false;
                        this.txtBANK_CNAME2_5.Enabled = false;
                        this.txtBANK_ENAME2_5.Enabled = false;
                        this.txtACCOUNT_CNAME2_5.Enabled = false;
                        this.txtACCOUNT_ENAME2_5.Enabled = false;
                        this.txtSWIFT_CODE2_5.Enabled = false;
                        this.txtACCOUNT2_5.Enabled = false;
                        this.txtBANK_C_ADDRESS2_5.Enabled = false;
                        this.txtBANK_E_ADDRESS2_5.Enabled = false;
                        this.btnImportToAUD2.Enabled = false;


                        this.txtBRANCH_CNAME_6.Enabled = false;
                        this.txtBRANCH_ENAME_6.Enabled = false;
                        this.txtBANK_CNAME_6.Enabled = false;
                        this.txtBANK_ENAME_6.Enabled = false;
                        this.txtACCOUNT_CNAME_6.Enabled = false;
                        this.txtACCOUNT_ENAME_6.Enabled = false;
                        this.txtSWIFT_CODE_6.Enabled = false;
                        this.txtACCOUNT_6.Enabled = false;
                        this.txtBANK_C_ADDRESS_6.Enabled = false;
                        this.txtBANK_E_ADDRESS_6.Enabled = false;
                        this.btnImportToJPY1.Enabled = false;

                        this.txtBRANCH_CNAME2_6.Enabled = false;
                        this.txtBRANCH_ENAME2_6.Enabled = false;
                        this.txtBANK_CNAME2_6.Enabled = false;
                        this.txtBANK_ENAME2_6.Enabled = false;
                        this.txtACCOUNT_CNAME2_6.Enabled = false;
                        this.txtACCOUNT_ENAME2_6.Enabled = false;
                        this.txtSWIFT_CODE2_6.Enabled = false;
                        this.txtACCOUNT2_6.Enabled = false;
                        this.txtBANK_C_ADDRESS2_6.Enabled = false;
                        this.txtBANK_E_ADDRESS2_6.Enabled = false;
                        this.btnImportToJPY2.Enabled = false;


                        this.txtBRANCH_CNAME_7.Enabled = false;
                        this.txtBRANCH_ENAME_7.Enabled = false;
                        this.txtBANK_CNAME_7.Enabled = false;
                        this.txtBANK_ENAME_7.Enabled = false;
                        this.txtACCOUNT_CNAME_7.Enabled = false;
                        this.txtACCOUNT_ENAME_7.Enabled = false;
                        this.txtSWIFT_CODE_7.Enabled = false;
                        this.txtACCOUNT_7.Enabled = false;
                        this.txtBANK_C_ADDRESS_7.Enabled = false;
                        this.txtBANK_E_ADDRESS_7.Enabled = false;
                        this.btnImportToNZD1.Enabled = false;

                        this.txtBRANCH_CNAME2_7.Enabled = false;
                        this.txtBRANCH_ENAME2_7.Enabled = false;
                        this.txtBANK_CNAME2_7.Enabled = false;
                        this.txtBANK_ENAME2_7.Enabled = false;
                        this.txtACCOUNT_CNAME2_7.Enabled = false;
                        this.txtACCOUNT_ENAME2_7.Enabled = false;
                        this.txtSWIFT_CODE2_7.Enabled = false;
                        this.txtACCOUNT2_7.Enabled = false;
                        this.txtBANK_C_ADDRESS2_7.Enabled = false;
                        this.txtBANK_E_ADDRESS2_7.Enabled = false;
                        this.btnImportToNZD2.Enabled = false;


                        this.chkMAIL_TYPE_MONTHLY1.Enabled = false;
                        this.chkMAIL_TYPE_MONTHLY2.Enabled = false;
                        this.chkMAIL_TYPE_END2.Enabled = false;
                        this.chkMAIL_TYPE_END1.Enabled = false;
                        this.chkMAIL_TYPE_END3.Enabled = false;
                        this.txtMAIL_TYPE_END.Enabled = false;
                        this.rdoWebDepositType_Null.Enabled = false;
                        this.rdoWebDepositType_Y.Enabled = false;


                        this.txtOrderNo.Enabled = true;
                        this.txtParentOrderNo.Enabled = true;
                        this.btnBringParentOrder.Enabled = true;
                        this.txtAmount_USD.Enabled = true;
                        this.txtAmount_NTD.Enabled = true;
                        this.txtAmount_RMB.Enabled = true;
                        this.txtAmount_EUR.Enabled = true;
                        this.txtAmount_AUD.Enabled = true;
                        this.txtAmount_JPY.Enabled = true;
                        this.txtAmount_NZD.Enabled = true;
                        this.txtAmount_USD_OLD.Enabled = true;
                        this.txtAmount_NTD_OLD.Enabled = true;
                        this.txtAmount_RMB_OLD.Enabled = true;
                        this.txtAmount_EUR_OLD.Enabled = true;
                        this.txtAmount_AUD_OLD.Enabled = true;
                        this.txtAmount_JPY_OLD.Enabled = true;
                        this.txtAmount_NZD_OLD.Enabled = true;
                        this.txtInterest_USD.Enabled = true;
                        this.txtInterest_NTD.Enabled = true;
                        this.txtInterest_RMB.Enabled = true;
                        this.txtInterest_EUR.Enabled = true;
                        this.txtInterest_AUD.Enabled = true;
                        this.txtInterest_JPY.Enabled = true;
                        this.txtInterest_NZD.Enabled = true;
                        this.txtInterestedRate.Enabled = true;
                        this.checkBox1.Enabled = true;
                        this.checkBox2.Enabled = true;
                        this.chkIsMultiContract.Enabled = true;
                        this.chk_ind_order.Enabled = true;
                        this.cboIS_REMIT_TO_AUS.Enabled = true;
                        this.rdoBonusA.Enabled = true;
                        this.rdoBonusB.Enabled = true;

                        this.dteOrderEnd.Enabled = true;
                        this.dteFromDate.Enabled = true;
                        this.dteExtendDate.Enabled = true;
                        this.btnDelExtendDate.Enabled = true;
                        this.dteEndDate.Enabled = true;
                        this.btnDelFromDate.Enabled = true;
                        this.btnDelBirthday.Enabled = false;
                        this.btnDelBirthday2.Enabled = false;

                        this.txtYear.Enabled = true;
                        this.txtMT4.Enabled = true;
                        this.txtNote.Enabled = true;

                        this.chkIsMultiContract.Visible = this.txtParentOrderNo.Text.Trim() != string.Empty;
                        #endregion

                        #region Reject By HK

                        //  香港退件, Audit重送, 直接送到香港
                        if (this.IsRejectByHK)
                        {
                            ////  香港退件的, 不可以修改重送
                            //this.btnSubmit.Visible = false;
                            //this.btnComfirmModify.Visible = false;
                            this.SetModifyALLStatus(false);

                            //this.IsViewMode = true;
                            //this.InitModifyData();
                        }
                        #endregion
                    }
                    else
                    {
                        this.InitModifyData();
                    }
                }
                else
                {
                    this.InitModifyData();
                    this.Text = "檢視";
                }

                if (this.IsRejectByHK)
                {
                    //  不可合約轉讓
                    this.btnTransOrder.Visible = false;
                }
                else {
                    this.btnTransOrder.Visible = true;
                }
            }
            else
            {
                this.btnTransOrder.Visible = false;
            }

            if (this.IsOnlyCust == true)
            {
                this.InitModifyData();
            }

            if (Global.str_UserName == "HK_ADMIN" || Global.UserRole.ConvertToString() == "50")
            {
                this.btnComfirmModify.Visible = false;
                this.btnModifyALL.Visible = false;
            }

            if (this.IsOrderVoid)
            {
                this.btnTransOrder.Visible = false;
            }

            //var test = txtYear.Text;
            this.SetIND_ContractEnable();
        }

        private void InitBind()
        {
            this.chkMAIL_TYPE_END1.Checked = false;
            this.chkMAIL_TYPE_END2.Checked = false;
            this.chkMAIL_TYPE_END3.Checked = false;
            this.chkMAIL_TYPE_MONTHLY1.Checked = false;
            this.chkMAIL_TYPE_MONTHLY2.Checked = false;
            this.txtMAIL_TYPE_END.Text = string.Empty;
            this.rdoWebDepositType_Null.Checked = false;
            this.rdoWebDepositType_Y.Checked = false;

            var IsHasData = false;
            var data = new List<BL_CUST>();
            if (this.IsOrderend == true)
            {
                var filter = new QueryCust();
                filter.IsIncOrderEnd = "Y";
                data = base.Cust.GetCustData(this.strCustID, filter);
            }
            else
            {
                 data = base.Cust.GetCustData(this.strCustID);
            }

            if (data.Count > 0)
            {
                #region Cust Info

                var dataSingle = data[0];
                IsHasData = true;

                this.txtCUST_CNAME.Text = dataSingle.CUST_CNAME.ConvertToString();
                this.txtCUST_ENAME.Text = dataSingle.CUST_ENAME.ConvertToString();
                this.txtCUST_CNAME2.Text = dataSingle.CUST_CNAME2.ConvertToString();
                this.txtCUST_ENAME2.Text = dataSingle.CUST_ENAME2.ConvertToString();
                if (dataSingle.SEX.ConvertToString() == "M")
                {
                    this.rdoWoman.Checked = false;
                    this.rdoMan.Checked = true;
                }
                else if (dataSingle.SEX.ConvertToString() == "F")
                {
                    this.rdoMan.Checked = false;
                    this.rdoWoman.Checked = true;
                }

                if (dataSingle.SEX2.ConvertToString() == "M")
                {
                    this.rdoWoman2.Checked = false;
                    this.rdoMan2.Checked = true;
                }
                else if (dataSingle.SEX2.ConvertToString() == "F")
                {
                    this.rdoMan2.Checked = false;
                    this.rdoWoman2.Checked = true;
                }

                if (dataSingle.DATE_OF_BIRTH != null)
                {
                    this.dteBirthday.Format = DateTimePickerFormat.Long;
                    this.dteBirthday.Value = dataSingle.DATE_OF_BIRTH.Value;
                    this.dteBirthday.Text = dataSingle.DATE_OF_BIRTH.Value.ToString("yyyy/MM/dd");
                }

                if (dataSingle.DATE_OF_BIRTH2 != null)
                {
                    this.dteBirthday2.Format = DateTimePickerFormat.Long;
                    this.dteBirthday2.Value = dataSingle.DATE_OF_BIRTH2.Value;
                    this.dteBirthday2.Text = dataSingle.DATE_OF_BIRTH2.Value.ToString("yyyy/MM/dd");
                }

                this.txtID_NUMBER.Text = dataSingle.ID_NUMBER.ConvertToString();
                this.txtPASSPORT.Text = dataSingle.PASSPORT.ConvertToString();
                this.txtID_NUMBER2.Text = dataSingle.ID_NUMBER2.ConvertToString();
                this.txtPASSPORT2.Text = dataSingle.PASSPORT2.ConvertToString();
                this.txtNATION.Text = dataSingle.PASSPORT_REGION.ConvertToString();
                this.txtNATION2.Text = dataSingle.PASSPORT_REGION2.ConvertToString();
                this.txtPHONE.Text = dataSingle.PHONE_1.ConvertToString();
                this.txtPHONE2.Text = dataSingle.PHONE_2.ConvertToString();
                this.txtEMAIL.Text = dataSingle.EMAIL_1.ConvertToString();
                this.txtEMAIL2.Text = dataSingle.EMAIL_2.ConvertToString();
                this.txtAddress.Text = dataSingle.C_ADDRESS.ConvertToString();
                this.txtAddress2.Text = dataSingle.C_ADDRESS2.ConvertToString();
                this.txtPOSTAL_CODE.Text = dataSingle.POSTAL_CODE.ConvertToString();
                this.txtPOSTAL_CODE2.Text = dataSingle.POSTAL_CODE2.ConvertToString();

                var strMAIL_TYPE_MONTHLY = dataSingle.MAIL_TYPE_MONTHLY.ConvertToString().Trim();
                var strMAIL_TYPE_END = dataSingle.MAIL_TYPE_END.ConvertToString().Trim();

                if (strMAIL_TYPE_MONTHLY != string.Empty)
                {
                    if (strMAIL_TYPE_MONTHLY == this.chkMAIL_TYPE_MONTHLY1.Text)
                    {
                        this.chkMAIL_TYPE_MONTHLY1.Checked = true;
                        this.chkMAIL_TYPE_MONTHLY2.Checked = false;
                    }
                    if (strMAIL_TYPE_MONTHLY == this.chkMAIL_TYPE_MONTHLY2.Text)
                    {
                        this.chkMAIL_TYPE_MONTHLY1.Checked = false;
                        this.chkMAIL_TYPE_MONTHLY2.Checked = true;
                    }
                }

                if (strMAIL_TYPE_END != string.Empty)
                {
                    if (strMAIL_TYPE_END == this.chkMAIL_TYPE_END1.Text)
                    {
                        this.chkMAIL_TYPE_END1.Checked = true;
                        this.chkMAIL_TYPE_END2.Checked = false;
                        this.chkMAIL_TYPE_END3.Checked = false;
                        this.txtMAIL_TYPE_END.Text = string.Empty;
                    }
                    else if (strMAIL_TYPE_END == this.chkMAIL_TYPE_END2.Text)
                    {
                        this.chkMAIL_TYPE_END1.Checked = false;
                        this.chkMAIL_TYPE_END2.Checked = true;
                        this.chkMAIL_TYPE_END3.Checked = false;
                        this.txtMAIL_TYPE_END.Text = string.Empty;
                    }
                    else
                    {
                        this.chkMAIL_TYPE_END1.Checked = false;
                        this.chkMAIL_TYPE_END2.Checked = false;
                        this.chkMAIL_TYPE_END3.Checked = true;
                        this.txtMAIL_TYPE_END.Text = strMAIL_TYPE_END;
                    }
                }
                switch (dataSingle.IS_ONLINE_WITHDRAWAL.ConvertToString().Trim().ToUpper())
                {
                    case "Y":
                        this.rdoWebDepositType_Null.Checked = false;
                        this.rdoWebDepositType_Y.Checked = true;
                        break;
                    default:
                        this.rdoWebDepositType_Null.Checked = true;
                        this.rdoWebDepositType_Y.Checked = false;
                        break;
                }

                switch (dataSingle.IS_ONLINE_WITHDRAWAL_CHECK.ConvertToString().Trim().ToUpper())
                {
                    case "P":
                        this.lblIS_ONLINE_WITHDRAWAL_CHECK.Text = "手機已通過驗證";
                        break;
                    case "E":
                        this.lblIS_ONLINE_WITHDRAWAL_CHECK.Text = "Email已通過驗證";
                        break;
                    case "A":
                        this.lblIS_ONLINE_WITHDRAWAL_CHECK.Text = "手機及Email均已通過驗證";
                        break;
                    default:
                        this.lblIS_ONLINE_WITHDRAWAL_CHECK.Text = "無";
                        break;
                }
                #endregion
            }

            this.txtBANK_CNAME.Tag = string.Empty;
            this.txtBANK_CNAME_2.Tag = string.Empty;
            this.txtBANK_CNAME_3.Tag = string.Empty;
            this.txtBANK_CNAME_4.Tag = string.Empty;
            this.txtBANK_CNAME_5.Tag = string.Empty;
            var dataBank = base.Cust.GetBankDataByCustID(this.strCustID);
            if (dataBank.Count > 0)
            {
                #region Bank Info

                var dataUSD = dataBank.Where(x => x.CURRENCY.ConvertToString() == "USD").ToList();
                var dataNTD = dataBank.Where(x => x.CURRENCY.ConvertToString() == "NTD").ToList();
                var dataRMB = dataBank.Where(x => x.CURRENCY.ConvertToString() == "RMB" && (x.ACCOUNT_TYPE.ConvertToString().Trim() == "" || x.ACCOUNT_TYPE.ConvertToString().Trim() == "OVERSEA")).ToList();
                var dataRMB2 = dataBank.Where(x => x.CURRENCY.ConvertToString() == "RMB" && x.ACCOUNT_TYPE.ConvertToString().Trim() == "DOMESTIC").ToList();
                var dataEUR = dataBank.Where(x => x.CURRENCY.ConvertToString() == "EUR").ToList();
                var dataAUD = dataBank.Where(x => x.CURRENCY.ConvertToString() == "AUD").ToList();
                var dataJPY = dataBank.Where(x => x.CURRENCY.ConvertToString() == "JPY").ToList();
                var dataNZD = dataBank.Where(x => x.CURRENCY.ConvertToString() == "NZD").ToList();

                if (dataUSD.Count() > 0)
                {
                    #region USD

                    var dataBankSingle = dataUSD[0];
                    IsHasData = true;

                    this.txtBANK_CNAME.Tag = dataBankSingle.BANK_ID == null ? "" : dataBankSingle.BANK_ID.Value.ConvertToString();
                    this.txtBANK_CNAME.Text = dataBankSingle.BANK_CNAME.ConvertToString();
                    this.txtBANK_ENAME.Text = dataBankSingle.BANK_ENAME.ConvertToString();
                    this.txtBRANCH_CNAME.Text = dataBankSingle.BRANCH_CNAME.ConvertToString();
                    this.txtBRANCH_ENAME.Text = dataBankSingle.BRANCH_ENAME.ConvertToString();
                    this.txtACCOUNT_CNAME.Text = dataBankSingle.ACCOUNT_CNAME.ConvertToString();
                    this.txtACCOUNT_ENAME.Text = dataBankSingle.ACCOUNT_ENAME.ConvertToString();
                    this.txtACCOUNT.Text = dataBankSingle.ACCOUNT.ConvertToString();
                    this.txtBANK_C_ADDRESS.Text = dataBankSingle.BANK_C_ADDRESS.ConvertToString();
                    this.txtBANK_E_ADDRESS.Text = dataBankSingle.BANK_E_ADDRESS.ConvertToString();
                    this.txtSWIFT_CODE.Text = dataBankSingle.SWIFT_CODE.ConvertToString();

                    this.txtBANK_CNAME2.Text = dataBankSingle.BANK_CNAME2.ConvertToString();
                    this.txtBANK_ENAME2.Text = dataBankSingle.BANK_ENAME2.ConvertToString();
                    this.txtBRANCH_CNAME2.Text = dataBankSingle.BRANCH_CNAME2.ConvertToString();
                    this.txtBRANCH_ENAME2.Text = dataBankSingle.BRANCH_ENAME2.ConvertToString();
                    this.txtACCOUNT_CNAME2.Text = dataBankSingle.ACCOUNT_CNAME2.ConvertToString();
                    this.txtACCOUNT_ENAME2.Text = dataBankSingle.ACCOUNT_ENAME2.ConvertToString();
                    this.txtACCOUNT2.Text = dataBankSingle.ACCOUNT2.ConvertToString();
                    this.txtBANK_C_ADDRESS2.Text = dataBankSingle.BANK_C_ADDRESS2.ConvertToString();
                    this.txtBANK_E_ADDRESS2.Text = dataBankSingle.BANK_E_ADDRESS2.ConvertToString();
                    this.txtSWIFT_CODE2.Text = dataBankSingle.SWIFT_CODE2.ConvertToString();

                    #endregion
                }

                if (dataNTD.Count() > 0)
                {
                    #region NTD

                    var dataBankSingle = dataNTD[0];
                    IsHasData = true;

                    this.txtBANK_CNAME_2.Tag = dataBankSingle.BANK_ID == null ? "" : dataBankSingle.BANK_ID.Value.ConvertToString();
                    this.txtBANK_CNAME_2.Text = dataBankSingle.BANK_CNAME.ConvertToString();
                    this.txtBANK_ENAME_2.Text = dataBankSingle.BANK_ENAME.ConvertToString();
                    this.txtBRANCH_CNAME_2.Text = dataBankSingle.BRANCH_CNAME.ConvertToString();
                    this.txtBRANCH_ENAME_2.Text = dataBankSingle.BRANCH_ENAME.ConvertToString();
                    this.txtACCOUNT_CNAME_2.Text = dataBankSingle.ACCOUNT_CNAME.ConvertToString();
                    this.txtACCOUNT_ENAME_2.Text = dataBankSingle.ACCOUNT_ENAME.ConvertToString();
                    this.txtACCOUNT_2.Text = dataBankSingle.ACCOUNT.ConvertToString();
                    this.txtBANK_C_ADDRESS_2.Text = dataBankSingle.BANK_C_ADDRESS.ConvertToString();
                    this.txtBANK_E_ADDRESS_2.Text = dataBankSingle.BANK_E_ADDRESS.ConvertToString();
                    this.txtSWIFT_CODE_2.Text = dataBankSingle.SWIFT_CODE.ConvertToString();

                    this.txtBANK_CNAME2_2.Text = dataBankSingle.BANK_CNAME2.ConvertToString();
                    this.txtBANK_ENAME2_2.Text = dataBankSingle.BANK_ENAME2.ConvertToString();
                    this.txtBRANCH_CNAME2_2.Text = dataBankSingle.BRANCH_CNAME2.ConvertToString();
                    this.txtBRANCH_ENAME2_2.Text = dataBankSingle.BRANCH_ENAME2.ConvertToString();
                    this.txtACCOUNT_CNAME2_2.Text = dataBankSingle.ACCOUNT_CNAME2.ConvertToString();
                    this.txtACCOUNT_ENAME2_2.Text = dataBankSingle.ACCOUNT_ENAME2.ConvertToString();
                    this.txtACCOUNT2_2.Text = dataBankSingle.ACCOUNT2.ConvertToString();
                    this.txtBANK_C_ADDRESS2_2.Text = dataBankSingle.BANK_C_ADDRESS2.ConvertToString();
                    this.txtBANK_E_ADDRESS2_2.Text = dataBankSingle.BANK_E_ADDRESS2.ConvertToString();
                    this.txtSWIFT_CODE2_2.Text = dataBankSingle.SWIFT_CODE2.ConvertToString();
                    #endregion
                }

                if (dataRMB.Count() > 0)
                {
                    #region RMB

                    var dataBankSingle = dataRMB[0];
                    IsHasData = true;

                    this.txtBANK_CNAME_3.Tag = dataBankSingle.BANK_ID == null ? "" : dataBankSingle.BANK_ID.Value.ConvertToString();
                    this.txtBANK_CNAME_3.Text = dataBankSingle.BANK_CNAME.ConvertToString();
                    this.txtBANK_ENAME_3.Text = dataBankSingle.BANK_ENAME.ConvertToString();
                    this.txtBRANCH_CNAME_3.Text = dataBankSingle.BRANCH_CNAME.ConvertToString();
                    this.txtBRANCH_ENAME_3.Text = dataBankSingle.BRANCH_ENAME.ConvertToString();
                    this.txtACCOUNT_CNAME_3.Text = dataBankSingle.ACCOUNT_CNAME.ConvertToString();
                    this.txtACCOUNT_ENAME_3.Text = dataBankSingle.ACCOUNT_ENAME.ConvertToString();
                    this.txtACCOUNT_3.Text = dataBankSingle.ACCOUNT.ConvertToString();
                    this.txtBANK_C_ADDRESS_3.Text = dataBankSingle.BANK_C_ADDRESS.ConvertToString();
                    this.txtBANK_E_ADDRESS_3.Text = dataBankSingle.BANK_E_ADDRESS.ConvertToString();
                    this.txtSWIFT_CODE_3.Text = dataBankSingle.SWIFT_CODE.ConvertToString();

                    this.txtBANK_CNAME2_3.Text = dataBankSingle.BANK_CNAME2.ConvertToString();
                    this.txtBANK_ENAME2_3.Text = dataBankSingle.BANK_ENAME2.ConvertToString();
                    this.txtBRANCH_CNAME2_3.Text = dataBankSingle.BRANCH_CNAME2.ConvertToString();
                    this.txtBRANCH_ENAME2_3.Text = dataBankSingle.BRANCH_ENAME2.ConvertToString();
                    this.txtACCOUNT_CNAME2_3.Text = dataBankSingle.ACCOUNT_CNAME2.ConvertToString();
                    this.txtACCOUNT_ENAME2_3.Text = dataBankSingle.ACCOUNT_ENAME2.ConvertToString();
                    this.txtACCOUNT2_3.Text = dataBankSingle.ACCOUNT2.ConvertToString();
                    this.txtBANK_C_ADDRESS2_3.Text = dataBankSingle.BANK_C_ADDRESS2.ConvertToString();
                    this.txtBANK_E_ADDRESS2_3.Text = dataBankSingle.BANK_E_ADDRESS2.ConvertToString();
                    this.txtSWIFT_CODE2_3.Text = dataBankSingle.SWIFT_CODE2.ConvertToString();
                    #endregion
                }

                if (dataRMB2.Count() > 0)
                {
                    #region RMB[境內]

                    var dataBankSingle = dataRMB2[0];
                    IsHasData = true;

                    this.txtBANK_CNAME_3_2.Tag = dataBankSingle.BANK_ID == null ? "" : dataBankSingle.BANK_ID.Value.ConvertToString();
                    this.txtBANK_CNAME_3_2.Text = dataBankSingle.BANK_CNAME.ConvertToString();
                    this.txtBANK_ENAME_3_2.Text = dataBankSingle.BANK_ENAME.ConvertToString();
                    this.txtBRANCH_CNAME_3_2.Text = dataBankSingle.BRANCH_CNAME.ConvertToString();
                    this.txtBRANCH_ENAME_3_2.Text = dataBankSingle.BRANCH_ENAME.ConvertToString();
                    this.txtACCOUNT_CNAME_3_2.Text = dataBankSingle.ACCOUNT_CNAME.ConvertToString();
                    this.txtACCOUNT_ENAME_3_2.Text = dataBankSingle.ACCOUNT_ENAME.ConvertToString();
                    this.txtACCOUNT_3_2.Text = dataBankSingle.ACCOUNT.ConvertToString();
                    this.txtBANK_C_ADDRESS_3_2.Text = dataBankSingle.BANK_C_ADDRESS.ConvertToString();
                    this.txtBANK_E_ADDRESS_3_2.Text = dataBankSingle.BANK_E_ADDRESS.ConvertToString();
                    this.txtSWIFT_CODE_3_2.Text = dataBankSingle.SWIFT_CODE.ConvertToString();

                    this.txtBANK_CNAME2_3_2.Text = dataBankSingle.BANK_CNAME2.ConvertToString();
                    this.txtBANK_ENAME2_3_2.Text = dataBankSingle.BANK_ENAME2.ConvertToString();
                    this.txtBRANCH_CNAME2_3_2.Text = dataBankSingle.BRANCH_CNAME2.ConvertToString();
                    this.txtBRANCH_ENAME2_3_2.Text = dataBankSingle.BRANCH_ENAME2.ConvertToString();
                    this.txtACCOUNT_CNAME2_3_2.Text = dataBankSingle.ACCOUNT_CNAME2.ConvertToString();
                    this.txtACCOUNT_ENAME2_3_2.Text = dataBankSingle.ACCOUNT_ENAME2.ConvertToString();
                    this.txtACCOUNT2_3_2.Text = dataBankSingle.ACCOUNT2.ConvertToString();
                    this.txtBANK_C_ADDRESS2_3_2.Text = dataBankSingle.BANK_C_ADDRESS2.ConvertToString();
                    this.txtBANK_E_ADDRESS2_3_2.Text = dataBankSingle.BANK_E_ADDRESS2.ConvertToString();
                    this.txtSWIFT_CODE2_3_2.Text = dataBankSingle.SWIFT_CODE2.ConvertToString();
                    #endregion
                }

                if (dataEUR.Count() > 0)
                {
                    #region EUR

                    var dataBankSingle = dataEUR[0];
                    IsHasData = true;

                    this.txtBANK_CNAME_4.Tag = dataBankSingle.BANK_ID == null ? "" : dataBankSingle.BANK_ID.Value.ConvertToString();
                    this.txtBANK_CNAME_4.Text = dataBankSingle.BANK_CNAME.ConvertToString();
                    this.txtBANK_ENAME_4.Text = dataBankSingle.BANK_ENAME.ConvertToString();
                    this.txtBRANCH_CNAME_4.Text = dataBankSingle.BRANCH_CNAME.ConvertToString();
                    this.txtBRANCH_ENAME_4.Text = dataBankSingle.BRANCH_ENAME.ConvertToString();
                    this.txtACCOUNT_CNAME_4.Text = dataBankSingle.ACCOUNT_CNAME.ConvertToString();
                    this.txtACCOUNT_ENAME_4.Text = dataBankSingle.ACCOUNT_ENAME.ConvertToString();
                    this.txtACCOUNT_4.Text = dataBankSingle.ACCOUNT.ConvertToString();
                    this.txtBANK_C_ADDRESS_4.Text = dataBankSingle.BANK_C_ADDRESS.ConvertToString();
                    this.txtBANK_E_ADDRESS_4.Text = dataBankSingle.BANK_E_ADDRESS.ConvertToString();
                    this.txtSWIFT_CODE_4.Text = dataBankSingle.SWIFT_CODE.ConvertToString();

                    this.txtBANK_CNAME2_4.Text = dataBankSingle.BANK_CNAME2.ConvertToString();
                    this.txtBANK_ENAME2_4.Text = dataBankSingle.BANK_ENAME2.ConvertToString();
                    this.txtBRANCH_CNAME2_4.Text = dataBankSingle.BRANCH_CNAME2.ConvertToString();
                    this.txtBRANCH_ENAME2_4.Text = dataBankSingle.BRANCH_ENAME2.ConvertToString();
                    this.txtACCOUNT_CNAME2_4.Text = dataBankSingle.ACCOUNT_CNAME2.ConvertToString();
                    this.txtACCOUNT_ENAME2_4.Text = dataBankSingle.ACCOUNT_ENAME2.ConvertToString();
                    this.txtACCOUNT2_4.Text = dataBankSingle.ACCOUNT2.ConvertToString();
                    this.txtBANK_C_ADDRESS2_4.Text = dataBankSingle.BANK_C_ADDRESS2.ConvertToString();
                    this.txtBANK_E_ADDRESS2_4.Text = dataBankSingle.BANK_E_ADDRESS2.ConvertToString();
                    this.txtSWIFT_CODE2_4.Text = dataBankSingle.SWIFT_CODE2.ConvertToString();
                    #endregion
                }

                if (dataAUD.Count() > 0)
                {
                    #region AUD

                    var dataBankSingle = dataAUD[0];
                    IsHasData = true;

                    this.txtBANK_CNAME_5.Tag = dataBankSingle.BANK_ID == null ? "" : dataBankSingle.BANK_ID.Value.ConvertToString();
                    this.txtBANK_CNAME_5.Text = dataBankSingle.BANK_CNAME.ConvertToString();
                    this.txtBANK_ENAME_5.Text = dataBankSingle.BANK_ENAME.ConvertToString();
                    this.txtBRANCH_CNAME_5.Text = dataBankSingle.BRANCH_CNAME.ConvertToString();
                    this.txtBRANCH_ENAME_5.Text = dataBankSingle.BRANCH_ENAME.ConvertToString();
                    this.txtACCOUNT_CNAME_5.Text = dataBankSingle.ACCOUNT_CNAME.ConvertToString();
                    this.txtACCOUNT_ENAME_5.Text = dataBankSingle.ACCOUNT_ENAME.ConvertToString();
                    this.txtACCOUNT_5.Text = dataBankSingle.ACCOUNT.ConvertToString();
                    this.txtBANK_C_ADDRESS_5.Text = dataBankSingle.BANK_C_ADDRESS.ConvertToString();
                    this.txtBANK_E_ADDRESS_5.Text = dataBankSingle.BANK_E_ADDRESS.ConvertToString();
                    this.txtSWIFT_CODE_5.Text = dataBankSingle.SWIFT_CODE.ConvertToString();

                    this.txtBANK_CNAME2_5.Text = dataBankSingle.BANK_CNAME2.ConvertToString();
                    this.txtBANK_ENAME2_5.Text = dataBankSingle.BANK_ENAME2.ConvertToString();
                    this.txtBRANCH_CNAME2_5.Text = dataBankSingle.BRANCH_CNAME2.ConvertToString();
                    this.txtBRANCH_ENAME2_5.Text = dataBankSingle.BRANCH_ENAME2.ConvertToString();
                    this.txtACCOUNT_CNAME2_5.Text = dataBankSingle.ACCOUNT_CNAME2.ConvertToString();
                    this.txtACCOUNT_ENAME2_5.Text = dataBankSingle.ACCOUNT_ENAME2.ConvertToString();
                    this.txtACCOUNT2_5.Text = dataBankSingle.ACCOUNT2.ConvertToString();
                    this.txtBANK_C_ADDRESS2_5.Text = dataBankSingle.BANK_C_ADDRESS2.ConvertToString();
                    this.txtBANK_E_ADDRESS2_5.Text = dataBankSingle.BANK_E_ADDRESS2.ConvertToString();
                    this.txtSWIFT_CODE2_5.Text = dataBankSingle.SWIFT_CODE2.ConvertToString();
                    #endregion
                }

                if (dataJPY.Count() > 0)
                {
                    #region JPY

                    var dataBankSingle = dataJPY[0];
                    IsHasData = true;

                    this.txtBANK_CNAME_6.Tag = dataBankSingle.BANK_ID == null ? "" : dataBankSingle.BANK_ID.Value.ConvertToString();
                    this.txtBANK_CNAME_6.Text = dataBankSingle.BANK_CNAME.ConvertToString();
                    this.txtBANK_ENAME_6.Text = dataBankSingle.BANK_ENAME.ConvertToString();
                    this.txtBRANCH_CNAME_6.Text = dataBankSingle.BRANCH_CNAME.ConvertToString();
                    this.txtBRANCH_ENAME_6.Text = dataBankSingle.BRANCH_ENAME.ConvertToString();
                    this.txtACCOUNT_CNAME_6.Text = dataBankSingle.ACCOUNT_CNAME.ConvertToString();
                    this.txtACCOUNT_ENAME_6.Text = dataBankSingle.ACCOUNT_ENAME.ConvertToString();
                    this.txtACCOUNT_6.Text = dataBankSingle.ACCOUNT.ConvertToString();
                    this.txtBANK_C_ADDRESS_6.Text = dataBankSingle.BANK_C_ADDRESS.ConvertToString();
                    this.txtBANK_E_ADDRESS_6.Text = dataBankSingle.BANK_E_ADDRESS.ConvertToString();
                    this.txtSWIFT_CODE_6.Text = dataBankSingle.SWIFT_CODE.ConvertToString();

                    this.txtBANK_CNAME2_6.Text = dataBankSingle.BANK_CNAME2.ConvertToString();
                    this.txtBANK_ENAME2_6.Text = dataBankSingle.BANK_ENAME2.ConvertToString();
                    this.txtBRANCH_CNAME2_6.Text = dataBankSingle.BRANCH_CNAME2.ConvertToString();
                    this.txtBRANCH_ENAME2_6.Text = dataBankSingle.BRANCH_ENAME2.ConvertToString();
                    this.txtACCOUNT_CNAME2_6.Text = dataBankSingle.ACCOUNT_CNAME2.ConvertToString();
                    this.txtACCOUNT_ENAME2_6.Text = dataBankSingle.ACCOUNT_ENAME2.ConvertToString();
                    this.txtACCOUNT2_6.Text = dataBankSingle.ACCOUNT2.ConvertToString();
                    this.txtBANK_C_ADDRESS2_6.Text = dataBankSingle.BANK_C_ADDRESS2.ConvertToString();
                    this.txtBANK_E_ADDRESS2_6.Text = dataBankSingle.BANK_E_ADDRESS2.ConvertToString();
                    this.txtSWIFT_CODE2_6.Text = dataBankSingle.SWIFT_CODE2.ConvertToString();
                    #endregion
                }

                if (dataNZD.Count() > 0)
                {
                    #region NZD

                    var dataBankSingle = dataNZD[0];
                    IsHasData = true;

                    this.txtBANK_CNAME_7.Tag = dataBankSingle.BANK_ID == null ? "" : dataBankSingle.BANK_ID.Value.ConvertToString();
                    this.txtBANK_CNAME_7.Text = dataBankSingle.BANK_CNAME.ConvertToString();
                    this.txtBANK_ENAME_7.Text = dataBankSingle.BANK_ENAME.ConvertToString();
                    this.txtBRANCH_CNAME_7.Text = dataBankSingle.BRANCH_CNAME.ConvertToString();
                    this.txtBRANCH_ENAME_7.Text = dataBankSingle.BRANCH_ENAME.ConvertToString();
                    this.txtACCOUNT_CNAME_7.Text = dataBankSingle.ACCOUNT_CNAME.ConvertToString();
                    this.txtACCOUNT_ENAME_7.Text = dataBankSingle.ACCOUNT_ENAME.ConvertToString();
                    this.txtACCOUNT_7.Text = dataBankSingle.ACCOUNT.ConvertToString();
                    this.txtBANK_C_ADDRESS_7.Text = dataBankSingle.BANK_C_ADDRESS.ConvertToString();
                    this.txtBANK_E_ADDRESS_7.Text = dataBankSingle.BANK_E_ADDRESS.ConvertToString();
                    this.txtSWIFT_CODE_7.Text = dataBankSingle.SWIFT_CODE.ConvertToString();

                    this.txtBANK_CNAME2_7.Text = dataBankSingle.BANK_CNAME2.ConvertToString();
                    this.txtBANK_ENAME2_7.Text = dataBankSingle.BANK_ENAME2.ConvertToString();
                    this.txtBRANCH_CNAME2_7.Text = dataBankSingle.BRANCH_CNAME2.ConvertToString();
                    this.txtBRANCH_ENAME2_7.Text = dataBankSingle.BRANCH_ENAME2.ConvertToString();
                    this.txtACCOUNT_CNAME2_7.Text = dataBankSingle.ACCOUNT_CNAME2.ConvertToString();
                    this.txtACCOUNT_ENAME2_7.Text = dataBankSingle.ACCOUNT_ENAME2.ConvertToString();
                    this.txtACCOUNT2_7.Text = dataBankSingle.ACCOUNT2.ConvertToString();
                    this.txtBANK_C_ADDRESS2_7.Text = dataBankSingle.BANK_C_ADDRESS2.ConvertToString();
                    this.txtBANK_E_ADDRESS2_7.Text = dataBankSingle.BANK_E_ADDRESS2.ConvertToString();
                    this.txtSWIFT_CODE2_7.Text = dataBankSingle.SWIFT_CODE2.ConvertToString();
                    #endregion
                }
                #endregion
            }

            //  若已有客戶資料, 就帶出來且唯獨
            if (IsHasData && this.IsResend == false)
            {
                #region Set Readonly

                this.txtCUST_CNAME.Enabled = false;
                this.rdoWoman.Enabled = false;
                this.rdoMan.Enabled = false;
                this.dteBirthday.Enabled = false;
                this.txtPASSPORT.Enabled = false;
                this.txtNATION.Enabled = false;
                this.txtPHONE.Enabled = false;
                this.txtEMAIL.Enabled = false;
                this.txtAddress.Enabled = false;
                this.txtBRANCH_CNAME.Enabled = false;
                this.txtBRANCH_ENAME.Enabled = false;
                this.txtBANK_CNAME.Enabled = false;
                this.txtBANK_ENAME.Enabled = false;
                this.txtACCOUNT_CNAME.Enabled = false;
                this.txtACCOUNT_ENAME.Enabled = false;
                this.txtSWIFT_CODE.Enabled = false;
                this.txtACCOUNT.Enabled = false;
                this.txtBANK_C_ADDRESS.Enabled = false;
                this.txtBANK_E_ADDRESS.Enabled = false;
                this.txtPOSTAL_CODE.Enabled = false;
                this.btnImportToUSD1.Enabled = false;

                this.txtBRANCH_CNAME2.Enabled = false;
                this.txtBRANCH_ENAME2.Enabled = false;
                this.txtBANK_CNAME2.Enabled = false;
                this.txtBANK_ENAME2.Enabled = false;
                this.txtACCOUNT_CNAME2.Enabled = false;
                this.txtACCOUNT_ENAME2.Enabled = false;
                this.txtSWIFT_CODE2.Enabled = false;
                this.txtACCOUNT2.Enabled = false;
                this.txtBANK_C_ADDRESS2.Enabled = false;
                this.txtBANK_E_ADDRESS2.Enabled = false;
                this.btnImportToUSD2.Enabled = false;


                this.txtBRANCH_CNAME_2.Enabled = false;
                this.txtBRANCH_ENAME_2.Enabled = false;
                this.txtBANK_CNAME_2.Enabled = false;
                this.txtBANK_ENAME_2.Enabled = false;
                this.txtACCOUNT_CNAME_2.Enabled = false;
                this.txtACCOUNT_ENAME_2.Enabled = false;
                this.txtSWIFT_CODE_2.Enabled = false;
                this.txtACCOUNT_2.Enabled = false;
                this.txtBANK_C_ADDRESS_2.Enabled = false;
                this.txtBANK_E_ADDRESS_2.Enabled = false;
                this.btnImportToNTD1.Enabled = false;

                this.txtBRANCH_CNAME2_2.Enabled = false;
                this.txtBRANCH_ENAME2_2.Enabled = false;
                this.txtBANK_CNAME2_2.Enabled = false;
                this.txtBANK_ENAME2_2.Enabled = false;
                this.txtACCOUNT_CNAME2_2.Enabled = false;
                this.txtACCOUNT_ENAME2_2.Enabled = false;
                this.txtSWIFT_CODE2_2.Enabled = false;
                this.txtACCOUNT2_2.Enabled = false;
                this.txtBANK_C_ADDRESS2_2.Enabled = false;
                this.txtBANK_E_ADDRESS2_2.Enabled = false;
                this.btnImportToNTD2.Enabled = false;


                this.txtBRANCH_CNAME_3.Enabled = false;
                this.txtBRANCH_ENAME_3.Enabled = false;
                this.txtBANK_CNAME_3.Enabled = false;
                this.txtBANK_ENAME_3.Enabled = false;
                this.txtACCOUNT_CNAME_3.Enabled = false;
                this.txtACCOUNT_ENAME_3.Enabled = false;
                this.txtSWIFT_CODE_3.Enabled = false;
                this.txtACCOUNT_3.Enabled = false;
                this.txtBANK_C_ADDRESS_3.Enabled = false;
                this.txtBANK_E_ADDRESS_3.Enabled = false;
                this.btnImportToRMB1.Enabled = false;

                this.txtBRANCH_CNAME2_3.Enabled = false;
                this.txtBRANCH_ENAME2_3.Enabled = false;
                this.txtBANK_CNAME2_3.Enabled = false;
                this.txtBANK_ENAME2_3.Enabled = false;
                this.txtACCOUNT_CNAME2_3.Enabled = false;
                this.txtACCOUNT_ENAME2_3.Enabled = false;
                this.txtSWIFT_CODE2_3.Enabled = false;
                this.txtACCOUNT2_3.Enabled = false;
                this.txtBANK_C_ADDRESS2_3.Enabled = false;
                this.txtBANK_E_ADDRESS2_3.Enabled = false;
                this.btnImportToRMB2.Enabled = false;

                this.txtBRANCH_CNAME_3_2.Enabled = false;
                this.txtBRANCH_ENAME_3_2.Enabled = false;
                this.txtBANK_CNAME_3_2.Enabled = false;
                this.txtBANK_ENAME_3_2.Enabled = false;
                this.txtACCOUNT_CNAME_3_2.Enabled = false;
                this.txtACCOUNT_ENAME_3_2.Enabled = false;
                this.txtSWIFT_CODE_3_2.Enabled = false;
                this.txtACCOUNT_3_2.Enabled = false;
                this.txtBANK_C_ADDRESS_3_2.Enabled = false;
                this.txtBANK_E_ADDRESS_3_2.Enabled = false;
                this.btnImportToRMB1_2.Enabled = false;

                this.txtBRANCH_CNAME2_3_2.Enabled = false;
                this.txtBRANCH_ENAME2_3_2.Enabled = false;
                this.txtBANK_CNAME2_3_2.Enabled = false;
                this.txtBANK_ENAME2_3_2.Enabled = false;
                this.txtACCOUNT_CNAME2_3_2.Enabled = false;
                this.txtACCOUNT_ENAME2_3_2.Enabled = false;
                this.txtSWIFT_CODE2_3_2.Enabled = false;
                this.txtACCOUNT2_3_2.Enabled = false;
                this.txtBANK_C_ADDRESS2_3_2.Enabled = false;
                this.txtBANK_E_ADDRESS2_3_2.Enabled = false;
                this.btnImportToRMB2_2.Enabled = false;


                this.txtBRANCH_CNAME_4.Enabled = false;
                this.txtBRANCH_ENAME_4.Enabled = false;
                this.txtBANK_CNAME_4.Enabled = false;
                this.txtBANK_ENAME_4.Enabled = false;
                this.txtACCOUNT_CNAME_4.Enabled = false;
                this.txtACCOUNT_ENAME_4.Enabled = false;
                this.txtSWIFT_CODE_4.Enabled = false;
                this.txtACCOUNT_4.Enabled = false;
                this.txtBANK_C_ADDRESS_4.Enabled = false;
                this.txtBANK_E_ADDRESS_4.Enabled = false;
                this.btnImportToEUR1.Enabled = false;

                this.txtBRANCH_CNAME2_4.Enabled = false;
                this.txtBRANCH_ENAME2_4.Enabled = false;
                this.txtBANK_CNAME2_4.Enabled = false;
                this.txtBANK_ENAME2_4.Enabled = false;
                this.txtACCOUNT_CNAME2_4.Enabled = false;
                this.txtACCOUNT_ENAME2_4.Enabled = false;
                this.txtSWIFT_CODE2_4.Enabled = false;
                this.txtACCOUNT2_4.Enabled = false;
                this.txtBANK_C_ADDRESS2_4.Enabled = false;
                this.txtBANK_E_ADDRESS2_4.Enabled = false;
                this.btnImportToEUR2.Enabled = false;


                this.txtBRANCH_CNAME_5.Enabled = false;
                this.txtBRANCH_ENAME_5.Enabled = false;
                this.txtBANK_CNAME_5.Enabled = false;
                this.txtBANK_ENAME_5.Enabled = false;
                this.txtACCOUNT_CNAME_5.Enabled = false;
                this.txtACCOUNT_ENAME_5.Enabled = false;
                this.txtSWIFT_CODE_5.Enabled = false;
                this.txtACCOUNT_5.Enabled = false;
                this.txtBANK_C_ADDRESS_5.Enabled = false;
                this.txtBANK_E_ADDRESS_5.Enabled = false;
                this.btnImportToAUD1.Enabled = false;

                this.txtBRANCH_CNAME2_5.Enabled = false;
                this.txtBRANCH_ENAME2_5.Enabled = false;
                this.txtBANK_CNAME2_5.Enabled = false;
                this.txtBANK_ENAME2_5.Enabled = false;
                this.txtACCOUNT_CNAME2_5.Enabled = false;
                this.txtACCOUNT_ENAME2_5.Enabled = false;
                this.txtSWIFT_CODE2_5.Enabled = false;
                this.txtACCOUNT2_5.Enabled = false;
                this.txtBANK_C_ADDRESS2_5.Enabled = false;
                this.txtBANK_E_ADDRESS2_5.Enabled = false;
                this.btnImportToAUD2.Enabled = false;


                this.txtBRANCH_CNAME_6.Enabled = false;
                this.txtBRANCH_ENAME_6.Enabled = false;
                this.txtBANK_CNAME_6.Enabled = false;
                this.txtBANK_ENAME_6.Enabled = false;
                this.txtACCOUNT_CNAME_6.Enabled = false;
                this.txtACCOUNT_ENAME_6.Enabled = false;
                this.txtSWIFT_CODE_6.Enabled = false;
                this.txtACCOUNT_6.Enabled = false;
                this.txtBANK_C_ADDRESS_6.Enabled = false;
                this.txtBANK_E_ADDRESS_6.Enabled = false;
                this.btnImportToJPY1.Enabled = false;

                this.txtBRANCH_CNAME2_6.Enabled = false;
                this.txtBRANCH_ENAME2_6.Enabled = false;
                this.txtBANK_CNAME2_6.Enabled = false;
                this.txtBANK_ENAME2_6.Enabled = false;
                this.txtACCOUNT_CNAME2_6.Enabled = false;
                this.txtACCOUNT_ENAME2_6.Enabled = false;
                this.txtSWIFT_CODE2_6.Enabled = false;
                this.txtACCOUNT2_6.Enabled = false;
                this.txtBANK_C_ADDRESS2_6.Enabled = false;
                this.txtBANK_E_ADDRESS2_6.Enabled = false;
                this.btnImportToJPY2.Enabled = false;


                this.txtBRANCH_CNAME_7.Enabled = false;
                this.txtBRANCH_ENAME_7.Enabled = false;
                this.txtBANK_CNAME_7.Enabled = false;
                this.txtBANK_ENAME_7.Enabled = false;
                this.txtACCOUNT_CNAME_7.Enabled = false;
                this.txtACCOUNT_ENAME_7.Enabled = false;
                this.txtSWIFT_CODE_7.Enabled = false;
                this.txtACCOUNT_7.Enabled = false;
                this.txtBANK_C_ADDRESS_7.Enabled = false;
                this.txtBANK_E_ADDRESS_7.Enabled = false;
                this.btnImportToNZD1.Enabled = false;

                this.txtBRANCH_CNAME2_7.Enabled = false;
                this.txtBRANCH_ENAME2_7.Enabled = false;
                this.txtBANK_CNAME2_7.Enabled = false;
                this.txtBANK_ENAME2_7.Enabled = false;
                this.txtACCOUNT_CNAME2_7.Enabled = false;
                this.txtACCOUNT_ENAME2_7.Enabled = false;
                this.txtSWIFT_CODE2_7.Enabled = false;
                this.txtACCOUNT2_7.Enabled = false;
                this.txtBANK_C_ADDRESS2_7.Enabled = false;
                this.txtBANK_E_ADDRESS2_7.Enabled = false;
                this.btnImportToNZD2.Enabled = false;


                this.txtID_NUMBER.Enabled = false;
                this.txtCUST_ENAME.Enabled = false;
                this.txtCUST_ENAME2.Enabled = false;
                this.txtID_NUMBER2.Enabled = false;
                this.rdoWoman2.Enabled = false;
                this.rdoMan2.Enabled = false;
                this.txtAddress2.Enabled = false;
                this.txtEMAIL2.Enabled = false;
                this.txtPHONE2.Enabled = false;
                this.txtNATION2.Enabled = false;
                this.txtPASSPORT2.Enabled = false;
                this.dteBirthday2.Enabled = false;
                this.txtCUST_CNAME2.Enabled = false;
                this.txtPOSTAL_CODE2.Enabled = false;
                //this.chkMAIL_TYPE_MONTHLY1.Enabled = false;
                //this.chkMAIL_TYPE_MONTHLY2.Enabled = false;
                //this.chkMAIL_TYPE_END2.Enabled = false;
                //this.chkMAIL_TYPE_END1.Enabled = false;
                //this.chkMAIL_TYPE_END3.Enabled = false;
                //this.txtMAIL_TYPE_END.Enabled = false;
                this.chkMAIL_TYPE_MONTHLY1.Enabled = true;
                this.chkMAIL_TYPE_MONTHLY2.Enabled = true;
                this.chkMAIL_TYPE_END2.Enabled = true;
                this.chkMAIL_TYPE_END1.Enabled = true;
                this.chkMAIL_TYPE_END3.Enabled = true;
                this.txtMAIL_TYPE_END.Enabled = true;
                this.rdoWebDepositType_Null.Enabled = true;
                this.rdoWebDepositType_Y.Enabled = true;

                #endregion
            }

         
        }

        private void SetIND_ContractEnable()
        {
            //  非TW1不可編輯IND合約
            if (Global.ORG_CODE != "TW1")
            {
                this.chk_ind_order.Enabled = false;
                this.chk_ind_order.Checked = false;
            }
        }

        private void InitModifyData()
        {
            #region Set Readonly

            if (IsViewMode)
            {
                #region View Mode

                this.txtPHONE.Enabled = false;
                this.txtPHONE2.Enabled = false;
                this.txtEMAIL.Enabled = false;
                this.txtEMAIL2.Enabled = false;
                this.txtAddress.Enabled = false;
                this.txtAddress2.Enabled = false;
                this.txtPOSTAL_CODE.Enabled = false;
                this.txtPOSTAL_CODE2.Enabled = false;

                this.txtBANK_CNAME.Enabled = false;
                this.txtBANK_ENAME.Enabled = false;
                this.txtBRANCH_CNAME.Enabled = false;
                this.txtBRANCH_ENAME.Enabled = false;
                this.txtACCOUNT_CNAME.Enabled = false;
                this.txtACCOUNT_ENAME.Enabled = false;
                this.txtACCOUNT.Enabled = false;
                this.txtSWIFT_CODE.Enabled = false;
                this.txtBANK_C_ADDRESS.Enabled = false;
                this.txtBANK_E_ADDRESS.Enabled = false;
                this.btnImportToUSD1.Enabled = false;

                this.txtBANK_CNAME2.Enabled = false;
                this.txtBANK_ENAME2.Enabled = false;
                this.txtBRANCH_CNAME2.Enabled = false;
                this.txtBRANCH_ENAME2.Enabled = false;
                this.txtACCOUNT_CNAME2.Enabled = false;
                this.txtACCOUNT_ENAME2.Enabled = false;
                this.txtACCOUNT2.Enabled = false;
                this.txtSWIFT_CODE2.Enabled = false;
                this.txtBANK_C_ADDRESS2.Enabled = false;
                this.txtBANK_E_ADDRESS2.Enabled = false;
                this.btnImportToUSD2.Enabled = false;


                this.txtBANK_CNAME_2.Enabled = false;
                this.txtBANK_ENAME_2.Enabled = false;
                this.txtBRANCH_CNAME_2.Enabled = false;
                this.txtBRANCH_ENAME_2.Enabled = false;
                this.txtACCOUNT_CNAME_2.Enabled = false;
                this.txtACCOUNT_ENAME_2.Enabled = false;
                this.txtACCOUNT_2.Enabled = false;
                this.txtSWIFT_CODE_2.Enabled = false;
                this.txtBANK_C_ADDRESS_2.Enabled = false;
                this.txtBANK_E_ADDRESS_2.Enabled = false;
                this.btnImportToNTD1.Enabled = false;

                this.txtBANK_CNAME2_2.Enabled = false;
                this.txtBANK_ENAME2_2.Enabled = false;
                this.txtBRANCH_CNAME2_2.Enabled = false;
                this.txtBRANCH_ENAME2_2.Enabled = false;
                this.txtACCOUNT_CNAME2_2.Enabled = false;
                this.txtACCOUNT_ENAME2_2.Enabled = false;
                this.txtACCOUNT2_2.Enabled = false;
                this.txtSWIFT_CODE2_2.Enabled = false;
                this.txtBANK_C_ADDRESS2_2.Enabled = false;
                this.txtBANK_E_ADDRESS2_2.Enabled = false;
                this.btnImportToNTD2.Enabled = false;


                this.txtBANK_CNAME_3.Enabled = false;
                this.txtBANK_ENAME_3.Enabled = false;
                this.txtBRANCH_CNAME_3.Enabled = false;
                this.txtBRANCH_ENAME_3.Enabled = false;
                this.txtACCOUNT_CNAME_3.Enabled = false;
                this.txtACCOUNT_ENAME_3.Enabled = false;
                this.txtACCOUNT_3.Enabled = false;
                this.txtSWIFT_CODE_3.Enabled = false;
                this.txtBANK_C_ADDRESS_3.Enabled = false;
                this.txtBANK_E_ADDRESS_3.Enabled = false;
                this.btnImportToRMB1.Enabled = false;

                this.txtBANK_CNAME2_3.Enabled = false;
                this.txtBANK_ENAME2_3.Enabled = false;
                this.txtBRANCH_CNAME2_3.Enabled = false;
                this.txtBRANCH_ENAME2_3.Enabled = false;
                this.txtACCOUNT_CNAME2_3.Enabled = false;
                this.txtACCOUNT_ENAME2_3.Enabled = false;
                this.txtACCOUNT2_3.Enabled = false;
                this.txtSWIFT_CODE2_3.Enabled = false;
                this.txtBANK_C_ADDRESS2_3.Enabled = false;
                this.txtBANK_E_ADDRESS2_3.Enabled = false;
                this.btnImportToRMB2.Enabled = false;

                this.txtBANK_CNAME_3_2.Enabled = false;
                this.txtBANK_ENAME_3_2.Enabled = false;
                this.txtBRANCH_CNAME_3_2.Enabled = false;
                this.txtBRANCH_ENAME_3_2.Enabled = false;
                this.txtACCOUNT_CNAME_3_2.Enabled = false;
                this.txtACCOUNT_ENAME_3_2.Enabled = false;
                this.txtACCOUNT_3_2.Enabled = false;
                this.txtSWIFT_CODE_3_2.Enabled = false;
                this.txtBANK_C_ADDRESS_3_2.Enabled = false;
                this.txtBANK_E_ADDRESS_3_2.Enabled = false;
                this.btnImportToRMB1_2.Enabled = false;

                this.txtBANK_CNAME2_3_2.Enabled = false;
                this.txtBANK_ENAME2_3_2.Enabled = false;
                this.txtBRANCH_CNAME2_3_2.Enabled = false;
                this.txtBRANCH_ENAME2_3_2.Enabled = false;
                this.txtACCOUNT_CNAME2_3_2.Enabled = false;
                this.txtACCOUNT_ENAME2_3_2.Enabled = false;
                this.txtACCOUNT2_3_2.Enabled = false;
                this.txtSWIFT_CODE2_3_2.Enabled = false;
                this.txtBANK_C_ADDRESS2_3_2.Enabled = false;
                this.txtBANK_E_ADDRESS2_3_2.Enabled = false;
                this.btnImportToRMB2_2.Enabled = false;


                this.txtBANK_CNAME_4.Enabled = false;
                this.txtBANK_ENAME_4.Enabled = false;
                this.txtBRANCH_CNAME_4.Enabled = false;
                this.txtBRANCH_ENAME_4.Enabled = false;
                this.txtACCOUNT_CNAME_4.Enabled = false;
                this.txtACCOUNT_ENAME_4.Enabled = false;
                this.txtACCOUNT_4.Enabled = false;
                this.txtSWIFT_CODE_4.Enabled = false;
                this.txtBANK_C_ADDRESS_4.Enabled = false;
                this.txtBANK_E_ADDRESS_4.Enabled = false;
                this.btnImportToEUR1.Enabled = false;

                this.txtBANK_CNAME2_4.Enabled = false;
                this.txtBANK_ENAME2_4.Enabled = false;
                this.txtBRANCH_CNAME2_4.Enabled = false;
                this.txtBRANCH_ENAME2_4.Enabled = false;
                this.txtACCOUNT_CNAME2_4.Enabled = false;
                this.txtACCOUNT_ENAME2_4.Enabled = false;
                this.txtACCOUNT2_4.Enabled = false;
                this.txtSWIFT_CODE2_4.Enabled = false;
                this.txtBANK_C_ADDRESS2_4.Enabled = false;
                this.txtBANK_E_ADDRESS2_4.Enabled = false;
                this.btnImportToEUR2.Enabled = false;


                this.txtBANK_CNAME_5.Enabled = false;
                this.txtBANK_ENAME_5.Enabled = false;
                this.txtBRANCH_CNAME_5.Enabled = false;
                this.txtBRANCH_ENAME_5.Enabled = false;
                this.txtACCOUNT_CNAME_5.Enabled = false;
                this.txtACCOUNT_ENAME_5.Enabled = false;
                this.txtACCOUNT_5.Enabled = false;
                this.txtSWIFT_CODE_5.Enabled = false;
                this.txtBANK_C_ADDRESS_5.Enabled = false;
                this.txtBANK_E_ADDRESS_5.Enabled = false;
                this.btnImportToAUD1.Enabled = false;

                this.txtBANK_CNAME2_5.Enabled = false;
                this.txtBANK_ENAME2_5.Enabled = false;
                this.txtBRANCH_CNAME2_5.Enabled = false;
                this.txtBRANCH_ENAME2_5.Enabled = false;
                this.txtACCOUNT_CNAME2_5.Enabled = false;
                this.txtACCOUNT_ENAME2_5.Enabled = false;
                this.txtACCOUNT2_5.Enabled = false;
                this.txtSWIFT_CODE2_5.Enabled = false;
                this.txtBANK_C_ADDRESS2_5.Enabled = false;
                this.txtBANK_E_ADDRESS2_5.Enabled = false;
                this.btnImportToAUD2.Enabled = false;


                this.txtBANK_CNAME_6.Enabled = false;
                this.txtBANK_ENAME_6.Enabled = false;
                this.txtBRANCH_CNAME_6.Enabled = false;
                this.txtBRANCH_ENAME_6.Enabled = false;
                this.txtACCOUNT_CNAME_6.Enabled = false;
                this.txtACCOUNT_ENAME_6.Enabled = false;
                this.txtACCOUNT_6.Enabled = false;
                this.txtSWIFT_CODE_6.Enabled = false;
                this.txtBANK_C_ADDRESS_6.Enabled = false;
                this.txtBANK_E_ADDRESS_6.Enabled = false;
                this.btnImportToJPY1.Enabled = false;

                this.txtBANK_CNAME2_6.Enabled = false;
                this.txtBANK_ENAME2_6.Enabled = false;
                this.txtBRANCH_CNAME2_6.Enabled = false;
                this.txtBRANCH_ENAME2_6.Enabled = false;
                this.txtACCOUNT_CNAME2_6.Enabled = false;
                this.txtACCOUNT_ENAME2_6.Enabled = false;
                this.txtACCOUNT2_6.Enabled = false;
                this.txtSWIFT_CODE2_6.Enabled = false;
                this.txtBANK_C_ADDRESS2_6.Enabled = false;
                this.txtBANK_E_ADDRESS2_6.Enabled = false;
                this.btnImportToJPY2.Enabled = false;


                this.txtBANK_CNAME_7.Enabled = false;
                this.txtBANK_ENAME_7.Enabled = false;
                this.txtBRANCH_CNAME_7.Enabled = false;
                this.txtBRANCH_ENAME_7.Enabled = false;
                this.txtACCOUNT_CNAME_7.Enabled = false;
                this.txtACCOUNT_ENAME_7.Enabled = false;
                this.txtACCOUNT_7.Enabled = false;
                this.txtSWIFT_CODE_7.Enabled = false;
                this.txtBANK_C_ADDRESS_7.Enabled = false;
                this.txtBANK_E_ADDRESS_7.Enabled = false;
                this.btnImportToNZD1.Enabled = false;

                this.txtBANK_CNAME2_7.Enabled = false;
                this.txtBANK_ENAME2_7.Enabled = false;
                this.txtBRANCH_CNAME2_7.Enabled = false;
                this.txtBRANCH_ENAME2_7.Enabled = false;
                this.txtACCOUNT_CNAME2_7.Enabled = false;
                this.txtACCOUNT_ENAME2_7.Enabled = false;
                this.txtACCOUNT2_7.Enabled = false;
                this.txtSWIFT_CODE2_7.Enabled = false;
                this.txtBANK_C_ADDRESS2_7.Enabled = false;
                this.txtBANK_E_ADDRESS2_7.Enabled = false;
                this.btnImportToNZD2.Enabled = false;


                this.chkMAIL_TYPE_MONTHLY1.Enabled = false;
                this.chkMAIL_TYPE_MONTHLY2.Enabled = false;
                this.chkMAIL_TYPE_END2.Enabled = false;
                this.chkMAIL_TYPE_END1.Enabled = false;
                this.chkMAIL_TYPE_END3.Enabled = false;
                this.txtMAIL_TYPE_END.Enabled = false;
                this.rdoWebDepositType_Null.Enabled = false;
                this.rdoWebDepositType_Y.Enabled = false;

                this.txtOrderNo.Enabled = false;
                this.txtParentOrderNo.Enabled = false;
                this.btnBringParentOrder.Enabled = false;
                this.txtAmount_USD.Enabled = false;
                this.txtAmount_NTD.Enabled = false;
                this.txtAmount_RMB.Enabled = false;
                this.txtAmount_EUR.Enabled = false;
                this.txtAmount_AUD.Enabled = false;
                this.txtAmount_JPY.Enabled = false;
                this.txtAmount_NZD.Enabled = false;
                this.txtAmount_USD_OLD.Enabled = false;
                this.txtAmount_NTD_OLD.Enabled = false;
                this.txtAmount_RMB_OLD.Enabled = false;
                this.txtAmount_EUR_OLD.Enabled = false;
                this.txtAmount_AUD_OLD.Enabled = false;
                this.txtAmount_JPY_OLD.Enabled = false;
                this.txtAmount_NZD_OLD.Enabled = false;
                this.txtInterest_USD.Enabled = false;
                this.txtInterest_NTD.Enabled = false;
                this.txtInterest_RMB.Enabled = false;
                this.txtInterest_EUR.Enabled = false;
                this.txtInterest_AUD.Enabled = false;
                this.txtInterest_JPY.Enabled = false;
                this.txtInterest_NZD.Enabled = false;
                this.txtInterestedRate.Enabled = false;
                this.checkBox1.Enabled = false;
                this.checkBox2.Enabled = false;
                this.chkIsMultiContract.Enabled = false;
                this.chk_ind_order.Enabled = false;
                this.cboIS_REMIT_TO_AUS.Enabled = false;
                this.rdoBonusA.Enabled = false;
                this.rdoBonusB.Enabled = false;

                this.dteOrderEnd.Enabled = false;
                this.dteFromDate.Enabled = false;
                this.dteExtendDate.Enabled = false;
                this.btnDelExtendDate.Enabled = false;
                this.dteEndDate.Enabled = false;
                this.btnDelFromDate.Enabled = false;
                this.btnDelBirthday.Enabled = false;
                this.btnDelBirthday2.Enabled = false;
                this.txtInterestedRate.Enabled = false;

                this.txtYear.Enabled = false;
                this.txtMT4.Enabled = false;
                this.txtNote.Enabled = false;
                this.btnSubmit.Visible = false;
                this.btnComfirmModify.Visible = false;
                this.SetModifyALLStatus(false);
                this.cb_close.Enabled = false;
                this.btnBringSales.Enabled = false;
                this.txtIBCode.Enabled = false;
                this.btnBringSales_Internal.Enabled = false;
                this.txtIBCode_Internal.Enabled = false;
                #endregion
            }
            else
            {
                #region Edit Mode

                this.txtPHONE.Enabled = true;
                this.txtPHONE2.Enabled = true;
                this.txtEMAIL.Enabled = true;
                this.txtEMAIL2.Enabled = true;
                this.txtAddress.Enabled = true;
                this.txtAddress2.Enabled = true;
                this.txtPOSTAL_CODE.Enabled = true;
                this.txtPOSTAL_CODE2.Enabled = true;

                this.txtBANK_CNAME.Enabled = true;
                this.txtBANK_ENAME.Enabled = true;
                this.txtBRANCH_CNAME.Enabled = true;
                this.txtBRANCH_ENAME.Enabled = true;
                this.txtACCOUNT_CNAME.Enabled = true;
                this.txtACCOUNT_ENAME.Enabled = true;
                this.txtACCOUNT.Enabled = true;
                this.txtSWIFT_CODE.Enabled = true;
                this.txtBANK_C_ADDRESS.Enabled = true;
                this.txtBANK_E_ADDRESS.Enabled = true;
                this.btnImportToUSD1.Enabled = true;

                this.txtBANK_CNAME2.Enabled = true;
                this.txtBANK_ENAME2.Enabled = true;
                this.txtBRANCH_CNAME2.Enabled = true;
                this.txtBRANCH_ENAME2.Enabled = true;
                this.txtACCOUNT_CNAME2.Enabled = true;
                this.txtACCOUNT_ENAME2.Enabled = true;
                this.txtACCOUNT2.Enabled = true;
                this.txtSWIFT_CODE2.Enabled = true;
                this.txtBANK_C_ADDRESS2.Enabled = true;
                this.txtBANK_E_ADDRESS2.Enabled = true;
                this.btnImportToUSD2.Enabled = true;


                this.txtBANK_CNAME_2.Enabled = true;
                this.txtBANK_ENAME_2.Enabled = true;
                this.txtBRANCH_CNAME_2.Enabled = true;
                this.txtBRANCH_ENAME_2.Enabled = true;
                this.txtACCOUNT_CNAME_2.Enabled = true;
                this.txtACCOUNT_ENAME_2.Enabled = true;
                this.txtACCOUNT_2.Enabled = true;
                this.txtSWIFT_CODE_2.Enabled = true;
                this.txtBANK_C_ADDRESS_2.Enabled = true;
                this.txtBANK_E_ADDRESS_2.Enabled = true;
                this.btnImportToNTD1.Enabled = true;

                this.txtBANK_CNAME2_2.Enabled = true;
                this.txtBANK_ENAME2_2.Enabled = true;
                this.txtBRANCH_CNAME2_2.Enabled = true;
                this.txtBRANCH_ENAME2_2.Enabled = true;
                this.txtACCOUNT_CNAME2_2.Enabled = true;
                this.txtACCOUNT_ENAME2_2.Enabled = true;
                this.txtACCOUNT2_2.Enabled = true;
                this.txtSWIFT_CODE2_2.Enabled = true;
                this.txtBANK_C_ADDRESS2_2.Enabled = true;
                this.txtBANK_E_ADDRESS2_2.Enabled = true;
                this.btnImportToNTD2.Enabled = true;


                this.txtBANK_CNAME_3.Enabled = true;
                this.txtBANK_ENAME_3.Enabled = true;
                this.txtBRANCH_CNAME_3.Enabled = true;
                this.txtBRANCH_ENAME_3.Enabled = true;
                this.txtACCOUNT_CNAME_3.Enabled = true;
                this.txtACCOUNT_ENAME_3.Enabled = true;
                this.txtACCOUNT_3.Enabled = true;
                this.txtSWIFT_CODE_3.Enabled = true;
                this.txtBANK_C_ADDRESS_3.Enabled = true;
                this.txtBANK_E_ADDRESS_3.Enabled = true;
                this.btnImportToRMB1.Enabled = true;

                this.txtBANK_CNAME2_3.Enabled = true;
                this.txtBANK_ENAME2_3.Enabled = true;
                this.txtBRANCH_CNAME2_3.Enabled = true;
                this.txtBRANCH_ENAME2_3.Enabled = true;
                this.txtACCOUNT_CNAME2_3.Enabled = true;
                this.txtACCOUNT_ENAME2_3.Enabled = true;
                this.txtACCOUNT2_3.Enabled = true;
                this.txtSWIFT_CODE2_3.Enabled = true;
                this.txtBANK_C_ADDRESS2_3.Enabled = true;
                this.txtBANK_E_ADDRESS2_3.Enabled = true;
                this.btnImportToRMB2.Enabled = true;

                this.txtBANK_CNAME_3_2.Enabled = true;
                this.txtBANK_ENAME_3_2.Enabled = true;
                this.txtBRANCH_CNAME_3_2.Enabled = true;
                this.txtBRANCH_ENAME_3_2.Enabled = true;
                this.txtACCOUNT_CNAME_3_2.Enabled = true;
                this.txtACCOUNT_ENAME_3_2.Enabled = true;
                this.txtACCOUNT_3_2.Enabled = true;
                this.txtSWIFT_CODE_3_2.Enabled = true;
                this.txtBANK_C_ADDRESS_3_2.Enabled = true;
                this.txtBANK_E_ADDRESS_3_2.Enabled = true;
                this.btnImportToRMB1_2.Enabled = true;

                this.txtBANK_CNAME2_3_2.Enabled = true;
                this.txtBANK_ENAME2_3_2.Enabled = true;
                this.txtBRANCH_CNAME2_3_2.Enabled = true;
                this.txtBRANCH_ENAME2_3_2.Enabled = true;
                this.txtACCOUNT_CNAME2_3_2.Enabled = true;
                this.txtACCOUNT_ENAME2_3_2.Enabled = true;
                this.txtACCOUNT2_3_2.Enabled = true;
                this.txtSWIFT_CODE2_3_2.Enabled = true;
                this.txtBANK_C_ADDRESS2_3_2.Enabled = true;
                this.txtBANK_E_ADDRESS2_3_2.Enabled = true;
                this.btnImportToRMB2_2.Enabled = true;


                this.txtBANK_CNAME_4.Enabled = true;
                this.txtBANK_ENAME_4.Enabled = true;
                this.txtBRANCH_CNAME_4.Enabled = true;
                this.txtBRANCH_ENAME_4.Enabled = true;
                this.txtACCOUNT_CNAME_4.Enabled = true;
                this.txtACCOUNT_ENAME_4.Enabled = true;
                this.txtACCOUNT_4.Enabled = true;
                this.txtSWIFT_CODE_4.Enabled = true;
                this.txtBANK_C_ADDRESS_4.Enabled = true;
                this.txtBANK_E_ADDRESS_4.Enabled = true;
                this.btnImportToEUR1.Enabled = true;

                this.txtBANK_CNAME2_4.Enabled = true;
                this.txtBANK_ENAME2_4.Enabled = true;
                this.txtBRANCH_CNAME2_4.Enabled = true;
                this.txtBRANCH_ENAME2_4.Enabled = true;
                this.txtACCOUNT_CNAME2_4.Enabled = true;
                this.txtACCOUNT_ENAME2_4.Enabled = true;
                this.txtACCOUNT2_4.Enabled = true;
                this.txtSWIFT_CODE2_4.Enabled = true;
                this.txtBANK_C_ADDRESS2_4.Enabled = true;
                this.txtBANK_E_ADDRESS2_4.Enabled = true;
                this.btnImportToEUR2.Enabled = true;

                this.txtBANK_CNAME_5.Enabled = true;
                this.txtBANK_ENAME_5.Enabled = true;
                this.txtBRANCH_CNAME_5.Enabled = true;
                this.txtBRANCH_ENAME_5.Enabled = true;
                this.txtACCOUNT_CNAME_5.Enabled = true;
                this.txtACCOUNT_ENAME_5.Enabled = true;
                this.txtACCOUNT_5.Enabled = true;
                this.txtSWIFT_CODE_5.Enabled = true;
                this.txtBANK_C_ADDRESS_5.Enabled = true;
                this.txtBANK_E_ADDRESS_5.Enabled = true;
                this.btnImportToAUD1.Enabled = true;

                this.txtBANK_CNAME2_5.Enabled = true;
                this.txtBANK_ENAME2_5.Enabled = true;
                this.txtBRANCH_CNAME2_5.Enabled = true;
                this.txtBRANCH_ENAME2_5.Enabled = true;
                this.txtACCOUNT_CNAME2_5.Enabled = true;
                this.txtACCOUNT_ENAME2_5.Enabled = true;
                this.txtACCOUNT2_5.Enabled = true;
                this.txtSWIFT_CODE2_5.Enabled = true;
                this.txtBANK_C_ADDRESS2_5.Enabled = true;
                this.txtBANK_E_ADDRESS2_5.Enabled = true;
                this.btnImportToAUD2.Enabled = true;


                this.txtBANK_CNAME_6.Enabled = true;
                this.txtBANK_ENAME_6.Enabled = true;
                this.txtBRANCH_CNAME_6.Enabled = true;
                this.txtBRANCH_ENAME_6.Enabled = true;
                this.txtACCOUNT_CNAME_6.Enabled = true;
                this.txtACCOUNT_ENAME_6.Enabled = true;
                this.txtACCOUNT_6.Enabled = true;
                this.txtSWIFT_CODE_6.Enabled = true;
                this.txtBANK_C_ADDRESS_6.Enabled = true;
                this.txtBANK_E_ADDRESS_6.Enabled = true;
                this.btnImportToJPY1.Enabled = true;

                this.txtBANK_CNAME2_6.Enabled = true;
                this.txtBANK_ENAME2_6.Enabled = true;
                this.txtBRANCH_CNAME2_6.Enabled = true;
                this.txtBRANCH_ENAME2_6.Enabled = true;
                this.txtACCOUNT_CNAME2_6.Enabled = true;
                this.txtACCOUNT_ENAME2_6.Enabled = true;
                this.txtACCOUNT2_6.Enabled = true;
                this.txtSWIFT_CODE2_6.Enabled = true;
                this.txtBANK_C_ADDRESS2_6.Enabled = true;
                this.txtBANK_E_ADDRESS2_6.Enabled = true;
                this.btnImportToJPY2.Enabled = true;


                this.txtBANK_CNAME_7.Enabled = true;
                this.txtBANK_ENAME_7.Enabled = true;
                this.txtBRANCH_CNAME_7.Enabled = true;
                this.txtBRANCH_ENAME_7.Enabled = true;
                this.txtACCOUNT_CNAME_7.Enabled = true;
                this.txtACCOUNT_ENAME_7.Enabled = true;
                this.txtACCOUNT_7.Enabled = true;
                this.txtSWIFT_CODE_7.Enabled = true;
                this.txtBANK_C_ADDRESS_7.Enabled = true;
                this.txtBANK_E_ADDRESS_7.Enabled = true;
                this.btnImportToNZD1.Enabled = true;

                this.txtBANK_CNAME2_7.Enabled = true;
                this.txtBANK_ENAME2_7.Enabled = true;
                this.txtBRANCH_CNAME2_7.Enabled = true;
                this.txtBRANCH_ENAME2_7.Enabled = true;
                this.txtACCOUNT_CNAME2_7.Enabled = true;
                this.txtACCOUNT_ENAME2_7.Enabled = true;
                this.txtACCOUNT2_7.Enabled = true;
                this.txtSWIFT_CODE2_7.Enabled = true;
                this.txtBANK_C_ADDRESS2_7.Enabled = true;
                this.txtBANK_E_ADDRESS2_7.Enabled = true;
                this.btnImportToNZD2.Enabled = true;


                this.chkMAIL_TYPE_MONTHLY1.Enabled = true;
                this.chkMAIL_TYPE_MONTHLY2.Enabled = true;
                this.chkMAIL_TYPE_END2.Enabled = true;
                this.chkMAIL_TYPE_END1.Enabled = true;
                this.chkMAIL_TYPE_END3.Enabled = true;
                this.txtMAIL_TYPE_END.Enabled = true;
                this.rdoWebDepositType_Null.Enabled = true;
                this.rdoWebDepositType_Y.Enabled = true;

                this.txtOrderNo.Enabled = false;
                this.txtParentOrderNo.Enabled = false;
                this.btnBringParentOrder.Enabled = false;
                this.txtAmount_USD.Enabled = false;
                this.txtAmount_NTD.Enabled = false;
                this.txtAmount_RMB.Enabled = false;
                this.txtAmount_EUR.Enabled = false;
                this.txtAmount_AUD.Enabled = false;
                this.txtAmount_JPY.Enabled = false;
                this.txtAmount_NZD.Enabled = false;
                this.txtAmount_USD_OLD.Enabled = false;
                this.txtAmount_NTD_OLD.Enabled = false;
                this.txtAmount_RMB_OLD.Enabled = false;
                this.txtAmount_EUR_OLD.Enabled = false;
                this.txtAmount_AUD_OLD.Enabled = false;
                this.txtAmount_JPY_OLD.Enabled = false;
                this.txtAmount_NZD_OLD.Enabled = false;
                this.txtInterest_USD.Enabled = false;
                this.txtInterest_NTD.Enabled = false;
                this.txtInterest_RMB.Enabled = false;
                this.txtInterest_EUR.Enabled = false;
                this.txtInterest_AUD.Enabled = false;
                this.txtInterest_JPY.Enabled = false;
                this.txtInterest_NZD.Enabled = false;
                this.txtInterestedRate.Enabled = false;
                this.checkBox1.Enabled = false;
                this.checkBox2.Enabled = false;
                this.chkIsMultiContract.Enabled = false;
                this.chk_ind_order.Enabled = false;
                this.cboIS_REMIT_TO_AUS.Enabled = true;
                this.rdoBonusA.Enabled = false;
                this.rdoBonusB.Enabled = false;

                this.dteOrderEnd.Enabled = true;// false;
                this.dteFromDate.Enabled = false;
                this.dteExtendDate.Enabled = true;
                this.btnDelExtendDate.Enabled = true;
                this.dteEndDate.Enabled = true;

                this.txtYear.Enabled = false;
                this.txtMT4.Enabled = false;
                this.txtNote.Enabled = false;
                this.btnSubmit.Visible = false;
                this.btnComfirmModify.Visible = true;
                this.SetModifyALLStatus(true);
                this.cb_close.Enabled = true;

                #endregion
            }
            #endregion


            this.SetIND_ContractEnable();
            this.chkIsMultiContract.Visible = false;
            this.chk_ind_order.Checked = false;
            this.rdoBonusA.Checked = false;
            this.rdoBonusB.Checked = false;
            this.lbl2020Condition.Text = string.Empty;

            //  Add on 2020/03/22
            if (DateTime.Now >= new DateTime(2020, 04, 01, 12, 00, 00))
            {
                //  2020/04/01 起預設勾選B
                this.rdoBonusA.Checked = false;
                this.rdoBonusB.Checked = true;
            }
            if (DateTime.Now >= new DateTime(2020, 06, 30, 12, 00, 00))
            {
                //  活動於2020/06/30結束
                this.rdoBonusA.Checked = false;
                this.rdoBonusB.Checked = false;
            }

            if (this.IsOnlyCust == false)
            {

                var dataOrder = base.Cust.GetOrderDatabyOrderID(this.strOrderID);
                if (dataOrder.Count > 0)
                {
                    var dataSingle = dataOrder[0];

                    this.txtOrderNo.Text = dataSingle.ORDER_NO.ConvertToString();
                    this.txtAmount_USD.Text = dataSingle.AMOUNT_USD == null ? "" : dataSingle.AMOUNT_USD.Value.ConvertToString();
                    this.txtAmount_NTD.Text = dataSingle.AMOUNT_NTD == null ? "" : dataSingle.AMOUNT_NTD.Value.ConvertToString();
                    this.txtAmount_RMB.Text = dataSingle.AMOUNT_RMB == null ? "" : dataSingle.AMOUNT_RMB.Value.ConvertToString();
                    this.txtAmount_EUR.Text = dataSingle.AMOUNT_EUR == null ? "" : dataSingle.AMOUNT_EUR.Value.ConvertToString();
                    this.txtAmount_AUD.Text = dataSingle.AMOUNT_AUD == null ? "" : dataSingle.AMOUNT_AUD.Value.ConvertToString();
                    this.txtAmount_JPY.Text = dataSingle.AMOUNT_JPY == null ? "" : dataSingle.AMOUNT_JPY.Value.ConvertToString();
                    this.txtAmount_NZD.Text = dataSingle.AMOUNT_NZD == null ? "" : dataSingle.AMOUNT_NZD.Value.ConvertToString();
                    this.txtParentOrderNo.Text = dataSingle.PARENT_ORDER_NO.ConvertToString();
                    this.txtAmount_USD_OLD.Text = dataSingle.OLD_AMOUNT_USD == null ? "" : dataSingle.OLD_AMOUNT_USD.Value.ConvertToString();
                    this.txtAmount_NTD_OLD.Text = dataSingle.OLD_AMOUNT_NTD == null ? "" : dataSingle.OLD_AMOUNT_NTD.Value.ConvertToString();
                    this.txtAmount_RMB_OLD.Text = dataSingle.OLD_AMOUNT_RMB == null ? "" : dataSingle.OLD_AMOUNT_RMB.Value.ConvertToString();
                    this.txtAmount_EUR_OLD.Text = dataSingle.OLD_AMOUNT_EUR == null ? "" : dataSingle.OLD_AMOUNT_EUR.Value.ConvertToString();
                    this.txtAmount_AUD_OLD.Text = dataSingle.OLD_AMOUNT_AUD == null ? "" : dataSingle.OLD_AMOUNT_AUD.Value.ConvertToString();
                    this.txtAmount_JPY_OLD.Text = dataSingle.OLD_AMOUNT_JPY == null ? "" : dataSingle.OLD_AMOUNT_JPY.Value.ConvertToString();
                    this.txtAmount_NZD_OLD.Text = dataSingle.OLD_AMOUNT_NZD == null ? "" : dataSingle.OLD_AMOUNT_NZD.Value.ConvertToString();

                    this.txtInterest_USD.Text = dataSingle.INTEREST_USD == null ? "" : dataSingle.INTEREST_USD.Value.ConvertToString();
                    this.txtInterest_NTD.Text = dataSingle.INTEREST_NTD == null ? "" : dataSingle.INTEREST_NTD.Value.ConvertToString();
                    this.txtInterest_RMB.Text = dataSingle.INTEREST_RMB == null ? "" : dataSingle.INTEREST_RMB.Value.ConvertToString();
                    this.txtInterest_EUR.Text = dataSingle.INTEREST_EUR == null ? "" : dataSingle.INTEREST_EUR.Value.ConvertToString();
                    this.txtInterest_AUD.Text = dataSingle.INTEREST_AUD == null ? "" : dataSingle.INTEREST_AUD.Value.ConvertToString();
                    this.txtInterest_JPY.Text = dataSingle.INTEREST_JPY == null ? "" : dataSingle.INTEREST_JPY.Value.ConvertToString();
                    this.txtInterest_NZD.Text = dataSingle.INTEREST_NZD == null ? "" : dataSingle.INTEREST_NZD.Value.ConvertToString();

                    if (this.txtParentOrderNo.Text.Trim() != string.Empty)
                    {
                        this.chkIsMultiContract.Visible = true;
                    }

                    if (dataSingle.FROM_DATE != null)
                    {
                        this.dteFromDate.Format = DateTimePickerFormat.Long;
                        this.dteFromDate.Value = dataSingle.FROM_DATE.Value;
                        this.dteFromDate.Text = dataSingle.FROM_DATE.Value.ToString("yyyy/MM/dd");
                    }
                    if (dataSingle.END_DATE != null)
                    {
                        //this.dteEndDate.Format = DateTimePickerFormat.Long;
                        //this.dteEndDate.Value = dataSingle.END_DATE.Value;
                        //this.dteEndDate.Text = dataSingle.END_DATE.Value.ToString("yyyy/MM/dd");
                        this.dteEndDate.Text = dataSingle.END_DATE.Value.ToString("yyyy/MM/dd");
                    }
                    this.txtYear.Text = dataSingle.YEAR == null ? "" : dataSingle.YEAR.Value.ConvertToString();
                    if (dataSingle.EXTEND_DATE != null)
                    {
                        this.dteExtendDate.Format = DateTimePickerFormat.Long;
                        this.dteExtendDate.Value = dataSingle.EXTEND_DATE.Value;
                        this.dteExtendDate.Text = dataSingle.EXTEND_DATE.Value.ToString("yyyy/MM/dd");

                        this.dteOrderEnd.Format = DateTimePickerFormat.Long;
                        this.dteOrderEnd.Value = dataSingle.EXTEND_DATE.Value;
                        this.dteOrderEnd.Text = dataSingle.EXTEND_DATE.Value.ToString("yyyy/MM/dd");

                        this.cb_close.Checked = true;
                    }
                    if (dataSingle.ORDER_END != null)
                    {
                        this.dteOrderEnd.Value = dataSingle.ORDER_END.Value;
                        this.dteOrderEnd.Text = dataSingle.ORDER_END.Value.ToString("yyyy/MM/dd");
                        this.cb_close.Checked = true;
                    }
                    this.txtMT4.Text = dataSingle.MT4_ACCOUNT.ConvertToString();
                    this.txtNote.Text = dataSingle.NOTE.ConvertToString();

                    this.txtIBCode.Text = dataSingle.IB_CODE.ConvertToString();
                    this.txtSales.Text = dataSingle.SALES_NAME.ConvertToString();
                    this.txtSales.Tag = dataSingle.SALES_EMP_CODE.ConvertToString();

                    this.txtIBCode_Internal.Text = dataSingle.INTERNAL_IB_CODE.ConvertToString();
                    this.txtSales_Intermal.Text = dataSingle.SALES_NAME_INTERNAL.ConvertToString();
                    this.txtSales_Intermal.Tag = dataSingle.SALES_EMP_CODE_INTERNAL.ConvertToString();


                    if (dataSingle.PROJECT.ConvertToString() == "2019金豬報喜")
                    {
                        this.checkBox1.Checked = true;
                    }
                    else
                    {
                        this.checkBox1.Checked = false;
                    }

                    if (dataSingle.INTEREST_RATE != null)
                    {
                        this.txtInterestedRate.Text = dataSingle.INTEREST_RATE.Value.ConvertToString();
                    }

                    //  Add on 2019/03/22
                    if (dataSingle.STATUS_CODE.ConvertToString() == "9" && dataSingle.REJECT_REASON.ConvertToString().Trim() != string.Empty)
                    {
                        this.IsRejectByHK = true;
                    }
                    //  Add on 2019/07/24
                    if (dataSingle.WITHDRAWAL_MODE.ConvertToString() == "AUTO")
                    {
                        this.checkBox2.Checked = true;
                    }
                    else
                    {
                        this.checkBox2.Checked = false;
                    }
                    this.SetRMB_CheckBox();
                    this.chkIsMultiContract.Checked = dataSingle.MULTI_ORDER_FLAG.ConvertToString().Trim() == "Y";
                    //  Add on 2019/09/17
                    //this.chkIS_REMIT_TO_AUS.Checked = dataSingle.IS_REMIT_TO_AUS.ConvertToString().Trim().ToUpper() == "Y";
                    this.cboIS_REMIT_TO_AUS.SelectedIndex = -1;
                    switch (dataSingle.IS_REMIT_TO_AUS.ConvertToString().Trim().ToUpper())
                    {
                        case "N":
                            this.cboIS_REMIT_TO_AUS.SelectedIndex = 1;
                            break;
                        case "E":
                            this.cboIS_REMIT_TO_AUS.SelectedIndex = 2;
                            break;
                        default:
                            this.cboIS_REMIT_TO_AUS.SelectedIndex = 0;
                            break;
                    }

                    if ((dataSingle.STATUS_CODE == null ? "0" : dataSingle.STATUS_CODE.Value.ConvertToString()) == "-1")
                    {
                        this.IsOrderVoid = true;
                        this.btnTransOrder.Visible = false;
                    }
                    if (dataSingle.ORG_CODE.ConvertToString().Trim().ToUpper() == "IND")
                    {
                        this.chk_ind_order.Checked = true;
                        this.txtDiscount.Text = dataSingle.DISCOUNT_AMOUNT == null ? "0" : dataSingle.DISCOUNT_AMOUNT.Value.ConvertToString();
                    }
                    else
                    {
                        this.chk_ind_order.Checked = false;
                        this.txtDiscount.Text = string.Empty;
                    }
                    if (dataSingle.BONUS_OPTION.ConvertToString() == "A")
                    {
                        this.rdoBonusA.Checked = true;
                    }
                    else if (dataSingle.BONUS_OPTION.ConvertToString() == "B")
                    {
                        this.rdoBonusB.Checked = true;
                    }
                    if (dataSingle.PROJECT.ConvertToString().Trim() == "2020金鼠年活動")
                    {
                        this.lbl2020Condition.Text = "符合";
                    }
                }
                else
                {
                    MessageBox.Show("訂單資料不存在");
                    this.Close();
                }

            }

        }

        private BL_CUST GetSaveData_Cust
        {
            get
            {
                var data = new BL_CUST()
                {
                    CUST_CNAME = this.txtCUST_CNAME.Text,
                    CUST_ENAME = this.txtCUST_ENAME.Text,
                    CUST_CNAME2 = this.txtCUST_CNAME2.Text,
                    CUST_ENAME2 = this.txtCUST_ENAME2.Text,
                    ID_NUMBER = this.txtID_NUMBER.Text,
                    ID_NUMBER2 = this.txtID_NUMBER2.Text,
                    PASSPORT = this.txtPASSPORT.Text,
                    PASSPORT2 = this.txtPASSPORT2.Text,
                    PHONE_1 = this.txtPHONE.Text,
                    PHONE_2 = this.txtPHONE2.Text,
                    EMAIL_1 = this.txtEMAIL.Text,
                    EMAIL_2 = this.txtEMAIL2.Text,
                    C_ADDRESS = this.txtAddress.Text,
                    C_ADDRESS2 = this.txtAddress2.Text,
                    PASSPORT_REGION = this.txtNATION.Text,
                    PASSPORT_REGION2 = this.txtNATION2.Text,
                    POSTAL_CODE = this.txtPOSTAL_CODE.Text,
                    POSTAL_CODE2 = this.txtPOSTAL_CODE2.Text,
                    ORG_CODE = Global.ORG_CODE
                };

                if (this.rdoMan.Checked) data.SEX = "M";
                if (this.rdoWoman.Checked) data.SEX = "F";
                if (this.rdoMan2.Checked) data.SEX2 = "M";
                if (this.rdoWoman2.Checked) data.SEX2 = "F";
                if (this.dteBirthday.Text.Trim() != string.Empty && this.dteBirthday.Text.IsDate())
                {
                    data.DATE_OF_BIRTH = DateTime.Parse(this.dteBirthday.Text);
                }
                if (this.dteBirthday2.Text.Trim() != string.Empty && this.dteBirthday2.Text.IsDate())
                {
                    data.DATE_OF_BIRTH2 = DateTime.Parse(this.dteBirthday2.Text);
                }

                if (this.chkMAIL_TYPE_MONTHLY1.Checked) data.MAIL_TYPE_MONTHLY = this.chkMAIL_TYPE_MONTHLY1.Text;
                if (this.chkMAIL_TYPE_MONTHLY2.Checked) data.MAIL_TYPE_MONTHLY = this.chkMAIL_TYPE_MONTHLY2.Text;
                if (this.chkMAIL_TYPE_END1.Checked) data.MAIL_TYPE_END = this.chkMAIL_TYPE_END1.Text;
                if (this.chkMAIL_TYPE_END2.Checked) data.MAIL_TYPE_END = this.chkMAIL_TYPE_END2.Text;
                if (this.chkMAIL_TYPE_END3.Checked) data.MAIL_TYPE_END = this.txtMAIL_TYPE_END.Text;
                if (this.rdoWebDepositType_Null.Checked) data.IS_ONLINE_WITHDRAWAL = string.Empty;
                if (this.rdoWebDepositType_Y.Checked) data.IS_ONLINE_WITHDRAWAL = "Y";
                return data;
            }
        }

        private BL_CUST_BANK GetSaveData_Bank
        {
            get
            {
                var data = new BL_CUST_BANK()
                {
                    BANK_CNAME = this.txtBANK_CNAME.Text,
                    BANK_ENAME = this.txtBANK_ENAME.Text,
                    BRANCH_CNAME = this.txtBRANCH_CNAME.Text,
                    BRANCH_ENAME = this.txtBRANCH_ENAME.Text,
                    ACCOUNT_CNAME = this.txtACCOUNT_CNAME.Text,
                    ACCOUNT_ENAME = this.txtACCOUNT_ENAME.Text,
                    ACCOUNT = this.txtACCOUNT.Text,
                    BANK_C_ADDRESS = this.txtBANK_C_ADDRESS.Text,
                    BANK_E_ADDRESS = this.txtBANK_E_ADDRESS.Text,
                    SWIFT_CODE = this.txtSWIFT_CODE.Text,

                    BANK_CNAME2 = this.txtBANK_CNAME2.Text,
                    BANK_ENAME2 = this.txtBANK_ENAME2.Text,
                    BRANCH_CNAME2 = this.txtBRANCH_CNAME2.Text,
                    BRANCH_ENAME2 = this.txtBRANCH_ENAME2.Text,
                    ACCOUNT_CNAME2 = this.txtACCOUNT_CNAME2.Text,
                    ACCOUNT_ENAME2 = this.txtACCOUNT_ENAME2.Text,
                    ACCOUNT2 = this.txtACCOUNT2.Text,
                    BANK_C_ADDRESS2 = this.txtBANK_C_ADDRESS2.Text,
                    BANK_E_ADDRESS2 = this.txtBANK_E_ADDRESS2.Text,
                    SWIFT_CODE2 = this.txtSWIFT_CODE2.Text,
                    CURRENCY = "USD",
                    CREATED_BY = Global.str_UserName,
                    LAST_UPDATED_BY = Global.str_UserName
                };

                if (this.txtBANK_CNAME.Tag.ConvertToString().IsNumeric())
                {
                    data.BANK_ID = decimal.Parse(this.txtBANK_CNAME.Tag.ConvertToString());
                }
                return data;
            }
        }

        private BL_CUST_BANK GetSaveData_Bank2
        {
            get
            {
                var data = new BL_CUST_BANK()
                {
                    BANK_CNAME = this.txtBANK_CNAME_2.Text,
                    BANK_ENAME = this.txtBANK_ENAME_2.Text,
                    BRANCH_CNAME = this.txtBRANCH_CNAME_2.Text,
                    BRANCH_ENAME = this.txtBRANCH_ENAME_2.Text,
                    ACCOUNT_CNAME = this.txtACCOUNT_CNAME_2.Text,
                    ACCOUNT_ENAME = this.txtACCOUNT_ENAME_2.Text,
                    ACCOUNT = this.txtACCOUNT_2.Text,
                    BANK_C_ADDRESS = this.txtBANK_C_ADDRESS_2.Text,
                    BANK_E_ADDRESS = this.txtBANK_E_ADDRESS_2.Text,
                    SWIFT_CODE = this.txtSWIFT_CODE_2.Text,

                    BANK_CNAME2 = this.txtBANK_CNAME2_2.Text,
                    BANK_ENAME2 = this.txtBANK_ENAME2_2.Text,
                    BRANCH_CNAME2 = this.txtBRANCH_CNAME2_2.Text,
                    BRANCH_ENAME2 = this.txtBRANCH_ENAME2_2.Text,
                    ACCOUNT_CNAME2 = this.txtACCOUNT_CNAME2_2.Text,
                    ACCOUNT_ENAME2 = this.txtACCOUNT_ENAME2_2.Text,
                    ACCOUNT2 = this.txtACCOUNT2_2.Text,
                    BANK_C_ADDRESS2 = this.txtBANK_C_ADDRESS2_2.Text,
                    BANK_E_ADDRESS2 = this.txtBANK_E_ADDRESS2_2.Text,
                    SWIFT_CODE2 = this.txtSWIFT_CODE2_2.Text,
                    CURRENCY = "NTD",
                    CREATED_BY = Global.str_UserName,
                    LAST_UPDATED_BY = Global.str_UserName
                };

                if (this.txtBANK_CNAME_2.Tag.ConvertToString().IsNumeric())
                {
                    data.BANK_ID = decimal.Parse(this.txtBANK_CNAME_2.Tag.ConvertToString());
                }
                return data;
            }
        }

        private BL_CUST_BANK GetSaveData_Bank3
        {
            get
            {
                var data = new BL_CUST_BANK()
                {
                    BANK_CNAME = this.txtBANK_CNAME_3.Text,
                    BANK_ENAME = this.txtBANK_ENAME_3.Text,
                    BRANCH_CNAME = this.txtBRANCH_CNAME_3.Text,
                    BRANCH_ENAME = this.txtBRANCH_ENAME_3.Text,
                    ACCOUNT_CNAME = this.txtACCOUNT_CNAME_3.Text,
                    ACCOUNT_ENAME = this.txtACCOUNT_ENAME_3.Text,
                    ACCOUNT = this.txtACCOUNT_3.Text,
                    BANK_C_ADDRESS = this.txtBANK_C_ADDRESS_3.Text,
                    BANK_E_ADDRESS = this.txtBANK_E_ADDRESS_3.Text,
                    SWIFT_CODE = this.txtSWIFT_CODE_3.Text,

                    BANK_CNAME2 = this.txtBANK_CNAME2_3.Text,
                    BANK_ENAME2 = this.txtBANK_ENAME2_3.Text,
                    BRANCH_CNAME2 = this.txtBRANCH_CNAME2_3.Text,
                    BRANCH_ENAME2 = this.txtBRANCH_ENAME2_3.Text,
                    ACCOUNT_CNAME2 = this.txtACCOUNT_CNAME2_3.Text,
                    ACCOUNT_ENAME2 = this.txtACCOUNT_ENAME2_3.Text,
                    ACCOUNT2 = this.txtACCOUNT2_3.Text,
                    BANK_C_ADDRESS2 = this.txtBANK_C_ADDRESS2_3.Text,
                    BANK_E_ADDRESS2 = this.txtBANK_E_ADDRESS2_3.Text,
                    SWIFT_CODE2 = this.txtSWIFT_CODE2_3.Text,
                    CURRENCY = "RMB",
                    CREATED_BY = Global.str_UserName,
                    LAST_UPDATED_BY = Global.str_UserName,
                    ACCOUNT_TYPE = "OVERSEA"
                };

                if (this.txtBANK_CNAME_3.Tag.ConvertToString().IsNumeric())
                {
                    data.BANK_ID = decimal.Parse(this.txtBANK_CNAME_3.Tag.ConvertToString());
                }
                return data;
            }
        }

        private BL_CUST_BANK GetSaveData_Bank3_2
        {
            get
            {
                var data = new BL_CUST_BANK()
                {
                    BANK_CNAME = this.txtBANK_CNAME_3_2.Text,
                    BANK_ENAME = this.txtBANK_ENAME_3_2.Text,
                    BRANCH_CNAME = this.txtBRANCH_CNAME_3_2.Text,
                    BRANCH_ENAME = this.txtBRANCH_ENAME_3_2.Text,
                    ACCOUNT_CNAME = this.txtACCOUNT_CNAME_3_2.Text,
                    ACCOUNT_ENAME = this.txtACCOUNT_ENAME_3_2.Text,
                    ACCOUNT = this.txtACCOUNT_3_2.Text,
                    BANK_C_ADDRESS = this.txtBANK_C_ADDRESS_3_2.Text,
                    BANK_E_ADDRESS = this.txtBANK_E_ADDRESS_3_2.Text,
                    SWIFT_CODE = this.txtSWIFT_CODE_3_2.Text,

                    BANK_CNAME2 = this.txtBANK_CNAME2_3_2.Text,
                    BANK_ENAME2 = this.txtBANK_ENAME2_3_2.Text,
                    BRANCH_CNAME2 = this.txtBRANCH_CNAME2_3_2.Text,
                    BRANCH_ENAME2 = this.txtBRANCH_ENAME2_3_2.Text,
                    ACCOUNT_CNAME2 = this.txtACCOUNT_CNAME2_3_2.Text,
                    ACCOUNT_ENAME2 = this.txtACCOUNT_ENAME2_3_2.Text,
                    ACCOUNT2 = this.txtACCOUNT2_3_2.Text,
                    BANK_C_ADDRESS2 = this.txtBANK_C_ADDRESS2_3_2.Text,
                    BANK_E_ADDRESS2 = this.txtBANK_E_ADDRESS2_3_2.Text,
                    SWIFT_CODE2 = this.txtSWIFT_CODE2_3_2.Text,
                    CURRENCY = "RMB",
                    CREATED_BY = Global.str_UserName,
                    LAST_UPDATED_BY = Global.str_UserName,
                    ACCOUNT_TYPE = "DOMESTIC"
                };

                if (this.txtBANK_CNAME_3_2.Tag.ConvertToString().IsNumeric())
                {
                    data.BANK_ID = decimal.Parse(this.txtBANK_CNAME_3_2.Tag.ConvertToString());
                }
                return data;
            }
        }

        private BL_CUST_BANK GetSaveData_Bank4
        {
            get
            {
                var data = new BL_CUST_BANK()
                {
                    BANK_CNAME = this.txtBANK_CNAME_4.Text,
                    BANK_ENAME = this.txtBANK_ENAME_4.Text,
                    BRANCH_CNAME = this.txtBRANCH_CNAME_4.Text,
                    BRANCH_ENAME = this.txtBRANCH_ENAME_4.Text,
                    ACCOUNT_CNAME = this.txtACCOUNT_CNAME_4.Text,
                    ACCOUNT_ENAME = this.txtACCOUNT_ENAME_4.Text,
                    ACCOUNT = this.txtACCOUNT_4.Text,
                    BANK_C_ADDRESS = this.txtBANK_C_ADDRESS_4.Text,
                    BANK_E_ADDRESS = this.txtBANK_E_ADDRESS_4.Text,
                    SWIFT_CODE = this.txtSWIFT_CODE_4.Text,

                    BANK_CNAME2 = this.txtBANK_CNAME2_4.Text,
                    BANK_ENAME2 = this.txtBANK_ENAME2_4.Text,
                    BRANCH_CNAME2 = this.txtBRANCH_CNAME2_4.Text,
                    BRANCH_ENAME2 = this.txtBRANCH_ENAME2_4.Text,
                    ACCOUNT_CNAME2 = this.txtACCOUNT_CNAME2_4.Text,
                    ACCOUNT_ENAME2 = this.txtACCOUNT_ENAME2_4.Text,
                    ACCOUNT2 = this.txtACCOUNT2_4.Text,
                    BANK_C_ADDRESS2 = this.txtBANK_C_ADDRESS2_4.Text,
                    BANK_E_ADDRESS2 = this.txtBANK_E_ADDRESS2_4.Text,
                    SWIFT_CODE2 = this.txtSWIFT_CODE2_4.Text,
                    CURRENCY = "EUR",
                    CREATED_BY = Global.str_UserName,
                    LAST_UPDATED_BY = Global.str_UserName
                };

                if (this.txtBANK_CNAME_4.Tag.ConvertToString().IsNumeric())
                {
                    data.BANK_ID = decimal.Parse(this.txtBANK_CNAME_4.Tag.ConvertToString());
                }
                return data;
            }
        }
        
        private BL_CUST_BANK GetSaveData_Bank5
        {
            get
            {
                var data = new BL_CUST_BANK()
                {
                    BANK_CNAME = this.txtBANK_CNAME_5.Text,
                    BANK_ENAME = this.txtBANK_ENAME_5.Text,
                    BRANCH_CNAME = this.txtBRANCH_CNAME_5.Text,
                    BRANCH_ENAME = this.txtBRANCH_ENAME_5.Text,
                    ACCOUNT_CNAME = this.txtACCOUNT_CNAME_5.Text,
                    ACCOUNT_ENAME = this.txtACCOUNT_ENAME_5.Text,
                    ACCOUNT = this.txtACCOUNT_5.Text,
                    BANK_C_ADDRESS = this.txtBANK_C_ADDRESS_5.Text,
                    BANK_E_ADDRESS = this.txtBANK_E_ADDRESS_5.Text,
                    SWIFT_CODE = this.txtSWIFT_CODE_5.Text,

                    BANK_CNAME2 = this.txtBANK_CNAME2_5.Text,
                    BANK_ENAME2 = this.txtBANK_ENAME2_5.Text,
                    BRANCH_CNAME2 = this.txtBRANCH_CNAME2_5.Text,
                    BRANCH_ENAME2 = this.txtBRANCH_ENAME2_5.Text,
                    ACCOUNT_CNAME2 = this.txtACCOUNT_CNAME2_5.Text,
                    ACCOUNT_ENAME2 = this.txtACCOUNT_ENAME2_5.Text,
                    ACCOUNT2 = this.txtACCOUNT2_5.Text,
                    BANK_C_ADDRESS2 = this.txtBANK_C_ADDRESS2_5.Text,
                    BANK_E_ADDRESS2 = this.txtBANK_E_ADDRESS2_5.Text,
                    SWIFT_CODE2 = this.txtSWIFT_CODE2_5.Text,
                    CURRENCY = "AUD",
                    CREATED_BY = Global.str_UserName,
                    LAST_UPDATED_BY = Global.str_UserName
                };

                if (this.txtBANK_CNAME_5.Tag.ConvertToString().IsNumeric())
                {
                    data.BANK_ID = decimal.Parse(this.txtBANK_CNAME_5.Tag.ConvertToString());
                }
                return data;
            }
        }

        private BL_CUST_BANK GetSaveData_Bank6
        {
            get
            {
                var data = new BL_CUST_BANK()
                {
                    BANK_CNAME = this.txtBANK_CNAME_6.Text,
                    BANK_ENAME = this.txtBANK_ENAME_6.Text,
                    BRANCH_CNAME = this.txtBRANCH_CNAME_6.Text,
                    BRANCH_ENAME = this.txtBRANCH_ENAME_6.Text,
                    ACCOUNT_CNAME = this.txtACCOUNT_CNAME_6.Text,
                    ACCOUNT_ENAME = this.txtACCOUNT_ENAME_6.Text,
                    ACCOUNT = this.txtACCOUNT_6.Text,
                    BANK_C_ADDRESS = this.txtBANK_C_ADDRESS_6.Text,
                    BANK_E_ADDRESS = this.txtBANK_E_ADDRESS_6.Text,
                    SWIFT_CODE = this.txtSWIFT_CODE_6.Text,

                    BANK_CNAME2 = this.txtBANK_CNAME2_6.Text,
                    BANK_ENAME2 = this.txtBANK_ENAME2_6.Text,
                    BRANCH_CNAME2 = this.txtBRANCH_CNAME2_6.Text,
                    BRANCH_ENAME2 = this.txtBRANCH_ENAME2_6.Text,
                    ACCOUNT_CNAME2 = this.txtACCOUNT_CNAME2_6.Text,
                    ACCOUNT_ENAME2 = this.txtACCOUNT_ENAME2_6.Text,
                    ACCOUNT2 = this.txtACCOUNT2_6.Text,
                    BANK_C_ADDRESS2 = this.txtBANK_C_ADDRESS2_6.Text,
                    BANK_E_ADDRESS2 = this.txtBANK_E_ADDRESS2_6.Text,
                    SWIFT_CODE2 = this.txtSWIFT_CODE2_6.Text,
                    CURRENCY = "JPY",
                    CREATED_BY = Global.str_UserName,
                    LAST_UPDATED_BY = Global.str_UserName
                };

                if (this.txtBANK_CNAME_6.Tag.ConvertToString().IsNumeric())
                {
                    data.BANK_ID = decimal.Parse(this.txtBANK_CNAME_6.Tag.ConvertToString());
                }
                return data;
            }
        }

        private BL_CUST_BANK GetSaveData_Bank7
        {
            get
            {
                var data = new BL_CUST_BANK()
                {
                    BANK_CNAME = this.txtBANK_CNAME_7.Text,
                    BANK_ENAME = this.txtBANK_ENAME_7.Text,
                    BRANCH_CNAME = this.txtBRANCH_CNAME_7.Text,
                    BRANCH_ENAME = this.txtBRANCH_ENAME_7.Text,
                    ACCOUNT_CNAME = this.txtACCOUNT_CNAME_7.Text,
                    ACCOUNT_ENAME = this.txtACCOUNT_ENAME_7.Text,
                    ACCOUNT = this.txtACCOUNT_7.Text,
                    BANK_C_ADDRESS = this.txtBANK_C_ADDRESS_7.Text,
                    BANK_E_ADDRESS = this.txtBANK_E_ADDRESS_7.Text,
                    SWIFT_CODE = this.txtSWIFT_CODE_7.Text,

                    BANK_CNAME2 = this.txtBANK_CNAME2_7.Text,
                    BANK_ENAME2 = this.txtBANK_ENAME2_7.Text,
                    BRANCH_CNAME2 = this.txtBRANCH_CNAME2_7.Text,
                    BRANCH_ENAME2 = this.txtBRANCH_ENAME2_7.Text,
                    ACCOUNT_CNAME2 = this.txtACCOUNT_CNAME2_7.Text,
                    ACCOUNT_ENAME2 = this.txtACCOUNT_ENAME2_7.Text,
                    ACCOUNT2 = this.txtACCOUNT2_7.Text,
                    BANK_C_ADDRESS2 = this.txtBANK_C_ADDRESS2_7.Text,
                    BANK_E_ADDRESS2 = this.txtBANK_E_ADDRESS2_7.Text,
                    SWIFT_CODE2 = this.txtSWIFT_CODE2_7.Text,
                    CURRENCY = "NZD",
                    CREATED_BY = Global.str_UserName,
                    LAST_UPDATED_BY = Global.str_UserName
                };

                if (this.txtBANK_CNAME_7.Tag.ConvertToString().IsNumeric())
                {
                    data.BANK_ID = decimal.Parse(this.txtBANK_CNAME_7.Tag.ConvertToString());
                }
                return data;
            }
        }

        private BL_ORDER GetSaveData_Order
        {
            get
            {
                decimal dbTry;
                var data = new BL_ORDER();
                //data.ORDER_NO = decimal.Parse(this.txtOrderNo.Text.Trim());
                data.ORDER_NO = this.txtOrderNo.Text.Trim();
                if (decimal.TryParse(this.txtAmount_USD.Text, out dbTry)) data.AMOUNT_USD = dbTry;
                if (decimal.TryParse(this.txtAmount_NTD.Text, out dbTry)) data.AMOUNT_NTD = dbTry;
                if (decimal.TryParse(this.txtAmount_RMB.Text, out dbTry)) data.AMOUNT_RMB = dbTry;
                if (decimal.TryParse(this.txtAmount_EUR.Text, out dbTry)) data.AMOUNT_EUR = dbTry;
                if (decimal.TryParse(this.txtAmount_AUD.Text, out dbTry)) data.AMOUNT_AUD = dbTry;
                if (decimal.TryParse(this.txtAmount_JPY.Text, out dbTry)) data.AMOUNT_JPY = dbTry;
                if (decimal.TryParse(this.txtAmount_NZD.Text, out dbTry)) data.AMOUNT_NZD = dbTry;
                data.PARENT_ORDER_NO = this.txtParentOrderNo.Text;
                if (decimal.TryParse(this.txtAmount_USD_OLD.Text, out dbTry)) data.OLD_AMOUNT_USD = dbTry;
                if (decimal.TryParse(this.txtAmount_NTD_OLD.Text, out dbTry)) data.OLD_AMOUNT_NTD = dbTry;
                if (decimal.TryParse(this.txtAmount_RMB_OLD.Text, out dbTry)) data.OLD_AMOUNT_RMB = dbTry;
                if (decimal.TryParse(this.txtAmount_EUR_OLD.Text, out dbTry)) data.OLD_AMOUNT_EUR = dbTry;
                if (decimal.TryParse(this.txtAmount_AUD_OLD.Text, out dbTry)) data.OLD_AMOUNT_AUD = dbTry;
                if (decimal.TryParse(this.txtAmount_JPY_OLD.Text, out dbTry)) data.OLD_AMOUNT_JPY = dbTry;
                if (decimal.TryParse(this.txtAmount_NZD_OLD.Text, out dbTry)) data.OLD_AMOUNT_NZD = dbTry;

                if (decimal.TryParse(this.txtInterest_USD.Text, out dbTry)) data.INTEREST_USD = dbTry;
                if (decimal.TryParse(this.txtInterest_NTD.Text, out dbTry)) data.INTEREST_NTD = dbTry;
                if (decimal.TryParse(this.txtInterest_RMB.Text, out dbTry)) data.INTEREST_RMB = dbTry;
                if (decimal.TryParse(this.txtInterest_EUR.Text, out dbTry)) data.INTEREST_EUR = dbTry;
                if (decimal.TryParse(this.txtInterest_AUD.Text, out dbTry)) data.INTEREST_AUD = dbTry;
                if (decimal.TryParse(this.txtInterest_JPY.Text, out dbTry)) data.INTEREST_JPY = dbTry;
                if (decimal.TryParse(this.txtInterest_NZD.Text, out dbTry)) data.INTEREST_NZD = dbTry;

                if (this.dteFromDate.Text.IsDate()) data.FROM_DATE = DateTime.Parse(this.dteFromDate.Text);
                if (this.dteEndDate.Text.IsDate()) data.END_DATE = DateTime.Parse(this.dteEndDate.Text);
                if (decimal.TryParse(this.txtYear.Text, out dbTry)) data.YEAR = dbTry;
                if (this.dteExtendDate.Text.IsDate()) data.EXTEND_DATE = DateTime.Parse(this.dteExtendDate.Text);
                if (this.dteOrderEnd.Text.IsDate()) data.ORDER_END = DateTime.Parse(this.dteOrderEnd.Text);
                data.MT4_ACCOUNT = this.txtMT4.Text;
                data.NOTE = this.txtNote.Text;
                data.EMP_CODE = this.txtSales.Tag.ConvertToString();
                data.INTERNAL_IB_CODE = this.txtIBCode_Internal.Text.ConvertToString();
                data.IB_CODE = this.txtIBCode.Text;

                var currency = string.Empty;
                if (data.AMOUNT_USD > 0)
                {
                    currency = "USD";
                }
                if (data.AMOUNT_NTD > 0)
                {
                    currency = "NTD";
                }
                if (data.AMOUNT_RMB > 0)
                {
                    currency = "RMB";
                }
                if (data.AMOUNT_EUR > 0)
                {
                    currency = "EUR";
                }
                if (data.AMOUNT_AUD > 0)
                {
                    currency = "AUD";
                }
                if (data.AMOUNT_JPY > 0)
                {
                    currency = "JPY";
                }
                if (data.AMOUNT_NZD > 0)
                {
                    currency = "NZD";
                }
                decimal? E_AMOUNT  = 0;
                if (data.AMOUNT_USD > 0)
                {
                    E_AMOUNT = data.AMOUNT_USD;
                }
                if (data.AMOUNT_NTD > 0)
                {
                    E_AMOUNT = data.AMOUNT_NTD;
                }
                if (data.AMOUNT_RMB > 0)
                {
                    E_AMOUNT = data.AMOUNT_RMB;
                }
                if (data.AMOUNT_EUR> 0)
                {
                    E_AMOUNT = data.AMOUNT_EUR;
                }
                if (data.AMOUNT_AUD > 0)
                {
                    E_AMOUNT = data.AMOUNT_AUD;
                }
                if (data.AMOUNT_JPY > 0)
                {
                    E_AMOUNT = data.AMOUNT_JPY;
                }
                if (data.AMOUNT_NZD > 0)
                {
                    E_AMOUNT = data.AMOUNT_NZD;
                }

                data.ENTER_CURRENCY = currency;
                data.CURRENCY = currency;
                data.ENTER_AMOUNT = E_AMOUNT;
                data.AMOUNT = E_AMOUNT;
                data.ATTRIBUTE1 = txtSales.Text;

                if (this.checkBox1.Checked)
                {
                    data.PROJECT = "2019金豬報喜";
                }
                if (decimal.TryParse(this.txtInterestedRate.Text, out dbTry))
                {
                    data.INTEREST_RATE = dbTry;
                }

                if (currency == "RMB" && this.checkBox2.Checked)
                {
                    data.WITHDRAWAL_MODE = "AUTO";
                }
                data.MULTI_ORDER_FLAG = this.chkIsMultiContract.Checked ? "Y" : string.Empty;
                //data.IS_REMIT_TO_AUS = this.chkIS_REMIT_TO_AUS.Checked ? "Y" : string.Empty;
                switch (this.cboIS_REMIT_TO_AUS.SelectedIndex)
                {
                    case 1:
                        data.IS_REMIT_TO_AUS = "N";
                        break;
                    case 2:
                        data.IS_REMIT_TO_AUS = "E";
                        break;
                    default:
                        data.IS_REMIT_TO_AUS = string.Empty;
                        break;
                }
                if (this.chk_ind_order.Checked)
                {
                    data.ORG_CODE = "IND";
                    if (this.txtDiscount.Text.IsNumeric())
                    {
                        data.DISCOUNT_AMOUNT = decimal.Parse(this.txtDiscount.Text);
                    }
                    else
                    {
                        data.DISCOUNT_AMOUNT = (decimal?)null;
                    }
                }
                else
                {
                    data.ORG_CODE = Global.ORG_CODE;
                    data.DISCOUNT_AMOUNT = (decimal?)null;
                }
                if (this.rdoBonusA.Checked)
                {
                    data.BONUS_OPTION = "A";
                }
                else if (this.rdoBonusB.Checked)
                {
                    data.BONUS_OPTION = "B";
                }
                return data;
            }
        }

        private bool DataValidation()
        {
            var msg = string.Empty;
            var result = true;


            #region Customer
            if (this.txtCUST_CNAME.Text.Trim() == string.Empty)
            {
                msg += "請輸入「姓名」" + Environment.NewLine;
            }
            if (this.txtCUST_ENAME.Text.Trim() == string.Empty)
            {
                msg += "請輸入「姓名(英)」" + Environment.NewLine;
            }
            if (rdoMan.Checked == false && this.rdoWoman.Checked == false)
            {
                msg += "請選擇「性別」" + Environment.NewLine;
            }
            if (this.dteBirthday.Text.Trim().IsDate() == false)
            {
                msg += "請輸入「出生年月日」" + Environment.NewLine;
            }
            if (this.txtID_NUMBER.Text.Trim() == string.Empty)
            {
                msg += "請輸入「身分證號碼」" + Environment.NewLine;
            }
            else
            {
                //  檢查此ID是否已經存在
                var dataChk = base.Cust.GetCustDataByID_Number(this.txtID_NUMBER.Text.Trim(), this.txtID_NUMBER2.Text.Trim());
                if (dataChk.Count > 0)
                {
                    var dataCurrent = dataChk.Where(x => x.CUST_ID.ConvertToString() != this.strCustID).ToList();
                    if (dataCurrent.Count > 0)
                    {
                        msg += "此客戶已經存在" + Environment.NewLine;
                    }

                }
            }
            if (this.txtNATION.Text.Trim() == string.Empty)
            {
                msg += "請輸入「國籍」" + Environment.NewLine;
            }
            if (this.txtPHONE.Text.Trim() == string.Empty)
            {
                msg += "請輸入「聯絡電話」" + Environment.NewLine;
            }
            if (this.txtEMAIL.Text.Trim() == string.Empty)
            {
                msg += "請輸入「電子郵件信箱」" + Environment.NewLine;
            }
            if (this.txtPOSTAL_CODE.Text.Trim() == string.Empty)
            {
                msg += "請輸入「郵遞區號」" + Environment.NewLine;
            }
            if (this.txtAddress.Text.Trim() == string.Empty)
            {
                msg += "請輸入「居住地址」" + Environment.NewLine;
            }

            if (this.chkMAIL_TYPE_MONTHLY1.Checked == false && this.chkMAIL_TYPE_MONTHLY2.Checked == false)
            {
                msg += "請選擇「合約」送達方式" + Environment.NewLine;
            }
            if (this.chkMAIL_TYPE_END1.Checked == false && this.chkMAIL_TYPE_END2.Checked == false && this.chkMAIL_TYPE_END3.Checked == false)
            {
                msg += "請選擇「月對帳單」取得方式" + Environment.NewLine;
            }
            if (this.chkMAIL_TYPE_END3.Checked && this.txtMAIL_TYPE_END.Text.Trim() == string.Empty)
            {
                msg += "請輸入「月對帳單」取得方式" + Environment.NewLine;
            }


            #endregion

            #region Bank

            var check_count = 0;
            var msg_bank = string.Empty;

            if (this.txtBANK_CNAME.Text.Trim() == string.Empty)
            {
                msg_bank += "請輸入「銀行名稱」" + Environment.NewLine;
            }
            if (this.txtBRANCH_CNAME.Text.Trim() == string.Empty)
            {
                msg_bank += "請輸入「分行名稱」" + Environment.NewLine;
            }
            if (this.txtACCOUNT_CNAME.Text.Trim() == string.Empty)
            {
                msg_bank += "請輸入「帳戶持有人姓名」" + Environment.NewLine;
            }
            if (this.txtACCOUNT.Text.Trim() == string.Empty)
            {
                msg_bank += "請輸入「銀行帳戶」" + Environment.NewLine;
            }
            if (this.txtSWIFT_CODE.Text.Trim() == string.Empty)
            {
                msg_bank += "請輸入「國際匯款代碼」" + Environment.NewLine;
            }
            if (this.txtBANK_C_ADDRESS.Text.Trim() == string.Empty)
            {
                msg_bank += "請輸入「銀行地址」" + Environment.NewLine;
            }

            if (msg_bank.Length > 0)
            {
                check_count = check_count + 1;
            }


            var msg_bank2 = string.Empty;

            if (this.txtBANK_CNAME_2.Text.Trim() == string.Empty)
            {
                msg_bank2 += "請輸入「銀行名稱」" + Environment.NewLine;
            }
            if (this.txtBRANCH_CNAME_2.Text.Trim() == string.Empty)
            {
                msg_bank2 += "請輸入「分行名稱」" + Environment.NewLine;
            }
            if (this.txtACCOUNT_CNAME_2.Text.Trim() == string.Empty)
            {
                msg_bank2 += "請輸入「帳戶持有人姓名」" + Environment.NewLine;
            }
            if (this.txtACCOUNT_2.Text.Trim() == string.Empty)
            {
                msg_bank2 += "請輸入「銀行帳戶」" + Environment.NewLine;
            }
            if (this.txtSWIFT_CODE_2.Text.Trim() == string.Empty)
            {
                msg_bank2 += "請輸入「國際匯款代碼」" + Environment.NewLine;
            }
            if (this.txtBANK_C_ADDRESS_2.Text.Trim() == string.Empty)
            {
                msg_bank2 += "請輸入「銀行地址」" + Environment.NewLine;
            }

            if (msg_bank2.Length > 0)
            {
                check_count = check_count + 1;
            }

            var msg_bank3 = string.Empty;

            if (this.txtBANK_CNAME_3.Text.Trim() == string.Empty)
            {
                msg_bank3 += "請輸入「銀行名稱」" + Environment.NewLine;
            }
            if (this.txtBRANCH_CNAME_3.Text.Trim() == string.Empty)
            {
                msg_bank3 += "請輸入「分行名稱」" + Environment.NewLine;
            }
            if (this.txtACCOUNT_CNAME_3.Text.Trim() == string.Empty)
            {
                msg_bank3 += "請輸入「帳戶持有人姓名」" + Environment.NewLine;
            }
            if (this.txtACCOUNT_3.Text.Trim() == string.Empty)
            {
                msg_bank3 += "請輸入「銀行帳戶」" + Environment.NewLine;
            }
            if (this.txtSWIFT_CODE_3.Text.Trim() == string.Empty)
            {
                msg_bank3 += "請輸入「國際匯款代碼」" + Environment.NewLine;
            }
            if (this.txtBANK_C_ADDRESS_3.Text.Trim() == string.Empty)
            {
                msg_bank3 += "請輸入「銀行地址」" + Environment.NewLine;
            }

            if (msg_bank3.Length > 0)
            {
                check_count = check_count + 1;
            }


            var msg_bank3_2 = string.Empty;

            if (this.txtBANK_CNAME_3_2.Text.Trim() == string.Empty)
            {
                msg_bank3_2 += "請輸入「銀行名稱」" + Environment.NewLine;
            }
            if (this.txtBRANCH_CNAME_3_2.Text.Trim() == string.Empty)
            {
                msg_bank3_2 += "請輸入「分行名稱」" + Environment.NewLine;
            }
            if (this.txtACCOUNT_CNAME_3_2.Text.Trim() == string.Empty)
            {
                msg_bank3_2 += "請輸入「帳戶持有人姓名」" + Environment.NewLine;
            }
            if (this.txtACCOUNT_3_2.Text.Trim() == string.Empty)
            {
                msg_bank3_2 += "請輸入「銀行帳戶」" + Environment.NewLine;
            }
            if (this.txtSWIFT_CODE_3_2.Text.Trim() == string.Empty)
            {
                msg_bank3_2 += "請輸入「國際匯款代碼」" + Environment.NewLine;
            }
            if (this.txtBANK_C_ADDRESS_3_2.Text.Trim() == string.Empty)
            {
                msg_bank3_2 += "請輸入「銀行地址」" + Environment.NewLine;
            }

            if (msg_bank3_2.Length > 0)
            {
                check_count = check_count + 1;
            }

            var msg_bank4 = string.Empty;

            if (this.txtBANK_CNAME_4.Text.Trim() == string.Empty)
            {
                msg_bank4 += "請輸入「銀行名稱」" + Environment.NewLine;
            }
            if (this.txtBRANCH_CNAME_4.Text.Trim() == string.Empty)
            {
                msg_bank4 += "請輸入「分行名稱」" + Environment.NewLine;
            }
            if (this.txtACCOUNT_CNAME_4.Text.Trim() == string.Empty)
            {
                msg_bank4 += "請輸入「帳戶持有人姓名」" + Environment.NewLine;
            }
            if (this.txtACCOUNT_4.Text.Trim() == string.Empty)
            {
                msg_bank4 += "請輸入「銀行帳戶」" + Environment.NewLine;
            }
            if (this.txtSWIFT_CODE_4.Text.Trim() == string.Empty)
            {
                msg_bank4 += "請輸入「國際匯款代碼」" + Environment.NewLine;
            }
            if (this.txtBANK_C_ADDRESS_4.Text.Trim() == string.Empty)
            {
                msg_bank4 += "請輸入「銀行地址」" + Environment.NewLine;
            }

            if (msg_bank4.Length > 0)
            {
                check_count = check_count + 1;
            }

            var msg_bank5 = string.Empty;

            if (this.txtBANK_CNAME_5.Text.Trim() == string.Empty)
            {
                msg_bank5 += "請輸入「銀行名稱」" + Environment.NewLine;
            }
            if (this.txtBRANCH_CNAME_5.Text.Trim() == string.Empty)
            {
                msg_bank5 += "請輸入「分行名稱」" + Environment.NewLine;
            }
            if (this.txtACCOUNT_CNAME_5.Text.Trim() == string.Empty)
            {
                msg_bank5 += "請輸入「帳戶持有人姓名」" + Environment.NewLine;
            }
            if (this.txtACCOUNT_5.Text.Trim() == string.Empty)
            {
                msg_bank5 += "請輸入「銀行帳戶」" + Environment.NewLine;
            }
            if (this.txtSWIFT_CODE_5.Text.Trim() == string.Empty)
            {
                msg_bank5 += "請輸入「國際匯款代碼」" + Environment.NewLine;
            }
            if (this.txtBANK_C_ADDRESS_5.Text.Trim() == string.Empty)
            {
                msg_bank5 += "請輸入「銀行地址」" + Environment.NewLine;
            }

            if (msg_bank5.Length > 0)
            {
                check_count = check_count + 1;
            }


            var msg_bank6 = string.Empty;

            if (this.txtBANK_CNAME_6.Text.Trim() == string.Empty)
            {
                msg_bank6 += "請輸入「銀行名稱」" + Environment.NewLine;
            }
            if (this.txtBRANCH_CNAME_6.Text.Trim() == string.Empty)
            {
                msg_bank6 += "請輸入「分行名稱」" + Environment.NewLine;
            }
            if (this.txtACCOUNT_CNAME_6.Text.Trim() == string.Empty)
            {
                msg_bank6 += "請輸入「帳戶持有人姓名」" + Environment.NewLine;
            }
            if (this.txtACCOUNT_6.Text.Trim() == string.Empty)
            {
                msg_bank6 += "請輸入「銀行帳戶」" + Environment.NewLine;
            }
            if (this.txtSWIFT_CODE_6.Text.Trim() == string.Empty)
            {
                msg_bank6 += "請輸入「國際匯款代碼」" + Environment.NewLine;
            }
            if (this.txtBANK_C_ADDRESS_6.Text.Trim() == string.Empty)
            {
                msg_bank6 += "請輸入「銀行地址」" + Environment.NewLine;
            }

            if (msg_bank6.Length > 0)
            {
                check_count = check_count + 1;
            }


            var msg_bank7 = string.Empty;

            if (this.txtBANK_CNAME_7.Text.Trim() == string.Empty)
            {
                msg_bank7 += "請輸入「銀行名稱」" + Environment.NewLine;
            }
            if (this.txtBRANCH_CNAME_7.Text.Trim() == string.Empty)
            {
                msg_bank7 += "請輸入「分行名稱」" + Environment.NewLine;
            }
            if (this.txtACCOUNT_CNAME_7.Text.Trim() == string.Empty)
            {
                msg_bank7 += "請輸入「帳戶持有人姓名」" + Environment.NewLine;
            }
            if (this.txtACCOUNT_7.Text.Trim() == string.Empty)
            {
                msg_bank7 += "請輸入「銀行帳戶」" + Environment.NewLine;
            }
            if (this.txtSWIFT_CODE_7.Text.Trim() == string.Empty)
            {
                msg_bank7 += "請輸入「國際匯款代碼」" + Environment.NewLine;
            }
            if (this.txtBANK_C_ADDRESS_7.Text.Trim() == string.Empty)
            {
                msg_bank7 += "請輸入「銀行地址」" + Environment.NewLine;
            }

            if (msg_bank7.Length > 0)
            {
                check_count = check_count + 1;
            }

            if (check_count == 8)
            {
                msg += "請至少輸入一個銀行帳戶資料" + Environment.NewLine;
            }
            
            #endregion


            if (msg != string.Empty)
            {
                MessageBox.Show(msg);
                result = false;
            }

            return result;
        }

        private bool DataValidation_Order()
        {
            var msg = string.Empty;
            var result = true;

            #region Order
            if (this.txtOrderNo.Text.Trim() == string.Empty)
            {
                msg += "請輸入「Order No」" + Environment.NewLine;
            }
            //else
            //{
            //    if (this.txtOrderNo.Text.Trim().IsNumeric() == false)
            //    {
            //        msg += "「Order No」請輸入數值" + Environment.NewLine;
            //    }
            //}

            if (this.txtAmount_NTD.Text.IsNumeric() == false &&
                this.txtAmount_RMB.Text.IsNumeric() == false &&
                this.txtAmount_USD.Text.IsNumeric() == false &&
                this.txtAmount_EUR.Text.IsNumeric() == false &&
                this.txtAmount_AUD.Text.IsNumeric() == false &&
                this.txtAmount_JPY.Text.IsNumeric() == false &&
                this.txtAmount_NZD.Text.IsNumeric() == false)
            {
                msg += "請輸入「Amount」" + Environment.NewLine;
            }

            if (this.dteFromDate.Text.IsDate() == false)
            {
                msg += "請輸入「From Date」" + Environment.NewLine;
            }
            if (this.dteEndDate.Text.IsDate() == false)
            {
                msg += "請輸入「End Date」" + Environment.NewLine;
            }
            //if (this.dteExtendDate.Text.IsDate() == false)
            //{
            //    msg += "請輸入「Extend Date」" + Environment.NewLine;
            //}
            //if (this.dteOrderEnd.Text.IsDate() == false)
            //{
            //    msg += "請輸入「Order End Date」" + Environment.NewLine;
            //}

            if (this.dteFromDate.Text.IsDate() && this.dteEndDate.Text.IsDate())
            {
                if (DateTime.Parse(this.dteFromDate.Text) > DateTime.Parse(this.dteEndDate.Text))
                {
                    msg += "「From Date」不可大於「End Date」" + Environment.NewLine;
                }
            }

            if (this.dteFromDate.Text.IsDate())
            {
                if (this.dteFromDate.Value >= new DateTime(2019, 1, 15) && this.dteFromDate.Value <= new DateTime(2019, 1, 31))
                {
                    //  Nothing
                }
            }
            else if (this.checkBox1.Checked)
            {
                this.checkBox1.Checked = false;
            }

            if (this.txtInterestedRate.Text.IsNumeric() == false)
            {
                msg += "「利率」請輸入數值" + Environment.NewLine;
            }

            //if (this.dteExtendDate.Text.IsDate() && this.dteOrderEnd.Text.IsDate())
            //{
            //    if (DateTime.Parse(this.dteExtendDate.Text) > DateTime.Parse(this.dteOrderEnd.Text))
            //    {
            //        msg += "「Extend Date」不可大於「Order End Date」" + Environment.NewLine;
            //    }
            //}

            if (this.txtYear.Text.Trim().IsNumeric() == false)
            {
                msg += "請輸入「Year」" + Environment.NewLine;
            }
            if (this.txtSales.Text.Trim() == string.Empty || this.txtIBCode.Text.Trim() == string.Empty)
            {
                msg += "請選擇「業務」" + Environment.NewLine;
            }

            #endregion

            if (msg != string.Empty)
            {
                MessageBox.Show(msg);
                result = false;
            }

            return result;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            var IsValid = false;
            if (this.strCustID != string.Empty && this.txtCUST_CNAME.Enabled == false)
            {
                //  不用驗證, 現有客戶
                IsValid = true;
            }
            else
            {
                IsValid = this.DataValidation();
            }

            if (IsValid)
            {
                if (this.DataValidation_Order())
                {

                    var dataCust = this.GetSaveData_Cust;
                    var dataBank = this.GetSaveData_Bank;
                    var dataBank2 = this.GetSaveData_Bank2;
                    var dataBank3 = this.GetSaveData_Bank3;
                    var dataBank3_2 = this.GetSaveData_Bank3_2;
                    var dataBank4 = this.GetSaveData_Bank4;
                    var dataBank5 = this.GetSaveData_Bank5;
                    var dataBank6 = this.GetSaveData_Bank6;
                    var dataBank7 = this.GetSaveData_Bank7;
                    var dataOrder = this.GetSaveData_Order;


                    var strResult = base.Cust.SaveNewContract(dataCust, dataBank, dataBank2, dataBank3, dataBank4, dataBank5, this.strCustID, dataOrder, Global.str_UserName, dataBank3_2, dataBank6, dataBank7);
                    if (strResult != string.Empty)
                    {
                        MessageBox.Show("儲存成功");
                        this.strCustID = strResult;
                        //this.InitBind();
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("儲存失敗");
                    }

                }
            }

        }

        private void dteBirthday_ValueChanged(object sender, EventArgs e)
        {
            this.dteBirthday.Format = DateTimePickerFormat.Long;
        }

        private void dteBirthday2_ValueChanged(object sender, EventArgs e)
        {
            this.dteBirthday2.Format = DateTimePickerFormat.Long;
        }

        private void btnBringParentOrder_Click(object sender, EventArgs e)
        {
            var frm = new frmContractDetail(this.strCustID, true) { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
            frm.ShowDialog();
            if (frm.status == MK.Demo.Data.enCommon.enFormStatus.OK)
            {
                this.txtParentOrderNo.Text = frm.strORDER_NO.ConvertToString();
                this.txtAmount_NTD_OLD.Text = frm.strAMOUNT_NTD.ConvertToString();
                this.txtAmount_RMB_OLD.Text = frm.strAMOUNT_RMB.ConvertToString();
                this.txtAmount_USD_OLD.Text = frm.strAMOUNT_USD.ConvertToString();
                this.txtAmount_EUR_OLD.Text = frm.strAMOUNT_EUR.ConvertToString();
                this.txtAmount_AUD_OLD.Text = frm.strAMOUNT_AUD.ConvertToString();
                this.txtAmount_JPY_OLD.Text = frm.strAMOUNT_JPY.ConvertToString();
                this.txtAmount_NZD_OLD.Text = frm.strAMOUNT_NZD.ConvertToString();
            }
            this.chkIsMultiContract.Visible = this.txtParentOrderNo.Text.Trim() != string.Empty;
         frm.Dispose();
            frm = null;
        }

        private void dte_ValueChanged(object sender, EventArgs e)
        {
            (sender as DateTimePicker).Format = DateTimePickerFormat.Long;

            if ((sender as DateTimePicker).Name == this.dteExtendDate.Name)
            {
                if ((sender as DateTimePicker).Text.Trim() != string.Empty)
                {
                    this.dteOrderEnd.Format = DateTimePickerFormat.Long;
                    this.dteOrderEnd.Value = (sender as DateTimePicker).Value;
                    this.dteOrderEnd.Text = (sender as DateTimePicker).Text;
                    this.cb_close.Checked = true;
                }
            }
        }

        private void btnBringSales_Click(object sender, EventArgs e)
        {
            var frm = new FrmSelectSales
                ( 
                    strORG_CODE: Global.ORG_CODE
                ) { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
            frm.ShowDialog();
            if (frm.status == MK.Demo.Data.enCommon.enFormStatus.OK)
            {
                this.txtIBCode.Text = frm.strIB_CODE.ConvertToString();
                this.txtSales.Text = frm.strCNAME.ConvertToString();
                this.txtSales.Tag = frm.strEMP_CODE.ConvertToString();
            }
            frm.Dispose();
            frm = null;

        }

        private void btnComfirmModify_Click(object sender, EventArgs e)
        {
            var strCustJson_Before = base.Cust.GetCustLogData_Json(this.strCustID);
            var strCustBankJson_Before = base.Cust.GetCustBankLogData_Json(this.strCustID);
            var strCustJson_After = string.Empty;
            var strCustBankJson_After = string.Empty;
            

            if (this.IsResend)
            {
                #region 審批退件-修改重送
                var IsValid = true;
                //IsValid = this.DataValidation();
                if (IsValid)
                {
                    //  修改重送只能異動Order資料
                    if (this.DataValidation_Order())
                    {

                        var dataCust = this.GetSaveData_Cust;
                        var dataBank = this.GetSaveData_Bank;
                        var dataBank2 = this.GetSaveData_Bank2;
                        var dataBank3 = this.GetSaveData_Bank3;
                        var dataBank3_2 = this.GetSaveData_Bank3_2;
                        var dataBank4 = this.GetSaveData_Bank4;
                        var dataBank5 = this.GetSaveData_Bank5;
                        var dataBank6 = this.GetSaveData_Bank6;
                        var dataBank7 = this.GetSaveData_Bank7;
                        var dataOrder = this.GetSaveData_Order;

                        if
                          (
                              base.Cust.UpdateOrderData
                                  (
                                      strOrderID: this.strOrderID,
                                      strCustID: this.strCustID,
                                      dataCust: this.GetSaveData_Cust,
                                      dataBank: this.GetSaveData_Bank,
                                      dataBank2: this.GetSaveData_Bank2,
                                      dataBank3: this.GetSaveData_Bank3,
                                      dataBank4: this.GetSaveData_Bank4,
                                      dataBank5: this.GetSaveData_Bank5,
                                      dataOrder: this.GetSaveData_Order,
                                      strUserName: Global.str_UserName,
                                      IsResend: this.IsResend,
                                      dataBank3_2: this.GetSaveData_Bank3_2,
                                      dataBank6: this.GetSaveData_Bank6,
                                      IsRejectByHKtoResend: this.IsRejectByHK,
                                      dataBank7: this.GetSaveData_Bank7
                                  )
                          )
                        {
                            strCustJson_After = base.Cust.GetCustLogData_Json(this.strCustID);
                            strCustBankJson_After = base.Cust.GetCustBankLogData_Json(this.strCustID);
                            this.WriteLogByJsonStr
                                (
                                    strCustJson_Before,
                                    strCustJson_After,
                                    strCustBankJson_Before,
                                    strCustBankJson_After
                                );

                            MessageBox.Show("重送成功");
                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show("重送失敗");
                        }

                    }
                }
           

                #endregion
            }
            else
            {
                #region 一般的資料修改

                if (this.DataValidation_Modify())
                {
                    if (this.IsOnlyCust)
                    {
                        if
                            (

                                base.Cust.UpdateCustData
                                    (
                                        strCustID: this.strCustID,
                                        dataCust: this.GetSaveData_Cust,
                                        dataBank: this.GetSaveData_Bank,
                                        dataBank2: this.GetSaveData_Bank2,
                                        dataBank3: this.GetSaveData_Bank3,
                                        dataBank4: this.GetSaveData_Bank4,
                                        dataBank5: this.GetSaveData_Bank5,
                                        strUserName: Global.str_UserName,
                                        dataBank3_2: this.GetSaveData_Bank3_2,
                                        dataBank6: this.GetSaveData_Bank6,
                                        dataBank7: this.GetSaveData_Bank7
                                    )
                            )
                        {
                            strCustJson_After = base.Cust.GetCustLogData_Json(this.strCustID);
                            strCustBankJson_After = base.Cust.GetCustBankLogData_Json(this.strCustID);
                            this.WriteLogByJsonStr
                                (
                                    strCustJson_Before,
                                    strCustJson_After,
                                    strCustBankJson_Before,
                                    strCustBankJson_After
                                );

                            MessageBox.Show("更新成功");
                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show("更新失敗");
                        }
                    }
                    else
                    {
                        if
                            (
                                base.Cust.UpdateOrderData
                                    (
                                        strOrderID: this.strOrderID,
                                        strCustID: this.strCustID,
                                        dataCust: this.GetSaveData_Cust,
                                        dataBank: this.GetSaveData_Bank,
                                        dataBank2: this.GetSaveData_Bank2,
                                        dataBank3: this.GetSaveData_Bank3,
                                        dataBank4: this.GetSaveData_Bank4,
                                        dataBank5: this.GetSaveData_Bank5,
                                        dataOrder: this.GetSaveData_Order,
                                        strUserName: Global.str_UserName,
                                        IsResend: this.IsResend,
                                        dataBank3_2: this.GetSaveData_Bank3_2,
                                        dataBank6: this.GetSaveData_Bank6,
                                        dataBank7: this.GetSaveData_Bank7
                                    )
                            )
                        {
                            strCustJson_After = base.Cust.GetCustLogData_Json(this.strCustID);
                            strCustBankJson_After = base.Cust.GetCustBankLogData_Json(this.strCustID);
                            this.WriteLogByJsonStr
                                (
                                    strCustJson_Before,
                                    strCustJson_After,
                                    strCustBankJson_Before,
                                    strCustBankJson_After
                                );

                            MessageBox.Show("更新成功");
                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show("更新失敗");
                        }
                    }
                }
                #endregion
            }
        }

        private bool WriteLogByJsonStr
            (
                string strCust_Before,
                string strCust_After,
                string strBank_Before,
                string strBank_After
            )
        {
            var result = base.Cust.WriteJsonLog(new List<BL_CUST_EDIT_LOG>()
                {
                    new BL_CUST_EDIT_LOG()
                    {
                        EDIT_BY = Global.str_UserName.ConvertToString(),
                        EDIT_OLD_DESC = strCust_Before,
                        EDIT_NEW_DESC = strCust_After
                    },
                    new BL_CUST_EDIT_LOG()
                    {
                        EDIT_BY = Global.str_UserName.ConvertToString(),
                        EDIT_OLD_DESC = strBank_Before,
                        EDIT_NEW_DESC = strBank_After
                    }
                });

            //  發送通知Mail
            #region Mail 通知異動
            //BL_CUST_LOG
            //BL_CUST_BANK_LOG
            var dataCust_After = JsonConvert.DeserializeObject<BL_CUST_LOG>(strCust_After);
            var dataBank_Before = JsonConvert.DeserializeObject<List<BL_CUST_BANK_LOG>>(strBank_Before);
            var dataBank_After = JsonConvert.DeserializeObject<List<BL_CUST_BANK_LOG>>(strBank_After);
            var IsHasChange = false;    //判斷銀行帳號是否有異動, 有的話才發送通知

            var strMailSubject = "客戶[" + dataCust_After.CUST_CNAME.ConvertToString() + "]銀行資料異動通知";
            var strMailTo = GetConfigValueByKey("Mail_ModifyCustBaseInfo").Split(new string[] { ";" }, StringSplitOptions.None).ToList();
            var strMailBody =
                "您好，<br/>" +
                "　　以下為銀行資料異動內容<br/>" +
                "　　客戶：" + dataCust_After.CUST_CNAME.ConvertToString() + "<br/>" +
                "　　CUST ID：" + (dataCust_After.CUST_ID == null ? "" : dataCust_After.CUST_ID.Value.ConvertToString()) + "<br/>" +
                "　　異動者：" + dataBank_After[0].LAST_UPDATED_BY.ConvertToString() + "<br/>" +
                "　　異動時間：" + (dataBank_After[0].LAST_UPDATE_DATE == null ? string.Empty : dataBank_After[0].LAST_UPDATE_DATE.Value.ToString("yyyy/MM/dd HH:mm:ss")) +
                "<br/><br/>";

            var strAlert = " style='color:red;' ";
            dataBank_After.ForEach(x =>
            {
                var dataBank_Before_Single = new BL_CUST_BANK_LOG();
                var strShowAlert = string.Empty;
                if (dataBank_Before.Where(y => y.CURRENCY == x.CURRENCY).ToList().Count() > 0)
                {
                    dataBank_Before_Single = dataBank_Before.Where(y => y.CURRENCY == x.CURRENCY).ToList()[0];
                }
                strMailBody += "<div><b>幣別：" + x.CURRENCY.ConvertToString() + "</b></div>";
                strMailBody += "<table border='1' style='width:100%'>";

                #region 主要持有人
                strMailBody += "<tr><th colspan='3'>主要持有人</th></tr>";
                strMailBody += "<tr><th style='width:20%;'>欄位名稱</th><th style='width:40%;'>修改前</th><th style='width:40%;'>修改後</th></tr>";

                strShowAlert = dataBank_Before_Single.BANK_CNAME.ConvertToString().Trim() != x.BANK_CNAME.ConvertToString().Trim() ? strAlert : string.Empty;
                strMailBody += "<tr" + strShowAlert + "><th>銀行名稱</th><td>" + dataBank_Before_Single.BANK_CNAME.ConvertToString() + "</td><td>" + x.BANK_CNAME.ConvertToString() + "</td></tr>";

                strShowAlert = dataBank_Before_Single.BRANCH_CNAME.ConvertToString().Trim() != x.BRANCH_CNAME.ConvertToString().Trim() ? strAlert : string.Empty;
                strMailBody += "<tr" + strShowAlert + "><th>分行名稱</th><td>" + dataBank_Before_Single.BRANCH_CNAME.ConvertToString() + "</td><td>" + x.BRANCH_CNAME.ConvertToString() + "</td></tr>";

                strShowAlert = dataBank_Before_Single.BANK_ENAME.ConvertToString().Trim() != x.BANK_ENAME.ConvertToString().Trim() ? strAlert : string.Empty;
                strMailBody += "<tr" + strShowAlert + "><th>銀行名稱(英)</th><td>" + dataBank_Before_Single.BANK_ENAME.ConvertToString() + "</td><td>" + x.BANK_ENAME.ConvertToString() + "</td></tr>";

                strShowAlert = dataBank_Before_Single.BRANCH_ENAME.ConvertToString().Trim() != x.BRANCH_ENAME.ConvertToString().Trim() ? strAlert : string.Empty;
                strMailBody += "<tr" + strShowAlert + "><th>分行名稱(英)</th><td>" + dataBank_Before_Single.BRANCH_ENAME.ConvertToString() + "</td><td>" + x.BRANCH_ENAME.ConvertToString() + "</td></tr>";

                strShowAlert = dataBank_Before_Single.ACCOUNT_CNAME.ConvertToString().Trim() != x.ACCOUNT_CNAME.ConvertToString().Trim() ? strAlert : string.Empty;
                strMailBody += "<tr" + strShowAlert + "><th>帳戶持有人姓名</th><td>" + dataBank_Before_Single.ACCOUNT_CNAME.ConvertToString() + "</td><td>" + x.ACCOUNT_CNAME.ConvertToString() + "</td></tr>";

                strShowAlert = dataBank_Before_Single.ACCOUNT_ENAME.ConvertToString().Trim() != x.ACCOUNT_ENAME.ConvertToString().Trim() ? strAlert : string.Empty;
                strMailBody += "<tr" + strShowAlert + "><th>帳戶持有人姓名(英)</th><td>" + dataBank_Before_Single.ACCOUNT_ENAME.ConvertToString() + "</td><td>" + x.ACCOUNT_ENAME.ConvertToString() + "</td></tr>";

                strShowAlert = dataBank_Before_Single.ACCOUNT.ConvertToString().Trim() != x.ACCOUNT.ConvertToString().Trim() ? strAlert : string.Empty;
                strMailBody += "<tr" + strShowAlert + "><th>銀行帳戶</th><td>" + dataBank_Before_Single.ACCOUNT.ConvertToString() + "</td><td>" + x.ACCOUNT.ConvertToString() + "</td></tr>";

                strShowAlert = dataBank_Before_Single.SWIFT_CODE.ConvertToString().Trim() != x.SWIFT_CODE.ConvertToString().Trim() ? strAlert : string.Empty;
                strMailBody += "<tr" + strShowAlert + "><th>SWIFT CODE</th><td>" + dataBank_Before_Single.SWIFT_CODE.ConvertToString() + "</td><td>" + x.SWIFT_CODE.ConvertToString() + "</td></tr>";

                strShowAlert = dataBank_Before_Single.BANK_C_ADDRESS.ConvertToString().Trim() != x.BANK_C_ADDRESS.ConvertToString().Trim() ? strAlert : string.Empty;
                strMailBody += "<tr" + strShowAlert + "><th>銀行地址</th><td>" + dataBank_Before_Single.BANK_C_ADDRESS.ConvertToString() + "</td><td>" + x.BANK_C_ADDRESS.ConvertToString() + "</td></tr>";

                strShowAlert = dataBank_Before_Single.BANK_E_ADDRESS.ConvertToString().Trim() != x.BANK_E_ADDRESS.ConvertToString().Trim() ? strAlert : string.Empty;
                strMailBody += "<tr" + strShowAlert + "><th>銀行地址(英)</th><td>" + dataBank_Before_Single.BANK_E_ADDRESS.ConvertToString() + "</td><td>" + x.BANK_E_ADDRESS.ConvertToString() + "</td></tr>";

                #endregion
                
                #region 聯名持有人
                strMailBody += "<tr><th colspan='3'>聯名持有人</th></tr>";
                strMailBody += "<tr><th>欄位名稱</th><th>修改前</th><th>修改後</th></tr>";

                strShowAlert = dataBank_Before_Single.BANK_CNAME2.ConvertToString().Trim() != x.BANK_CNAME2.ConvertToString().Trim() ? strAlert : string.Empty;
                strMailBody += "<tr" + strShowAlert + "><th>銀行名稱</th><td>" + dataBank_Before_Single.BANK_CNAME2.ConvertToString() + "</td><td>" + x.BANK_CNAME2.ConvertToString() + "</td></tr>";

                strShowAlert = dataBank_Before_Single.BRANCH_CNAME2.ConvertToString().Trim() != x.BRANCH_CNAME2.ConvertToString().Trim() ? strAlert : string.Empty;
                strMailBody += "<tr" + strShowAlert + "><th>分行名稱</th><td>" + dataBank_Before_Single.BRANCH_CNAME2.ConvertToString() + "</td><td>" + x.BRANCH_CNAME2.ConvertToString() + "</td></tr>";

                strShowAlert = dataBank_Before_Single.BANK_ENAME2.ConvertToString().Trim() != x.BANK_ENAME2.ConvertToString().Trim() ? strAlert : string.Empty;
                strMailBody += "<tr" + strShowAlert + "><th>銀行名稱(英)</th><td>" + dataBank_Before_Single.BANK_ENAME2.ConvertToString() + "</td><td>" + x.BANK_ENAME2.ConvertToString() + "</td></tr>";

                strShowAlert = dataBank_Before_Single.BRANCH_ENAME2.ConvertToString().Trim() != x.BRANCH_ENAME2.ConvertToString().Trim() ? strAlert : string.Empty;
                strMailBody += "<tr" + strShowAlert + "><th>分行名稱(英)</th><td>" + dataBank_Before_Single.BRANCH_ENAME2.ConvertToString() + "</td><td>" + x.BRANCH_ENAME2.ConvertToString() + "</td></tr>";

                strShowAlert = dataBank_Before_Single.ACCOUNT_CNAME2.ConvertToString().Trim() != x.ACCOUNT_CNAME2.ConvertToString().Trim() ? strAlert : string.Empty;
                strMailBody += "<tr" + strShowAlert + "><th>帳戶持有人姓名</th><td>" + dataBank_Before_Single.ACCOUNT_CNAME2.ConvertToString() + "</td><td>" + x.ACCOUNT_CNAME2.ConvertToString() + "</td></tr>";

                strShowAlert = dataBank_Before_Single.ACCOUNT_ENAME2.ConvertToString().Trim() != x.ACCOUNT_ENAME2.ConvertToString().Trim() ? strAlert : string.Empty;
                strMailBody += "<tr" + strShowAlert + "><th>帳戶持有人姓名(英)</th><td>" + dataBank_Before_Single.ACCOUNT_ENAME2.ConvertToString() + "</td><td>" + x.ACCOUNT_ENAME2.ConvertToString() + "</td></tr>";

                strShowAlert = dataBank_Before_Single.ACCOUNT2.ConvertToString().Trim() != x.ACCOUNT2.ConvertToString().Trim() ? strAlert : string.Empty;
                strMailBody += "<tr" + strShowAlert + "><th>銀行帳戶</th><td>" + dataBank_Before_Single.ACCOUNT2.ConvertToString() + "</td><td>" + x.ACCOUNT2.ConvertToString() + "</td></tr>";

                strShowAlert = dataBank_Before_Single.SWIFT_CODE2.ConvertToString().Trim() != x.SWIFT_CODE2.ConvertToString().Trim() ? strAlert : string.Empty;
                strMailBody += "<tr" + strShowAlert + "><th>SWIFT CODE</th><td>" + dataBank_Before_Single.SWIFT_CODE2.ConvertToString() + "</td><td>" + x.SWIFT_CODE2.ConvertToString() + "</td></tr>";

                strShowAlert = dataBank_Before_Single.BANK_C_ADDRESS2.ConvertToString().Trim() != x.BANK_C_ADDRESS2.ConvertToString().Trim() ? strAlert : string.Empty;
                strMailBody += "<tr" + strShowAlert + "><th>銀行地址</th><td>" + dataBank_Before_Single.BANK_C_ADDRESS2.ConvertToString() + "</td><td>" + x.BANK_C_ADDRESS2.ConvertToString() + "</td></tr>";

                strShowAlert = dataBank_Before_Single.BANK_E_ADDRESS2.ConvertToString().Trim() != x.BANK_E_ADDRESS2.ConvertToString().Trim() ? strAlert : string.Empty;
                strMailBody += "<tr" + strShowAlert + "><th>銀行地址(英)</th><td>" + dataBank_Before_Single.BANK_E_ADDRESS2.ConvertToString() + "</td><td>" + x.BANK_E_ADDRESS2.ConvertToString() + "</td></tr>";

                #endregion

                strMailBody += "</table>";
                strMailBody += "<br/>";


                //  2020/03/22 檢查部分欄位有異動才發送通知[主要持有人]
                //  銀行名稱 / 分行名稱 / 銀行名稱(英) / 分行名稱 (英) / 帳戶持有人姓名 / 帳戶持有人姓名(英文)
                //  銀行帳戶 / Swift Code / 銀行地址 / 銀行地址(英)
                ////  檢核[銀行帳戶]是否有異動
                //if 
                //    (
                //        dataBank_Before_Single.ACCOUNT.ConvertToString().Trim() != x.ACCOUNT.ConvertToString().Trim() ||
                //        dataBank_Before_Single.ACCOUNT2.ConvertToString().Trim() != x.ACCOUNT2.ConvertToString().Trim()
                //    )
                //{
                //    IsHasChange = true;
                //}
                if
                    (
                        dataBank_Before_Single.BANK_CNAME.ConvertToString().Trim() != x.BANK_CNAME.ConvertToString().Trim() ||
                        dataBank_Before_Single.BRANCH_CNAME.ConvertToString().Trim() != x.BRANCH_CNAME.ConvertToString().Trim() ||
                        dataBank_Before_Single.BANK_ENAME.ConvertToString().Trim() != x.BANK_ENAME.ConvertToString().Trim() ||
                        dataBank_Before_Single.BRANCH_ENAME.ConvertToString().Trim() != x.BRANCH_ENAME.ConvertToString().Trim() ||
                        dataBank_Before_Single.ACCOUNT_CNAME.ConvertToString().Trim() != x.ACCOUNT_CNAME.ConvertToString().Trim() ||
                        dataBank_Before_Single.ACCOUNT_ENAME.ConvertToString().Trim() != x.ACCOUNT_ENAME.ConvertToString().Trim() ||
                        dataBank_Before_Single.ACCOUNT.ConvertToString().Trim() != x.ACCOUNT.ConvertToString().Trim() ||
                        dataBank_Before_Single.SWIFT_CODE.ConvertToString().Trim() != x.SWIFT_CODE.ConvertToString().Trim() ||
                        dataBank_Before_Single.BANK_C_ADDRESS.ConvertToString().Trim() != x.BANK_C_ADDRESS.ConvertToString().Trim() ||
                        dataBank_Before_Single.BANK_E_ADDRESS.ConvertToString().Trim() != x.BANK_E_ADDRESS.ConvertToString().Trim()
                    )
                {
                    IsHasChange = true;
                }
            });

            strMailBody = strMailBody.Replace(Environment.NewLine, "<br/>");
            if (IsHasChange)
            {
                //  2019/07/05 > 有異動才發送通知
                SendMail(strMailTo, strMailSubject, strMailBody);
            }
            #endregion

            return result;
        }

        private bool DataValidation_Modify()
        {
            var result = true;
            var msg = string.Empty;

            #region Customer
            if (this.IsAuditModifyMode)
            {
                if (this.txtCUST_CNAME.Text.Trim() == string.Empty)
                {
                    msg += "請輸入「姓名」" + Environment.NewLine;
                }
                if (this.txtCUST_ENAME.Text.Trim() == string.Empty)
                {
                    msg += "請輸入「姓名(英)」" + Environment.NewLine;
                }
                if (rdoMan.Checked == false && this.rdoWoman.Checked == false)
                {
                    msg += "請選擇「性別」" + Environment.NewLine;
                }

                if (this.dteBirthday.Text.Trim().IsDate() == false)
                {
                    msg += "請輸入「出生年月日」" + Environment.NewLine;
                }
                if (this.txtID_NUMBER.Text.Trim() == string.Empty)
                {
                    msg += "請輸入「身分證號碼」" + Environment.NewLine;
                }
                else
                {
                    //  檢查此ID是否已經存在
                    var dataChk = base.Cust.GetCustDataByID_Number(this.txtID_NUMBER.Text.Trim(), this.txtID_NUMBER2.Text.Trim());
                    if (dataChk.Count > 0)
                    {
                        var dataCurrent = dataChk.Where(x => x.CUST_ID.ConvertToString() != this.strCustID).ToList();
                        if (dataCurrent.Count > 0)
                        {
                            msg += "此客戶已經存在" + Environment.NewLine;
                        }

                    }
                }
            }
            
            if (this.txtPHONE.Text.Trim() == string.Empty)
            {
                msg += "請輸入「聯絡電話」" + Environment.NewLine;
            }
            if (this.txtEMAIL.Text.Trim() == string.Empty)
            {
                msg += "請輸入「電子郵件信箱」" + Environment.NewLine;
            }
            if (this.txtPOSTAL_CODE.Text.Trim() == string.Empty)
            {
                msg += "請輸入「郵遞區號」" + Environment.NewLine;
            }
            if (this.txtAddress.Text.Trim() == string.Empty)
            {
                msg += "請輸入「居住地址」" + Environment.NewLine;
            }

            if (this.IsOnlyCust == false)
            {
                if (this.chkMAIL_TYPE_MONTHLY1.Checked == false && this.chkMAIL_TYPE_MONTHLY2.Checked == false)
                {
                    msg += "請選擇「合約」送達方式" + Environment.NewLine;
                }
                if (this.chkMAIL_TYPE_END1.Checked == false && this.chkMAIL_TYPE_END2.Checked == false && this.chkMAIL_TYPE_END3.Checked == false)
                {
                    msg += "請選擇「月對帳單」取得方式" + Environment.NewLine;
                }
                if (this.chkMAIL_TYPE_END3.Checked && this.txtMAIL_TYPE_END.Text.Trim() == string.Empty)
                {
                    msg += "請輸入「月對帳單」取得方式" + Environment.NewLine;
                }
            }
            #endregion


            #region Bank
            var check_count = 0;
            var msg_bank = string.Empty;
            
            if (this.txtBANK_CNAME.Text.Trim() == string.Empty)
            {
                msg_bank += "請輸入「銀行名稱」" + Environment.NewLine;
            }
            if (this.txtBRANCH_CNAME.Text.Trim() == string.Empty)
            {
                msg_bank += "請輸入「分行名稱」" + Environment.NewLine;
            }
            if (this.txtACCOUNT_CNAME.Text.Trim() == string.Empty)
            {
                msg_bank += "請輸入「帳戶持有人姓名」" + Environment.NewLine;
            }
            if (this.txtACCOUNT.Text.Trim() == string.Empty)
            {
                msg_bank += "請輸入「銀行帳戶」" + Environment.NewLine;
            }
            if (this.txtSWIFT_CODE.Text.Trim() == string.Empty)
            {
                msg_bank += "請輸入「國際匯款代碼」" + Environment.NewLine;
            }
            if (this.txtBANK_C_ADDRESS.Text.Trim() == string.Empty)
            {
                msg_bank += "請輸入「銀行地址」" + Environment.NewLine;
            }

            if (msg_bank.Length > 0)
            {
                check_count = check_count+1;
            }


            var msg_bank2 = string.Empty;

            if (this.txtBANK_CNAME_2.Text.Trim() == string.Empty)
            {
                msg_bank2 += "請輸入「銀行名稱」" + Environment.NewLine;
            }
            if (this.txtBRANCH_CNAME_2.Text.Trim() == string.Empty)
            {
                msg_bank2 += "請輸入「分行名稱」" + Environment.NewLine;
            }
            if (this.txtACCOUNT_CNAME_2.Text.Trim() == string.Empty)
            {
                msg_bank2 += "請輸入「帳戶持有人姓名」" + Environment.NewLine;
            }
            if (this.txtACCOUNT_2.Text.Trim() == string.Empty)
            {
                msg_bank2 += "請輸入「銀行帳戶」" + Environment.NewLine;
            }
            if (this.txtSWIFT_CODE_2.Text.Trim() == string.Empty)
            {
                msg_bank2 += "請輸入「國際匯款代碼」" + Environment.NewLine;
            }
            if (this.txtBANK_C_ADDRESS_2.Text.Trim() == string.Empty)
            {
                msg_bank2 += "請輸入「銀行地址」" + Environment.NewLine;
            }

            if (msg_bank2.Length > 0)
            {
                check_count = check_count+ 1;
            }

            var msg_bank3 = string.Empty;

            if (this.txtBANK_CNAME_3.Text.Trim() == string.Empty)
            {
                msg_bank3 += "請輸入「銀行名稱」" + Environment.NewLine;
            }
            if (this.txtBRANCH_CNAME_3.Text.Trim() == string.Empty)
            {
                msg_bank3 += "請輸入「分行名稱」" + Environment.NewLine;
            }
            if (this.txtACCOUNT_CNAME_3.Text.Trim() == string.Empty)
            {
                msg_bank3 += "請輸入「帳戶持有人姓名」" + Environment.NewLine;
            }
            if (this.txtACCOUNT_3.Text.Trim() == string.Empty)
            {
                msg_bank3 += "請輸入「銀行帳戶」" + Environment.NewLine;
            }
            if (this.txtSWIFT_CODE_3.Text.Trim() == string.Empty)
            {
                msg_bank3 += "請輸入「國際匯款代碼」" + Environment.NewLine;
            }
            if (this.txtBANK_C_ADDRESS_3.Text.Trim() == string.Empty)
            {
                msg_bank3 += "請輸入「銀行地址」" + Environment.NewLine;
            }

            if (msg_bank3.Length > 0)
            {
                check_count = check_count + 1;
            }

            var msg_bank3_2 = string.Empty;

            if (this.txtBANK_CNAME_3_2.Text.Trim() == string.Empty)
            {
                msg_bank3_2 += "請輸入「銀行名稱」" + Environment.NewLine;
            }
            if (this.txtBRANCH_CNAME_3_2.Text.Trim() == string.Empty)
            {
                msg_bank3_2 += "請輸入「分行名稱」" + Environment.NewLine;
            }
            if (this.txtACCOUNT_CNAME_3_2.Text.Trim() == string.Empty)
            {
                msg_bank3_2 += "請輸入「帳戶持有人姓名」" + Environment.NewLine;
            }
            if (this.txtACCOUNT_3_2.Text.Trim() == string.Empty)
            {
                msg_bank3_2 += "請輸入「銀行帳戶」" + Environment.NewLine;
            }
            if (this.txtSWIFT_CODE_3_2.Text.Trim() == string.Empty)
            {
                msg_bank3_2 += "請輸入「國際匯款代碼」" + Environment.NewLine;
            }
            if (this.txtBANK_C_ADDRESS_3_2.Text.Trim() == string.Empty)
            {
                msg_bank3_2 += "請輸入「銀行地址」" + Environment.NewLine;
            }

            if (msg_bank3_2.Length > 0)
            {
                check_count = check_count + 1;
            }

            var msg_bank4 = string.Empty;

            if (this.txtBANK_CNAME_4.Text.Trim() == string.Empty)
            {
                msg_bank4 += "請輸入「銀行名稱」" + Environment.NewLine;
            }
            if (this.txtBRANCH_CNAME_4.Text.Trim() == string.Empty)
            {
                msg_bank4 += "請輸入「分行名稱」" + Environment.NewLine;
            }
            if (this.txtACCOUNT_CNAME_4.Text.Trim() == string.Empty)
            {
                msg_bank4 += "請輸入「帳戶持有人姓名」" + Environment.NewLine;
            }
            if (this.txtACCOUNT_4.Text.Trim() == string.Empty)
            {
                msg_bank4 += "請輸入「銀行帳戶」" + Environment.NewLine;
            }
            if (this.txtSWIFT_CODE_4.Text.Trim() == string.Empty)
            {
                msg_bank4 += "請輸入「國際匯款代碼」" + Environment.NewLine;
            }
            if (this.txtBANK_C_ADDRESS_4.Text.Trim() == string.Empty)
            {
                msg_bank4 += "請輸入「銀行地址」" + Environment.NewLine;
            }

            if (msg_bank4.Length > 0)
            {
                check_count = check_count + 1;
            }

            var msg_bank5 = string.Empty;

            if (this.txtBANK_CNAME_5.Text.Trim() == string.Empty)
            {
                msg_bank5 += "請輸入「銀行名稱」" + Environment.NewLine;
            }
            if (this.txtBRANCH_CNAME_5.Text.Trim() == string.Empty)
            {
                msg_bank5 += "請輸入「分行名稱」" + Environment.NewLine;
            }
            if (this.txtACCOUNT_CNAME_5.Text.Trim() == string.Empty)
            {
                msg_bank5 += "請輸入「帳戶持有人姓名」" + Environment.NewLine;
            }
            if (this.txtACCOUNT_5.Text.Trim() == string.Empty)
            {
                msg_bank5 += "請輸入「銀行帳戶」" + Environment.NewLine;
            }
            if (this.txtSWIFT_CODE_5.Text.Trim() == string.Empty)
            {
                msg_bank5 += "請輸入「國際匯款代碼」" + Environment.NewLine;
            }
            if (this.txtBANK_C_ADDRESS_5.Text.Trim() == string.Empty)
            {
                msg_bank5 += "請輸入「銀行地址」" + Environment.NewLine;
            }

            if (msg_bank5.Length > 0)
            {
                check_count = check_count + 1;
            }


            var msg_bank6 = string.Empty;

            if (this.txtBANK_CNAME_6.Text.Trim() == string.Empty)
            {
                msg_bank6 += "請輸入「銀行名稱」" + Environment.NewLine;
            }
            if (this.txtBRANCH_CNAME_6.Text.Trim() == string.Empty)
            {
                msg_bank6 += "請輸入「分行名稱」" + Environment.NewLine;
            }
            if (this.txtACCOUNT_CNAME_6.Text.Trim() == string.Empty)
            {
                msg_bank6 += "請輸入「帳戶持有人姓名」" + Environment.NewLine;
            }
            if (this.txtACCOUNT_6.Text.Trim() == string.Empty)
            {
                msg_bank6 += "請輸入「銀行帳戶」" + Environment.NewLine;
            }
            if (this.txtSWIFT_CODE_6.Text.Trim() == string.Empty)
            {
                msg_bank6 += "請輸入「國際匯款代碼」" + Environment.NewLine;
            }
            if (this.txtBANK_C_ADDRESS_6.Text.Trim() == string.Empty)
            {
                msg_bank6 += "請輸入「銀行地址」" + Environment.NewLine;
            }

            if (msg_bank6.Length > 0)
            {
                check_count = check_count + 1;
            }


            var msg_bank7 = string.Empty;

            if (this.txtBANK_CNAME_7.Text.Trim() == string.Empty)
            {
                msg_bank7 += "請輸入「銀行名稱」" + Environment.NewLine;
            }
            if (this.txtBRANCH_CNAME_7.Text.Trim() == string.Empty)
            {
                msg_bank7 += "請輸入「分行名稱」" + Environment.NewLine;
            }
            if (this.txtACCOUNT_CNAME_7.Text.Trim() == string.Empty)
            {
                msg_bank7 += "請輸入「帳戶持有人姓名」" + Environment.NewLine;
            }
            if (this.txtACCOUNT_7.Text.Trim() == string.Empty)
            {
                msg_bank7 += "請輸入「銀行帳戶」" + Environment.NewLine;
            }
            if (this.txtSWIFT_CODE_7.Text.Trim() == string.Empty)
            {
                msg_bank7 += "請輸入「國際匯款代碼」" + Environment.NewLine;
            }
            if (this.txtBANK_C_ADDRESS_7.Text.Trim() == string.Empty)
            {
                msg_bank7 += "請輸入「銀行地址」" + Environment.NewLine;
            }

            if (msg_bank7.Length > 0)
            {
                check_count = check_count + 1;
            }

            if (check_count == 8)
            {
                msg += "請至少輸入一個銀行帳戶資料" + Environment.NewLine;
            }

            #endregion

            #region Order
            if (this.IsOnlyCust == false)
            {
                if (this.txtSales.Text.Trim() == string.Empty || this.txtIBCode.Text.Trim() == string.Empty)
                {
                    msg += "請選擇「業務」" + Environment.NewLine;
                }

                if (this.dteExtendDate.Text.IsDate() && this.dteEndDate.Text.IsDate())
                {

                    if (txtYear.Text != "0.50") //排除半年期
                    {
                        if (this.dteExtendDate.Value.ToString("yyyy/MM/dd") != DateTime.Parse(this.dteEndDate.Text).AddYears(1).ToString("yyyy/MM/dd"))
                        {
                            msg += "「Extend Date」需等於「End Date + 1年」" + Environment.NewLine;
                        }
                    }


                }
            }
            #endregion

            if (msg != string.Empty)
            {
                result = false;
                MessageBox.Show(msg);
            }
            return result;
        }

        private void dteFromDate_ValueChanged(object sender, EventArgs e)
        {
            
            (sender as DateTimePicker).Format = DateTimePickerFormat.Long;
            CheckEndDate();

            this.CheckFromDateEvent();
        }

        private void CheckFromDateEvent()
        {
            if (this.dteFromDate.Text.Trim().IsDate() == true)
            {
                if (this.dteFromDate.Value >= new DateTime(2019, 1, 15) && this.dteFromDate.Value <= new DateTime(2019, 1, 31))
                {
                    this.checkBox1.Enabled = true;
                }
                else
                {
                    this.checkBox1.Enabled = false;
                    this.txtYear.Enabled = true;
                    this.checkBox1.Checked = false;
                }
            }
            else
            {
                this.checkBox1.Enabled = false;
                this.txtYear.Enabled = true;
                this.checkBox1.Checked = false;
            }

            if (IsViewMode)
            {
                this.txtYear.Enabled = false;
                this.checkBox1.Enabled = false;
            }
        }

        private void txtYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckEndDate();
            switch (this.txtYear.Text.Trim())
            {
                case "0.5": // 半年期 
                    this.txtInterestedRate.Text = "3.6"; //利率
                    break;
                case "1":
                    this.txtInterestedRate.Text = "10.2";
                    break;
                case "2":
                    this.txtInterestedRate.Text = "11.04";
                    break;
                case "3":
                    this.txtInterestedRate.Text = "12";
                    break;
                default:
                    break;
            }
        }

        private void CheckEndDate()
        {
            var int_years = 0;
            bool check = false;
            if (txtYear.Text != "0.5")
            {

                check = int.TryParse(txtYear.Text, out int_years);
            }
            else
            {
                check = true;
            }

            if (check)
            {
                if (int_years > 0)
                {
                    dteEndDate.Text = dteFromDate.Value.AddYears(int_years).ToString("yyyy/MM/dd");
                    //this.SetOrderEndDate();
                }
                if (int_years <= 0.5)
                {
                    dteEndDate.Text = dteFromDate.Value.AddMonths(6).ToString("yyyy/MM/dd");
                    //dteExtendDate.Format = DateTimePickerFormat.Long;;
                    //dteExtendDate.Value = dteFromDate.Value.AddMonths(12)
                    //dteExtendDate.Text = dteFromDate.Value.AddMonths(12).ToString("yyyy/MM/dd");
                }
            }
        }

        private void cb_close_CheckedChanged(object sender, EventArgs e)
        {
            if (this.cb_close.Checked)
            {
                this.SetOrderEndDate();
            }
            else
            {
                this.dteOrderEnd.Value = DateTime.Now;
                this.dteOrderEnd.Text = string.Empty;
                this.dteOrderEnd.Format = DateTimePickerFormat.Custom;
                this.dteOrderEnd.CustomFormat = " ";
            }
            
        }
        private void SetOrderEndDate()
        {
            if (this.dteEndDate.Text.IsDate())
            {
                this.dteOrderEnd.Format = DateTimePickerFormat.Long;
                this.dteOrderEnd.Value = DateTime.Parse(this.dteEndDate.Text);
                this.dteOrderEnd.Text = this.dteEndDate.Text;
                this.cb_close.Checked = true;
            }
        }

        private void btnDelExtendDate_Click(object sender, EventArgs e)
        {
            this.dteExtendDate.Value = DateTime.Now;
            this.dteExtendDate.Text = string.Empty;
            this.dteExtendDate.Format = DateTimePickerFormat.Custom;
            this.dteExtendDate.CustomFormat = " ";
        }

        private void txtSales_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                // this.txtIBCode.Text = string.Empty;
                this.txtSales.Text = string.Empty;
                this.txtSales.Tag = string.Empty;

                var data = base.Emp.GetBL_EMP();
                data = data.Where(x => x.IB_CODE.ConvertToString().Trim() != string.Empty && x.INTERNAL_IB_CODE.ConvertToString().Trim()=="").ToList();
                data = data
                 .Where(x => x.IB_CODE.ConvertToString().ToUpper().IndexOf(this.txtIBCode.Text.Trim().ToUpper()) != -1)
                 .ToList();
                if (data.Count != 0)
                {
                    this.txtIBCode.Text = data[0].IB_CODE.ConvertToString();
                    this.txtSales.Text = data[0].CNAME.ConvertToString();
                    this.txtSales.Tag = data[0].EMP_CODE.ConvertToString();
                }
            }
        }

        private void btnDelFromDate_Click(object sender, EventArgs e)
        {
            this.dteFromDate.Value = DateTime.Now;
            this.dteFromDate.Text = string.Empty;
            this.dteFromDate.Format = DateTimePickerFormat.Custom;
            this.dteFromDate.CustomFormat = " ";
            this.dteEndDate.Text = string.Empty;
        }

        private void btnDelBirthday_Click(object sender, EventArgs e)
        {
            this.dteBirthday.Value = DateTime.Now;
            this.dteBirthday.Text = string.Empty;
            this.dteBirthday.Format = DateTimePickerFormat.Custom;
            this.dteBirthday.CustomFormat = " ";
        }

        private void btnDelBirthday2_Click(object sender, EventArgs e)
        {
            this.dteBirthday2.Value = DateTime.Now;
            this.dteBirthday2.Text = string.Empty;
            this.dteBirthday2.Format = DateTimePickerFormat.Custom;
            this.dteBirthday2.CustomFormat = " ";
        }

        private void txtParentOrderNo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.txtAmount_NTD_OLD.Text = string.Empty;
                this.txtAmount_RMB_OLD.Text = string.Empty;
                this.txtAmount_USD_OLD.Text = string.Empty;
                this.txtAmount_EUR_OLD.Text = string.Empty;
                this.txtAmount_AUD_OLD.Text = string.Empty;
                this.txtAmount_JPY_OLD.Text = string.Empty;
                this.txtAmount_NZD_OLD.Text = string.Empty;

                var data = base.Cust.GetContractDataByCustID(this.strCustID.ConvertToString());
                data = data.Where(x => x.ORDER_NO.ConvertToString().Trim() == this.txtParentOrderNo.Text.Trim()).ToList();
                if (data.Count() > 0)
                {
                    var dataSingle = data[0];
                    this.txtParentOrderNo.Text = dataSingle.ORDER_NO.ConvertToString();
                    this.txtAmount_USD_OLD.Text = dataSingle.AMOUNT_USD == null ? "" : dataSingle.AMOUNT_USD.Value.ConvertToString();
                    this.txtAmount_NTD_OLD.Text = dataSingle.AMOUNT_NTD == null ? "" : dataSingle.AMOUNT_NTD.Value.ConvertToString();
                    this.txtAmount_RMB_OLD.Text = dataSingle.AMOUNT_RMB == null ? "" : dataSingle.AMOUNT_RMB.Value.ConvertToString();
                    this.txtAmount_EUR_OLD.Text = dataSingle.AMOUNT_EUR == null ? "" : dataSingle.AMOUNT_EUR.Value.ConvertToString();
                    this.txtAmount_AUD_OLD.Text = dataSingle.AMOUNT_AUD == null ? "" : dataSingle.AMOUNT_AUD.Value.ConvertToString();
                    this.txtAmount_JPY_OLD.Text = dataSingle.AMOUNT_JPY == null ? "" : dataSingle.AMOUNT_JPY.Value.ConvertToString();
                    this.txtAmount_NZD_OLD.Text = dataSingle.AMOUNT_NZD == null ? "" : dataSingle.AMOUNT_NZD.Value.ConvertToString();
                    this.chkIsMultiContract.Visible = true;
                }
                else
                {
                    this.txtParentOrderNo.Text = string.Empty;
                    this.chkIsMultiContract.Visible = false;
                }
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (this.checkBox1.Checked)
            {
                this.txtYear.SelectedIndex = 2; //1 Years
                this.txtYear.Enabled = false;
                //this.txtYear_SelectedIndexChanged(this.txtYear, new EventArgs());
                this.txtInterestedRate.Text = "12";
            }
            else
            {
                this.txtYear.Enabled = true;
                this.txtYear_SelectedIndexChanged(this.txtYear, new EventArgs());
            }
        }

        private void btnTransOrder_Click(object sender, EventArgs e)
        {
            if (this.strOrderID.IsNumeric() && this.strCustID.IsNumeric())
            {

                var strNewCustID = string.Empty;
                var frm = new FrmSelectCust
                    (
                        CustID: new List<string>(),
                        excCustID: this.strCustID
                    )
                { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
                frm.ShowDialog();
                if (frm.status == MK.Demo.Data.enCommon.enFormStatus.OK)
                {
                    strNewCustID = frm.liCustID.Count() > 0 ? frm.liCustID[0] : string.Empty;
                }
                frm.Dispose();
                frm = null;

                
                if (strNewCustID != string.Empty)
                {
                    if (MessageBox.Show("確認要將此筆訂單轉換?", "系統訊息", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        if (base.Cust.TransOrder(this.strOrderID, this.strCustID, strNewCustID, Global.str_UserName))
                        {
                            MessageBox.Show("合約轉換完畢");
                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show("合約轉換失敗");
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("不符合合約轉讓條件");
            }
        }

        private void btnModifyALL_Click(object sender, EventArgs e)
        {
            //  將所有唯獨欄位改為可編輯
            this.IsAuditModifyMode = true;

            this.txtCUST_CNAME.Enabled = true;
            this.txtCUST_CNAME2.Enabled = true;
            this.txtCUST_ENAME.Enabled = true;
            this.txtCUST_ENAME2.Enabled = true;

            this.rdoWoman.Enabled = true;
            this.rdoMan.Enabled = true;
            this.rdoWoman2.Enabled = true;
            this.rdoMan2.Enabled = true;

            this.dteBirthday.Enabled = true;
            this.btnDelBirthday.Enabled = true;
            this.dteBirthday2.Enabled = true;
            this.btnDelBirthday2.Enabled = true;

            this.txtNATION.Enabled = true;
            this.txtNATION2.Enabled = true;
            this.txtPASSPORT.Enabled = true;
            this.txtPASSPORT2.Enabled = true;
            this.txtID_NUMBER.Enabled = true;
            this.txtID_NUMBER2.Enabled = true;

            this.txtOrderNo.Enabled = true;
        }


        private void SetModifyALLStatus(bool IsCanEdit)
        {
            if (
                    Global.str_UserName.ToUpper() == "ADMIN" ||
                    Global.UserRole.ConvertToString() == "40" ||    //  Admin
                    Global.UserRole.ConvertToString() == "20"   //  行政查核人員
                )
            {
                this.btnModifyALL.Visible = IsCanEdit;
            }
            else
            {
                this.btnModifyALL.Visible = false;
            }
        }

        private void btnModifyHist_Click(object sender, EventArgs e)
        {
            var frm = new frmCustDataModifyHist(this.strCustID) { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
            frm.ShowDialog();
            frm.Dispose();
            frm = null;
        }


        #region 帶入客戶基本資料到銀行內

        private void btnImportToUSD1_Click(object sender, EventArgs e)
        {
            this.BringCustNameToNank("1", this.txtACCOUNT_CNAME, this.txtACCOUNT_ENAME);
        }

        private void btnImportToUSD2_Click(object sender, EventArgs e)
        {
            this.BringCustNameToNank("2", this.txtACCOUNT_CNAME2, this.txtACCOUNT_ENAME2);
        }


        private void btnImportToNTD1_Click(object sender, EventArgs e)
        {
            this.BringCustNameToNank("1", this.txtACCOUNT_CNAME_2, this.txtACCOUNT_ENAME_2);
        }

        private void btnImportToNTD2_Click(object sender, EventArgs e)
        {
            this.BringCustNameToNank("2", this.txtACCOUNT_CNAME2_2, this.txtACCOUNT_ENAME2_2);
        }


        private void btnImportToRMB1_Click(object sender, EventArgs e)
        {
            this.BringCustNameToNank("1", this.txtACCOUNT_CNAME_3, this.txtACCOUNT_ENAME_3);
        }

        private void btnImportToRMB2_Click(object sender, EventArgs e)
        {
            this.BringCustNameToNank("2", this.txtACCOUNT_CNAME2_3, this.txtACCOUNT_ENAME2_3);
        }

        private void btnImportToRMB1_2_Click(object sender, EventArgs e)
        {
            this.BringCustNameToNank("1", this.txtACCOUNT_CNAME_3_2, this.txtACCOUNT_ENAME_3_2);
        }

        private void btnImportToRMB2_2_Click(object sender, EventArgs e)
        {
            this.BringCustNameToNank("2", this.txtACCOUNT_CNAME2_3_2, this.txtACCOUNT_ENAME2_3_2);
        }

        private void btnImportToEUR1_Click(object sender, EventArgs e)
        {
            this.BringCustNameToNank("1", this.txtACCOUNT_CNAME_4, this.txtACCOUNT_ENAME_4);
        }

        private void btnImportToEUR2_Click(object sender, EventArgs e)
        {
            this.BringCustNameToNank("2", this.txtACCOUNT_CNAME2_4, this.txtACCOUNT_ENAME2_4);
        }


        private void btnImportToAUD1_Click(object sender, EventArgs e)
        {
            this.BringCustNameToNank("1", this.txtACCOUNT_CNAME_5, this.txtACCOUNT_ENAME_5);
        }

        private void btnImportToAUD2_Click(object sender, EventArgs e)
        {
            this.BringCustNameToNank("2", this.txtACCOUNT_CNAME2_5, this.txtACCOUNT_ENAME2_5);
        }

        private void btnImportToJPY1_Click(object sender, EventArgs e)
        {
            this.BringCustNameToNank("1", this.txtACCOUNT_CNAME_6, this.txtACCOUNT_ENAME_6);
        }

        private void btnImportToJPY2_Click(object sender, EventArgs e)
        {
            this.BringCustNameToNank("2", this.txtACCOUNT_CNAME2_6, this.txtACCOUNT_ENAME2_6);
        }

        private void btnImportToNZD1_Click(object sender, EventArgs e)
        {
            this.BringCustNameToNank("1", this.txtACCOUNT_CNAME_7, this.txtACCOUNT_ENAME_7);
        }

        private void btnImportToNZD2_Click(object sender, EventArgs e)
        {
            this.BringCustNameToNank("2", this.txtACCOUNT_CNAME2_7, this.txtACCOUNT_ENAME2_7);
        }

        private void BringCustNameToNank(string strType, TextBox txtC, TextBox txtE)
        {
            switch (strType)
            {
                case "1":
                    txtC.Text = this.txtCUST_CNAME.Text;
                    txtE.Text = this.txtCUST_ENAME.Text;
                    break;
                case "2":
                    txtC.Text = this.txtCUST_CNAME2.Text;
                    txtE.Text = this.txtCUST_ENAME2.Text;
                    break;
                default:
                    break;
            }
        }

        #endregion


        private void txtAmount_RMB_TextChanged(object sender, EventArgs e)
        {
            this.SetRMB_CheckBox();
            this.SetIndDiscountAmount();
        }

        private void SetRMB_CheckBox()
        {
            if (this.txtAmount_RMB.Text.Trim().IsNumeric())
            {
                this.checkBox2.Visible = true;
            }
            else
            {
                this.checkBox2.Visible = false;
                this.checkBox2.Checked = false;
            }
        }

        private void chkIsMultiContract_CheckedChanged(object sender, EventArgs e)
        {
            if (IsViewMode == false)
            {

                this.txtAmount_NTD_OLD.Enabled = this.chkIsMultiContract.Checked;
                this.txtAmount_USD_OLD.Enabled = this.chkIsMultiContract.Checked;
                this.txtAmount_RMB_OLD.Enabled = this.chkIsMultiContract.Checked;
                this.txtAmount_EUR_OLD.Enabled = this.chkIsMultiContract.Checked;
                this.txtAmount_AUD_OLD.Enabled = this.chkIsMultiContract.Checked;
                this.txtAmount_JPY_OLD.Enabled = this.chkIsMultiContract.Checked;
                this.txtAmount_NZD_OLD.Enabled = this.chkIsMultiContract.Checked;
            }
        }

        private void chk_ind_order_CheckedChanged(object sender, EventArgs e)
        {
            //  Call Amount
            this.SetIndDiscountAmount();
        }

        private void txtAmount_USD_TextChanged(object sender, EventArgs e)
        {
            this.SetIndDiscountAmount();
        }

        private void txtAmount_NTD_TextChanged(object sender, EventArgs e)
        {
            this.SetIndDiscountAmount();
        }

        private void txtAmount_EUR_TextChanged(object sender, EventArgs e)
        {
            this.SetIndDiscountAmount();
        }

        private void txtAmount_AUD_TextChanged(object sender, EventArgs e)
        {
            this.SetIndDiscountAmount();
        }

        private void txtAmount_JPY_TextChanged(object sender, EventArgs e)
        {
            this.SetIndDiscountAmount();
        }
        private void txtAmount_NZD_TextChanged(object sender, EventArgs e)
        {
            this.SetIndDiscountAmount();
        }

        private void SetIndDiscountAmount()
        {
            this.txtDiscount.Text = string.Empty;
            if (this.chk_ind_order.Checked)
            {
                var strCurrency = string.Empty;
                var strAmount = string.Empty;
                if (this.txtAmount_USD.Text.Trim().IsNumeric())
                {
                    strCurrency = "USD";
                    strAmount = this.txtAmount_USD.Text.Trim();
                }
                if (this.txtAmount_NTD.Text.Trim().IsNumeric())
                {
                    strCurrency = "NTD";
                    strAmount = this.txtAmount_NTD.Text.Trim();
                }
                if (this.txtAmount_RMB.Text.Trim().IsNumeric())
                {
                    strCurrency = "RMB";
                    strAmount = this.txtAmount_RMB.Text.Trim();
                }
                if (this.txtAmount_EUR.Text.Trim().IsNumeric())
                {
                    strCurrency = "EUR";
                    strAmount = this.txtAmount_EUR.Text.Trim();
                }
                if (this.txtAmount_AUD.Text.Trim().IsNumeric())
                {
                    strCurrency = "AUD";
                    strAmount = this.txtAmount_AUD.Text.Trim();
                }
                if (this.txtAmount_JPY.Text.Trim().IsNumeric())
                {
                    strCurrency = "JPY";
                    strAmount = this.txtAmount_JPY.Text.Trim();
                }
                if (this.txtAmount_NZD.Text.Trim().IsNumeric())
                {
                    strCurrency = "NZD";
                    strAmount = this.txtAmount_NZD.Text.Trim();
                }

                if (strCurrency != string.Empty && strAmount.IsNumeric())
                {
                    var data = base.Cust.GetIND_DISCOUNT_AMOUNT(strCurrency, strAmount);
                    if (data != null)
                    {
                        if (data.DiscountAmount != null)
                        {
                            this.txtDiscount.Text = data.DiscountAmount.Value.ConvertToString();
                        }
                    }
                }
            }
        }

        private void txtSales_Intermal_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                // this.txtIBCode.Text = string.Empty;
                this.txtSales_Intermal.Text = string.Empty;
                this.txtSales_Intermal.Tag = string.Empty;

                var data = base.Emp.GetBL_EMP();
                data = data.Where(x => x.INTERNAL_IB_CODE.ConvertToString().Trim() != string.Empty).ToList();
                data = data
                 .Where(x => x.INTERNAL_IB_CODE.ConvertToString().ToUpper().IndexOf(this.txtIBCode_Internal.Text.Trim().ToUpper()) != -1)
                 .ToList();
                if (data.Count != 0)
                {
                    this.txtIBCode_Internal.Text = data[0].INTERNAL_IB_CODE.ConvertToString();
                    this.txtSales_Intermal.Text = data[0].CNAME.ConvertToString();
                    this.txtSales_Intermal.Tag = data[0].EMP_CODE.ConvertToString();
                }
            }
        }

        private void btnBringSales_Internal_Click(object sender, EventArgs e)
        {
            var frm = new FrmSelectSales_Internal
                (
                    strORG_CODE: Global.ORG_CODE
                )
            { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
            frm.ShowDialog();
            if (frm.status == MK.Demo.Data.enCommon.enFormStatus.OK)
            {
                this.txtIBCode_Internal.Text = frm.strIB_CODE.ConvertToString();
                this.txtSales_Intermal.Text = frm.strCNAME.ConvertToString();
                this.txtSales_Intermal.Tag = frm.strEMP_CODE.ConvertToString();
            }
            frm.Dispose();
            frm = null;
        }
    }
}
