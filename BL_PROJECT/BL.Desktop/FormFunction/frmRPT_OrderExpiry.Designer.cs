﻿namespace MK_DEMO.Destop.FormFunction
{
    partial class frmRPT_OrderExpiry
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnExport = new System.Windows.Forms.Button();
            this.cboMonth = new System.Windows.Forms.ComboBox();
            this.cboYear = new System.Windows.Forms.ComboBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dgv_New = new System.Windows.Forms.DataGridView();
            this.ORDER_NO_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.newCNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.newSALES = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.newIB_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.newFROM_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.newEND_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.newEXTEND_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.newORDER_END = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.newNOTICE_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.newINTEREST_RATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.newYEAR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUST_ID_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ORDEREND_MONTH_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dgv_Old = new System.Windows.Forms.DataGridView();
            this.ORDER_NO_2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.oldCNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.oldSALES = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.oldIB_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.oldFROM_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.oldEND_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.oldEXTEND_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.oldORDER_END = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.oldNOTICE_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.oldINTEREST_RATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.oldYEAR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUST_ID_2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ORDEREND_MONTH_2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dgv_Old2 = new System.Windows.Forms.DataGridView();
            this.ORDER_NO_3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUST_ID_3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ORDEREND_MONTH_3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnSendEmail = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_New)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Old)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Old2)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微軟正黑體", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(16, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 19);
            this.label1.TabIndex = 127;
            this.label1.Text = "合約到期年月";
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("微軟正黑體", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnSearch.Location = new System.Drawing.Point(322, 9);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(2);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(76, 30);
            this.btnSearch.TabIndex = 126;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnExport
            // 
            this.btnExport.Font = new System.Drawing.Font("微軟正黑體", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnExport.Location = new System.Drawing.Point(424, 9);
            this.btnExport.Margin = new System.Windows.Forms.Padding(2);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(114, 30);
            this.btnExport.TabIndex = 129;
            this.btnExport.Text = "產生PDF檔";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // cboMonth
            // 
            this.cboMonth.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.cboMonth.FormattingEnabled = true;
            this.cboMonth.Location = new System.Drawing.Point(238, 12);
            this.cboMonth.Name = "cboMonth";
            this.cboMonth.Size = new System.Drawing.Size(67, 25);
            this.cboMonth.TabIndex = 131;
            // 
            // cboYear
            // 
            this.cboYear.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.cboYear.FormattingEnabled = true;
            this.cboYear.Location = new System.Drawing.Point(121, 12);
            this.cboYear.Name = "cboYear";
            this.cboYear.Size = new System.Drawing.Size(111, 25);
            this.cboYear.TabIndex = 130;
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(20, 43);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(854, 467);
            this.tabControl1.TabIndex = 132;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dgv_New);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(846, 434);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "新合約 [0]";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dgv_New
            // 
            this.dgv_New.AllowUserToAddRows = false;
            this.dgv_New.AllowUserToDeleteRows = false;
            this.dgv_New.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_New.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_New.ColumnHeadersHeight = 25;
            this.dgv_New.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv_New.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ORDER_NO_1,
            this.newCNAME,
            this.newSALES,
            this.newIB_CODE,
            this.newFROM_DATE,
            this.newEND_DATE,
            this.newEXTEND_DATE,
            this.newORDER_END,
            this.newNOTICE_DATE,
            this.newINTEREST_RATE,
            this.newYEAR,
            this.CUST_ID_1,
            this.ORDEREND_MONTH_1});
            this.dgv_New.Location = new System.Drawing.Point(6, 6);
            this.dgv_New.Name = "dgv_New";
            this.dgv_New.ReadOnly = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.RoyalBlue;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Yellow;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_New.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgv_New.RowHeadersWidth = 25;
            this.dgv_New.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgv_New.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(226)))), ((int)(((byte)(239)))));
            this.dgv_New.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_New.RowTemplate.Height = 24;
            this.dgv_New.Size = new System.Drawing.Size(834, 408);
            this.dgv_New.TabIndex = 1;
            // 
            // ORDER_NO_1
            // 
            this.ORDER_NO_1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ORDER_NO_1.DataPropertyName = "ORDER_NO";
            this.ORDER_NO_1.HeaderText = "ORDER_NO";
            this.ORDER_NO_1.Name = "ORDER_NO_1";
            this.ORDER_NO_1.ReadOnly = true;
            this.ORDER_NO_1.Width = 110;
            // 
            // newCNAME
            // 
            this.newCNAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.newCNAME.DataPropertyName = "CNAME";
            this.newCNAME.HeaderText = "客戶名稱";
            this.newCNAME.Name = "newCNAME";
            this.newCNAME.ReadOnly = true;
            this.newCNAME.Width = 170;
            // 
            // newSALES
            // 
            this.newSALES.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.newSALES.DataPropertyName = "SALES";
            this.newSALES.HeaderText = "業務姓名";
            this.newSALES.Name = "newSALES";
            this.newSALES.ReadOnly = true;
            // 
            // newIB_CODE
            // 
            this.newIB_CODE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.newIB_CODE.DataPropertyName = "IB_CODE";
            this.newIB_CODE.HeaderText = "IB_CODE";
            this.newIB_CODE.Name = "newIB_CODE";
            this.newIB_CODE.ReadOnly = true;
            this.newIB_CODE.Width = 110;
            // 
            // newFROM_DATE
            // 
            this.newFROM_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.newFROM_DATE.DataPropertyName = "FROM_DATE";
            this.newFROM_DATE.HeaderText = "FROM_DATE";
            this.newFROM_DATE.Name = "newFROM_DATE";
            this.newFROM_DATE.ReadOnly = true;
            this.newFROM_DATE.Width = 105;
            // 
            // newEND_DATE
            // 
            this.newEND_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.newEND_DATE.DataPropertyName = "END_DATE";
            this.newEND_DATE.HeaderText = "END_DATE";
            this.newEND_DATE.Name = "newEND_DATE";
            this.newEND_DATE.ReadOnly = true;
            this.newEND_DATE.Width = 105;
            // 
            // newEXTEND_DATE
            // 
            this.newEXTEND_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.newEXTEND_DATE.DataPropertyName = "EXTEND_DATE";
            this.newEXTEND_DATE.HeaderText = "EXTEND_DATE";
            this.newEXTEND_DATE.Name = "newEXTEND_DATE";
            this.newEXTEND_DATE.ReadOnly = true;
            this.newEXTEND_DATE.Width = 120;
            // 
            // newORDER_END
            // 
            this.newORDER_END.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.newORDER_END.DataPropertyName = "ORDER_END";
            this.newORDER_END.HeaderText = "ORDER_END";
            this.newORDER_END.Name = "newORDER_END";
            this.newORDER_END.ReadOnly = true;
            this.newORDER_END.Width = 105;
            // 
            // newNOTICE_DATE
            // 
            this.newNOTICE_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.newNOTICE_DATE.DataPropertyName = "NOTICE_DATE";
            this.newNOTICE_DATE.HeaderText = "通知日期";
            this.newNOTICE_DATE.Name = "newNOTICE_DATE";
            this.newNOTICE_DATE.ReadOnly = true;
            this.newNOTICE_DATE.Width = 105;
            // 
            // newINTEREST_RATE
            // 
            this.newINTEREST_RATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.newINTEREST_RATE.DataPropertyName = "INTEREST_RATE";
            this.newINTEREST_RATE.HeaderText = "INTEREST_RATE";
            this.newINTEREST_RATE.Name = "newINTEREST_RATE";
            this.newINTEREST_RATE.ReadOnly = true;
            this.newINTEREST_RATE.Width = 130;
            // 
            // newYEAR
            // 
            this.newYEAR.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.newYEAR.DataPropertyName = "YEAR";
            this.newYEAR.HeaderText = "YEAR";
            this.newYEAR.Name = "newYEAR";
            this.newYEAR.ReadOnly = true;
            this.newYEAR.Width = 70;
            // 
            // CUST_ID_1
            // 
            this.CUST_ID_1.DataPropertyName = "CUST_ID";
            this.CUST_ID_1.HeaderText = "CUST_ID";
            this.CUST_ID_1.Name = "CUST_ID_1";
            this.CUST_ID_1.ReadOnly = true;
            this.CUST_ID_1.Visible = false;
            // 
            // ORDEREND_MONTH_1
            // 
            this.ORDEREND_MONTH_1.DataPropertyName = "ORDEREND_MONTH";
            this.ORDEREND_MONTH_1.HeaderText = "ORDEREND_MONTH";
            this.ORDEREND_MONTH_1.Name = "ORDEREND_MONTH_1";
            this.ORDEREND_MONTH_1.ReadOnly = true;
            this.ORDEREND_MONTH_1.Visible = false;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dgv_Old);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(846, 441);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "舊合約-可續約 [0]";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dgv_Old
            // 
            this.dgv_Old.AllowUserToAddRows = false;
            this.dgv_Old.AllowUserToDeleteRows = false;
            this.dgv_Old.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_Old.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_Old.ColumnHeadersHeight = 25;
            this.dgv_Old.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv_Old.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ORDER_NO_2,
            this.oldCNAME,
            this.oldSALES,
            this.oldIB_CODE,
            this.oldFROM_DATE,
            this.oldEND_DATE,
            this.oldEXTEND_DATE,
            this.oldORDER_END,
            this.oldNOTICE_DATE,
            this.oldINTEREST_RATE,
            this.oldYEAR,
            this.CUST_ID_2,
            this.ORDEREND_MONTH_2});
            this.dgv_Old.Location = new System.Drawing.Point(6, 6);
            this.dgv_Old.Name = "dgv_Old";
            this.dgv_Old.ReadOnly = true;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.RoyalBlue;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Yellow;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_Old.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgv_Old.RowHeadersWidth = 25;
            this.dgv_Old.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgv_Old.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(226)))), ((int)(((byte)(239)))));
            this.dgv_Old.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_Old.RowTemplate.Height = 24;
            this.dgv_Old.Size = new System.Drawing.Size(834, 415);
            this.dgv_Old.TabIndex = 2;
            // 
            // ORDER_NO_2
            // 
            this.ORDER_NO_2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ORDER_NO_2.DataPropertyName = "ORDER_NO";
            this.ORDER_NO_2.HeaderText = "ORDER_NO";
            this.ORDER_NO_2.Name = "ORDER_NO_2";
            this.ORDER_NO_2.ReadOnly = true;
            this.ORDER_NO_2.Width = 110;
            // 
            // oldCNAME
            // 
            this.oldCNAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.oldCNAME.DataPropertyName = "CNAME";
            this.oldCNAME.HeaderText = "客戶名稱";
            this.oldCNAME.Name = "oldCNAME";
            this.oldCNAME.ReadOnly = true;
            this.oldCNAME.Width = 170;
            // 
            // oldSALES
            // 
            this.oldSALES.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.oldSALES.DataPropertyName = "SALES";
            this.oldSALES.HeaderText = "業務姓名";
            this.oldSALES.Name = "oldSALES";
            this.oldSALES.ReadOnly = true;
            // 
            // oldIB_CODE
            // 
            this.oldIB_CODE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.oldIB_CODE.DataPropertyName = "IB_CODE";
            this.oldIB_CODE.HeaderText = "IB_CODE";
            this.oldIB_CODE.Name = "oldIB_CODE";
            this.oldIB_CODE.ReadOnly = true;
            this.oldIB_CODE.Width = 110;
            // 
            // oldFROM_DATE
            // 
            this.oldFROM_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.oldFROM_DATE.DataPropertyName = "FROM_DATE";
            this.oldFROM_DATE.HeaderText = "FROM_DATE";
            this.oldFROM_DATE.Name = "oldFROM_DATE";
            this.oldFROM_DATE.ReadOnly = true;
            this.oldFROM_DATE.Width = 105;
            // 
            // oldEND_DATE
            // 
            this.oldEND_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.oldEND_DATE.DataPropertyName = "END_DATE";
            this.oldEND_DATE.HeaderText = "END_DATE";
            this.oldEND_DATE.Name = "oldEND_DATE";
            this.oldEND_DATE.ReadOnly = true;
            this.oldEND_DATE.Width = 105;
            // 
            // oldEXTEND_DATE
            // 
            this.oldEXTEND_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.oldEXTEND_DATE.DataPropertyName = "EXTEND_DATE";
            this.oldEXTEND_DATE.HeaderText = "EXTEND_DATE";
            this.oldEXTEND_DATE.Name = "oldEXTEND_DATE";
            this.oldEXTEND_DATE.ReadOnly = true;
            this.oldEXTEND_DATE.Width = 120;
            // 
            // oldORDER_END
            // 
            this.oldORDER_END.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.oldORDER_END.DataPropertyName = "ORDER_END";
            this.oldORDER_END.HeaderText = "ORDER_END";
            this.oldORDER_END.Name = "oldORDER_END";
            this.oldORDER_END.ReadOnly = true;
            this.oldORDER_END.Width = 105;
            // 
            // oldNOTICE_DATE
            // 
            this.oldNOTICE_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.oldNOTICE_DATE.DataPropertyName = "NOTICE_DATE";
            this.oldNOTICE_DATE.HeaderText = "通知日期";
            this.oldNOTICE_DATE.Name = "oldNOTICE_DATE";
            this.oldNOTICE_DATE.ReadOnly = true;
            this.oldNOTICE_DATE.Width = 105;
            // 
            // oldINTEREST_RATE
            // 
            this.oldINTEREST_RATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.oldINTEREST_RATE.DataPropertyName = "INTEREST_RATE";
            this.oldINTEREST_RATE.HeaderText = "INTEREST_RATE";
            this.oldINTEREST_RATE.Name = "oldINTEREST_RATE";
            this.oldINTEREST_RATE.ReadOnly = true;
            this.oldINTEREST_RATE.Width = 130;
            // 
            // oldYEAR
            // 
            this.oldYEAR.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.oldYEAR.DataPropertyName = "YEAR";
            this.oldYEAR.HeaderText = "YEAR";
            this.oldYEAR.Name = "oldYEAR";
            this.oldYEAR.ReadOnly = true;
            this.oldYEAR.Width = 70;
            // 
            // CUST_ID_2
            // 
            this.CUST_ID_2.DataPropertyName = "CUST_ID";
            this.CUST_ID_2.HeaderText = "CUST_ID";
            this.CUST_ID_2.Name = "CUST_ID_2";
            this.CUST_ID_2.ReadOnly = true;
            this.CUST_ID_2.Visible = false;
            // 
            // ORDEREND_MONTH_2
            // 
            this.ORDEREND_MONTH_2.DataPropertyName = "ORDEREND_MONTH";
            this.ORDEREND_MONTH_2.HeaderText = "ORDEREND_MONTH";
            this.ORDEREND_MONTH_2.Name = "ORDEREND_MONTH_2";
            this.ORDEREND_MONTH_2.ReadOnly = true;
            this.ORDEREND_MONTH_2.Visible = false;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.dgv_Old2);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(846, 441);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "舊合約-不可續約[0]";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // dgv_Old2
            // 
            this.dgv_Old2.AllowUserToAddRows = false;
            this.dgv_Old2.AllowUserToDeleteRows = false;
            this.dgv_Old2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_Old2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_Old2.ColumnHeadersHeight = 25;
            this.dgv_Old2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv_Old2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ORDER_NO_3,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.CUST_ID_3,
            this.ORDEREND_MONTH_3});
            this.dgv_Old2.Location = new System.Drawing.Point(6, 10);
            this.dgv_Old2.Name = "dgv_Old2";
            this.dgv_Old2.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.RoyalBlue;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Yellow;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_Old2.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgv_Old2.RowHeadersWidth = 25;
            this.dgv_Old2.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgv_Old2.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(226)))), ((int)(((byte)(239)))));
            this.dgv_Old2.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_Old2.RowTemplate.Height = 24;
            this.dgv_Old2.Size = new System.Drawing.Size(834, 415);
            this.dgv_Old2.TabIndex = 3;
            // 
            // ORDER_NO_3
            // 
            this.ORDER_NO_3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ORDER_NO_3.DataPropertyName = "ORDER_NO";
            this.ORDER_NO_3.HeaderText = "ORDER_NO";
            this.ORDER_NO_3.Name = "ORDER_NO_3";
            this.ORDER_NO_3.ReadOnly = true;
            this.ORDER_NO_3.Width = 110;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "CNAME";
            this.dataGridViewTextBoxColumn2.HeaderText = "客戶名稱";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 170;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "SALES";
            this.dataGridViewTextBoxColumn3.HeaderText = "業務姓名";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "IB_CODE";
            this.dataGridViewTextBoxColumn4.HeaderText = "IB_CODE";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 110;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn5.DataPropertyName = "FROM_DATE";
            this.dataGridViewTextBoxColumn5.HeaderText = "FROM_DATE";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 105;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn6.DataPropertyName = "END_DATE";
            this.dataGridViewTextBoxColumn6.HeaderText = "END_DATE";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 105;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn7.DataPropertyName = "EXTEND_DATE";
            this.dataGridViewTextBoxColumn7.HeaderText = "EXTEND_DATE";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 120;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn8.DataPropertyName = "ORDER_END";
            this.dataGridViewTextBoxColumn8.HeaderText = "ORDER_END";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Width = 105;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn9.DataPropertyName = "NOTICE_DATE";
            this.dataGridViewTextBoxColumn9.HeaderText = "通知日期";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Width = 105;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn10.DataPropertyName = "INTEREST_RATE";
            this.dataGridViewTextBoxColumn10.HeaderText = "INTEREST_RATE";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.Width = 130;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn11.DataPropertyName = "YEAR";
            this.dataGridViewTextBoxColumn11.HeaderText = "YEAR";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            this.dataGridViewTextBoxColumn11.Width = 70;
            // 
            // CUST_ID_3
            // 
            this.CUST_ID_3.DataPropertyName = "CUST_ID";
            this.CUST_ID_3.HeaderText = "CUST_ID";
            this.CUST_ID_3.Name = "CUST_ID_3";
            this.CUST_ID_3.ReadOnly = true;
            this.CUST_ID_3.Visible = false;
            // 
            // ORDEREND_MONTH_3
            // 
            this.ORDEREND_MONTH_3.DataPropertyName = "ORDEREND_MONTH";
            this.ORDEREND_MONTH_3.HeaderText = "ORDEREND_MONTH";
            this.ORDEREND_MONTH_3.Name = "ORDEREND_MONTH_3";
            this.ORDEREND_MONTH_3.ReadOnly = true;
            this.ORDEREND_MONTH_3.Visible = false;
            // 
            // btnSendEmail
            // 
            this.btnSendEmail.Font = new System.Drawing.Font("微軟正黑體", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnSendEmail.Location = new System.Drawing.Point(565, 9);
            this.btnSendEmail.Margin = new System.Windows.Forms.Padding(2);
            this.btnSendEmail.Name = "btnSendEmail";
            this.btnSendEmail.Size = new System.Drawing.Size(210, 30);
            this.btnSendEmail.TabIndex = 133;
            this.btnSendEmail.Text = "發送 PDF 檔";
            this.btnSendEmail.UseVisualStyleBackColor = true;
            this.btnSendEmail.Click += new System.EventHandler(this.btnSendEmail_Click);
            // 
            // frmRPT_OrderExpiry
            // 
            this.ClientSize = new System.Drawing.Size(886, 522);
            this.Controls.Add(this.btnSendEmail);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.cboMonth);
            this.Controls.Add(this.cboYear);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSearch);
            this.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Name = "frmRPT_OrderExpiry";
            this.Text = "到期通知書";
            this.Load += new System.EventHandler(this.frmRPT_OrderExpiry_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_New)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Old)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Old2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.ComboBox cboMonth;
        private System.Windows.Forms.ComboBox cboYear;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dgv_New;
        private System.Windows.Forms.DataGridView dgv_Old;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView dgv_Old2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ORDER_NO_1;
        private System.Windows.Forms.DataGridViewTextBoxColumn newCNAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn newSALES;
        private System.Windows.Forms.DataGridViewTextBoxColumn newIB_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn newFROM_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn newEND_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn newEXTEND_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn newORDER_END;
        private System.Windows.Forms.DataGridViewTextBoxColumn newNOTICE_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn newINTEREST_RATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn newYEAR;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUST_ID_1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ORDEREND_MONTH_1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ORDER_NO_2;
        private System.Windows.Forms.DataGridViewTextBoxColumn oldCNAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn oldSALES;
        private System.Windows.Forms.DataGridViewTextBoxColumn oldIB_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn oldFROM_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn oldEND_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn oldEXTEND_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn oldORDER_END;
        private System.Windows.Forms.DataGridViewTextBoxColumn oldNOTICE_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn oldINTEREST_RATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn oldYEAR;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUST_ID_2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ORDEREND_MONTH_2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ORDER_NO_3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUST_ID_3;
        private System.Windows.Forms.DataGridViewTextBoxColumn ORDEREND_MONTH_3;
        private System.Windows.Forms.Button btnSendEmail;
    }
}
