﻿namespace MK_DEMO.Destop.FormFunction
{
    partial class frmContractApproveDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmContractApproveDetail));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            this.picLoder = new System.Windows.Forms.PictureBox();
            this.btn_send = new System.Windows.Forms.Button();
            this.btn_close = new System.Windows.Forms.Button();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.dgv_header = new System.Windows.Forms.DataGridView();
            this.check = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ORDER_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CONTRACT_STATUS = new System.Windows.Forms.DataGridViewLinkColumn();
            this.CUST_CNAME = new System.Windows.Forms.DataGridViewLinkColumn();
            this.CUST_ENAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SALES = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IB_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT_USD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT_NTD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT_RMB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT_EUR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT_AUD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT_JPY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT_NZD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INTEREST_USD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INTEREST_NTD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INTEREST_RMB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INTEREST_EUR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INTEREST_AUD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INTEREST_JPY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INTEREST_NZD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FROM_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.END_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.YEAR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ORDER_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUST_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.STATUS_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colFROM_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.REJECT_REASON = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ORG_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EMAIL_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EMAIL_2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EMAIL2_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EMAIL2_2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SALES_EMAIL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_TW1 = new System.Windows.Forms.Button();
            this.btn_IN = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.picLoder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_header)).BeginInit();
            this.SuspendLayout();
            // 
            // picLoder
            // 
            this.picLoder.BackColor = System.Drawing.Color.Transparent;
            this.picLoder.Image = ((System.Drawing.Image)(resources.GetObject("picLoder.Image")));
            this.picLoder.Location = new System.Drawing.Point(763, 64);
            this.picLoder.Name = "picLoder";
            this.picLoder.Size = new System.Drawing.Size(174, 19);
            this.picLoder.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picLoder.TabIndex = 115;
            this.picLoder.TabStop = false;
            this.picLoder.Visible = false;
            // 
            // btn_send
            // 
            this.btn_send.Font = new System.Drawing.Font("微軟正黑體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btn_send.Location = new System.Drawing.Point(353, 437);
            this.btn_send.Name = "btn_send";
            this.btn_send.Size = new System.Drawing.Size(130, 40);
            this.btn_send.TabIndex = 1;
            this.btn_send.Text = "送出通知";
            this.btn_send.UseVisualStyleBackColor = true;
            this.btn_send.Click += new System.EventHandler(this.btn_send_Click);
            // 
            // btn_close
            // 
            this.btn_close.Font = new System.Drawing.Font("微軟正黑體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btn_close.Location = new System.Drawing.Point(489, 437);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(130, 40);
            this.btn_close.TabIndex = 2;
            this.btn_close.Text = "取消";
            this.btn_close.UseVisualStyleBackColor = true;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // dgv_header
            // 
            this.dgv_header.AllowUserToAddRows = false;
            this.dgv_header.AllowUserToDeleteRows = false;
            this.dgv_header.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_header.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_header.ColumnHeadersHeight = 25;
            this.dgv_header.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv_header.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.check,
            this.ORDER_NO,
            this.CONTRACT_STATUS,
            this.CUST_CNAME,
            this.CUST_ENAME,
            this.SALES,
            this.IB_CODE,
            this.AMOUNT_USD,
            this.AMOUNT_NTD,
            this.AMOUNT_RMB,
            this.AMOUNT_EUR,
            this.AMOUNT_AUD,
            this.AMOUNT_JPY,
            this.AMOUNT_NZD,
            this.INTEREST_USD,
            this.INTEREST_NTD,
            this.INTEREST_RMB,
            this.INTEREST_EUR,
            this.INTEREST_AUD,
            this.INTEREST_JPY,
            this.INTEREST_NZD,
            this.FROM_DATE,
            this.END_DATE,
            this.YEAR,
            this.ORDER_ID,
            this.CUST_ID,
            this.STATUS_CODE,
            this.colFROM_DATE,
            this.REJECT_REASON,
            this.ORG_CODE,
            this.EMAIL_1,
            this.EMAIL_2,
            this.EMAIL2_1,
            this.EMAIL2_2,
            this.SALES_EMAIL});
            this.dgv_header.Location = new System.Drawing.Point(6, 36);
            this.dgv_header.Name = "dgv_header";
            this.dgv_header.ReadOnly = true;
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle32.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle32.Font = new System.Drawing.Font("微軟正黑體", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            dataGridViewCellStyle32.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle32.SelectionBackColor = System.Drawing.Color.RoyalBlue;
            dataGridViewCellStyle32.SelectionForeColor = System.Drawing.Color.Yellow;
            dataGridViewCellStyle32.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_header.RowHeadersDefaultCellStyle = dataGridViewCellStyle32;
            this.dgv_header.RowHeadersWidth = 25;
            this.dgv_header.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgv_header.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(226)))), ((int)(((byte)(239)))));
            this.dgv_header.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_header.RowTemplate.Height = 24;
            this.dgv_header.Size = new System.Drawing.Size(948, 395);
            this.dgv_header.TabIndex = 116;
            this.dgv_header.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_header_CellClick);
            // 
            // check
            // 
            this.check.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.check.FillWeight = 25F;
            this.check.HeaderText = "選";
            this.check.MinimumWidth = 25;
            this.check.Name = "check";
            this.check.ReadOnly = true;
            this.check.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.check.Width = 25;
            // 
            // ORDER_NO
            // 
            this.ORDER_NO.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ORDER_NO.DataPropertyName = "ORDER_NO";
            this.ORDER_NO.HeaderText = "合約編號";
            this.ORDER_NO.MinimumWidth = 6;
            this.ORDER_NO.Name = "ORDER_NO";
            this.ORDER_NO.ReadOnly = true;
            this.ORDER_NO.Width = 90;
            // 
            // CONTRACT_STATUS
            // 
            this.CONTRACT_STATUS.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.CONTRACT_STATUS.DataPropertyName = "GetSTATUS";
            this.CONTRACT_STATUS.HeaderText = "合約狀態";
            this.CONTRACT_STATUS.MinimumWidth = 6;
            this.CONTRACT_STATUS.Name = "CONTRACT_STATUS";
            this.CONTRACT_STATUS.ReadOnly = true;
            this.CONTRACT_STATUS.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.CONTRACT_STATUS.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.CONTRACT_STATUS.Width = 90;
            // 
            // CUST_CNAME
            // 
            this.CUST_CNAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.CUST_CNAME.DataPropertyName = "CUST_CNAME";
            this.CUST_CNAME.HeaderText = "姓名";
            this.CUST_CNAME.MinimumWidth = 6;
            this.CUST_CNAME.Name = "CUST_CNAME";
            this.CUST_CNAME.ReadOnly = true;
            this.CUST_CNAME.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.CUST_CNAME.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.CUST_CNAME.Width = 130;
            // 
            // CUST_ENAME
            // 
            this.CUST_ENAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.CUST_ENAME.DataPropertyName = "CUST_ENAME";
            this.CUST_ENAME.HeaderText = "英文姓名";
            this.CUST_ENAME.MinimumWidth = 6;
            this.CUST_ENAME.Name = "CUST_ENAME";
            this.CUST_ENAME.ReadOnly = true;
            this.CUST_ENAME.Width = 220;
            // 
            // SALES
            // 
            this.SALES.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.SALES.DataPropertyName = "SALES_NAME";
            this.SALES.HeaderText = "業務";
            this.SALES.MinimumWidth = 6;
            this.SALES.Name = "SALES";
            this.SALES.ReadOnly = true;
            this.SALES.Width = 130;
            // 
            // IB_CODE
            // 
            this.IB_CODE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.IB_CODE.DataPropertyName = "IB_CODE";
            this.IB_CODE.HeaderText = "IB_CODE";
            this.IB_CODE.MinimumWidth = 100;
            this.IB_CODE.Name = "IB_CODE";
            this.IB_CODE.ReadOnly = true;
            this.IB_CODE.Width = 125;
            // 
            // AMOUNT_USD
            // 
            this.AMOUNT_USD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.AMOUNT_USD.DataPropertyName = "AMOUNT_USD";
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle17.Format = "N2";
            dataGridViewCellStyle17.NullValue = null;
            this.AMOUNT_USD.DefaultCellStyle = dataGridViewCellStyle17;
            this.AMOUNT_USD.HeaderText = "入金金額(美元)";
            this.AMOUNT_USD.MinimumWidth = 6;
            this.AMOUNT_USD.Name = "AMOUNT_USD";
            this.AMOUNT_USD.ReadOnly = true;
            this.AMOUNT_USD.Width = 150;
            // 
            // AMOUNT_NTD
            // 
            this.AMOUNT_NTD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.AMOUNT_NTD.DataPropertyName = "AMOUNT_NTD";
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle18.Format = "N2";
            dataGridViewCellStyle18.NullValue = null;
            this.AMOUNT_NTD.DefaultCellStyle = dataGridViewCellStyle18;
            this.AMOUNT_NTD.HeaderText = "入金金額(新台幣)";
            this.AMOUNT_NTD.MinimumWidth = 6;
            this.AMOUNT_NTD.Name = "AMOUNT_NTD";
            this.AMOUNT_NTD.ReadOnly = true;
            this.AMOUNT_NTD.Width = 150;
            // 
            // AMOUNT_RMB
            // 
            this.AMOUNT_RMB.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.AMOUNT_RMB.DataPropertyName = "AMOUNT_RMB";
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle19.Format = "N2";
            dataGridViewCellStyle19.NullValue = null;
            this.AMOUNT_RMB.DefaultCellStyle = dataGridViewCellStyle19;
            this.AMOUNT_RMB.HeaderText = "入金金額(人民幣)";
            this.AMOUNT_RMB.MinimumWidth = 6;
            this.AMOUNT_RMB.Name = "AMOUNT_RMB";
            this.AMOUNT_RMB.ReadOnly = true;
            this.AMOUNT_RMB.Width = 150;
            // 
            // AMOUNT_EUR
            // 
            this.AMOUNT_EUR.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.AMOUNT_EUR.DataPropertyName = "AMOUNT_EUR";
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle20.Format = "N2";
            this.AMOUNT_EUR.DefaultCellStyle = dataGridViewCellStyle20;
            this.AMOUNT_EUR.HeaderText = "入金金額(歐元)";
            this.AMOUNT_EUR.MinimumWidth = 6;
            this.AMOUNT_EUR.Name = "AMOUNT_EUR";
            this.AMOUNT_EUR.ReadOnly = true;
            this.AMOUNT_EUR.Width = 150;
            // 
            // AMOUNT_AUD
            // 
            this.AMOUNT_AUD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.AMOUNT_AUD.DataPropertyName = "AMOUNT_AUD";
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle21.Format = "N2";
            this.AMOUNT_AUD.DefaultCellStyle = dataGridViewCellStyle21;
            this.AMOUNT_AUD.HeaderText = "入金金額(澳幣)";
            this.AMOUNT_AUD.MinimumWidth = 6;
            this.AMOUNT_AUD.Name = "AMOUNT_AUD";
            this.AMOUNT_AUD.ReadOnly = true;
            this.AMOUNT_AUD.Width = 150;
            // 
            // AMOUNT_JPY
            // 
            this.AMOUNT_JPY.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.AMOUNT_JPY.DataPropertyName = "AMOUNT_JPY";
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle22.Format = "N2";
            this.AMOUNT_JPY.DefaultCellStyle = dataGridViewCellStyle22;
            this.AMOUNT_JPY.HeaderText = "入金金額(日幣)";
            this.AMOUNT_JPY.MinimumWidth = 6;
            this.AMOUNT_JPY.Name = "AMOUNT_JPY";
            this.AMOUNT_JPY.ReadOnly = true;
            this.AMOUNT_JPY.Width = 150;
            // 
            // AMOUNT_NZD
            // 
            this.AMOUNT_NZD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.AMOUNT_NZD.DataPropertyName = "AMOUNT_NZD";
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle23.Format = "N2";
            this.AMOUNT_NZD.DefaultCellStyle = dataGridViewCellStyle23;
            this.AMOUNT_NZD.HeaderText = "入金金額(紐幣)";
            this.AMOUNT_NZD.MinimumWidth = 6;
            this.AMOUNT_NZD.Name = "AMOUNT_NZD";
            this.AMOUNT_NZD.ReadOnly = true;
            this.AMOUNT_NZD.Width = 150;
            // 
            // INTEREST_USD
            // 
            this.INTEREST_USD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.INTEREST_USD.DataPropertyName = "INTEREST_USD";
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle24.Format = "N2";
            dataGridViewCellStyle24.NullValue = null;
            this.INTEREST_USD.DefaultCellStyle = dataGridViewCellStyle24;
            this.INTEREST_USD.HeaderText = "利息(美金)";
            this.INTEREST_USD.MinimumWidth = 95;
            this.INTEREST_USD.Name = "INTEREST_USD";
            this.INTEREST_USD.ReadOnly = true;
            this.INTEREST_USD.Width = 115;
            // 
            // INTEREST_NTD
            // 
            this.INTEREST_NTD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.INTEREST_NTD.DataPropertyName = "INTEREST_NTD";
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle25.Format = "N2";
            dataGridViewCellStyle25.NullValue = null;
            this.INTEREST_NTD.DefaultCellStyle = dataGridViewCellStyle25;
            this.INTEREST_NTD.HeaderText = "利息(新台幣)";
            this.INTEREST_NTD.MinimumWidth = 95;
            this.INTEREST_NTD.Name = "INTEREST_NTD";
            this.INTEREST_NTD.ReadOnly = true;
            this.INTEREST_NTD.Width = 120;
            // 
            // INTEREST_RMB
            // 
            this.INTEREST_RMB.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.INTEREST_RMB.DataPropertyName = "INTEREST_RMB";
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle26.Format = "N2";
            dataGridViewCellStyle26.NullValue = null;
            this.INTEREST_RMB.DefaultCellStyle = dataGridViewCellStyle26;
            this.INTEREST_RMB.HeaderText = "利息(人民幣)";
            this.INTEREST_RMB.MinimumWidth = 95;
            this.INTEREST_RMB.Name = "INTEREST_RMB";
            this.INTEREST_RMB.ReadOnly = true;
            this.INTEREST_RMB.Width = 120;
            // 
            // INTEREST_EUR
            // 
            this.INTEREST_EUR.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.INTEREST_EUR.DataPropertyName = "INTEREST_EUR";
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle27.Format = "N2";
            this.INTEREST_EUR.DefaultCellStyle = dataGridViewCellStyle27;
            this.INTEREST_EUR.HeaderText = "利息(歐元)";
            this.INTEREST_EUR.MinimumWidth = 6;
            this.INTEREST_EUR.Name = "INTEREST_EUR";
            this.INTEREST_EUR.ReadOnly = true;
            this.INTEREST_EUR.Width = 150;
            // 
            // INTEREST_AUD
            // 
            this.INTEREST_AUD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.INTEREST_AUD.DataPropertyName = "INTEREST_AUD";
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle28.Format = "N2";
            this.INTEREST_AUD.DefaultCellStyle = dataGridViewCellStyle28;
            this.INTEREST_AUD.HeaderText = "利息(澳幣)";
            this.INTEREST_AUD.MinimumWidth = 6;
            this.INTEREST_AUD.Name = "INTEREST_AUD";
            this.INTEREST_AUD.ReadOnly = true;
            this.INTEREST_AUD.Width = 150;
            // 
            // INTEREST_JPY
            // 
            this.INTEREST_JPY.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.INTEREST_JPY.DataPropertyName = "INTEREST_JPY";
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle29.Format = "N2";
            this.INTEREST_JPY.DefaultCellStyle = dataGridViewCellStyle29;
            this.INTEREST_JPY.HeaderText = "利息(日幣)";
            this.INTEREST_JPY.MinimumWidth = 6;
            this.INTEREST_JPY.Name = "INTEREST_JPY";
            this.INTEREST_JPY.ReadOnly = true;
            this.INTEREST_JPY.Width = 150;
            // 
            // INTEREST_NZD
            // 
            this.INTEREST_NZD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.INTEREST_NZD.DataPropertyName = "INTEREST_NZD";
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle30.Format = "N2";
            this.INTEREST_NZD.DefaultCellStyle = dataGridViewCellStyle30;
            this.INTEREST_NZD.HeaderText = "利息(紐幣)";
            this.INTEREST_NZD.MinimumWidth = 6;
            this.INTEREST_NZD.Name = "INTEREST_NZD";
            this.INTEREST_NZD.ReadOnly = true;
            this.INTEREST_NZD.Width = 150;
            // 
            // FROM_DATE
            // 
            this.FROM_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.FROM_DATE.DataPropertyName = "FROM_DATE";
            this.FROM_DATE.HeaderText = "合約日期";
            this.FROM_DATE.MinimumWidth = 6;
            this.FROM_DATE.Name = "FROM_DATE";
            this.FROM_DATE.ReadOnly = true;
            this.FROM_DATE.Width = 110;
            // 
            // END_DATE
            // 
            this.END_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.END_DATE.DataPropertyName = "END_DATE";
            this.END_DATE.HeaderText = "合約結束日期";
            this.END_DATE.MinimumWidth = 6;
            this.END_DATE.Name = "END_DATE";
            this.END_DATE.ReadOnly = true;
            this.END_DATE.Width = 120;
            // 
            // YEAR
            // 
            this.YEAR.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.YEAR.DataPropertyName = "YEAR";
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.YEAR.DefaultCellStyle = dataGridViewCellStyle31;
            this.YEAR.HeaderText = "年期";
            this.YEAR.MinimumWidth = 6;
            this.YEAR.Name = "YEAR";
            this.YEAR.ReadOnly = true;
            this.YEAR.Width = 90;
            // 
            // ORDER_ID
            // 
            this.ORDER_ID.DataPropertyName = "ORDER_ID";
            this.ORDER_ID.HeaderText = "ORDER_ID";
            this.ORDER_ID.MinimumWidth = 6;
            this.ORDER_ID.Name = "ORDER_ID";
            this.ORDER_ID.ReadOnly = true;
            this.ORDER_ID.Visible = false;
            // 
            // CUST_ID
            // 
            this.CUST_ID.DataPropertyName = "CUST_ID";
            this.CUST_ID.HeaderText = "CUST_ID";
            this.CUST_ID.MinimumWidth = 6;
            this.CUST_ID.Name = "CUST_ID";
            this.CUST_ID.ReadOnly = true;
            this.CUST_ID.Visible = false;
            // 
            // STATUS_CODE
            // 
            this.STATUS_CODE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.STATUS_CODE.DataPropertyName = "STATUS_CODE";
            this.STATUS_CODE.HeaderText = "STATUS_CODE";
            this.STATUS_CODE.MinimumWidth = 6;
            this.STATUS_CODE.Name = "STATUS_CODE";
            this.STATUS_CODE.ReadOnly = true;
            this.STATUS_CODE.Visible = false;
            this.STATUS_CODE.Width = 10;
            // 
            // colFROM_DATE
            // 
            this.colFROM_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.colFROM_DATE.DataPropertyName = "FROM_DATE";
            this.colFROM_DATE.HeaderText = "FROM_DATE";
            this.colFROM_DATE.MinimumWidth = 6;
            this.colFROM_DATE.Name = "colFROM_DATE";
            this.colFROM_DATE.ReadOnly = true;
            this.colFROM_DATE.Width = 130;
            // 
            // REJECT_REASON
            // 
            this.REJECT_REASON.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.REJECT_REASON.DataPropertyName = "REJECT_REASON";
            this.REJECT_REASON.HeaderText = "退件原因";
            this.REJECT_REASON.MinimumWidth = 6;
            this.REJECT_REASON.Name = "REJECT_REASON";
            this.REJECT_REASON.ReadOnly = true;
            this.REJECT_REASON.Width = 250;
            // 
            // ORG_CODE
            // 
            this.ORG_CODE.DataPropertyName = "ORG_CODE";
            this.ORG_CODE.HeaderText = "ORG_CODE";
            this.ORG_CODE.MinimumWidth = 6;
            this.ORG_CODE.Name = "ORG_CODE";
            this.ORG_CODE.ReadOnly = true;
            this.ORG_CODE.Visible = false;
            // 
            // EMAIL_1
            // 
            this.EMAIL_1.DataPropertyName = "EMAIL_1";
            this.EMAIL_1.HeaderText = "EMAIL_1";
            this.EMAIL_1.MinimumWidth = 6;
            this.EMAIL_1.Name = "EMAIL_1";
            this.EMAIL_1.ReadOnly = true;
            this.EMAIL_1.Visible = false;
            // 
            // EMAIL_2
            // 
            this.EMAIL_2.DataPropertyName = "EMAIL_2";
            this.EMAIL_2.HeaderText = "EMAIL_2";
            this.EMAIL_2.MinimumWidth = 6;
            this.EMAIL_2.Name = "EMAIL_2";
            this.EMAIL_2.ReadOnly = true;
            this.EMAIL_2.Visible = false;
            // 
            // EMAIL2_1
            // 
            this.EMAIL2_1.DataPropertyName = "EMAIL2_1";
            this.EMAIL2_1.HeaderText = "EMAIL2_1";
            this.EMAIL2_1.MinimumWidth = 6;
            this.EMAIL2_1.Name = "EMAIL2_1";
            this.EMAIL2_1.ReadOnly = true;
            this.EMAIL2_1.Visible = false;
            // 
            // EMAIL2_2
            // 
            this.EMAIL2_2.DataPropertyName = "EMAIL2_2";
            this.EMAIL2_2.HeaderText = "EMAIL2_2";
            this.EMAIL2_2.MinimumWidth = 6;
            this.EMAIL2_2.Name = "EMAIL2_2";
            this.EMAIL2_2.ReadOnly = true;
            this.EMAIL2_2.Visible = false;
            // 
            // SALES_EMAIL
            // 
            this.SALES_EMAIL.DataPropertyName = "SALES_EMAIL";
            this.SALES_EMAIL.HeaderText = "SALES_EMAIL";
            this.SALES_EMAIL.MinimumWidth = 6;
            this.SALES_EMAIL.Name = "SALES_EMAIL";
            this.SALES_EMAIL.ReadOnly = true;
            this.SALES_EMAIL.Visible = false;
            // 
            // btn_TW1
            // 
            this.btn_TW1.Location = new System.Drawing.Point(6, 3);
            this.btn_TW1.Name = "btn_TW1";
            this.btn_TW1.Size = new System.Drawing.Size(100, 34);
            this.btn_TW1.TabIndex = 117;
            this.btn_TW1.Text = "台北新客戶";
            this.btn_TW1.UseVisualStyleBackColor = true;
            this.btn_TW1.Click += new System.EventHandler(this.btn_TW1_Click);
            // 
            // btn_IN
            // 
            this.btn_IN.Location = new System.Drawing.Point(103, 3);
            this.btn_IN.Name = "btn_IN";
            this.btn_IN.Size = new System.Drawing.Size(100, 34);
            this.btn_IN.TabIndex = 118;
            this.btn_IN.Text = "印尼新客戶";
            this.btn_IN.UseVisualStyleBackColor = true;
            this.btn_IN.Click += new System.EventHandler(this.btn_IN_Click);
            // 
            // frmContractApproveDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(949, 489);
            this.Controls.Add(this.btn_IN);
            this.Controls.Add(this.btn_TW1);
            this.Controls.Add(this.dgv_header);
            this.Controls.Add(this.picLoder);
            this.Controls.Add(this.btn_close);
            this.Controls.Add(this.btn_send);
            this.Name = "frmContractApproveDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmContractApproveDetail";
            ((System.ComponentModel.ISupportInitialize)(this.picLoder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_header)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btn_send;
        private System.Windows.Forms.Button btn_close;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.PictureBox picLoder;
        private System.Windows.Forms.DataGridView dgv_header;
        private System.Windows.Forms.DataGridViewCheckBoxColumn check;
        private System.Windows.Forms.DataGridViewTextBoxColumn ORDER_NO;
        private System.Windows.Forms.DataGridViewLinkColumn CONTRACT_STATUS;
        private System.Windows.Forms.DataGridViewLinkColumn CUST_CNAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUST_ENAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn SALES;
        private System.Windows.Forms.DataGridViewTextBoxColumn IB_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn AMOUNT_USD;
        private System.Windows.Forms.DataGridViewTextBoxColumn AMOUNT_NTD;
        private System.Windows.Forms.DataGridViewTextBoxColumn AMOUNT_RMB;
        private System.Windows.Forms.DataGridViewTextBoxColumn AMOUNT_EUR;
        private System.Windows.Forms.DataGridViewTextBoxColumn AMOUNT_AUD;
        private System.Windows.Forms.DataGridViewTextBoxColumn AMOUNT_JPY;
        private System.Windows.Forms.DataGridViewTextBoxColumn AMOUNT_NZD;
        private System.Windows.Forms.DataGridViewTextBoxColumn INTEREST_USD;
        private System.Windows.Forms.DataGridViewTextBoxColumn INTEREST_NTD;
        private System.Windows.Forms.DataGridViewTextBoxColumn INTEREST_RMB;
        private System.Windows.Forms.DataGridViewTextBoxColumn INTEREST_EUR;
        private System.Windows.Forms.DataGridViewTextBoxColumn INTEREST_AUD;
        private System.Windows.Forms.DataGridViewTextBoxColumn INTEREST_JPY;
        private System.Windows.Forms.DataGridViewTextBoxColumn INTEREST_NZD;
        private System.Windows.Forms.DataGridViewTextBoxColumn FROM_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn END_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn YEAR;
        private System.Windows.Forms.DataGridViewTextBoxColumn ORDER_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUST_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn STATUS_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFROM_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn REJECT_REASON;
        private System.Windows.Forms.DataGridViewTextBoxColumn ORG_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMAIL_1;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMAIL_2;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMAIL2_1;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMAIL2_2;
        private System.Windows.Forms.DataGridViewTextBoxColumn SALES_EMAIL;
        private System.Windows.Forms.Button btn_TW1;
        private System.Windows.Forms.Button btn_IN;
    }
}