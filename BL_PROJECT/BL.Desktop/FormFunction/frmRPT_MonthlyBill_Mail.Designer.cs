﻿namespace MK_DEMO.Destop.FormFunction
{
    partial class frmRPT_MonthlyBill_Mail
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.dgv_header = new System.Windows.Forms.DataGridView();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.lblBatchNo = new System.Windows.Forms.Label();
            this.cboBatchNo = new System.Windows.Forms.ComboBox();
            this.chkIsSendTest = new System.Windows.Forms.CheckBox();
            this.cboSales = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.check = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.CUST_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MAIL_TYPE_END = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FILE_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EMAIL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FILE_PATH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUST_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DATA_YM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IB_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SALES_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.JOB_STATUS_DESC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.JOB_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUST_NAME2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_header)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSubmit
            // 
            this.btnSubmit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSubmit.Location = new System.Drawing.Point(386, 450);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(147, 48);
            this.btnSubmit.TabIndex = 11;
            this.btnSubmit.Text = "發送Mail";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(552, 450);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(147, 48);
            this.btnClose.TabIndex = 10;
            this.btnClose.Text = "關閉";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // dgv_header
            // 
            this.dgv_header.AllowUserToAddRows = false;
            this.dgv_header.AllowUserToDeleteRows = false;
            this.dgv_header.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_header.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_header.ColumnHeadersHeight = 25;
            this.dgv_header.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv_header.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.check,
            this.CUST_NAME,
            this.MAIL_TYPE_END,
            this.FILE_NAME,
            this.EMAIL,
            this.FILE_PATH,
            this.CUST_ID,
            this.DATA_YM,
            this.IB_CODE,
            this.SALES_NAME,
            this.JOB_STATUS_DESC,
            this.JOB_ID,
            this.CUST_NAME2});
            this.dgv_header.Location = new System.Drawing.Point(12, 44);
            this.dgv_header.Name = "dgv_header";
            this.dgv_header.ReadOnly = true;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("新細明體", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.RoyalBlue;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Yellow;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_header.RowHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_header.RowHeadersWidth = 25;
            this.dgv_header.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgv_header.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(226)))), ((int)(((byte)(239)))));
            this.dgv_header.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_header.RowTemplate.Height = 24;
            this.dgv_header.Size = new System.Drawing.Size(1074, 400);
            this.dgv_header.TabIndex = 12;
            this.dgv_header.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_header_CellClick);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(42, 53);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(15, 14);
            this.checkBox1.TabIndex = 13;
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // lblBatchNo
            // 
            this.lblBatchNo.AutoSize = true;
            this.lblBatchNo.Font = new System.Drawing.Font("微軟正黑體", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lblBatchNo.Location = new System.Drawing.Point(21, 12);
            this.lblBatchNo.Name = "lblBatchNo";
            this.lblBatchNo.Size = new System.Drawing.Size(69, 19);
            this.lblBatchNo.TabIndex = 14;
            this.lblBatchNo.Text = "發送批號";
            // 
            // cboBatchNo
            // 
            this.cboBatchNo.FormattingEnabled = true;
            this.cboBatchNo.Location = new System.Drawing.Point(97, 10);
            this.cboBatchNo.Name = "cboBatchNo";
            this.cboBatchNo.Size = new System.Drawing.Size(150, 23);
            this.cboBatchNo.TabIndex = 133;
            this.cboBatchNo.SelectedIndexChanged += new System.EventHandler(this.cboBatchNo_SelectedIndexChanged);
            // 
            // chkIsSendTest
            // 
            this.chkIsSendTest.AutoSize = true;
            this.chkIsSendTest.Location = new System.Drawing.Point(591, 12);
            this.chkIsSendTest.Name = "chkIsSendTest";
            this.chkIsSendTest.Size = new System.Drawing.Size(101, 19);
            this.chkIsSendTest.TabIndex = 134;
            this.chkIsSendTest.Text = "發送測試信";
            this.chkIsSendTest.UseVisualStyleBackColor = true;
            // 
            // cboSales
            // 
            this.cboSales.FormattingEnabled = true;
            this.cboSales.Location = new System.Drawing.Point(338, 10);
            this.cboSales.Name = "cboSales";
            this.cboSales.Size = new System.Drawing.Size(225, 23);
            this.cboSales.TabIndex = 136;
            this.cboSales.SelectedIndexChanged += new System.EventHandler(this.cboSales_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微軟正黑體", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(286, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 19);
            this.label1.TabIndex = 135;
            this.label1.Text = "業務";
            // 
            // check
            // 
            this.check.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.check.FillWeight = 25F;
            this.check.HeaderText = "";
            this.check.MinimumWidth = 25;
            this.check.Name = "check";
            this.check.ReadOnly = true;
            this.check.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.check.Width = 25;
            // 
            // CUST_NAME
            // 
            this.CUST_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.CUST_NAME.DataPropertyName = "CUST_NAME";
            this.CUST_NAME.HeaderText = "客戶姓名";
            this.CUST_NAME.Name = "CUST_NAME";
            this.CUST_NAME.ReadOnly = true;
            this.CUST_NAME.Width = 150;
            // 
            // MAIL_TYPE_END
            // 
            this.MAIL_TYPE_END.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.MAIL_TYPE_END.DataPropertyName = "MAIL_TYPE_END";
            this.MAIL_TYPE_END.HeaderText = "寄送方式";
            this.MAIL_TYPE_END.Name = "MAIL_TYPE_END";
            this.MAIL_TYPE_END.ReadOnly = true;
            this.MAIL_TYPE_END.Width = 130;
            // 
            // FILE_NAME
            // 
            this.FILE_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.FILE_NAME.DataPropertyName = "FILE_NAME";
            this.FILE_NAME.HeaderText = "檔名";
            this.FILE_NAME.Name = "FILE_NAME";
            this.FILE_NAME.ReadOnly = true;
            this.FILE_NAME.Width = 210;
            // 
            // EMAIL
            // 
            this.EMAIL.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.EMAIL.DataPropertyName = "EMAIL";
            this.EMAIL.HeaderText = "EMAIL";
            this.EMAIL.Name = "EMAIL";
            this.EMAIL.ReadOnly = true;
            this.EMAIL.Width = 250;
            // 
            // FILE_PATH
            // 
            this.FILE_PATH.DataPropertyName = "FILE_PATH";
            this.FILE_PATH.HeaderText = "FILE_PATH";
            this.FILE_PATH.Name = "FILE_PATH";
            this.FILE_PATH.ReadOnly = true;
            this.FILE_PATH.Visible = false;
            // 
            // CUST_ID
            // 
            this.CUST_ID.DataPropertyName = "CUST_ID";
            this.CUST_ID.HeaderText = "CUST_ID";
            this.CUST_ID.Name = "CUST_ID";
            this.CUST_ID.ReadOnly = true;
            this.CUST_ID.Visible = false;
            // 
            // DATA_YM
            // 
            this.DATA_YM.DataPropertyName = "DATA_YM";
            this.DATA_YM.HeaderText = "DATA_YM";
            this.DATA_YM.Name = "DATA_YM";
            this.DATA_YM.ReadOnly = true;
            this.DATA_YM.Visible = false;
            // 
            // IB_CODE
            // 
            this.IB_CODE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.IB_CODE.DataPropertyName = "IB_CODE";
            this.IB_CODE.HeaderText = "IB_CODE";
            this.IB_CODE.Name = "IB_CODE";
            this.IB_CODE.ReadOnly = true;
            this.IB_CODE.Width = 90;
            // 
            // SALES_NAME
            // 
            this.SALES_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.SALES_NAME.DataPropertyName = "SALES_NAME";
            this.SALES_NAME.HeaderText = "業務";
            this.SALES_NAME.Name = "SALES_NAME";
            this.SALES_NAME.ReadOnly = true;
            this.SALES_NAME.Width = 110;
            // 
            // JOB_STATUS_DESC
            // 
            this.JOB_STATUS_DESC.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.JOB_STATUS_DESC.DataPropertyName = "JOB_STATUS_DESC";
            this.JOB_STATUS_DESC.HeaderText = "狀態";
            this.JOB_STATUS_DESC.Name = "JOB_STATUS_DESC";
            this.JOB_STATUS_DESC.ReadOnly = true;
            this.JOB_STATUS_DESC.Width = 64;
            // 
            // JOB_ID
            // 
            this.JOB_ID.DataPropertyName = "JOB_ID";
            this.JOB_ID.HeaderText = "JOB_ID";
            this.JOB_ID.Name = "JOB_ID";
            this.JOB_ID.ReadOnly = true;
            this.JOB_ID.Visible = false;
            // 
            // CUST_NAME2
            // 
            this.CUST_NAME2.DataPropertyName = "CUST_NAME2";
            this.CUST_NAME2.HeaderText = "CUST_CNAME2";
            this.CUST_NAME2.Name = "CUST_NAME2";
            this.CUST_NAME2.ReadOnly = true;
            this.CUST_NAME2.Visible = false;
            // 
            // frmRPT_MonthlyBill_Mail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.ClientSize = new System.Drawing.Size(1098, 511);
            this.Controls.Add(this.cboSales);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chkIsSendTest);
            this.Controls.Add(this.cboBatchNo);
            this.Controls.Add(this.lblBatchNo);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.dgv_header);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.btnClose);
            this.Font = new System.Drawing.Font("新細明體", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Name = "frmRPT_MonthlyBill_Mail";
            this.Text = "月對帳單發送";
            this.Load += new System.EventHandler(this.frmRPT_MonthlyBill_Mail_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_header)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.DataGridView dgv_header;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label lblBatchNo;
        private System.Windows.Forms.ComboBox cboBatchNo;
        private System.Windows.Forms.CheckBox chkIsSendTest;
        private System.Windows.Forms.ComboBox cboSales;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn check;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUST_NAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn MAIL_TYPE_END;
        private System.Windows.Forms.DataGridViewTextBoxColumn FILE_NAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMAIL;
        private System.Windows.Forms.DataGridViewTextBoxColumn FILE_PATH;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUST_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn DATA_YM;
        private System.Windows.Forms.DataGridViewTextBoxColumn IB_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn SALES_NAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn JOB_STATUS_DESC;
        private System.Windows.Forms.DataGridViewTextBoxColumn JOB_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUST_NAME2;
    }
}
