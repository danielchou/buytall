﻿using MK.Demo.Logic;
using MK.Demo.Model;
using MK_DEMO.Destop.FormFunction;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MK.Demo.Utility;


namespace MK_DEMO.Destop.FormFunction
{
    public partial class frmBL_PERIOD_Management : MK_DEMO.Destop.BaseForm
    {

        private List<BL_PERIOD> dataBefore;

        public frmBL_PERIOD_Management()
        {
            InitializeComponent();

            this.dgv_header.AutoGenerateColumns = false;
            this.dgv_header.MultiSelect = false;
            this.dgv_header.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            this.dataBefore = new List<BL_PERIOD>();
        }



        private void frmBL_PERIOD_Management_Load(object sender, EventArgs e)
        {
            for (int i = DateTime.Now.AddYears(-1).Year; i <= DateTime.Now.AddYears(1).Year; i++)
            {
                this.cboYear.Items.Add(i.ConvertToString());
            }
            this.cboYear.SelectedIndex = this.cboYear.FindStringExact(DateTime.Now.Year.ConvertToString());

            this.BindData();
        }

        private void cboYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.BindData();
        }

        private void BindData()
        {
            var data = base.Cust.GetBL_PERIOD_DataByYear(this.cboYear.Text.ConvertToString().Trim());
            this.dataBefore = data;
            this.dgv_header.DataBindByList(data);


            this.dgv_header.Rows.Cast<DataGridViewRow>()
                .ForEach(x =>
                {
                    var cboStatus = x.Cells["STATUS"] as DataGridViewComboBoxCell;
                    
                    switch (x.Cells["STATUS"].Value.ConvertToString().ToUpper().Trim())
                    {
                        case "CLOSED":
                            cboStatus.ReadOnly = true;
                            break;
                        case "OPEN":
                            cboStatus.ReadOnly = false;
                            cboStatus.Items.Clear();
                            cboStatus.Items.Add("OPEN");
                            cboStatus.Items.Add("CLOSED");
                            break;
                        case "FUTURE":
                            cboStatus.ReadOnly = false;
                            cboStatus.Items.Clear();
                            cboStatus.Items.Add("FUTURE");
                            cboStatus.Items.Add("OPEN");
                            break;
                        default:
                            cboStatus.ReadOnly = false;
                            break;
                    }
                });

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            var dataUpdate = new List<BL_PERIOD>();
            this.dgv_header.Rows.Cast<DataGridViewRow>()
                .ForEach(x =>
                {
                    var strYear = x.Cells["YEAR"].Value.ConvertToString();
                    var strMon = x.Cells["MON"].Value.ConvertToString();
                    var strStatus = x.Cells["STATUS"].Value.ConvertToString();

                    if (this.dataBefore.Where(y => y.YEAR == strYear && y.MON == strMon && y.STATUS == strStatus).ToList().Count() == 0)
                    {
                        dataUpdate.Add(new BL_PERIOD()
                        {
                            YEAR = strYear,
                            MON = strMon,
                            STATUS = strStatus
                        });
                    }
                });

            if (dataUpdate.Count() > 0)
            {
                if (base.Cust.UpdateBL_PERIOD_Data(dataSave: dataUpdate, strUserName: Global.str_UserName.ConvertToString()))
                {
                    MessageBox.Show("資料更新完畢");
                    //  更新完畢 > 重新Bind
                    this.BindData();
                }
                else
                {
                    MessageBox.Show("資料更新失敗");
                }
            }
            else
            {
                MessageBox.Show("無資料異動");
            }
        }

        private void dgv_header_CellClick(object sender, DataGridViewCellEventArgs e)
        {
          
            if (e.ColumnIndex >= 0)
            {
                if (this.dgv_header.Columns[e.ColumnIndex].Name == "STATUS")
                {

                }
            }
        }

        private void dgv_header_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
          
            if (e.ColumnIndex >= 0)
            {
                if (this.dgv_header.Columns[e.ColumnIndex].Name == "STATUS")
                {
                    this.dgv_header.BeginEdit(false);
                    var drpStatuts = this.dgv_header.EditingControl as DataGridViewComboBoxEditingControl;
                    if (drpStatuts != null && drpStatuts.Width - e.X < SystemInformation.VerticalScrollBarWidth)
                    {
                        drpStatuts.DroppedDown = true;
                    }
                }
            }
        }

        private void dgv_header_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            if (e.Exception != null)
            {
               // MessageBox.Show(e.Exception.Message.ToString());
            }
        }
    }
}
