﻿using MK.Demo.Logic;
using MK.Demo.Model;
using MK_DEMO.Destop.FormFunction;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MK.Demo.Utility;

namespace MK_DEMO.Destop.FormFunction
{
    public partial class frmAlertToCustomer : MK_DEMO.Destop.BaseForm
    {
        private List<string> _CustomerID;
        public frmAlertToCustomer(List<string> CustomerID)
        {
            InitializeComponent();

            this.dgv_header.AutoGenerateColumns = false;
            this.dgv_header.MultiSelect = false;
            this.dgv_header.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            this._CustomerID = CustomerID;
        }

        private void frmAlertToCustomer_Load(object sender, EventArgs e)
        {
            var data = base.Cust.GetData(this._CustomerID);
            data.ForEach(x =>
            {
                Random rnd = new Random(Guid.NewGuid().GetHashCode());
                var strVal = rnd.Next(0, 999999).ToString().PadLeft(6, '0');
                x.WEB_PASSWORD = strVal;

                //  檢核沒有UNION_ID_NUMBER的聯名戶, 就要先產生資料
                if
                    (
                        x.CUST_CNAME.ConvertToString().Trim() != string.Empty &&
                        x.CUST_CNAME2.ConvertToString().Trim() != string.Empty &&
                        x.UNION_ID_NUMBER.ConvertToString().Trim() == string.Empty
                    )
                {
                    //  1. Get Key
                    x.UNION_ID_NUMBER = base.Cust.GetNewUnionIdNumber();
                    //  2. Update to DB
                    base.Cust.UpdateUnionIdNumber(x);
                }

            });
            this.dgv_header.DataBindByList(data);
            this.dgv_header.Rows.Cast<DataGridViewRow>()
                .ForEach(x =>
                {
                    x.Cells["DEL"].Value = "刪除";
                    //Random rnd = new Random(Guid.NewGuid().GetHashCode());
                    //var strVal = rnd.Next(0, 999999).ToString().PadLeft(6, '0');
                    //x.Cells["WEB_PASSWORD"].Value = strVal;
                    x.Cells["WEB_PASSWORD"].ReadOnly = false;
                    
                });
            this.BindSelectionInfo();
        }

        private void BindSelectionInfo()
        {
            this.lblInfo.Text = "您已選擇" + this.dgv_header.Rows.Count.ConvertToString() + " 個客戶";
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgv_header_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1 && e.ColumnIndex != -1)
            {
                switch (this.dgv_header.Columns[e.ColumnIndex].Name)
                {
                    case "WEB_PASSWORD":
                        this.dgv_header.BeginEdit(true);
                        break;
                    case "DEL":
                        this.dgv_header.Rows.RemoveAt(e.RowIndex);
                        this.BindSelectionInfo();
                        break;
                    default:
                        break;
                }
            }
        }




        private void Txt_TextChanged(object sender, EventArgs e)
        {
            var txt = sender as TextBox;
            if (txt != null)
            {
                if (txt.Text.IsNumeric())
                {

                }

            }
        }

        private void dgv_header_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            var txt = e.Control as TextBox;
            e.Control.TextChanged -= Txt_TextChanged;
            e.Control.TextChanged += Txt_TextChanged;

            if (txt != null)
            {
                txt.TextChanged -= Txt_TextChanged;
                txt.TextChanged += Txt_TextChanged;

            }
        }

        private void dgv_header_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1 && e.ColumnIndex != -1)
            {
                if (this.dgv_header.Columns[e.ColumnIndex].Name == "WEB_PASSWORD")
                {
                    DataGridViewCellStyle style = new DataGridViewCellStyle();
                    style.BackColor = Color.Yellow;
                    this.dgv_header.CurrentCell.Style.ApplyStyle(style);
                }
            }
        }

        private void dgv_header_CellLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1 && e.ColumnIndex != -1)
            {
                if (this.dgv_header.Columns[e.ColumnIndex].Name == "WEB_PASSWORD")
                {
                    DataGridViewCellStyle style = new DataGridViewCellStyle();
                    style.BackColor = Color.White;
                    this.dgv_header.CurrentCell.Style.ApplyStyle(style);
                }
            }
        }



        private void btnSubmit_Click(object sender, EventArgs e)
        {
            var strMailSubject = "BestLeader 客戶線上查詢通知";
            var strMailBody = "Dear {0}<br/><br/>" +
              "很高興您成為 BestLeader 尊榮客戶<br/>" +
              "您可以透過電腦網頁查詢您所有的合約資料<br/><br/>" +
              "網址:<a href='https://www.bestleader-service.com/Customer_login.aspx'>https://www.bestleader-service.com/Customer_login.aspx</a><br/>" +
              "帳號:{1}<br/>" +
              "密碼:{2}<br/><br/>" +
              "首次登入, 請先變更密碼, 謝謝" +
               "<br/><br/>  Best Leader Markets PTY LTD <br/> 百麗客服部 敬啟";
            this.SendToCust(strMailSubject, strMailBody);
        }

        private void btnSubmit_Resend_Click(object sender, EventArgs e)
        {
            var strMailSubject = "BestLeader 客戶線上查詢變更密碼通知";
            var strMailBody = "Dear {0}<br/><br/>" +
                "您的密碼申請變更<br/>" +
                "請您登入下面網址<br/>" +
                "<a href='https://www.bestleader-service.com/Customer_login.aspx'>https://www.bestleader-service.com/Customer_login.aspx</a><br/>" +
                "登入帳號:{1}<br/>" +
                "預設密碼:{2}<br/><br/>" +
                "進入後進行變更密碼即可";
            this.SendToCust(strMailSubject, strMailBody);
        }

        private void SendToCust(string strMailSubject, string strMailBody)
        {
            //  檢查是否所有的預設密碼都已經填入值了
            if
                (
                    this.dgv_header.Rows.Cast<DataGridViewRow>()
                        .Where(x => x.Cells["WEB_PASSWORD"].Value.ConvertToString().Trim() == string.Empty)
                        .ToList().Count() > 0
                )
            {
                MessageBox.Show("請填入所有的預設密碼");
                return;
            }

            var dataSave = new List<BL_CUST>();
            this.dgv_header.Rows.Cast<DataGridViewRow>()
                .ForEach(x =>
                {
                    dataSave.Add(new BL_CUST()
                    {
                        CUST_ID = decimal.Parse(x.Cells["CUST_ID"].Value.ConvertToString()),
                        WEB_PASSWORD = x.Cells["WEB_PASSWORD"].Value.ConvertToString(),
                        CUST_CNAME = x.Cells["CUST_CNAME"].Value.ConvertToString(),
                        CUST_ENAME = x.Cells["CUST_ENAME"].Value.ConvertToString(),
                        ID_NUMBER = x.Cells["ID_NUMBER"].Value.ConvertToString(),
                        EMAIL_1 = x.Cells["EMAIL_1"].Value.ConvertToString(),
                        EMAIL_2 = x.Cells["EMAIL_2"].Value.ConvertToString(),       //20191112 Fix
                        CUST_CNAME2 = x.Cells["CUST_CNAME2"].Value.ConvertToString(),
                        UNION_ID_NUMBER = x.Cells["UNION_ID_NUMBER"].Value.ConvertToString()
                    });
                });

            //  1. 先更新預設密碼回系統
            if (base.Cust.UpdateCustWebPwd(dataSave))
            {
                //  2. 逐筆發送Mail
                dataSave.ForEach(x =>
                {
                    var strAccount = "您的身分證號碼(或統一編號)";
                    if (x.CUST_CNAME.ConvertToString().Trim() != string.Empty && x.CUST_CNAME2.ConvertToString().Trim() != string.Empty)
                    {
                        strAccount = x.UNION_ID_NUMBER;
                    }
                    var mailTo = new List<string>();
                    var MailSubject = strMailSubject;
                    
                    var MailBody = string.Format
                        (
                            strMailBody,
                            x.CUST_CNAME.ConvertToString() + (x.CUST_CNAME2.ConvertToString().Trim() != string.Empty ? "/" + x.CUST_CNAME2.ConvertToString().Trim() : ""),
                            strAccount,
                            x.WEB_PASSWORD.ConvertToString()
                        );

                    if (Global.IsTestMode)
                    {
                        //  測試階段只發送給Admin
                        mailTo = GetConfigValueByKey("MailSystemAdmin").Split(new string[] { ";" }, StringSplitOptions.None).ToList();
                        MailSubject += "(Testing)";
                    }
                    else
                    {
                        mailTo.Add(x.EMAIL_1.ConvertToString());
                        //  Add On 2019/10/16 聯名戶
                        if (x.CUST_CNAME2.ConvertToString().Trim()!=string.Empty)
                        {
                            mailTo.Add(x.EMAIL_2.ConvertToString());
                        }
                    }
                    SendMail(mailTo, MailSubject, MailBody);
                });

                MessageBox.Show("送出完成");
                this.Close();
            }
            else
            {
                MessageBox.Show("送出失敗");
            }

        }

        private void dgv_header_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
