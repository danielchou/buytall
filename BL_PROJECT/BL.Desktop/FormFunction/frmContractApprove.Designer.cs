﻿namespace MK_DEMO.Destop.FormFunction
{
    partial class frmContractApprove
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmContractApprove));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.btnBatchReject = new System.Windows.Forms.Button();
            this.cboApproveStatus = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.chkALL = new System.Windows.Forms.CheckBox();
            this.btnBatchDepositOut = new System.Windows.Forms.Button();
            this.txtCustomerName = new System.Windows.Forms.TextBox();
            this.txtOrderNo = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnCleanCondition = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.picLoder = new System.Windows.Forms.PictureBox();
            this.dgv_header = new System.Windows.Forms.DataGridView();
            this.check = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ORDER_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CONTRACT_STATUS = new System.Windows.Forms.DataGridViewLinkColumn();
            this.CUST_CNAME = new System.Windows.Forms.DataGridViewLinkColumn();
            this.CUST_ENAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SALES = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IB_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT_USD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT_NTD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT_RMB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT_EUR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT_AUD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT_JPY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT_NZD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INTEREST_USD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INTEREST_NTD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INTEREST_RMB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INTEREST_EUR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INTEREST_AUD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INTEREST_JPY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INTEREST_NZD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FROM_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.END_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.YEAR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ORDER_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUST_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.STATUS_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colFROM_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.REJECT_REASON = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ORG_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EMAIL_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EMAIL_2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EMAIL2_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EMAIL2_2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SALES_EMAIL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnBatchVoid = new System.Windows.Forms.Button();
            this.btn_IND_Contract_Notic = new System.Windows.Forms.Button();
            this.btn_Welcome_Contract_Notic = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.picLoder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_header)).BeginInit();
            this.SuspendLayout();
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.label2.Location = new System.Drawing.Point(232, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 24);
            this.label2.TabIndex = 142;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.button1.ForeColor = System.Drawing.Color.Red;
            this.button1.Location = new System.Drawing.Point(602, 38);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(177, 30);
            this.button1.TabIndex = 141;
            this.button1.Text = "批次Approve作業(香港)";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnBatchReject
            // 
            this.btnBatchReject.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.btnBatchReject.Location = new System.Drawing.Point(785, 6);
            this.btnBatchReject.Name = "btnBatchReject";
            this.btnBatchReject.Size = new System.Drawing.Size(134, 30);
            this.btnBatchReject.TabIndex = 140;
            this.btnBatchReject.Text = "批次退件作業";
            this.btnBatchReject.UseVisualStyleBackColor = true;
            this.btnBatchReject.Click += new System.EventHandler(this.btnBatchReject_Click);
            // 
            // cboApproveStatus
            // 
            this.cboApproveStatus.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.cboApproveStatus.FormattingEnabled = true;
            this.cboApproveStatus.Location = new System.Drawing.Point(92, 38);
            this.cboApproveStatus.Name = "cboApproveStatus";
            this.cboApproveStatus.Size = new System.Drawing.Size(134, 31);
            this.cboApproveStatus.TabIndex = 139;
            this.cboApproveStatus.SelectedIndexChanged += new System.EventHandler(this.cboApproveStatus_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.label1.Location = new System.Drawing.Point(15, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 24);
            this.label1.TabIndex = 138;
            this.label1.Text = "狀態";
            // 
            // chkALL
            // 
            this.chkALL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkALL.AutoSize = true;
            this.chkALL.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.chkALL.Location = new System.Drawing.Point(12, 401);
            this.chkALL.Name = "chkALL";
            this.chkALL.Size = new System.Drawing.Size(70, 28);
            this.chkALL.TabIndex = 137;
            this.chkALL.Text = "全選";
            this.chkALL.UseVisualStyleBackColor = true;
            this.chkALL.CheckedChanged += new System.EventHandler(this.chkALL_CheckedChanged);
            // 
            // btnBatchDepositOut
            // 
            this.btnBatchDepositOut.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.btnBatchDepositOut.Location = new System.Drawing.Point(602, 6);
            this.btnBatchDepositOut.Name = "btnBatchDepositOut";
            this.btnBatchDepositOut.Size = new System.Drawing.Size(177, 30);
            this.btnBatchDepositOut.TabIndex = 136;
            this.btnBatchDepositOut.Text = "批次查核作業(行政)";
            this.btnBatchDepositOut.UseVisualStyleBackColor = true;
            this.btnBatchDepositOut.Click += new System.EventHandler(this.btnBatchDepositOut_Click);
            // 
            // txtCustomerName
            // 
            this.txtCustomerName.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.txtCustomerName.Location = new System.Drawing.Point(288, 6);
            this.txtCustomerName.Name = "txtCustomerName";
            this.txtCustomerName.Size = new System.Drawing.Size(134, 32);
            this.txtCustomerName.TabIndex = 135;
            // 
            // txtOrderNo
            // 
            this.txtOrderNo.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.txtOrderNo.Location = new System.Drawing.Point(92, 6);
            this.txtOrderNo.Name = "txtOrderNo";
            this.txtOrderNo.Size = new System.Drawing.Size(134, 32);
            this.txtOrderNo.TabIndex = 134;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.label4.Location = new System.Drawing.Point(232, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 24);
            this.label4.TabIndex = 133;
            this.label4.Text = "客戶名";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.label5.Location = new System.Drawing.Point(15, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 24);
            this.label5.TabIndex = 132;
            this.label5.Text = "Order No";
            // 
            // btnCleanCondition
            // 
            this.btnCleanCondition.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.btnCleanCondition.Location = new System.Drawing.Point(428, 38);
            this.btnCleanCondition.Name = "btnCleanCondition";
            this.btnCleanCondition.Size = new System.Drawing.Size(157, 30);
            this.btnCleanCondition.TabIndex = 131;
            this.btnCleanCondition.Text = "清除查詢條件";
            this.btnCleanCondition.UseVisualStyleBackColor = true;
            this.btnCleanCondition.Click += new System.EventHandler(this.btnCleanCondition_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.btnSearch.Location = new System.Drawing.Point(428, 6);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(157, 30);
            this.btnSearch.TabIndex = 130;
            this.btnSearch.Text = "查詢";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // picLoder
            // 
            this.picLoder.BackColor = System.Drawing.Color.Transparent;
            this.picLoder.Image = ((System.Drawing.Image)(resources.GetObject("picLoder.Image")));
            this.picLoder.Location = new System.Drawing.Point(372, 228);
            this.picLoder.Name = "picLoder";
            this.picLoder.Size = new System.Drawing.Size(174, 19);
            this.picLoder.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picLoder.TabIndex = 114;
            this.picLoder.TabStop = false;
            this.picLoder.Visible = false;
            // 
            // dgv_header
            // 
            this.dgv_header.AllowUserToAddRows = false;
            this.dgv_header.AllowUserToDeleteRows = false;
            this.dgv_header.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_header.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_header.ColumnHeadersHeight = 25;
            this.dgv_header.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv_header.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.check,
            this.ORDER_NO,
            this.CONTRACT_STATUS,
            this.CUST_CNAME,
            this.CUST_ENAME,
            this.SALES,
            this.IB_CODE,
            this.AMOUNT_USD,
            this.AMOUNT_NTD,
            this.AMOUNT_RMB,
            this.AMOUNT_EUR,
            this.AMOUNT_AUD,
            this.AMOUNT_JPY,
            this.AMOUNT_NZD,
            this.INTEREST_USD,
            this.INTEREST_NTD,
            this.INTEREST_RMB,
            this.INTEREST_EUR,
            this.INTEREST_AUD,
            this.INTEREST_JPY,
            this.INTEREST_NZD,
            this.FROM_DATE,
            this.END_DATE,
            this.YEAR,
            this.ORDER_ID,
            this.CUST_ID,
            this.STATUS_CODE,
            this.colFROM_DATE,
            this.REJECT_REASON,
            this.ORG_CODE,
            this.EMAIL_1,
            this.EMAIL_2,
            this.EMAIL2_1,
            this.EMAIL2_2,
            this.SALES_EMAIL});
            this.dgv_header.Location = new System.Drawing.Point(12, 108);
            this.dgv_header.Name = "dgv_header";
            this.dgv_header.ReadOnly = true;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("微軟正黑體", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.Color.RoyalBlue;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.Color.Yellow;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_header.RowHeadersDefaultCellStyle = dataGridViewCellStyle16;
            this.dgv_header.RowHeadersWidth = 25;
            this.dgv_header.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgv_header.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(226)))), ((int)(((byte)(239)))));
            this.dgv_header.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_header.RowTemplate.Height = 24;
            this.dgv_header.Size = new System.Drawing.Size(905, 293);
            this.dgv_header.TabIndex = 6;
            this.dgv_header.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_header_CellClick);
            this.dgv_header.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgv_header_DataBindingComplete);
            // 
            // check
            // 
            this.check.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.check.FillWeight = 25F;
            this.check.HeaderText = "選";
            this.check.MinimumWidth = 25;
            this.check.Name = "check";
            this.check.ReadOnly = true;
            this.check.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.check.Width = 25;
            // 
            // ORDER_NO
            // 
            this.ORDER_NO.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ORDER_NO.DataPropertyName = "ORDER_NO";
            this.ORDER_NO.HeaderText = "合約編號";
            this.ORDER_NO.MinimumWidth = 6;
            this.ORDER_NO.Name = "ORDER_NO";
            this.ORDER_NO.ReadOnly = true;
            this.ORDER_NO.Width = 90;
            // 
            // CONTRACT_STATUS
            // 
            this.CONTRACT_STATUS.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.CONTRACT_STATUS.DataPropertyName = "GetSTATUS";
            this.CONTRACT_STATUS.HeaderText = "合約狀態";
            this.CONTRACT_STATUS.MinimumWidth = 6;
            this.CONTRACT_STATUS.Name = "CONTRACT_STATUS";
            this.CONTRACT_STATUS.ReadOnly = true;
            this.CONTRACT_STATUS.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.CONTRACT_STATUS.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.CONTRACT_STATUS.Width = 90;
            // 
            // CUST_CNAME
            // 
            this.CUST_CNAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.CUST_CNAME.DataPropertyName = "CUST_CNAME";
            this.CUST_CNAME.HeaderText = "姓名";
            this.CUST_CNAME.MinimumWidth = 6;
            this.CUST_CNAME.Name = "CUST_CNAME";
            this.CUST_CNAME.ReadOnly = true;
            this.CUST_CNAME.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.CUST_CNAME.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.CUST_CNAME.Width = 130;
            // 
            // CUST_ENAME
            // 
            this.CUST_ENAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.CUST_ENAME.DataPropertyName = "CUST_ENAME";
            this.CUST_ENAME.HeaderText = "英文姓名";
            this.CUST_ENAME.MinimumWidth = 6;
            this.CUST_ENAME.Name = "CUST_ENAME";
            this.CUST_ENAME.ReadOnly = true;
            this.CUST_ENAME.Width = 220;
            // 
            // SALES
            // 
            this.SALES.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.SALES.DataPropertyName = "SALES_NAME";
            this.SALES.HeaderText = "業務";
            this.SALES.MinimumWidth = 6;
            this.SALES.Name = "SALES";
            this.SALES.ReadOnly = true;
            this.SALES.Width = 130;
            // 
            // IB_CODE
            // 
            this.IB_CODE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.IB_CODE.DataPropertyName = "IB_CODE";
            this.IB_CODE.HeaderText = "IB_CODE";
            this.IB_CODE.MinimumWidth = 100;
            this.IB_CODE.Name = "IB_CODE";
            this.IB_CODE.ReadOnly = true;
            this.IB_CODE.Width = 125;
            // 
            // AMOUNT_USD
            // 
            this.AMOUNT_USD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.AMOUNT_USD.DataPropertyName = "AMOUNT_USD";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle1.Format = "N2";
            dataGridViewCellStyle1.NullValue = null;
            this.AMOUNT_USD.DefaultCellStyle = dataGridViewCellStyle1;
            this.AMOUNT_USD.HeaderText = "入金金額(美元)";
            this.AMOUNT_USD.MinimumWidth = 6;
            this.AMOUNT_USD.Name = "AMOUNT_USD";
            this.AMOUNT_USD.ReadOnly = true;
            this.AMOUNT_USD.Width = 150;
            // 
            // AMOUNT_NTD
            // 
            this.AMOUNT_NTD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.AMOUNT_NTD.DataPropertyName = "AMOUNT_NTD";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "N2";
            dataGridViewCellStyle2.NullValue = null;
            this.AMOUNT_NTD.DefaultCellStyle = dataGridViewCellStyle2;
            this.AMOUNT_NTD.HeaderText = "入金金額(新台幣)";
            this.AMOUNT_NTD.MinimumWidth = 6;
            this.AMOUNT_NTD.Name = "AMOUNT_NTD";
            this.AMOUNT_NTD.ReadOnly = true;
            this.AMOUNT_NTD.Width = 150;
            // 
            // AMOUNT_RMB
            // 
            this.AMOUNT_RMB.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.AMOUNT_RMB.DataPropertyName = "AMOUNT_RMB";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = null;
            this.AMOUNT_RMB.DefaultCellStyle = dataGridViewCellStyle3;
            this.AMOUNT_RMB.HeaderText = "入金金額(人民幣)";
            this.AMOUNT_RMB.MinimumWidth = 6;
            this.AMOUNT_RMB.Name = "AMOUNT_RMB";
            this.AMOUNT_RMB.ReadOnly = true;
            this.AMOUNT_RMB.Width = 150;
            // 
            // AMOUNT_EUR
            // 
            this.AMOUNT_EUR.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.AMOUNT_EUR.DataPropertyName = "AMOUNT_EUR";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N2";
            this.AMOUNT_EUR.DefaultCellStyle = dataGridViewCellStyle4;
            this.AMOUNT_EUR.HeaderText = "入金金額(歐元)";
            this.AMOUNT_EUR.MinimumWidth = 6;
            this.AMOUNT_EUR.Name = "AMOUNT_EUR";
            this.AMOUNT_EUR.ReadOnly = true;
            this.AMOUNT_EUR.Width = 150;
            // 
            // AMOUNT_AUD
            // 
            this.AMOUNT_AUD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.AMOUNT_AUD.DataPropertyName = "AMOUNT_AUD";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "N2";
            this.AMOUNT_AUD.DefaultCellStyle = dataGridViewCellStyle5;
            this.AMOUNT_AUD.HeaderText = "入金金額(澳幣)";
            this.AMOUNT_AUD.MinimumWidth = 6;
            this.AMOUNT_AUD.Name = "AMOUNT_AUD";
            this.AMOUNT_AUD.ReadOnly = true;
            this.AMOUNT_AUD.Width = 150;
            // 
            // AMOUNT_JPY
            // 
            this.AMOUNT_JPY.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.AMOUNT_JPY.DataPropertyName = "AMOUNT_JPY";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "N2";
            this.AMOUNT_JPY.DefaultCellStyle = dataGridViewCellStyle6;
            this.AMOUNT_JPY.HeaderText = "入金金額(日幣)";
            this.AMOUNT_JPY.MinimumWidth = 6;
            this.AMOUNT_JPY.Name = "AMOUNT_JPY";
            this.AMOUNT_JPY.ReadOnly = true;
            this.AMOUNT_JPY.Width = 150;
            // 
            // AMOUNT_NZD
            // 
            this.AMOUNT_NZD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.AMOUNT_NZD.DataPropertyName = "AMOUNT_NZD";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Format = "N2";
            this.AMOUNT_NZD.DefaultCellStyle = dataGridViewCellStyle7;
            this.AMOUNT_NZD.HeaderText = "入金金額(紐幣)";
            this.AMOUNT_NZD.MinimumWidth = 6;
            this.AMOUNT_NZD.Name = "AMOUNT_NZD";
            this.AMOUNT_NZD.ReadOnly = true;
            this.AMOUNT_NZD.Width = 150;
            // 
            // INTEREST_USD
            // 
            this.INTEREST_USD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.INTEREST_USD.DataPropertyName = "INTEREST_USD";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.Format = "N2";
            dataGridViewCellStyle8.NullValue = null;
            this.INTEREST_USD.DefaultCellStyle = dataGridViewCellStyle8;
            this.INTEREST_USD.HeaderText = "利息(美金)";
            this.INTEREST_USD.MinimumWidth = 95;
            this.INTEREST_USD.Name = "INTEREST_USD";
            this.INTEREST_USD.ReadOnly = true;
            this.INTEREST_USD.Width = 115;
            // 
            // INTEREST_NTD
            // 
            this.INTEREST_NTD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.INTEREST_NTD.DataPropertyName = "INTEREST_NTD";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle9.Format = "N2";
            dataGridViewCellStyle9.NullValue = null;
            this.INTEREST_NTD.DefaultCellStyle = dataGridViewCellStyle9;
            this.INTEREST_NTD.HeaderText = "利息(新台幣)";
            this.INTEREST_NTD.MinimumWidth = 95;
            this.INTEREST_NTD.Name = "INTEREST_NTD";
            this.INTEREST_NTD.ReadOnly = true;
            this.INTEREST_NTD.Width = 120;
            // 
            // INTEREST_RMB
            // 
            this.INTEREST_RMB.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.INTEREST_RMB.DataPropertyName = "INTEREST_RMB";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle10.Format = "N2";
            dataGridViewCellStyle10.NullValue = null;
            this.INTEREST_RMB.DefaultCellStyle = dataGridViewCellStyle10;
            this.INTEREST_RMB.HeaderText = "利息(人民幣)";
            this.INTEREST_RMB.MinimumWidth = 95;
            this.INTEREST_RMB.Name = "INTEREST_RMB";
            this.INTEREST_RMB.ReadOnly = true;
            this.INTEREST_RMB.Width = 120;
            // 
            // INTEREST_EUR
            // 
            this.INTEREST_EUR.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.INTEREST_EUR.DataPropertyName = "INTEREST_EUR";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle11.Format = "N2";
            this.INTEREST_EUR.DefaultCellStyle = dataGridViewCellStyle11;
            this.INTEREST_EUR.HeaderText = "利息(歐元)";
            this.INTEREST_EUR.MinimumWidth = 6;
            this.INTEREST_EUR.Name = "INTEREST_EUR";
            this.INTEREST_EUR.ReadOnly = true;
            this.INTEREST_EUR.Width = 150;
            // 
            // INTEREST_AUD
            // 
            this.INTEREST_AUD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.INTEREST_AUD.DataPropertyName = "INTEREST_AUD";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle12.Format = "N2";
            this.INTEREST_AUD.DefaultCellStyle = dataGridViewCellStyle12;
            this.INTEREST_AUD.HeaderText = "利息(澳幣)";
            this.INTEREST_AUD.MinimumWidth = 6;
            this.INTEREST_AUD.Name = "INTEREST_AUD";
            this.INTEREST_AUD.ReadOnly = true;
            this.INTEREST_AUD.Width = 150;
            // 
            // INTEREST_JPY
            // 
            this.INTEREST_JPY.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.INTEREST_JPY.DataPropertyName = "INTEREST_JPY";
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle13.Format = "N2";
            this.INTEREST_JPY.DefaultCellStyle = dataGridViewCellStyle13;
            this.INTEREST_JPY.HeaderText = "利息(日幣)";
            this.INTEREST_JPY.MinimumWidth = 6;
            this.INTEREST_JPY.Name = "INTEREST_JPY";
            this.INTEREST_JPY.ReadOnly = true;
            this.INTEREST_JPY.Width = 150;
            // 
            // INTEREST_NZD
            // 
            this.INTEREST_NZD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.INTEREST_NZD.DataPropertyName = "INTEREST_NZD";
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle14.Format = "N2";
            this.INTEREST_NZD.DefaultCellStyle = dataGridViewCellStyle14;
            this.INTEREST_NZD.HeaderText = "利息(紐幣)";
            this.INTEREST_NZD.MinimumWidth = 6;
            this.INTEREST_NZD.Name = "INTEREST_NZD";
            this.INTEREST_NZD.ReadOnly = true;
            this.INTEREST_NZD.Width = 150;
            // 
            // FROM_DATE
            // 
            this.FROM_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.FROM_DATE.DataPropertyName = "FROM_DATE";
            this.FROM_DATE.HeaderText = "合約日期";
            this.FROM_DATE.MinimumWidth = 6;
            this.FROM_DATE.Name = "FROM_DATE";
            this.FROM_DATE.ReadOnly = true;
            this.FROM_DATE.Width = 110;
            // 
            // END_DATE
            // 
            this.END_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.END_DATE.DataPropertyName = "END_DATE";
            this.END_DATE.HeaderText = "合約結束日期";
            this.END_DATE.MinimumWidth = 6;
            this.END_DATE.Name = "END_DATE";
            this.END_DATE.ReadOnly = true;
            this.END_DATE.Width = 120;
            // 
            // YEAR
            // 
            this.YEAR.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.YEAR.DataPropertyName = "YEAR";
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.YEAR.DefaultCellStyle = dataGridViewCellStyle15;
            this.YEAR.HeaderText = "年期";
            this.YEAR.MinimumWidth = 6;
            this.YEAR.Name = "YEAR";
            this.YEAR.ReadOnly = true;
            this.YEAR.Width = 90;
            // 
            // ORDER_ID
            // 
            this.ORDER_ID.DataPropertyName = "ORDER_ID";
            this.ORDER_ID.HeaderText = "ORDER_ID";
            this.ORDER_ID.MinimumWidth = 6;
            this.ORDER_ID.Name = "ORDER_ID";
            this.ORDER_ID.ReadOnly = true;
            this.ORDER_ID.Visible = false;
            // 
            // CUST_ID
            // 
            this.CUST_ID.DataPropertyName = "CUST_ID";
            this.CUST_ID.HeaderText = "CUST_ID";
            this.CUST_ID.MinimumWidth = 6;
            this.CUST_ID.Name = "CUST_ID";
            this.CUST_ID.ReadOnly = true;
            this.CUST_ID.Visible = false;
            // 
            // STATUS_CODE
            // 
            this.STATUS_CODE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.STATUS_CODE.DataPropertyName = "STATUS_CODE";
            this.STATUS_CODE.HeaderText = "STATUS_CODE";
            this.STATUS_CODE.MinimumWidth = 6;
            this.STATUS_CODE.Name = "STATUS_CODE";
            this.STATUS_CODE.ReadOnly = true;
            this.STATUS_CODE.Visible = false;
            this.STATUS_CODE.Width = 10;
            // 
            // colFROM_DATE
            // 
            this.colFROM_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.colFROM_DATE.DataPropertyName = "FROM_DATE";
            this.colFROM_DATE.HeaderText = "FROM_DATE";
            this.colFROM_DATE.MinimumWidth = 6;
            this.colFROM_DATE.Name = "colFROM_DATE";
            this.colFROM_DATE.ReadOnly = true;
            this.colFROM_DATE.Width = 130;
            // 
            // REJECT_REASON
            // 
            this.REJECT_REASON.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.REJECT_REASON.DataPropertyName = "REJECT_REASON";
            this.REJECT_REASON.HeaderText = "退件原因";
            this.REJECT_REASON.MinimumWidth = 6;
            this.REJECT_REASON.Name = "REJECT_REASON";
            this.REJECT_REASON.ReadOnly = true;
            this.REJECT_REASON.Width = 250;
            // 
            // ORG_CODE
            // 
            this.ORG_CODE.DataPropertyName = "ORG_CODE";
            this.ORG_CODE.HeaderText = "ORG_CODE";
            this.ORG_CODE.MinimumWidth = 6;
            this.ORG_CODE.Name = "ORG_CODE";
            this.ORG_CODE.ReadOnly = true;
            this.ORG_CODE.Visible = false;
            // 
            // EMAIL_1
            // 
            this.EMAIL_1.DataPropertyName = "EMAIL_1";
            this.EMAIL_1.HeaderText = "EMAIL_1";
            this.EMAIL_1.MinimumWidth = 6;
            this.EMAIL_1.Name = "EMAIL_1";
            this.EMAIL_1.ReadOnly = true;
            this.EMAIL_1.Visible = false;
            // 
            // EMAIL_2
            // 
            this.EMAIL_2.DataPropertyName = "EMAIL_2";
            this.EMAIL_2.HeaderText = "EMAIL_2";
            this.EMAIL_2.MinimumWidth = 6;
            this.EMAIL_2.Name = "EMAIL_2";
            this.EMAIL_2.ReadOnly = true;
            this.EMAIL_2.Visible = false;
            // 
            // EMAIL2_1
            // 
            this.EMAIL2_1.DataPropertyName = "EMAIL2_1";
            this.EMAIL2_1.HeaderText = "EMAIL2_1";
            this.EMAIL2_1.MinimumWidth = 6;
            this.EMAIL2_1.Name = "EMAIL2_1";
            this.EMAIL2_1.ReadOnly = true;
            this.EMAIL2_1.Visible = false;
            // 
            // EMAIL2_2
            // 
            this.EMAIL2_2.DataPropertyName = "EMAIL2_2";
            this.EMAIL2_2.HeaderText = "EMAIL2_2";
            this.EMAIL2_2.MinimumWidth = 6;
            this.EMAIL2_2.Name = "EMAIL2_2";
            this.EMAIL2_2.ReadOnly = true;
            this.EMAIL2_2.Visible = false;
            // 
            // SALES_EMAIL
            // 
            this.SALES_EMAIL.DataPropertyName = "SALES_EMAIL";
            this.SALES_EMAIL.HeaderText = "SALES_EMAIL";
            this.SALES_EMAIL.MinimumWidth = 6;
            this.SALES_EMAIL.Name = "SALES_EMAIL";
            this.SALES_EMAIL.ReadOnly = true;
            this.SALES_EMAIL.Visible = false;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.btnClose.Location = new System.Drawing.Point(807, 402);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(110, 36);
            this.btnClose.TabIndex = 5;
            this.btnClose.Text = "關閉";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnBatchVoid
            // 
            this.btnBatchVoid.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.btnBatchVoid.Location = new System.Drawing.Point(785, 38);
            this.btnBatchVoid.Name = "btnBatchVoid";
            this.btnBatchVoid.Size = new System.Drawing.Size(134, 30);
            this.btnBatchVoid.TabIndex = 143;
            this.btnBatchVoid.Text = "批次作廢作業";
            this.btnBatchVoid.UseVisualStyleBackColor = true;
            this.btnBatchVoid.Click += new System.EventHandler(this.btnBatchVoid_Click);
            // 
            // btn_IND_Contract_Notic
            // 
            this.btn_IND_Contract_Notic.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.btn_IND_Contract_Notic.Location = new System.Drawing.Point(238, 36);
            this.btn_IND_Contract_Notic.Name = "btn_IND_Contract_Notic";
            this.btn_IND_Contract_Notic.Size = new System.Drawing.Size(184, 30);
            this.btn_IND_Contract_Notic.TabIndex = 144;
            this.btn_IND_Contract_Notic.Text = "印尼合約通知功能";
            this.btn_IND_Contract_Notic.UseVisualStyleBackColor = true;
            this.btn_IND_Contract_Notic.Click += new System.EventHandler(this.btn_IND_Contract_Notic_Click);
            // 
            // btn_Welcome_Contract_Notic
            // 
            this.btn_Welcome_Contract_Notic.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.btn_Welcome_Contract_Notic.Location = new System.Drawing.Point(238, 72);
            this.btn_Welcome_Contract_Notic.Name = "btn_Welcome_Contract_Notic";
            this.btn_Welcome_Contract_Notic.Size = new System.Drawing.Size(184, 30);
            this.btn_Welcome_Contract_Notic.TabIndex = 145;
            this.btn_Welcome_Contract_Notic.Text = "Welcome Letter 通知";
            this.btn_Welcome_Contract_Notic.UseVisualStyleBackColor = true;
            this.btn_Welcome_Contract_Notic.Click += new System.EventHandler(this.btn_Welcome_Contract_Notic_Click);
            // 
            // frmContractApprove
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 22F);
            this.ClientSize = new System.Drawing.Size(924, 450);
            this.Controls.Add(this.btn_Welcome_Contract_Notic);
            this.Controls.Add(this.btn_IND_Contract_Notic);
            this.Controls.Add(this.btnBatchVoid);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnBatchReject);
            this.Controls.Add(this.cboApproveStatus);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chkALL);
            this.Controls.Add(this.btnBatchDepositOut);
            this.Controls.Add(this.txtCustomerName);
            this.Controls.Add(this.txtOrderNo);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnCleanCondition);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.picLoder);
            this.Controls.Add(this.dgv_header);
            this.Controls.Add(this.btnClose);
            this.Font = new System.Drawing.Font("微軟正黑體", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Name = "frmContractApprove";
            this.Text = "合約資料";
            this.Load += new System.EventHandler(this.frmContractDetail_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picLoder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_header)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.DataGridView dgv_header;
        private System.Windows.Forms.PictureBox picLoder;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.TextBox txtCustomerName;
        private System.Windows.Forms.TextBox txtOrderNo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnCleanCondition;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnBatchDepositOut;
        private System.Windows.Forms.CheckBox chkALL;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboApproveStatus;
        private System.Windows.Forms.Button btnBatchReject;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnBatchVoid;
        private System.Windows.Forms.Button btn_IND_Contract_Notic;
        private System.Windows.Forms.DataGridViewCheckBoxColumn check;
        private System.Windows.Forms.DataGridViewTextBoxColumn ORDER_NO;
        private System.Windows.Forms.DataGridViewLinkColumn CONTRACT_STATUS;
        private System.Windows.Forms.DataGridViewLinkColumn CUST_CNAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUST_ENAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn SALES;
        private System.Windows.Forms.DataGridViewTextBoxColumn IB_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn AMOUNT_USD;
        private System.Windows.Forms.DataGridViewTextBoxColumn AMOUNT_NTD;
        private System.Windows.Forms.DataGridViewTextBoxColumn AMOUNT_RMB;
        private System.Windows.Forms.DataGridViewTextBoxColumn AMOUNT_EUR;
        private System.Windows.Forms.DataGridViewTextBoxColumn AMOUNT_AUD;
        private System.Windows.Forms.DataGridViewTextBoxColumn AMOUNT_JPY;
        private System.Windows.Forms.DataGridViewTextBoxColumn AMOUNT_NZD;
        private System.Windows.Forms.DataGridViewTextBoxColumn INTEREST_USD;
        private System.Windows.Forms.DataGridViewTextBoxColumn INTEREST_NTD;
        private System.Windows.Forms.DataGridViewTextBoxColumn INTEREST_RMB;
        private System.Windows.Forms.DataGridViewTextBoxColumn INTEREST_EUR;
        private System.Windows.Forms.DataGridViewTextBoxColumn INTEREST_AUD;
        private System.Windows.Forms.DataGridViewTextBoxColumn INTEREST_JPY;
        private System.Windows.Forms.DataGridViewTextBoxColumn INTEREST_NZD;
        private System.Windows.Forms.DataGridViewTextBoxColumn FROM_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn END_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn YEAR;
        private System.Windows.Forms.DataGridViewTextBoxColumn ORDER_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUST_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn STATUS_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFROM_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn REJECT_REASON;
        private System.Windows.Forms.DataGridViewTextBoxColumn ORG_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMAIL_1;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMAIL_2;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMAIL2_1;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMAIL2_2;
        private System.Windows.Forms.DataGridViewTextBoxColumn SALES_EMAIL;
        private System.Windows.Forms.Button btn_Welcome_Contract_Notic;
    }
}
