﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MK.Demo.Utility;
using Microsoft.Reporting.WinForms;
using MK.Demo.Logic;

namespace MK_DEMO.Destop.FormFunction
{
    public partial class frmRPT_SalesPerformByMonth : Form
    {
        private List<string> liCustID = new List<string>();

        public frmRPT_SalesPerformByMonth()
        {
            InitializeComponent();
        }

        private void frmRPT_SalesPerformByMonth_Load(object sender, EventArgs e)
        {
            this.InitComboBox();
            //this.BindReport();
        }

        private void InitComboBox()
        {
            this.cboYear.Items.Clear();
            this.cboMonth.Items.Clear();
            for (int i = DateTime.Now.Year; i >= DateTime.Now.Year - 3; i--)
            {
                this.cboYear.Items.Add(i.ConvertToString());
            }
            for (int i = 1; i <= 12; i++)
            {
                this.cboMonth.Items.Add(i.ConvertToString());
            }
            this.cboYear.SelectedIndex = this.cboYear.FindStringExact(DateTime.Now.Year.ConvertToString());
            this.cboMonth.SelectedIndex = this.cboMonth.FindStringExact(DateTime.Now.Month.ConvertToString());

        }



        private void button2_Click(object sender, EventArgs e)
        {
            this.BindReport();
        }


        private void BindReport()
        {
            var strYear = this.cboYear.Text;
            var strMonth = this.cboMonth.Text;

            if (strYear.Trim()==string.Empty || strMonth.Trim()== string.Empty)
            {
                MessageBox.Show("請選擇基準年月");
                return;
            }


            var strORG = Global.ORG_CODE;

            string exeFolder = Application.StartupPath;
            string reportPath = System.IO.Path.Combine(exeFolder, @"RPT\RPT_SALES_PERFORMANCE_BY_MONTH.rdlc");
            var data = new logicCust(Global.IsTestMode).GetRPT_SALES_PERFORMANCE_BY_MONTH(strYear + strMonth.PadLeft(2,'0'), strORG);

            var binding = new BindingSource();
            binding.DataSource = data;
            this.reportViewer2.Reset();

            this.reportViewer2.LocalReport.ReportPath = reportPath;
            this.reportViewer2.LocalReport.DataSources.Add(new ReportDataSource("dsReport", binding));
            this.reportViewer2.RefreshReport();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

    




    }
}
