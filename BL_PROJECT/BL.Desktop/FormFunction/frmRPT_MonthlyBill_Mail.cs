﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MK.Demo.Utility;
using MK.Demo.Logic;
using System.IO;
using MK.Demo.Model;



namespace MK_DEMO.Destop.FormFunction
{
    public partial class frmRPT_MonthlyBill_Mail : MK_DEMO.Destop.BaseForm
    {
        private List<FILE_INFO> dataOutput;
        public frmRPT_MonthlyBill_Mail(List<FILE_INFO> data = null)
        {
            InitializeComponent();

            this.dgv_header.AutoGenerateColumns = false;
            this.dgv_header.MultiSelect = false;
            this.dgv_header.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            
            this.dataOutput = data;
        }

        private void frmRPT_MonthlyBill_Mail_Load(object sender, EventArgs e)
        {
            if (this.dataOutput != null)
            {
                this.lblBatchNo.Visible = false;
                this.cboBatchNo.Visible = false;
                this.dgv_header.DataBindByList(this.dataOutput);
                this.dgv_header.Columns["JOB_STATUS_DESC"].Visible = false;
            }
            else
            {
                //  透過批號選取資料做發送動作, 前端會單獨產生PDF並取得一組批號, 在這邊會透過批號撈取資料
                this.lblBatchNo.Visible = true;
                this.cboBatchNo.Visible = true;
                //  Bind Batch No
                this.cboBatchNo.DataBindByList(
                    data: new logicCust(Global.IsTestMode).GetAllBillBatchNoByUser(Global.str_UserName),
                    display: "JOB_ID",
                    value: "JOB_ID",
                    top: null
                    );
                this.BindSalesList();
                this.BindDataByBatchNo();
                this.dgv_header.Columns["JOB_STATUS_DESC"].Visible = true;
            }
        }

        private void BindSalesList()
        {
            var liBatch = new List<string>();
            liBatch.Add(this.cboBatchNo.Text.ConvertToString());
            var data = new logicCust(Global.IsTestMode).GetSalesInfoByBatchNo(liBatch);
            var dataSales = new List<ComboBoxData<string>>();

            data
                .Select(x => new { x.IB_CODE, x.SALES_NAME })
                .Distinct()
                .ToList()
                .ForEach(x =>
                {
                    var strDisplay = x.IB_CODE.ConvertToString() + "-" + x.SALES_NAME.ConvertToString();
                    var strValue = string.Empty;
                    data
                        .Where(y => y.IB_CODE == x.IB_CODE && y.SALES_NAME == x.SALES_NAME)
                        .ToList()
                        .ForEach(y =>
                        {
                            if (y.CUST_ID != null)
                            {
                            strValue += strValue == string.Empty ? "" : ",";
                                strValue += y.CUST_ID.Value.ConvertToString();
                            }
                        });
                    dataSales.Add(new ComboBoxData<string>()
                    {
                        Display = strDisplay,
                        Value = strValue
                    });
                });

            //  Bind Batch No
            this.cboSales.DataBindByList(
                data: dataSales,
                display: "Display",
                value: "Value",
                top: new ComboBoxData<string>() { Display = string.Empty, Value = string.Empty }
                );
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            var selection = this.dgv_header.Rows.Cast<DataGridViewRow>().Where(x => (bool?)x.Cells["check"].Value == true).ToList();
            var strErrList = string.Empty;
            if (selection.Count() > 0)
            {
                var dataUpdateFlag = new List<BL_BILL_MAIL_LOG>();
                selection.ForEach(x =>
                 {
                     var strMailSubject = x.Cells["DATA_YM"].Value.ConvertToString() + "對帳單";
                     var strCust = x.Cells["CUST_NAME"].Value.ConvertToString() + (x.Cells["CUST_NAME2"].Value.ConvertToString().Trim() != string.Empty ? "/" + x.Cells["CUST_NAME2"].Value.ConvertToString() : string.Empty);
                     //var strMailPws = x.Cells["CUST_NAME2"].Value.ConvertToString().Trim() != string.Empty ? "聯名戶ID" : "身分證字號";
                     var strMailPws = x.Cells["CUST_NAME2"].Value.ConvertToString().Trim() != string.Empty ? "<span style='color:red;'>聯名戶帳號</span>" : "身分證字號";
                     var strMailBody = "   親愛的客戶  " + strCust + "<br/><br/>" +
                         "　　現附上" + x.Cells["DATA_YM"].Value.ConvertToString() + "月份的月報表以供查閱, 如有任何疑問請與您的業務代理聯絡" + "<br/><br/>" +
                         "　　備註：開啟pdf檔附件時，輸入您的個人資料(" + strMailPws + ")，查看您的對帳單明細內容。<br/>　　另提醒您，已加密帳單開啟密碼之英文字母皆需輸入大寫。<br/><br/><br/>" +
                         "    客戶服務部 " + "<br/>" +
                         "    Best Leader Markets PTY Ltd";
                     List<string> strMailTo = new List<string>();
                     var strMailAddress = x.Cells["EMAIL"].Value.ConvertToString().Trim();
                     strMailTo.Add(strMailAddress);
                     //strMailTo.Add("ROSA7650@GMAIL.COM");

                     if (this.chkIsSendTest.Checked)
                     {
                         //  發送測試信
                         strMailBody = "系統測試信(Mail To:" + x.Cells["EMAIL"].Value.ConvertToString().Trim() + ")<br/>" + strMailBody;
                         strMailTo = new List<string>();
                         GetConfigValueByKey("MailTo_BillPDF_TestingUser")
                             .Split(new string[] { ";" }, StringSplitOptions.None)
                             .ToList()
                             .ForEach(y =>
                             {
                                 strMailTo.Add(y);
                             });
                     }

                     var strFileName = x.Cells["FILE_PATH"].Value.ConvertToString() + @"\" + x.Cells["FILE_NAME"].Value.ConvertToString();

                     if (strMailAddress != string.Empty)
                     {
                         if (SendMail(strMailTo, strMailSubject, strMailBody, strFileName, false))
                         {
                             if (this.cboBatchNo.Visible == true)
                             {
                                 dataUpdateFlag.Add(new BL_BILL_MAIL_LOG()
                                 {
                                     JOB_ID = x.Cells["JOB_ID"].Value.ConvertToString(),
                                     CUST_ID = x.Cells["CUST_ID"].Value.IsNumeric() ? decimal.Parse(x.Cells["CUST_ID"].Value.ConvertToString()) : (decimal?)null
                                 });
                             }
                         }
                         else
                         {
                             strErrList += strErrList == string.Empty ? "" : "、";
                             strErrList += strCust;
                         }
                     }
                     else
                     {
                         strErrList += strErrList == string.Empty ? "" : "、";
                         strErrList += strCust;
                     }
                 });

                if (this.cboBatchNo.Visible && dataUpdateFlag.Count() > 0 && this.chkIsSendTest.Checked == false)
                {
                    if (new logicCust(Global.IsTestMode).UpdateBillMailSendFlag(dataUpdateFlag, Global.str_UserName))
                    {
                        this.BindDataByBatchNo();
                    }
                }

                var strMSG = "Mail發送完畢";
                if (selection.Count() != dataUpdateFlag.Count())
                {
                    strMSG = "Mail發送完畢，部分郵件發送失敗, 請確認收件者資訊是否正確" + Environment.NewLine + Environment.NewLine +
                        "發送失敗名單如下：" + Environment.NewLine +
                        strErrList;
                }
                MessageBox.Show(strMSG);
            }
            else
            {
                MessageBox.Show("請選擇要發送的資料");
            }
        }


        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            this.dgv_header.Rows.Cast<DataGridViewRow>()
                .ForEach(x =>
                {
                    x.Cells["check"].Value = this.checkBox1.Checked;
                });
        }

        private void dgv_header_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                if ((bool?)this.dgv_header.Rows[e.RowIndex].Cells["check"].Value == true)
                {
                    this.dgv_header.Rows[e.RowIndex].Cells["check"].Value = false;
                }
                else
                {
                    this.dgv_header.Rows[e.RowIndex].Cells["check"].Value = true;
                }
            }
        }

        private void cboBatchNo_SelectedIndexChanged(object sender, EventArgs e)
        {


            this.BindDataByBatchNo();
        }

        private void cboSales_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.BindDataByBatchNo();
        }

        private void BindDataByBatchNo()
        {
            var data = new logicCust(Global.IsTestMode).GetBillMailDataByBatchNo(this.cboBatchNo.Text);

            if (this.cboSales.SelectedValue.ConvertToString().Trim() != string.Empty)
            {
                var liCustID = this.cboSales.SelectedValue.ConvertToString().Trim()
                                        .Split(new string[] { "," }, StringSplitOptions.None)
                                        .ToList();

                data = data.Where(x => liCustID.Where(y => (x.CUST_ID == null ? "" : x.CUST_ID.Value.ConvertToString()) == y).ToList().Count() > 0).ToList();
            }

            this.dgv_header.DataBindByList(data);
        }
    }
}
