﻿using MK.Demo.Logic;
using MK.Demo.Model;
using MK_DEMO.Destop.FormFunction;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MK.Demo.Utility;
using static MK.Demo.Data.enCommon;

namespace MK_DEMO.Destop.FormFunction
{
    public partial class FrmSelectSales_Internal : MK_DEMO.Destop.BaseForm
    {
        public string strIB_CODE;
        public string strEMP_CODE;
        public string strCNAME;
        public string strEMP_ID;
        public enFormStatus status;
        private bool _IsShowAll = false;
        private bool _IsMultiSelect = false;
        private string _ORG_CODE = "";

        public string strCNAME_ALL;
        public List<BL_EMP> liEmp;

        public FrmSelectSales_Internal(bool IsShowALL = false, bool IsMultiSelect = false, string strORG_CODE = "")
        {
            InitializeComponent();
            this.status = enFormStatus.Cancel;

            this.dgv_header.AutoGenerateColumns = false;
            this.dgv_header.MultiSelect = false;
            this.dgv_header.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this._IsShowAll = IsShowALL;
            this._IsMultiSelect = IsMultiSelect;
            this._ORG_CODE = strORG_CODE;
        }

        private void FrmSelectSales_Internal_Load(object sender, EventArgs e)
        {
            this.BindData();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.status = enFormStatus.Cancel;
            this.Close();
        }

        private void BindData()
        {
            var data = base.Emp.GetBL_EMP(this._ORG_CODE);
            //if (this._IsShowAll == false)
            //{
            //    data = data.Where(x => x.INTERNAL_IB_CODE.ConvertToString().Trim() != string.Empty).ToList();
            //}
            data = data.Where(x => x.INTERNAL_IB_CODE.ConvertToString().Trim() != string.Empty).ToList();
            //this.dgv_header.DataSource = data;
            this.dgv_header.DataBindByList(data);
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            var selection = this.dgv_header.Rows.Cast<DataGridViewRow>().Where(x => (bool?)x.Cells["check"].Value == true).ToList();
            if (this._IsMultiSelect == false)
            {

                if (selection.Count == 1)
                {
                    this.strIB_CODE = selection[0].Cells["INTERNAL_IB_CODE"].Value.ConvertToString();
                    this.strEMP_CODE = selection[0].Cells["EMP_CODE"].Value.ConvertToString();
                    this.strCNAME = selection[0].Cells["CNAME"].Value.ConvertToString();
                    this.strEMP_ID = selection[0].Cells["EMP_ID"].Value.ConvertToString();

                    this.status = enFormStatus.OK;
                    this.Close();
                }
                else
                {
                    MessageBox.Show("請單選");
                }
            }
            else
            {
                if (selection.Count == 0)
                {
                    MessageBox.Show("請選擇");
                }
                else
                {
                    this.strCNAME_ALL = string.Empty;
                    this.liEmp = new List<BL_EMP>();

                    selection.ForEach(x =>
                    {
                        this.strCNAME_ALL += this.strCNAME_ALL == string.Empty ? "" : ",";
                        this.strCNAME_ALL += x.Cells["CNAME"].Value.ConvertToString();

                        this.liEmp.Add(new BL_EMP()
                        {
                            IB_CODE = x.Cells["INTERNAL_IB_CODE"].Value.ConvertToString(),
                            EMP_CODE = x.Cells["EMP_CODE"].Value.ConvertToString(),
                            CNAME = x.Cells["CNAME"].Value.ConvertToString(),
                            EMP_ID = decimal.Parse(x.Cells["EMP_ID"].Value.ConvertToString())
                        });
                    });

                    this.status = enFormStatus.OK;
                    this.Close();
                }

            }

        }

        private void dgv_header_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                if (this._IsMultiSelect == false)
                {
                    Parallel.ForEach(this.dgv_header.Rows.Cast<DataGridViewRow>(), x =>
                    {
                        x.Cells["check"].Value = false;
                    });
                    this.dgv_header.Rows[e.RowIndex].Cells["check"].Value = true;
                }
                else
                {
                    this.dgv_header.Rows[e.RowIndex].Cells["check"].Value = (bool?)this.dgv_header.Rows[e.RowIndex].Cells["check"].Value == true ? false : true;
                }
            }
        }

        private void txtIB_CODE_TextChanged(object sender, EventArgs e)
        {
            var data = base.Emp.GetBL_EMP(this._ORG_CODE);
            data = data.Where(x => x.INTERNAL_IB_CODE.ConvertToString().Trim() != string.Empty).ToList();
            if (this.txtIB_CODE.Text.Trim() != string.Empty)
            {
                data = data
                    .Where(x => x.INTERNAL_IB_CODE.ConvertToString().ToUpper().IndexOf(this.txtIB_CODE.Text.Trim().ToUpper()) != -1)
                    .ToList();
            }
            //this.dgv_header.DataSource = data;
            this.dgv_header.DataBindByList(data);
        }
    }
}
