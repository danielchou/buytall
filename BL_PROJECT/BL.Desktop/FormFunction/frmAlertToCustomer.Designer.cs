﻿namespace MK_DEMO.Destop.FormFunction
{
    partial class frmAlertToCustomer
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblInfo = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.dgv_header = new System.Windows.Forms.DataGridView();
            this.btnSubmit_Resend = new System.Windows.Forms.Button();
            this.CUST_CNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUST_CNAME2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUST_ENAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID_NUMBER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WEB_PASSWORD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ORDER_CNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EMAIL_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EMAIL_2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UNION_ID_NUMBER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DEL = new System.Windows.Forms.DataGridViewLinkColumn();
            this.CUST_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_header)).BeginInit();
            this.SuspendLayout();
            // 
            // lblInfo
            // 
            this.lblInfo.AutoSize = true;
            this.lblInfo.Location = new System.Drawing.Point(12, 13);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(51, 19);
            this.lblInfo.TabIndex = 4;
            this.lblInfo.Text = "label1";
            // 
            // btnClose
            // 
            this.btnClose.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnClose.Location = new System.Drawing.Point(667, 412);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(141, 54);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "取消";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSubmit
            // 
            this.btnSubmit.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnSubmit.Location = new System.Drawing.Point(512, 412);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(141, 54);
            this.btnSubmit.TabIndex = 2;
            this.btnSubmit.Text = "送出通知";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // dgv_header
            // 
            this.dgv_header.AllowUserToAddRows = false;
            this.dgv_header.AllowUserToDeleteRows = false;
            this.dgv_header.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_header.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_header.ColumnHeadersHeight = 25;
            this.dgv_header.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv_header.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CUST_CNAME,
            this.CUST_CNAME2,
            this.CUST_ENAME,
            this.ID_NUMBER,
            this.WEB_PASSWORD,
            this.ORDER_CNT,
            this.EMAIL_1,
            this.EMAIL_2,
            this.UNION_ID_NUMBER,
            this.DEL,
            this.CUST_ID});
            this.dgv_header.Location = new System.Drawing.Point(12, 35);
            this.dgv_header.Name = "dgv_header";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.RoyalBlue;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Yellow;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_header.RowHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_header.RowHeadersWidth = 25;
            this.dgv_header.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgv_header.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(226)))), ((int)(((byte)(239)))));
            this.dgv_header.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_header.RowTemplate.Height = 24;
            this.dgv_header.Size = new System.Drawing.Size(1139, 362);
            this.dgv_header.TabIndex = 1;
            this.dgv_header.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_header_CellClick);
            this.dgv_header.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_header_CellContentClick);
            this.dgv_header.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_header_CellEnter);
            this.dgv_header.CellLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_header_CellLeave);
            this.dgv_header.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgv_header_EditingControlShowing);
            // 
            // btnSubmit_Resend
            // 
            this.btnSubmit_Resend.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnSubmit_Resend.Location = new System.Drawing.Point(357, 412);
            this.btnSubmit_Resend.Name = "btnSubmit_Resend";
            this.btnSubmit_Resend.Size = new System.Drawing.Size(141, 54);
            this.btnSubmit_Resend.TabIndex = 5;
            this.btnSubmit_Resend.Text = "重設密碼通知";
            this.btnSubmit_Resend.UseVisualStyleBackColor = true;
            this.btnSubmit_Resend.Click += new System.EventHandler(this.btnSubmit_Resend_Click);
            // 
            // CUST_CNAME
            // 
            this.CUST_CNAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.CUST_CNAME.DataPropertyName = "CUST_CNAME";
            this.CUST_CNAME.HeaderText = "客戶姓名";
            this.CUST_CNAME.Name = "CUST_CNAME";
            this.CUST_CNAME.ReadOnly = true;
            this.CUST_CNAME.Width = 115;
            // 
            // CUST_CNAME2
            // 
            this.CUST_CNAME2.DataPropertyName = "CUST_CNAME2";
            this.CUST_CNAME2.HeaderText = "客戶姓名2";
            this.CUST_CNAME2.Name = "CUST_CNAME2";
            this.CUST_CNAME2.ReadOnly = true;
            // 
            // CUST_ENAME
            // 
            this.CUST_ENAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.CUST_ENAME.DataPropertyName = "CUST_ENAME";
            this.CUST_ENAME.HeaderText = "英文姓名";
            this.CUST_ENAME.Name = "CUST_ENAME";
            this.CUST_ENAME.ReadOnly = true;
            this.CUST_ENAME.Width = 140;
            // 
            // ID_NUMBER
            // 
            this.ID_NUMBER.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ID_NUMBER.DataPropertyName = "ID_NUMBER";
            this.ID_NUMBER.HeaderText = "身分證字號";
            this.ID_NUMBER.Name = "ID_NUMBER";
            this.ID_NUMBER.ReadOnly = true;
            this.ID_NUMBER.Width = 110;
            // 
            // WEB_PASSWORD
            // 
            this.WEB_PASSWORD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.WEB_PASSWORD.DataPropertyName = "WEB_PASSWORD";
            this.WEB_PASSWORD.HeaderText = "預設密碼";
            this.WEB_PASSWORD.Name = "WEB_PASSWORD";
            this.WEB_PASSWORD.Width = 90;
            // 
            // ORDER_CNT
            // 
            this.ORDER_CNT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ORDER_CNT.DataPropertyName = "ORDER_CNT";
            this.ORDER_CNT.HeaderText = "合約數目";
            this.ORDER_CNT.Name = "ORDER_CNT";
            this.ORDER_CNT.ReadOnly = true;
            this.ORDER_CNT.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ORDER_CNT.Width = 90;
            // 
            // EMAIL_1
            // 
            this.EMAIL_1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.EMAIL_1.DataPropertyName = "EMAIL_1";
            this.EMAIL_1.HeaderText = "email";
            this.EMAIL_1.Name = "EMAIL_1";
            this.EMAIL_1.ReadOnly = true;
            this.EMAIL_1.Width = 170;
            // 
            // EMAIL_2
            // 
            this.EMAIL_2.DataPropertyName = "EMAIL_2";
            this.EMAIL_2.HeaderText = "email2";
            this.EMAIL_2.Name = "EMAIL_2";
            // 
            // UNION_ID_NUMBER
            // 
            this.UNION_ID_NUMBER.DataPropertyName = "UNION_ID_NUMBER";
            this.UNION_ID_NUMBER.HeaderText = "UNION_ID_NUMBER";
            this.UNION_ID_NUMBER.Name = "UNION_ID_NUMBER";
            this.UNION_ID_NUMBER.ReadOnly = true;
            // 
            // DEL
            // 
            this.DEL.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.DEL.HeaderText = "刪除";
            this.DEL.MinimumWidth = 55;
            this.DEL.Name = "DEL";
            this.DEL.ReadOnly = true;
            this.DEL.Width = 55;
            // 
            // CUST_ID
            // 
            this.CUST_ID.DataPropertyName = "CUST_ID";
            this.CUST_ID.HeaderText = "CUST_ID";
            this.CUST_ID.Name = "CUST_ID";
            this.CUST_ID.ReadOnly = true;
            this.CUST_ID.Visible = false;
            // 
            // frmAlertToCustomer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.ClientSize = new System.Drawing.Size(1163, 478);
            this.Controls.Add(this.btnSubmit_Resend);
            this.Controls.Add(this.lblInfo);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.dgv_header);
            this.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Name = "frmAlertToCustomer";
            this.Text = "客人WebApp登入資訊通知";
            this.Load += new System.EventHandler(this.frmAlertToCustomer_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_header)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv_header;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label lblInfo;
        private System.Windows.Forms.Button btnSubmit_Resend;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUST_CNAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUST_CNAME2;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUST_ENAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID_NUMBER;
        private System.Windows.Forms.DataGridViewTextBoxColumn WEB_PASSWORD;
        private System.Windows.Forms.DataGridViewTextBoxColumn ORDER_CNT;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMAIL_1;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMAIL_2;
        private System.Windows.Forms.DataGridViewTextBoxColumn UNION_ID_NUMBER;
        private System.Windows.Forms.DataGridViewLinkColumn DEL;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUST_ID;
    }
}
