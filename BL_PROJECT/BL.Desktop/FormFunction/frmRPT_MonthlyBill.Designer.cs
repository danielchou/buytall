﻿namespace MK_DEMO.Destop.FormFunction
{
    partial class frmRPT_MonthlyBill
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.reportViewer2 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.dteReportEndDate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.btnBringCust = new System.Windows.Forms.Button();
            this.txtCust = new System.Windows.Forms.TextBox();
            this.btnExportPDF = new System.Windows.Forms.Button();
            this.btnExportPDF_Encryption = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // reportViewer2
            // 
            this.reportViewer2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.reportViewer2.LocalReport.ReportEmbeddedResource = "MK_DEMO.Destop.RPT.RPTExpiredOrder.rdlc";
            this.reportViewer2.Location = new System.Drawing.Point(9, 71);
            this.reportViewer2.Margin = new System.Windows.Forms.Padding(2);
            this.reportViewer2.Name = "reportViewer2";
            this.reportViewer2.Size = new System.Drawing.Size(1008, 517);
            this.reportViewer2.TabIndex = 0;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("微軟正黑體", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button2.Location = new System.Drawing.Point(262, 6);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(76, 30);
            this.button2.TabIndex = 3;
            this.button2.Text = "Search";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("微軟正黑體", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button1.Location = new System.Drawing.Point(950, 624);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(101, 27);
            this.button1.TabIndex = 4;
            this.button1.Text = "Close";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微軟正黑體", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(8, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 19);
            this.label1.TabIndex = 6;
            this.label1.Text = "報表結算日";
            // 
            // dteReportEndDate
            // 
            this.dteReportEndDate.Font = new System.Drawing.Font("微軟正黑體", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.dteReportEndDate.Location = new System.Drawing.Point(96, 8);
            this.dteReportEndDate.Name = "dteReportEndDate";
            this.dteReportEndDate.Size = new System.Drawing.Size(152, 27);
            this.dteReportEndDate.TabIndex = 125;
            this.dteReportEndDate.ValueChanged += new System.EventHandler(this.dteReportEndDate_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微軟正黑體", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(8, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 19);
            this.label2.TabIndex = 126;
            this.label2.Text = "客戶";
            // 
            // btnBringCust
            // 
            this.btnBringCust.Font = new System.Drawing.Font("微軟正黑體", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnBringCust.Location = new System.Drawing.Point(52, 40);
            this.btnBringCust.Margin = new System.Windows.Forms.Padding(2);
            this.btnBringCust.Name = "btnBringCust";
            this.btnBringCust.Size = new System.Drawing.Size(40, 30);
            this.btnBringCust.TabIndex = 127;
            this.btnBringCust.Text = "...";
            this.btnBringCust.UseVisualStyleBackColor = true;
            this.btnBringCust.Click += new System.EventHandler(this.btnBringCust_Click);
            // 
            // txtCust
            // 
            this.txtCust.Enabled = false;
            this.txtCust.Font = new System.Drawing.Font("微軟正黑體", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txtCust.Location = new System.Drawing.Point(97, 40);
            this.txtCust.Name = "txtCust";
            this.txtCust.Size = new System.Drawing.Size(919, 27);
            this.txtCust.TabIndex = 128;
            // 
            // btnExportPDF
            // 
            this.btnExportPDF.Font = new System.Drawing.Font("微軟正黑體", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnExportPDF.Location = new System.Drawing.Point(355, 6);
            this.btnExportPDF.Margin = new System.Windows.Forms.Padding(2);
            this.btnExportPDF.Name = "btnExportPDF";
            this.btnExportPDF.Size = new System.Drawing.Size(114, 30);
            this.btnExportPDF.TabIndex = 129;
            this.btnExportPDF.Text = "轉存 PDF";
            this.btnExportPDF.UseVisualStyleBackColor = true;
            this.btnExportPDF.Click += new System.EventHandler(this.btnExportPDF_Click);
            // 
            // btnExportPDF_Encryption
            // 
            this.btnExportPDF_Encryption.Font = new System.Drawing.Font("微軟正黑體", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnExportPDF_Encryption.Location = new System.Drawing.Point(486, 6);
            this.btnExportPDF_Encryption.Margin = new System.Windows.Forms.Padding(2);
            this.btnExportPDF_Encryption.Name = "btnExportPDF_Encryption";
            this.btnExportPDF_Encryption.Size = new System.Drawing.Size(125, 30);
            this.btnExportPDF_Encryption.TabIndex = 130;
            this.btnExportPDF_Encryption.Text = "轉存 PDF(加密)";
            this.btnExportPDF_Encryption.UseVisualStyleBackColor = true;
            this.btnExportPDF_Encryption.Click += new System.EventHandler(this.btnExportPDF_Encryption_Click);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("微軟正黑體", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button3.Location = new System.Drawing.Point(626, 6);
            this.button3.Margin = new System.Windows.Forms.Padding(2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(167, 30);
            this.button3.TabIndex = 131;
            this.button3.Text = "轉存 PDF (加密) 並發送";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Visible = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("微軟正黑體", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button4.Location = new System.Drawing.Point(806, 5);
            this.button4.Margin = new System.Windows.Forms.Padding(2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(210, 30);
            this.button4.TabIndex = 132;
            this.button4.Text = "發送 PDF 對帳單";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // frmRPT_MonthlyBill
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1028, 599);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.btnExportPDF_Encryption);
            this.Controls.Add(this.btnExportPDF);
            this.Controls.Add(this.txtCust);
            this.Controls.Add(this.btnBringCust);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dteReportEndDate);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.reportViewer2);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "frmRPT_MonthlyBill";
            this.Text = "月對帳單(客戶)";
            this.Load += new System.EventHandler(this.frmRPT_MonthlyBill_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dteReportEndDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnBringCust;
        private System.Windows.Forms.TextBox txtCust;
        private System.Windows.Forms.Button btnExportPDF;
        private System.Windows.Forms.Button btnExportPDF_Encryption;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
    }
}