﻿using MK.Demo.Logic;
using MK.Demo.Model;
using MK_DEMO.Destop.FormFunction;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MK.Demo.Utility;

namespace MK_DEMO.Destop.FormFunction
{
    public partial class frmCustDetail : MK_DEMO.Destop.BaseForm
    {

        private string CustID;

        public frmCustDetail(string strCustID)
        {
            InitializeComponent();

            this.CustID = strCustID;

            this.dteBirthday.Value = DateTime.Now;
            this.dteBirthday.Text = string.Empty;
            this.dteBirthday.Format = DateTimePickerFormat.Custom;
            this.dteBirthday.CustomFormat = " ";
        }

        private void frmCustDetail_Load(object sender, EventArgs e)
        {
            this.picLoder.BringToFront();

            this.BindCustData();
            this.BindBankData();

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BindCustData()
        {
            var strORG = Global.ORG_CODE;

            var data = base.Cust.GetCustData
                (
                    strCustID: this.CustID,
                    strORG: strORG
                );
            Parallel.ForEach(
                this.tabPage1.Controls.Cast<Control>()
                    .Where(x => x.GetType() == typeof(TextBox)), x =>
                        {
                            x.Text = string.Empty;
                        });

            if (data.Count > 0)
            {
                var dataSingle = data[0];

                this.txtCUST_CNAME.Text = dataSingle.CUST_CNAME.ConvertToString();
                this.txtCUST_ENAME.Text = dataSingle.CUST_ENAME.ConvertToString();
                if (dataSingle.SEX.ConvertToString() == "M")
                {
                    this.rdoWoman.Checked = false;
                    this.rdoMan.Checked = true;
                }
                else if (dataSingle.SEX.ConvertToString() == "F")
                {
                    this.rdoMan.Checked = false;
                    this.rdoWoman.Checked = true;
                }


                if (dataSingle.DATE_OF_BIRTH != null)
                {
                    this.dteBirthday.Format = DateTimePickerFormat.Long;
                    this.dteBirthday.Value = dataSingle.DATE_OF_BIRTH.Value;
                    this.dteBirthday.Text = dataSingle.DATE_OF_BIRTH.Value.ToString("yyyy/MM/dd");
                }

                this.txtID_NUMBER.Text = dataSingle.ID_NUMBER.ConvertToString();
                this.txtPASSPORT.Text = dataSingle.PASSPORT.ConvertToString();
                this.txtNATION.Text = dataSingle.PASSPORT_REGION.ConvertToString();
                this.txtPHONE.Text = dataSingle.PHONE_1.ConvertToString();
                this.txtEMAIL.Text = dataSingle.EMAIL_1.ConvertToString();
                this.txtAddress.Text = dataSingle.C_ADDRESS.ConvertToString();

            }
        }

        private void BindBankData()
        {
            var dataBank = base.Cust.GetBankDataByCustID(this.CustID);
            Parallel.ForEach(
              this.tabPage2.Controls.Cast<Control>()
                  .Where(x => x.GetType() == typeof(TextBox)), x =>
                  {
                      x.Text = string.Empty;
                  });

            if (dataBank.Count > 0)
            {
                var dataBankSingle = dataBank[0];

                this.txtBANK_CNAME.Text = dataBankSingle.BANK_CNAME.ConvertToString();
                this.txtBRANCH_CNAME.Text = dataBankSingle.BRANCH_CNAME.ConvertToString();
                this.txtACCOUNT_CNAME.Text = dataBankSingle.ACCOUNT_CNAME.ConvertToString();
                this.txtACCOUNT.Text = dataBankSingle.ACCOUNT.ConvertToString();
                this.txtBANK_C_ADDRESS.Text = dataBankSingle.BANK_C_ADDRESS.ConvertToString();
                this.txtSWIFT_CODE.Text = dataBankSingle.SWIFT_CODE.ConvertToString();
            }
        }




    }
}
