﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using MK.Demo.Utility;
using MK.Demo.Model;
using MK.Demo.Logic;
using System.Threading.Tasks;
using System.Linq;
using static MK.Demo.Data.enCommon;



namespace MK_DEMO.Destop.FormFunction
{
    public partial class frmEMP_MANAGER_Q : MK_DEMO.Destop.BaseForm
    {
        public frmEMP_MANAGER_Q()
        {
            InitializeComponent();

            this.dgv_header.AutoGenerateColumns = false;
            this.dgv_header.MultiSelect = false;
            this.dgv_header.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        private void frmEMP_MANAGER_Q_Load(object sender, EventArgs e)
        {
            this.CleanCondition();
            this.BindData();
        }


        private void dte_ValueChanged(object sender, EventArgs e)
        {
            (sender as DateTimePicker).Format = DateTimePickerFormat.Long;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.BindData();
        }

        private void BindData()
        {
            string EmpCode = this.txtEmpCode.Text.Trim().ToUpper();
            string EmpName = this.txtEmpName.Text.Trim().ToUpper();
            string IB_CODE = this.txtIB_CODE.Text.Trim().ToUpper();
            string ID_NO = this.txtID_NO.Text.Trim().ToUpper();
            DateTime? HIRE_DATE_S = (DateTime?)null;
            DateTime? HIRE_DATE_E = (DateTime?)null;
            DateTime? QUIT_DATE_S = (DateTime?)null;
            DateTime? QUIT_DATE_E = (DateTime?)null;
            DateTime dtTry;
            if (DateTime.TryParse(this.dteHIRE_DATE_S.Text.Trim(), out dtTry)) HIRE_DATE_S = dtTry;
            if (DateTime.TryParse(this.dteHIRE_DATE_E.Text.Trim(), out dtTry)) HIRE_DATE_E = dtTry;
            if (DateTime.TryParse(this.dteQUIT_DATE_S.Text.Trim(), out dtTry)) QUIT_DATE_S = dtTry;
            if (DateTime.TryParse(this.dteQUIT_DATE_E.Text.Trim(), out dtTry)) QUIT_DATE_E = dtTry;

            var param = new object[] 
                {
                    EmpCode,
                    EmpName,
                    IB_CODE,
                    ID_NO,
                    HIRE_DATE_S,
                    HIRE_DATE_E,
                    QUIT_DATE_S,
                    QUIT_DATE_E,
                    Global.ORG_CODE.ConvertToString()
                };
            this.picLoder.Visible = true;
            this.picLoder.BringToFront();
            if (this.backgroundWorker1.IsBusy == false)
            {
                this.backgroundWorker1.RunWorkerAsync(param);
            }
            //var data = base.Cust.SelectBL_EMP
            //    (
            //        EmpCode,
            //        EmpName,
            //        IB_CODE,
            //        ID_NO,
            //        HIRE_DATE_S,
            //        HIRE_DATE_E,
            //        QUIT_DATE_S,
            //        QUIT_DATE_E
            //    );
            //this.dgv_header.DataBindByList(data);
            //this.dgv_header.Rows.Cast<DataGridViewRow>()
            //    .ForEach(x =>
            //    {
            //        x.Cells["DoModify"].Value = "修改";
            //        x.Cells["DoDel"].Value = "刪除";
            //    });

            //if (data.Count == 0)
            //{
            //    MessageBox.Show("查無符合資料");
            //}
        }


        private void btnCleanCondition_Click(object sender, EventArgs e)
        {
            this.CleanCondition();
        }

        private void CleanCondition()
        {
            var dte = new List<DateTimePicker>()
            {
                this.dteHIRE_DATE_S,
                this.dteHIRE_DATE_E,
                this.dteQUIT_DATE_S,
                this.dteQUIT_DATE_E
            };
            var txt = new List<TextBox>()
            {
                this.txtEmpCode,
                this.txtEmpName,
                this.txtIB_CODE,
                this.txtID_NO
            };

            dte.ForEach(x =>
            {
                x.Value = DateTime.Now;
                x.Text = string.Empty;
                x.Format = DateTimePickerFormat.Custom;
                x.CustomFormat = " ";
            });
            txt.ForEach(x =>
            {
                x.Text = string.Empty;
            });
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            if (e.Argument as object[] != null)
            {
                var param = e.Argument as object[];
                if (param.Length >= 9)
                {
                    string EmpCode = param[0] as string;
                    string EmpName = param[1] as string;
                    string IB_CODE = param[2] as string;
                    string ID_NO = param[3] as string;
                    DateTime? HIRE_DATE_S = param[4] as DateTime?;
                    DateTime? HIRE_DATE_E = param[5] as DateTime?;
                    DateTime? QUIT_DATE_S = param[6] as DateTime?;
                    DateTime? QUIT_DATE_E = param[7] as DateTime?;
                    string ORG_CODE = param[8] as string;
                    var data = base.Cust.SelectBL_EMP
                        (
                            EmpCode,
                            EmpName,
                            IB_CODE,
                            ID_NO,
                            HIRE_DATE_S,
                            HIRE_DATE_E,
                            QUIT_DATE_S,
                            QUIT_DATE_E,
                            ORG_CODE
                        );
                    e.Result = data;
                }
            }
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.dgv_header.DataClean();
            if (e.Result != null)
            {
                if (e.Result as List<BL_EMP> != null)
                {
                    var data = e.Result as List<BL_EMP>;
                    this.dgv_header.DataBindByList(data);
                }
            }


            this.dgv_header.Rows.Cast<DataGridViewRow>()
                .ForEach(x =>
                {
                    x.Cells["DoModify"].Value = "修改";
                    x.Cells["DoDel"].Value = "刪除";
                });

            if (this.dgv_header.Rows.Count == 0)
            {
                MessageBox.Show("查無符合資料");
            }
            this.picLoder.Visible = false;
        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            //  新增員工
            var frm = new frmEMP_MANAGER_A() { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
            frm.ShowDialog();
            frm.Dispose();
            frm = null;

            this.BindData();
        }

        private void dgv_header_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                switch (this.dgv_header.Columns[e.ColumnIndex].Name.ConvertToString().ToUpper().Trim())
                {
                    case "DOMODIFY":
                        #region 修改
                        var frm = new frmEMP_MANAGER_A(this.dgv_header.Rows[e.RowIndex].Cells["EMP_ID"].Value.ConvertToString().Trim())
                        { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
                        frm.ShowDialog();
                        frm.Dispose();
                        frm = null;

                        this.BindData();
                        #endregion
                        break;
                    case "DODEL":
                        #region 刪除
                        var strDelMsg = "請確定是否要刪除此筆資料?" + Environment.NewLine +
                            "  員工:" + this.dgv_header.Rows[e.RowIndex].Cells["CNAME"].Value.ConvertToString().Trim();

                        if (MessageBox.Show(strDelMsg, "系統訊息", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        {
                            if (base.Cust.DeleteBL_EMPByEMP_ID(this.dgv_header.Rows[e.RowIndex].Cells["EMP_ID"].Value.ConvertToString()))
                            {
                                MessageBox.Show("刪除成功");
                                this.BindData();
                            }
                            else
                            {
                                MessageBox.Show("刪除失敗");
                            }

                        }
                        #endregion
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
