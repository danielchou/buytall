﻿namespace MK_DEMO.Destop.FormFunction
{
    partial class frmCustDetail
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCustDetail));
            this.btnClose = new System.Windows.Forms.Button();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.picLoder = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCUST_CNAME = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dteBirthday = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPASSPORT = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtNATION = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtPHONE = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtEMAIL = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.rdoWoman = new System.Windows.Forms.RadioButton();
            this.rdoMan = new System.Windows.Forms.RadioButton();
            this.label24 = new System.Windows.Forms.Label();
            this.txtID_NUMBER = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtCUST_ENAME = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label11 = new System.Windows.Forms.Label();
            this.txtBANK_CNAME = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtBRANCH_CNAME = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtACCOUNT_CNAME = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtACCOUNT = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtSWIFT_CODE = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtBANK_C_ADDRESS = new System.Windows.Forms.TextBox();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLoder)).BeginInit();
            this.panel5.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(347, 260);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(248, 37);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "關閉";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.picLoder);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.txtCUST_CNAME);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.dteBirthday);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.txtPASSPORT);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.txtNATION);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.txtPHONE);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.txtEMAIL);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.txtAddress);
            this.tabPage1.Controls.Add(this.panel5);
            this.tabPage1.Controls.Add(this.label24);
            this.tabPage1.Controls.Add(this.txtID_NUMBER);
            this.tabPage1.Controls.Add(this.label25);
            this.tabPage1.Controls.Add(this.txtCUST_ENAME);
            this.tabPage1.Controls.Add(this.label26);
            this.tabPage1.Location = new System.Drawing.Point(4, 26);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage1.Size = new System.Drawing.Size(581, 218);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "基本資料";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // picLoder
            // 
            this.picLoder.BackColor = System.Drawing.Color.Transparent;
            this.picLoder.Image = ((System.Drawing.Image)(resources.GetObject("picLoder.Image")));
            this.picLoder.Location = new System.Drawing.Point(244, 98);
            this.picLoder.Name = "picLoder";
            this.picLoder.Size = new System.Drawing.Size(174, 19);
            this.picLoder.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picLoder.TabIndex = 135;
            this.picLoder.TabStop = false;
            this.picLoder.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 18);
            this.label1.TabIndex = 115;
            this.label1.Text = "姓名";
            // 
            // txtCUST_CNAME
            // 
            this.txtCUST_CNAME.Enabled = false;
            this.txtCUST_CNAME.Location = new System.Drawing.Point(119, 10);
            this.txtCUST_CNAME.Name = "txtCUST_CNAME";
            this.txtCUST_CNAME.Size = new System.Drawing.Size(154, 25);
            this.txtCUST_CNAME.TabIndex = 116;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(277, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 18);
            this.label3.TabIndex = 117;
            this.label3.Text = "出生年月日";
            // 
            // dteBirthday
            // 
            this.dteBirthday.Enabled = false;
            this.dteBirthday.Location = new System.Drawing.Point(375, 42);
            this.dteBirthday.Name = "dteBirthday";
            this.dteBirthday.Size = new System.Drawing.Size(186, 25);
            this.dteBirthday.TabIndex = 118;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(277, 77);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 18);
            this.label4.TabIndex = 119;
            this.label4.Text = "護照號碼";
            // 
            // txtPASSPORT
            // 
            this.txtPASSPORT.Enabled = false;
            this.txtPASSPORT.Location = new System.Drawing.Point(375, 74);
            this.txtPASSPORT.Name = "txtPASSPORT";
            this.txtPASSPORT.Size = new System.Drawing.Size(186, 25);
            this.txtPASSPORT.TabIndex = 120;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 77);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 18);
            this.label5.TabIndex = 121;
            this.label5.Text = "國籍";
            // 
            // txtNATION
            // 
            this.txtNATION.Enabled = false;
            this.txtNATION.Location = new System.Drawing.Point(119, 74);
            this.txtNATION.Name = "txtNATION";
            this.txtNATION.Size = new System.Drawing.Size(154, 25);
            this.txtNATION.TabIndex = 122;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 109);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 18);
            this.label6.TabIndex = 123;
            this.label6.Text = "聯絡電話";
            // 
            // txtPHONE
            // 
            this.txtPHONE.Enabled = false;
            this.txtPHONE.Location = new System.Drawing.Point(119, 106);
            this.txtPHONE.Name = "txtPHONE";
            this.txtPHONE.Size = new System.Drawing.Size(154, 25);
            this.txtPHONE.TabIndex = 124;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 142);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(92, 18);
            this.label7.TabIndex = 125;
            this.label7.Text = "電子郵件信箱";
            // 
            // txtEMAIL
            // 
            this.txtEMAIL.Enabled = false;
            this.txtEMAIL.Location = new System.Drawing.Point(119, 139);
            this.txtEMAIL.Name = "txtEMAIL";
            this.txtEMAIL.Size = new System.Drawing.Size(442, 25);
            this.txtEMAIL.TabIndex = 126;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 174);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 18);
            this.label8.TabIndex = 127;
            this.label8.Text = "居住地址";
            // 
            // txtAddress
            // 
            this.txtAddress.Enabled = false;
            this.txtAddress.Location = new System.Drawing.Point(119, 171);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(442, 25);
            this.txtAddress.TabIndex = 128;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.rdoWoman);
            this.panel5.Controls.Add(this.rdoMan);
            this.panel5.Location = new System.Drawing.Point(376, 8);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(117, 28);
            this.panel5.TabIndex = 129;
            // 
            // rdoWoman
            // 
            this.rdoWoman.AutoSize = true;
            this.rdoWoman.Enabled = false;
            this.rdoWoman.Location = new System.Drawing.Point(3, 3);
            this.rdoWoman.Name = "rdoWoman";
            this.rdoWoman.Size = new System.Drawing.Size(54, 22);
            this.rdoWoman.TabIndex = 11;
            this.rdoWoman.TabStop = true;
            this.rdoWoman.Text = "女士";
            this.rdoWoman.UseVisualStyleBackColor = true;
            // 
            // rdoMan
            // 
            this.rdoMan.AutoSize = true;
            this.rdoMan.Enabled = false;
            this.rdoMan.Location = new System.Drawing.Point(63, 3);
            this.rdoMan.Name = "rdoMan";
            this.rdoMan.Size = new System.Drawing.Size(54, 22);
            this.rdoMan.TabIndex = 12;
            this.rdoMan.TabStop = true;
            this.rdoMan.Text = "男士";
            this.rdoMan.UseVisualStyleBackColor = true;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(277, 109);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(78, 18);
            this.label24.TabIndex = 130;
            this.label24.Text = "身分證號碼";
            // 
            // txtID_NUMBER
            // 
            this.txtID_NUMBER.Enabled = false;
            this.txtID_NUMBER.Location = new System.Drawing.Point(375, 106);
            this.txtID_NUMBER.Name = "txtID_NUMBER";
            this.txtID_NUMBER.Size = new System.Drawing.Size(186, 25);
            this.txtID_NUMBER.TabIndex = 131;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(7, 45);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(58, 18);
            this.label25.TabIndex = 132;
            this.label25.Text = "姓名(英)";
            // 
            // txtCUST_ENAME
            // 
            this.txtCUST_ENAME.Enabled = false;
            this.txtCUST_ENAME.Location = new System.Drawing.Point(119, 42);
            this.txtCUST_ENAME.Name = "txtCUST_ENAME";
            this.txtCUST_ENAME.Size = new System.Drawing.Size(154, 25);
            this.txtCUST_ENAME.TabIndex = 133;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(277, 13);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(36, 18);
            this.label26.TabIndex = 134;
            this.label26.Text = "性別";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(6, 4);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(589, 248);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label11);
            this.tabPage2.Controls.Add(this.txtBANK_CNAME);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.txtBRANCH_CNAME);
            this.tabPage2.Controls.Add(this.label12);
            this.tabPage2.Controls.Add(this.txtACCOUNT_CNAME);
            this.tabPage2.Controls.Add(this.label14);
            this.tabPage2.Controls.Add(this.txtACCOUNT);
            this.tabPage2.Controls.Add(this.label13);
            this.tabPage2.Controls.Add(this.txtSWIFT_CODE);
            this.tabPage2.Controls.Add(this.label15);
            this.tabPage2.Controls.Add(this.txtBANK_C_ADDRESS);
            this.tabPage2.Location = new System.Drawing.Point(4, 26);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(581, 218);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "銀行資料";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 10);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(64, 18);
            this.label11.TabIndex = 37;
            this.label11.Text = "銀行名稱";
            // 
            // txtBANK_CNAME
            // 
            this.txtBANK_CNAME.Enabled = false;
            this.txtBANK_CNAME.Location = new System.Drawing.Point(100, 7);
            this.txtBANK_CNAME.Name = "txtBANK_CNAME";
            this.txtBANK_CNAME.Size = new System.Drawing.Size(152, 25);
            this.txtBANK_CNAME.TabIndex = 38;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(266, 10);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(64, 18);
            this.label10.TabIndex = 39;
            this.label10.Text = "分行名稱";
            // 
            // txtBRANCH_CNAME
            // 
            this.txtBRANCH_CNAME.Enabled = false;
            this.txtBRANCH_CNAME.Location = new System.Drawing.Point(373, 7);
            this.txtBRANCH_CNAME.Name = "txtBRANCH_CNAME";
            this.txtBRANCH_CNAME.Size = new System.Drawing.Size(186, 25);
            this.txtBRANCH_CNAME.TabIndex = 40;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 41);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(106, 18);
            this.label12.TabIndex = 41;
            this.label12.Text = "帳戶持有人姓名";
            // 
            // txtACCOUNT_CNAME
            // 
            this.txtACCOUNT_CNAME.Enabled = false;
            this.txtACCOUNT_CNAME.Location = new System.Drawing.Point(124, 38);
            this.txtACCOUNT_CNAME.Name = "txtACCOUNT_CNAME";
            this.txtACCOUNT_CNAME.Size = new System.Drawing.Size(189, 25);
            this.txtACCOUNT_CNAME.TabIndex = 42;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(3, 73);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(64, 18);
            this.label14.TabIndex = 43;
            this.label14.Text = "銀行帳戶";
            // 
            // txtACCOUNT
            // 
            this.txtACCOUNT.Enabled = false;
            this.txtACCOUNT.Location = new System.Drawing.Point(100, 70);
            this.txtACCOUNT.Name = "txtACCOUNT";
            this.txtACCOUNT.Size = new System.Drawing.Size(152, 25);
            this.txtACCOUNT.TabIndex = 44;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(266, 73);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(94, 18);
            this.label13.TabIndex = 45;
            this.label13.Text = "SWIFT_CODE";
            // 
            // txtSWIFT_CODE
            // 
            this.txtSWIFT_CODE.Enabled = false;
            this.txtSWIFT_CODE.Location = new System.Drawing.Point(373, 70);
            this.txtSWIFT_CODE.Name = "txtSWIFT_CODE";
            this.txtSWIFT_CODE.Size = new System.Drawing.Size(186, 25);
            this.txtSWIFT_CODE.TabIndex = 46;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(3, 104);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(64, 18);
            this.label15.TabIndex = 47;
            this.label15.Text = "銀行地址";
            // 
            // txtBANK_C_ADDRESS
            // 
            this.txtBANK_C_ADDRESS.Enabled = false;
            this.txtBANK_C_ADDRESS.Location = new System.Drawing.Point(100, 101);
            this.txtBANK_C_ADDRESS.Name = "txtBANK_C_ADDRESS";
            this.txtBANK_C_ADDRESS.Size = new System.Drawing.Size(459, 25);
            this.txtBANK_C_ADDRESS.TabIndex = 48;
            // 
            // frmCustDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.ClientSize = new System.Drawing.Size(602, 304);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmCustDetail";
            this.Text = "穩健型商品客戶-詳細";
            this.Load += new System.EventHandler(this.frmCustDetail_Load);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLoder)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCUST_CNAME;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dteBirthday;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtPASSPORT;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtNATION;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtPHONE;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtEMAIL;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.RadioButton rdoWoman;
        private System.Windows.Forms.RadioButton rdoMan;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtID_NUMBER;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtCUST_ENAME;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.PictureBox picLoder;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtBANK_CNAME;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtBRANCH_CNAME;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtACCOUNT_CNAME;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtACCOUNT;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtSWIFT_CODE;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtBANK_C_ADDRESS;
    }
}
