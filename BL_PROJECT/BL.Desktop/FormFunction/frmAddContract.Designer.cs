﻿namespace MK_DEMO.Destop.FormFunction
{
    partial class frmAddContract
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAddContract));
            this.btnModifyHist = new System.Windows.Forms.Button();
            this.btnModifyALL = new System.Windows.Forms.Button();
            this.btnTransOrder = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label113 = new System.Windows.Forms.Label();
            this.txtPOSTAL_CODE2 = new System.Windows.Forms.TextBox();
            this.label112 = new System.Windows.Forms.Label();
            this.txtPOSTAL_CODE = new System.Windows.Forms.TextBox();
            this.btnDelBirthday2 = new System.Windows.Forms.Button();
            this.btnDelBirthday = new System.Windows.Forms.Button();
            this.panContractInfo = new System.Windows.Forms.Panel();
            this.panel21 = new System.Windows.Forms.Panel();
            this.rdoWebDepositType_Null = new System.Windows.Forms.RadioButton();
            this.rdoWebDepositType_Y = new System.Windows.Forms.RadioButton();
            this.label192 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label30 = new System.Windows.Forms.Label();
            this.txtMAIL_TYPE_END = new System.Windows.Forms.TextBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.chkMAIL_TYPE_MONTHLY1 = new System.Windows.Forms.RadioButton();
            this.chkMAIL_TYPE_MONTHLY2 = new System.Windows.Forms.RadioButton();
            this.panel8 = new System.Windows.Forms.Panel();
            this.chkMAIL_TYPE_END1 = new System.Windows.Forms.RadioButton();
            this.chkMAIL_TYPE_END2 = new System.Windows.Forms.RadioButton();
            this.chkMAIL_TYPE_END3 = new System.Windows.Forms.RadioButton();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCUST_CNAME = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dteBirthday = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPASSPORT = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtNATION = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtPHONE = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtEMAIL = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.rdoWoman = new System.Windows.Forms.RadioButton();
            this.rdoMan = new System.Windows.Forms.RadioButton();
            this.label24 = new System.Windows.Forms.Label();
            this.txtID_NUMBER = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtCUST_ENAME = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label22 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.txtCUST_CNAME2 = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.dteBirthday2 = new System.Windows.Forms.DateTimePicker();
            this.label27 = new System.Windows.Forms.Label();
            this.txtPASSPORT2 = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtNATION2 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtPHONE2 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtEMAIL2 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtAddress2 = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.rdoWoman2 = new System.Windows.Forms.RadioButton();
            this.rdoMan2 = new System.Windows.Forms.RadioButton();
            this.label18 = new System.Windows.Forms.Label();
            this.txtID_NUMBER2 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtCUST_ENAME2 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnImportToNZD2 = new System.Windows.Forms.Button();
            this.btnImportToNZD1 = new System.Windows.Forms.Button();
            this.label220 = new System.Windows.Forms.Label();
            this.txtACCOUNT_ENAME2_7 = new System.Windows.Forms.TextBox();
            this.label222 = new System.Windows.Forms.Label();
            this.txtACCOUNT_ENAME_7 = new System.Windows.Forms.TextBox();
            this.label223 = new System.Windows.Forms.Label();
            this.txtBANK_E_ADDRESS2_7 = new System.Windows.Forms.TextBox();
            this.label224 = new System.Windows.Forms.Label();
            this.txtBANK_ENAME2_7 = new System.Windows.Forms.TextBox();
            this.label225 = new System.Windows.Forms.Label();
            this.txtBRANCH_ENAME2_7 = new System.Windows.Forms.TextBox();
            this.label226 = new System.Windows.Forms.Label();
            this.txtBANK_E_ADDRESS_7 = new System.Windows.Forms.TextBox();
            this.label227 = new System.Windows.Forms.Label();
            this.txtBANK_ENAME_7 = new System.Windows.Forms.TextBox();
            this.label228 = new System.Windows.Forms.Label();
            this.txtBRANCH_ENAME_7 = new System.Windows.Forms.TextBox();
            this.panel25 = new System.Windows.Forms.Panel();
            this.label229 = new System.Windows.Forms.Label();
            this.label230 = new System.Windows.Forms.Label();
            this.panel26 = new System.Windows.Forms.Panel();
            this.label231 = new System.Windows.Forms.Label();
            this.txtBANK_CNAME2_7 = new System.Windows.Forms.TextBox();
            this.label232 = new System.Windows.Forms.Label();
            this.label233 = new System.Windows.Forms.Label();
            this.txtBANK_CNAME_7 = new System.Windows.Forms.TextBox();
            this.txtBRANCH_CNAME2_7 = new System.Windows.Forms.TextBox();
            this.label234 = new System.Windows.Forms.Label();
            this.label235 = new System.Windows.Forms.Label();
            this.txtBRANCH_CNAME_7 = new System.Windows.Forms.TextBox();
            this.txtACCOUNT_CNAME2_7 = new System.Windows.Forms.TextBox();
            this.label236 = new System.Windows.Forms.Label();
            this.label237 = new System.Windows.Forms.Label();
            this.txtACCOUNT_CNAME_7 = new System.Windows.Forms.TextBox();
            this.txtACCOUNT2_7 = new System.Windows.Forms.TextBox();
            this.label238 = new System.Windows.Forms.Label();
            this.label239 = new System.Windows.Forms.Label();
            this.txtACCOUNT_7 = new System.Windows.Forms.TextBox();
            this.txtSWIFT_CODE2_7 = new System.Windows.Forms.TextBox();
            this.label240 = new System.Windows.Forms.Label();
            this.label241 = new System.Windows.Forms.Label();
            this.txtSWIFT_CODE_7 = new System.Windows.Forms.TextBox();
            this.txtBANK_C_ADDRESS2_7 = new System.Windows.Forms.TextBox();
            this.label242 = new System.Windows.Forms.Label();
            this.txtBANK_C_ADDRESS_7 = new System.Windows.Forms.TextBox();
            this.btnImportToJPY2 = new System.Windows.Forms.Button();
            this.btnImportToJPY1 = new System.Windows.Forms.Button();
            this.label194 = new System.Windows.Forms.Label();
            this.txtACCOUNT_ENAME2_6 = new System.Windows.Forms.TextBox();
            this.label195 = new System.Windows.Forms.Label();
            this.txtACCOUNT_ENAME_6 = new System.Windows.Forms.TextBox();
            this.label196 = new System.Windows.Forms.Label();
            this.txtBANK_E_ADDRESS2_6 = new System.Windows.Forms.TextBox();
            this.label197 = new System.Windows.Forms.Label();
            this.txtBANK_ENAME2_6 = new System.Windows.Forms.TextBox();
            this.label198 = new System.Windows.Forms.Label();
            this.txtBRANCH_ENAME2_6 = new System.Windows.Forms.TextBox();
            this.label199 = new System.Windows.Forms.Label();
            this.txtBANK_E_ADDRESS_6 = new System.Windows.Forms.TextBox();
            this.label200 = new System.Windows.Forms.Label();
            this.txtBANK_ENAME_6 = new System.Windows.Forms.TextBox();
            this.label201 = new System.Windows.Forms.Label();
            this.txtBRANCH_ENAME_6 = new System.Windows.Forms.TextBox();
            this.panel22 = new System.Windows.Forms.Panel();
            this.label202 = new System.Windows.Forms.Label();
            this.label203 = new System.Windows.Forms.Label();
            this.panel23 = new System.Windows.Forms.Panel();
            this.label204 = new System.Windows.Forms.Label();
            this.txtBANK_CNAME2_6 = new System.Windows.Forms.TextBox();
            this.label205 = new System.Windows.Forms.Label();
            this.label206 = new System.Windows.Forms.Label();
            this.txtBANK_CNAME_6 = new System.Windows.Forms.TextBox();
            this.txtBRANCH_CNAME2_6 = new System.Windows.Forms.TextBox();
            this.label207 = new System.Windows.Forms.Label();
            this.label208 = new System.Windows.Forms.Label();
            this.txtBRANCH_CNAME_6 = new System.Windows.Forms.TextBox();
            this.txtACCOUNT_CNAME2_6 = new System.Windows.Forms.TextBox();
            this.label209 = new System.Windows.Forms.Label();
            this.label210 = new System.Windows.Forms.Label();
            this.txtACCOUNT_CNAME_6 = new System.Windows.Forms.TextBox();
            this.txtACCOUNT2_6 = new System.Windows.Forms.TextBox();
            this.label211 = new System.Windows.Forms.Label();
            this.label212 = new System.Windows.Forms.Label();
            this.txtACCOUNT_6 = new System.Windows.Forms.TextBox();
            this.txtSWIFT_CODE2_6 = new System.Windows.Forms.TextBox();
            this.label213 = new System.Windows.Forms.Label();
            this.label214 = new System.Windows.Forms.Label();
            this.txtSWIFT_CODE_6 = new System.Windows.Forms.TextBox();
            this.txtBANK_C_ADDRESS2_6 = new System.Windows.Forms.TextBox();
            this.label215 = new System.Windows.Forms.Label();
            this.txtBANK_C_ADDRESS_6 = new System.Windows.Forms.TextBox();
            this.btnImportToRMB2_2 = new System.Windows.Forms.Button();
            this.btnImportToRMB1_2 = new System.Windows.Forms.Button();
            this.label170 = new System.Windows.Forms.Label();
            this.txtACCOUNT_ENAME2_3_2 = new System.Windows.Forms.TextBox();
            this.label171 = new System.Windows.Forms.Label();
            this.txtACCOUNT_ENAME_3_2 = new System.Windows.Forms.TextBox();
            this.label172 = new System.Windows.Forms.Label();
            this.txtBANK_E_ADDRESS2_3_2 = new System.Windows.Forms.TextBox();
            this.label173 = new System.Windows.Forms.Label();
            this.txtBANK_ENAME2_3_2 = new System.Windows.Forms.TextBox();
            this.label174 = new System.Windows.Forms.Label();
            this.txtBRANCH_ENAME2_3_2 = new System.Windows.Forms.TextBox();
            this.label175 = new System.Windows.Forms.Label();
            this.txtBANK_E_ADDRESS_3_2 = new System.Windows.Forms.TextBox();
            this.label176 = new System.Windows.Forms.Label();
            this.txtBANK_ENAME_3_2 = new System.Windows.Forms.TextBox();
            this.label177 = new System.Windows.Forms.Label();
            this.txtBRANCH_ENAME_3_2 = new System.Windows.Forms.TextBox();
            this.panel19 = new System.Windows.Forms.Panel();
            this.label178 = new System.Windows.Forms.Label();
            this.label179 = new System.Windows.Forms.Label();
            this.panel20 = new System.Windows.Forms.Panel();
            this.label180 = new System.Windows.Forms.Label();
            this.txtBANK_CNAME2_3_2 = new System.Windows.Forms.TextBox();
            this.label181 = new System.Windows.Forms.Label();
            this.label182 = new System.Windows.Forms.Label();
            this.txtBANK_CNAME_3_2 = new System.Windows.Forms.TextBox();
            this.txtBRANCH_CNAME2_3_2 = new System.Windows.Forms.TextBox();
            this.label183 = new System.Windows.Forms.Label();
            this.label184 = new System.Windows.Forms.Label();
            this.txtBRANCH_CNAME_3_2 = new System.Windows.Forms.TextBox();
            this.txtACCOUNT_CNAME2_3_2 = new System.Windows.Forms.TextBox();
            this.label185 = new System.Windows.Forms.Label();
            this.label186 = new System.Windows.Forms.Label();
            this.txtACCOUNT_CNAME_3_2 = new System.Windows.Forms.TextBox();
            this.txtACCOUNT2_3_2 = new System.Windows.Forms.TextBox();
            this.label187 = new System.Windows.Forms.Label();
            this.label188 = new System.Windows.Forms.Label();
            this.txtACCOUNT_3_2 = new System.Windows.Forms.TextBox();
            this.txtSWIFT_CODE2_3_2 = new System.Windows.Forms.TextBox();
            this.label189 = new System.Windows.Forms.Label();
            this.label190 = new System.Windows.Forms.Label();
            this.txtSWIFT_CODE_3_2 = new System.Windows.Forms.TextBox();
            this.txtBANK_C_ADDRESS2_3_2 = new System.Windows.Forms.TextBox();
            this.label191 = new System.Windows.Forms.Label();
            this.txtBANK_C_ADDRESS_3_2 = new System.Windows.Forms.TextBox();
            this.btnImportToAUD2 = new System.Windows.Forms.Button();
            this.btnImportToAUD1 = new System.Windows.Forms.Button();
            this.btnImportToEUR2 = new System.Windows.Forms.Button();
            this.btnImportToEUR1 = new System.Windows.Forms.Button();
            this.btnImportToRMB2 = new System.Windows.Forms.Button();
            this.btnImportToRMB1 = new System.Windows.Forms.Button();
            this.btnImportToNTD2 = new System.Windows.Forms.Button();
            this.btnImportToNTD1 = new System.Windows.Forms.Button();
            this.btnImportToUSD2 = new System.Windows.Forms.Button();
            this.btnImportToUSD1 = new System.Windows.Forms.Button();
            this.label142 = new System.Windows.Forms.Label();
            this.label120 = new System.Windows.Forms.Label();
            this.txtACCOUNT_ENAME2_5 = new System.Windows.Forms.TextBox();
            this.txtACCOUNT_ENAME2_4 = new System.Windows.Forms.TextBox();
            this.label143 = new System.Windows.Forms.Label();
            this.label121 = new System.Windows.Forms.Label();
            this.txtACCOUNT_ENAME_5 = new System.Windows.Forms.TextBox();
            this.txtACCOUNT_ENAME_4 = new System.Windows.Forms.TextBox();
            this.label144 = new System.Windows.Forms.Label();
            this.label122 = new System.Windows.Forms.Label();
            this.txtBANK_E_ADDRESS2_5 = new System.Windows.Forms.TextBox();
            this.txtBANK_E_ADDRESS2_4 = new System.Windows.Forms.TextBox();
            this.label145 = new System.Windows.Forms.Label();
            this.label123 = new System.Windows.Forms.Label();
            this.txtBANK_ENAME2_5 = new System.Windows.Forms.TextBox();
            this.txtBANK_ENAME2_4 = new System.Windows.Forms.TextBox();
            this.label146 = new System.Windows.Forms.Label();
            this.label124 = new System.Windows.Forms.Label();
            this.txtBRANCH_ENAME2_5 = new System.Windows.Forms.TextBox();
            this.txtBRANCH_ENAME2_4 = new System.Windows.Forms.TextBox();
            this.label147 = new System.Windows.Forms.Label();
            this.label125 = new System.Windows.Forms.Label();
            this.txtBANK_E_ADDRESS_5 = new System.Windows.Forms.TextBox();
            this.txtBANK_E_ADDRESS_4 = new System.Windows.Forms.TextBox();
            this.label148 = new System.Windows.Forms.Label();
            this.label126 = new System.Windows.Forms.Label();
            this.txtBANK_ENAME_5 = new System.Windows.Forms.TextBox();
            this.txtBANK_ENAME_4 = new System.Windows.Forms.TextBox();
            this.label149 = new System.Windows.Forms.Label();
            this.label127 = new System.Windows.Forms.Label();
            this.txtBRANCH_ENAME_5 = new System.Windows.Forms.TextBox();
            this.txtBRANCH_ENAME_4 = new System.Windows.Forms.TextBox();
            this.panel17 = new System.Windows.Forms.Panel();
            this.label150 = new System.Windows.Forms.Label();
            this.label151 = new System.Windows.Forms.Label();
            this.panel15 = new System.Windows.Forms.Panel();
            this.label128 = new System.Windows.Forms.Label();
            this.panel18 = new System.Windows.Forms.Panel();
            this.label152 = new System.Windows.Forms.Label();
            this.label129 = new System.Windows.Forms.Label();
            this.txtBANK_CNAME2_5 = new System.Windows.Forms.TextBox();
            this.panel16 = new System.Windows.Forms.Panel();
            this.label130 = new System.Windows.Forms.Label();
            this.label153 = new System.Windows.Forms.Label();
            this.txtBANK_CNAME2_4 = new System.Windows.Forms.TextBox();
            this.label154 = new System.Windows.Forms.Label();
            this.label131 = new System.Windows.Forms.Label();
            this.txtBANK_CNAME_5 = new System.Windows.Forms.TextBox();
            this.label132 = new System.Windows.Forms.Label();
            this.txtBRANCH_CNAME2_5 = new System.Windows.Forms.TextBox();
            this.txtBANK_CNAME_4 = new System.Windows.Forms.TextBox();
            this.label155 = new System.Windows.Forms.Label();
            this.txtBRANCH_CNAME2_4 = new System.Windows.Forms.TextBox();
            this.label156 = new System.Windows.Forms.Label();
            this.label133 = new System.Windows.Forms.Label();
            this.txtBRANCH_CNAME_5 = new System.Windows.Forms.TextBox();
            this.label134 = new System.Windows.Forms.Label();
            this.txtACCOUNT_CNAME2_5 = new System.Windows.Forms.TextBox();
            this.txtBRANCH_CNAME_4 = new System.Windows.Forms.TextBox();
            this.label157 = new System.Windows.Forms.Label();
            this.txtACCOUNT_CNAME2_4 = new System.Windows.Forms.TextBox();
            this.label158 = new System.Windows.Forms.Label();
            this.label135 = new System.Windows.Forms.Label();
            this.txtACCOUNT_CNAME_5 = new System.Windows.Forms.TextBox();
            this.label136 = new System.Windows.Forms.Label();
            this.txtACCOUNT2_5 = new System.Windows.Forms.TextBox();
            this.txtACCOUNT_CNAME_4 = new System.Windows.Forms.TextBox();
            this.label159 = new System.Windows.Forms.Label();
            this.txtACCOUNT2_4 = new System.Windows.Forms.TextBox();
            this.label160 = new System.Windows.Forms.Label();
            this.label137 = new System.Windows.Forms.Label();
            this.txtACCOUNT_5 = new System.Windows.Forms.TextBox();
            this.label138 = new System.Windows.Forms.Label();
            this.txtSWIFT_CODE2_5 = new System.Windows.Forms.TextBox();
            this.txtACCOUNT_4 = new System.Windows.Forms.TextBox();
            this.label161 = new System.Windows.Forms.Label();
            this.txtSWIFT_CODE2_4 = new System.Windows.Forms.TextBox();
            this.label162 = new System.Windows.Forms.Label();
            this.label139 = new System.Windows.Forms.Label();
            this.txtSWIFT_CODE_5 = new System.Windows.Forms.TextBox();
            this.label140 = new System.Windows.Forms.Label();
            this.txtBANK_C_ADDRESS2_5 = new System.Windows.Forms.TextBox();
            this.txtSWIFT_CODE_4 = new System.Windows.Forms.TextBox();
            this.label163 = new System.Windows.Forms.Label();
            this.txtBANK_C_ADDRESS2_4 = new System.Windows.Forms.TextBox();
            this.txtBANK_C_ADDRESS_5 = new System.Windows.Forms.TextBox();
            this.label141 = new System.Windows.Forms.Label();
            this.txtBANK_C_ADDRESS_4 = new System.Windows.Forms.TextBox();
            this.label119 = new System.Windows.Forms.Label();
            this.txtACCOUNT_ENAME2_3 = new System.Windows.Forms.TextBox();
            this.label118 = new System.Windows.Forms.Label();
            this.txtACCOUNT_ENAME_3 = new System.Windows.Forms.TextBox();
            this.label117 = new System.Windows.Forms.Label();
            this.txtACCOUNT_ENAME2_2 = new System.Windows.Forms.TextBox();
            this.label116 = new System.Windows.Forms.Label();
            this.txtACCOUNT_ENAME_2 = new System.Windows.Forms.TextBox();
            this.label115 = new System.Windows.Forms.Label();
            this.txtACCOUNT_ENAME2 = new System.Windows.Forms.TextBox();
            this.label114 = new System.Windows.Forms.Label();
            this.txtACCOUNT_ENAME = new System.Windows.Forms.TextBox();
            this.label89 = new System.Windows.Forms.Label();
            this.txtBANK_E_ADDRESS2_3 = new System.Windows.Forms.TextBox();
            this.label90 = new System.Windows.Forms.Label();
            this.txtBANK_ENAME2_3 = new System.Windows.Forms.TextBox();
            this.label91 = new System.Windows.Forms.Label();
            this.txtBRANCH_ENAME2_3 = new System.Windows.Forms.TextBox();
            this.label92 = new System.Windows.Forms.Label();
            this.txtBANK_E_ADDRESS_3 = new System.Windows.Forms.TextBox();
            this.label93 = new System.Windows.Forms.Label();
            this.txtBANK_ENAME_3 = new System.Windows.Forms.TextBox();
            this.label94 = new System.Windows.Forms.Label();
            this.txtBRANCH_ENAME_3 = new System.Windows.Forms.TextBox();
            this.panel13 = new System.Windows.Forms.Panel();
            this.label95 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.panel14 = new System.Windows.Forms.Panel();
            this.label97 = new System.Windows.Forms.Label();
            this.txtBANK_CNAME2_3 = new System.Windows.Forms.TextBox();
            this.label98 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.txtBANK_CNAME_3 = new System.Windows.Forms.TextBox();
            this.txtBRANCH_CNAME2_3 = new System.Windows.Forms.TextBox();
            this.label100 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.txtBRANCH_CNAME_3 = new System.Windows.Forms.TextBox();
            this.txtACCOUNT_CNAME2_3 = new System.Windows.Forms.TextBox();
            this.label102 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.txtACCOUNT_CNAME_3 = new System.Windows.Forms.TextBox();
            this.txtACCOUNT2_3 = new System.Windows.Forms.TextBox();
            this.label104 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.txtACCOUNT_3 = new System.Windows.Forms.TextBox();
            this.txtSWIFT_CODE2_3 = new System.Windows.Forms.TextBox();
            this.label106 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.txtSWIFT_CODE_3 = new System.Windows.Forms.TextBox();
            this.txtBANK_C_ADDRESS2_3 = new System.Windows.Forms.TextBox();
            this.label108 = new System.Windows.Forms.Label();
            this.txtBANK_C_ADDRESS_3 = new System.Windows.Forms.TextBox();
            this.label69 = new System.Windows.Forms.Label();
            this.txtBANK_E_ADDRESS2_2 = new System.Windows.Forms.TextBox();
            this.label70 = new System.Windows.Forms.Label();
            this.txtBANK_ENAME2_2 = new System.Windows.Forms.TextBox();
            this.label71 = new System.Windows.Forms.Label();
            this.txtBRANCH_ENAME2_2 = new System.Windows.Forms.TextBox();
            this.label72 = new System.Windows.Forms.Label();
            this.txtBANK_E_ADDRESS_2 = new System.Windows.Forms.TextBox();
            this.label73 = new System.Windows.Forms.Label();
            this.txtBANK_ENAME_2 = new System.Windows.Forms.TextBox();
            this.label74 = new System.Windows.Forms.Label();
            this.txtBRANCH_ENAME_2 = new System.Windows.Forms.TextBox();
            this.panel11 = new System.Windows.Forms.Panel();
            this.label75 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.label77 = new System.Windows.Forms.Label();
            this.txtBANK_CNAME2_2 = new System.Windows.Forms.TextBox();
            this.label78 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.txtBANK_CNAME_2 = new System.Windows.Forms.TextBox();
            this.txtBRANCH_CNAME2_2 = new System.Windows.Forms.TextBox();
            this.label80 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.txtBRANCH_CNAME_2 = new System.Windows.Forms.TextBox();
            this.txtACCOUNT_CNAME2_2 = new System.Windows.Forms.TextBox();
            this.label82 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.txtACCOUNT_CNAME_2 = new System.Windows.Forms.TextBox();
            this.txtACCOUNT2_2 = new System.Windows.Forms.TextBox();
            this.label84 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.txtACCOUNT_2 = new System.Windows.Forms.TextBox();
            this.txtSWIFT_CODE2_2 = new System.Windows.Forms.TextBox();
            this.label86 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.txtSWIFT_CODE_2 = new System.Windows.Forms.TextBox();
            this.txtBANK_C_ADDRESS2_2 = new System.Windows.Forms.TextBox();
            this.label88 = new System.Windows.Forms.Label();
            this.txtBANK_C_ADDRESS_2 = new System.Windows.Forms.TextBox();
            this.label68 = new System.Windows.Forms.Label();
            this.txtBANK_E_ADDRESS2 = new System.Windows.Forms.TextBox();
            this.label66 = new System.Windows.Forms.Label();
            this.txtBANK_ENAME2 = new System.Windows.Forms.TextBox();
            this.label67 = new System.Windows.Forms.Label();
            this.txtBRANCH_ENAME2 = new System.Windows.Forms.TextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.txtBANK_E_ADDRESS = new System.Windows.Forms.TextBox();
            this.label63 = new System.Windows.Forms.Label();
            this.txtBANK_ENAME = new System.Windows.Forms.TextBox();
            this.label64 = new System.Windows.Forms.Label();
            this.txtBRANCH_ENAME = new System.Windows.Forms.TextBox();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.txtBANK_CNAME2 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.txtBANK_CNAME = new System.Windows.Forms.TextBox();
            this.txtBRANCH_CNAME2 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.txtBRANCH_CNAME = new System.Windows.Forms.TextBox();
            this.txtACCOUNT_CNAME2 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.txtACCOUNT_CNAME = new System.Windows.Forms.TextBox();
            this.txtACCOUNT2 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.txtACCOUNT = new System.Windows.Forms.TextBox();
            this.txtSWIFT_CODE2 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.txtSWIFT_CODE = new System.Windows.Forms.TextBox();
            this.txtBANK_C_ADDRESS2 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtBANK_C_ADDRESS = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.txtIBCode_Internal = new System.Windows.Forms.TextBox();
            this.label246 = new System.Windows.Forms.Label();
            this.btnBringSales_Internal = new System.Windows.Forms.Button();
            this.txtSales_Intermal = new System.Windows.Forms.TextBox();
            this.label247 = new System.Windows.Forms.Label();
            this.panel27 = new System.Windows.Forms.Panel();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.label216 = new System.Windows.Forms.Label();
            this.txtAmount_JPY = new System.Windows.Forms.TextBox();
            this.label218 = new System.Windows.Forms.Label();
            this.label217 = new System.Windows.Forms.Label();
            this.txtInterest_JPY = new System.Windows.Forms.TextBox();
            this.txtAmount_JPY_OLD = new System.Windows.Forms.TextBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.txtInterest_NZD = new System.Windows.Forms.TextBox();
            this.txtAmount_NZD = new System.Windows.Forms.TextBox();
            this.label245 = new System.Windows.Forms.Label();
            this.label243 = new System.Windows.Forms.Label();
            this.txtAmount_NZD_OLD = new System.Windows.Forms.TextBox();
            this.label244 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.txtInterest_AUD = new System.Windows.Forms.TextBox();
            this.label169 = new System.Windows.Forms.Label();
            this.txtAmount_AUD_OLD = new System.Windows.Forms.TextBox();
            this.label168 = new System.Windows.Forms.Label();
            this.txtAmount_AUD = new System.Windows.Forms.TextBox();
            this.label167 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txtAmount_EUR = new System.Windows.Forms.TextBox();
            this.label166 = new System.Windows.Forms.Label();
            this.txtInterest_EUR = new System.Windows.Forms.TextBox();
            this.txtAmount_EUR_OLD = new System.Windows.Forms.TextBox();
            this.label165 = new System.Windows.Forms.Label();
            this.label164 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtAmount_RMB = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.txtAmount_RMB_OLD = new System.Windows.Forms.TextBox();
            this.txtInterest_RMB = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtAmount_NTD = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.txtAmount_NTD_OLD = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.txtInterest_NTD = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtAmount_USD = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.txtAmount_USD_OLD = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.txtInterest_USD = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.panel24 = new System.Windows.Forms.Panel();
            this.lbl2020Condition = new System.Windows.Forms.Label();
            this.label221 = new System.Windows.Forms.Label();
            this.rdoBonusA = new System.Windows.Forms.RadioButton();
            this.rdoBonusB = new System.Windows.Forms.RadioButton();
            this.label219 = new System.Windows.Forms.Label();
            this.txtDiscount = new System.Windows.Forms.TextBox();
            this.chk_ind_order = new System.Windows.Forms.CheckBox();
            this.cboIS_REMIT_TO_AUS = new System.Windows.Forms.ComboBox();
            this.label193 = new System.Windows.Forms.Label();
            this.chkIsMultiContract = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.txtInterestedRate = new System.Windows.Forms.TextBox();
            this.label111 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.label110 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.btnDelFromDate = new System.Windows.Forms.Button();
            this.btnDelExtendDate = new System.Windows.Forms.Button();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label35 = new System.Windows.Forms.Label();
            this.cb_close = new System.Windows.Forms.CheckBox();
            this.label36 = new System.Windows.Forms.Label();
            this.dteEndDate = new System.Windows.Forms.Label();
            this.txtOrderNo = new System.Windows.Forms.TextBox();
            this.txtYear = new System.Windows.Forms.ComboBox();
            this.label37 = new System.Windows.Forms.Label();
            this.txtParentOrderNo = new System.Windows.Forms.TextBox();
            this.txtIBCode = new System.Windows.Forms.TextBox();
            this.btnBringParentOrder = new System.Windows.Forms.Button();
            this.label54 = new System.Windows.Forms.Label();
            this.btnBringSales = new System.Windows.Forms.Button();
            this.txtSales = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.txtMT4 = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.dteExtendDate = new System.Windows.Forms.DateTimePicker();
            this.label50 = new System.Windows.Forms.Label();
            this.dteOrderEnd = new System.Windows.Forms.DateTimePicker();
            this.label51 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.dteFromDate = new System.Windows.Forms.DateTimePicker();
            this.label48 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.btnComfirmModify = new System.Windows.Forms.Button();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.label248 = new System.Windows.Forms.Label();
            this.lblIS_ONLINE_WITHDRAWAL_CHECK = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panContractInfo.SuspendLayout();
            this.panel21.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel25.SuspendLayout();
            this.panel26.SuspendLayout();
            this.panel22.SuspendLayout();
            this.panel23.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.panel27.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel24.SuspendLayout();
            this.panel9.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnModifyHist
            // 
            this.btnModifyHist.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnModifyHist.Location = new System.Drawing.Point(265, 563);
            this.btnModifyHist.Name = "btnModifyHist";
            this.btnModifyHist.Size = new System.Drawing.Size(181, 48);
            this.btnModifyHist.TabIndex = 141;
            this.btnModifyHist.Text = "修改紀錄";
            this.btnModifyHist.UseVisualStyleBackColor = true;
            this.btnModifyHist.Visible = false;
            this.btnModifyHist.Click += new System.EventHandler(this.btnModifyHist_Click);
            // 
            // btnModifyALL
            // 
            this.btnModifyALL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnModifyALL.Location = new System.Drawing.Point(537, 563);
            this.btnModifyALL.Name = "btnModifyALL";
            this.btnModifyALL.Size = new System.Drawing.Size(181, 48);
            this.btnModifyALL.TabIndex = 140;
            this.btnModifyALL.Text = "資料全開可修改";
            this.btnModifyALL.UseVisualStyleBackColor = true;
            this.btnModifyALL.Visible = false;
            this.btnModifyALL.Click += new System.EventHandler(this.btnModifyALL_Click);
            // 
            // btnTransOrder
            // 
            this.btnTransOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTransOrder.Location = new System.Drawing.Point(78, 563);
            this.btnTransOrder.Name = "btnTransOrder";
            this.btnTransOrder.Size = new System.Drawing.Size(181, 48);
            this.btnTransOrder.TabIndex = 139;
            this.btnTransOrder.Text = "合約轉讓";
            this.btnTransOrder.UseVisualStyleBackColor = true;
            this.btnTransOrder.Visible = false;
            this.btnTransOrder.Click += new System.EventHandler(this.btnTransOrder_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(9, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1302, 545);
            this.tabControl1.TabIndex = 138;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label113);
            this.tabPage1.Controls.Add(this.txtPOSTAL_CODE2);
            this.tabPage1.Controls.Add(this.label112);
            this.tabPage1.Controls.Add(this.txtPOSTAL_CODE);
            this.tabPage1.Controls.Add(this.btnDelBirthday2);
            this.tabPage1.Controls.Add(this.btnDelBirthday);
            this.tabPage1.Controls.Add(this.panContractInfo);
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.txtCUST_CNAME);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.dteBirthday);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.txtPASSPORT);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.txtNATION);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.txtPHONE);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.txtEMAIL);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.txtAddress);
            this.tabPage1.Controls.Add(this.panel5);
            this.tabPage1.Controls.Add(this.label24);
            this.tabPage1.Controls.Add(this.txtID_NUMBER);
            this.tabPage1.Controls.Add(this.label25);
            this.tabPage1.Controls.Add(this.txtCUST_ENAME);
            this.tabPage1.Controls.Add(this.label26);
            this.tabPage1.Controls.Add(this.panel4);
            this.tabPage1.Controls.Add(this.label29);
            this.tabPage1.Controls.Add(this.txtCUST_CNAME2);
            this.tabPage1.Controls.Add(this.label28);
            this.tabPage1.Controls.Add(this.dteBirthday2);
            this.tabPage1.Controls.Add(this.label27);
            this.tabPage1.Controls.Add(this.txtPASSPORT2);
            this.tabPage1.Controls.Add(this.label23);
            this.tabPage1.Controls.Add(this.txtNATION2);
            this.tabPage1.Controls.Add(this.label21);
            this.tabPage1.Controls.Add(this.txtPHONE2);
            this.tabPage1.Controls.Add(this.label20);
            this.tabPage1.Controls.Add(this.txtEMAIL2);
            this.tabPage1.Controls.Add(this.label19);
            this.tabPage1.Controls.Add(this.txtAddress2);
            this.tabPage1.Controls.Add(this.panel3);
            this.tabPage1.Controls.Add(this.label18);
            this.tabPage1.Controls.Add(this.txtID_NUMBER2);
            this.tabPage1.Controls.Add(this.label17);
            this.tabPage1.Controls.Add(this.txtCUST_ENAME2);
            this.tabPage1.Controls.Add(this.label16);
            this.tabPage1.Location = new System.Drawing.Point(4, 26);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1294, 515);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "持有人資訊";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Location = new System.Drawing.Point(622, 230);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(64, 18);
            this.label113.TabIndex = 144;
            this.label113.Text = "郵遞區號";
            // 
            // txtPOSTAL_CODE2
            // 
            this.txtPOSTAL_CODE2.Location = new System.Drawing.Point(719, 227);
            this.txtPOSTAL_CODE2.Name = "txtPOSTAL_CODE2";
            this.txtPOSTAL_CODE2.Size = new System.Drawing.Size(200, 25);
            this.txtPOSTAL_CODE2.TabIndex = 145;
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Location = new System.Drawing.Point(11, 230);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(64, 18);
            this.label112.TabIndex = 142;
            this.label112.Text = "郵遞區號";
            // 
            // txtPOSTAL_CODE
            // 
            this.txtPOSTAL_CODE.Location = new System.Drawing.Point(109, 227);
            this.txtPOSTAL_CODE.Name = "txtPOSTAL_CODE";
            this.txtPOSTAL_CODE.Size = new System.Drawing.Size(199, 25);
            this.txtPOSTAL_CODE.TabIndex = 143;
            // 
            // btnDelBirthday2
            // 
            this.btnDelBirthday2.Image = ((System.Drawing.Image)(resources.GetObject("btnDelBirthday2.Image")));
            this.btnDelBirthday2.Location = new System.Drawing.Point(1176, 97);
            this.btnDelBirthday2.Name = "btnDelBirthday2";
            this.btnDelBirthday2.Size = new System.Drawing.Size(31, 28);
            this.btnDelBirthday2.TabIndex = 141;
            this.btnDelBirthday2.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.btnDelBirthday2.UseVisualStyleBackColor = true;
            this.btnDelBirthday2.Click += new System.EventHandler(this.btnDelBirthday2_Click);
            // 
            // btnDelBirthday
            // 
            this.btnDelBirthday.Image = ((System.Drawing.Image)(resources.GetObject("btnDelBirthday.Image")));
            this.btnDelBirthday.Location = new System.Drawing.Point(561, 100);
            this.btnDelBirthday.Name = "btnDelBirthday";
            this.btnDelBirthday.Size = new System.Drawing.Size(31, 28);
            this.btnDelBirthday.TabIndex = 140;
            this.btnDelBirthday.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.btnDelBirthday.UseVisualStyleBackColor = true;
            this.btnDelBirthday.Click += new System.EventHandler(this.btnDelBirthday_Click);
            // 
            // panContractInfo
            // 
            this.panContractInfo.Controls.Add(this.lblIS_ONLINE_WITHDRAWAL_CHECK);
            this.panContractInfo.Controls.Add(this.label248);
            this.panContractInfo.Controls.Add(this.panel21);
            this.panContractInfo.Controls.Add(this.label192);
            this.panContractInfo.Controls.Add(this.panel6);
            this.panContractInfo.Controls.Add(this.txtMAIL_TYPE_END);
            this.panContractInfo.Controls.Add(this.panel7);
            this.panContractInfo.Controls.Add(this.panel8);
            this.panContractInfo.Controls.Add(this.label33);
            this.panContractInfo.Controls.Add(this.label32);
            this.panContractInfo.Controls.Add(this.label31);
            this.panContractInfo.Controls.Add(this.label34);
            this.panContractInfo.Location = new System.Drawing.Point(6, 285);
            this.panContractInfo.Name = "panContractInfo";
            this.panContractInfo.Size = new System.Drawing.Size(587, 200);
            this.panContractInfo.TabIndex = 92;
            // 
            // panel21
            // 
            this.panel21.Controls.Add(this.rdoWebDepositType_Null);
            this.panel21.Controls.Add(this.rdoWebDepositType_Y);
            this.panel21.Location = new System.Drawing.Point(111, 132);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(461, 28);
            this.panel21.TabIndex = 93;
            // 
            // rdoWebDepositType_Null
            // 
            this.rdoWebDepositType_Null.AutoSize = true;
            this.rdoWebDepositType_Null.Location = new System.Drawing.Point(3, 3);
            this.rdoWebDepositType_Null.Name = "rdoWebDepositType_Null";
            this.rdoWebDepositType_Null.Size = new System.Drawing.Size(82, 22);
            this.rdoWebDepositType_Null.TabIndex = 85;
            this.rdoWebDepositType_Null.Text = "不可使用";
            this.rdoWebDepositType_Null.UseVisualStyleBackColor = true;
            // 
            // rdoWebDepositType_Y
            // 
            this.rdoWebDepositType_Y.AutoSize = true;
            this.rdoWebDepositType_Y.Location = new System.Drawing.Point(90, 3);
            this.rdoWebDepositType_Y.Name = "rdoWebDepositType_Y";
            this.rdoWebDepositType_Y.Size = new System.Drawing.Size(68, 22);
            this.rdoWebDepositType_Y.TabIndex = 86;
            this.rdoWebDepositType_Y.Text = "可使用";
            this.rdoWebDepositType_Y.UseVisualStyleBackColor = true;
            // 
            // label192
            // 
            this.label192.AutoSize = true;
            this.label192.Location = new System.Drawing.Point(6, 136);
            this.label192.Name = "label192";
            this.label192.Size = new System.Drawing.Size(106, 18);
            this.label192.TabIndex = 92;
            this.label192.Text = "核准可線上出金";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.Info;
            this.panel6.Controls.Add(this.label30);
            this.panel6.Location = new System.Drawing.Point(0, 3);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(587, 58);
            this.panel6.TabIndex = 12;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("微軟正黑體", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label30.Location = new System.Drawing.Point(3, 12);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(110, 31);
            this.label30.TabIndex = 0;
            this.label30.Text = "合約資訊";
            // 
            // txtMAIL_TYPE_END
            // 
            this.txtMAIL_TYPE_END.Location = new System.Drawing.Point(384, 99);
            this.txtMAIL_TYPE_END.Name = "txtMAIL_TYPE_END";
            this.txtMAIL_TYPE_END.Size = new System.Drawing.Size(91, 25);
            this.txtMAIL_TYPE_END.TabIndex = 89;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.chkMAIL_TYPE_MONTHLY1);
            this.panel7.Controls.Add(this.chkMAIL_TYPE_MONTHLY2);
            this.panel7.Location = new System.Drawing.Point(90, 67);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(212, 22);
            this.panel7.TabIndex = 90;
            // 
            // chkMAIL_TYPE_MONTHLY1
            // 
            this.chkMAIL_TYPE_MONTHLY1.AutoSize = true;
            this.chkMAIL_TYPE_MONTHLY1.Location = new System.Drawing.Point(3, 3);
            this.chkMAIL_TYPE_MONTHLY1.Name = "chkMAIL_TYPE_MONTHLY1";
            this.chkMAIL_TYPE_MONTHLY1.Size = new System.Drawing.Size(110, 22);
            this.chkMAIL_TYPE_MONTHLY1.TabIndex = 83;
            this.chkMAIL_TYPE_MONTHLY1.Text = "業務人員親送";
            this.chkMAIL_TYPE_MONTHLY1.UseVisualStyleBackColor = true;
            // 
            // chkMAIL_TYPE_MONTHLY2
            // 
            this.chkMAIL_TYPE_MONTHLY2.AutoSize = true;
            this.chkMAIL_TYPE_MONTHLY2.Location = new System.Drawing.Point(127, 3);
            this.chkMAIL_TYPE_MONTHLY2.Name = "chkMAIL_TYPE_MONTHLY2";
            this.chkMAIL_TYPE_MONTHLY2.Size = new System.Drawing.Size(82, 22);
            this.chkMAIL_TYPE_MONTHLY2.TabIndex = 84;
            this.chkMAIL_TYPE_MONTHLY2.Text = "郵寄掛號";
            this.chkMAIL_TYPE_MONTHLY2.UseVisualStyleBackColor = true;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.chkMAIL_TYPE_END1);
            this.panel8.Controls.Add(this.chkMAIL_TYPE_END2);
            this.panel8.Controls.Add(this.chkMAIL_TYPE_END3);
            this.panel8.Location = new System.Drawing.Point(111, 98);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(270, 28);
            this.panel8.TabIndex = 91;
            // 
            // chkMAIL_TYPE_END1
            // 
            this.chkMAIL_TYPE_END1.AutoSize = true;
            this.chkMAIL_TYPE_END1.Location = new System.Drawing.Point(3, 3);
            this.chkMAIL_TYPE_END1.Name = "chkMAIL_TYPE_END1";
            this.chkMAIL_TYPE_END1.Size = new System.Drawing.Size(110, 22);
            this.chkMAIL_TYPE_END1.TabIndex = 85;
            this.chkMAIL_TYPE_END1.Text = "業務人員親送";
            this.chkMAIL_TYPE_END1.UseVisualStyleBackColor = true;
            // 
            // chkMAIL_TYPE_END2
            // 
            this.chkMAIL_TYPE_END2.AutoSize = true;
            this.chkMAIL_TYPE_END2.Location = new System.Drawing.Point(119, 3);
            this.chkMAIL_TYPE_END2.Name = "chkMAIL_TYPE_END2";
            this.chkMAIL_TYPE_END2.Size = new System.Drawing.Size(82, 22);
            this.chkMAIL_TYPE_END2.TabIndex = 86;
            this.chkMAIL_TYPE_END2.Text = "電子郵件";
            this.chkMAIL_TYPE_END2.UseVisualStyleBackColor = true;
            // 
            // chkMAIL_TYPE_END3
            // 
            this.chkMAIL_TYPE_END3.AutoSize = true;
            this.chkMAIL_TYPE_END3.Location = new System.Drawing.Point(207, 3);
            this.chkMAIL_TYPE_END3.Name = "chkMAIL_TYPE_END3";
            this.chkMAIL_TYPE_END3.Size = new System.Drawing.Size(57, 22);
            this.chkMAIL_TYPE_END3.TabIndex = 87;
            this.chkMAIL_TYPE_END3.Text = "其他:";
            this.chkMAIL_TYPE_END3.UseVisualStyleBackColor = true;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(311, 70);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(78, 18);
            this.label33.TabIndex = 82;
            this.label33.Text = "之方式取得";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(6, 102);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(106, 18);
            this.label32.TabIndex = 81;
            this.label32.Text = "月對帳單，需經";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(6, 70);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(78, 18);
            this.label31.TabIndex = 80;
            this.label31.Text = "合約，需經";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(481, 101);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(78, 18);
            this.label34.TabIndex = 88;
            this.label34.Text = "之方式取得";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Info;
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(6, 6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(587, 58);
            this.panel1.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微軟正黑體", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(3, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(134, 31);
            this.label2.TabIndex = 0;
            this.label2.Text = "主要持有人";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 72);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 18);
            this.label1.TabIndex = 8;
            this.label1.Text = "姓名";
            // 
            // txtCUST_CNAME
            // 
            this.txtCUST_CNAME.Location = new System.Drawing.Point(109, 69);
            this.txtCUST_CNAME.Name = "txtCUST_CNAME";
            this.txtCUST_CNAME.Size = new System.Drawing.Size(199, 25);
            this.txtCUST_CNAME.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(318, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 18);
            this.label3.TabIndex = 13;
            this.label3.Text = "出生年月日";
            // 
            // dteBirthday
            // 
            this.dteBirthday.Location = new System.Drawing.Point(403, 101);
            this.dteBirthday.Name = "dteBirthday";
            this.dteBirthday.Size = new System.Drawing.Size(152, 25);
            this.dteBirthday.TabIndex = 14;
            this.dteBirthday.ValueChanged += new System.EventHandler(this.dteBirthday_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(318, 136);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 18);
            this.label4.TabIndex = 15;
            this.label4.Text = "護照號碼";
            // 
            // txtPASSPORT
            // 
            this.txtPASSPORT.Location = new System.Drawing.Point(403, 133);
            this.txtPASSPORT.Name = "txtPASSPORT";
            this.txtPASSPORT.Size = new System.Drawing.Size(186, 25);
            this.txtPASSPORT.TabIndex = 16;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 136);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 18);
            this.label5.TabIndex = 17;
            this.label5.Text = "國籍";
            // 
            // txtNATION
            // 
            this.txtNATION.Location = new System.Drawing.Point(109, 133);
            this.txtNATION.Name = "txtNATION";
            this.txtNATION.Size = new System.Drawing.Size(199, 25);
            this.txtNATION.TabIndex = 18;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 168);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 18);
            this.label6.TabIndex = 19;
            this.label6.Text = "聯絡電話";
            // 
            // txtPHONE
            // 
            this.txtPHONE.Location = new System.Drawing.Point(109, 165);
            this.txtPHONE.Name = "txtPHONE";
            this.txtPHONE.Size = new System.Drawing.Size(199, 25);
            this.txtPHONE.TabIndex = 20;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(11, 201);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(92, 18);
            this.label7.TabIndex = 21;
            this.label7.Text = "電子郵件信箱";
            // 
            // txtEMAIL
            // 
            this.txtEMAIL.Location = new System.Drawing.Point(109, 198);
            this.txtEMAIL.Name = "txtEMAIL";
            this.txtEMAIL.Size = new System.Drawing.Size(480, 25);
            this.txtEMAIL.TabIndex = 22;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(11, 261);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 18);
            this.label8.TabIndex = 23;
            this.label8.Text = "居住地址";
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(109, 258);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(480, 25);
            this.txtAddress.TabIndex = 24;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.rdoWoman);
            this.panel5.Controls.Add(this.rdoMan);
            this.panel5.Location = new System.Drawing.Point(404, 67);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(117, 28);
            this.panel5.TabIndex = 38;
            // 
            // rdoWoman
            // 
            this.rdoWoman.AutoSize = true;
            this.rdoWoman.Location = new System.Drawing.Point(3, 3);
            this.rdoWoman.Name = "rdoWoman";
            this.rdoWoman.Size = new System.Drawing.Size(54, 22);
            this.rdoWoman.TabIndex = 11;
            this.rdoWoman.TabStop = true;
            this.rdoWoman.Text = "女士";
            this.rdoWoman.UseVisualStyleBackColor = true;
            // 
            // rdoMan
            // 
            this.rdoMan.AutoSize = true;
            this.rdoMan.Location = new System.Drawing.Point(63, 3);
            this.rdoMan.Name = "rdoMan";
            this.rdoMan.Size = new System.Drawing.Size(54, 22);
            this.rdoMan.TabIndex = 12;
            this.rdoMan.TabStop = true;
            this.rdoMan.Text = "男士";
            this.rdoMan.UseVisualStyleBackColor = true;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(318, 168);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(78, 18);
            this.label24.TabIndex = 55;
            this.label24.Text = "身分證號碼";
            // 
            // txtID_NUMBER
            // 
            this.txtID_NUMBER.Location = new System.Drawing.Point(403, 165);
            this.txtID_NUMBER.Name = "txtID_NUMBER";
            this.txtID_NUMBER.Size = new System.Drawing.Size(186, 25);
            this.txtID_NUMBER.TabIndex = 56;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(11, 104);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(58, 18);
            this.label25.TabIndex = 57;
            this.label25.Text = "姓名(英)";
            // 
            // txtCUST_ENAME
            // 
            this.txtCUST_ENAME.Location = new System.Drawing.Point(109, 101);
            this.txtCUST_ENAME.Name = "txtCUST_ENAME";
            this.txtCUST_ENAME.Size = new System.Drawing.Size(199, 25);
            this.txtCUST_ENAME.TabIndex = 58;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(318, 72);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(36, 18);
            this.label26.TabIndex = 59;
            this.label26.Text = "性別";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.Info;
            this.panel4.Controls.Add(this.label22);
            this.panel4.Location = new System.Drawing.Point(618, 6);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(590, 58);
            this.panel4.TabIndex = 41;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("微軟正黑體", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label22.Location = new System.Drawing.Point(3, 12);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(134, 31);
            this.label22.TabIndex = 0;
            this.label22.Text = "聯名持有人";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(623, 71);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(36, 18);
            this.label29.TabIndex = 60;
            this.label29.Text = "姓名";
            // 
            // txtCUST_CNAME2
            // 
            this.txtCUST_CNAME2.Location = new System.Drawing.Point(719, 68);
            this.txtCUST_CNAME2.Name = "txtCUST_CNAME2";
            this.txtCUST_CNAME2.Size = new System.Drawing.Size(200, 25);
            this.txtCUST_CNAME2.TabIndex = 61;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(925, 103);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(78, 18);
            this.label28.TabIndex = 62;
            this.label28.Text = "出生年月日";
            // 
            // dteBirthday2
            // 
            this.dteBirthday2.Location = new System.Drawing.Point(1011, 100);
            this.dteBirthday2.Name = "dteBirthday2";
            this.dteBirthday2.Size = new System.Drawing.Size(159, 25);
            this.dteBirthday2.TabIndex = 63;
            this.dteBirthday2.ValueChanged += new System.EventHandler(this.dteBirthday2_ValueChanged);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(925, 135);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(64, 18);
            this.label27.TabIndex = 64;
            this.label27.Text = "護照號碼";
            // 
            // txtPASSPORT2
            // 
            this.txtPASSPORT2.Location = new System.Drawing.Point(1011, 132);
            this.txtPASSPORT2.Name = "txtPASSPORT2";
            this.txtPASSPORT2.Size = new System.Drawing.Size(193, 25);
            this.txtPASSPORT2.TabIndex = 65;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(623, 135);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(36, 18);
            this.label23.TabIndex = 66;
            this.label23.Text = "國籍";
            // 
            // txtNATION2
            // 
            this.txtNATION2.Location = new System.Drawing.Point(719, 132);
            this.txtNATION2.Name = "txtNATION2";
            this.txtNATION2.Size = new System.Drawing.Size(200, 25);
            this.txtNATION2.TabIndex = 67;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(623, 167);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(64, 18);
            this.label21.TabIndex = 68;
            this.label21.Text = "聯絡電話";
            // 
            // txtPHONE2
            // 
            this.txtPHONE2.Location = new System.Drawing.Point(719, 164);
            this.txtPHONE2.Name = "txtPHONE2";
            this.txtPHONE2.Size = new System.Drawing.Size(200, 25);
            this.txtPHONE2.TabIndex = 69;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(623, 200);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(92, 18);
            this.label20.TabIndex = 70;
            this.label20.Text = "電子郵件信箱";
            // 
            // txtEMAIL2
            // 
            this.txtEMAIL2.Location = new System.Drawing.Point(719, 197);
            this.txtEMAIL2.Name = "txtEMAIL2";
            this.txtEMAIL2.Size = new System.Drawing.Size(485, 25);
            this.txtEMAIL2.TabIndex = 71;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(623, 260);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(64, 18);
            this.label19.TabIndex = 72;
            this.label19.Text = "居住地址";
            // 
            // txtAddress2
            // 
            this.txtAddress2.Location = new System.Drawing.Point(719, 257);
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.Size = new System.Drawing.Size(485, 25);
            this.txtAddress2.TabIndex = 73;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.rdoWoman2);
            this.panel3.Controls.Add(this.rdoMan2);
            this.panel3.Location = new System.Drawing.Point(1011, 66);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(117, 28);
            this.panel3.TabIndex = 74;
            // 
            // rdoWoman2
            // 
            this.rdoWoman2.AutoSize = true;
            this.rdoWoman2.Location = new System.Drawing.Point(3, 3);
            this.rdoWoman2.Name = "rdoWoman2";
            this.rdoWoman2.Size = new System.Drawing.Size(54, 22);
            this.rdoWoman2.TabIndex = 11;
            this.rdoWoman2.TabStop = true;
            this.rdoWoman2.Text = "女士";
            this.rdoWoman2.UseVisualStyleBackColor = true;
            // 
            // rdoMan2
            // 
            this.rdoMan2.AutoSize = true;
            this.rdoMan2.Location = new System.Drawing.Point(63, 3);
            this.rdoMan2.Name = "rdoMan2";
            this.rdoMan2.Size = new System.Drawing.Size(54, 22);
            this.rdoMan2.TabIndex = 12;
            this.rdoMan2.TabStop = true;
            this.rdoMan2.Text = "男士";
            this.rdoMan2.UseVisualStyleBackColor = true;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(925, 167);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(78, 18);
            this.label18.TabIndex = 75;
            this.label18.Text = "身分證號碼";
            // 
            // txtID_NUMBER2
            // 
            this.txtID_NUMBER2.Location = new System.Drawing.Point(1011, 164);
            this.txtID_NUMBER2.Name = "txtID_NUMBER2";
            this.txtID_NUMBER2.Size = new System.Drawing.Size(193, 25);
            this.txtID_NUMBER2.TabIndex = 76;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(623, 103);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(58, 18);
            this.label17.TabIndex = 77;
            this.label17.Text = "姓名(英)";
            // 
            // txtCUST_ENAME2
            // 
            this.txtCUST_ENAME2.Location = new System.Drawing.Point(719, 100);
            this.txtCUST_ENAME2.Name = "txtCUST_ENAME2";
            this.txtCUST_ENAME2.Size = new System.Drawing.Size(200, 25);
            this.txtCUST_ENAME2.TabIndex = 78;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(925, 71);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(36, 18);
            this.label16.TabIndex = 79;
            this.label16.Text = "性別";
            // 
            // tabPage2
            // 
            this.tabPage2.AutoScroll = true;
            this.tabPage2.Controls.Add(this.btnImportToNZD2);
            this.tabPage2.Controls.Add(this.btnImportToNZD1);
            this.tabPage2.Controls.Add(this.label220);
            this.tabPage2.Controls.Add(this.txtACCOUNT_ENAME2_7);
            this.tabPage2.Controls.Add(this.label222);
            this.tabPage2.Controls.Add(this.txtACCOUNT_ENAME_7);
            this.tabPage2.Controls.Add(this.label223);
            this.tabPage2.Controls.Add(this.txtBANK_E_ADDRESS2_7);
            this.tabPage2.Controls.Add(this.label224);
            this.tabPage2.Controls.Add(this.txtBANK_ENAME2_7);
            this.tabPage2.Controls.Add(this.label225);
            this.tabPage2.Controls.Add(this.txtBRANCH_ENAME2_7);
            this.tabPage2.Controls.Add(this.label226);
            this.tabPage2.Controls.Add(this.txtBANK_E_ADDRESS_7);
            this.tabPage2.Controls.Add(this.label227);
            this.tabPage2.Controls.Add(this.txtBANK_ENAME_7);
            this.tabPage2.Controls.Add(this.label228);
            this.tabPage2.Controls.Add(this.txtBRANCH_ENAME_7);
            this.tabPage2.Controls.Add(this.panel25);
            this.tabPage2.Controls.Add(this.label230);
            this.tabPage2.Controls.Add(this.panel26);
            this.tabPage2.Controls.Add(this.txtBANK_CNAME2_7);
            this.tabPage2.Controls.Add(this.label232);
            this.tabPage2.Controls.Add(this.label233);
            this.tabPage2.Controls.Add(this.txtBANK_CNAME_7);
            this.tabPage2.Controls.Add(this.txtBRANCH_CNAME2_7);
            this.tabPage2.Controls.Add(this.label234);
            this.tabPage2.Controls.Add(this.label235);
            this.tabPage2.Controls.Add(this.txtBRANCH_CNAME_7);
            this.tabPage2.Controls.Add(this.txtACCOUNT_CNAME2_7);
            this.tabPage2.Controls.Add(this.label236);
            this.tabPage2.Controls.Add(this.label237);
            this.tabPage2.Controls.Add(this.txtACCOUNT_CNAME_7);
            this.tabPage2.Controls.Add(this.txtACCOUNT2_7);
            this.tabPage2.Controls.Add(this.label238);
            this.tabPage2.Controls.Add(this.label239);
            this.tabPage2.Controls.Add(this.txtACCOUNT_7);
            this.tabPage2.Controls.Add(this.txtSWIFT_CODE2_7);
            this.tabPage2.Controls.Add(this.label240);
            this.tabPage2.Controls.Add(this.label241);
            this.tabPage2.Controls.Add(this.txtSWIFT_CODE_7);
            this.tabPage2.Controls.Add(this.txtBANK_C_ADDRESS2_7);
            this.tabPage2.Controls.Add(this.label242);
            this.tabPage2.Controls.Add(this.txtBANK_C_ADDRESS_7);
            this.tabPage2.Controls.Add(this.btnImportToJPY2);
            this.tabPage2.Controls.Add(this.btnImportToJPY1);
            this.tabPage2.Controls.Add(this.label194);
            this.tabPage2.Controls.Add(this.txtACCOUNT_ENAME2_6);
            this.tabPage2.Controls.Add(this.label195);
            this.tabPage2.Controls.Add(this.txtACCOUNT_ENAME_6);
            this.tabPage2.Controls.Add(this.label196);
            this.tabPage2.Controls.Add(this.txtBANK_E_ADDRESS2_6);
            this.tabPage2.Controls.Add(this.label197);
            this.tabPage2.Controls.Add(this.txtBANK_ENAME2_6);
            this.tabPage2.Controls.Add(this.label198);
            this.tabPage2.Controls.Add(this.txtBRANCH_ENAME2_6);
            this.tabPage2.Controls.Add(this.label199);
            this.tabPage2.Controls.Add(this.txtBANK_E_ADDRESS_6);
            this.tabPage2.Controls.Add(this.label200);
            this.tabPage2.Controls.Add(this.txtBANK_ENAME_6);
            this.tabPage2.Controls.Add(this.label201);
            this.tabPage2.Controls.Add(this.txtBRANCH_ENAME_6);
            this.tabPage2.Controls.Add(this.panel22);
            this.tabPage2.Controls.Add(this.label203);
            this.tabPage2.Controls.Add(this.panel23);
            this.tabPage2.Controls.Add(this.txtBANK_CNAME2_6);
            this.tabPage2.Controls.Add(this.label205);
            this.tabPage2.Controls.Add(this.label206);
            this.tabPage2.Controls.Add(this.txtBANK_CNAME_6);
            this.tabPage2.Controls.Add(this.txtBRANCH_CNAME2_6);
            this.tabPage2.Controls.Add(this.label207);
            this.tabPage2.Controls.Add(this.label208);
            this.tabPage2.Controls.Add(this.txtBRANCH_CNAME_6);
            this.tabPage2.Controls.Add(this.txtACCOUNT_CNAME2_6);
            this.tabPage2.Controls.Add(this.label209);
            this.tabPage2.Controls.Add(this.label210);
            this.tabPage2.Controls.Add(this.txtACCOUNT_CNAME_6);
            this.tabPage2.Controls.Add(this.txtACCOUNT2_6);
            this.tabPage2.Controls.Add(this.label211);
            this.tabPage2.Controls.Add(this.label212);
            this.tabPage2.Controls.Add(this.txtACCOUNT_6);
            this.tabPage2.Controls.Add(this.txtSWIFT_CODE2_6);
            this.tabPage2.Controls.Add(this.label213);
            this.tabPage2.Controls.Add(this.label214);
            this.tabPage2.Controls.Add(this.txtSWIFT_CODE_6);
            this.tabPage2.Controls.Add(this.txtBANK_C_ADDRESS2_6);
            this.tabPage2.Controls.Add(this.label215);
            this.tabPage2.Controls.Add(this.txtBANK_C_ADDRESS_6);
            this.tabPage2.Controls.Add(this.btnImportToRMB2_2);
            this.tabPage2.Controls.Add(this.btnImportToRMB1_2);
            this.tabPage2.Controls.Add(this.label170);
            this.tabPage2.Controls.Add(this.txtACCOUNT_ENAME2_3_2);
            this.tabPage2.Controls.Add(this.label171);
            this.tabPage2.Controls.Add(this.txtACCOUNT_ENAME_3_2);
            this.tabPage2.Controls.Add(this.label172);
            this.tabPage2.Controls.Add(this.txtBANK_E_ADDRESS2_3_2);
            this.tabPage2.Controls.Add(this.label173);
            this.tabPage2.Controls.Add(this.txtBANK_ENAME2_3_2);
            this.tabPage2.Controls.Add(this.label174);
            this.tabPage2.Controls.Add(this.txtBRANCH_ENAME2_3_2);
            this.tabPage2.Controls.Add(this.label175);
            this.tabPage2.Controls.Add(this.txtBANK_E_ADDRESS_3_2);
            this.tabPage2.Controls.Add(this.label176);
            this.tabPage2.Controls.Add(this.txtBANK_ENAME_3_2);
            this.tabPage2.Controls.Add(this.label177);
            this.tabPage2.Controls.Add(this.txtBRANCH_ENAME_3_2);
            this.tabPage2.Controls.Add(this.panel19);
            this.tabPage2.Controls.Add(this.label179);
            this.tabPage2.Controls.Add(this.panel20);
            this.tabPage2.Controls.Add(this.txtBANK_CNAME2_3_2);
            this.tabPage2.Controls.Add(this.label181);
            this.tabPage2.Controls.Add(this.label182);
            this.tabPage2.Controls.Add(this.txtBANK_CNAME_3_2);
            this.tabPage2.Controls.Add(this.txtBRANCH_CNAME2_3_2);
            this.tabPage2.Controls.Add(this.label183);
            this.tabPage2.Controls.Add(this.label184);
            this.tabPage2.Controls.Add(this.txtBRANCH_CNAME_3_2);
            this.tabPage2.Controls.Add(this.txtACCOUNT_CNAME2_3_2);
            this.tabPage2.Controls.Add(this.label185);
            this.tabPage2.Controls.Add(this.label186);
            this.tabPage2.Controls.Add(this.txtACCOUNT_CNAME_3_2);
            this.tabPage2.Controls.Add(this.txtACCOUNT2_3_2);
            this.tabPage2.Controls.Add(this.label187);
            this.tabPage2.Controls.Add(this.label188);
            this.tabPage2.Controls.Add(this.txtACCOUNT_3_2);
            this.tabPage2.Controls.Add(this.txtSWIFT_CODE2_3_2);
            this.tabPage2.Controls.Add(this.label189);
            this.tabPage2.Controls.Add(this.label190);
            this.tabPage2.Controls.Add(this.txtSWIFT_CODE_3_2);
            this.tabPage2.Controls.Add(this.txtBANK_C_ADDRESS2_3_2);
            this.tabPage2.Controls.Add(this.label191);
            this.tabPage2.Controls.Add(this.txtBANK_C_ADDRESS_3_2);
            this.tabPage2.Controls.Add(this.btnImportToAUD2);
            this.tabPage2.Controls.Add(this.btnImportToAUD1);
            this.tabPage2.Controls.Add(this.btnImportToEUR2);
            this.tabPage2.Controls.Add(this.btnImportToEUR1);
            this.tabPage2.Controls.Add(this.btnImportToRMB2);
            this.tabPage2.Controls.Add(this.btnImportToRMB1);
            this.tabPage2.Controls.Add(this.btnImportToNTD2);
            this.tabPage2.Controls.Add(this.btnImportToNTD1);
            this.tabPage2.Controls.Add(this.btnImportToUSD2);
            this.tabPage2.Controls.Add(this.btnImportToUSD1);
            this.tabPage2.Controls.Add(this.label142);
            this.tabPage2.Controls.Add(this.label120);
            this.tabPage2.Controls.Add(this.txtACCOUNT_ENAME2_5);
            this.tabPage2.Controls.Add(this.txtACCOUNT_ENAME2_4);
            this.tabPage2.Controls.Add(this.label143);
            this.tabPage2.Controls.Add(this.label121);
            this.tabPage2.Controls.Add(this.txtACCOUNT_ENAME_5);
            this.tabPage2.Controls.Add(this.txtACCOUNT_ENAME_4);
            this.tabPage2.Controls.Add(this.label144);
            this.tabPage2.Controls.Add(this.label122);
            this.tabPage2.Controls.Add(this.txtBANK_E_ADDRESS2_5);
            this.tabPage2.Controls.Add(this.txtBANK_E_ADDRESS2_4);
            this.tabPage2.Controls.Add(this.label145);
            this.tabPage2.Controls.Add(this.label123);
            this.tabPage2.Controls.Add(this.txtBANK_ENAME2_5);
            this.tabPage2.Controls.Add(this.txtBANK_ENAME2_4);
            this.tabPage2.Controls.Add(this.label146);
            this.tabPage2.Controls.Add(this.label124);
            this.tabPage2.Controls.Add(this.txtBRANCH_ENAME2_5);
            this.tabPage2.Controls.Add(this.txtBRANCH_ENAME2_4);
            this.tabPage2.Controls.Add(this.label147);
            this.tabPage2.Controls.Add(this.label125);
            this.tabPage2.Controls.Add(this.txtBANK_E_ADDRESS_5);
            this.tabPage2.Controls.Add(this.txtBANK_E_ADDRESS_4);
            this.tabPage2.Controls.Add(this.label148);
            this.tabPage2.Controls.Add(this.label126);
            this.tabPage2.Controls.Add(this.txtBANK_ENAME_5);
            this.tabPage2.Controls.Add(this.txtBANK_ENAME_4);
            this.tabPage2.Controls.Add(this.label149);
            this.tabPage2.Controls.Add(this.label127);
            this.tabPage2.Controls.Add(this.txtBRANCH_ENAME_5);
            this.tabPage2.Controls.Add(this.txtBRANCH_ENAME_4);
            this.tabPage2.Controls.Add(this.panel17);
            this.tabPage2.Controls.Add(this.label151);
            this.tabPage2.Controls.Add(this.panel15);
            this.tabPage2.Controls.Add(this.panel18);
            this.tabPage2.Controls.Add(this.label129);
            this.tabPage2.Controls.Add(this.txtBANK_CNAME2_5);
            this.tabPage2.Controls.Add(this.panel16);
            this.tabPage2.Controls.Add(this.label153);
            this.tabPage2.Controls.Add(this.txtBANK_CNAME2_4);
            this.tabPage2.Controls.Add(this.label154);
            this.tabPage2.Controls.Add(this.label131);
            this.tabPage2.Controls.Add(this.txtBANK_CNAME_5);
            this.tabPage2.Controls.Add(this.label132);
            this.tabPage2.Controls.Add(this.txtBRANCH_CNAME2_5);
            this.tabPage2.Controls.Add(this.txtBANK_CNAME_4);
            this.tabPage2.Controls.Add(this.label155);
            this.tabPage2.Controls.Add(this.txtBRANCH_CNAME2_4);
            this.tabPage2.Controls.Add(this.label156);
            this.tabPage2.Controls.Add(this.label133);
            this.tabPage2.Controls.Add(this.txtBRANCH_CNAME_5);
            this.tabPage2.Controls.Add(this.label134);
            this.tabPage2.Controls.Add(this.txtACCOUNT_CNAME2_5);
            this.tabPage2.Controls.Add(this.txtBRANCH_CNAME_4);
            this.tabPage2.Controls.Add(this.label157);
            this.tabPage2.Controls.Add(this.txtACCOUNT_CNAME2_4);
            this.tabPage2.Controls.Add(this.label158);
            this.tabPage2.Controls.Add(this.label135);
            this.tabPage2.Controls.Add(this.txtACCOUNT_CNAME_5);
            this.tabPage2.Controls.Add(this.label136);
            this.tabPage2.Controls.Add(this.txtACCOUNT2_5);
            this.tabPage2.Controls.Add(this.txtACCOUNT_CNAME_4);
            this.tabPage2.Controls.Add(this.label159);
            this.tabPage2.Controls.Add(this.txtACCOUNT2_4);
            this.tabPage2.Controls.Add(this.label160);
            this.tabPage2.Controls.Add(this.label137);
            this.tabPage2.Controls.Add(this.txtACCOUNT_5);
            this.tabPage2.Controls.Add(this.label138);
            this.tabPage2.Controls.Add(this.txtSWIFT_CODE2_5);
            this.tabPage2.Controls.Add(this.txtACCOUNT_4);
            this.tabPage2.Controls.Add(this.label161);
            this.tabPage2.Controls.Add(this.txtSWIFT_CODE2_4);
            this.tabPage2.Controls.Add(this.label162);
            this.tabPage2.Controls.Add(this.label139);
            this.tabPage2.Controls.Add(this.txtSWIFT_CODE_5);
            this.tabPage2.Controls.Add(this.label140);
            this.tabPage2.Controls.Add(this.txtBANK_C_ADDRESS2_5);
            this.tabPage2.Controls.Add(this.txtSWIFT_CODE_4);
            this.tabPage2.Controls.Add(this.label163);
            this.tabPage2.Controls.Add(this.txtBANK_C_ADDRESS2_4);
            this.tabPage2.Controls.Add(this.txtBANK_C_ADDRESS_5);
            this.tabPage2.Controls.Add(this.label141);
            this.tabPage2.Controls.Add(this.txtBANK_C_ADDRESS_4);
            this.tabPage2.Controls.Add(this.label119);
            this.tabPage2.Controls.Add(this.txtACCOUNT_ENAME2_3);
            this.tabPage2.Controls.Add(this.label118);
            this.tabPage2.Controls.Add(this.txtACCOUNT_ENAME_3);
            this.tabPage2.Controls.Add(this.label117);
            this.tabPage2.Controls.Add(this.txtACCOUNT_ENAME2_2);
            this.tabPage2.Controls.Add(this.label116);
            this.tabPage2.Controls.Add(this.txtACCOUNT_ENAME_2);
            this.tabPage2.Controls.Add(this.label115);
            this.tabPage2.Controls.Add(this.txtACCOUNT_ENAME2);
            this.tabPage2.Controls.Add(this.label114);
            this.tabPage2.Controls.Add(this.txtACCOUNT_ENAME);
            this.tabPage2.Controls.Add(this.label89);
            this.tabPage2.Controls.Add(this.txtBANK_E_ADDRESS2_3);
            this.tabPage2.Controls.Add(this.label90);
            this.tabPage2.Controls.Add(this.txtBANK_ENAME2_3);
            this.tabPage2.Controls.Add(this.label91);
            this.tabPage2.Controls.Add(this.txtBRANCH_ENAME2_3);
            this.tabPage2.Controls.Add(this.label92);
            this.tabPage2.Controls.Add(this.txtBANK_E_ADDRESS_3);
            this.tabPage2.Controls.Add(this.label93);
            this.tabPage2.Controls.Add(this.txtBANK_ENAME_3);
            this.tabPage2.Controls.Add(this.label94);
            this.tabPage2.Controls.Add(this.txtBRANCH_ENAME_3);
            this.tabPage2.Controls.Add(this.panel13);
            this.tabPage2.Controls.Add(this.label96);
            this.tabPage2.Controls.Add(this.panel14);
            this.tabPage2.Controls.Add(this.txtBANK_CNAME2_3);
            this.tabPage2.Controls.Add(this.label98);
            this.tabPage2.Controls.Add(this.label99);
            this.tabPage2.Controls.Add(this.txtBANK_CNAME_3);
            this.tabPage2.Controls.Add(this.txtBRANCH_CNAME2_3);
            this.tabPage2.Controls.Add(this.label100);
            this.tabPage2.Controls.Add(this.label101);
            this.tabPage2.Controls.Add(this.txtBRANCH_CNAME_3);
            this.tabPage2.Controls.Add(this.txtACCOUNT_CNAME2_3);
            this.tabPage2.Controls.Add(this.label102);
            this.tabPage2.Controls.Add(this.label103);
            this.tabPage2.Controls.Add(this.txtACCOUNT_CNAME_3);
            this.tabPage2.Controls.Add(this.txtACCOUNT2_3);
            this.tabPage2.Controls.Add(this.label104);
            this.tabPage2.Controls.Add(this.label105);
            this.tabPage2.Controls.Add(this.txtACCOUNT_3);
            this.tabPage2.Controls.Add(this.txtSWIFT_CODE2_3);
            this.tabPage2.Controls.Add(this.label106);
            this.tabPage2.Controls.Add(this.label107);
            this.tabPage2.Controls.Add(this.txtSWIFT_CODE_3);
            this.tabPage2.Controls.Add(this.txtBANK_C_ADDRESS2_3);
            this.tabPage2.Controls.Add(this.label108);
            this.tabPage2.Controls.Add(this.txtBANK_C_ADDRESS_3);
            this.tabPage2.Controls.Add(this.label69);
            this.tabPage2.Controls.Add(this.txtBANK_E_ADDRESS2_2);
            this.tabPage2.Controls.Add(this.label70);
            this.tabPage2.Controls.Add(this.txtBANK_ENAME2_2);
            this.tabPage2.Controls.Add(this.label71);
            this.tabPage2.Controls.Add(this.txtBRANCH_ENAME2_2);
            this.tabPage2.Controls.Add(this.label72);
            this.tabPage2.Controls.Add(this.txtBANK_E_ADDRESS_2);
            this.tabPage2.Controls.Add(this.label73);
            this.tabPage2.Controls.Add(this.txtBANK_ENAME_2);
            this.tabPage2.Controls.Add(this.label74);
            this.tabPage2.Controls.Add(this.txtBRANCH_ENAME_2);
            this.tabPage2.Controls.Add(this.panel11);
            this.tabPage2.Controls.Add(this.label76);
            this.tabPage2.Controls.Add(this.panel12);
            this.tabPage2.Controls.Add(this.txtBANK_CNAME2_2);
            this.tabPage2.Controls.Add(this.label78);
            this.tabPage2.Controls.Add(this.label79);
            this.tabPage2.Controls.Add(this.txtBANK_CNAME_2);
            this.tabPage2.Controls.Add(this.txtBRANCH_CNAME2_2);
            this.tabPage2.Controls.Add(this.label80);
            this.tabPage2.Controls.Add(this.label81);
            this.tabPage2.Controls.Add(this.txtBRANCH_CNAME_2);
            this.tabPage2.Controls.Add(this.txtACCOUNT_CNAME2_2);
            this.tabPage2.Controls.Add(this.label82);
            this.tabPage2.Controls.Add(this.label83);
            this.tabPage2.Controls.Add(this.txtACCOUNT_CNAME_2);
            this.tabPage2.Controls.Add(this.txtACCOUNT2_2);
            this.tabPage2.Controls.Add(this.label84);
            this.tabPage2.Controls.Add(this.label85);
            this.tabPage2.Controls.Add(this.txtACCOUNT_2);
            this.tabPage2.Controls.Add(this.txtSWIFT_CODE2_2);
            this.tabPage2.Controls.Add(this.label86);
            this.tabPage2.Controls.Add(this.label87);
            this.tabPage2.Controls.Add(this.txtSWIFT_CODE_2);
            this.tabPage2.Controls.Add(this.txtBANK_C_ADDRESS2_2);
            this.tabPage2.Controls.Add(this.label88);
            this.tabPage2.Controls.Add(this.txtBANK_C_ADDRESS_2);
            this.tabPage2.Controls.Add(this.label68);
            this.tabPage2.Controls.Add(this.txtBANK_E_ADDRESS2);
            this.tabPage2.Controls.Add(this.label66);
            this.tabPage2.Controls.Add(this.txtBANK_ENAME2);
            this.tabPage2.Controls.Add(this.label67);
            this.tabPage2.Controls.Add(this.txtBRANCH_ENAME2);
            this.tabPage2.Controls.Add(this.label65);
            this.tabPage2.Controls.Add(this.txtBANK_E_ADDRESS);
            this.tabPage2.Controls.Add(this.label63);
            this.tabPage2.Controls.Add(this.txtBANK_ENAME);
            this.tabPage2.Controls.Add(this.label64);
            this.tabPage2.Controls.Add(this.txtBRANCH_ENAME);
            this.tabPage2.Controls.Add(this.panel10);
            this.tabPage2.Controls.Add(this.label57);
            this.tabPage2.Controls.Add(this.panel2);
            this.tabPage2.Controls.Add(this.txtBANK_CNAME2);
            this.tabPage2.Controls.Add(this.label11);
            this.tabPage2.Controls.Add(this.label58);
            this.tabPage2.Controls.Add(this.txtBANK_CNAME);
            this.tabPage2.Controls.Add(this.txtBRANCH_CNAME2);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.label59);
            this.tabPage2.Controls.Add(this.txtBRANCH_CNAME);
            this.tabPage2.Controls.Add(this.txtACCOUNT_CNAME2);
            this.tabPage2.Controls.Add(this.label12);
            this.tabPage2.Controls.Add(this.label60);
            this.tabPage2.Controls.Add(this.txtACCOUNT_CNAME);
            this.tabPage2.Controls.Add(this.txtACCOUNT2);
            this.tabPage2.Controls.Add(this.label14);
            this.tabPage2.Controls.Add(this.label61);
            this.tabPage2.Controls.Add(this.txtACCOUNT);
            this.tabPage2.Controls.Add(this.txtSWIFT_CODE2);
            this.tabPage2.Controls.Add(this.label13);
            this.tabPage2.Controls.Add(this.label62);
            this.tabPage2.Controls.Add(this.txtSWIFT_CODE);
            this.tabPage2.Controls.Add(this.txtBANK_C_ADDRESS2);
            this.tabPage2.Controls.Add(this.label15);
            this.tabPage2.Controls.Add(this.txtBANK_C_ADDRESS);
            this.tabPage2.Location = new System.Drawing.Point(4, 26);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1294, 515);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "銀行資訊";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btnImportToNZD2
            // 
            this.btnImportToNZD2.Image = ((System.Drawing.Image)(resources.GetObject("btnImportToNZD2.Image")));
            this.btnImportToNZD2.Location = new System.Drawing.Point(626, 2504);
            this.btnImportToNZD2.Name = "btnImportToNZD2";
            this.btnImportToNZD2.Size = new System.Drawing.Size(28, 28);
            this.btnImportToNZD2.TabIndex = 375;
            this.btnImportToNZD2.UseVisualStyleBackColor = true;
            this.btnImportToNZD2.Click += new System.EventHandler(this.btnImportToNZD2_Click);
            // 
            // btnImportToNZD1
            // 
            this.btnImportToNZD1.Image = ((System.Drawing.Image)(resources.GetObject("btnImportToNZD1.Image")));
            this.btnImportToNZD1.Location = new System.Drawing.Point(11, 2504);
            this.btnImportToNZD1.Name = "btnImportToNZD1";
            this.btnImportToNZD1.Size = new System.Drawing.Size(28, 28);
            this.btnImportToNZD1.TabIndex = 374;
            this.btnImportToNZD1.UseVisualStyleBackColor = true;
            this.btnImportToNZD1.Click += new System.EventHandler(this.btnImportToNZD1_Click);
            // 
            // label220
            // 
            this.label220.AutoSize = true;
            this.label220.Location = new System.Drawing.Point(623, 2565);
            this.label220.Name = "label220";
            this.label220.Size = new System.Drawing.Size(128, 18);
            this.label220.TabIndex = 372;
            this.label220.Text = "帳戶持有人姓名(英)";
            // 
            // txtACCOUNT_ENAME2_7
            // 
            this.txtACCOUNT_ENAME2_7.Location = new System.Drawing.Point(758, 2562);
            this.txtACCOUNT_ENAME2_7.Name = "txtACCOUNT_ENAME2_7";
            this.txtACCOUNT_ENAME2_7.Size = new System.Drawing.Size(421, 25);
            this.txtACCOUNT_ENAME2_7.TabIndex = 373;
            // 
            // label222
            // 
            this.label222.AutoSize = true;
            this.label222.Location = new System.Drawing.Point(9, 2565);
            this.label222.Name = "label222";
            this.label222.Size = new System.Drawing.Size(128, 18);
            this.label222.TabIndex = 370;
            this.label222.Text = "帳戶持有人姓名(英)";
            // 
            // txtACCOUNT_ENAME_7
            // 
            this.txtACCOUNT_ENAME_7.Location = new System.Drawing.Point(144, 2562);
            this.txtACCOUNT_ENAME_7.Name = "txtACCOUNT_ENAME_7";
            this.txtACCOUNT_ENAME_7.Size = new System.Drawing.Size(421, 25);
            this.txtACCOUNT_ENAME_7.TabIndex = 371;
            // 
            // label223
            // 
            this.label223.AutoSize = true;
            this.label223.Location = new System.Drawing.Point(623, 2658);
            this.label223.Name = "label223";
            this.label223.Size = new System.Drawing.Size(86, 18);
            this.label223.TabIndex = 368;
            this.label223.Text = "銀行地址(英)";
            // 
            // txtBANK_E_ADDRESS2_7
            // 
            this.txtBANK_E_ADDRESS2_7.Location = new System.Drawing.Point(720, 2655);
            this.txtBANK_E_ADDRESS2_7.Multiline = true;
            this.txtBANK_E_ADDRESS2_7.Name = "txtBANK_E_ADDRESS2_7";
            this.txtBANK_E_ADDRESS2_7.Size = new System.Drawing.Size(459, 55);
            this.txtBANK_E_ADDRESS2_7.TabIndex = 369;
            // 
            // label224
            // 
            this.label224.AutoSize = true;
            this.label224.Location = new System.Drawing.Point(623, 2483);
            this.label224.Name = "label224";
            this.label224.Size = new System.Drawing.Size(86, 18);
            this.label224.TabIndex = 364;
            this.label224.Text = "銀行名稱(英)";
            // 
            // txtBANK_ENAME2_7
            // 
            this.txtBANK_ENAME2_7.Location = new System.Drawing.Point(720, 2480);
            this.txtBANK_ENAME2_7.Multiline = true;
            this.txtBANK_ENAME2_7.Name = "txtBANK_ENAME2_7";
            this.txtBANK_ENAME2_7.Size = new System.Drawing.Size(230, 45);
            this.txtBANK_ENAME2_7.TabIndex = 365;
            // 
            // label225
            // 
            this.label225.AutoSize = true;
            this.label225.Location = new System.Drawing.Point(961, 2483);
            this.label225.Name = "label225";
            this.label225.Size = new System.Drawing.Size(86, 18);
            this.label225.TabIndex = 366;
            this.label225.Text = "分行名稱(英)";
            // 
            // txtBRANCH_ENAME2_7
            // 
            this.txtBRANCH_ENAME2_7.Location = new System.Drawing.Point(1061, 2480);
            this.txtBRANCH_ENAME2_7.Multiline = true;
            this.txtBRANCH_ENAME2_7.Name = "txtBRANCH_ENAME2_7";
            this.txtBRANCH_ENAME2_7.Size = new System.Drawing.Size(118, 45);
            this.txtBRANCH_ENAME2_7.TabIndex = 367;
            // 
            // label226
            // 
            this.label226.AutoSize = true;
            this.label226.Location = new System.Drawing.Point(9, 2658);
            this.label226.Name = "label226";
            this.label226.Size = new System.Drawing.Size(86, 18);
            this.label226.TabIndex = 362;
            this.label226.Text = "銀行地址(英)";
            // 
            // txtBANK_E_ADDRESS_7
            // 
            this.txtBANK_E_ADDRESS_7.Location = new System.Drawing.Point(106, 2655);
            this.txtBANK_E_ADDRESS_7.Multiline = true;
            this.txtBANK_E_ADDRESS_7.Name = "txtBANK_E_ADDRESS_7";
            this.txtBANK_E_ADDRESS_7.Size = new System.Drawing.Size(459, 55);
            this.txtBANK_E_ADDRESS_7.TabIndex = 363;
            // 
            // label227
            // 
            this.label227.AutoSize = true;
            this.label227.Location = new System.Drawing.Point(9, 2483);
            this.label227.Name = "label227";
            this.label227.Size = new System.Drawing.Size(86, 18);
            this.label227.TabIndex = 358;
            this.label227.Text = "銀行名稱(英)";
            // 
            // txtBANK_ENAME_7
            // 
            this.txtBANK_ENAME_7.Location = new System.Drawing.Point(106, 2480);
            this.txtBANK_ENAME_7.Multiline = true;
            this.txtBANK_ENAME_7.Name = "txtBANK_ENAME_7";
            this.txtBANK_ENAME_7.Size = new System.Drawing.Size(230, 45);
            this.txtBANK_ENAME_7.TabIndex = 359;
            // 
            // label228
            // 
            this.label228.AutoSize = true;
            this.label228.Location = new System.Drawing.Point(349, 2483);
            this.label228.Name = "label228";
            this.label228.Size = new System.Drawing.Size(86, 18);
            this.label228.TabIndex = 360;
            this.label228.Text = "分行名稱(英)";
            // 
            // txtBRANCH_ENAME_7
            // 
            this.txtBRANCH_ENAME_7.Location = new System.Drawing.Point(446, 2480);
            this.txtBRANCH_ENAME_7.Multiline = true;
            this.txtBRANCH_ENAME_7.Name = "txtBRANCH_ENAME_7";
            this.txtBRANCH_ENAME_7.Size = new System.Drawing.Size(120, 45);
            this.txtBRANCH_ENAME_7.TabIndex = 361;
            // 
            // panel25
            // 
            this.panel25.BackColor = System.Drawing.SystemColors.Info;
            this.panel25.Controls.Add(this.label229);
            this.panel25.Location = new System.Drawing.Point(620, 2385);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(562, 58);
            this.panel25.TabIndex = 345;
            // 
            // label229
            // 
            this.label229.AutoSize = true;
            this.label229.Font = new System.Drawing.Font("微軟正黑體", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label229.Location = new System.Drawing.Point(3, 12);
            this.label229.Name = "label229";
            this.label229.Size = new System.Drawing.Size(316, 31);
            this.label229.TabIndex = 0;
            this.label229.Text = "銀行資訊(聯名持有人)(NZD)";
            // 
            // label230
            // 
            this.label230.AutoSize = true;
            this.label230.Location = new System.Drawing.Point(623, 2452);
            this.label230.Name = "label230";
            this.label230.Size = new System.Drawing.Size(64, 18);
            this.label230.TabIndex = 346;
            this.label230.Text = "銀行名稱";
            // 
            // panel26
            // 
            this.panel26.BackColor = System.Drawing.SystemColors.Info;
            this.panel26.Controls.Add(this.label231);
            this.panel26.Location = new System.Drawing.Point(6, 2385);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(562, 58);
            this.panel26.TabIndex = 332;
            // 
            // label231
            // 
            this.label231.AutoSize = true;
            this.label231.Font = new System.Drawing.Font("微軟正黑體", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label231.Location = new System.Drawing.Point(3, 12);
            this.label231.Name = "label231";
            this.label231.Size = new System.Drawing.Size(180, 31);
            this.label231.TabIndex = 0;
            this.label231.Text = "銀行資訊(NZD)";
            // 
            // txtBANK_CNAME2_7
            // 
            this.txtBANK_CNAME2_7.Location = new System.Drawing.Point(720, 2449);
            this.txtBANK_CNAME2_7.Name = "txtBANK_CNAME2_7";
            this.txtBANK_CNAME2_7.Size = new System.Drawing.Size(230, 25);
            this.txtBANK_CNAME2_7.TabIndex = 347;
            // 
            // label232
            // 
            this.label232.AutoSize = true;
            this.label232.Location = new System.Drawing.Point(9, 2452);
            this.label232.Name = "label232";
            this.label232.Size = new System.Drawing.Size(64, 18);
            this.label232.TabIndex = 333;
            this.label232.Text = "銀行名稱";
            // 
            // label233
            // 
            this.label233.AutoSize = true;
            this.label233.Location = new System.Drawing.Point(961, 2452);
            this.label233.Name = "label233";
            this.label233.Size = new System.Drawing.Size(64, 18);
            this.label233.TabIndex = 348;
            this.label233.Text = "分行名稱";
            // 
            // txtBANK_CNAME_7
            // 
            this.txtBANK_CNAME_7.Location = new System.Drawing.Point(106, 2449);
            this.txtBANK_CNAME_7.Name = "txtBANK_CNAME_7";
            this.txtBANK_CNAME_7.Size = new System.Drawing.Size(230, 25);
            this.txtBANK_CNAME_7.TabIndex = 334;
            // 
            // txtBRANCH_CNAME2_7
            // 
            this.txtBRANCH_CNAME2_7.Location = new System.Drawing.Point(1061, 2449);
            this.txtBRANCH_CNAME2_7.Name = "txtBRANCH_CNAME2_7";
            this.txtBRANCH_CNAME2_7.Size = new System.Drawing.Size(118, 25);
            this.txtBRANCH_CNAME2_7.TabIndex = 349;
            // 
            // label234
            // 
            this.label234.AutoSize = true;
            this.label234.Location = new System.Drawing.Point(349, 2452);
            this.label234.Name = "label234";
            this.label234.Size = new System.Drawing.Size(64, 18);
            this.label234.TabIndex = 335;
            this.label234.Text = "分行名稱";
            // 
            // label235
            // 
            this.label235.AutoSize = true;
            this.label235.Location = new System.Drawing.Point(623, 2534);
            this.label235.Name = "label235";
            this.label235.Size = new System.Drawing.Size(106, 18);
            this.label235.TabIndex = 350;
            this.label235.Text = "帳戶持有人姓名";
            // 
            // txtBRANCH_CNAME_7
            // 
            this.txtBRANCH_CNAME_7.Location = new System.Drawing.Point(445, 2449);
            this.txtBRANCH_CNAME_7.Name = "txtBRANCH_CNAME_7";
            this.txtBRANCH_CNAME_7.Size = new System.Drawing.Size(120, 25);
            this.txtBRANCH_CNAME_7.TabIndex = 336;
            // 
            // txtACCOUNT_CNAME2_7
            // 
            this.txtACCOUNT_CNAME2_7.Location = new System.Drawing.Point(758, 2531);
            this.txtACCOUNT_CNAME2_7.Name = "txtACCOUNT_CNAME2_7";
            this.txtACCOUNT_CNAME2_7.Size = new System.Drawing.Size(421, 25);
            this.txtACCOUNT_CNAME2_7.TabIndex = 351;
            // 
            // label236
            // 
            this.label236.AutoSize = true;
            this.label236.Location = new System.Drawing.Point(9, 2534);
            this.label236.Name = "label236";
            this.label236.Size = new System.Drawing.Size(106, 18);
            this.label236.TabIndex = 337;
            this.label236.Text = "帳戶持有人姓名";
            // 
            // label237
            // 
            this.label237.AutoSize = true;
            this.label237.Location = new System.Drawing.Point(623, 2596);
            this.label237.Name = "label237";
            this.label237.Size = new System.Drawing.Size(64, 18);
            this.label237.TabIndex = 352;
            this.label237.Text = "銀行帳戶";
            // 
            // txtACCOUNT_CNAME_7
            // 
            this.txtACCOUNT_CNAME_7.Location = new System.Drawing.Point(144, 2531);
            this.txtACCOUNT_CNAME_7.Name = "txtACCOUNT_CNAME_7";
            this.txtACCOUNT_CNAME_7.Size = new System.Drawing.Size(421, 25);
            this.txtACCOUNT_CNAME_7.TabIndex = 338;
            // 
            // txtACCOUNT2_7
            // 
            this.txtACCOUNT2_7.Location = new System.Drawing.Point(720, 2593);
            this.txtACCOUNT2_7.Name = "txtACCOUNT2_7";
            this.txtACCOUNT2_7.Size = new System.Drawing.Size(230, 25);
            this.txtACCOUNT2_7.TabIndex = 353;
            // 
            // label238
            // 
            this.label238.AutoSize = true;
            this.label238.Location = new System.Drawing.Point(9, 2596);
            this.label238.Name = "label238";
            this.label238.Size = new System.Drawing.Size(64, 18);
            this.label238.TabIndex = 339;
            this.label238.Text = "銀行帳戶";
            // 
            // label239
            // 
            this.label239.AutoSize = true;
            this.label239.Location = new System.Drawing.Point(961, 2596);
            this.label239.Name = "label239";
            this.label239.Size = new System.Drawing.Size(94, 18);
            this.label239.TabIndex = 354;
            this.label239.Text = "SWIFT_CODE";
            // 
            // txtACCOUNT_7
            // 
            this.txtACCOUNT_7.Location = new System.Drawing.Point(106, 2593);
            this.txtACCOUNT_7.Name = "txtACCOUNT_7";
            this.txtACCOUNT_7.Size = new System.Drawing.Size(230, 25);
            this.txtACCOUNT_7.TabIndex = 340;
            // 
            // txtSWIFT_CODE2_7
            // 
            this.txtSWIFT_CODE2_7.Location = new System.Drawing.Point(1061, 2593);
            this.txtSWIFT_CODE2_7.Name = "txtSWIFT_CODE2_7";
            this.txtSWIFT_CODE2_7.Size = new System.Drawing.Size(118, 25);
            this.txtSWIFT_CODE2_7.TabIndex = 355;
            // 
            // label240
            // 
            this.label240.AutoSize = true;
            this.label240.Location = new System.Drawing.Point(348, 2596);
            this.label240.Name = "label240";
            this.label240.Size = new System.Drawing.Size(94, 18);
            this.label240.TabIndex = 341;
            this.label240.Text = "SWIFT_CODE";
            // 
            // label241
            // 
            this.label241.AutoSize = true;
            this.label241.Location = new System.Drawing.Point(623, 2627);
            this.label241.Name = "label241";
            this.label241.Size = new System.Drawing.Size(64, 18);
            this.label241.TabIndex = 356;
            this.label241.Text = "銀行地址";
            // 
            // txtSWIFT_CODE_7
            // 
            this.txtSWIFT_CODE_7.Location = new System.Drawing.Point(446, 2593);
            this.txtSWIFT_CODE_7.Name = "txtSWIFT_CODE_7";
            this.txtSWIFT_CODE_7.Size = new System.Drawing.Size(119, 25);
            this.txtSWIFT_CODE_7.TabIndex = 342;
            // 
            // txtBANK_C_ADDRESS2_7
            // 
            this.txtBANK_C_ADDRESS2_7.Location = new System.Drawing.Point(720, 2624);
            this.txtBANK_C_ADDRESS2_7.Name = "txtBANK_C_ADDRESS2_7";
            this.txtBANK_C_ADDRESS2_7.Size = new System.Drawing.Size(459, 25);
            this.txtBANK_C_ADDRESS2_7.TabIndex = 357;
            // 
            // label242
            // 
            this.label242.AutoSize = true;
            this.label242.Location = new System.Drawing.Point(9, 2627);
            this.label242.Name = "label242";
            this.label242.Size = new System.Drawing.Size(64, 18);
            this.label242.TabIndex = 343;
            this.label242.Text = "銀行地址";
            // 
            // txtBANK_C_ADDRESS_7
            // 
            this.txtBANK_C_ADDRESS_7.Location = new System.Drawing.Point(106, 2624);
            this.txtBANK_C_ADDRESS_7.Name = "txtBANK_C_ADDRESS_7";
            this.txtBANK_C_ADDRESS_7.Size = new System.Drawing.Size(459, 25);
            this.txtBANK_C_ADDRESS_7.TabIndex = 344;
            // 
            // btnImportToJPY2
            // 
            this.btnImportToJPY2.Image = ((System.Drawing.Image)(resources.GetObject("btnImportToJPY2.Image")));
            this.btnImportToJPY2.Location = new System.Drawing.Point(626, 2154);
            this.btnImportToJPY2.Name = "btnImportToJPY2";
            this.btnImportToJPY2.Size = new System.Drawing.Size(28, 28);
            this.btnImportToJPY2.TabIndex = 331;
            this.btnImportToJPY2.UseVisualStyleBackColor = true;
            this.btnImportToJPY2.Click += new System.EventHandler(this.btnImportToJPY2_Click);
            // 
            // btnImportToJPY1
            // 
            this.btnImportToJPY1.Image = ((System.Drawing.Image)(resources.GetObject("btnImportToJPY1.Image")));
            this.btnImportToJPY1.Location = new System.Drawing.Point(11, 2154);
            this.btnImportToJPY1.Name = "btnImportToJPY1";
            this.btnImportToJPY1.Size = new System.Drawing.Size(28, 28);
            this.btnImportToJPY1.TabIndex = 330;
            this.btnImportToJPY1.UseVisualStyleBackColor = true;
            this.btnImportToJPY1.Click += new System.EventHandler(this.btnImportToJPY1_Click);
            // 
            // label194
            // 
            this.label194.AutoSize = true;
            this.label194.Location = new System.Drawing.Point(623, 2215);
            this.label194.Name = "label194";
            this.label194.Size = new System.Drawing.Size(128, 18);
            this.label194.TabIndex = 328;
            this.label194.Text = "帳戶持有人姓名(英)";
            // 
            // txtACCOUNT_ENAME2_6
            // 
            this.txtACCOUNT_ENAME2_6.Location = new System.Drawing.Point(758, 2212);
            this.txtACCOUNT_ENAME2_6.Name = "txtACCOUNT_ENAME2_6";
            this.txtACCOUNT_ENAME2_6.Size = new System.Drawing.Size(421, 25);
            this.txtACCOUNT_ENAME2_6.TabIndex = 329;
            // 
            // label195
            // 
            this.label195.AutoSize = true;
            this.label195.Location = new System.Drawing.Point(9, 2215);
            this.label195.Name = "label195";
            this.label195.Size = new System.Drawing.Size(128, 18);
            this.label195.TabIndex = 326;
            this.label195.Text = "帳戶持有人姓名(英)";
            // 
            // txtACCOUNT_ENAME_6
            // 
            this.txtACCOUNT_ENAME_6.Location = new System.Drawing.Point(144, 2212);
            this.txtACCOUNT_ENAME_6.Name = "txtACCOUNT_ENAME_6";
            this.txtACCOUNT_ENAME_6.Size = new System.Drawing.Size(421, 25);
            this.txtACCOUNT_ENAME_6.TabIndex = 327;
            // 
            // label196
            // 
            this.label196.AutoSize = true;
            this.label196.Location = new System.Drawing.Point(623, 2308);
            this.label196.Name = "label196";
            this.label196.Size = new System.Drawing.Size(86, 18);
            this.label196.TabIndex = 324;
            this.label196.Text = "銀行地址(英)";
            // 
            // txtBANK_E_ADDRESS2_6
            // 
            this.txtBANK_E_ADDRESS2_6.Location = new System.Drawing.Point(720, 2305);
            this.txtBANK_E_ADDRESS2_6.Multiline = true;
            this.txtBANK_E_ADDRESS2_6.Name = "txtBANK_E_ADDRESS2_6";
            this.txtBANK_E_ADDRESS2_6.Size = new System.Drawing.Size(459, 55);
            this.txtBANK_E_ADDRESS2_6.TabIndex = 325;
            // 
            // label197
            // 
            this.label197.AutoSize = true;
            this.label197.Location = new System.Drawing.Point(623, 2133);
            this.label197.Name = "label197";
            this.label197.Size = new System.Drawing.Size(86, 18);
            this.label197.TabIndex = 320;
            this.label197.Text = "銀行名稱(英)";
            // 
            // txtBANK_ENAME2_6
            // 
            this.txtBANK_ENAME2_6.Location = new System.Drawing.Point(720, 2130);
            this.txtBANK_ENAME2_6.Multiline = true;
            this.txtBANK_ENAME2_6.Name = "txtBANK_ENAME2_6";
            this.txtBANK_ENAME2_6.Size = new System.Drawing.Size(230, 45);
            this.txtBANK_ENAME2_6.TabIndex = 321;
            // 
            // label198
            // 
            this.label198.AutoSize = true;
            this.label198.Location = new System.Drawing.Point(961, 2133);
            this.label198.Name = "label198";
            this.label198.Size = new System.Drawing.Size(86, 18);
            this.label198.TabIndex = 322;
            this.label198.Text = "分行名稱(英)";
            // 
            // txtBRANCH_ENAME2_6
            // 
            this.txtBRANCH_ENAME2_6.Location = new System.Drawing.Point(1061, 2130);
            this.txtBRANCH_ENAME2_6.Multiline = true;
            this.txtBRANCH_ENAME2_6.Name = "txtBRANCH_ENAME2_6";
            this.txtBRANCH_ENAME2_6.Size = new System.Drawing.Size(118, 45);
            this.txtBRANCH_ENAME2_6.TabIndex = 323;
            // 
            // label199
            // 
            this.label199.AutoSize = true;
            this.label199.Location = new System.Drawing.Point(9, 2308);
            this.label199.Name = "label199";
            this.label199.Size = new System.Drawing.Size(86, 18);
            this.label199.TabIndex = 318;
            this.label199.Text = "銀行地址(英)";
            // 
            // txtBANK_E_ADDRESS_6
            // 
            this.txtBANK_E_ADDRESS_6.Location = new System.Drawing.Point(106, 2305);
            this.txtBANK_E_ADDRESS_6.Multiline = true;
            this.txtBANK_E_ADDRESS_6.Name = "txtBANK_E_ADDRESS_6";
            this.txtBANK_E_ADDRESS_6.Size = new System.Drawing.Size(459, 55);
            this.txtBANK_E_ADDRESS_6.TabIndex = 319;
            // 
            // label200
            // 
            this.label200.AutoSize = true;
            this.label200.Location = new System.Drawing.Point(9, 2133);
            this.label200.Name = "label200";
            this.label200.Size = new System.Drawing.Size(86, 18);
            this.label200.TabIndex = 314;
            this.label200.Text = "銀行名稱(英)";
            // 
            // txtBANK_ENAME_6
            // 
            this.txtBANK_ENAME_6.Location = new System.Drawing.Point(106, 2130);
            this.txtBANK_ENAME_6.Multiline = true;
            this.txtBANK_ENAME_6.Name = "txtBANK_ENAME_6";
            this.txtBANK_ENAME_6.Size = new System.Drawing.Size(230, 45);
            this.txtBANK_ENAME_6.TabIndex = 315;
            // 
            // label201
            // 
            this.label201.AutoSize = true;
            this.label201.Location = new System.Drawing.Point(349, 2133);
            this.label201.Name = "label201";
            this.label201.Size = new System.Drawing.Size(86, 18);
            this.label201.TabIndex = 316;
            this.label201.Text = "分行名稱(英)";
            // 
            // txtBRANCH_ENAME_6
            // 
            this.txtBRANCH_ENAME_6.Location = new System.Drawing.Point(446, 2130);
            this.txtBRANCH_ENAME_6.Multiline = true;
            this.txtBRANCH_ENAME_6.Name = "txtBRANCH_ENAME_6";
            this.txtBRANCH_ENAME_6.Size = new System.Drawing.Size(120, 45);
            this.txtBRANCH_ENAME_6.TabIndex = 317;
            // 
            // panel22
            // 
            this.panel22.BackColor = System.Drawing.SystemColors.Info;
            this.panel22.Controls.Add(this.label202);
            this.panel22.Location = new System.Drawing.Point(620, 2035);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(562, 58);
            this.panel22.TabIndex = 301;
            // 
            // label202
            // 
            this.label202.AutoSize = true;
            this.label202.Font = new System.Drawing.Font("微軟正黑體", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label202.Location = new System.Drawing.Point(3, 12);
            this.label202.Name = "label202";
            this.label202.Size = new System.Drawing.Size(302, 31);
            this.label202.TabIndex = 0;
            this.label202.Text = "銀行資訊(聯名持有人)(JPY)";
            // 
            // label203
            // 
            this.label203.AutoSize = true;
            this.label203.Location = new System.Drawing.Point(623, 2102);
            this.label203.Name = "label203";
            this.label203.Size = new System.Drawing.Size(64, 18);
            this.label203.TabIndex = 302;
            this.label203.Text = "銀行名稱";
            // 
            // panel23
            // 
            this.panel23.BackColor = System.Drawing.SystemColors.Info;
            this.panel23.Controls.Add(this.label204);
            this.panel23.Location = new System.Drawing.Point(6, 2035);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(562, 58);
            this.panel23.TabIndex = 288;
            // 
            // label204
            // 
            this.label204.AutoSize = true;
            this.label204.Font = new System.Drawing.Font("微軟正黑體", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label204.Location = new System.Drawing.Point(3, 12);
            this.label204.Name = "label204";
            this.label204.Size = new System.Drawing.Size(166, 31);
            this.label204.TabIndex = 0;
            this.label204.Text = "銀行資訊(JPY)";
            // 
            // txtBANK_CNAME2_6
            // 
            this.txtBANK_CNAME2_6.Location = new System.Drawing.Point(720, 2099);
            this.txtBANK_CNAME2_6.Name = "txtBANK_CNAME2_6";
            this.txtBANK_CNAME2_6.Size = new System.Drawing.Size(230, 25);
            this.txtBANK_CNAME2_6.TabIndex = 303;
            // 
            // label205
            // 
            this.label205.AutoSize = true;
            this.label205.Location = new System.Drawing.Point(9, 2102);
            this.label205.Name = "label205";
            this.label205.Size = new System.Drawing.Size(64, 18);
            this.label205.TabIndex = 289;
            this.label205.Text = "銀行名稱";
            // 
            // label206
            // 
            this.label206.AutoSize = true;
            this.label206.Location = new System.Drawing.Point(961, 2102);
            this.label206.Name = "label206";
            this.label206.Size = new System.Drawing.Size(64, 18);
            this.label206.TabIndex = 304;
            this.label206.Text = "分行名稱";
            // 
            // txtBANK_CNAME_6
            // 
            this.txtBANK_CNAME_6.Location = new System.Drawing.Point(106, 2099);
            this.txtBANK_CNAME_6.Name = "txtBANK_CNAME_6";
            this.txtBANK_CNAME_6.Size = new System.Drawing.Size(230, 25);
            this.txtBANK_CNAME_6.TabIndex = 290;
            // 
            // txtBRANCH_CNAME2_6
            // 
            this.txtBRANCH_CNAME2_6.Location = new System.Drawing.Point(1061, 2099);
            this.txtBRANCH_CNAME2_6.Name = "txtBRANCH_CNAME2_6";
            this.txtBRANCH_CNAME2_6.Size = new System.Drawing.Size(118, 25);
            this.txtBRANCH_CNAME2_6.TabIndex = 305;
            // 
            // label207
            // 
            this.label207.AutoSize = true;
            this.label207.Location = new System.Drawing.Point(349, 2102);
            this.label207.Name = "label207";
            this.label207.Size = new System.Drawing.Size(64, 18);
            this.label207.TabIndex = 291;
            this.label207.Text = "分行名稱";
            // 
            // label208
            // 
            this.label208.AutoSize = true;
            this.label208.Location = new System.Drawing.Point(623, 2184);
            this.label208.Name = "label208";
            this.label208.Size = new System.Drawing.Size(106, 18);
            this.label208.TabIndex = 306;
            this.label208.Text = "帳戶持有人姓名";
            // 
            // txtBRANCH_CNAME_6
            // 
            this.txtBRANCH_CNAME_6.Location = new System.Drawing.Point(445, 2099);
            this.txtBRANCH_CNAME_6.Name = "txtBRANCH_CNAME_6";
            this.txtBRANCH_CNAME_6.Size = new System.Drawing.Size(120, 25);
            this.txtBRANCH_CNAME_6.TabIndex = 292;
            // 
            // txtACCOUNT_CNAME2_6
            // 
            this.txtACCOUNT_CNAME2_6.Location = new System.Drawing.Point(758, 2181);
            this.txtACCOUNT_CNAME2_6.Name = "txtACCOUNT_CNAME2_6";
            this.txtACCOUNT_CNAME2_6.Size = new System.Drawing.Size(421, 25);
            this.txtACCOUNT_CNAME2_6.TabIndex = 307;
            // 
            // label209
            // 
            this.label209.AutoSize = true;
            this.label209.Location = new System.Drawing.Point(9, 2184);
            this.label209.Name = "label209";
            this.label209.Size = new System.Drawing.Size(106, 18);
            this.label209.TabIndex = 293;
            this.label209.Text = "帳戶持有人姓名";
            // 
            // label210
            // 
            this.label210.AutoSize = true;
            this.label210.Location = new System.Drawing.Point(623, 2246);
            this.label210.Name = "label210";
            this.label210.Size = new System.Drawing.Size(64, 18);
            this.label210.TabIndex = 308;
            this.label210.Text = "銀行帳戶";
            // 
            // txtACCOUNT_CNAME_6
            // 
            this.txtACCOUNT_CNAME_6.Location = new System.Drawing.Point(144, 2181);
            this.txtACCOUNT_CNAME_6.Name = "txtACCOUNT_CNAME_6";
            this.txtACCOUNT_CNAME_6.Size = new System.Drawing.Size(421, 25);
            this.txtACCOUNT_CNAME_6.TabIndex = 294;
            // 
            // txtACCOUNT2_6
            // 
            this.txtACCOUNT2_6.Location = new System.Drawing.Point(720, 2243);
            this.txtACCOUNT2_6.Name = "txtACCOUNT2_6";
            this.txtACCOUNT2_6.Size = new System.Drawing.Size(230, 25);
            this.txtACCOUNT2_6.TabIndex = 309;
            // 
            // label211
            // 
            this.label211.AutoSize = true;
            this.label211.Location = new System.Drawing.Point(9, 2246);
            this.label211.Name = "label211";
            this.label211.Size = new System.Drawing.Size(64, 18);
            this.label211.TabIndex = 295;
            this.label211.Text = "銀行帳戶";
            // 
            // label212
            // 
            this.label212.AutoSize = true;
            this.label212.Location = new System.Drawing.Point(961, 2246);
            this.label212.Name = "label212";
            this.label212.Size = new System.Drawing.Size(94, 18);
            this.label212.TabIndex = 310;
            this.label212.Text = "SWIFT_CODE";
            // 
            // txtACCOUNT_6
            // 
            this.txtACCOUNT_6.Location = new System.Drawing.Point(106, 2243);
            this.txtACCOUNT_6.Name = "txtACCOUNT_6";
            this.txtACCOUNT_6.Size = new System.Drawing.Size(230, 25);
            this.txtACCOUNT_6.TabIndex = 296;
            // 
            // txtSWIFT_CODE2_6
            // 
            this.txtSWIFT_CODE2_6.Location = new System.Drawing.Point(1061, 2243);
            this.txtSWIFT_CODE2_6.Name = "txtSWIFT_CODE2_6";
            this.txtSWIFT_CODE2_6.Size = new System.Drawing.Size(118, 25);
            this.txtSWIFT_CODE2_6.TabIndex = 311;
            // 
            // label213
            // 
            this.label213.AutoSize = true;
            this.label213.Location = new System.Drawing.Point(348, 2246);
            this.label213.Name = "label213";
            this.label213.Size = new System.Drawing.Size(94, 18);
            this.label213.TabIndex = 297;
            this.label213.Text = "SWIFT_CODE";
            // 
            // label214
            // 
            this.label214.AutoSize = true;
            this.label214.Location = new System.Drawing.Point(623, 2277);
            this.label214.Name = "label214";
            this.label214.Size = new System.Drawing.Size(64, 18);
            this.label214.TabIndex = 312;
            this.label214.Text = "銀行地址";
            // 
            // txtSWIFT_CODE_6
            // 
            this.txtSWIFT_CODE_6.Location = new System.Drawing.Point(446, 2243);
            this.txtSWIFT_CODE_6.Name = "txtSWIFT_CODE_6";
            this.txtSWIFT_CODE_6.Size = new System.Drawing.Size(119, 25);
            this.txtSWIFT_CODE_6.TabIndex = 298;
            // 
            // txtBANK_C_ADDRESS2_6
            // 
            this.txtBANK_C_ADDRESS2_6.Location = new System.Drawing.Point(720, 2274);
            this.txtBANK_C_ADDRESS2_6.Name = "txtBANK_C_ADDRESS2_6";
            this.txtBANK_C_ADDRESS2_6.Size = new System.Drawing.Size(459, 25);
            this.txtBANK_C_ADDRESS2_6.TabIndex = 313;
            // 
            // label215
            // 
            this.label215.AutoSize = true;
            this.label215.Location = new System.Drawing.Point(9, 2277);
            this.label215.Name = "label215";
            this.label215.Size = new System.Drawing.Size(64, 18);
            this.label215.TabIndex = 299;
            this.label215.Text = "銀行地址";
            // 
            // txtBANK_C_ADDRESS_6
            // 
            this.txtBANK_C_ADDRESS_6.Location = new System.Drawing.Point(106, 2274);
            this.txtBANK_C_ADDRESS_6.Name = "txtBANK_C_ADDRESS_6";
            this.txtBANK_C_ADDRESS_6.Size = new System.Drawing.Size(459, 25);
            this.txtBANK_C_ADDRESS_6.TabIndex = 300;
            // 
            // btnImportToRMB2_2
            // 
            this.btnImportToRMB2_2.Image = ((System.Drawing.Image)(resources.GetObject("btnImportToRMB2_2.Image")));
            this.btnImportToRMB2_2.Location = new System.Drawing.Point(627, 796);
            this.btnImportToRMB2_2.Name = "btnImportToRMB2_2";
            this.btnImportToRMB2_2.Size = new System.Drawing.Size(28, 28);
            this.btnImportToRMB2_2.TabIndex = 287;
            this.btnImportToRMB2_2.UseVisualStyleBackColor = true;
            this.btnImportToRMB2_2.Click += new System.EventHandler(this.btnImportToRMB2_2_Click);
            // 
            // btnImportToRMB1_2
            // 
            this.btnImportToRMB1_2.Image = ((System.Drawing.Image)(resources.GetObject("btnImportToRMB1_2.Image")));
            this.btnImportToRMB1_2.Location = new System.Drawing.Point(12, 796);
            this.btnImportToRMB1_2.Name = "btnImportToRMB1_2";
            this.btnImportToRMB1_2.Size = new System.Drawing.Size(28, 28);
            this.btnImportToRMB1_2.TabIndex = 286;
            this.btnImportToRMB1_2.UseVisualStyleBackColor = true;
            this.btnImportToRMB1_2.Click += new System.EventHandler(this.btnImportToRMB1_2_Click);
            // 
            // label170
            // 
            this.label170.AutoSize = true;
            this.label170.Location = new System.Drawing.Point(623, 856);
            this.label170.Name = "label170";
            this.label170.Size = new System.Drawing.Size(128, 18);
            this.label170.TabIndex = 284;
            this.label170.Text = "帳戶持有人姓名(英)";
            // 
            // txtACCOUNT_ENAME2_3_2
            // 
            this.txtACCOUNT_ENAME2_3_2.Location = new System.Drawing.Point(758, 853);
            this.txtACCOUNT_ENAME2_3_2.Name = "txtACCOUNT_ENAME2_3_2";
            this.txtACCOUNT_ENAME2_3_2.Size = new System.Drawing.Size(421, 25);
            this.txtACCOUNT_ENAME2_3_2.TabIndex = 285;
            // 
            // label171
            // 
            this.label171.AutoSize = true;
            this.label171.Location = new System.Drawing.Point(9, 856);
            this.label171.Name = "label171";
            this.label171.Size = new System.Drawing.Size(128, 18);
            this.label171.TabIndex = 282;
            this.label171.Text = "帳戶持有人姓名(英)";
            // 
            // txtACCOUNT_ENAME_3_2
            // 
            this.txtACCOUNT_ENAME_3_2.Location = new System.Drawing.Point(144, 853);
            this.txtACCOUNT_ENAME_3_2.Name = "txtACCOUNT_ENAME_3_2";
            this.txtACCOUNT_ENAME_3_2.Size = new System.Drawing.Size(421, 25);
            this.txtACCOUNT_ENAME_3_2.TabIndex = 283;
            // 
            // label172
            // 
            this.label172.AutoSize = true;
            this.label172.Location = new System.Drawing.Point(623, 947);
            this.label172.Name = "label172";
            this.label172.Size = new System.Drawing.Size(86, 18);
            this.label172.TabIndex = 280;
            this.label172.Text = "銀行地址(英)";
            // 
            // txtBANK_E_ADDRESS2_3_2
            // 
            this.txtBANK_E_ADDRESS2_3_2.Location = new System.Drawing.Point(720, 944);
            this.txtBANK_E_ADDRESS2_3_2.Multiline = true;
            this.txtBANK_E_ADDRESS2_3_2.Name = "txtBANK_E_ADDRESS2_3_2";
            this.txtBANK_E_ADDRESS2_3_2.Size = new System.Drawing.Size(459, 55);
            this.txtBANK_E_ADDRESS2_3_2.TabIndex = 281;
            // 
            // label173
            // 
            this.label173.AutoSize = true;
            this.label173.Location = new System.Drawing.Point(623, 777);
            this.label173.Name = "label173";
            this.label173.Size = new System.Drawing.Size(86, 18);
            this.label173.TabIndex = 276;
            this.label173.Text = "銀行名稱(英)";
            // 
            // txtBANK_ENAME2_3_2
            // 
            this.txtBANK_ENAME2_3_2.Location = new System.Drawing.Point(720, 774);
            this.txtBANK_ENAME2_3_2.Multiline = true;
            this.txtBANK_ENAME2_3_2.Name = "txtBANK_ENAME2_3_2";
            this.txtBANK_ENAME2_3_2.Size = new System.Drawing.Size(230, 45);
            this.txtBANK_ENAME2_3_2.TabIndex = 277;
            // 
            // label174
            // 
            this.label174.AutoSize = true;
            this.label174.Location = new System.Drawing.Point(961, 777);
            this.label174.Name = "label174";
            this.label174.Size = new System.Drawing.Size(86, 18);
            this.label174.TabIndex = 278;
            this.label174.Text = "分行名稱(英)";
            // 
            // txtBRANCH_ENAME2_3_2
            // 
            this.txtBRANCH_ENAME2_3_2.Location = new System.Drawing.Point(1061, 774);
            this.txtBRANCH_ENAME2_3_2.Multiline = true;
            this.txtBRANCH_ENAME2_3_2.Name = "txtBRANCH_ENAME2_3_2";
            this.txtBRANCH_ENAME2_3_2.Size = new System.Drawing.Size(118, 45);
            this.txtBRANCH_ENAME2_3_2.TabIndex = 279;
            // 
            // label175
            // 
            this.label175.AutoSize = true;
            this.label175.Location = new System.Drawing.Point(9, 947);
            this.label175.Name = "label175";
            this.label175.Size = new System.Drawing.Size(86, 18);
            this.label175.TabIndex = 274;
            this.label175.Text = "銀行地址(英)";
            // 
            // txtBANK_E_ADDRESS_3_2
            // 
            this.txtBANK_E_ADDRESS_3_2.Location = new System.Drawing.Point(106, 944);
            this.txtBANK_E_ADDRESS_3_2.Multiline = true;
            this.txtBANK_E_ADDRESS_3_2.Name = "txtBANK_E_ADDRESS_3_2";
            this.txtBANK_E_ADDRESS_3_2.Size = new System.Drawing.Size(459, 55);
            this.txtBANK_E_ADDRESS_3_2.TabIndex = 275;
            // 
            // label176
            // 
            this.label176.AutoSize = true;
            this.label176.Location = new System.Drawing.Point(9, 777);
            this.label176.Name = "label176";
            this.label176.Size = new System.Drawing.Size(86, 18);
            this.label176.TabIndex = 270;
            this.label176.Text = "銀行名稱(英)";
            // 
            // txtBANK_ENAME_3_2
            // 
            this.txtBANK_ENAME_3_2.Location = new System.Drawing.Point(106, 774);
            this.txtBANK_ENAME_3_2.Multiline = true;
            this.txtBANK_ENAME_3_2.Name = "txtBANK_ENAME_3_2";
            this.txtBANK_ENAME_3_2.Size = new System.Drawing.Size(230, 45);
            this.txtBANK_ENAME_3_2.TabIndex = 271;
            // 
            // label177
            // 
            this.label177.AutoSize = true;
            this.label177.Location = new System.Drawing.Point(349, 777);
            this.label177.Name = "label177";
            this.label177.Size = new System.Drawing.Size(86, 18);
            this.label177.TabIndex = 272;
            this.label177.Text = "分行名稱(英)";
            // 
            // txtBRANCH_ENAME_3_2
            // 
            this.txtBRANCH_ENAME_3_2.Location = new System.Drawing.Point(446, 774);
            this.txtBRANCH_ENAME_3_2.Multiline = true;
            this.txtBRANCH_ENAME_3_2.Name = "txtBRANCH_ENAME_3_2";
            this.txtBRANCH_ENAME_3_2.Size = new System.Drawing.Size(120, 45);
            this.txtBRANCH_ENAME_3_2.TabIndex = 273;
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.SystemColors.Info;
            this.panel19.Controls.Add(this.label178);
            this.panel19.Location = new System.Drawing.Point(620, 1016);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(562, 58);
            this.panel19.TabIndex = 257;
            // 
            // label178
            // 
            this.label178.AutoSize = true;
            this.label178.Font = new System.Drawing.Font("微軟正黑體", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label178.Location = new System.Drawing.Point(3, 12);
            this.label178.Name = "label178";
            this.label178.Size = new System.Drawing.Size(365, 31);
            this.label178.TabIndex = 0;
            this.label178.Text = "銀行資訊(聯名持有人)(境外RMB)";
            // 
            // label179
            // 
            this.label179.AutoSize = true;
            this.label179.Location = new System.Drawing.Point(623, 746);
            this.label179.Name = "label179";
            this.label179.Size = new System.Drawing.Size(64, 18);
            this.label179.TabIndex = 258;
            this.label179.Text = "銀行名稱";
            // 
            // panel20
            // 
            this.panel20.BackColor = System.Drawing.SystemColors.Info;
            this.panel20.Controls.Add(this.label180);
            this.panel20.Location = new System.Drawing.Point(6, 1016);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(562, 58);
            this.panel20.TabIndex = 244;
            // 
            // label180
            // 
            this.label180.AutoSize = true;
            this.label180.Font = new System.Drawing.Font("微軟正黑體", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label180.Location = new System.Drawing.Point(3, 12);
            this.label180.Name = "label180";
            this.label180.Size = new System.Drawing.Size(229, 31);
            this.label180.TabIndex = 0;
            this.label180.Text = "銀行資訊(境外RMB)";
            // 
            // txtBANK_CNAME2_3_2
            // 
            this.txtBANK_CNAME2_3_2.Location = new System.Drawing.Point(720, 743);
            this.txtBANK_CNAME2_3_2.Name = "txtBANK_CNAME2_3_2";
            this.txtBANK_CNAME2_3_2.Size = new System.Drawing.Size(230, 25);
            this.txtBANK_CNAME2_3_2.TabIndex = 259;
            // 
            // label181
            // 
            this.label181.AutoSize = true;
            this.label181.Location = new System.Drawing.Point(9, 746);
            this.label181.Name = "label181";
            this.label181.Size = new System.Drawing.Size(64, 18);
            this.label181.TabIndex = 245;
            this.label181.Text = "銀行名稱";
            // 
            // label182
            // 
            this.label182.AutoSize = true;
            this.label182.Location = new System.Drawing.Point(961, 746);
            this.label182.Name = "label182";
            this.label182.Size = new System.Drawing.Size(64, 18);
            this.label182.TabIndex = 260;
            this.label182.Text = "分行名稱";
            // 
            // txtBANK_CNAME_3_2
            // 
            this.txtBANK_CNAME_3_2.Location = new System.Drawing.Point(106, 743);
            this.txtBANK_CNAME_3_2.Name = "txtBANK_CNAME_3_2";
            this.txtBANK_CNAME_3_2.Size = new System.Drawing.Size(230, 25);
            this.txtBANK_CNAME_3_2.TabIndex = 246;
            // 
            // txtBRANCH_CNAME2_3_2
            // 
            this.txtBRANCH_CNAME2_3_2.Location = new System.Drawing.Point(1061, 743);
            this.txtBRANCH_CNAME2_3_2.Name = "txtBRANCH_CNAME2_3_2";
            this.txtBRANCH_CNAME2_3_2.Size = new System.Drawing.Size(118, 25);
            this.txtBRANCH_CNAME2_3_2.TabIndex = 261;
            // 
            // label183
            // 
            this.label183.AutoSize = true;
            this.label183.Location = new System.Drawing.Point(349, 746);
            this.label183.Name = "label183";
            this.label183.Size = new System.Drawing.Size(64, 18);
            this.label183.TabIndex = 247;
            this.label183.Text = "分行名稱";
            // 
            // label184
            // 
            this.label184.AutoSize = true;
            this.label184.Location = new System.Drawing.Point(623, 827);
            this.label184.Name = "label184";
            this.label184.Size = new System.Drawing.Size(106, 18);
            this.label184.TabIndex = 262;
            this.label184.Text = "帳戶持有人姓名";
            // 
            // txtBRANCH_CNAME_3_2
            // 
            this.txtBRANCH_CNAME_3_2.Location = new System.Drawing.Point(446, 743);
            this.txtBRANCH_CNAME_3_2.Name = "txtBRANCH_CNAME_3_2";
            this.txtBRANCH_CNAME_3_2.Size = new System.Drawing.Size(120, 25);
            this.txtBRANCH_CNAME_3_2.TabIndex = 248;
            // 
            // txtACCOUNT_CNAME2_3_2
            // 
            this.txtACCOUNT_CNAME2_3_2.Location = new System.Drawing.Point(758, 824);
            this.txtACCOUNT_CNAME2_3_2.Name = "txtACCOUNT_CNAME2_3_2";
            this.txtACCOUNT_CNAME2_3_2.Size = new System.Drawing.Size(421, 25);
            this.txtACCOUNT_CNAME2_3_2.TabIndex = 263;
            // 
            // label185
            // 
            this.label185.AutoSize = true;
            this.label185.Location = new System.Drawing.Point(9, 827);
            this.label185.Name = "label185";
            this.label185.Size = new System.Drawing.Size(106, 18);
            this.label185.TabIndex = 249;
            this.label185.Text = "帳戶持有人姓名";
            // 
            // label186
            // 
            this.label186.AutoSize = true;
            this.label186.Location = new System.Drawing.Point(623, 885);
            this.label186.Name = "label186";
            this.label186.Size = new System.Drawing.Size(64, 18);
            this.label186.TabIndex = 264;
            this.label186.Text = "銀行帳戶";
            // 
            // txtACCOUNT_CNAME_3_2
            // 
            this.txtACCOUNT_CNAME_3_2.Location = new System.Drawing.Point(144, 824);
            this.txtACCOUNT_CNAME_3_2.Name = "txtACCOUNT_CNAME_3_2";
            this.txtACCOUNT_CNAME_3_2.Size = new System.Drawing.Size(421, 25);
            this.txtACCOUNT_CNAME_3_2.TabIndex = 250;
            // 
            // txtACCOUNT2_3_2
            // 
            this.txtACCOUNT2_3_2.Location = new System.Drawing.Point(720, 882);
            this.txtACCOUNT2_3_2.Name = "txtACCOUNT2_3_2";
            this.txtACCOUNT2_3_2.Size = new System.Drawing.Size(230, 25);
            this.txtACCOUNT2_3_2.TabIndex = 265;
            // 
            // label187
            // 
            this.label187.AutoSize = true;
            this.label187.Location = new System.Drawing.Point(9, 885);
            this.label187.Name = "label187";
            this.label187.Size = new System.Drawing.Size(64, 18);
            this.label187.TabIndex = 251;
            this.label187.Text = "銀行帳戶";
            // 
            // label188
            // 
            this.label188.AutoSize = true;
            this.label188.Location = new System.Drawing.Point(961, 885);
            this.label188.Name = "label188";
            this.label188.Size = new System.Drawing.Size(94, 18);
            this.label188.TabIndex = 266;
            this.label188.Text = "SWIFT_CODE";
            // 
            // txtACCOUNT_3_2
            // 
            this.txtACCOUNT_3_2.Location = new System.Drawing.Point(106, 882);
            this.txtACCOUNT_3_2.Name = "txtACCOUNT_3_2";
            this.txtACCOUNT_3_2.Size = new System.Drawing.Size(230, 25);
            this.txtACCOUNT_3_2.TabIndex = 252;
            // 
            // txtSWIFT_CODE2_3_2
            // 
            this.txtSWIFT_CODE2_3_2.Location = new System.Drawing.Point(1061, 882);
            this.txtSWIFT_CODE2_3_2.Name = "txtSWIFT_CODE2_3_2";
            this.txtSWIFT_CODE2_3_2.Size = new System.Drawing.Size(118, 25);
            this.txtSWIFT_CODE2_3_2.TabIndex = 267;
            // 
            // label189
            // 
            this.label189.AutoSize = true;
            this.label189.Location = new System.Drawing.Point(348, 885);
            this.label189.Name = "label189";
            this.label189.Size = new System.Drawing.Size(94, 18);
            this.label189.TabIndex = 253;
            this.label189.Text = "SWIFT_CODE";
            // 
            // label190
            // 
            this.label190.AutoSize = true;
            this.label190.Location = new System.Drawing.Point(623, 916);
            this.label190.Name = "label190";
            this.label190.Size = new System.Drawing.Size(64, 18);
            this.label190.TabIndex = 268;
            this.label190.Text = "銀行地址";
            // 
            // txtSWIFT_CODE_3_2
            // 
            this.txtSWIFT_CODE_3_2.Location = new System.Drawing.Point(446, 882);
            this.txtSWIFT_CODE_3_2.Name = "txtSWIFT_CODE_3_2";
            this.txtSWIFT_CODE_3_2.Size = new System.Drawing.Size(119, 25);
            this.txtSWIFT_CODE_3_2.TabIndex = 254;
            // 
            // txtBANK_C_ADDRESS2_3_2
            // 
            this.txtBANK_C_ADDRESS2_3_2.Location = new System.Drawing.Point(720, 913);
            this.txtBANK_C_ADDRESS2_3_2.Name = "txtBANK_C_ADDRESS2_3_2";
            this.txtBANK_C_ADDRESS2_3_2.Size = new System.Drawing.Size(459, 25);
            this.txtBANK_C_ADDRESS2_3_2.TabIndex = 269;
            // 
            // label191
            // 
            this.label191.AutoSize = true;
            this.label191.Location = new System.Drawing.Point(9, 916);
            this.label191.Name = "label191";
            this.label191.Size = new System.Drawing.Size(64, 18);
            this.label191.TabIndex = 255;
            this.label191.Text = "銀行地址";
            // 
            // txtBANK_C_ADDRESS_3_2
            // 
            this.txtBANK_C_ADDRESS_3_2.Location = new System.Drawing.Point(106, 913);
            this.txtBANK_C_ADDRESS_3_2.Name = "txtBANK_C_ADDRESS_3_2";
            this.txtBANK_C_ADDRESS_3_2.Size = new System.Drawing.Size(459, 25);
            this.txtBANK_C_ADDRESS_3_2.TabIndex = 256;
            // 
            // btnImportToAUD2
            // 
            this.btnImportToAUD2.Image = ((System.Drawing.Image)(resources.GetObject("btnImportToAUD2.Image")));
            this.btnImportToAUD2.Location = new System.Drawing.Point(627, 1810);
            this.btnImportToAUD2.Name = "btnImportToAUD2";
            this.btnImportToAUD2.Size = new System.Drawing.Size(28, 28);
            this.btnImportToAUD2.TabIndex = 243;
            this.btnImportToAUD2.UseVisualStyleBackColor = true;
            this.btnImportToAUD2.Click += new System.EventHandler(this.btnImportToAUD2_Click);
            // 
            // btnImportToAUD1
            // 
            this.btnImportToAUD1.Image = ((System.Drawing.Image)(resources.GetObject("btnImportToAUD1.Image")));
            this.btnImportToAUD1.Location = new System.Drawing.Point(12, 1810);
            this.btnImportToAUD1.Name = "btnImportToAUD1";
            this.btnImportToAUD1.Size = new System.Drawing.Size(28, 28);
            this.btnImportToAUD1.TabIndex = 242;
            this.btnImportToAUD1.UseVisualStyleBackColor = true;
            this.btnImportToAUD1.Click += new System.EventHandler(this.btnImportToAUD1_Click);
            // 
            // btnImportToEUR2
            // 
            this.btnImportToEUR2.Image = ((System.Drawing.Image)(resources.GetObject("btnImportToEUR2.Image")));
            this.btnImportToEUR2.Location = new System.Drawing.Point(627, 1475);
            this.btnImportToEUR2.Name = "btnImportToEUR2";
            this.btnImportToEUR2.Size = new System.Drawing.Size(28, 28);
            this.btnImportToEUR2.TabIndex = 241;
            this.btnImportToEUR2.UseVisualStyleBackColor = true;
            this.btnImportToEUR2.Click += new System.EventHandler(this.btnImportToEUR2_Click);
            // 
            // btnImportToEUR1
            // 
            this.btnImportToEUR1.Image = ((System.Drawing.Image)(resources.GetObject("btnImportToEUR1.Image")));
            this.btnImportToEUR1.Location = new System.Drawing.Point(12, 1475);
            this.btnImportToEUR1.Name = "btnImportToEUR1";
            this.btnImportToEUR1.Size = new System.Drawing.Size(28, 28);
            this.btnImportToEUR1.TabIndex = 240;
            this.btnImportToEUR1.UseVisualStyleBackColor = true;
            this.btnImportToEUR1.Click += new System.EventHandler(this.btnImportToEUR1_Click);
            // 
            // btnImportToRMB2
            // 
            this.btnImportToRMB2.Image = ((System.Drawing.Image)(resources.GetObject("btnImportToRMB2.Image")));
            this.btnImportToRMB2.Location = new System.Drawing.Point(628, 1133);
            this.btnImportToRMB2.Name = "btnImportToRMB2";
            this.btnImportToRMB2.Size = new System.Drawing.Size(28, 28);
            this.btnImportToRMB2.TabIndex = 239;
            this.btnImportToRMB2.UseVisualStyleBackColor = true;
            this.btnImportToRMB2.Click += new System.EventHandler(this.btnImportToRMB2_Click);
            // 
            // btnImportToRMB1
            // 
            this.btnImportToRMB1.Image = ((System.Drawing.Image)(resources.GetObject("btnImportToRMB1.Image")));
            this.btnImportToRMB1.Location = new System.Drawing.Point(13, 1133);
            this.btnImportToRMB1.Name = "btnImportToRMB1";
            this.btnImportToRMB1.Size = new System.Drawing.Size(28, 28);
            this.btnImportToRMB1.TabIndex = 238;
            this.btnImportToRMB1.UseVisualStyleBackColor = true;
            this.btnImportToRMB1.Click += new System.EventHandler(this.btnImportToRMB1_Click);
            // 
            // btnImportToNTD2
            // 
            this.btnImportToNTD2.Image = ((System.Drawing.Image)(resources.GetObject("btnImportToNTD2.Image")));
            this.btnImportToNTD2.Location = new System.Drawing.Point(627, 462);
            this.btnImportToNTD2.Name = "btnImportToNTD2";
            this.btnImportToNTD2.Size = new System.Drawing.Size(28, 28);
            this.btnImportToNTD2.TabIndex = 237;
            this.btnImportToNTD2.UseVisualStyleBackColor = true;
            this.btnImportToNTD2.Click += new System.EventHandler(this.btnImportToNTD2_Click);
            // 
            // btnImportToNTD1
            // 
            this.btnImportToNTD1.Image = ((System.Drawing.Image)(resources.GetObject("btnImportToNTD1.Image")));
            this.btnImportToNTD1.Location = new System.Drawing.Point(12, 462);
            this.btnImportToNTD1.Name = "btnImportToNTD1";
            this.btnImportToNTD1.Size = new System.Drawing.Size(28, 28);
            this.btnImportToNTD1.TabIndex = 236;
            this.btnImportToNTD1.UseVisualStyleBackColor = true;
            this.btnImportToNTD1.Click += new System.EventHandler(this.btnImportToNTD1_Click);
            // 
            // btnImportToUSD2
            // 
            this.btnImportToUSD2.Image = ((System.Drawing.Image)(resources.GetObject("btnImportToUSD2.Image")));
            this.btnImportToUSD2.Location = new System.Drawing.Point(626, 125);
            this.btnImportToUSD2.Name = "btnImportToUSD2";
            this.btnImportToUSD2.Size = new System.Drawing.Size(28, 28);
            this.btnImportToUSD2.TabIndex = 235;
            this.btnImportToUSD2.UseVisualStyleBackColor = true;
            this.btnImportToUSD2.Click += new System.EventHandler(this.btnImportToUSD2_Click);
            // 
            // btnImportToUSD1
            // 
            this.btnImportToUSD1.Image = ((System.Drawing.Image)(resources.GetObject("btnImportToUSD1.Image")));
            this.btnImportToUSD1.Location = new System.Drawing.Point(11, 125);
            this.btnImportToUSD1.Name = "btnImportToUSD1";
            this.btnImportToUSD1.Size = new System.Drawing.Size(28, 28);
            this.btnImportToUSD1.TabIndex = 234;
            this.btnImportToUSD1.UseVisualStyleBackColor = true;
            this.btnImportToUSD1.Click += new System.EventHandler(this.btnImportToUSD1_Click);
            // 
            // label142
            // 
            this.label142.AutoSize = true;
            this.label142.Location = new System.Drawing.Point(623, 1869);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(128, 18);
            this.label142.TabIndex = 232;
            this.label142.Text = "帳戶持有人姓名(英)";
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Location = new System.Drawing.Point(623, 1535);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(128, 18);
            this.label120.TabIndex = 190;
            this.label120.Text = "帳戶持有人姓名(英)";
            // 
            // txtACCOUNT_ENAME2_5
            // 
            this.txtACCOUNT_ENAME2_5.Location = new System.Drawing.Point(758, 1866);
            this.txtACCOUNT_ENAME2_5.Name = "txtACCOUNT_ENAME2_5";
            this.txtACCOUNT_ENAME2_5.Size = new System.Drawing.Size(421, 25);
            this.txtACCOUNT_ENAME2_5.TabIndex = 233;
            // 
            // txtACCOUNT_ENAME2_4
            // 
            this.txtACCOUNT_ENAME2_4.Location = new System.Drawing.Point(758, 1532);
            this.txtACCOUNT_ENAME2_4.Name = "txtACCOUNT_ENAME2_4";
            this.txtACCOUNT_ENAME2_4.Size = new System.Drawing.Size(421, 25);
            this.txtACCOUNT_ENAME2_4.TabIndex = 191;
            // 
            // label143
            // 
            this.label143.AutoSize = true;
            this.label143.Location = new System.Drawing.Point(9, 1869);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(128, 18);
            this.label143.TabIndex = 230;
            this.label143.Text = "帳戶持有人姓名(英)";
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Location = new System.Drawing.Point(9, 1535);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(128, 18);
            this.label121.TabIndex = 188;
            this.label121.Text = "帳戶持有人姓名(英)";
            // 
            // txtACCOUNT_ENAME_5
            // 
            this.txtACCOUNT_ENAME_5.Location = new System.Drawing.Point(144, 1866);
            this.txtACCOUNT_ENAME_5.Name = "txtACCOUNT_ENAME_5";
            this.txtACCOUNT_ENAME_5.Size = new System.Drawing.Size(421, 25);
            this.txtACCOUNT_ENAME_5.TabIndex = 231;
            // 
            // txtACCOUNT_ENAME_4
            // 
            this.txtACCOUNT_ENAME_4.Location = new System.Drawing.Point(144, 1532);
            this.txtACCOUNT_ENAME_4.Name = "txtACCOUNT_ENAME_4";
            this.txtACCOUNT_ENAME_4.Size = new System.Drawing.Size(421, 25);
            this.txtACCOUNT_ENAME_4.TabIndex = 189;
            // 
            // label144
            // 
            this.label144.AutoSize = true;
            this.label144.Location = new System.Drawing.Point(623, 1960);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(86, 18);
            this.label144.TabIndex = 228;
            this.label144.Text = "銀行地址(英)";
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Location = new System.Drawing.Point(623, 1627);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(86, 18);
            this.label122.TabIndex = 186;
            this.label122.Text = "銀行地址(英)";
            // 
            // txtBANK_E_ADDRESS2_5
            // 
            this.txtBANK_E_ADDRESS2_5.Location = new System.Drawing.Point(720, 1957);
            this.txtBANK_E_ADDRESS2_5.Multiline = true;
            this.txtBANK_E_ADDRESS2_5.Name = "txtBANK_E_ADDRESS2_5";
            this.txtBANK_E_ADDRESS2_5.Size = new System.Drawing.Size(459, 55);
            this.txtBANK_E_ADDRESS2_5.TabIndex = 229;
            // 
            // txtBANK_E_ADDRESS2_4
            // 
            this.txtBANK_E_ADDRESS2_4.Location = new System.Drawing.Point(720, 1624);
            this.txtBANK_E_ADDRESS2_4.Multiline = true;
            this.txtBANK_E_ADDRESS2_4.Name = "txtBANK_E_ADDRESS2_4";
            this.txtBANK_E_ADDRESS2_4.Size = new System.Drawing.Size(459, 55);
            this.txtBANK_E_ADDRESS2_4.TabIndex = 187;
            // 
            // label145
            // 
            this.label145.AutoSize = true;
            this.label145.Location = new System.Drawing.Point(623, 1790);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(86, 18);
            this.label145.TabIndex = 224;
            this.label145.Text = "銀行名稱(英)";
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Location = new System.Drawing.Point(623, 1456);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(86, 18);
            this.label123.TabIndex = 182;
            this.label123.Text = "銀行名稱(英)";
            // 
            // txtBANK_ENAME2_5
            // 
            this.txtBANK_ENAME2_5.Location = new System.Drawing.Point(720, 1787);
            this.txtBANK_ENAME2_5.Multiline = true;
            this.txtBANK_ENAME2_5.Name = "txtBANK_ENAME2_5";
            this.txtBANK_ENAME2_5.Size = new System.Drawing.Size(230, 45);
            this.txtBANK_ENAME2_5.TabIndex = 225;
            // 
            // txtBANK_ENAME2_4
            // 
            this.txtBANK_ENAME2_4.Location = new System.Drawing.Point(720, 1453);
            this.txtBANK_ENAME2_4.Multiline = true;
            this.txtBANK_ENAME2_4.Name = "txtBANK_ENAME2_4";
            this.txtBANK_ENAME2_4.Size = new System.Drawing.Size(230, 45);
            this.txtBANK_ENAME2_4.TabIndex = 183;
            // 
            // label146
            // 
            this.label146.AutoSize = true;
            this.label146.Location = new System.Drawing.Point(961, 1790);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(86, 18);
            this.label146.TabIndex = 226;
            this.label146.Text = "分行名稱(英)";
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Location = new System.Drawing.Point(961, 1456);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(86, 18);
            this.label124.TabIndex = 184;
            this.label124.Text = "分行名稱(英)";
            // 
            // txtBRANCH_ENAME2_5
            // 
            this.txtBRANCH_ENAME2_5.Location = new System.Drawing.Point(1061, 1787);
            this.txtBRANCH_ENAME2_5.Multiline = true;
            this.txtBRANCH_ENAME2_5.Name = "txtBRANCH_ENAME2_5";
            this.txtBRANCH_ENAME2_5.Size = new System.Drawing.Size(118, 45);
            this.txtBRANCH_ENAME2_5.TabIndex = 227;
            // 
            // txtBRANCH_ENAME2_4
            // 
            this.txtBRANCH_ENAME2_4.Location = new System.Drawing.Point(1061, 1453);
            this.txtBRANCH_ENAME2_4.Multiline = true;
            this.txtBRANCH_ENAME2_4.Name = "txtBRANCH_ENAME2_4";
            this.txtBRANCH_ENAME2_4.Size = new System.Drawing.Size(118, 45);
            this.txtBRANCH_ENAME2_4.TabIndex = 185;
            // 
            // label147
            // 
            this.label147.AutoSize = true;
            this.label147.Location = new System.Drawing.Point(9, 1960);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(86, 18);
            this.label147.TabIndex = 222;
            this.label147.Text = "銀行地址(英)";
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Location = new System.Drawing.Point(9, 1627);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(86, 18);
            this.label125.TabIndex = 180;
            this.label125.Text = "銀行地址(英)";
            // 
            // txtBANK_E_ADDRESS_5
            // 
            this.txtBANK_E_ADDRESS_5.Location = new System.Drawing.Point(106, 1957);
            this.txtBANK_E_ADDRESS_5.Multiline = true;
            this.txtBANK_E_ADDRESS_5.Name = "txtBANK_E_ADDRESS_5";
            this.txtBANK_E_ADDRESS_5.Size = new System.Drawing.Size(459, 55);
            this.txtBANK_E_ADDRESS_5.TabIndex = 223;
            // 
            // txtBANK_E_ADDRESS_4
            // 
            this.txtBANK_E_ADDRESS_4.Location = new System.Drawing.Point(106, 1624);
            this.txtBANK_E_ADDRESS_4.Multiline = true;
            this.txtBANK_E_ADDRESS_4.Name = "txtBANK_E_ADDRESS_4";
            this.txtBANK_E_ADDRESS_4.Size = new System.Drawing.Size(459, 55);
            this.txtBANK_E_ADDRESS_4.TabIndex = 181;
            // 
            // label148
            // 
            this.label148.AutoSize = true;
            this.label148.Location = new System.Drawing.Point(9, 1790);
            this.label148.Name = "label148";
            this.label148.Size = new System.Drawing.Size(86, 18);
            this.label148.TabIndex = 218;
            this.label148.Text = "銀行名稱(英)";
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Location = new System.Drawing.Point(9, 1456);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(86, 18);
            this.label126.TabIndex = 176;
            this.label126.Text = "銀行名稱(英)";
            // 
            // txtBANK_ENAME_5
            // 
            this.txtBANK_ENAME_5.Location = new System.Drawing.Point(106, 1787);
            this.txtBANK_ENAME_5.Multiline = true;
            this.txtBANK_ENAME_5.Name = "txtBANK_ENAME_5";
            this.txtBANK_ENAME_5.Size = new System.Drawing.Size(230, 45);
            this.txtBANK_ENAME_5.TabIndex = 219;
            // 
            // txtBANK_ENAME_4
            // 
            this.txtBANK_ENAME_4.Location = new System.Drawing.Point(106, 1453);
            this.txtBANK_ENAME_4.Multiline = true;
            this.txtBANK_ENAME_4.Name = "txtBANK_ENAME_4";
            this.txtBANK_ENAME_4.Size = new System.Drawing.Size(230, 45);
            this.txtBANK_ENAME_4.TabIndex = 177;
            // 
            // label149
            // 
            this.label149.AutoSize = true;
            this.label149.Location = new System.Drawing.Point(349, 1790);
            this.label149.Name = "label149";
            this.label149.Size = new System.Drawing.Size(86, 18);
            this.label149.TabIndex = 220;
            this.label149.Text = "分行名稱(英)";
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Location = new System.Drawing.Point(349, 1456);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(86, 18);
            this.label127.TabIndex = 178;
            this.label127.Text = "分行名稱(英)";
            // 
            // txtBRANCH_ENAME_5
            // 
            this.txtBRANCH_ENAME_5.Location = new System.Drawing.Point(446, 1787);
            this.txtBRANCH_ENAME_5.Multiline = true;
            this.txtBRANCH_ENAME_5.Name = "txtBRANCH_ENAME_5";
            this.txtBRANCH_ENAME_5.Size = new System.Drawing.Size(120, 45);
            this.txtBRANCH_ENAME_5.TabIndex = 221;
            // 
            // txtBRANCH_ENAME_4
            // 
            this.txtBRANCH_ENAME_4.Location = new System.Drawing.Point(446, 1453);
            this.txtBRANCH_ENAME_4.Multiline = true;
            this.txtBRANCH_ENAME_4.Name = "txtBRANCH_ENAME_4";
            this.txtBRANCH_ENAME_4.Size = new System.Drawing.Size(120, 45);
            this.txtBRANCH_ENAME_4.TabIndex = 179;
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.SystemColors.Info;
            this.panel17.Controls.Add(this.label150);
            this.panel17.Location = new System.Drawing.Point(620, 1692);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(562, 58);
            this.panel17.TabIndex = 205;
            // 
            // label150
            // 
            this.label150.AutoSize = true;
            this.label150.Font = new System.Drawing.Font("微軟正黑體", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label150.Location = new System.Drawing.Point(3, 12);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(316, 31);
            this.label150.TabIndex = 0;
            this.label150.Text = "銀行資訊(聯名持有人)(AUD)";
            // 
            // label151
            // 
            this.label151.AutoSize = true;
            this.label151.Location = new System.Drawing.Point(623, 1759);
            this.label151.Name = "label151";
            this.label151.Size = new System.Drawing.Size(64, 18);
            this.label151.TabIndex = 206;
            this.label151.Text = "銀行名稱";
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.SystemColors.Info;
            this.panel15.Controls.Add(this.label128);
            this.panel15.Location = new System.Drawing.Point(620, 1358);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(562, 58);
            this.panel15.TabIndex = 163;
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Font = new System.Drawing.Font("微軟正黑體", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label128.Location = new System.Drawing.Point(3, 12);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(309, 31);
            this.label128.TabIndex = 0;
            this.label128.Text = "銀行資訊(聯名持有人)(EUR)";
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.SystemColors.Info;
            this.panel18.Controls.Add(this.label152);
            this.panel18.Location = new System.Drawing.Point(6, 1692);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(562, 58);
            this.panel18.TabIndex = 192;
            // 
            // label152
            // 
            this.label152.AutoSize = true;
            this.label152.Font = new System.Drawing.Font("微軟正黑體", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label152.Location = new System.Drawing.Point(3, 12);
            this.label152.Name = "label152";
            this.label152.Size = new System.Drawing.Size(180, 31);
            this.label152.TabIndex = 0;
            this.label152.Text = "銀行資訊(AUD)";
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Location = new System.Drawing.Point(623, 1425);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(64, 18);
            this.label129.TabIndex = 164;
            this.label129.Text = "銀行名稱";
            // 
            // txtBANK_CNAME2_5
            // 
            this.txtBANK_CNAME2_5.Location = new System.Drawing.Point(720, 1756);
            this.txtBANK_CNAME2_5.Name = "txtBANK_CNAME2_5";
            this.txtBANK_CNAME2_5.Size = new System.Drawing.Size(230, 25);
            this.txtBANK_CNAME2_5.TabIndex = 207;
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.SystemColors.Info;
            this.panel16.Controls.Add(this.label130);
            this.panel16.Location = new System.Drawing.Point(6, 1358);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(562, 58);
            this.panel16.TabIndex = 150;
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Font = new System.Drawing.Font("微軟正黑體", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label130.Location = new System.Drawing.Point(3, 12);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(173, 31);
            this.label130.TabIndex = 0;
            this.label130.Text = "銀行資訊(EUR)";
            // 
            // label153
            // 
            this.label153.AutoSize = true;
            this.label153.Location = new System.Drawing.Point(9, 1759);
            this.label153.Name = "label153";
            this.label153.Size = new System.Drawing.Size(64, 18);
            this.label153.TabIndex = 193;
            this.label153.Text = "銀行名稱";
            // 
            // txtBANK_CNAME2_4
            // 
            this.txtBANK_CNAME2_4.Location = new System.Drawing.Point(720, 1422);
            this.txtBANK_CNAME2_4.Name = "txtBANK_CNAME2_4";
            this.txtBANK_CNAME2_4.Size = new System.Drawing.Size(230, 25);
            this.txtBANK_CNAME2_4.TabIndex = 165;
            // 
            // label154
            // 
            this.label154.AutoSize = true;
            this.label154.Location = new System.Drawing.Point(961, 1759);
            this.label154.Name = "label154";
            this.label154.Size = new System.Drawing.Size(64, 18);
            this.label154.TabIndex = 208;
            this.label154.Text = "分行名稱";
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Location = new System.Drawing.Point(9, 1425);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(64, 18);
            this.label131.TabIndex = 151;
            this.label131.Text = "銀行名稱";
            // 
            // txtBANK_CNAME_5
            // 
            this.txtBANK_CNAME_5.Location = new System.Drawing.Point(106, 1756);
            this.txtBANK_CNAME_5.Name = "txtBANK_CNAME_5";
            this.txtBANK_CNAME_5.Size = new System.Drawing.Size(230, 25);
            this.txtBANK_CNAME_5.TabIndex = 194;
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.Location = new System.Drawing.Point(961, 1425);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(64, 18);
            this.label132.TabIndex = 166;
            this.label132.Text = "分行名稱";
            // 
            // txtBRANCH_CNAME2_5
            // 
            this.txtBRANCH_CNAME2_5.Location = new System.Drawing.Point(1061, 1756);
            this.txtBRANCH_CNAME2_5.Name = "txtBRANCH_CNAME2_5";
            this.txtBRANCH_CNAME2_5.Size = new System.Drawing.Size(118, 25);
            this.txtBRANCH_CNAME2_5.TabIndex = 209;
            // 
            // txtBANK_CNAME_4
            // 
            this.txtBANK_CNAME_4.Location = new System.Drawing.Point(106, 1422);
            this.txtBANK_CNAME_4.Name = "txtBANK_CNAME_4";
            this.txtBANK_CNAME_4.Size = new System.Drawing.Size(230, 25);
            this.txtBANK_CNAME_4.TabIndex = 152;
            // 
            // label155
            // 
            this.label155.AutoSize = true;
            this.label155.Location = new System.Drawing.Point(349, 1759);
            this.label155.Name = "label155";
            this.label155.Size = new System.Drawing.Size(64, 18);
            this.label155.TabIndex = 195;
            this.label155.Text = "分行名稱";
            // 
            // txtBRANCH_CNAME2_4
            // 
            this.txtBRANCH_CNAME2_4.Location = new System.Drawing.Point(1061, 1422);
            this.txtBRANCH_CNAME2_4.Name = "txtBRANCH_CNAME2_4";
            this.txtBRANCH_CNAME2_4.Size = new System.Drawing.Size(118, 25);
            this.txtBRANCH_CNAME2_4.TabIndex = 167;
            // 
            // label156
            // 
            this.label156.AutoSize = true;
            this.label156.Location = new System.Drawing.Point(623, 1840);
            this.label156.Name = "label156";
            this.label156.Size = new System.Drawing.Size(106, 18);
            this.label156.TabIndex = 210;
            this.label156.Text = "帳戶持有人姓名";
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.Location = new System.Drawing.Point(349, 1425);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(64, 18);
            this.label133.TabIndex = 153;
            this.label133.Text = "分行名稱";
            // 
            // txtBRANCH_CNAME_5
            // 
            this.txtBRANCH_CNAME_5.Location = new System.Drawing.Point(446, 1756);
            this.txtBRANCH_CNAME_5.Name = "txtBRANCH_CNAME_5";
            this.txtBRANCH_CNAME_5.Size = new System.Drawing.Size(120, 25);
            this.txtBRANCH_CNAME_5.TabIndex = 196;
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.Location = new System.Drawing.Point(623, 1506);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(106, 18);
            this.label134.TabIndex = 168;
            this.label134.Text = "帳戶持有人姓名";
            // 
            // txtACCOUNT_CNAME2_5
            // 
            this.txtACCOUNT_CNAME2_5.Location = new System.Drawing.Point(758, 1837);
            this.txtACCOUNT_CNAME2_5.Name = "txtACCOUNT_CNAME2_5";
            this.txtACCOUNT_CNAME2_5.Size = new System.Drawing.Size(421, 25);
            this.txtACCOUNT_CNAME2_5.TabIndex = 211;
            // 
            // txtBRANCH_CNAME_4
            // 
            this.txtBRANCH_CNAME_4.Location = new System.Drawing.Point(446, 1422);
            this.txtBRANCH_CNAME_4.Name = "txtBRANCH_CNAME_4";
            this.txtBRANCH_CNAME_4.Size = new System.Drawing.Size(120, 25);
            this.txtBRANCH_CNAME_4.TabIndex = 154;
            // 
            // label157
            // 
            this.label157.AutoSize = true;
            this.label157.Location = new System.Drawing.Point(9, 1840);
            this.label157.Name = "label157";
            this.label157.Size = new System.Drawing.Size(106, 18);
            this.label157.TabIndex = 197;
            this.label157.Text = "帳戶持有人姓名";
            // 
            // txtACCOUNT_CNAME2_4
            // 
            this.txtACCOUNT_CNAME2_4.Location = new System.Drawing.Point(758, 1503);
            this.txtACCOUNT_CNAME2_4.Name = "txtACCOUNT_CNAME2_4";
            this.txtACCOUNT_CNAME2_4.Size = new System.Drawing.Size(421, 25);
            this.txtACCOUNT_CNAME2_4.TabIndex = 169;
            // 
            // label158
            // 
            this.label158.AutoSize = true;
            this.label158.Location = new System.Drawing.Point(623, 1898);
            this.label158.Name = "label158";
            this.label158.Size = new System.Drawing.Size(64, 18);
            this.label158.TabIndex = 212;
            this.label158.Text = "銀行帳戶";
            // 
            // label135
            // 
            this.label135.AutoSize = true;
            this.label135.Location = new System.Drawing.Point(9, 1506);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(106, 18);
            this.label135.TabIndex = 155;
            this.label135.Text = "帳戶持有人姓名";
            // 
            // txtACCOUNT_CNAME_5
            // 
            this.txtACCOUNT_CNAME_5.Location = new System.Drawing.Point(144, 1837);
            this.txtACCOUNT_CNAME_5.Name = "txtACCOUNT_CNAME_5";
            this.txtACCOUNT_CNAME_5.Size = new System.Drawing.Size(421, 25);
            this.txtACCOUNT_CNAME_5.TabIndex = 198;
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Location = new System.Drawing.Point(623, 1565);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(64, 18);
            this.label136.TabIndex = 170;
            this.label136.Text = "銀行帳戶";
            // 
            // txtACCOUNT2_5
            // 
            this.txtACCOUNT2_5.Location = new System.Drawing.Point(720, 1895);
            this.txtACCOUNT2_5.Name = "txtACCOUNT2_5";
            this.txtACCOUNT2_5.Size = new System.Drawing.Size(230, 25);
            this.txtACCOUNT2_5.TabIndex = 213;
            // 
            // txtACCOUNT_CNAME_4
            // 
            this.txtACCOUNT_CNAME_4.Location = new System.Drawing.Point(144, 1503);
            this.txtACCOUNT_CNAME_4.Name = "txtACCOUNT_CNAME_4";
            this.txtACCOUNT_CNAME_4.Size = new System.Drawing.Size(421, 25);
            this.txtACCOUNT_CNAME_4.TabIndex = 156;
            // 
            // label159
            // 
            this.label159.AutoSize = true;
            this.label159.Location = new System.Drawing.Point(9, 1898);
            this.label159.Name = "label159";
            this.label159.Size = new System.Drawing.Size(64, 18);
            this.label159.TabIndex = 199;
            this.label159.Text = "銀行帳戶";
            // 
            // txtACCOUNT2_4
            // 
            this.txtACCOUNT2_4.Location = new System.Drawing.Point(720, 1562);
            this.txtACCOUNT2_4.Name = "txtACCOUNT2_4";
            this.txtACCOUNT2_4.Size = new System.Drawing.Size(230, 25);
            this.txtACCOUNT2_4.TabIndex = 171;
            // 
            // label160
            // 
            this.label160.AutoSize = true;
            this.label160.Location = new System.Drawing.Point(961, 1898);
            this.label160.Name = "label160";
            this.label160.Size = new System.Drawing.Size(94, 18);
            this.label160.TabIndex = 214;
            this.label160.Text = "SWIFT_CODE";
            // 
            // label137
            // 
            this.label137.AutoSize = true;
            this.label137.Location = new System.Drawing.Point(9, 1565);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(64, 18);
            this.label137.TabIndex = 157;
            this.label137.Text = "銀行帳戶";
            // 
            // txtACCOUNT_5
            // 
            this.txtACCOUNT_5.Location = new System.Drawing.Point(106, 1895);
            this.txtACCOUNT_5.Name = "txtACCOUNT_5";
            this.txtACCOUNT_5.Size = new System.Drawing.Size(230, 25);
            this.txtACCOUNT_5.TabIndex = 200;
            // 
            // label138
            // 
            this.label138.AutoSize = true;
            this.label138.Location = new System.Drawing.Point(961, 1565);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(94, 18);
            this.label138.TabIndex = 172;
            this.label138.Text = "SWIFT_CODE";
            // 
            // txtSWIFT_CODE2_5
            // 
            this.txtSWIFT_CODE2_5.Location = new System.Drawing.Point(1061, 1895);
            this.txtSWIFT_CODE2_5.Name = "txtSWIFT_CODE2_5";
            this.txtSWIFT_CODE2_5.Size = new System.Drawing.Size(118, 25);
            this.txtSWIFT_CODE2_5.TabIndex = 215;
            // 
            // txtACCOUNT_4
            // 
            this.txtACCOUNT_4.Location = new System.Drawing.Point(106, 1562);
            this.txtACCOUNT_4.Name = "txtACCOUNT_4";
            this.txtACCOUNT_4.Size = new System.Drawing.Size(230, 25);
            this.txtACCOUNT_4.TabIndex = 158;
            // 
            // label161
            // 
            this.label161.AutoSize = true;
            this.label161.Location = new System.Drawing.Point(348, 1898);
            this.label161.Name = "label161";
            this.label161.Size = new System.Drawing.Size(94, 18);
            this.label161.TabIndex = 201;
            this.label161.Text = "SWIFT_CODE";
            // 
            // txtSWIFT_CODE2_4
            // 
            this.txtSWIFT_CODE2_4.Location = new System.Drawing.Point(1061, 1562);
            this.txtSWIFT_CODE2_4.Name = "txtSWIFT_CODE2_4";
            this.txtSWIFT_CODE2_4.Size = new System.Drawing.Size(118, 25);
            this.txtSWIFT_CODE2_4.TabIndex = 173;
            // 
            // label162
            // 
            this.label162.AutoSize = true;
            this.label162.Location = new System.Drawing.Point(623, 1929);
            this.label162.Name = "label162";
            this.label162.Size = new System.Drawing.Size(64, 18);
            this.label162.TabIndex = 216;
            this.label162.Text = "銀行地址";
            // 
            // label139
            // 
            this.label139.AutoSize = true;
            this.label139.Location = new System.Drawing.Point(348, 1565);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(94, 18);
            this.label139.TabIndex = 159;
            this.label139.Text = "SWIFT_CODE";
            // 
            // txtSWIFT_CODE_5
            // 
            this.txtSWIFT_CODE_5.Location = new System.Drawing.Point(446, 1895);
            this.txtSWIFT_CODE_5.Name = "txtSWIFT_CODE_5";
            this.txtSWIFT_CODE_5.Size = new System.Drawing.Size(119, 25);
            this.txtSWIFT_CODE_5.TabIndex = 202;
            // 
            // label140
            // 
            this.label140.AutoSize = true;
            this.label140.Location = new System.Drawing.Point(623, 1596);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(64, 18);
            this.label140.TabIndex = 174;
            this.label140.Text = "銀行地址";
            // 
            // txtBANK_C_ADDRESS2_5
            // 
            this.txtBANK_C_ADDRESS2_5.Location = new System.Drawing.Point(720, 1926);
            this.txtBANK_C_ADDRESS2_5.Name = "txtBANK_C_ADDRESS2_5";
            this.txtBANK_C_ADDRESS2_5.Size = new System.Drawing.Size(459, 25);
            this.txtBANK_C_ADDRESS2_5.TabIndex = 217;
            // 
            // txtSWIFT_CODE_4
            // 
            this.txtSWIFT_CODE_4.Location = new System.Drawing.Point(446, 1562);
            this.txtSWIFT_CODE_4.Name = "txtSWIFT_CODE_4";
            this.txtSWIFT_CODE_4.Size = new System.Drawing.Size(119, 25);
            this.txtSWIFT_CODE_4.TabIndex = 160;
            // 
            // label163
            // 
            this.label163.AutoSize = true;
            this.label163.Location = new System.Drawing.Point(9, 1929);
            this.label163.Name = "label163";
            this.label163.Size = new System.Drawing.Size(64, 18);
            this.label163.TabIndex = 203;
            this.label163.Text = "銀行地址";
            // 
            // txtBANK_C_ADDRESS2_4
            // 
            this.txtBANK_C_ADDRESS2_4.Location = new System.Drawing.Point(720, 1593);
            this.txtBANK_C_ADDRESS2_4.Name = "txtBANK_C_ADDRESS2_4";
            this.txtBANK_C_ADDRESS2_4.Size = new System.Drawing.Size(459, 25);
            this.txtBANK_C_ADDRESS2_4.TabIndex = 175;
            // 
            // txtBANK_C_ADDRESS_5
            // 
            this.txtBANK_C_ADDRESS_5.Location = new System.Drawing.Point(106, 1926);
            this.txtBANK_C_ADDRESS_5.Name = "txtBANK_C_ADDRESS_5";
            this.txtBANK_C_ADDRESS_5.Size = new System.Drawing.Size(459, 25);
            this.txtBANK_C_ADDRESS_5.TabIndex = 204;
            // 
            // label141
            // 
            this.label141.AutoSize = true;
            this.label141.Location = new System.Drawing.Point(9, 1596);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(64, 18);
            this.label141.TabIndex = 161;
            this.label141.Text = "銀行地址";
            // 
            // txtBANK_C_ADDRESS_4
            // 
            this.txtBANK_C_ADDRESS_4.Location = new System.Drawing.Point(106, 1593);
            this.txtBANK_C_ADDRESS_4.Name = "txtBANK_C_ADDRESS_4";
            this.txtBANK_C_ADDRESS_4.Size = new System.Drawing.Size(459, 25);
            this.txtBANK_C_ADDRESS_4.TabIndex = 162;
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Location = new System.Drawing.Point(624, 1193);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(128, 18);
            this.label119.TabIndex = 148;
            this.label119.Text = "帳戶持有人姓名(英)";
            // 
            // txtACCOUNT_ENAME2_3
            // 
            this.txtACCOUNT_ENAME2_3.Location = new System.Drawing.Point(759, 1190);
            this.txtACCOUNT_ENAME2_3.Name = "txtACCOUNT_ENAME2_3";
            this.txtACCOUNT_ENAME2_3.Size = new System.Drawing.Size(421, 25);
            this.txtACCOUNT_ENAME2_3.TabIndex = 149;
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Location = new System.Drawing.Point(10, 1193);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(128, 18);
            this.label118.TabIndex = 146;
            this.label118.Text = "帳戶持有人姓名(英)";
            // 
            // txtACCOUNT_ENAME_3
            // 
            this.txtACCOUNT_ENAME_3.Location = new System.Drawing.Point(145, 1190);
            this.txtACCOUNT_ENAME_3.Name = "txtACCOUNT_ENAME_3";
            this.txtACCOUNT_ENAME_3.Size = new System.Drawing.Size(421, 25);
            this.txtACCOUNT_ENAME_3.TabIndex = 147;
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Location = new System.Drawing.Point(623, 520);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(128, 18);
            this.label117.TabIndex = 144;
            this.label117.Text = "帳戶持有人姓名(英)";
            // 
            // txtACCOUNT_ENAME2_2
            // 
            this.txtACCOUNT_ENAME2_2.Location = new System.Drawing.Point(758, 517);
            this.txtACCOUNT_ENAME2_2.Name = "txtACCOUNT_ENAME2_2";
            this.txtACCOUNT_ENAME2_2.Size = new System.Drawing.Size(421, 25);
            this.txtACCOUNT_ENAME2_2.TabIndex = 145;
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Location = new System.Drawing.Point(9, 524);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(128, 18);
            this.label116.TabIndex = 142;
            this.label116.Text = "帳戶持有人姓名(英)";
            // 
            // txtACCOUNT_ENAME_2
            // 
            this.txtACCOUNT_ENAME_2.Location = new System.Drawing.Point(144, 521);
            this.txtACCOUNT_ENAME_2.Name = "txtACCOUNT_ENAME_2";
            this.txtACCOUNT_ENAME_2.Size = new System.Drawing.Size(421, 25);
            this.txtACCOUNT_ENAME_2.TabIndex = 143;
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Location = new System.Drawing.Point(623, 186);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(128, 18);
            this.label115.TabIndex = 140;
            this.label115.Text = "帳戶持有人姓名(英)";
            // 
            // txtACCOUNT_ENAME2
            // 
            this.txtACCOUNT_ENAME2.Location = new System.Drawing.Point(758, 183);
            this.txtACCOUNT_ENAME2.Name = "txtACCOUNT_ENAME2";
            this.txtACCOUNT_ENAME2.Size = new System.Drawing.Size(421, 25);
            this.txtACCOUNT_ENAME2.TabIndex = 141;
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Location = new System.Drawing.Point(9, 186);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(128, 18);
            this.label114.TabIndex = 138;
            this.label114.Text = "帳戶持有人姓名(英)";
            // 
            // txtACCOUNT_ENAME
            // 
            this.txtACCOUNT_ENAME.Location = new System.Drawing.Point(144, 183);
            this.txtACCOUNT_ENAME.Name = "txtACCOUNT_ENAME";
            this.txtACCOUNT_ENAME.Size = new System.Drawing.Size(421, 25);
            this.txtACCOUNT_ENAME.TabIndex = 139;
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(624, 1284);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(86, 18);
            this.label89.TabIndex = 136;
            this.label89.Text = "銀行地址(英)";
            // 
            // txtBANK_E_ADDRESS2_3
            // 
            this.txtBANK_E_ADDRESS2_3.Location = new System.Drawing.Point(721, 1281);
            this.txtBANK_E_ADDRESS2_3.Multiline = true;
            this.txtBANK_E_ADDRESS2_3.Name = "txtBANK_E_ADDRESS2_3";
            this.txtBANK_E_ADDRESS2_3.Size = new System.Drawing.Size(459, 55);
            this.txtBANK_E_ADDRESS2_3.TabIndex = 137;
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Location = new System.Drawing.Point(624, 1114);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(86, 18);
            this.label90.TabIndex = 132;
            this.label90.Text = "銀行名稱(英)";
            // 
            // txtBANK_ENAME2_3
            // 
            this.txtBANK_ENAME2_3.Location = new System.Drawing.Point(721, 1111);
            this.txtBANK_ENAME2_3.Multiline = true;
            this.txtBANK_ENAME2_3.Name = "txtBANK_ENAME2_3";
            this.txtBANK_ENAME2_3.Size = new System.Drawing.Size(230, 45);
            this.txtBANK_ENAME2_3.TabIndex = 133;
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Location = new System.Drawing.Point(962, 1114);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(86, 18);
            this.label91.TabIndex = 134;
            this.label91.Text = "分行名稱(英)";
            // 
            // txtBRANCH_ENAME2_3
            // 
            this.txtBRANCH_ENAME2_3.Location = new System.Drawing.Point(1062, 1111);
            this.txtBRANCH_ENAME2_3.Multiline = true;
            this.txtBRANCH_ENAME2_3.Name = "txtBRANCH_ENAME2_3";
            this.txtBRANCH_ENAME2_3.Size = new System.Drawing.Size(118, 45);
            this.txtBRANCH_ENAME2_3.TabIndex = 135;
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Location = new System.Drawing.Point(10, 1284);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(86, 18);
            this.label92.TabIndex = 130;
            this.label92.Text = "銀行地址(英)";
            // 
            // txtBANK_E_ADDRESS_3
            // 
            this.txtBANK_E_ADDRESS_3.Location = new System.Drawing.Point(107, 1281);
            this.txtBANK_E_ADDRESS_3.Multiline = true;
            this.txtBANK_E_ADDRESS_3.Name = "txtBANK_E_ADDRESS_3";
            this.txtBANK_E_ADDRESS_3.Size = new System.Drawing.Size(459, 55);
            this.txtBANK_E_ADDRESS_3.TabIndex = 131;
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(10, 1114);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(86, 18);
            this.label93.TabIndex = 126;
            this.label93.Text = "銀行名稱(英)";
            // 
            // txtBANK_ENAME_3
            // 
            this.txtBANK_ENAME_3.Location = new System.Drawing.Point(107, 1111);
            this.txtBANK_ENAME_3.Multiline = true;
            this.txtBANK_ENAME_3.Name = "txtBANK_ENAME_3";
            this.txtBANK_ENAME_3.Size = new System.Drawing.Size(230, 45);
            this.txtBANK_ENAME_3.TabIndex = 127;
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Location = new System.Drawing.Point(350, 1114);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(86, 18);
            this.label94.TabIndex = 128;
            this.label94.Text = "分行名稱(英)";
            // 
            // txtBRANCH_ENAME_3
            // 
            this.txtBRANCH_ENAME_3.Location = new System.Drawing.Point(447, 1111);
            this.txtBRANCH_ENAME_3.Multiline = true;
            this.txtBRANCH_ENAME_3.Name = "txtBRANCH_ENAME_3";
            this.txtBRANCH_ENAME_3.Size = new System.Drawing.Size(120, 45);
            this.txtBRANCH_ENAME_3.TabIndex = 129;
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.SystemColors.Info;
            this.panel13.Controls.Add(this.label95);
            this.panel13.Location = new System.Drawing.Point(620, 676);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(562, 58);
            this.panel13.TabIndex = 113;
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Font = new System.Drawing.Font("微軟正黑體", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label95.Location = new System.Drawing.Point(3, 12);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(365, 31);
            this.label95.TabIndex = 0;
            this.label95.Text = "銀行資訊(聯名持有人)(境內RMB)";
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(624, 1083);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(64, 18);
            this.label96.TabIndex = 114;
            this.label96.Text = "銀行名稱";
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.SystemColors.Info;
            this.panel14.Controls.Add(this.label97);
            this.panel14.Location = new System.Drawing.Point(6, 676);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(562, 58);
            this.panel14.TabIndex = 100;
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Font = new System.Drawing.Font("微軟正黑體", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label97.Location = new System.Drawing.Point(3, 12);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(229, 31);
            this.label97.TabIndex = 0;
            this.label97.Text = "銀行資訊(境內RMB)";
            // 
            // txtBANK_CNAME2_3
            // 
            this.txtBANK_CNAME2_3.Location = new System.Drawing.Point(721, 1080);
            this.txtBANK_CNAME2_3.Name = "txtBANK_CNAME2_3";
            this.txtBANK_CNAME2_3.Size = new System.Drawing.Size(230, 25);
            this.txtBANK_CNAME2_3.TabIndex = 115;
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(10, 1083);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(64, 18);
            this.label98.TabIndex = 101;
            this.label98.Text = "銀行名稱";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(962, 1083);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(64, 18);
            this.label99.TabIndex = 116;
            this.label99.Text = "分行名稱";
            // 
            // txtBANK_CNAME_3
            // 
            this.txtBANK_CNAME_3.Location = new System.Drawing.Point(107, 1080);
            this.txtBANK_CNAME_3.Name = "txtBANK_CNAME_3";
            this.txtBANK_CNAME_3.Size = new System.Drawing.Size(230, 25);
            this.txtBANK_CNAME_3.TabIndex = 102;
            // 
            // txtBRANCH_CNAME2_3
            // 
            this.txtBRANCH_CNAME2_3.Location = new System.Drawing.Point(1062, 1080);
            this.txtBRANCH_CNAME2_3.Name = "txtBRANCH_CNAME2_3";
            this.txtBRANCH_CNAME2_3.Size = new System.Drawing.Size(118, 25);
            this.txtBRANCH_CNAME2_3.TabIndex = 117;
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(350, 1083);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(64, 18);
            this.label100.TabIndex = 103;
            this.label100.Text = "分行名稱";
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(624, 1164);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(106, 18);
            this.label101.TabIndex = 118;
            this.label101.Text = "帳戶持有人姓名";
            // 
            // txtBRANCH_CNAME_3
            // 
            this.txtBRANCH_CNAME_3.Location = new System.Drawing.Point(447, 1080);
            this.txtBRANCH_CNAME_3.Name = "txtBRANCH_CNAME_3";
            this.txtBRANCH_CNAME_3.Size = new System.Drawing.Size(120, 25);
            this.txtBRANCH_CNAME_3.TabIndex = 104;
            // 
            // txtACCOUNT_CNAME2_3
            // 
            this.txtACCOUNT_CNAME2_3.Location = new System.Drawing.Point(759, 1161);
            this.txtACCOUNT_CNAME2_3.Name = "txtACCOUNT_CNAME2_3";
            this.txtACCOUNT_CNAME2_3.Size = new System.Drawing.Size(421, 25);
            this.txtACCOUNT_CNAME2_3.TabIndex = 119;
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Location = new System.Drawing.Point(10, 1164);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(106, 18);
            this.label102.TabIndex = 105;
            this.label102.Text = "帳戶持有人姓名";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(624, 1222);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(64, 18);
            this.label103.TabIndex = 120;
            this.label103.Text = "銀行帳戶";
            // 
            // txtACCOUNT_CNAME_3
            // 
            this.txtACCOUNT_CNAME_3.Location = new System.Drawing.Point(145, 1161);
            this.txtACCOUNT_CNAME_3.Name = "txtACCOUNT_CNAME_3";
            this.txtACCOUNT_CNAME_3.Size = new System.Drawing.Size(421, 25);
            this.txtACCOUNT_CNAME_3.TabIndex = 106;
            // 
            // txtACCOUNT2_3
            // 
            this.txtACCOUNT2_3.Location = new System.Drawing.Point(721, 1219);
            this.txtACCOUNT2_3.Name = "txtACCOUNT2_3";
            this.txtACCOUNT2_3.Size = new System.Drawing.Size(230, 25);
            this.txtACCOUNT2_3.TabIndex = 121;
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Location = new System.Drawing.Point(10, 1222);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(64, 18);
            this.label104.TabIndex = 107;
            this.label104.Text = "銀行帳戶";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Location = new System.Drawing.Point(962, 1222);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(94, 18);
            this.label105.TabIndex = 122;
            this.label105.Text = "SWIFT_CODE";
            // 
            // txtACCOUNT_3
            // 
            this.txtACCOUNT_3.Location = new System.Drawing.Point(107, 1219);
            this.txtACCOUNT_3.Name = "txtACCOUNT_3";
            this.txtACCOUNT_3.Size = new System.Drawing.Size(230, 25);
            this.txtACCOUNT_3.TabIndex = 108;
            // 
            // txtSWIFT_CODE2_3
            // 
            this.txtSWIFT_CODE2_3.Location = new System.Drawing.Point(1062, 1219);
            this.txtSWIFT_CODE2_3.Name = "txtSWIFT_CODE2_3";
            this.txtSWIFT_CODE2_3.Size = new System.Drawing.Size(118, 25);
            this.txtSWIFT_CODE2_3.TabIndex = 123;
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Location = new System.Drawing.Point(349, 1222);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(94, 18);
            this.label106.TabIndex = 109;
            this.label106.Text = "SWIFT_CODE";
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Location = new System.Drawing.Point(624, 1253);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(64, 18);
            this.label107.TabIndex = 124;
            this.label107.Text = "銀行地址";
            // 
            // txtSWIFT_CODE_3
            // 
            this.txtSWIFT_CODE_3.Location = new System.Drawing.Point(447, 1219);
            this.txtSWIFT_CODE_3.Name = "txtSWIFT_CODE_3";
            this.txtSWIFT_CODE_3.Size = new System.Drawing.Size(119, 25);
            this.txtSWIFT_CODE_3.TabIndex = 110;
            // 
            // txtBANK_C_ADDRESS2_3
            // 
            this.txtBANK_C_ADDRESS2_3.Location = new System.Drawing.Point(721, 1250);
            this.txtBANK_C_ADDRESS2_3.Name = "txtBANK_C_ADDRESS2_3";
            this.txtBANK_C_ADDRESS2_3.Size = new System.Drawing.Size(459, 25);
            this.txtBANK_C_ADDRESS2_3.TabIndex = 125;
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Location = new System.Drawing.Point(10, 1253);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(64, 18);
            this.label108.TabIndex = 111;
            this.label108.Text = "銀行地址";
            // 
            // txtBANK_C_ADDRESS_3
            // 
            this.txtBANK_C_ADDRESS_3.Location = new System.Drawing.Point(107, 1250);
            this.txtBANK_C_ADDRESS_3.Name = "txtBANK_C_ADDRESS_3";
            this.txtBANK_C_ADDRESS_3.Size = new System.Drawing.Size(459, 25);
            this.txtBANK_C_ADDRESS_3.TabIndex = 112;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(623, 614);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(86, 18);
            this.label69.TabIndex = 98;
            this.label69.Text = "銀行地址(英)";
            // 
            // txtBANK_E_ADDRESS2_2
            // 
            this.txtBANK_E_ADDRESS2_2.Location = new System.Drawing.Point(720, 611);
            this.txtBANK_E_ADDRESS2_2.Multiline = true;
            this.txtBANK_E_ADDRESS2_2.Name = "txtBANK_E_ADDRESS2_2";
            this.txtBANK_E_ADDRESS2_2.Size = new System.Drawing.Size(459, 55);
            this.txtBANK_E_ADDRESS2_2.TabIndex = 99;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(623, 441);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(86, 18);
            this.label70.TabIndex = 94;
            this.label70.Text = "銀行名稱(英)";
            // 
            // txtBANK_ENAME2_2
            // 
            this.txtBANK_ENAME2_2.Location = new System.Drawing.Point(720, 438);
            this.txtBANK_ENAME2_2.Multiline = true;
            this.txtBANK_ENAME2_2.Name = "txtBANK_ENAME2_2";
            this.txtBANK_ENAME2_2.Size = new System.Drawing.Size(230, 45);
            this.txtBANK_ENAME2_2.TabIndex = 95;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(961, 441);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(86, 18);
            this.label71.TabIndex = 96;
            this.label71.Text = "分行名稱(英)";
            // 
            // txtBRANCH_ENAME2_2
            // 
            this.txtBRANCH_ENAME2_2.Location = new System.Drawing.Point(1061, 438);
            this.txtBRANCH_ENAME2_2.Multiline = true;
            this.txtBRANCH_ENAME2_2.Name = "txtBRANCH_ENAME2_2";
            this.txtBRANCH_ENAME2_2.Size = new System.Drawing.Size(118, 45);
            this.txtBRANCH_ENAME2_2.TabIndex = 97;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(9, 614);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(86, 18);
            this.label72.TabIndex = 92;
            this.label72.Text = "銀行地址(英)";
            // 
            // txtBANK_E_ADDRESS_2
            // 
            this.txtBANK_E_ADDRESS_2.Location = new System.Drawing.Point(106, 611);
            this.txtBANK_E_ADDRESS_2.Multiline = true;
            this.txtBANK_E_ADDRESS_2.Name = "txtBANK_E_ADDRESS_2";
            this.txtBANK_E_ADDRESS_2.Size = new System.Drawing.Size(459, 55);
            this.txtBANK_E_ADDRESS_2.TabIndex = 93;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(9, 441);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(86, 18);
            this.label73.TabIndex = 88;
            this.label73.Text = "銀行名稱(英)";
            // 
            // txtBANK_ENAME_2
            // 
            this.txtBANK_ENAME_2.Location = new System.Drawing.Point(106, 438);
            this.txtBANK_ENAME_2.Multiline = true;
            this.txtBANK_ENAME_2.Name = "txtBANK_ENAME_2";
            this.txtBANK_ENAME_2.Size = new System.Drawing.Size(230, 45);
            this.txtBANK_ENAME_2.TabIndex = 89;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(349, 441);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(86, 18);
            this.label74.TabIndex = 90;
            this.label74.Text = "分行名稱(英)";
            // 
            // txtBRANCH_ENAME_2
            // 
            this.txtBRANCH_ENAME_2.Location = new System.Drawing.Point(446, 438);
            this.txtBRANCH_ENAME_2.Multiline = true;
            this.txtBRANCH_ENAME_2.Name = "txtBRANCH_ENAME_2";
            this.txtBRANCH_ENAME_2.Size = new System.Drawing.Size(120, 45);
            this.txtBRANCH_ENAME_2.TabIndex = 91;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.SystemColors.Info;
            this.panel11.Controls.Add(this.label75);
            this.panel11.Location = new System.Drawing.Point(620, 343);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(562, 58);
            this.panel11.TabIndex = 75;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("微軟正黑體", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label75.Location = new System.Drawing.Point(3, 12);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(315, 31);
            this.label75.TabIndex = 0;
            this.label75.Text = "銀行資訊(聯名持有人)(NTD)";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(623, 410);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(64, 18);
            this.label76.TabIndex = 76;
            this.label76.Text = "銀行名稱";
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.SystemColors.Info;
            this.panel12.Controls.Add(this.label77);
            this.panel12.Location = new System.Drawing.Point(6, 343);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(562, 58);
            this.panel12.TabIndex = 62;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("微軟正黑體", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label77.Location = new System.Drawing.Point(3, 12);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(179, 31);
            this.label77.TabIndex = 0;
            this.label77.Text = "銀行資訊(NTD)";
            // 
            // txtBANK_CNAME2_2
            // 
            this.txtBANK_CNAME2_2.Location = new System.Drawing.Point(720, 407);
            this.txtBANK_CNAME2_2.Name = "txtBANK_CNAME2_2";
            this.txtBANK_CNAME2_2.Size = new System.Drawing.Size(230, 25);
            this.txtBANK_CNAME2_2.TabIndex = 77;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(9, 410);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(64, 18);
            this.label78.TabIndex = 63;
            this.label78.Text = "銀行名稱";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(961, 410);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(64, 18);
            this.label79.TabIndex = 78;
            this.label79.Text = "分行名稱";
            // 
            // txtBANK_CNAME_2
            // 
            this.txtBANK_CNAME_2.Location = new System.Drawing.Point(106, 407);
            this.txtBANK_CNAME_2.Name = "txtBANK_CNAME_2";
            this.txtBANK_CNAME_2.Size = new System.Drawing.Size(230, 25);
            this.txtBANK_CNAME_2.TabIndex = 64;
            // 
            // txtBRANCH_CNAME2_2
            // 
            this.txtBRANCH_CNAME2_2.Location = new System.Drawing.Point(1061, 407);
            this.txtBRANCH_CNAME2_2.Name = "txtBRANCH_CNAME2_2";
            this.txtBRANCH_CNAME2_2.Size = new System.Drawing.Size(118, 25);
            this.txtBRANCH_CNAME2_2.TabIndex = 79;
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(349, 410);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(64, 18);
            this.label80.TabIndex = 65;
            this.label80.Text = "分行名稱";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(623, 493);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(106, 18);
            this.label81.TabIndex = 80;
            this.label81.Text = "帳戶持有人姓名";
            // 
            // txtBRANCH_CNAME_2
            // 
            this.txtBRANCH_CNAME_2.Location = new System.Drawing.Point(446, 407);
            this.txtBRANCH_CNAME_2.Name = "txtBRANCH_CNAME_2";
            this.txtBRANCH_CNAME_2.Size = new System.Drawing.Size(120, 25);
            this.txtBRANCH_CNAME_2.TabIndex = 66;
            // 
            // txtACCOUNT_CNAME2_2
            // 
            this.txtACCOUNT_CNAME2_2.Location = new System.Drawing.Point(758, 490);
            this.txtACCOUNT_CNAME2_2.Name = "txtACCOUNT_CNAME2_2";
            this.txtACCOUNT_CNAME2_2.Size = new System.Drawing.Size(421, 25);
            this.txtACCOUNT_CNAME2_2.TabIndex = 81;
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(9, 493);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(106, 18);
            this.label82.TabIndex = 67;
            this.label82.Text = "帳戶持有人姓名";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(623, 552);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(64, 18);
            this.label83.TabIndex = 82;
            this.label83.Text = "銀行帳戶";
            // 
            // txtACCOUNT_CNAME_2
            // 
            this.txtACCOUNT_CNAME_2.Location = new System.Drawing.Point(144, 490);
            this.txtACCOUNT_CNAME_2.Name = "txtACCOUNT_CNAME_2";
            this.txtACCOUNT_CNAME_2.Size = new System.Drawing.Size(421, 25);
            this.txtACCOUNT_CNAME_2.TabIndex = 68;
            // 
            // txtACCOUNT2_2
            // 
            this.txtACCOUNT2_2.Location = new System.Drawing.Point(720, 549);
            this.txtACCOUNT2_2.Name = "txtACCOUNT2_2";
            this.txtACCOUNT2_2.Size = new System.Drawing.Size(230, 25);
            this.txtACCOUNT2_2.TabIndex = 83;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(9, 552);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(64, 18);
            this.label84.TabIndex = 69;
            this.label84.Text = "銀行帳戶";
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(961, 552);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(94, 18);
            this.label85.TabIndex = 84;
            this.label85.Text = "SWIFT_CODE";
            // 
            // txtACCOUNT_2
            // 
            this.txtACCOUNT_2.Location = new System.Drawing.Point(106, 549);
            this.txtACCOUNT_2.Name = "txtACCOUNT_2";
            this.txtACCOUNT_2.Size = new System.Drawing.Size(230, 25);
            this.txtACCOUNT_2.TabIndex = 70;
            // 
            // txtSWIFT_CODE2_2
            // 
            this.txtSWIFT_CODE2_2.Location = new System.Drawing.Point(1061, 549);
            this.txtSWIFT_CODE2_2.Name = "txtSWIFT_CODE2_2";
            this.txtSWIFT_CODE2_2.Size = new System.Drawing.Size(118, 25);
            this.txtSWIFT_CODE2_2.TabIndex = 85;
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(348, 552);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(94, 18);
            this.label86.TabIndex = 71;
            this.label86.Text = "SWIFT_CODE";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(623, 583);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(64, 18);
            this.label87.TabIndex = 86;
            this.label87.Text = "銀行地址";
            // 
            // txtSWIFT_CODE_2
            // 
            this.txtSWIFT_CODE_2.Location = new System.Drawing.Point(446, 549);
            this.txtSWIFT_CODE_2.Name = "txtSWIFT_CODE_2";
            this.txtSWIFT_CODE_2.Size = new System.Drawing.Size(119, 25);
            this.txtSWIFT_CODE_2.TabIndex = 72;
            // 
            // txtBANK_C_ADDRESS2_2
            // 
            this.txtBANK_C_ADDRESS2_2.Location = new System.Drawing.Point(720, 580);
            this.txtBANK_C_ADDRESS2_2.Name = "txtBANK_C_ADDRESS2_2";
            this.txtBANK_C_ADDRESS2_2.Size = new System.Drawing.Size(459, 25);
            this.txtBANK_C_ADDRESS2_2.TabIndex = 87;
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(9, 583);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(64, 18);
            this.label88.TabIndex = 73;
            this.label88.Text = "銀行地址";
            // 
            // txtBANK_C_ADDRESS_2
            // 
            this.txtBANK_C_ADDRESS_2.Location = new System.Drawing.Point(106, 580);
            this.txtBANK_C_ADDRESS_2.Name = "txtBANK_C_ADDRESS_2";
            this.txtBANK_C_ADDRESS_2.Size = new System.Drawing.Size(459, 25);
            this.txtBANK_C_ADDRESS_2.TabIndex = 74;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(623, 279);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(86, 18);
            this.label68.TabIndex = 60;
            this.label68.Text = "銀行地址(英)";
            // 
            // txtBANK_E_ADDRESS2
            // 
            this.txtBANK_E_ADDRESS2.Location = new System.Drawing.Point(720, 276);
            this.txtBANK_E_ADDRESS2.Multiline = true;
            this.txtBANK_E_ADDRESS2.Name = "txtBANK_E_ADDRESS2";
            this.txtBANK_E_ADDRESS2.Size = new System.Drawing.Size(459, 55);
            this.txtBANK_E_ADDRESS2.TabIndex = 61;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(623, 104);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(86, 18);
            this.label66.TabIndex = 56;
            this.label66.Text = "銀行名稱(英)";
            // 
            // txtBANK_ENAME2
            // 
            this.txtBANK_ENAME2.Location = new System.Drawing.Point(720, 101);
            this.txtBANK_ENAME2.Multiline = true;
            this.txtBANK_ENAME2.Name = "txtBANK_ENAME2";
            this.txtBANK_ENAME2.Size = new System.Drawing.Size(230, 45);
            this.txtBANK_ENAME2.TabIndex = 57;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(961, 104);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(86, 18);
            this.label67.TabIndex = 58;
            this.label67.Text = "分行名稱(英)";
            // 
            // txtBRANCH_ENAME2
            // 
            this.txtBRANCH_ENAME2.Location = new System.Drawing.Point(1061, 101);
            this.txtBRANCH_ENAME2.Multiline = true;
            this.txtBRANCH_ENAME2.Name = "txtBRANCH_ENAME2";
            this.txtBRANCH_ENAME2.Size = new System.Drawing.Size(118, 45);
            this.txtBRANCH_ENAME2.TabIndex = 59;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(9, 279);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(86, 18);
            this.label65.TabIndex = 54;
            this.label65.Text = "銀行地址(英)";
            // 
            // txtBANK_E_ADDRESS
            // 
            this.txtBANK_E_ADDRESS.Location = new System.Drawing.Point(106, 276);
            this.txtBANK_E_ADDRESS.Multiline = true;
            this.txtBANK_E_ADDRESS.Name = "txtBANK_E_ADDRESS";
            this.txtBANK_E_ADDRESS.Size = new System.Drawing.Size(459, 55);
            this.txtBANK_E_ADDRESS.TabIndex = 55;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(9, 104);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(86, 18);
            this.label63.TabIndex = 50;
            this.label63.Text = "銀行名稱(英)";
            // 
            // txtBANK_ENAME
            // 
            this.txtBANK_ENAME.Location = new System.Drawing.Point(106, 101);
            this.txtBANK_ENAME.Multiline = true;
            this.txtBANK_ENAME.Name = "txtBANK_ENAME";
            this.txtBANK_ENAME.Size = new System.Drawing.Size(230, 45);
            this.txtBANK_ENAME.TabIndex = 51;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(349, 104);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(86, 18);
            this.label64.TabIndex = 52;
            this.label64.Text = "分行名稱(英)";
            // 
            // txtBRANCH_ENAME
            // 
            this.txtBRANCH_ENAME.Location = new System.Drawing.Point(446, 101);
            this.txtBRANCH_ENAME.Multiline = true;
            this.txtBRANCH_ENAME.Name = "txtBRANCH_ENAME";
            this.txtBRANCH_ENAME.Size = new System.Drawing.Size(120, 45);
            this.txtBRANCH_ENAME.TabIndex = 53;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.SystemColors.Info;
            this.panel10.Controls.Add(this.label56);
            this.panel10.Location = new System.Drawing.Point(620, 6);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(562, 58);
            this.panel10.TabIndex = 37;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("微軟正黑體", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label56.Location = new System.Drawing.Point(3, 12);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(313, 31);
            this.label56.TabIndex = 0;
            this.label56.Text = "銀行資訊(聯名持有人)(USD)";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(623, 73);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(64, 18);
            this.label57.TabIndex = 38;
            this.label57.Text = "銀行名稱";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Info;
            this.panel2.Controls.Add(this.label9);
            this.panel2.Location = new System.Drawing.Point(6, 6);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(562, 58);
            this.panel2.TabIndex = 11;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("微軟正黑體", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label9.Location = new System.Drawing.Point(3, 12);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(177, 31);
            this.label9.TabIndex = 0;
            this.label9.Text = "銀行資訊(USD)";
            // 
            // txtBANK_CNAME2
            // 
            this.txtBANK_CNAME2.Location = new System.Drawing.Point(720, 70);
            this.txtBANK_CNAME2.Name = "txtBANK_CNAME2";
            this.txtBANK_CNAME2.Size = new System.Drawing.Size(230, 25);
            this.txtBANK_CNAME2.TabIndex = 39;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(9, 73);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(64, 18);
            this.label11.TabIndex = 25;
            this.label11.Text = "銀行名稱";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(961, 73);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(64, 18);
            this.label58.TabIndex = 40;
            this.label58.Text = "分行名稱";
            // 
            // txtBANK_CNAME
            // 
            this.txtBANK_CNAME.Location = new System.Drawing.Point(106, 70);
            this.txtBANK_CNAME.Name = "txtBANK_CNAME";
            this.txtBANK_CNAME.Size = new System.Drawing.Size(230, 25);
            this.txtBANK_CNAME.TabIndex = 26;
            // 
            // txtBRANCH_CNAME2
            // 
            this.txtBRANCH_CNAME2.Location = new System.Drawing.Point(1061, 70);
            this.txtBRANCH_CNAME2.Name = "txtBRANCH_CNAME2";
            this.txtBRANCH_CNAME2.Size = new System.Drawing.Size(118, 25);
            this.txtBRANCH_CNAME2.TabIndex = 41;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(349, 73);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(64, 18);
            this.label10.TabIndex = 27;
            this.label10.Text = "分行名稱";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(623, 155);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(106, 18);
            this.label59.TabIndex = 42;
            this.label59.Text = "帳戶持有人姓名";
            // 
            // txtBRANCH_CNAME
            // 
            this.txtBRANCH_CNAME.Location = new System.Drawing.Point(445, 70);
            this.txtBRANCH_CNAME.Name = "txtBRANCH_CNAME";
            this.txtBRANCH_CNAME.Size = new System.Drawing.Size(120, 25);
            this.txtBRANCH_CNAME.TabIndex = 28;
            // 
            // txtACCOUNT_CNAME2
            // 
            this.txtACCOUNT_CNAME2.Location = new System.Drawing.Point(758, 152);
            this.txtACCOUNT_CNAME2.Name = "txtACCOUNT_CNAME2";
            this.txtACCOUNT_CNAME2.Size = new System.Drawing.Size(421, 25);
            this.txtACCOUNT_CNAME2.TabIndex = 43;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(9, 155);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(106, 18);
            this.label12.TabIndex = 29;
            this.label12.Text = "帳戶持有人姓名";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(623, 217);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(64, 18);
            this.label60.TabIndex = 44;
            this.label60.Text = "銀行帳戶";
            // 
            // txtACCOUNT_CNAME
            // 
            this.txtACCOUNT_CNAME.Location = new System.Drawing.Point(144, 152);
            this.txtACCOUNT_CNAME.Name = "txtACCOUNT_CNAME";
            this.txtACCOUNT_CNAME.Size = new System.Drawing.Size(421, 25);
            this.txtACCOUNT_CNAME.TabIndex = 30;
            // 
            // txtACCOUNT2
            // 
            this.txtACCOUNT2.Location = new System.Drawing.Point(720, 214);
            this.txtACCOUNT2.Name = "txtACCOUNT2";
            this.txtACCOUNT2.Size = new System.Drawing.Size(230, 25);
            this.txtACCOUNT2.TabIndex = 45;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(9, 217);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(64, 18);
            this.label14.TabIndex = 31;
            this.label14.Text = "銀行帳戶";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(961, 217);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(94, 18);
            this.label61.TabIndex = 46;
            this.label61.Text = "SWIFT_CODE";
            // 
            // txtACCOUNT
            // 
            this.txtACCOUNT.Location = new System.Drawing.Point(106, 214);
            this.txtACCOUNT.Name = "txtACCOUNT";
            this.txtACCOUNT.Size = new System.Drawing.Size(230, 25);
            this.txtACCOUNT.TabIndex = 32;
            // 
            // txtSWIFT_CODE2
            // 
            this.txtSWIFT_CODE2.Location = new System.Drawing.Point(1061, 214);
            this.txtSWIFT_CODE2.Name = "txtSWIFT_CODE2";
            this.txtSWIFT_CODE2.Size = new System.Drawing.Size(118, 25);
            this.txtSWIFT_CODE2.TabIndex = 47;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(348, 217);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(94, 18);
            this.label13.TabIndex = 33;
            this.label13.Text = "SWIFT_CODE";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(623, 248);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(64, 18);
            this.label62.TabIndex = 48;
            this.label62.Text = "銀行地址";
            // 
            // txtSWIFT_CODE
            // 
            this.txtSWIFT_CODE.Location = new System.Drawing.Point(446, 214);
            this.txtSWIFT_CODE.Name = "txtSWIFT_CODE";
            this.txtSWIFT_CODE.Size = new System.Drawing.Size(119, 25);
            this.txtSWIFT_CODE.TabIndex = 34;
            // 
            // txtBANK_C_ADDRESS2
            // 
            this.txtBANK_C_ADDRESS2.Location = new System.Drawing.Point(720, 245);
            this.txtBANK_C_ADDRESS2.Name = "txtBANK_C_ADDRESS2";
            this.txtBANK_C_ADDRESS2.Size = new System.Drawing.Size(459, 25);
            this.txtBANK_C_ADDRESS2.TabIndex = 49;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(9, 248);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(64, 18);
            this.label15.TabIndex = 35;
            this.label15.Text = "銀行地址";
            // 
            // txtBANK_C_ADDRESS
            // 
            this.txtBANK_C_ADDRESS.Location = new System.Drawing.Point(106, 245);
            this.txtBANK_C_ADDRESS.Name = "txtBANK_C_ADDRESS";
            this.txtBANK_C_ADDRESS.Size = new System.Drawing.Size(459, 25);
            this.txtBANK_C_ADDRESS.TabIndex = 36;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.txtIBCode_Internal);
            this.tabPage3.Controls.Add(this.label246);
            this.tabPage3.Controls.Add(this.btnBringSales_Internal);
            this.tabPage3.Controls.Add(this.txtSales_Intermal);
            this.tabPage3.Controls.Add(this.label247);
            this.tabPage3.Controls.Add(this.panel27);
            this.tabPage3.Controls.Add(this.panel24);
            this.tabPage3.Controls.Add(this.label219);
            this.tabPage3.Controls.Add(this.txtDiscount);
            this.tabPage3.Controls.Add(this.chk_ind_order);
            this.tabPage3.Controls.Add(this.cboIS_REMIT_TO_AUS);
            this.tabPage3.Controls.Add(this.label193);
            this.tabPage3.Controls.Add(this.chkIsMultiContract);
            this.tabPage3.Controls.Add(this.checkBox2);
            this.tabPage3.Controls.Add(this.txtInterestedRate);
            this.tabPage3.Controls.Add(this.label111);
            this.tabPage3.Controls.Add(this.checkBox1);
            this.tabPage3.Controls.Add(this.label110);
            this.tabPage3.Controls.Add(this.label109);
            this.tabPage3.Controls.Add(this.btnDelFromDate);
            this.tabPage3.Controls.Add(this.btnDelExtendDate);
            this.tabPage3.Controls.Add(this.panel9);
            this.tabPage3.Controls.Add(this.cb_close);
            this.tabPage3.Controls.Add(this.label36);
            this.tabPage3.Controls.Add(this.dteEndDate);
            this.tabPage3.Controls.Add(this.txtOrderNo);
            this.tabPage3.Controls.Add(this.txtYear);
            this.tabPage3.Controls.Add(this.label37);
            this.tabPage3.Controls.Add(this.txtParentOrderNo);
            this.tabPage3.Controls.Add(this.txtIBCode);
            this.tabPage3.Controls.Add(this.btnBringParentOrder);
            this.tabPage3.Controls.Add(this.label54);
            this.tabPage3.Controls.Add(this.btnBringSales);
            this.tabPage3.Controls.Add(this.txtSales);
            this.tabPage3.Controls.Add(this.label55);
            this.tabPage3.Controls.Add(this.txtNote);
            this.tabPage3.Controls.Add(this.label53);
            this.tabPage3.Controls.Add(this.txtMT4);
            this.tabPage3.Controls.Add(this.label52);
            this.tabPage3.Controls.Add(this.dteExtendDate);
            this.tabPage3.Controls.Add(this.label50);
            this.tabPage3.Controls.Add(this.dteOrderEnd);
            this.tabPage3.Controls.Add(this.label51);
            this.tabPage3.Controls.Add(this.label49);
            this.tabPage3.Controls.Add(this.dteFromDate);
            this.tabPage3.Controls.Add(this.label48);
            this.tabPage3.Controls.Add(this.label47);
            this.tabPage3.Location = new System.Drawing.Point(4, 26);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1294, 515);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "訂單資訊";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // txtIBCode_Internal
            // 
            this.txtIBCode_Internal.Location = new System.Drawing.Point(440, 377);
            this.txtIBCode_Internal.Name = "txtIBCode_Internal";
            this.txtIBCode_Internal.Size = new System.Drawing.Size(170, 25);
            this.txtIBCode_Internal.TabIndex = 177;
            this.txtIBCode_Internal.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSales_Intermal_KeyDown);
            // 
            // label246
            // 
            this.label246.AutoSize = true;
            this.label246.Location = new System.Drawing.Point(347, 380);
            this.label246.Name = "label246";
            this.label246.Size = new System.Drawing.Size(60, 18);
            this.label246.TabIndex = 176;
            this.label246.Text = "IB Code";
            // 
            // btnBringSales_Internal
            // 
            this.btnBringSales_Internal.Location = new System.Drawing.Point(610, 347);
            this.btnBringSales_Internal.Name = "btnBringSales_Internal";
            this.btnBringSales_Internal.Size = new System.Drawing.Size(36, 25);
            this.btnBringSales_Internal.TabIndex = 175;
            this.btnBringSales_Internal.Text = "...";
            this.btnBringSales_Internal.UseVisualStyleBackColor = true;
            this.btnBringSales_Internal.Click += new System.EventHandler(this.btnBringSales_Internal_Click);
            // 
            // txtSales_Intermal
            // 
            this.txtSales_Intermal.Enabled = false;
            this.txtSales_Intermal.Location = new System.Drawing.Point(440, 347);
            this.txtSales_Intermal.Name = "txtSales_Intermal";
            this.txtSales_Intermal.Size = new System.Drawing.Size(169, 25);
            this.txtSales_Intermal.TabIndex = 174;
            this.txtSales_Intermal.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSales_Intermal_KeyDown);
            // 
            // label247
            // 
            this.label247.AutoSize = true;
            this.label247.Location = new System.Drawing.Point(347, 350);
            this.label247.Name = "label247";
            this.label247.Size = new System.Drawing.Size(64, 18);
            this.label247.TabIndex = 173;
            this.label247.Text = "外圍業務";
            // 
            // panel27
            // 
            this.panel27.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel27.AutoScroll = true;
            this.panel27.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel27.Controls.Add(this.groupBox8);
            this.panel27.Controls.Add(this.groupBox7);
            this.panel27.Controls.Add(this.groupBox6);
            this.panel27.Controls.Add(this.groupBox5);
            this.panel27.Controls.Add(this.groupBox4);
            this.panel27.Controls.Add(this.groupBox3);
            this.panel27.Controls.Add(this.groupBox2);
            this.panel27.Location = new System.Drawing.Point(11, 114);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(1274, 147);
            this.panel27.TabIndex = 172;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.label216);
            this.groupBox8.Controls.Add(this.txtAmount_JPY);
            this.groupBox8.Controls.Add(this.label218);
            this.groupBox8.Controls.Add(this.label217);
            this.groupBox8.Controls.Add(this.txtInterest_JPY);
            this.groupBox8.Controls.Add(this.txtAmount_JPY_OLD);
            this.groupBox8.Location = new System.Drawing.Point(1055, 3);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(203, 120);
            this.groupBox8.TabIndex = 151;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "JPY";
            // 
            // label216
            // 
            this.label216.AutoSize = true;
            this.label216.Location = new System.Drawing.Point(11, 23);
            this.label216.Name = "label216";
            this.label216.Size = new System.Drawing.Size(63, 18);
            this.label216.TabIndex = 157;
            this.label216.Text = "Amount";
            // 
            // txtAmount_JPY
            // 
            this.txtAmount_JPY.Location = new System.Drawing.Point(107, 20);
            this.txtAmount_JPY.Name = "txtAmount_JPY";
            this.txtAmount_JPY.Size = new System.Drawing.Size(86, 25);
            this.txtAmount_JPY.TabIndex = 158;
            this.txtAmount_JPY.TextChanged += new System.EventHandler(this.txtAmount_JPY_TextChanged);
            // 
            // label218
            // 
            this.label218.AutoSize = true;
            this.label218.Location = new System.Drawing.Point(11, 88);
            this.label218.Name = "label218";
            this.label218.Size = new System.Drawing.Size(58, 18);
            this.label218.TabIndex = 161;
            this.label218.Text = "Interest";
            // 
            // label217
            // 
            this.label217.AutoSize = true;
            this.label217.Location = new System.Drawing.Point(11, 57);
            this.label217.Name = "label217";
            this.label217.Size = new System.Drawing.Size(91, 18);
            this.label217.TabIndex = 159;
            this.label217.Text = "Old Amount";
            // 
            // txtInterest_JPY
            // 
            this.txtInterest_JPY.Location = new System.Drawing.Point(107, 85);
            this.txtInterest_JPY.Name = "txtInterest_JPY";
            this.txtInterest_JPY.Size = new System.Drawing.Size(86, 25);
            this.txtInterest_JPY.TabIndex = 162;
            // 
            // txtAmount_JPY_OLD
            // 
            this.txtAmount_JPY_OLD.Enabled = false;
            this.txtAmount_JPY_OLD.Location = new System.Drawing.Point(107, 54);
            this.txtAmount_JPY_OLD.Name = "txtAmount_JPY_OLD";
            this.txtAmount_JPY_OLD.Size = new System.Drawing.Size(86, 25);
            this.txtAmount_JPY_OLD.TabIndex = 160;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.txtInterest_NZD);
            this.groupBox7.Controls.Add(this.txtAmount_NZD);
            this.groupBox7.Controls.Add(this.label245);
            this.groupBox7.Controls.Add(this.label243);
            this.groupBox7.Controls.Add(this.txtAmount_NZD_OLD);
            this.groupBox7.Controls.Add(this.label244);
            this.groupBox7.Location = new System.Drawing.Point(1264, 3);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(203, 120);
            this.groupBox7.TabIndex = 113;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "NZD";
            // 
            // txtInterest_NZD
            // 
            this.txtInterest_NZD.Location = new System.Drawing.Point(108, 85);
            this.txtInterest_NZD.Name = "txtInterest_NZD";
            this.txtInterest_NZD.Size = new System.Drawing.Size(86, 25);
            this.txtInterest_NZD.TabIndex = 168;
            // 
            // txtAmount_NZD
            // 
            this.txtAmount_NZD.Location = new System.Drawing.Point(108, 20);
            this.txtAmount_NZD.Name = "txtAmount_NZD";
            this.txtAmount_NZD.Size = new System.Drawing.Size(86, 25);
            this.txtAmount_NZD.TabIndex = 164;
            this.txtAmount_NZD.TextChanged += new System.EventHandler(this.txtAmount_NZD_TextChanged);
            // 
            // label245
            // 
            this.label245.AutoSize = true;
            this.label245.Location = new System.Drawing.Point(10, 88);
            this.label245.Name = "label245";
            this.label245.Size = new System.Drawing.Size(58, 18);
            this.label245.TabIndex = 167;
            this.label245.Text = "Interest";
            // 
            // label243
            // 
            this.label243.AutoSize = true;
            this.label243.Location = new System.Drawing.Point(10, 23);
            this.label243.Name = "label243";
            this.label243.Size = new System.Drawing.Size(63, 18);
            this.label243.TabIndex = 163;
            this.label243.Text = "Amount";
            // 
            // txtAmount_NZD_OLD
            // 
            this.txtAmount_NZD_OLD.Enabled = false;
            this.txtAmount_NZD_OLD.Location = new System.Drawing.Point(108, 54);
            this.txtAmount_NZD_OLD.Name = "txtAmount_NZD_OLD";
            this.txtAmount_NZD_OLD.Size = new System.Drawing.Size(86, 25);
            this.txtAmount_NZD_OLD.TabIndex = 166;
            // 
            // label244
            // 
            this.label244.AutoSize = true;
            this.label244.Location = new System.Drawing.Point(10, 57);
            this.label244.Name = "label244";
            this.label244.Size = new System.Drawing.Size(91, 18);
            this.label244.TabIndex = 165;
            this.label244.Text = "Old Amount";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.txtInterest_AUD);
            this.groupBox6.Controls.Add(this.label169);
            this.groupBox6.Controls.Add(this.txtAmount_AUD_OLD);
            this.groupBox6.Controls.Add(this.label168);
            this.groupBox6.Controls.Add(this.txtAmount_AUD);
            this.groupBox6.Controls.Add(this.label167);
            this.groupBox6.Location = new System.Drawing.Point(846, 3);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(203, 120);
            this.groupBox6.TabIndex = 114;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "AUD";
            // 
            // txtInterest_AUD
            // 
            this.txtInterest_AUD.Location = new System.Drawing.Point(107, 85);
            this.txtInterest_AUD.Name = "txtInterest_AUD";
            this.txtInterest_AUD.Size = new System.Drawing.Size(86, 25);
            this.txtInterest_AUD.TabIndex = 156;
            // 
            // label169
            // 
            this.label169.AutoSize = true;
            this.label169.Location = new System.Drawing.Point(10, 88);
            this.label169.Name = "label169";
            this.label169.Size = new System.Drawing.Size(58, 18);
            this.label169.TabIndex = 155;
            this.label169.Text = "Interest";
            // 
            // txtAmount_AUD_OLD
            // 
            this.txtAmount_AUD_OLD.Enabled = false;
            this.txtAmount_AUD_OLD.Location = new System.Drawing.Point(107, 54);
            this.txtAmount_AUD_OLD.Name = "txtAmount_AUD_OLD";
            this.txtAmount_AUD_OLD.Size = new System.Drawing.Size(86, 25);
            this.txtAmount_AUD_OLD.TabIndex = 154;
            // 
            // label168
            // 
            this.label168.AutoSize = true;
            this.label168.Location = new System.Drawing.Point(10, 57);
            this.label168.Name = "label168";
            this.label168.Size = new System.Drawing.Size(91, 18);
            this.label168.TabIndex = 153;
            this.label168.Text = "Old Amount";
            // 
            // txtAmount_AUD
            // 
            this.txtAmount_AUD.Location = new System.Drawing.Point(107, 20);
            this.txtAmount_AUD.Name = "txtAmount_AUD";
            this.txtAmount_AUD.Size = new System.Drawing.Size(86, 25);
            this.txtAmount_AUD.TabIndex = 152;
            this.txtAmount_AUD.TextChanged += new System.EventHandler(this.txtAmount_AUD_TextChanged);
            // 
            // label167
            // 
            this.label167.AutoSize = true;
            this.label167.Location = new System.Drawing.Point(10, 23);
            this.label167.Name = "label167";
            this.label167.Size = new System.Drawing.Size(63, 18);
            this.label167.TabIndex = 151;
            this.label167.Text = "Amount";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.txtAmount_EUR);
            this.groupBox5.Controls.Add(this.label166);
            this.groupBox5.Controls.Add(this.txtInterest_EUR);
            this.groupBox5.Controls.Add(this.txtAmount_EUR_OLD);
            this.groupBox5.Controls.Add(this.label165);
            this.groupBox5.Controls.Add(this.label164);
            this.groupBox5.Location = new System.Drawing.Point(637, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(203, 120);
            this.groupBox5.TabIndex = 113;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "EUR";
            // 
            // txtAmount_EUR
            // 
            this.txtAmount_EUR.Location = new System.Drawing.Point(106, 20);
            this.txtAmount_EUR.Name = "txtAmount_EUR";
            this.txtAmount_EUR.Size = new System.Drawing.Size(86, 25);
            this.txtAmount_EUR.TabIndex = 146;
            this.txtAmount_EUR.TextChanged += new System.EventHandler(this.txtAmount_EUR_TextChanged);
            // 
            // label166
            // 
            this.label166.AutoSize = true;
            this.label166.Location = new System.Drawing.Point(10, 88);
            this.label166.Name = "label166";
            this.label166.Size = new System.Drawing.Size(58, 18);
            this.label166.TabIndex = 149;
            this.label166.Text = "Interest";
            // 
            // txtInterest_EUR
            // 
            this.txtInterest_EUR.Location = new System.Drawing.Point(106, 85);
            this.txtInterest_EUR.Name = "txtInterest_EUR";
            this.txtInterest_EUR.Size = new System.Drawing.Size(86, 25);
            this.txtInterest_EUR.TabIndex = 150;
            // 
            // txtAmount_EUR_OLD
            // 
            this.txtAmount_EUR_OLD.Enabled = false;
            this.txtAmount_EUR_OLD.Location = new System.Drawing.Point(106, 54);
            this.txtAmount_EUR_OLD.Name = "txtAmount_EUR_OLD";
            this.txtAmount_EUR_OLD.Size = new System.Drawing.Size(86, 25);
            this.txtAmount_EUR_OLD.TabIndex = 148;
            // 
            // label165
            // 
            this.label165.AutoSize = true;
            this.label165.Location = new System.Drawing.Point(10, 57);
            this.label165.Name = "label165";
            this.label165.Size = new System.Drawing.Size(91, 18);
            this.label165.TabIndex = 147;
            this.label165.Text = "Old Amount";
            // 
            // label164
            // 
            this.label164.AutoSize = true;
            this.label164.Location = new System.Drawing.Point(10, 23);
            this.label164.Name = "label164";
            this.label164.Size = new System.Drawing.Size(63, 18);
            this.label164.TabIndex = 145;
            this.label164.Text = "Amount";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtAmount_RMB);
            this.groupBox4.Controls.Add(this.label43);
            this.groupBox4.Controls.Add(this.label42);
            this.groupBox4.Controls.Add(this.txtAmount_RMB_OLD);
            this.groupBox4.Controls.Add(this.txtInterest_RMB);
            this.groupBox4.Controls.Add(this.label46);
            this.groupBox4.Location = new System.Drawing.Point(428, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(203, 120);
            this.groupBox4.TabIndex = 112;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "RMB";
            // 
            // txtAmount_RMB
            // 
            this.txtAmount_RMB.Location = new System.Drawing.Point(105, 20);
            this.txtAmount_RMB.Name = "txtAmount_RMB";
            this.txtAmount_RMB.Size = new System.Drawing.Size(86, 25);
            this.txtAmount_RMB.TabIndex = 106;
            this.txtAmount_RMB.TextChanged += new System.EventHandler(this.txtAmount_RMB_TextChanged);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(12, 23);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(63, 18);
            this.label43.TabIndex = 105;
            this.label43.Text = "Amount";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(12, 57);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(91, 18);
            this.label42.TabIndex = 107;
            this.label42.Text = "Old Amount";
            // 
            // txtAmount_RMB_OLD
            // 
            this.txtAmount_RMB_OLD.Enabled = false;
            this.txtAmount_RMB_OLD.Location = new System.Drawing.Point(105, 54);
            this.txtAmount_RMB_OLD.Name = "txtAmount_RMB_OLD";
            this.txtAmount_RMB_OLD.Size = new System.Drawing.Size(86, 25);
            this.txtAmount_RMB_OLD.TabIndex = 108;
            // 
            // txtInterest_RMB
            // 
            this.txtInterest_RMB.Location = new System.Drawing.Point(105, 85);
            this.txtInterest_RMB.Name = "txtInterest_RMB";
            this.txtInterest_RMB.Size = new System.Drawing.Size(86, 25);
            this.txtInterest_RMB.TabIndex = 114;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(12, 88);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(58, 18);
            this.label46.TabIndex = 113;
            this.label46.Text = "Interest";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtAmount_NTD);
            this.groupBox3.Controls.Add(this.label41);
            this.groupBox3.Controls.Add(this.label40);
            this.groupBox3.Controls.Add(this.txtAmount_NTD_OLD);
            this.groupBox3.Controls.Add(this.label45);
            this.groupBox3.Controls.Add(this.txtInterest_NTD);
            this.groupBox3.Location = new System.Drawing.Point(218, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(203, 120);
            this.groupBox3.TabIndex = 111;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "NTD";
            // 
            // txtAmount_NTD
            // 
            this.txtAmount_NTD.Location = new System.Drawing.Point(110, 20);
            this.txtAmount_NTD.Name = "txtAmount_NTD";
            this.txtAmount_NTD.Size = new System.Drawing.Size(86, 25);
            this.txtAmount_NTD.TabIndex = 102;
            this.txtAmount_NTD.TextChanged += new System.EventHandler(this.txtAmount_NTD_TextChanged);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(12, 23);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(63, 18);
            this.label41.TabIndex = 101;
            this.label41.Text = "Amount";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(12, 57);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(91, 18);
            this.label40.TabIndex = 103;
            this.label40.Text = "Old Amount";
            // 
            // txtAmount_NTD_OLD
            // 
            this.txtAmount_NTD_OLD.Enabled = false;
            this.txtAmount_NTD_OLD.Location = new System.Drawing.Point(110, 54);
            this.txtAmount_NTD_OLD.Name = "txtAmount_NTD_OLD";
            this.txtAmount_NTD_OLD.Size = new System.Drawing.Size(86, 25);
            this.txtAmount_NTD_OLD.TabIndex = 104;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(12, 91);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(58, 18);
            this.label45.TabIndex = 111;
            this.label45.Text = "Interest";
            // 
            // txtInterest_NTD
            // 
            this.txtInterest_NTD.Location = new System.Drawing.Point(110, 88);
            this.txtInterest_NTD.Name = "txtInterest_NTD";
            this.txtInterest_NTD.Size = new System.Drawing.Size(86, 25);
            this.txtInterest_NTD.TabIndex = 112;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtAmount_USD);
            this.groupBox2.Controls.Add(this.label39);
            this.groupBox2.Controls.Add(this.txtAmount_USD_OLD);
            this.groupBox2.Controls.Add(this.label44);
            this.groupBox2.Controls.Add(this.txtInterest_USD);
            this.groupBox2.Controls.Add(this.label38);
            this.groupBox2.Location = new System.Drawing.Point(9, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(203, 120);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "USD";
            // 
            // txtAmount_USD
            // 
            this.txtAmount_USD.Location = new System.Drawing.Point(109, 20);
            this.txtAmount_USD.Name = "txtAmount_USD";
            this.txtAmount_USD.Size = new System.Drawing.Size(86, 25);
            this.txtAmount_USD.TabIndex = 98;
            this.txtAmount_USD.TextChanged += new System.EventHandler(this.txtAmount_USD_TextChanged);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(13, 57);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(91, 18);
            this.label39.TabIndex = 99;
            this.label39.Text = "Old Amount";
            // 
            // txtAmount_USD_OLD
            // 
            this.txtAmount_USD_OLD.Enabled = false;
            this.txtAmount_USD_OLD.Location = new System.Drawing.Point(109, 54);
            this.txtAmount_USD_OLD.Name = "txtAmount_USD_OLD";
            this.txtAmount_USD_OLD.Size = new System.Drawing.Size(86, 25);
            this.txtAmount_USD_OLD.TabIndex = 100;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(13, 91);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(58, 18);
            this.label44.TabIndex = 109;
            this.label44.Text = "Interest";
            // 
            // txtInterest_USD
            // 
            this.txtInterest_USD.Location = new System.Drawing.Point(109, 88);
            this.txtInterest_USD.Name = "txtInterest_USD";
            this.txtInterest_USD.Size = new System.Drawing.Size(86, 25);
            this.txtInterest_USD.TabIndex = 110;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(13, 23);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(63, 18);
            this.label38.TabIndex = 97;
            this.label38.Text = "Amount";
            // 
            // panel24
            // 
            this.panel24.BackColor = System.Drawing.Color.Moccasin;
            this.panel24.Controls.Add(this.lbl2020Condition);
            this.panel24.Controls.Add(this.label221);
            this.panel24.Controls.Add(this.rdoBonusA);
            this.panel24.Controls.Add(this.rdoBonusB);
            this.panel24.Location = new System.Drawing.Point(300, 417);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(291, 88);
            this.panel24.TabIndex = 171;
            // 
            // lbl2020Condition
            // 
            this.lbl2020Condition.AutoSize = true;
            this.lbl2020Condition.Font = new System.Drawing.Font("微軟正黑體", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lbl2020Condition.ForeColor = System.Drawing.Color.Red;
            this.lbl2020Condition.Location = new System.Drawing.Point(223, 14);
            this.lbl2020Condition.Name = "lbl2020Condition";
            this.lbl2020Condition.Size = new System.Drawing.Size(48, 24);
            this.lbl2020Condition.TabIndex = 175;
            this.lbl2020Condition.Text = "符合";
            // 
            // label221
            // 
            this.label221.AutoSize = true;
            this.label221.Font = new System.Drawing.Font("微軟正黑體", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label221.Location = new System.Drawing.Point(17, 14);
            this.label221.Name = "label221";
            this.label221.Size = new System.Drawing.Size(194, 18);
            this.label221.TabIndex = 174;
            this.label221.Text = "金鼠年活動 BONUS_OPTION";
            // 
            // rdoBonusA
            // 
            this.rdoBonusA.AutoSize = true;
            this.rdoBonusA.Location = new System.Drawing.Point(7, 47);
            this.rdoBonusA.Name = "rdoBonusA";
            this.rdoBonusA.Size = new System.Drawing.Size(132, 22);
            this.rdoBonusA.TabIndex = 173;
            this.rdoBonusA.TabStop = true;
            this.rdoBonusA.Text = "方案A (固定獎金)";
            this.rdoBonusA.UseVisualStyleBackColor = true;
            // 
            // rdoBonusB
            // 
            this.rdoBonusB.AutoSize = true;
            this.rdoBonusB.Location = new System.Drawing.Point(140, 47);
            this.rdoBonusB.Name = "rdoBonusB";
            this.rdoBonusB.Size = new System.Drawing.Size(141, 22);
            this.rdoBonusB.TabIndex = 172;
            this.rdoBonusB.TabStop = true;
            this.rdoBonusB.Text = "方案B(報酬百分比)";
            this.rdoBonusB.UseVisualStyleBackColor = true;
            // 
            // label219
            // 
            this.label219.AutoSize = true;
            this.label219.Location = new System.Drawing.Point(922, 72);
            this.label219.Name = "label219";
            this.label219.Size = new System.Drawing.Size(129, 18);
            this.label219.TabIndex = 164;
            this.label219.Text = "Discount_Amount";
            // 
            // txtDiscount
            // 
            this.txtDiscount.Location = new System.Drawing.Point(1054, 67);
            this.txtDiscount.Name = "txtDiscount";
            this.txtDiscount.ReadOnly = true;
            this.txtDiscount.Size = new System.Drawing.Size(108, 25);
            this.txtDiscount.TabIndex = 165;
            // 
            // chk_ind_order
            // 
            this.chk_ind_order.AutoSize = true;
            this.chk_ind_order.BackColor = System.Drawing.Color.Salmon;
            this.chk_ind_order.Location = new System.Drawing.Point(793, 70);
            this.chk_ind_order.Name = "chk_ind_order";
            this.chk_ind_order.Size = new System.Drawing.Size(81, 22);
            this.chk_ind_order.TabIndex = 163;
            this.chk_ind_order.Text = "IND合約";
            this.chk_ind_order.UseVisualStyleBackColor = false;
            this.chk_ind_order.CheckedChanged += new System.EventHandler(this.chk_ind_order_CheckedChanged);
            // 
            // cboIS_REMIT_TO_AUS
            // 
            this.cboIS_REMIT_TO_AUS.FormattingEnabled = true;
            this.cboIS_REMIT_TO_AUS.Items.AddRange(new object[] {
            "",
            "新合約匯入澳洲帳戶",
            "現有合約自澳洲銀行贖回(收2%)"});
            this.cboIS_REMIT_TO_AUS.Location = new System.Drawing.Point(702, 427);
            this.cboIS_REMIT_TO_AUS.Name = "cboIS_REMIT_TO_AUS";
            this.cboIS_REMIT_TO_AUS.Size = new System.Drawing.Size(229, 25);
            this.cboIS_REMIT_TO_AUS.TabIndex = 162;
            // 
            // label193
            // 
            this.label193.AutoSize = true;
            this.label193.Location = new System.Drawing.Point(597, 432);
            this.label193.Name = "label193";
            this.label193.Size = new System.Drawing.Size(92, 18);
            this.label193.TabIndex = 161;
            this.label193.Text = "澳洲帳戶類型";
            // 
            // chkIsMultiContract
            // 
            this.chkIsMultiContract.AutoSize = true;
            this.chkIsMultiContract.Location = new System.Drawing.Point(270, 91);
            this.chkIsMultiContract.Name = "chkIsMultiContract";
            this.chkIsMultiContract.Size = new System.Drawing.Size(125, 22);
            this.chkIsMultiContract.TabIndex = 159;
            this.chkIsMultiContract.Text = "是否為多筆合約";
            this.chkIsMultiContract.UseVisualStyleBackColor = true;
            this.chkIsMultiContract.Visible = false;
            this.chkIsMultiContract.CheckedChanged += new System.EventHandler(this.chkIsMultiContract_CheckedChanged);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(146, 443);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(97, 22);
            this.checkBox2.TabIndex = 158;
            this.checkBox2.Text = "境內人民幣";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.Visible = false;
            // 
            // txtInterestedRate
            // 
            this.txtInterestedRate.Location = new System.Drawing.Point(634, 67);
            this.txtInterestedRate.Name = "txtInterestedRate";
            this.txtInterestedRate.Size = new System.Drawing.Size(97, 25);
            this.txtInterestedRate.TabIndex = 144;
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(19, 416);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(106, 18);
            this.label111.TabIndex = 143;
            this.label111.Text = "是否有行銷活動";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(146, 414);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(115, 22);
            this.checkBox1.TabIndex = 142;
            this.checkBox1.Text = "2019金豬報喜";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Location = new System.Drawing.Point(740, 70);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(24, 18);
            this.label110.TabIndex = 141;
            this.label110.Text = " %";
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Location = new System.Drawing.Point(548, 70);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(36, 18);
            this.label109.TabIndex = 140;
            this.label109.Text = "利率";
            // 
            // btnDelFromDate
            // 
            this.btnDelFromDate.Image = ((System.Drawing.Image)(resources.GetObject("btnDelFromDate.Image")));
            this.btnDelFromDate.Location = new System.Drawing.Point(568, 264);
            this.btnDelFromDate.Name = "btnDelFromDate";
            this.btnDelFromDate.Size = new System.Drawing.Size(31, 28);
            this.btnDelFromDate.TabIndex = 139;
            this.btnDelFromDate.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.btnDelFromDate.UseVisualStyleBackColor = true;
            this.btnDelFromDate.Click += new System.EventHandler(this.btnDelFromDate_Click);
            // 
            // btnDelExtendDate
            // 
            this.btnDelExtendDate.Image = ((System.Drawing.Image)(resources.GetObject("btnDelExtendDate.Image")));
            this.btnDelExtendDate.Location = new System.Drawing.Point(258, 305);
            this.btnDelExtendDate.Name = "btnDelExtendDate";
            this.btnDelExtendDate.Size = new System.Drawing.Size(31, 28);
            this.btnDelExtendDate.TabIndex = 138;
            this.btnDelExtendDate.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.btnDelExtendDate.UseVisualStyleBackColor = true;
            this.btnDelExtendDate.Click += new System.EventHandler(this.btnDelExtendDate_Click);
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.SystemColors.Info;
            this.panel9.Controls.Add(this.label35);
            this.panel9.Location = new System.Drawing.Point(3, 3);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(1282, 58);
            this.panel9.TabIndex = 12;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("微軟正黑體", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label35.Location = new System.Drawing.Point(3, 12);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(110, 31);
            this.label35.TabIndex = 0;
            this.label35.Text = "訂單資訊";
            // 
            // cb_close
            // 
            this.cb_close.AutoSize = true;
            this.cb_close.Enabled = false;
            this.cb_close.Location = new System.Drawing.Point(578, 311);
            this.cb_close.Name = "cb_close";
            this.cb_close.Size = new System.Drawing.Size(83, 22);
            this.cb_close.TabIndex = 137;
            this.cb_close.Text = "中止合約";
            this.cb_close.UseVisualStyleBackColor = true;
            this.cb_close.CheckedChanged += new System.EventHandler(this.cb_close_CheckedChanged);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(8, 70);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(70, 18);
            this.label36.TabIndex = 92;
            this.label36.Text = "Order No";
            // 
            // dteEndDate
            // 
            this.dteEndDate.AutoSize = true;
            this.dteEndDate.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.dteEndDate.Location = new System.Drawing.Point(707, 264);
            this.dteEndDate.Name = "dteEndDate";
            this.dteEndDate.Size = new System.Drawing.Size(0, 20);
            this.dteEndDate.TabIndex = 136;
            // 
            // txtOrderNo
            // 
            this.txtOrderNo.Location = new System.Drawing.Point(101, 67);
            this.txtOrderNo.Name = "txtOrderNo";
            this.txtOrderNo.Size = new System.Drawing.Size(151, 25);
            this.txtOrderNo.TabIndex = 93;
            // 
            // txtYear
            // 
            this.txtYear.FormattingEnabled = true;
            this.txtYear.Items.AddRange(new object[] {
            "",
            "0.5",
            "1",
            "2",
            "3"});
            this.txtYear.Location = new System.Drawing.Point(111, 269);
            this.txtYear.Name = "txtYear";
            this.txtYear.Size = new System.Drawing.Size(152, 25);
            this.txtYear.TabIndex = 135;
            this.txtYear.SelectedIndexChanged += new System.EventHandler(this.txtYear_SelectedIndexChanged);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(265, 70);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(117, 18);
            this.label37.TabIndex = 94;
            this.label37.Text = "Parent Order No";
            // 
            // txtParentOrderNo
            // 
            this.txtParentOrderNo.Location = new System.Drawing.Point(401, 67);
            this.txtParentOrderNo.Name = "txtParentOrderNo";
            this.txtParentOrderNo.Size = new System.Drawing.Size(84, 25);
            this.txtParentOrderNo.TabIndex = 95;
            this.txtParentOrderNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtParentOrderNo_KeyDown);
            // 
            // txtIBCode
            // 
            this.txtIBCode.Location = new System.Drawing.Point(111, 377);
            this.txtIBCode.Name = "txtIBCode";
            this.txtIBCode.Size = new System.Drawing.Size(170, 25);
            this.txtIBCode.TabIndex = 133;
            this.txtIBCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSales_KeyDown);
            // 
            // btnBringParentOrder
            // 
            this.btnBringParentOrder.Location = new System.Drawing.Point(491, 67);
            this.btnBringParentOrder.Name = "btnBringParentOrder";
            this.btnBringParentOrder.Size = new System.Drawing.Size(36, 25);
            this.btnBringParentOrder.TabIndex = 96;
            this.btnBringParentOrder.Text = "...";
            this.btnBringParentOrder.UseVisualStyleBackColor = true;
            this.btnBringParentOrder.Click += new System.EventHandler(this.btnBringParentOrder_Click);
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(18, 380);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(60, 18);
            this.label54.TabIndex = 132;
            this.label54.Text = "IB Code";
            // 
            // btnBringSales
            // 
            this.btnBringSales.Location = new System.Drawing.Point(281, 347);
            this.btnBringSales.Name = "btnBringSales";
            this.btnBringSales.Size = new System.Drawing.Size(36, 25);
            this.btnBringSales.TabIndex = 131;
            this.btnBringSales.Text = "...";
            this.btnBringSales.UseVisualStyleBackColor = true;
            this.btnBringSales.Click += new System.EventHandler(this.btnBringSales_Click);
            // 
            // txtSales
            // 
            this.txtSales.Enabled = false;
            this.txtSales.Location = new System.Drawing.Point(111, 347);
            this.txtSales.Name = "txtSales";
            this.txtSales.Size = new System.Drawing.Size(169, 25);
            this.txtSales.TabIndex = 130;
            this.txtSales.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSales_KeyDown);
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(18, 350);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(36, 18);
            this.label55.TabIndex = 129;
            this.label55.Text = "業務";
            // 
            // txtNote
            // 
            this.txtNote.Location = new System.Drawing.Point(877, 308);
            this.txtNote.Multiline = true;
            this.txtNote.Name = "txtNote";
            this.txtNote.Size = new System.Drawing.Size(329, 94);
            this.txtNote.TabIndex = 128;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(804, 312);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(41, 18);
            this.label53.TabIndex = 127;
            this.label53.Text = "Note";
            // 
            // txtMT4
            // 
            this.txtMT4.Location = new System.Drawing.Point(1031, 267);
            this.txtMT4.Name = "txtMT4";
            this.txtMT4.Size = new System.Drawing.Size(175, 25);
            this.txtMT4.TabIndex = 126;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(893, 273);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(38, 18);
            this.label52.TabIndex = 125;
            this.label52.Text = "MT4";
            // 
            // dteExtendDate
            // 
            this.dteExtendDate.Location = new System.Drawing.Point(111, 306);
            this.dteExtendDate.Name = "dteExtendDate";
            this.dteExtendDate.Size = new System.Drawing.Size(146, 25);
            this.dteExtendDate.TabIndex = 124;
            this.dteExtendDate.ValueChanged += new System.EventHandler(this.dte_ValueChanged);
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(18, 311);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(90, 18);
            this.label50.TabIndex = 123;
            this.label50.Text = "Extend Date";
            // 
            // dteOrderEnd
            // 
            this.dteOrderEnd.Location = new System.Drawing.Point(412, 306);
            this.dteOrderEnd.Name = "dteOrderEnd";
            this.dteOrderEnd.Size = new System.Drawing.Size(159, 25);
            this.dteOrderEnd.TabIndex = 122;
            this.dteOrderEnd.ValueChanged += new System.EventHandler(this.dte_ValueChanged);
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(305, 311);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(76, 18);
            this.label51.TabIndex = 121;
            this.label51.Text = "Order End";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(18, 273);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(37, 18);
            this.label49.TabIndex = 119;
            this.label49.Text = "Year";
            // 
            // dteFromDate
            // 
            this.dteFromDate.Location = new System.Drawing.Point(412, 266);
            this.dteFromDate.Name = "dteFromDate";
            this.dteFromDate.Size = new System.Drawing.Size(150, 25);
            this.dteFromDate.TabIndex = 118;
            this.dteFromDate.ValueChanged += new System.EventHandler(this.dteFromDate_ValueChanged);
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(303, 270);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(78, 18);
            this.label48.TabIndex = 117;
            this.label48.Text = "From Date";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(621, 270);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(70, 18);
            this.label47.TabIndex = 115;
            this.label47.Text = "End Date";
            // 
            // btnComfirmModify
            // 
            this.btnComfirmModify.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnComfirmModify.Location = new System.Drawing.Point(736, 565);
            this.btnComfirmModify.Name = "btnComfirmModify";
            this.btnComfirmModify.Size = new System.Drawing.Size(181, 48);
            this.btnComfirmModify.TabIndex = 134;
            this.btnComfirmModify.Text = "確定修改";
            this.btnComfirmModify.UseVisualStyleBackColor = true;
            this.btnComfirmModify.Visible = false;
            this.btnComfirmModify.Click += new System.EventHandler(this.btnComfirmModify_Click);
            // 
            // btnSubmit
            // 
            this.btnSubmit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSubmit.Location = new System.Drawing.Point(934, 565);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(181, 48);
            this.btnSubmit.TabIndex = 7;
            this.btnSubmit.Text = "確定新增合約";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(1132, 565);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(181, 48);
            this.btnClose.TabIndex = 6;
            this.btnClose.Text = "關閉";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label248
            // 
            this.label248.AutoSize = true;
            this.label248.Location = new System.Drawing.Point(6, 168);
            this.label248.Name = "label248";
            this.label248.Size = new System.Drawing.Size(120, 18);
            this.label248.TabIndex = 94;
            this.label248.Text = "檢核通過的方式：";
            // 
            // lblIS_ONLINE_WITHDRAWAL_CHECK
            // 
            this.lblIS_ONLINE_WITHDRAWAL_CHECK.AutoSize = true;
            this.lblIS_ONLINE_WITHDRAWAL_CHECK.Location = new System.Drawing.Point(132, 168);
            this.lblIS_ONLINE_WITHDRAWAL_CHECK.Name = "lblIS_ONLINE_WITHDRAWAL_CHECK";
            this.lblIS_ONLINE_WITHDRAWAL_CHECK.Size = new System.Drawing.Size(0, 18);
            this.lblIS_ONLINE_WITHDRAWAL_CHECK.TabIndex = 95;
            // 
            // frmAddContract
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.ClientSize = new System.Drawing.Size(1323, 618);
            this.Controls.Add(this.btnModifyHist);
            this.Controls.Add(this.btnModifyALL);
            this.Controls.Add(this.btnTransOrder);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.btnComfirmModify);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.btnClose);
            this.Font = new System.Drawing.Font("微軟正黑體", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.MinimizeBox = false;
            this.Name = "frmAddContract";
            this.Text = "新增合約";
            this.Load += new System.EventHandler(this.frmAddContract_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.panContractInfo.ResumeLayout(false);
            this.panContractInfo.PerformLayout();
            this.panel21.ResumeLayout(false);
            this.panel21.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.panel25.ResumeLayout(false);
            this.panel25.PerformLayout();
            this.panel26.ResumeLayout(false);
            this.panel26.PerformLayout();
            this.panel22.ResumeLayout(false);
            this.panel22.PerformLayout();
            this.panel23.ResumeLayout(false);
            this.panel23.PerformLayout();
            this.panel19.ResumeLayout(false);
            this.panel19.PerformLayout();
            this.panel20.ResumeLayout(false);
            this.panel20.PerformLayout();
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.panel27.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel24.ResumeLayout(false);
            this.panel24.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.TextBox txtCUST_CNAME;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rdoWoman;
        private System.Windows.Forms.RadioButton rdoMan;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dteBirthday;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtPASSPORT;
        private System.Windows.Forms.TextBox txtNATION;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtPHONE;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtEMAIL;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtBRANCH_CNAME;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtBANK_CNAME;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtACCOUNT_CNAME;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtSWIFT_CODE;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtACCOUNT;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtBANK_C_ADDRESS;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtID_NUMBER;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtCUST_ENAME;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtCUST_ENAME2;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtID_NUMBER2;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RadioButton rdoWoman2;
        private System.Windows.Forms.RadioButton rdoMan2;
        private System.Windows.Forms.TextBox txtAddress2;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtEMAIL2;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtPHONE2;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtNATION2;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtPASSPORT2;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.DateTimePicker dteBirthday2;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox txtCUST_CNAME2;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.RadioButton chkMAIL_TYPE_MONTHLY1;
        private System.Windows.Forms.RadioButton chkMAIL_TYPE_MONTHLY2;
        private System.Windows.Forms.RadioButton chkMAIL_TYPE_END2;
        private System.Windows.Forms.RadioButton chkMAIL_TYPE_END1;
        private System.Windows.Forms.RadioButton chkMAIL_TYPE_END3;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox txtMAIL_TYPE_END;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox txtOrderNo;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox txtParentOrderNo;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Button btnBringParentOrder;
        private System.Windows.Forms.TextBox txtAmount_USD;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox txtAmount_USD_OLD;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox txtAmount_NTD_OLD;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox txtAmount_NTD;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox txtAmount_RMB_OLD;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox txtAmount_RMB;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox txtInterest_USD;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox txtInterest_NTD;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox txtInterest_RMB;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.DateTimePicker dteFromDate;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.DateTimePicker dteExtendDate;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.DateTimePicker dteOrderEnd;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.TextBox txtMT4;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.TextBox txtNote;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TextBox txtIBCode;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Button btnBringSales;
        private System.Windows.Forms.TextBox txtSales;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Button btnComfirmModify;
        private System.Windows.Forms.ComboBox txtYear;
        private System.Windows.Forms.Label dteEndDate;
        private System.Windows.Forms.CheckBox cb_close;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.TextBox txtBANK_CNAME2;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.TextBox txtBRANCH_CNAME2;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.TextBox txtACCOUNT_CNAME2;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.TextBox txtACCOUNT2;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.TextBox txtSWIFT_CODE2;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.TextBox txtBANK_C_ADDRESS2;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.TextBox txtBANK_ENAME;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.TextBox txtBRANCH_ENAME;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.TextBox txtBANK_E_ADDRESS;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.TextBox txtBANK_ENAME2;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.TextBox txtBRANCH_ENAME2;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.TextBox txtBANK_E_ADDRESS2;
        private System.Windows.Forms.Panel panContractInfo;
        private System.Windows.Forms.Button btnDelExtendDate;
        private System.Windows.Forms.Button btnDelFromDate;
        private System.Windows.Forms.Button btnDelBirthday;
        private System.Windows.Forms.Button btnDelBirthday2;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.TextBox txtBANK_E_ADDRESS2_2;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.TextBox txtBANK_ENAME2_2;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.TextBox txtBRANCH_ENAME2_2;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.TextBox txtBANK_E_ADDRESS_2;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.TextBox txtBANK_ENAME_2;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.TextBox txtBRANCH_ENAME_2;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.TextBox txtBANK_CNAME2_2;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.TextBox txtBANK_CNAME_2;
        private System.Windows.Forms.TextBox txtBRANCH_CNAME2_2;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.TextBox txtBRANCH_CNAME_2;
        private System.Windows.Forms.TextBox txtACCOUNT_CNAME2_2;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.TextBox txtACCOUNT_CNAME_2;
        private System.Windows.Forms.TextBox txtACCOUNT2_2;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.TextBox txtACCOUNT_2;
        private System.Windows.Forms.TextBox txtSWIFT_CODE2_2;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.TextBox txtSWIFT_CODE_2;
        private System.Windows.Forms.TextBox txtBANK_C_ADDRESS2_2;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.TextBox txtBANK_C_ADDRESS_2;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.TextBox txtBANK_E_ADDRESS2_3;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.TextBox txtBANK_ENAME2_3;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.TextBox txtBRANCH_ENAME2_3;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.TextBox txtBANK_E_ADDRESS_3;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.TextBox txtBANK_ENAME_3;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.TextBox txtBRANCH_ENAME_3;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.TextBox txtBANK_CNAME2_3;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.TextBox txtBANK_CNAME_3;
        private System.Windows.Forms.TextBox txtBRANCH_CNAME2_3;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.TextBox txtBRANCH_CNAME_3;
        private System.Windows.Forms.TextBox txtACCOUNT_CNAME2_3;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.TextBox txtACCOUNT_CNAME_3;
        private System.Windows.Forms.TextBox txtACCOUNT2_3;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.TextBox txtACCOUNT_3;
        private System.Windows.Forms.TextBox txtSWIFT_CODE2_3;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.TextBox txtSWIFT_CODE_3;
        private System.Windows.Forms.TextBox txtBANK_C_ADDRESS2_3;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.TextBox txtBANK_C_ADDRESS_3;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.TextBox txtInterestedRate;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.TextBox txtPOSTAL_CODE;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.TextBox txtPOSTAL_CODE2;
        private System.Windows.Forms.Button btnTransOrder;
        private System.Windows.Forms.Button btnModifyALL;
        private System.Windows.Forms.Button btnModifyHist;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.TextBox txtACCOUNT_ENAME;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.TextBox txtACCOUNT_ENAME2;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.TextBox txtACCOUNT_ENAME_2;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.TextBox txtACCOUNT_ENAME2_2;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.TextBox txtACCOUNT_ENAME_3;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.TextBox txtACCOUNT_ENAME2_3;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.TextBox txtACCOUNT_ENAME2_4;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.TextBox txtACCOUNT_ENAME_4;
        private System.Windows.Forms.Label label122;
        private System.Windows.Forms.TextBox txtBANK_E_ADDRESS2_4;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.TextBox txtBANK_ENAME2_4;
        private System.Windows.Forms.Label label124;
        private System.Windows.Forms.TextBox txtBRANCH_ENAME2_4;
        private System.Windows.Forms.Label label125;
        private System.Windows.Forms.TextBox txtBANK_E_ADDRESS_4;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.TextBox txtBANK_ENAME_4;
        private System.Windows.Forms.Label label127;
        private System.Windows.Forms.TextBox txtBRANCH_ENAME_4;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Label label128;
        private System.Windows.Forms.Label label129;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Label label130;
        private System.Windows.Forms.TextBox txtBANK_CNAME2_4;
        private System.Windows.Forms.Label label131;
        private System.Windows.Forms.Label label132;
        private System.Windows.Forms.TextBox txtBANK_CNAME_4;
        private System.Windows.Forms.TextBox txtBRANCH_CNAME2_4;
        private System.Windows.Forms.Label label133;
        private System.Windows.Forms.Label label134;
        private System.Windows.Forms.TextBox txtBRANCH_CNAME_4;
        private System.Windows.Forms.TextBox txtACCOUNT_CNAME2_4;
        private System.Windows.Forms.Label label135;
        private System.Windows.Forms.Label label136;
        private System.Windows.Forms.TextBox txtACCOUNT_CNAME_4;
        private System.Windows.Forms.TextBox txtACCOUNT2_4;
        private System.Windows.Forms.Label label137;
        private System.Windows.Forms.Label label138;
        private System.Windows.Forms.TextBox txtACCOUNT_4;
        private System.Windows.Forms.TextBox txtSWIFT_CODE2_4;
        private System.Windows.Forms.Label label139;
        private System.Windows.Forms.Label label140;
        private System.Windows.Forms.TextBox txtSWIFT_CODE_4;
        private System.Windows.Forms.TextBox txtBANK_C_ADDRESS2_4;
        private System.Windows.Forms.Label label141;
        private System.Windows.Forms.TextBox txtBANK_C_ADDRESS_4;
        private System.Windows.Forms.Label label142;
        private System.Windows.Forms.TextBox txtACCOUNT_ENAME2_5;
        private System.Windows.Forms.Label label143;
        private System.Windows.Forms.TextBox txtACCOUNT_ENAME_5;
        private System.Windows.Forms.Label label144;
        private System.Windows.Forms.TextBox txtBANK_E_ADDRESS2_5;
        private System.Windows.Forms.Label label145;
        private System.Windows.Forms.TextBox txtBANK_ENAME2_5;
        private System.Windows.Forms.Label label146;
        private System.Windows.Forms.TextBox txtBRANCH_ENAME2_5;
        private System.Windows.Forms.Label label147;
        private System.Windows.Forms.TextBox txtBANK_E_ADDRESS_5;
        private System.Windows.Forms.Label label148;
        private System.Windows.Forms.TextBox txtBANK_ENAME_5;
        private System.Windows.Forms.Label label149;
        private System.Windows.Forms.TextBox txtBRANCH_ENAME_5;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Label label150;
        private System.Windows.Forms.Label label151;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Label label152;
        private System.Windows.Forms.TextBox txtBANK_CNAME2_5;
        private System.Windows.Forms.Label label153;
        private System.Windows.Forms.Label label154;
        private System.Windows.Forms.TextBox txtBANK_CNAME_5;
        private System.Windows.Forms.TextBox txtBRANCH_CNAME2_5;
        private System.Windows.Forms.Label label155;
        private System.Windows.Forms.Label label156;
        private System.Windows.Forms.TextBox txtBRANCH_CNAME_5;
        private System.Windows.Forms.TextBox txtACCOUNT_CNAME2_5;
        private System.Windows.Forms.Label label157;
        private System.Windows.Forms.Label label158;
        private System.Windows.Forms.TextBox txtACCOUNT_CNAME_5;
        private System.Windows.Forms.TextBox txtACCOUNT2_5;
        private System.Windows.Forms.Label label159;
        private System.Windows.Forms.Label label160;
        private System.Windows.Forms.TextBox txtACCOUNT_5;
        private System.Windows.Forms.TextBox txtSWIFT_CODE2_5;
        private System.Windows.Forms.Label label161;
        private System.Windows.Forms.Label label162;
        private System.Windows.Forms.TextBox txtSWIFT_CODE_5;
        private System.Windows.Forms.TextBox txtBANK_C_ADDRESS2_5;
        private System.Windows.Forms.Label label163;
        private System.Windows.Forms.TextBox txtBANK_C_ADDRESS_5;
        private System.Windows.Forms.Label label167;
        private System.Windows.Forms.TextBox txtAmount_AUD;
        private System.Windows.Forms.Label label168;
        private System.Windows.Forms.TextBox txtAmount_AUD_OLD;
        private System.Windows.Forms.TextBox txtInterest_AUD;
        private System.Windows.Forms.Label label169;
        private System.Windows.Forms.Label label164;
        private System.Windows.Forms.TextBox txtAmount_EUR;
        private System.Windows.Forms.Label label165;
        private System.Windows.Forms.TextBox txtAmount_EUR_OLD;
        private System.Windows.Forms.TextBox txtInterest_EUR;
        private System.Windows.Forms.Label label166;
        private System.Windows.Forms.Button btnImportToUSD1;
        private System.Windows.Forms.Button btnImportToUSD2;
        private System.Windows.Forms.Button btnImportToNTD2;
        private System.Windows.Forms.Button btnImportToNTD1;
        private System.Windows.Forms.Button btnImportToRMB2;
        private System.Windows.Forms.Button btnImportToRMB1;
        private System.Windows.Forms.Button btnImportToEUR2;
        private System.Windows.Forms.Button btnImportToEUR1;
        private System.Windows.Forms.Button btnImportToAUD2;
        private System.Windows.Forms.Button btnImportToAUD1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.Button btnImportToRMB2_2;
        private System.Windows.Forms.Button btnImportToRMB1_2;
        private System.Windows.Forms.Label label170;
        private System.Windows.Forms.TextBox txtACCOUNT_ENAME2_3_2;
        private System.Windows.Forms.Label label171;
        private System.Windows.Forms.TextBox txtACCOUNT_ENAME_3_2;
        private System.Windows.Forms.Label label172;
        private System.Windows.Forms.TextBox txtBANK_E_ADDRESS2_3_2;
        private System.Windows.Forms.Label label173;
        private System.Windows.Forms.TextBox txtBANK_ENAME2_3_2;
        private System.Windows.Forms.Label label174;
        private System.Windows.Forms.TextBox txtBRANCH_ENAME2_3_2;
        private System.Windows.Forms.Label label175;
        private System.Windows.Forms.TextBox txtBANK_E_ADDRESS_3_2;
        private System.Windows.Forms.Label label176;
        private System.Windows.Forms.TextBox txtBANK_ENAME_3_2;
        private System.Windows.Forms.Label label177;
        private System.Windows.Forms.TextBox txtBRANCH_ENAME_3_2;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Label label178;
        private System.Windows.Forms.Label label179;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Label label180;
        private System.Windows.Forms.TextBox txtBANK_CNAME2_3_2;
        private System.Windows.Forms.Label label181;
        private System.Windows.Forms.Label label182;
        private System.Windows.Forms.TextBox txtBANK_CNAME_3_2;
        private System.Windows.Forms.TextBox txtBRANCH_CNAME2_3_2;
        private System.Windows.Forms.Label label183;
        private System.Windows.Forms.Label label184;
        private System.Windows.Forms.TextBox txtBRANCH_CNAME_3_2;
        private System.Windows.Forms.TextBox txtACCOUNT_CNAME2_3_2;
        private System.Windows.Forms.Label label185;
        private System.Windows.Forms.Label label186;
        private System.Windows.Forms.TextBox txtACCOUNT_CNAME_3_2;
        private System.Windows.Forms.TextBox txtACCOUNT2_3_2;
        private System.Windows.Forms.Label label187;
        private System.Windows.Forms.Label label188;
        private System.Windows.Forms.TextBox txtACCOUNT_3_2;
        private System.Windows.Forms.TextBox txtSWIFT_CODE2_3_2;
        private System.Windows.Forms.Label label189;
        private System.Windows.Forms.Label label190;
        private System.Windows.Forms.TextBox txtSWIFT_CODE_3_2;
        private System.Windows.Forms.TextBox txtBANK_C_ADDRESS2_3_2;
        private System.Windows.Forms.Label label191;
        private System.Windows.Forms.TextBox txtBANK_C_ADDRESS_3_2;
        private System.Windows.Forms.CheckBox chkIsMultiContract;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.RadioButton rdoWebDepositType_Null;
        private System.Windows.Forms.RadioButton rdoWebDepositType_Y;
        private System.Windows.Forms.Label label192;
        private System.Windows.Forms.Label label193;
        private System.Windows.Forms.ComboBox cboIS_REMIT_TO_AUS;
        private System.Windows.Forms.CheckBox chk_ind_order;
        private System.Windows.Forms.Button btnImportToJPY2;
        private System.Windows.Forms.Button btnImportToJPY1;
        private System.Windows.Forms.Label label194;
        private System.Windows.Forms.TextBox txtACCOUNT_ENAME2_6;
        private System.Windows.Forms.Label label195;
        private System.Windows.Forms.TextBox txtACCOUNT_ENAME_6;
        private System.Windows.Forms.Label label196;
        private System.Windows.Forms.TextBox txtBANK_E_ADDRESS2_6;
        private System.Windows.Forms.Label label197;
        private System.Windows.Forms.TextBox txtBANK_ENAME2_6;
        private System.Windows.Forms.Label label198;
        private System.Windows.Forms.TextBox txtBRANCH_ENAME2_6;
        private System.Windows.Forms.Label label199;
        private System.Windows.Forms.TextBox txtBANK_E_ADDRESS_6;
        private System.Windows.Forms.Label label200;
        private System.Windows.Forms.TextBox txtBANK_ENAME_6;
        private System.Windows.Forms.Label label201;
        private System.Windows.Forms.TextBox txtBRANCH_ENAME_6;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Label label202;
        private System.Windows.Forms.Label label203;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Label label204;
        private System.Windows.Forms.TextBox txtBANK_CNAME2_6;
        private System.Windows.Forms.Label label205;
        private System.Windows.Forms.Label label206;
        private System.Windows.Forms.TextBox txtBANK_CNAME_6;
        private System.Windows.Forms.TextBox txtBRANCH_CNAME2_6;
        private System.Windows.Forms.Label label207;
        private System.Windows.Forms.Label label208;
        private System.Windows.Forms.TextBox txtBRANCH_CNAME_6;
        private System.Windows.Forms.TextBox txtACCOUNT_CNAME2_6;
        private System.Windows.Forms.Label label209;
        private System.Windows.Forms.Label label210;
        private System.Windows.Forms.TextBox txtACCOUNT_CNAME_6;
        private System.Windows.Forms.TextBox txtACCOUNT2_6;
        private System.Windows.Forms.Label label211;
        private System.Windows.Forms.Label label212;
        private System.Windows.Forms.TextBox txtACCOUNT_6;
        private System.Windows.Forms.TextBox txtSWIFT_CODE2_6;
        private System.Windows.Forms.Label label213;
        private System.Windows.Forms.Label label214;
        private System.Windows.Forms.TextBox txtSWIFT_CODE_6;
        private System.Windows.Forms.TextBox txtBANK_C_ADDRESS2_6;
        private System.Windows.Forms.Label label215;
        private System.Windows.Forms.TextBox txtBANK_C_ADDRESS_6;
        private System.Windows.Forms.Label label219;
        private System.Windows.Forms.TextBox txtDiscount;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.RadioButton rdoBonusA;
        private System.Windows.Forms.RadioButton rdoBonusB;
        private System.Windows.Forms.Label label221;
        private System.Windows.Forms.Label lbl2020Condition;
        private System.Windows.Forms.Button btnImportToNZD2;
        private System.Windows.Forms.Button btnImportToNZD1;
        private System.Windows.Forms.Label label220;
        private System.Windows.Forms.TextBox txtACCOUNT_ENAME2_7;
        private System.Windows.Forms.Label label222;
        private System.Windows.Forms.TextBox txtACCOUNT_ENAME_7;
        private System.Windows.Forms.Label label223;
        private System.Windows.Forms.TextBox txtBANK_E_ADDRESS2_7;
        private System.Windows.Forms.Label label224;
        private System.Windows.Forms.TextBox txtBANK_ENAME2_7;
        private System.Windows.Forms.Label label225;
        private System.Windows.Forms.TextBox txtBRANCH_ENAME2_7;
        private System.Windows.Forms.Label label226;
        private System.Windows.Forms.TextBox txtBANK_E_ADDRESS_7;
        private System.Windows.Forms.Label label227;
        private System.Windows.Forms.TextBox txtBANK_ENAME_7;
        private System.Windows.Forms.Label label228;
        private System.Windows.Forms.TextBox txtBRANCH_ENAME_7;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Label label229;
        private System.Windows.Forms.Label label230;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.Label label231;
        private System.Windows.Forms.TextBox txtBANK_CNAME2_7;
        private System.Windows.Forms.Label label232;
        private System.Windows.Forms.Label label233;
        private System.Windows.Forms.TextBox txtBANK_CNAME_7;
        private System.Windows.Forms.TextBox txtBRANCH_CNAME2_7;
        private System.Windows.Forms.Label label234;
        private System.Windows.Forms.Label label235;
        private System.Windows.Forms.TextBox txtBRANCH_CNAME_7;
        private System.Windows.Forms.TextBox txtACCOUNT_CNAME2_7;
        private System.Windows.Forms.Label label236;
        private System.Windows.Forms.Label label237;
        private System.Windows.Forms.TextBox txtACCOUNT_CNAME_7;
        private System.Windows.Forms.TextBox txtACCOUNT2_7;
        private System.Windows.Forms.Label label238;
        private System.Windows.Forms.Label label239;
        private System.Windows.Forms.TextBox txtACCOUNT_7;
        private System.Windows.Forms.TextBox txtSWIFT_CODE2_7;
        private System.Windows.Forms.Label label240;
        private System.Windows.Forms.Label label241;
        private System.Windows.Forms.TextBox txtSWIFT_CODE_7;
        private System.Windows.Forms.TextBox txtBANK_C_ADDRESS2_7;
        private System.Windows.Forms.Label label242;
        private System.Windows.Forms.TextBox txtBANK_C_ADDRESS_7;
        private System.Windows.Forms.TextBox txtInterest_NZD;
        private System.Windows.Forms.Label label245;
        private System.Windows.Forms.TextBox txtAmount_NZD_OLD;
        private System.Windows.Forms.Label label244;
        private System.Windows.Forms.Label label243;
        private System.Windows.Forms.TextBox txtAmount_NZD;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label label216;
        private System.Windows.Forms.TextBox txtAmount_JPY;
        private System.Windows.Forms.Label label218;
        private System.Windows.Forms.Label label217;
        private System.Windows.Forms.TextBox txtInterest_JPY;
        private System.Windows.Forms.TextBox txtAmount_JPY_OLD;
        private System.Windows.Forms.TextBox txtIBCode_Internal;
        private System.Windows.Forms.Label label246;
        private System.Windows.Forms.Button btnBringSales_Internal;
        private System.Windows.Forms.TextBox txtSales_Intermal;
        private System.Windows.Forms.Label label247;
        private System.Windows.Forms.Label label248;
        private System.Windows.Forms.Label lblIS_ONLINE_WITHDRAWAL_CHECK;
    }
}
