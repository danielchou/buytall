﻿namespace MK_DEMO.Destop.FormFunction
{
    partial class frmNTD_Payment_Job
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.btnClose = new System.Windows.Forms.Button();
            this.btnDoJob = new System.Windows.Forms.Button();
            this.lblLastExecuteInfo1 = new System.Windows.Forms.Label();
            this.lblLastExecuteInfo2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnClose.Location = new System.Drawing.Point(25, 190);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(328, 95);
            this.btnClose.TabIndex = 7;
            this.btnClose.Text = "關閉";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnDoJob
            // 
            this.btnDoJob.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnDoJob.Location = new System.Drawing.Point(25, 76);
            this.btnDoJob.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnDoJob.Name = "btnDoJob";
            this.btnDoJob.Size = new System.Drawing.Size(328, 95);
            this.btnDoJob.TabIndex = 6;
            this.btnDoJob.Text = "執行台幣提領執行作業";
            this.btnDoJob.UseVisualStyleBackColor = true;
            this.btnDoJob.Click += new System.EventHandler(this.btnDoJob_Click);
            // 
            // lblLastExecuteInfo1
            // 
            this.lblLastExecuteInfo1.AutoSize = true;
            this.lblLastExecuteInfo1.Font = new System.Drawing.Font("微軟正黑體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lblLastExecuteInfo1.ForeColor = System.Drawing.Color.Blue;
            this.lblLastExecuteInfo1.Location = new System.Drawing.Point(25, 9);
            this.lblLastExecuteInfo1.Name = "lblLastExecuteInfo1";
            this.lblLastExecuteInfo1.Size = new System.Drawing.Size(183, 24);
            this.lblLastExecuteInfo1.TabIndex = 8;
            this.lblLastExecuteInfo1.Text = "lblLastExecuteInfo1";
            // 
            // lblLastExecuteInfo2
            // 
            this.lblLastExecuteInfo2.AutoSize = true;
            this.lblLastExecuteInfo2.Font = new System.Drawing.Font("微軟正黑體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lblLastExecuteInfo2.ForeColor = System.Drawing.Color.Blue;
            this.lblLastExecuteInfo2.Location = new System.Drawing.Point(25, 39);
            this.lblLastExecuteInfo2.Name = "lblLastExecuteInfo2";
            this.lblLastExecuteInfo2.Size = new System.Drawing.Size(183, 24);
            this.lblLastExecuteInfo2.TabIndex = 9;
            this.lblLastExecuteInfo2.Text = "lblLastExecuteInfo2";
            // 
            // frmNTD_Payment_Job
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.ClientSize = new System.Drawing.Size(382, 303);
            this.Controls.Add(this.lblLastExecuteInfo2);
            this.Controls.Add(this.lblLastExecuteInfo1);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnDoJob);
            this.Font = new System.Drawing.Font("微軟正黑體", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmNTD_Payment_Job";
            this.Text = "台幣提領執行作業";
            this.Load += new System.EventHandler(this.frmNTD_Payment_Job_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnDoJob;
        private System.Windows.Forms.Label lblLastExecuteInfo1;
        private System.Windows.Forms.Label lblLastExecuteInfo2;
    }
}
