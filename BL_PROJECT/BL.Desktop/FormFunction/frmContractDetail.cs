﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using MK.Demo.Utility;
using MK.Demo.Model;
using MK.Demo.Logic;
using System.Threading.Tasks;
using System.Linq;
using static MK.Demo.Data.enCommon;

namespace MK_DEMO.Destop.FormFunction
{
    public partial class frmContractDetail : MK_DEMO.Destop.BaseForm
    {
        private string strCustID;
        public enFormStatus status;
        private bool IsSelection = false;

        public string strORDER_NO;
        public string strAMOUNT_USD;
        public string strAMOUNT_NTD;
        public string strAMOUNT_RMB;
        public string strAMOUNT_EUR;
        public string strAMOUNT_AUD;
        public string strAMOUNT_JPY;
        public string strAMOUNT_NZD;


        public frmContractDetail(string strCustID)
        {
            InitializeComponent();

            this.strCustID = strCustID;

            this.dgv_header.AutoGenerateColumns = false;
            this.dgv_header.MultiSelect = false;
            this.dgv_header.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        public frmContractDetail(string strCustID, bool IsFromSelection) : this(strCustID)
        {
            this.IsSelection = IsFromSelection;
        }

        private void frmContractDetail_Load(object sender, EventArgs e)
        {
            this.btnOK.Visible = this.IsSelection;
            this.BindData();
        }

        private void BindData()
        {
            this.picLoder.Visible = true;
            this.picLoder.BringToFront();
            if (this.backgroundWorker1.IsBusy == false)
            {
                this.backgroundWorker1.RunWorkerAsync();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            status = enFormStatus.Cancel;
            this.Close();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            System.Threading.Thread.Sleep(50);
            var data = base.Cust.GetContractDataByCustID(this.strCustID, this.txtCustName.Text);
            e.Result = data;
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //this.dgv_header.DataSource = new List<BL_ORDER>();
            this.dgv_header.DataBindByList(new List<BL_ORDER>());
            var data = e.Result as List<BL_ORDER>;
            if (data != null)
            {
                //this.dgv_header.DataSource = data;
                this.dgv_header.DataBindByList(data);
            }
            this.picLoder.Visible = false;
        }

        private void dgv_header_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Parallel.ForEach(this.dgv_header.Rows.Cast<DataGridViewRow>(), x =>
                {
                    x.Cells["check"].Value = false;
                });
                this.dgv_header.Rows[e.RowIndex].Cells["check"].Value = true;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            var selection = this.dgv_header.Rows.Cast<DataGridViewRow>().Where(x => (bool?)x.Cells["check"].Value == true).ToList();
            if (selection.Count == 1)
            {
                this.strORDER_NO = selection[0].Cells["ORDER_NO"].Value.ConvertToString();
                this.strAMOUNT_USD = selection[0].Cells["AMOUNT_USD"].Value.ConvertToString();
                this.strAMOUNT_NTD = selection[0].Cells["AMOUNT_NTD"].Value.ConvertToString();
                this.strAMOUNT_RMB = selection[0].Cells["AMOUNT_RMB"].Value.ConvertToString();
                this.strAMOUNT_EUR = selection[0].Cells["AMOUNT_EUR"].Value.ConvertToString();
                this.strAMOUNT_AUD = selection[0].Cells["AMOUNT_AUD"].Value.ConvertToString();
                this.strAMOUNT_JPY = selection[0].Cells["AMOUNT_JPY"].Value.ConvertToString();
                this.strAMOUNT_NZD = selection[0].Cells["AMOUNT_NZD"].Value.ConvertToString();

                this.status = enFormStatus.OK;
                this.Close();
            }
            else
            {
                MessageBox.Show("請單選");
            }

        }

        private void dgv_header_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            Parallel.ForEach(this.dgv_header.Rows.Cast<DataGridViewRow>(), x =>
            {

                if (x.Cells["ORDER_END"].Value.ConvertToString().Trim() != string.Empty)
                {
                    x.DefaultCellStyle.BackColor = Color.Pink;
                }
            });
        }

        private void dgv_header_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {

            if (dgv_header.Columns[e.ColumnIndex].Name.Equals("CURRENCY"))
            {
                string CURRENCY = (string)e.Value;
                if (CURRENCY == "USD")
                {
                    e.CellStyle.ForeColor = Color.Red;
                }
                else if (CURRENCY == "NTD")
                {
                    e.CellStyle.ForeColor = Color.Blue;
                }
                else if (CURRENCY == "AUD")
                {
                    e.CellStyle.ForeColor = Color.Green;
                }
                else
                {
                    e.CellStyle.ForeColor = Color.DeepPink;
                }

            }

        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.BindData();
        }
    }
}
