﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MK.Demo.Utility;
using Microsoft.Reporting.WinForms;
using MK.Demo.Logic;
using System.IO;
using MK.Demo.Model;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.xml;
using iTextSharp.text.html;

namespace MK_DEMO.Destop.FormFunction
{
    public partial class frmRPT_MonthlyBill : Form
    {
        private List<string> liCustID = new List<string>();
        private List<BL_CUST> liCust_ALL = new List<BL_CUST>();
        
        public frmRPT_MonthlyBill()
        {
            InitializeComponent();
        }

        private void frmRPT_MonthlyBill_Load(object sender, EventArgs e)
        {
            this.InitComboBox();
            //this.BindReport();
        }

        private void InitComboBox()
        {
            var dataMaxDate = new logicCust(Global.IsTestMode).GetPeriodMaxDate();

            this.dteReportEndDate.Value = DateTime.Now;
            this.dteReportEndDate.Text = string.Empty;
            this.dteReportEndDate.Format = DateTimePickerFormat.Custom;
            this.dteReportEndDate.CustomFormat = " ";

            var dteMax = new DateTime(int.Parse(dataMaxDate.YEAR), int.Parse(dataMaxDate.MON), 1);
            this.dteReportEndDate.MaxDate = dteMax.AddMonths(1).AddDays(-1);
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.BindReport();
        }


        private void BindReport()
        {
            var strRepoetEndDate = DateTime.Now;
            if (this.dteReportEndDate.Text.IsDate()) strRepoetEndDate = this.dteReportEndDate.Value;
            //var liCust = this.liCustID;
            var liCust = new List<long>();
            long lnTry;
            this.liCustID.ForEach(x =>
            {
                if (long.TryParse(x, out lnTry))
                {
                    liCust.Add(lnTry);
                }
            });

            if (this.DataValidation())
            {

                //long BatchSN = new logicCust(Global.IsTestMode).InsertRPT_MONTHLY_BILLData(strRepoetEndDate, liCust);
                //var data = new logicCust(Global.IsTestMode).GetRPT_MONTHLY_BILLDataByBatchSN(BatchSN);
                var data = new logicCust(Global.IsTestMode).GetRPT_MONTHLY_BILLData(strRepoetEndDate, liCust);
                this.CombineIB_CODE(data);

                string exeFolder = Application.StartupPath;
                string reportPath = System.IO.Path.Combine(exeFolder, @"RPT\RPT_MONTHLY_BILL.rdlc");

                var binding = new BindingSource();
                binding.DataSource = data;
                this.reportViewer2.Reset();

                this.reportViewer2.LocalReport.ReportPath = reportPath;
                this.reportViewer2.LocalReport.SetParameters(new ReportParameter("rptMonth", strRepoetEndDate.ToString("MM")));
                var strEnd = DateTime.Parse(strRepoetEndDate.ToString("yyyy/MM/01")).AddMonths(1).AddDays(-1).ToString("yyyy/MM/dd");
                this.reportViewer2.LocalReport.SetParameters(new ReportParameter("rptEnd", strEnd));
                this.reportViewer2.LocalReport.DataSources.Add(new ReportDataSource("dsReport", binding));
                this.reportViewer2.RefreshReport();

            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dteReportEndDate_ValueChanged(object sender, EventArgs e)
        {
            this.dteReportEndDate.Format = DateTimePickerFormat.Long;
        }

        private void btnBringCust_Click(object sender, EventArgs e)
        {
            var strTXN_DATE = this.dteReportEndDate.Value.ToString("yyyy/MM");
            var frm = new FrmSelectCust(this.liCustID, strTXN_DATE, true) { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
            frm.ShowDialog();
            if (frm.status == MK.Demo.Data.enCommon.enFormStatus.OK)
            {
                this.txtCust.Text = frm.strDisplay;
                this.liCustID = frm.liCustID;
                this.liCust_ALL = frm.liCust;
            }
            frm.Dispose();
            frm = null;
        }

        private bool DataValidation()
        {
            var result = true;
            var msg = string.Empty;

            //var liCust = this.liCustID;
            var liCust = new List<long>();
            long lnTry;
            this.liCustID.ForEach(x =>
            {
                if (long.TryParse(x, out lnTry))
                {
                    liCust.Add(lnTry);
                }
            });

            if (liCust.Count == 0)
            {
                msg += "請先選取客戶";
            }


            if (msg != string.Empty)
            {
                result = false;
                MessageBox.Show(msg);
            }
            return result;
        }

        private void btnExportPDF_Click(object sender, EventArgs e)
        {
            var strRepoetEndDate = DateTime.Now;
            if (this.dteReportEndDate.Text.IsDate()) strRepoetEndDate = this.dteReportEndDate.Value;

            if (this.DataValidation())
            {
                using (var fbd = new FolderBrowserDialog())
                {
                    DialogResult result = fbd.ShowDialog();
                    if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                    {
                        //  存放路徑
                        var strSavePath = fbd.SelectedPath;

                        this.liCust_ALL.ForEach(x =>
                        {
                            long CUST_ID = x.CUST_ID == null ? 0 : long.Parse(x.CUST_ID.Value.ConvertToString());
                            if (CUST_ID > 0)
                            {
                                //long BatchSN = new logicCust(Global.IsTestMode).InsertRPT_MONTHLY_BILLData(strRepoetEndDate, new List<long>() { CUST_ID });
                                //var data = new logicCust(Global.IsTestMode).GetRPT_MONTHLY_BILLDataByBatchSN(BatchSN);
                                var data = new logicCust(Global.IsTestMode).GetRPT_MONTHLY_BILLData(strRepoetEndDate, new List<long>() { CUST_ID });
                                this.CombineIB_CODE(data);

                                string exeFolder = Application.StartupPath;
                                string reportPath = System.IO.Path.Combine(exeFolder, @"RPT\RPT_MONTHLY_BILL.rdlc");
                                

                                var binding = new BindingSource();
                                binding.DataSource = data;
                                this.reportViewer2.Reset();

                                this.reportViewer2.LocalReport.ReportPath = reportPath;
                                this.reportViewer2.LocalReport.SetParameters(new ReportParameter("rptMonth", strRepoetEndDate.ToString("MM")));
                                var strEnd = DateTime.Parse(strRepoetEndDate.ToString("yyyy/MM/01")).AddMonths(1).AddDays(-1).ToString("yyyy/MM/dd");
                                this.reportViewer2.LocalReport.SetParameters(new ReportParameter("rptEnd", strEnd));
                                this.reportViewer2.LocalReport.DataSources.Add(new ReportDataSource("dsReport", binding));
                                this.reportViewer2.RefreshReport();

                                //  Export
                                var strFileName = strSavePath + "/" + x.CUST_CNAME.ConvertToString() + "_" + DateTime.Now.ToString("yyyyMMddHHmmssffff") + ".pdf";
                                //strFileName = strSavePath + "/" + x.CUST_CNAME.ConvertToString() + "_" +
                                //    (
                                //        x.CUST_CNAME2.ConvertToString().Trim() != string.Empty
                                //            ? x.UNION_ID_NUMBER.ConvertToString().Trim()
                                //            : x.ID_NUMBER.ConvertToString().Trim()
                                //    ) + ".pdf";
                                strFileName = strSavePath + "/" + x.CUST_CNAME.ConvertToString() + ".pdf";
                                strFileName = strSavePath + "/" + "月對帳單_" + strRepoetEndDate.ToString("yyyyMM") + "_" + x.CUST_CNAME.ConvertToString() + ".pdf";
                                Warning[] warnings;
                                string[] streamids;
                                string mimeType;
                                string encoding;
                                string filenameExtension;

                                byte[] bytes = this.reportViewer2.LocalReport.Render(
                                    "PDF", null, out mimeType, out encoding, out filenameExtension,
                                    out streamids, out warnings);

                                using (FileStream fs = new FileStream(strFileName, FileMode.Create))
                                {
                                    fs.Write(bytes, 0, bytes.Length);
                                }
                            }
                        });
                    }
                }
                //  最後Bind全部Report回畫面上
                this.BindReport();
                MessageBox.Show("檔案已全數產生完畢");
            }
        }

        private void btnExportPDF_Encryption_Click(object sender, EventArgs e)
        {
            var strRepoetEndDate = DateTime.Now;
            if (this.dteReportEndDate.Text.IsDate()) strRepoetEndDate = this.dteReportEndDate.Value;

            if (this.DataValidation())
            {
                using (var fbd = new FolderBrowserDialog())
                {
                    DialogResult result = fbd.ShowDialog();
                    if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                    {
                        var logic = new logicCust(Global.IsTestMode);
                        var dataLog = new List<BL_BILL_MAIL_LOG>();
                        var strJob_ID = logic.GetBillMailJob_ID();
                        var strTXN_DATE = strRepoetEndDate.ToString("yyyy/MM");
                        var strYYMMDD_thisEnd = DateTime.Parse(strRepoetEndDate.ToString("yyyy/MM/01")).AddMonths(1).AddDays(-1).ToString("yyyy/MM/dd");
                        var strYYMMDD_prevEnd = DateTime.Parse(strRepoetEndDate.ToString("yyyy/MM/01")).AddDays(-1).ToString("yyyy/MM/dd");


                        //  存放路徑
                        var strSavePath = fbd.SelectedPath;
                        var fileEncryption = new List<FILE_ENCRYPTION>();

                        this.liCust_ALL.ForEach(x =>
                        {
                            long CUST_ID = x.CUST_ID == null ? 0 : long.Parse(x.CUST_ID.Value.ConvertToString());
                            if (CUST_ID > 0)
                            {
                                //long BatchSN = new logicCust(Global.IsTestMode).InsertRPT_MONTHLY_BILLData(strRepoetEndDate, new List<long>() { CUST_ID });
                                //var data = new logicCust(Global.IsTestMode).GetRPT_MONTHLY_BILLDataByBatchSN(BatchSN);
                                var data = new logicCust(Global.IsTestMode).GetRPT_MONTHLY_BILLData(strRepoetEndDate, new List<long>() { CUST_ID });
                                this.CombineIB_CODE(data);

                                string exeFolder = Application.StartupPath;
                                string reportPath = System.IO.Path.Combine(exeFolder, @"RPT\RPT_MONTHLY_BILL.rdlc");
                                

                                var binding = new BindingSource();
                                binding.DataSource = data;
                                this.reportViewer2.Reset();

                                this.reportViewer2.LocalReport.ReportPath = reportPath;
                                this.reportViewer2.LocalReport.SetParameters(new ReportParameter("rptMonth", strRepoetEndDate.ToString("MM")));
                                var strEnd = DateTime.Parse(strRepoetEndDate.ToString("yyyy/MM/01")).AddMonths(1).AddDays(-1).ToString("yyyy/MM/dd");
                                this.reportViewer2.LocalReport.SetParameters(new ReportParameter("rptEnd", strEnd));
                                this.reportViewer2.LocalReport.DataSources.Add(new ReportDataSource("dsReport", binding));
                                this.reportViewer2.RefreshReport();

                                //  Export
                                //聯名戶要秀出
                                var IsUnionCust = false;
                                var custname_all = x.CUST_CNAME2.ConvertToString() == string.Empty ? x.CUST_CNAME.ConvertToString() : x.CUST_CNAME.ConvertToString() +"_"+ x.CUST_CNAME2.ConvertToString();
                                var strFileName = x.CUST_CNAME.ConvertToString() + "_" + DateTime.Now.ToString("yyyyMMddHHmmssffff");
                                if (x.CUST_CNAME2.ConvertToString() != string.Empty)
                                {
                                    IsUnionCust = true;
                                }
                                // var strFileName = custname_all + "_" + DateTime.Now.ToString("yyyyMMddHHmmssffff");

                                //strFileName = x.CUST_CNAME.ConvertToString() + "_" +
                                //    (
                                //        x.CUST_CNAME2.ConvertToString().Trim() != string.Empty
                                //            ? x.UNION_ID_NUMBER.ConvertToString().Trim()
                                //            : x.ID_NUMBER.ConvertToString().Trim()
                                //    );
                                //   strFileName = x.CUST_CNAME.ConvertToString();
                                custname_all = custname_all.Replace("？", "_");  //  2019/07/10 > 處理難字問題
                                strFileName = "月對帳單_" + strRepoetEndDate.ToString("yyyyMM") + "_" + custname_all;//x.CUST_CNAME.ConvertToString();
                                strFileName = custname_all + "_月對帳單_" + strRepoetEndDate.ToString("yyyyMM");
                                fileEncryption.Add(new FILE_ENCRYPTION()
                                {
                                    FileName = strFileName,
                                    Password = x.CUST_CNAME2.ConvertToString().Trim() != string.Empty
                                        ? x.UNION_ID_NUMBER.ConvertToString().Trim()
                                        : x.ID_NUMBER.ConvertToString().Trim(),
                                    Sales_Name = x.SALES_NAME
                                });


                                Warning[] warnings;
                                string[] streamids;
                                string mimeType;
                                string encoding;
                                string filenameExtension;

                                byte[] bytes = this.reportViewer2.LocalReport.Render(
                                    "PDF", null, out mimeType, out encoding, out filenameExtension,
                                    out streamids, out warnings);

                                using (FileStream fs = new FileStream(strSavePath + "/" + x.SALES_NAME + "_" + strFileName + "_Temp.pdf", FileMode.Create))
                                {
                                    fs.Write(bytes, 0, bytes.Length);
                                }

                                dataLog.Add(new BL_BILL_MAIL_LOG()
                                {
                                    JOB_ID = strJob_ID,
                                    CUST_ID = x.CUST_ID,
                                    PDF_LOCATION = strSavePath + "\\" + strFileName + ".pdf",
                                    EMAIL = x.EMAIL_1.ConvertToString(),
                                    PREV_DATE = strYYMMDD_prevEnd,
                                    THIS_DATE = strYYMMDD_thisEnd,
                                    TXN_DATE = strRepoetEndDate
                                });
                                if (IsUnionCust)
                                {
                                    //  2019/08/05 > 聯名戶也要收到mail
                                    dataLog.Add(new BL_BILL_MAIL_LOG()
                                    {
                                        JOB_ID = strJob_ID,
                                        CUST_ID = x.CUST_ID,
                                        PDF_LOCATION = strSavePath + "\\" + strFileName + ".pdf",
                                        EMAIL = x.EMAIL_2.ConvertToString(),
                                        PREV_DATE = strYYMMDD_prevEnd,
                                        THIS_DATE = strYYMMDD_thisEnd,
                                        TXN_DATE = strRepoetEndDate
                                    });
                                }
                            }
                        });

                        //  Encryption
                        fileEncryption.ForEach(x =>
                        {
                            PdfReader reader = new PdfReader(strSavePath + "/" + x.Sales_Name + "_" + x.FileName + "_Temp.pdf");
                            using (var os = new FileStream(strSavePath + "/" + x.FileName + ".pdf", FileMode.Create))
                            {
                                PdfEncryptor.Encrypt(reader, os, true, x.Password, "", PdfWriter.AllowPrinting);
                            }
                        });

                        if (logic.WriteBillMailLog(dataLog, strJob_ID, Global.str_UserName))
                        {
                            MessageBox.Show("檔案已全數產生完畢" + Environment.NewLine + "本次產生批號 : " + strJob_ID);
                        }
                        else
                        {
                            MessageBox.Show("批次產生失敗");
                        }
                    }
                }
                //  最後Bind全部Report回畫面上
                this.BindReport();
                //MessageBox.Show("檔案已全數產生完畢");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var dataOutputFile = new List<FILE_INFO>();
            var strRepoetEndDate = DateTime.Now;
            if (this.dteReportEndDate.Text.IsDate()) strRepoetEndDate = this.dteReportEndDate.Value;

            if (this.DataValidation())
            {
                using (var fbd = new FolderBrowserDialog())
                {
                    DialogResult result = fbd.ShowDialog();
                    if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                    {
                        //  存放路徑
                        var strSavePath = fbd.SelectedPath;
                        var fileEncryption = new List<FILE_ENCRYPTION>();

                        this.liCust_ALL.ForEach(x =>
                        {
                            long CUST_ID = x.CUST_ID == null ? 0 : long.Parse(x.CUST_ID.Value.ConvertToString());
                            if (CUST_ID > 0)
                            {
                                //long BatchSN = new logicCust(Global.IsTestMode).InsertRPT_MONTHLY_BILLData(strRepoetEndDate, new List<long>() { CUST_ID });
                                //var data = new logicCust(Global.IsTestMode).GetRPT_MONTHLY_BILLDataByBatchSN(BatchSN);
                                var data = new logicCust(Global.IsTestMode).GetRPT_MONTHLY_BILLData(strRepoetEndDate, new List<long>() { CUST_ID });
                                this.CombineIB_CODE(data);

                                string exeFolder = Application.StartupPath;
                                string reportPath = System.IO.Path.Combine(exeFolder, @"RPT\RPT_MONTHLY_BILL.rdlc");

                                var binding = new BindingSource();
                                binding.DataSource = data;
                                this.reportViewer2.Reset();

                                this.reportViewer2.LocalReport.ReportPath = reportPath;
                                this.reportViewer2.LocalReport.SetParameters(new ReportParameter("rptMonth", strRepoetEndDate.ToString("MM")));
                                var strEnd = DateTime.Parse(strRepoetEndDate.ToString("yyyy/MM/01")).AddMonths(1).AddDays(-1).ToString("yyyy/MM/dd");
                                this.reportViewer2.LocalReport.SetParameters(new ReportParameter("rptEnd", strEnd));
                                this.reportViewer2.LocalReport.DataSources.Add(new ReportDataSource("dsReport", binding));
                                this.reportViewer2.RefreshReport();

                                //  Export
                                var strFileName = x.CUST_CNAME.ConvertToString() + "_" + DateTime.Now.ToString("yyyyMMddHHmmssffff");
                                
                                strFileName = x.CUST_CNAME.ConvertToString();
                                strFileName = "月對帳單_" + strRepoetEndDate.ToString("yyyyMM") + "_" + x.CUST_CNAME.ConvertToString();
                                fileEncryption.Add(new FILE_ENCRYPTION()
                                {
                                    FileName = strFileName,
                                    Password = x.CUST_CNAME2.ConvertToString().Trim() != string.Empty
                                        ? x.UNION_ID_NUMBER.ConvertToString().Trim()
                                        : x.ID_NUMBER.ConvertToString().Trim()
                                });


                                Warning[] warnings;
                                string[] streamids;
                                string mimeType;
                                string encoding;
                                string filenameExtension;

                                byte[] bytes = this.reportViewer2.LocalReport.Render(
                                    "PDF", null, out mimeType, out encoding, out filenameExtension,
                                    out streamids, out warnings);

                                using (FileStream fs = new FileStream(strSavePath + "/" + strFileName + "_Temp.pdf", FileMode.Create))
                                {
                                    fs.Write(bytes, 0, bytes.Length);
                                }

                                dataOutputFile.Add(new FILE_INFO()
                                {
                                    CUST_ID = CUST_ID.ConvertToString(),
                                    CUST_NAME = x.CUST_CNAME.ConvertToString(),
                                    FILE_NAME = strFileName + ".pdf",
                                    FILE_PATH = strSavePath,
                                    EMAIL = x.EMAIL_1.ConvertToString(),
                                    DATA_YM = strRepoetEndDate.ToString("yyyyMM"),
                                    MAIL_TYPE_END = x.MAIL_TYPE_END
                                });
                            }
                        });

                        //  Encryption
                        fileEncryption.ForEach(x =>
                        {
                            PdfReader reader = new PdfReader(strSavePath + "/" + x.FileName + "_Temp.pdf");
                            using (var os = new FileStream(strSavePath + "/" + x.FileName + ".pdf", FileMode.Create))
                            {
                                PdfEncryptor.Encrypt(reader, os, true, x.Password, "", PdfWriter.AllowPrinting);
                            }
                        });
                    }
                }

                if (dataOutputFile.Count() > 0)
                {
                    //  開啟發送視窗
                    var frm = new frmRPT_MonthlyBill_Mail(dataOutputFile) { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
                    frm.ShowDialog();
                    frm.Dispose();
                    frm = null;
                }

                //  最後Bind全部Report回畫面上
                this.BindReport();
            }
        }

        private void CombineIB_CODE(List<BL_RPT_MONTHLY_BILL> data)
        {
            data.GroupBy(x => new
            {
                x.CUST_CNAME,
                x.CURRENCY
            })
            .Distinct()
            .ForEach(x =>
            {
                var strIB_CODE = string.Empty;
                data
                    .Where
                    (
                        y =>
                            x.Key.CUST_CNAME == y.CUST_CNAME &&
                            x.Key.CURRENCY == y.CURRENCY
                    ).Select(y => new
                    {
                        y.IB_CODE
                    }).Distinct()
                    .ForEach(y =>
                    {
                        strIB_CODE += strIB_CODE == string.Empty ? "" : "/";
                        strIB_CODE += y.IB_CODE.ConvertToString();
                    });

                data
                    .Where
                    (
                        y =>
                            x.Key.CUST_CNAME == y.CUST_CNAME &&
                            x.Key.CURRENCY == y.CURRENCY
                    ).ForEach(y =>
                    {
                        y.IB_CODE_COMBINE = strIB_CODE;
                    });
            });

        }

        private void button4_Click(object sender, EventArgs e)
        {
            //  開啟發送視窗
            var frm = new frmRPT_MonthlyBill_Mail(null) { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
            frm.ShowDialog();
            frm.Dispose();
            frm = null;
        }
    }



}
