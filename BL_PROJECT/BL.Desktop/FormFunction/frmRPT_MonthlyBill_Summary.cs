﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MK.Demo.Utility;
using Microsoft.Reporting.WinForms;
using MK.Demo.Logic;

namespace MK_DEMO.Destop.FormFunction
{
    public partial class frmRPT_MonthlyBill_Summary : Form
    {
        private List<string> liCustID = new List<string>();

        public frmRPT_MonthlyBill_Summary()
        {
            InitializeComponent();
        }

        private void frmRPT_MonthlyBill_Summary_Load(object sender, EventArgs e)
        {
            this.InitComboBox();
            //this.BindReport();
        }

        private void InitComboBox()
        {
     
        }

        private void button1_Click(object sender, EventArgs e)
        {
          
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.BindReport();
        }


        private void BindReport()
        {
            var strORG = Global.ORG_CODE;
            string exeFolder = Application.StartupPath;
            string reportPath = System.IO.Path.Combine(exeFolder, @"RPT\RPT_MONTHLY_BILL_SUMMARY.rdlc");
            var data = new logicCust(Global.IsTestMode).GetRpt_RPT_MonthlyBill_Summary_Data(this.dteReportEndDate.Value, strORG);

            var binding = new BindingSource();
            binding.DataSource = data;
            this.reportViewer2.Reset();

            this.reportViewer2.LocalReport.ReportPath = reportPath;
            this.reportViewer2.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", binding));
            this.reportViewer2.RefreshReport();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

     



    }
}
