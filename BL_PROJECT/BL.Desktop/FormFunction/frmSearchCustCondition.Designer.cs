﻿namespace MK_DEMO.Destop.FormFunction
{
    partial class frmSearchCustCondition
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtCUST_CNAME = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.txtID_NUMBER2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtID_NUMBER = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPASSPORT = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtSALES = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCONTRACT_COUNT = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.drpCONTRACT_COUNT_CONDITION = new System.Windows.Forms.ComboBox();
            this.btnReset = new System.Windows.Forms.Button();
            this.chkUnSendMail = new System.Windows.Forms.CheckBox();
            this.chkIsShowOrderEnd = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.btnSearch.Location = new System.Drawing.Point(134, 277);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(110, 48);
            this.btnSearch.TabIndex = 0;
            this.btnSearch.Text = "查詢";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtCUST_CNAME
            // 
            this.txtCUST_CNAME.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.txtCUST_CNAME.Location = new System.Drawing.Point(102, 22);
            this.txtCUST_CNAME.Name = "txtCUST_CNAME";
            this.txtCUST_CNAME.Size = new System.Drawing.Size(264, 27);
            this.txtCUST_CNAME.TabIndex = 3;
            this.txtCUST_CNAME.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCUST_CNAME_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.label1.Location = new System.Drawing.Point(11, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 19);
            this.label1.TabIndex = 2;
            this.label1.Text = "客戶姓名";
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.btnCancel.Location = new System.Drawing.Point(255, 277);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(110, 48);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "取消";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // txtID_NUMBER2
            // 
            this.txtID_NUMBER2.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.txtID_NUMBER2.Location = new System.Drawing.Point(102, 88);
            this.txtID_NUMBER2.Name = "txtID_NUMBER2";
            this.txtID_NUMBER2.Size = new System.Drawing.Size(264, 27);
            this.txtID_NUMBER2.TabIndex = 6;
            this.txtID_NUMBER2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtID_NUMBER2_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.label2.Location = new System.Drawing.Point(11, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 19);
            this.label2.TabIndex = 5;
            this.label2.Text = "統一編號";
            // 
            // txtID_NUMBER
            // 
            this.txtID_NUMBER.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.txtID_NUMBER.Location = new System.Drawing.Point(102, 55);
            this.txtID_NUMBER.Name = "txtID_NUMBER";
            this.txtID_NUMBER.Size = new System.Drawing.Size(264, 27);
            this.txtID_NUMBER.TabIndex = 8;
            this.txtID_NUMBER.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtID_NUMBER_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.label3.Location = new System.Drawing.Point(11, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 19);
            this.label3.TabIndex = 7;
            this.label3.Text = "身分證字號";
            // 
            // txtPASSPORT
            // 
            this.txtPASSPORT.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.txtPASSPORT.Location = new System.Drawing.Point(102, 121);
            this.txtPASSPORT.Name = "txtPASSPORT";
            this.txtPASSPORT.Size = new System.Drawing.Size(264, 27);
            this.txtPASSPORT.TabIndex = 10;
            this.txtPASSPORT.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPASSPORT_KeyDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.label4.Location = new System.Drawing.Point(11, 124);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 19);
            this.label4.TabIndex = 9;
            this.label4.Text = "護照號碼";
            // 
            // txtSALES
            // 
            this.txtSALES.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.txtSALES.Location = new System.Drawing.Point(102, 154);
            this.txtSALES.Name = "txtSALES";
            this.txtSALES.Size = new System.Drawing.Size(264, 27);
            this.txtSALES.TabIndex = 12;
            this.txtSALES.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSALES_KeyDown);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.label5.Location = new System.Drawing.Point(11, 157);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 19);
            this.label5.TabIndex = 11;
            this.label5.Text = "所屬業務";
            // 
            // txtCONTRACT_COUNT
            // 
            this.txtCONTRACT_COUNT.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.txtCONTRACT_COUNT.Location = new System.Drawing.Point(255, 187);
            this.txtCONTRACT_COUNT.Name = "txtCONTRACT_COUNT";
            this.txtCONTRACT_COUNT.Size = new System.Drawing.Size(111, 27);
            this.txtCONTRACT_COUNT.TabIndex = 14;
            this.txtCONTRACT_COUNT.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCONTRACT_COUNT_KeyDown);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.label6.Location = new System.Drawing.Point(11, 190);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 19);
            this.label6.TabIndex = 13;
            this.label6.Text = "合約數";
            // 
            // drpCONTRACT_COUNT_CONDITION
            // 
            this.drpCONTRACT_COUNT_CONDITION.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.drpCONTRACT_COUNT_CONDITION.FormattingEnabled = true;
            this.drpCONTRACT_COUNT_CONDITION.Items.AddRange(new object[] {
            "",
            ">",
            ">=",
            "=",
            "<",
            "<="});
            this.drpCONTRACT_COUNT_CONDITION.Location = new System.Drawing.Point(102, 187);
            this.drpCONTRACT_COUNT_CONDITION.Name = "drpCONTRACT_COUNT_CONDITION";
            this.drpCONTRACT_COUNT_CONDITION.Size = new System.Drawing.Size(103, 27);
            this.drpCONTRACT_COUNT_CONDITION.TabIndex = 15;
            // 
            // btnReset
            // 
            this.btnReset.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.btnReset.Location = new System.Drawing.Point(13, 277);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(110, 48);
            this.btnReset.TabIndex = 16;
            this.btnReset.Text = "清空查詢條件";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // chkUnSendMail
            // 
            this.chkUnSendMail.AutoSize = true;
            this.chkUnSendMail.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.chkUnSendMail.Location = new System.Drawing.Point(11, 221);
            this.chkUnSendMail.Name = "chkUnSendMail";
            this.chkUnSendMail.Size = new System.Drawing.Size(178, 23);
            this.chkUnSendMail.TabIndex = 17;
            this.chkUnSendMail.Text = "尚未發送推知信之客戶";
            this.chkUnSendMail.UseVisualStyleBackColor = true;
            // 
            // chkIsShowOrderEnd
            // 
            this.chkIsShowOrderEnd.AutoSize = true;
            this.chkIsShowOrderEnd.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.chkIsShowOrderEnd.Location = new System.Drawing.Point(11, 249);
            this.chkIsShowOrderEnd.Name = "chkIsShowOrderEnd";
            this.chkIsShowOrderEnd.Size = new System.Drawing.Size(178, 23);
            this.chkIsShowOrderEnd.TabIndex = 18;
            this.chkIsShowOrderEnd.Text = "不排除合約到期之客戶";
            this.chkIsShowOrderEnd.UseVisualStyleBackColor = true;
            // 
            // frmSearchCustCondition
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(378, 339);
            this.Controls.Add(this.chkIsShowOrderEnd);
            this.Controls.Add(this.chkUnSendMail);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.drpCONTRACT_COUNT_CONDITION);
            this.Controls.Add(this.txtCONTRACT_COUNT);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtSALES);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtPASSPORT);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtID_NUMBER);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtID_NUMBER2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.txtCUST_CNAME);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSearch);
            this.Font = new System.Drawing.Font("微軟正黑體", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSearchCustCondition";
            this.Text = "查詢";
            this.Load += new System.EventHandler(this.frmSearchCustCondition_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox txtCUST_CNAME;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TextBox txtID_NUMBER2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtID_NUMBER;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtPASSPORT;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtSALES;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtCONTRACT_COUNT;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox drpCONTRACT_COUNT_CONDITION;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.CheckBox chkUnSendMail;
        private System.Windows.Forms.CheckBox chkIsShowOrderEnd;
    }
}