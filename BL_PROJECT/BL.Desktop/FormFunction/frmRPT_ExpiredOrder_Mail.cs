﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MK.Demo.Utility;
using MK.Demo.Logic;
using System.IO;
using MK.Demo.Model;



namespace MK_DEMO.Destop.FormFunction
{
    public partial class frmRPT_ExpiredOrder_Mail : MK_DEMO.Destop.BaseForm
    {
        private List<FILE_INFO> dataOutput;
        public frmRPT_ExpiredOrder_Mail(List<FILE_INFO> data = null)
        {
            InitializeComponent();

            this.dgv_header.AutoGenerateColumns = false;
            this.dgv_header.MultiSelect = false;
            this.dgv_header.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            
            this.dataOutput = data;
        }

        private void frmRPT_ExpiredOrder_Mail_Load(object sender, EventArgs e)
        {
            if (this.dataOutput != null)
            {
                this.lblBatchNo.Visible = false;
                this.cboBatchNo.Visible = false;
                this.dgv_header.DataBindByList(this.dataOutput);
                this.dgv_header.Columns["JOB_STATUS_DESC"].Visible = false;
            }
            else
            {
                //  透過批號選取資料做發送動作, 前端會單獨產生PDF並取得一組批號, 在這邊會透過批號撈取資料
                this.lblBatchNo.Visible = true;
                this.cboBatchNo.Visible = true;
                //  Bind Batch No
                this.cboBatchNo.DataBindByList(
                    data: new logicCust(Global.IsTestMode).GetAllOrderEndBatchNoByUser(Global.str_UserName),
                    display: "JOB_ID",
                    value: "JOB_ID",
                    top: null
                    );
                this.BindSalesList();
                this.BindDataByBatchNo();
                this.dgv_header.Columns["JOB_STATUS_DESC"].Visible = true;
            }
        }

        private void BindSalesList()
        {
            var liBatch = new List<string>();
            liBatch.Add(this.cboBatchNo.Text.ConvertToString());
            var data = new logicCust(Global.IsTestMode).GetSalesInfoByBatchNo(liBatch);
            var dataSales = new List<ComboBoxData<string>>();

            data
                .Select(x => new { x.IB_CODE, x.SALES_NAME })
                .Distinct()
                .ToList()
                .ForEach(x =>
                {
                    var strDisplay = x.IB_CODE.ConvertToString() + "-" + x.SALES_NAME.ConvertToString();
                    var strValue = string.Empty;
                    data
                        .Where(y => y.IB_CODE == x.IB_CODE && y.SALES_NAME == x.SALES_NAME)
                        .ToList()
                        .ForEach(y =>
                        {
                            if (y.CUST_ID != null)
                            {
                            strValue += strValue == string.Empty ? "" : ",";
                                strValue += y.CUST_ID.Value.ConvertToString();
                            }
                        });
                    dataSales.Add(new ComboBoxData<string>()
                    {
                        Display = strDisplay,
                        Value = strValue
                    });
                });

            //  Bind Batch No
            this.cboSales.DataBindByList(
                data: dataSales,
                display: "Display",
                value: "Value",
                top: new ComboBoxData<string>() { Display = string.Empty, Value = string.Empty }
                );
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            
            var selection = this.dgv_header.Rows.Cast<DataGridViewRow>().Where(x => (bool?)x.Cells["check"].Value == true).ToList();
            var strErrList = string.Empty;
            if (selection.Count() > 0)
            {
                var dataUpdateFlag = new List<BL_ORDEREND_MAIL_LOG>();
                //  2019/10/03 改為不排除
                ////  排除聯名戶, 然後Group CUST_ID
                selection
                    //.Where(x => x.Cells["CUST_NAME2"].Value.ConvertToString().Trim() == string.Empty)
                    .Select(x => new
                    {
                        CUST_ID = x.Cells["CUST_ID"].Value.ConvertToString()
                    })
                    .Where(x => x.CUST_ID.ConvertToString().Trim() != string.Empty)
                    .Distinct()
                    .ForEach(x =>
                    {
                        var data = selection.Where(y => y.Cells["CUST_ID"].Value.ConvertToString() == x.CUST_ID)
                            .ToList();
                        if (data.Count() > 0)
                        {
                            var strOrderEnd_Month = data[0].Cells["ORDEREND_MONTH"].Value.ConvertToString();
                            var strMailSubject = "百麗金融" + strOrderEnd_Month.Substring(0, 4) + "年" + strOrderEnd_Month.Substring(4, 2) + "月份到期合約通知函";
                            var strCust = data[0].Cells["CUST_NAME"].Value.ConvertToString() + (data[0].Cells["CUST_NAME2"].Value.ConvertToString().Trim() != string.Empty ? "/" + data[0].Cells["CUST_NAME2"].Value.ConvertToString() : string.Empty);
                            var strMailPws = data[0].Cells["CUST_NAME2"].Value.ConvertToString().Trim() != string.Empty ? "聯名戶ID" : "身分證字號";
                            var strMailBody = "   親愛的  " + strCust + "<br/><br/>" +
                                "　　隨信附上" + strOrderEnd_Month.Substring(4, 2) + "月份電子合約到期通知函，請立即打開<span style='color:red;text-decoration:underline;'>附件</span>，檢視相關資訊。<br/>" +
                                "    如有任何疑問，請與您承辦的服務人員聯繫。<br/>"+
                                "　　非常感謝您使用電子通知函，為地球延續生存盡一份心力。<br/><br/>" +
                                "    <span style='background-color:#E6E6E6;'>開啟步驟</span> " + "<br/>" +
                                "    1.	開啟此封信件附加檔案。<br/>" +
                                "    2.	輸入您的個人資料(身分證字號或本公司提供之帳號) ，查看內容。<br/>" +
                                "    備註：另提醒您，已加密文件開啟密碼之英文字母皆需輸入大寫。<br/><br/>" +
                                "    客戶服務部" +
                                "    Best Leader Markets PTY Ltd";

                            List<string> strMailTo = new List<string>();
                            var strMailAddress = data[0].Cells["EMAIL"].Value.ConvertToString().Trim();
                            strMailTo.Add(strMailAddress);

                            if (data[0].Cells["CUST_NAME2"].Value.ConvertToString().Trim() != string.Empty)
                            {
                                //  聯名戶, 要一同收到Mail
                                if (data[0].Cells["EMAIL_2"].Value.ConvertToString().Trim() != string.Empty)
                                {
                                    strMailTo.Add(data[0].Cells["EMAIL_2"].Value.ConvertToString().Trim());
                                }
                            }

                            if (this.chkIsSendTest.Checked)
                            {
                                //  發送測試信
                                strMailBody = "系統測試信(Mail To:" + data[0].Cells["EMAIL"].Value.ConvertToString().Trim() + ")<br/>" + strMailBody;
                                strMailTo = new List<string>();
                                GetConfigValueByKey("MailTo_BillPDF_TestingUser")
                                    .Split(new string[] { ";" }, StringSplitOptions.None)
                                    .ToList()
                                    .ForEach(y =>
                                    {
                                        strMailTo.Add(y);
                                    });
                            }

                            List<string> strFileNames = new List<string>();
                            data.ForEach(y =>
                            {
                                strFileNames.Add(y.Cells["FILE_PATH"].Value.ConvertToString() + @"\" + y.Cells["FILE_NAME"].Value.ConvertToString());
                            });


                            if (strMailAddress != string.Empty)
                            {
                                if (SendMail(strMailTo, strMailSubject, strMailBody, strFileNames, false))
                                {
                                    if (this.cboBatchNo.Visible == true)
                                    {
                                        dataUpdateFlag.Add(new BL_ORDEREND_MAIL_LOG()
                                        {
                                            JOB_ID = data[0].Cells["JOB_ID"].Value.ConvertToString(),
                                            CUST_ID = data[0].Cells["CUST_ID"].Value.IsNumeric() ? decimal.Parse(data[0].Cells["CUST_ID"].Value.ConvertToString()) : (decimal?)null,
                                            ORDERNO_DESC = data[0].Cells["ORDERNO_DESC"].Value.ConvertToString()
                                        });
                                    }
                                }
                                else
                                {
                                    strErrList += strErrList == string.Empty ? "" : "、";
                                    strErrList += strCust;
                                }
                            }
                            else
                            {
                                strErrList += strErrList == string.Empty ? "" : "、";
                                strErrList += strCust;
                            }
                        }
                    });

                
                if (this.cboBatchNo.Visible && dataUpdateFlag.Count() > 0 && this.chkIsSendTest.Checked == false)
                {
                    if (new logicCust(Global.IsTestMode).UpdateOrderEndMailSendFlag(dataUpdateFlag, Global.str_UserName))
                    {
                        this.BindDataByBatchNo();
                    }
                }

                var strMSG = "Mail發送完畢";
                if (selection.Count() != dataUpdateFlag.Count())
                {
                    strMSG = "Mail發送完畢，部分郵件發送失敗, 請確認收件者資訊是否正確" + Environment.NewLine + Environment.NewLine +
                        "發送失敗名單如下：" + Environment.NewLine +
                        strErrList;
                }
                MessageBox.Show(strMSG);
            }
            else
            {
                MessageBox.Show("請選擇要發送的資料");
            }
        }


        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            this.dgv_header.Rows.Cast<DataGridViewRow>()
                .ForEach(x =>
                {
                    x.Cells["check"].Value = this.checkBox1.Checked;
                });
        }

        private void dgv_header_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                if ((bool?)this.dgv_header.Rows[e.RowIndex].Cells["check"].Value == true)
                {
                    this.dgv_header.Rows[e.RowIndex].Cells["check"].Value = false;
                }
                else
                {
                    this.dgv_header.Rows[e.RowIndex].Cells["check"].Value = true;
                }
            }
        }

        private void cboBatchNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.BindDataByBatchNo();
        }

        private void cboSales_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.BindDataByBatchNo();
        }

        private void cboContractExpiredType_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.BindDataByBatchNo();
        }

        private void BindDataByBatchNo()
        {
            var data = new logicCust(Global.IsTestMode).GetOrderEndMailDataByBatchNo(this.cboBatchNo.Text);

            if (this.cboSales.SelectedValue.ConvertToString().Trim() != string.Empty)
            {
                var liCustID = this.cboSales.SelectedValue.ConvertToString().Trim()
                                        .Split(new string[] { "," }, StringSplitOptions.None)
                                        .ToList();

                data = data.Where(x => liCustID.Where(y => (x.CUST_ID == null ? "" : x.CUST_ID.Value.ConvertToString()) == y).ToList().Count() > 0).ToList();
            }

            if (this.cboContractExpiredType.Text.ConvertToString().Trim() != string.Empty)
            {
                data = data.Where(x => x.ORDER_TYPE_DESC.ConvertToString() == this.cboContractExpiredType.Text.ConvertToString().Trim()).ToList();

            }
            
            //

            this.dgv_header.DataBindByList(data);
        }

       
    }
}
