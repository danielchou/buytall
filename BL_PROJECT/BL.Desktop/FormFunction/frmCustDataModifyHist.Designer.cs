﻿namespace MK_DEMO.Destop.FormFunction
{
    partial class frmCustDataModifyHist
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btnClose = new System.Windows.Forms.Button();
            this.EDIT_BY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EDIT_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EDIT_OLD_DESC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EDIT_NEW_DESC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeight = 25;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EDIT_BY,
            this.EDIT_DATE,
            this.EDIT_OLD_DESC,
            this.EDIT_NEW_DESC});
            this.dataGridView1.Location = new System.Drawing.Point(12, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("新細明體", 11F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.RoyalBlue;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Yellow;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView1.RowHeadersWidth = 25;
            this.dataGridView1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridView1.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(226)))), ((int)(((byte)(239)))));
            this.dataGridView1.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridView1.RowTemplate.Height = 250;
            this.dataGridView1.Size = new System.Drawing.Size(981, 429);
            this.dataGridView1.TabIndex = 143;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.btnClose.Location = new System.Drawing.Point(734, 452);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(259, 48);
            this.btnClose.TabIndex = 144;
            this.btnClose.Text = "關閉";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // EDIT_BY
            // 
            this.EDIT_BY.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.EDIT_BY.DataPropertyName = "EDIT_BY";
            this.EDIT_BY.HeaderText = "修改人";
            this.EDIT_BY.Name = "EDIT_BY";
            this.EDIT_BY.ReadOnly = true;
            this.EDIT_BY.Width = 80;
            // 
            // EDIT_DATE
            // 
            this.EDIT_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.EDIT_DATE.DataPropertyName = "EDIT_DATE";
            this.EDIT_DATE.HeaderText = "修改時間";
            this.EDIT_DATE.Name = "EDIT_DATE";
            this.EDIT_DATE.ReadOnly = true;
            this.EDIT_DATE.Width = 120;
            // 
            // EDIT_OLD_DESC
            // 
            this.EDIT_OLD_DESC.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.EDIT_OLD_DESC.DataPropertyName = "EDIT_OLD_DESC_SHOW";
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.EDIT_OLD_DESC.DefaultCellStyle = dataGridViewCellStyle1;
            this.EDIT_OLD_DESC.HeaderText = "修改前";
            this.EDIT_OLD_DESC.Name = "EDIT_OLD_DESC";
            this.EDIT_OLD_DESC.ReadOnly = true;
            this.EDIT_OLD_DESC.Width = 360;
            // 
            // EDIT_NEW_DESC
            // 
            this.EDIT_NEW_DESC.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.EDIT_NEW_DESC.DataPropertyName = "EDIT_NEW_DESC_SHOW";
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.EDIT_NEW_DESC.DefaultCellStyle = dataGridViewCellStyle2;
            this.EDIT_NEW_DESC.HeaderText = "修改後";
            this.EDIT_NEW_DESC.Name = "EDIT_NEW_DESC";
            this.EDIT_NEW_DESC.ReadOnly = true;
            this.EDIT_NEW_DESC.Width = 360;
            // 
            // frmCustDataModifyHist
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.ClientSize = new System.Drawing.Size(1005, 512);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.dataGridView1);
            this.Font = new System.Drawing.Font("新細明體", 11F);
            this.Name = "frmCustDataModifyHist";
            this.Text = "客戶資料修改紀錄";
            this.Load += new System.EventHandler(this.frmCustDataModifyHist_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.DataGridViewTextBoxColumn EDIT_BY;
        private System.Windows.Forms.DataGridViewTextBoxColumn EDIT_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn EDIT_OLD_DESC;
        private System.Windows.Forms.DataGridViewTextBoxColumn EDIT_NEW_DESC;
    }
}
