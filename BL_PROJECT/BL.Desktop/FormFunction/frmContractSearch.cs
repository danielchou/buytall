﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using MK.Demo.Utility;
using MK.Demo.Model;
using MK.Demo.Logic;
using System.Threading.Tasks;
using System.Linq;
using static MK.Demo.Data.enCommon;

namespace MK_DEMO.Destop.FormFunction
{
    public partial class frmContractSearch : MK_DEMO.Destop.BaseForm
    {
        public enFormStatus status;


        public frmContractSearch()
        {
            InitializeComponent();

            this.dgv_header.AutoGenerateColumns = false;
            this.dgv_header.MultiSelect = false;
            this.dgv_header.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

     


        private void frmContractDetail_Load(object sender, EventArgs e)
        {
            switch (Global.str_UserName)
            {
                case "HK_ADMIN":
                case "MAGGIELIU":
                case "MONIQUECHOY":
                case "OLIVIAHUNG":
                    this.dgv_header.Columns["ModifyContract"].Visible = false;
                    btnBatchDepositOut.Visible = false;
                    break;
            }
            if (Global.UserRole.ConvertToString() == "50")
            {
                //  HK Admin
                this.dgv_header.Columns["ModifyContract"].Visible = false;
                btnBatchDepositOut.Visible = false;
            }

            this.CleacCondition();
            this.BindData();
        }

        private void BindData()
        {
            this.picLoder.Visible = true;
            this.picLoder.BringToFront();
            if (this.backgroundWorker1.IsBusy == false)
            {
                var strParam = new string[]
                {
                    this.txtOrderNo.Text.Trim(),
                    this.txtCustomerName.Text.Trim(),
                    this.dteEND_DATE_S.Text.Trim(),
                    this.dteEND_DATE_E.Text.Trim(),
                    this.txtCustID.Text.Trim(),
                    this.dteORDER_END_S.Text.Trim(),
                    this.dteORDER_END_E.Text.Trim()
                };

                this.backgroundWorker1.RunWorkerAsync(strParam);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            status = enFormStatus.Cancel;
            this.Close();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            System.Threading.Thread.Sleep(50);
            var strPara = e.Argument as string[];
            if (strPara != null)
            {
                if (strPara.Length >= 7)
                {
                    DateTime dtTry;
                    var liOrderNo = strPara[0].Split(new string[] { ",", "/", "-", "." }, StringSplitOptions.None).ToList();
                    var strCustName = strPara[1];
                    DateTime? END_DATE_S = (DateTime?)null;
                    DateTime? END_DATE_E = (DateTime?)null;
                    DateTime? ORDER_END_S = (DateTime?)null;
                    DateTime? ORDER_END_E = (DateTime?)null;
                    if (DateTime.TryParse(strPara[2], out dtTry))
                    {
                        END_DATE_S = dtTry;
                    }
                    if (DateTime.TryParse(strPara[3],out dtTry ))
                    {
                        END_DATE_E = dtTry;
                    }
                    var strCustID = strPara[4];
                    if (DateTime.TryParse(strPara[5],out dtTry))
                    {
                        ORDER_END_S = dtTry;
                    }
                    if (DateTime.TryParse(strPara[6], out dtTry))
                    {
                        ORDER_END_E = dtTry;
                    }

                    var strORG = Global.ORG_CODE;
                    //  Param
                    //  0   Order No
                    //  1   CustName
                    //  2   END_DATE Start
                    //  3   END_DATE End
                    //  4   CUST ID
                    //  5   ORDER_END Start
                    //  6   ORDER_END End
                    var data = base.Cust.GetDataByOrderNo
                        (
                            liOrderNo, 
                            strCustName, 
                            END_DATE_S, 
                            END_DATE_E, 
                            strCustID,
                            ORDER_END_S,
                            ORDER_END_E,
                            strORG
                        );

                    if (this.chk_has_order_end.Checked == false)
                    {
                        data = data.Where(p => p.ORDER_END == null ||  p.ORDER_END > System.DateTime.Now).ToList();
                    }

                    e.Result = data;
                }
            }
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //this.dgv_header.DataSource = new List<BL_ORDER>();
            this.dgv_header.DataClean();
            var data = e.Result as List<BL_ORDER>;
            if (data != null)
            {
                //this.dgv_header.DataSource = data;
                this.dgv_header.DataBindByList(data);
                
                this.dgv_header.Rows.Cast<DataGridViewRow>().ForEach(x =>
                {
                    x.Cells["DoDepositOut"].Value = "出金作業";
                    x.Cells["ModifyContract"].Value = "合約修改";
                    DateTime dtTry;
                    if (DateTime.TryParse(x.Cells["LAST_UPDATE_DATE"].Value.ConvertToString().Trim(), out dtTry))
                    {
                        if (dtTry.ToString("yyyy/MM/dd") == DateTime.Now.ToString("yyyy/MM/dd"))
                        {
                            x.DefaultCellStyle.ForeColor = Color.Red;
                        }
                    }


                    x.DefaultCellStyle.BackColor = Color.White;
                    DateTime? dtMax = (DateTime?)null;
                    if (DateTime.TryParse(x.Cells["END_DATE"].Value.ConvertToString(), out dtTry))
                    {
                        if (dtMax == null) dtMax = dtTry;
                        else if (dtMax.Value < dtTry) dtMax = dtTry;
                    }
                    if (DateTime.TryParse(x.Cells["ext_date"].Value.ConvertToString(), out dtTry))
                    {
                        if (dtMax == null) dtMax = dtTry;
                        else if (dtMax.Value < dtTry) dtMax = dtTry;
                    }
                    if (DateTime.TryParse(x.Cells["ORDER_END"].Value.ConvertToString(), out dtTry))
                    {
                        if (dtMax == null) dtMax = dtTry;
                        else if (dtMax.Value < dtTry) dtMax = dtTry;
                    }

                    if (dtMax != null)
                    {
                        if (DateTime.Parse(dtMax.Value.ToString("yyyy/MM/dd")) < DateTime.Parse(DateTime.Now.ToString("yyyy/MM/dd")))
                        {
                            x.DefaultCellStyle.BackColor = Color.FromKnownColor(KnownColor.Control);
                        }
                    }

                });

                this.SetSeq();
            }
            this.picLoder.Visible = false;
        }

        private void dgv_header_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                //Parallel.ForEach(this.dgv_header.Rows.Cast<DataGridViewRow>(), x =>
                //{
                //    x.Cells["check"].Value = false;
                //});
                if ((bool?)this.dgv_header.Rows[e.RowIndex].Cells["check"].Value == true)
                {
                    this.dgv_header.Rows[e.RowIndex].Cells["check"].Value = false;
                }
                else
                {
                    this.dgv_header.Rows[e.RowIndex].Cells["check"].Value = true;
                }
                 

                if (this.dgv_header.Columns[e.ColumnIndex].Name == "DoDepositOut")
                {
                    //  出金作業
                    var strOrderNo = this.dgv_header.Rows[e.RowIndex].Cells["ORDER_NO"].Value.ConvertToString();

                    var frm = new frmDepositOut(strOrderNo, true) { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
                    frm.ShowDialog();
                    frm.Dispose();
                    frm = null;
                }
                if (this.dgv_header.Columns[e.ColumnIndex].Name == "ModifyContract")
                {
                    //  合約修改
                    var strOrderID = this.dgv_header.Rows[e.RowIndex].Cells["ORDER_ID"].Value.ConvertToString();
                    var strCustID = this.dgv_header.Rows[e.RowIndex].Cells["CUST_ID"].Value.ConvertToString();
                    var frm = new frmAddContract(strCustID, strOrderID) { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
                    frm.ShowDialog();
                    frm.Dispose();
                    frm = null;

                    this.BindData();
                
                }
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.BindData();
        }

       
        private void btnCleanCondition_Click(object sender, EventArgs e)
        {
            this.CleacCondition();
        }

        private void CleacCondition()
        {
            this.txtCustomerName.Text = string.Empty;
            this.txtOrderNo.Text = string.Empty;


            this.dteEND_DATE_S.Value = DateTime.Now;
            this.dteEND_DATE_S.Text = string.Empty;
            this.dteEND_DATE_S.Format = DateTimePickerFormat.Custom;
            this.dteEND_DATE_S.CustomFormat = " ";

            this.dteEND_DATE_E.Value = DateTime.Now;
            this.dteEND_DATE_E.Text = string.Empty;
            this.dteEND_DATE_E.Format = DateTimePickerFormat.Custom;
            this.dteEND_DATE_E.CustomFormat = " ";

            this.txtCustID.Text = string.Empty;

            
            this.dteORDER_END_S.Value = DateTime.Now;
            this.dteORDER_END_S.Text = string.Empty;
            this.dteORDER_END_S.Format = DateTimePickerFormat.Custom;
            this.dteORDER_END_S.CustomFormat = " ";

            this.dteORDER_END_E.Value = DateTime.Now;
            this.dteORDER_END_E.Text = string.Empty;
            this.dteORDER_END_E.Format = DateTimePickerFormat.Custom;
            this.dteORDER_END_E.CustomFormat = " ";
        }

        private void dgv_header_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            Parallel.ForEach(this.dgv_header.Rows.Cast<DataGridViewRow>(), x =>
            {
                x.Cells["DoDepositOut"].Value = "出金作業";
                x.Cells["ModifyContract"].Value = "合約修改";

            });
        }

        private void btnBatchDepositOut_Click(object sender, EventArgs e)
        {
            var selectionItem = this.dgv_header.Rows.Cast<DataGridViewRow>()
                .Where(x => (bool?)x.Cells["check"].Value == true).ToList();

            if (selectionItem.Count() > 0)
            {
                //  只能選取相同的客戶
                var strFirstCUST_CNAME = selectionItem[0].Cells["CUST_CNAME"].Value.ConvertToString().Trim();
                var strFirstCUST_CNAME2 = selectionItem[0].Cells["CUST_CNAME2"].Value.ConvertToString().Trim();
                var strFirstCurrency = selectionItem[0].Cells["CURRENCY"].Value.ConvertToString().Trim();
                var IsAllSame = true;
                selectionItem.ForEach(x =>
                {
                    if (x.Cells["CUST_CNAME"].Value.ConvertToString().Trim() != strFirstCUST_CNAME ||
                        x.Cells["CUST_CNAME2"].Value.ConvertToString().Trim() != strFirstCUST_CNAME2 ||
                        x.Cells["CURRENCY"].Value.ConvertToString().Trim() != strFirstCurrency)
                    {
                        IsAllSame = false;
                    }
                });
                if (IsAllSame == false)
                {
                    MessageBox.Show("請選擇相同客戶且相同幣別的資料");
                    return;
                }

                var selection = new List<decimal>();

                selectionItem.ForEach(x =>
                {
                    selection.Add(decimal.Parse(x.Cells["ORDER_ID"].Value.ConvertToString()));
                });

                var frm = new AddDepositOut_Batch(selection) { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
                frm.ShowDialog();
                frm.Dispose();
                frm = null;

                this.BindData();
            }
            else
            {
                MessageBox.Show("請選擇要批次出金的資料");
            }
        }

        private void chkALL_CheckedChanged(object sender, EventArgs e)
        {
            Parallel.ForEach(this.dgv_header.Rows.Cast<DataGridViewRow>(), x =>
            {
                x.Cells["check"].Value = this.chkALL.Checked;
            });
        }
        

        private void txtOrderNo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Tab || e.KeyCode == Keys.Enter)
            {
                this.BindData();
            }
        }

     
        private void txtCustomerName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Tab || e.KeyCode == Keys.Enter)
            {
                this.BindData();
            }
        }

        private void dteEND_DATE_S_ValueChanged(object sender, EventArgs e)
        {
            (sender as DateTimePicker).Format = DateTimePickerFormat.Long;
        }

        private void dteEND_DATE_E_ValueChanged(object sender, EventArgs e)
        {
            (sender as DateTimePicker).Format = DateTimePickerFormat.Long;
        }

        private void dteORDER_END_S_ValueChanged(object sender, EventArgs e)
        {
            (sender as DateTimePicker).Format = DateTimePickerFormat.Long;
        }

        private void dteORDER_END_E_ValueChanged(object sender, EventArgs e)
        {
            (sender as DateTimePicker).Format = DateTimePickerFormat.Long;
        }

        private void dgv_header_Sorted(object sender, EventArgs e)
        {
            this.SetSeq();
        }

        private void SetSeq()
        {
            int i = 1;
            this.dgv_header.Rows.Cast<DataGridViewRow>().ForEach(x =>
            {
                x.Cells["SEQ"].Value = i.ToString();
                i += 1;
            });
        }


    }
}
