﻿using MK.Demo.Logic;
using MK.Demo.Model;
using MK_DEMO.Destop.FormFunction;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MK.Demo.Utility;

namespace MK_DEMO.Destop.FormFunction
{
    public partial class frmSystemVersionInfo : MK_DEMO.Destop.BaseForm
    {
        private decimal MAX_MSG_SN;
        public frmSystemVersionInfo()
        {
            InitializeComponent();

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            //  寫Log 
            base.Cust.UpdateMSG_CheckLog(this.MAX_MSG_SN, Global.BL_BACKEND_LOGIN_LOG_SN);

            this.Close();
        }

        private void frmSystemVersionInfo_Load(object sender, EventArgs e)
        {
            this.BindVersionInfo();
        }

        private void BindVersionInfo()
        {
            var dataSystemVersion = base.Cust.GetSystemVersionData();
            dataSystemVersion = dataSystemVersion
                .OrderByDescending(x => x.START_DATE)
                .ToList();
            for (int i = 0; i < dataSystemVersion.Count(); i++)
            {
                var color = new Color();
                if (i == 0)
                {
                    color = Color.Red;
                }
                this.writeMsg("版本 " + (dataSystemVersion[i].START_DATE == null ? string.Empty : dataSystemVersion[i].START_DATE.Value.ToString("yyyy/MM/dd")), color);
                this.writeMsg(dataSystemVersion[i].MSG_DATA.ConvertToString(), color);
                this.writeMsg(string.Empty, color);
            }

            this.MAX_MSG_SN = dataSystemVersion.Max(x => x.BL_MSG_DATA_SN);
        }

        private void writeMsg(string msg, Color color = new Color())
        {
            this.richTextBox1.SelectionColor = color;
            this.richTextBox1.AppendText(msg + Environment.NewLine);
        }
    }
}
