﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MK_DEMO.Destop.FormFunction
{
    public partial class frmAdminSentTxn : BaseForm
    {
        public frmAdminSentTxn()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
           
            var data = base.Cust.GetUnExpiredOrderDatabyOrderID(this.textBox1.Text.Trim());
            if (data.Count > 0)
            {
                label2.Text = data.FirstOrDefault().ORDER_ID.ToString();
                lb_custname.Text = data.FirstOrDefault().CUST_CNAME;
                lb_salesname.Text = data.FirstOrDefault().SALES_NAME;
                lb_formdate.Text = data.FirstOrDefault().FROM_DATE.GetValueOrDefault().ToString("yyyy/MM/dd");
                lb_years.Text = data.FirstOrDefault().YEAR.ToString();


            }
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
           
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            if (label2.Text.Trim() != "")
            {
                base.Cust.SaveFREE_TXN(decimal.Parse(label2.Text.Trim()), comboBox1.Text, comboBox2.Text, comboBox3.Text, textBox2.Text, this.dteBirthday.Value, this.txtRemark.Text, this.comboBox4.Text);

                MessageBox.Show("Success");
            }
            else
            {
                MessageBox.Show("請先查一筆合約");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
