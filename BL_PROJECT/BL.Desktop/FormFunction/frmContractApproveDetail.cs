﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using MK.Demo.Utility;
using MK.Demo.Model;
using MK.Demo.Logic;
using System.Threading.Tasks;
using System.Linq;
using static MK.Demo.Data.enCommon;
using Microsoft.Reporting.WinForms;
using System.IO;
using System.Data.OracleClient;
using MK.Demo.Data;

namespace MK_DEMO.Destop.FormFunction
{
    public partial class frmContractApproveDetail : MK_DEMO.Destop.BaseForm
    {
        public string _strOrderNo { get; set; }
        public string _strCustomerName { get; set; }
        public string _strCboApproveStatus { get; set; }
       
        public frmContractApproveDetail(string orderNo, string customerName, string approveStatus)
        {
            this._strOrderNo = orderNo;
            this._strCustomerName = customerName;
            this._strCboApproveStatus = approveStatus;

            InitializeComponent();

            Global.ORG_CODE = "TW1";
            this.BindData();
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BindData()
        {
            this.picLoder.Visible = true;
            this.picLoder.BringToFront();
            if (this.backgroundWorker1.IsBusy == false)
            {
                var strParam = new string[]
                {
                    this._strOrderNo,
                    this._strCustomerName,
                    this._strCboApproveStatus
                };

                this.backgroundWorker1.RunWorkerAsync(strParam);
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            System.Threading.Thread.Sleep(50);
            var strPara = e.Argument as string[];
            if (strPara != null)
            {
                if (strPara.Length >= 3)
                {
                    var strORG = Global.ORG_CODE;
                    var data = base.Cust.GetUnApproveDataByOrderNo_NewCustomer(strPara[0], strPara[1], strPara[2], strORG);


                    e.Result = data;
                }
            }
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.dgv_header.DataClean();
            var data = e.Result as List<BL_ORDER>;
            if (data != null)
            {
                //this.dgv_header.DataSource = data;
                this.dgv_header.DataBindByList(data);
                this.dgv_header.Rows.Cast<DataGridViewRow>()
                    .Where(x => x.Cells["ORG_CODE"].Value.ConvertToString().Trim() == "IND")
                    .ForEach(x =>
                    {
                        x.Cells["ORDER_NO"].Style.ForeColor = Color.DarkGreen;
                    });
            }
            this.picLoder.Visible = false;
        }

        private void btn_TW1_Click(object sender, EventArgs e)
        {
            this.btn_IN.BackColor = Color.Gray;
            this.btn_TW1.BackColor = Color.Gray;

            this.btn_TW1.BackColor = Color.LightGray;

            Global.ORG_CODE = "TW1";
            this.BindData();
        }

        private void btn_IN_Click(object sender, EventArgs e)
        {
            this.btn_IN.BackColor = Color.Gray;
            this.btn_TW1.BackColor = Color.Gray;

            this.btn_IN.BackColor = Color.LightGray;
            Global.ORG_CODE = "IND";
            this.BindData();
        }


        private void dgv_header_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1 && e.ColumnIndex != -1)
            {
                //Parallel.ForEach(this.dgv_header.Rows.Cast<DataGridViewRow>(), x =>
                //{
                //    x.Cells["check"].Value = false;
                //});
                if ((bool?)this.dgv_header.Rows[e.RowIndex].Cells["check"].Value == true)
                {
                    this.dgv_header.Rows[e.RowIndex].Cells["check"].Value = false;
                }
                else
                {
                    this.dgv_header.Rows[e.RowIndex].Cells["check"].Value = true;
                }

                if (this.dgv_header.Columns[e.ColumnIndex].Name == "CUST_CNAME")
                {
                    //  開啟客戶詳細訂單資訊
                    var strOrderID = this.dgv_header.Rows[e.RowIndex].Cells["ORDER_ID"].Value.ConvertToString();
                    var strCustID = this.dgv_header.Rows[e.RowIndex].Cells["CUST_ID"].Value.ConvertToString();

                    var frm = new frmAddContract(strCustID, strOrderID, true) { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
                    frm.ShowDialog();
                    frm.Dispose();
                    frm = null;
                }
                if (this.dgv_header.Columns[e.ColumnIndex].Name == "CONTRACT_STATUS")
                {
                    if (this.dgv_header.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ConvertToString().Trim() == "已退件")
                    {
                        //  已退件資料, 可以修改重送
                        var strOrderID = this.dgv_header.Rows[e.RowIndex].Cells["ORDER_ID"].Value.ConvertToString();
                        var strCustID = this.dgv_header.Rows[e.RowIndex].Cells["CUST_ID"].Value.ConvertToString();

                        var frm = new frmAddContract(strCustID, strOrderID, false, true) { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
                        frm.ShowDialog();
                        frm.Dispose();
                        frm = null;
                        this.BindData();
                    }
                }
            }
        }

        private void btn_send_Click(object sender, EventArgs e)
        {
            var selectionItem = this.dgv_header.Rows.Cast<DataGridViewRow>()
                                .Where(x => (bool?)x.Cells["check"].Value == true).ToList();
            int iSuccess = 0;

            if (selectionItem.Count() > 0)
            {
                selectionItem
                    .ForEach(x =>
                    {
                        var strCustName = x.Cells["CUST_CNAME"].Value.ConvertToString();
                        var strCustName_Eng = x.Cells["CUST_ENAME"].Value.ConvertToString();
                        var strOrderNo = x.Cells["ORDER_NO"].Value.ConvertToString();
                        var strCustMail = x.Cells["EMAIL_1"].Value.ConvertToString().Trim();
                        var strSalesMail = x.Cells["SALES_EMAIL"].Value.ConvertToString().Trim();
                        var strCustID = x.Cells["CUST_ID"].Value.ConvertToString();

                        #region sample code...
                        //ReportViewer rptExport = new ReportViewer();
                        //rptExport.Reset();
                        //rptExport.LocalReport.ReportPath = reportPath;
                        //rptExport.LocalReport.SetParameters(new ReportParameter("CUST_CNAME", strCustName));
                        //rptExport.LocalReport.SetParameters(new ReportParameter("CUST_ENAME", strCustName_Eng));
                        //rptExport.LocalReport.SetParameters(new ReportParameter("ORDER_NO", strOrderNo));
                        //rptExport.RefreshReport();

                        ////  Export
                        //var strFileName = strSavePath + "/" + strOrderNo + "_" + strCustName + ".pdf";

                        //Warning[] warnings;
                        //string[] streamids;
                        //string mimeType;
                        //string encoding;
                        //string filenameExtension;

                        //// 

                        //byte[] bytes = rptExport.LocalReport.Render(
                        //"PDF", null, out mimeType, out encoding, out filenameExtension,
                        //out streamids, out warnings);

                        //using (System.IO.FileStream fs = new System.IO.FileStream(strFileName, System.IO.FileMode.Create))
                        //{
                        //    fs.Write(bytes, 0, bytes.Length);
                        //}

                        //  Send Mail
                        //  Todo > Alex > Testing and Change Body content.
                        #endregion

                        var strMailSubject = "Sincerely thank you for becoming our valued wealth management client";
                        var strMailTo = strCustMail;

                        string strMailBody = @"
<html><body style='font-family: Calisto MT; font-size: 14px; line-height: 1.25; width: 400px; border: solid 1px #eee; padding: 10px;'><div style='width:100%; float:left; border:solid 0px green;'><img src='https://www.bestleader-service.com/images/bestleader_logo.jpg' style='display:block; margin: 0 auto; width:90px;' /></div><p>尊敬的{0}，</p><p>謝謝您的支持與愛護，我們已收到您的合約 {1}</p><p>合約會送至國外用印，這會需要一段時間，請耐心等待。</p><p>如有任何問題請與您的專員聯絡，我們將會提供您專業的服務。</p><p>&nbsp;</p><p>謝謝</p><p>&nbsp;</p><p>Best Leader Markets PTY Ltd</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>Dear {2}：</p><p>Thank you for your trust in Best Leader Markets PTY Ltd.,</p><p>we will give you the contract number: {3}</p><p>to complete the necessary administrative matters,</p><p>because the operation process must be completed overseas,</p><p>there will be a waiting time, please also be patient .</p><p>If you have any questions, there is always welcome to contact to your</p><p>service business and we will provide you with professional services.</p><p>&nbsp;</p><p>Thank you.</p><p>&nbsp;</p><p>Best Leader Markets PTY Ltd</p><div style='width:100%; float:left;'><img src='https://www.bestleader-service.com/images/bestleader_aprove.jpg' style='display:block; float:right; border:solid 0px #aaa; width:110px;' /></div></body></html>";
                        strMailBody = string.Format(strMailBody, strCustName, strOrderNo, strCustName_Eng, strOrderNo);


                        var strCC = strSalesMail + ";customerservice@bestleader-service.com";
                        var strBCC = string.Empty;

                        SendMail
                        (
                            MailTo: new List<string>() { strMailTo },
                            MailSubject: strMailSubject,
                            MailBody: strMailBody,

                            strFileName: string.Empty,
                            strBCC: strBCC,
                            strCC: strCC
                        );

                        if (base.Cust.UpdateCustomer_IsOldCustomer(strCustID))
                        {
                            iSuccess += 1;
                        }

                    });

                this.BindData();
                MessageBox.Show("Welcome Letter通知合約發送成功!!");

            }
            else
            {
                MessageBox.Show("請選擇要發送Welcome Letter通知的合約");
            }
        }
    }
}
