﻿using MK.Demo.Logic;
using MK.Demo.Model;
using MK_DEMO.Destop.FormFunction;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MK.Demo.Utility;

namespace MK_DEMO.Destop.FormFunction
{
    public partial class frmAlertToCustomer_DepositOnLine : MK_DEMO.Destop.BaseForm
    {
        public frmAlertToCustomer_DepositOnLine()
        {
            InitializeComponent();

            this.dgv_header.AutoGenerateColumns = false;
            this.dgv_header.MultiSelect = false;
            this.dgv_header.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            
        }

        private void frmAlertToCustomer_DepositOnLine_Load(object sender, EventArgs e)
        {
            this.BindData();
        }

        private void BindData()
        {

            var data = base.Cust.GetData_CanOnlineDeposit();
            data.ForEach(x =>
            {   
                //  檢核沒有UNION_ID_NUMBER的聯名戶, 就要先產生資料
                if
                    (
                        x.CUST_CNAME.ConvertToString().Trim() != string.Empty &&
                        x.CUST_CNAME2.ConvertToString().Trim() != string.Empty &&
                        x.UNION_ID_NUMBER.ConvertToString().Trim() == string.Empty
                    )
                {
                    //  1. Get Key
                    x.UNION_ID_NUMBER = base.Cust.GetNewUnionIdNumber();
                    //  2. Update to DB
                    base.Cust.UpdateUnionIdNumber(x);
                }

            });
            this.dgv_header.DataBindByList(data);
        }
        
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgv_header_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1 && e.ColumnIndex != -1)
            {
                if ((bool?)this.dgv_header.Rows[e.RowIndex].Cells["Chk"].Value == true)
                {
                    this.dgv_header.Rows[e.RowIndex].Cells["Chk"].Value = false;
                }
                else
                {
                    this.dgv_header.Rows[e.RowIndex].Cells["Chk"].Value = true;
                }
            }
        }


        private void dgv_header_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        { }

        private void dgv_header_CellEnter(object sender, DataGridViewCellEventArgs e)
        { }

        private void dgv_header_CellLeave(object sender, DataGridViewCellEventArgs e)
        { }

        private void dgv_header_CellContentClick(object sender, DataGridViewCellEventArgs e)
        { }



        private void btnSubmit_Click(object sender, EventArgs e)
        {
            var strMailSubject = "線上出金申請通知信";
            var strMailBody = "親愛的客戶您好<br/><br/>" +
              "您已經核准可以進行web線上出金申請<br/>" +
              "請先點選網址進行開通 , 手機或EMAIL檢核認證, 謝謝<br/><br/>" +
              "網址:<a href='https://www.bestleader-service.com/Customer_Auth.aspx?mode=E&P={0}'>https://www.bestleader-service.com/Customer_Auth.aspx</a><br/><br/>" +
               "<br/><br/>  Best Leader Markets PTY LTD <br/> 百麗客服部 敬啟";
            this.SendToCust(strMailSubject, strMailBody);
        }
        

        private void SendToCust(string strMailSubject, string strMailBody)
        {
            var dataSelection = this.dgv_header.Rows.Cast<DataGridViewRow>()
                .Where(x => (bool?)x.Cells["Chk"].Value == true)
                .ToList();
            if (dataSelection.Count() <= 0)
            {
                MessageBox.Show("請先選取要發送通知之客戶");
                return;
            }

            //  * Update GU_UD
            var dataSave = new List<BL_CUST>();
            dataSelection.ForEach(x =>
            {
                dataSave.Add(new BL_CUST()
                {
                    CUST_ID = decimal.Parse(x.Cells["CUST_ID"].Value.ConvertToString()),
                    CUST_CNAME = x.Cells["CUST_CNAME"].Value.ConvertToString(),
                    CUST_ENAME = x.Cells["CUST_ENAME"].Value.ConvertToString(),
                    ID_NUMBER = x.Cells["ID_NUMBER"].Value.ConvertToString(),
                    EMAIL_1 = x.Cells["EMAIL_1"].Value.ConvertToString(),
                    EMAIL_2 = x.Cells["EMAIL_2"].Value.ConvertToString(),       //20191112 Fix
                    CUST_CNAME2 = x.Cells["CUST_CNAME2"].Value.ConvertToString(),
                    SALES_EMAIL = x.Cells["SALES_EMAIL"].Value.ConvertToString(),
                    IS_ONLINE_MAIL_GUID = Guid.NewGuid().ConvertToString()
                });
            });
            
            //  1. 先更新預設密碼回系統
            if (base.Cust.UpdateWebGU_ID(dataSave))
            {
                //  2. 逐筆發送Mail
                dataSave.ForEach(x =>
                {
                    var mailTo = new List<string>();
                    var strMailCC = string.Empty;
                    var MailSubject = strMailSubject;

                    var MailBody = string.Format
                        (
                            strMailBody,
                            x.IS_ONLINE_MAIL_GUID.ConvertToString()
                        );

                    if (Global.IsTestMode)
                    {
                        //  測試階段只發送給Admin
                        mailTo = GetConfigValueByKey("MailSystemAdmin").Split(new string[] { ";" }, StringSplitOptions.None).ToList();
                        MailSubject += "(Testing)";
                    }
                    else
                    {
                        mailTo.Add(x.EMAIL_1.ConvertToString());
                        //  Add On 2019/10/16 聯名戶
                        if (x.CUST_CNAME2.ConvertToString().Trim() != string.Empty)
                        {
                            mailTo.Add(x.EMAIL_2.ConvertToString());
                        }
                        strMailCC = x.SALES_EMAIL.ConvertToString();
                    }
                    SendMail
                    (
                        MailTo: mailTo,
                        MailSubject: MailSubject,
                        MailBody: MailBody,
                        strCC: strMailCC
                    );
                });

                MessageBox.Show("送出完成");
                this.Close();
            }
            else
            {
                MessageBox.Show("送出失敗");
            }

        }


        private void chkALL_CheckedChanged(object sender, EventArgs e)
        {
            this.dgv_header.Rows.Cast<DataGridViewRow>()
                .ForEach(x =>
                {
                    x.Cells["Chk"].Value = this.chkALL.Checked;
                });
        }
    }
}
