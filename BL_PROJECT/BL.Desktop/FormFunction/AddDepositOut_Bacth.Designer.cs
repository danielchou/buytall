﻿namespace MK_DEMO.Destop.FormFunction
{
    partial class AddDepositOut_Batch
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblCurrenct = new System.Windows.Forms.Label();
            this.txt_Bankename = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.txtCustomer_en2 = new System.Windows.Forms.TextBox();
            this.txtAmount2ACTUAL = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtCustomer_en = new System.Windows.Forms.TextBox();
            this.txtAmount3 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtAmount2 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtAmount1 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtCustomer = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtRemark = new System.Windows.Forms.TextBox();
            this.txtSWIFT_CODE = new System.Windows.Forms.TextBox();
            this.txtBANK_CNAME = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtBatchID = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.label14 = new System.Windows.Forms.Label();
            this.dgv_header = new System.Windows.Forms.DataGridView();
            this.No = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.check = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.RowDel = new System.Windows.Forms.DataGridViewLinkColumn();
            this.ORDER_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.END_BALANCE_FLAG = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.END_BALANCE_FLAG2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUST_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.APPLY_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CHECK_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.APPROVE_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ORDER_Currency = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ORDER_Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ORDER_INTEREST_MONTHLY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AmountOut = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OnPathAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ACTUAL_AmountOut = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BALANCE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsNet = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsTransToNewForm = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NewOrderNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ORDER_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SCHEDULE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUST_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IS_ALL_BALANCE_OUT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BONUS_TSF = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NEW_CONTRACT_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cust_en = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cust_en2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtACCOUNT = new System.Windows.Forms.TextBox();
            this.txtBRANCH_CNAME = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tb_BALANCE = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtAMOUNT_RMB = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtAMOUNT_NTD = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtAMOUNT_USD = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dteAPPROVE_DATE = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.dteCHECK_DATE = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.dteAPPLY_DATE = new System.Windows.Forms.DateTimePicker();
            this.label28 = new System.Windows.Forms.Label();
            this.txtAmount_RMB_Order = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.txtAmount_NTD_Order = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.txtAmount_USD_Order = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.btnBringOrder = new System.Windows.Forms.Button();
            this.txtParentOrderNo = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.txtInterestTimes = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtAmount_AUD_Order = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtAmount_EUR_Order = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtAMOUNT_AUD = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtAMOUNT_EUR = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtAmount_JPY_Order = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtAMOUNT_JPY = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtAmount_NZD_Order = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txtAMOUNT_NZD = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_header)).BeginInit();
            this.SuspendLayout();
            // 
            // lblCurrenct
            // 
            this.lblCurrenct.AutoSize = true;
            this.lblCurrenct.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lblCurrenct.Location = new System.Drawing.Point(1140, 466);
            this.lblCurrenct.Name = "lblCurrenct";
            this.lblCurrenct.Size = new System.Drawing.Size(86, 19);
            this.lblCurrenct.TabIndex = 159;
            this.lblCurrenct.Text = "lblCurrenct";
            this.lblCurrenct.Visible = false;
            // 
            // txt_Bankename
            // 
            this.txt_Bankename.Enabled = false;
            this.txt_Bankename.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txt_Bankename.Location = new System.Drawing.Point(970, 4);
            this.txt_Bankename.Multiline = true;
            this.txt_Bankename.Name = "txt_Bankename";
            this.txt_Bankename.Size = new System.Drawing.Size(385, 50);
            this.txt_Bankename.TabIndex = 158;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label19.Location = new System.Drawing.Point(830, 7);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(109, 19);
            this.label19.TabIndex = 157;
            this.label19.Text = "銀行名稱(英文)";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tabControl1.Location = new System.Drawing.Point(19, 447);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(592, 152);
            this.tabControl1.TabIndex = 156;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.tabPage1.Controls.Add(this.txtCustomer_en2);
            this.tabPage1.Controls.Add(this.txtAmount2ACTUAL);
            this.tabPage1.Controls.Add(this.label20);
            this.tabPage1.Controls.Add(this.txtCustomer_en);
            this.tabPage1.Controls.Add(this.txtAmount3);
            this.tabPage1.Controls.Add(this.label18);
            this.tabPage1.Controls.Add(this.txtAmount2);
            this.tabPage1.Controls.Add(this.label17);
            this.tabPage1.Controls.Add(this.txtAmount1);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.txtCustomer);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Location = new System.Drawing.Point(4, 28);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(584, 120);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Summary";
            // 
            // txtCustomer_en2
            // 
            this.txtCustomer_en2.Enabled = false;
            this.txtCustomer_en2.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txtCustomer_en2.Location = new System.Drawing.Point(72, 63);
            this.txtCustomer_en2.Name = "txtCustomer_en2";
            this.txtCustomer_en2.Size = new System.Drawing.Size(220, 27);
            this.txtCustomer_en2.TabIndex = 169;
            // 
            // txtAmount2ACTUAL
            // 
            this.txtAmount2ACTUAL.Enabled = false;
            this.txtAmount2ACTUAL.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txtAmount2ACTUAL.Location = new System.Drawing.Point(417, 63);
            this.txtAmount2ACTUAL.Name = "txtAmount2ACTUAL";
            this.txtAmount2ACTUAL.Size = new System.Drawing.Size(156, 27);
            this.txtAmount2ACTUAL.TabIndex = 168;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label20.Location = new System.Drawing.Point(312, 67);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(99, 19);
            this.label20.TabIndex = 167;
            this.label20.Text = "實際提領金額";
            // 
            // txtCustomer_en
            // 
            this.txtCustomer_en.Enabled = false;
            this.txtCustomer_en.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txtCustomer_en.Location = new System.Drawing.Point(72, 36);
            this.txtCustomer_en.Name = "txtCustomer_en";
            this.txtCustomer_en.Size = new System.Drawing.Size(220, 27);
            this.txtCustomer_en.TabIndex = 166;
            // 
            // txtAmount3
            // 
            this.txtAmount3.Enabled = false;
            this.txtAmount3.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txtAmount3.Location = new System.Drawing.Point(417, 91);
            this.txtAmount3.Name = "txtAmount3";
            this.txtAmount3.Size = new System.Drawing.Size(156, 27);
            this.txtAmount3.TabIndex = 164;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label18.Location = new System.Drawing.Point(312, 95);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(39, 19);
            this.label18.TabIndex = 163;
            this.label18.Text = "餘額";
            // 
            // txtAmount2
            // 
            this.txtAmount2.Enabled = false;
            this.txtAmount2.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txtAmount2.Location = new System.Drawing.Point(417, 36);
            this.txtAmount2.Name = "txtAmount2";
            this.txtAmount2.Size = new System.Drawing.Size(156, 27);
            this.txtAmount2.TabIndex = 162;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label17.Location = new System.Drawing.Point(312, 40);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(69, 19);
            this.label17.TabIndex = 161;
            this.label17.Text = "提領金額";
            // 
            // txtAmount1
            // 
            this.txtAmount1.Enabled = false;
            this.txtAmount1.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txtAmount1.Location = new System.Drawing.Point(417, 7);
            this.txtAmount1.Name = "txtAmount1";
            this.txtAmount1.Size = new System.Drawing.Size(156, 27);
            this.txtAmount1.TabIndex = 160;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label12.Location = new System.Drawing.Point(312, 11);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(39, 19);
            this.label12.TabIndex = 159;
            this.label12.Text = "本金";
            // 
            // txtCustomer
            // 
            this.txtCustomer.Enabled = false;
            this.txtCustomer.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txtCustomer.Location = new System.Drawing.Point(72, 7);
            this.txtCustomer.Name = "txtCustomer";
            this.txtCustomer.Size = new System.Drawing.Size(220, 27);
            this.txtCustomer.TabIndex = 158;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label7.Location = new System.Drawing.Point(10, 11);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(39, 19);
            this.label7.TabIndex = 157;
            this.label7.Text = "客戶";
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.checkBox2.Location = new System.Drawing.Point(195, 60);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(88, 23);
            this.checkBox2.TabIndex = 155;
            this.checkBox2.Text = "內轉新單";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label15.Location = new System.Drawing.Point(33, 92);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(63, 19);
            this.label15.TabIndex = 149;
            this.label15.Text = "Remark";
            // 
            // txtRemark
            // 
            this.txtRemark.Enabled = false;
            this.txtRemark.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txtRemark.Location = new System.Drawing.Point(120, 88);
            this.txtRemark.Name = "txtRemark";
            this.txtRemark.Size = new System.Drawing.Size(538, 27);
            this.txtRemark.TabIndex = 150;
            // 
            // txtSWIFT_CODE
            // 
            this.txtSWIFT_CODE.Enabled = false;
            this.txtSWIFT_CODE.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txtSWIFT_CODE.Location = new System.Drawing.Point(1134, 58);
            this.txtSWIFT_CODE.Name = "txtSWIFT_CODE";
            this.txtSWIFT_CODE.Size = new System.Drawing.Size(221, 27);
            this.txtSWIFT_CODE.TabIndex = 148;
            this.txtSWIFT_CODE.TextChanged += new System.EventHandler(this.txtSWIFT_CODE_TextChanged);
            // 
            // txtBANK_CNAME
            // 
            this.txtBANK_CNAME.Enabled = false;
            this.txtBANK_CNAME.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txtBANK_CNAME.Location = new System.Drawing.Point(558, 4);
            this.txtBANK_CNAME.Name = "txtBANK_CNAME";
            this.txtBANK_CNAME.Size = new System.Drawing.Size(266, 27);
            this.txtBANK_CNAME.TabIndex = 140;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label13.Location = new System.Drawing.Point(1031, 62);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(85, 19);
            this.label13.TabIndex = 147;
            this.label13.Text = "Swift Code";
            // 
            // txtBatchID
            // 
            this.txtBatchID.Enabled = false;
            this.txtBatchID.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txtBatchID.Location = new System.Drawing.Point(86, 4);
            this.txtBatchID.Name = "txtBatchID";
            this.txtBatchID.Size = new System.Drawing.Size(101, 27);
            this.txtBatchID.TabIndex = 154;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label16.Location = new System.Drawing.Point(15, 8);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(75, 19);
            this.label16.TabIndex = 153;
            this.label16.Text = "Batch No.";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.checkBox1.Location = new System.Drawing.Point(1036, 462);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(88, 23);
            this.checkBox1.TabIndex = 152;
            this.checkBox1.Text = "餘額結清";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.Visible = false;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label14.Location = new System.Drawing.Point(674, 92);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(69, 19);
            this.label14.TabIndex = 145;
            this.label14.Text = "銀行帳戶";
            // 
            // dgv_header
            // 
            this.dgv_header.AllowUserToAddRows = false;
            this.dgv_header.AllowUserToDeleteRows = false;
            this.dgv_header.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_header.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_header.ColumnHeadersHeight = 25;
            this.dgv_header.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv_header.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.No,
            this.check,
            this.RowDel,
            this.ORDER_NO,
            this.END_BALANCE_FLAG,
            this.END_BALANCE_FLAG2,
            this.CUST_NAME,
            this.APPLY_DATE,
            this.CHECK_DATE,
            this.APPROVE_DATE,
            this.ORDER_Currency,
            this.ORDER_Amount,
            this.ORDER_INTEREST_MONTHLY,
            this.AmountOut,
            this.OnPathAmount,
            this.ACTUAL_AmountOut,
            this.BALANCE,
            this.IsNet,
            this.IsTransToNewForm,
            this.NewOrderNo,
            this.ORDER_ID,
            this.SCHEDULE,
            this.CUST_ID,
            this.IS_ALL_BALANCE_OUT,
            this.BONUS_TSF,
            this.NEW_CONTRACT_NO,
            this.cust_en,
            this.cust_en2});
            this.dgv_header.Location = new System.Drawing.Point(12, 121);
            this.dgv_header.Name = "dgv_header";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.RoyalBlue;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Yellow;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_header.RowHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgv_header.RowHeadersWidth = 25;
            this.dgv_header.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgv_header.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(226)))), ((int)(((byte)(239)))));
            this.dgv_header.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_header.RowTemplate.Height = 24;
            this.dgv_header.Size = new System.Drawing.Size(1346, 330);
            this.dgv_header.TabIndex = 138;
            this.dgv_header.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_header_CellClick);
            this.dgv_header.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_header_CellContentClick);
            this.dgv_header.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_header_CellEnter);
            this.dgv_header.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgv_header_CellFormatting);
            this.dgv_header.CellLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_header_CellLeave);
            this.dgv_header.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgv_header_DataBindingComplete);
            this.dgv_header.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgv_header_EditingControlShowing);
            this.dgv_header.Sorted += new System.EventHandler(this.dgv_header_Sorted);
            this.dgv_header.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgv_header_KeyDown);
            // 
            // No
            // 
            this.No.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.No.HeaderText = "No";
            this.No.Name = "No";
            this.No.ReadOnly = true;
            this.No.Width = 35;
            // 
            // check
            // 
            this.check.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.check.FillWeight = 25F;
            this.check.HeaderText = "選";
            this.check.MinimumWidth = 25;
            this.check.Name = "check";
            this.check.ReadOnly = true;
            this.check.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.check.Width = 25;
            // 
            // RowDel
            // 
            this.RowDel.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.RowDel.HeaderText = "刪除";
            this.RowDel.Name = "RowDel";
            this.RowDel.ReadOnly = true;
            this.RowDel.Width = 45;
            // 
            // ORDER_NO
            // 
            this.ORDER_NO.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ORDER_NO.DataPropertyName = "ORDER_NO";
            this.ORDER_NO.HeaderText = "Order No";
            this.ORDER_NO.Name = "ORDER_NO";
            this.ORDER_NO.ReadOnly = true;
            this.ORDER_NO.Width = 85;
            // 
            // END_BALANCE_FLAG
            // 
            this.END_BALANCE_FLAG.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.END_BALANCE_FLAG.DataPropertyName = "END_BALANCE_FLAG";
            this.END_BALANCE_FLAG.HeaderText = "類型";
            this.END_BALANCE_FLAG.Name = "END_BALANCE_FLAG";
            this.END_BALANCE_FLAG.ReadOnly = true;
            this.END_BALANCE_FLAG.Visible = false;
            this.END_BALANCE_FLAG.Width = 95;
            // 
            // END_BALANCE_FLAG2
            // 
            this.END_BALANCE_FLAG2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.END_BALANCE_FLAG2.HeaderText = "類型";
            this.END_BALANCE_FLAG2.Name = "END_BALANCE_FLAG2";
            this.END_BALANCE_FLAG2.ReadOnly = true;
            this.END_BALANCE_FLAG2.Width = 90;
            // 
            // CUST_NAME
            // 
            this.CUST_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.CUST_NAME.DataPropertyName = "CUST_CNAME";
            this.CUST_NAME.HeaderText = "客戶";
            this.CUST_NAME.Name = "CUST_NAME";
            this.CUST_NAME.ReadOnly = true;
            this.CUST_NAME.Width = 125;
            // 
            // APPLY_DATE
            // 
            this.APPLY_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.APPLY_DATE.DataPropertyName = "APPLY_DATE";
            dataGridViewCellStyle1.Format = "d";
            dataGridViewCellStyle1.NullValue = null;
            this.APPLY_DATE.DefaultCellStyle = dataGridViewCellStyle1;
            this.APPLY_DATE.HeaderText = "出金申請日";
            this.APPLY_DATE.Name = "APPLY_DATE";
            this.APPLY_DATE.ReadOnly = true;
            this.APPLY_DATE.Width = 110;
            // 
            // CHECK_DATE
            // 
            this.CHECK_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.CHECK_DATE.DataPropertyName = "FROM_DATE";
            this.CHECK_DATE.HeaderText = "合約啟始日";
            this.CHECK_DATE.Name = "CHECK_DATE";
            this.CHECK_DATE.ReadOnly = true;
            this.CHECK_DATE.Width = 110;
            // 
            // APPROVE_DATE
            // 
            this.APPROVE_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.APPROVE_DATE.DataPropertyName = "YEAR";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.APPROVE_DATE.DefaultCellStyle = dataGridViewCellStyle2;
            this.APPROVE_DATE.HeaderText = "年期";
            this.APPROVE_DATE.Name = "APPROVE_DATE";
            this.APPROVE_DATE.ReadOnly = true;
            this.APPROVE_DATE.Width = 50;
            // 
            // ORDER_Currency
            // 
            this.ORDER_Currency.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ORDER_Currency.DataPropertyName = "ORDER_Currency";
            this.ORDER_Currency.HeaderText = "幣別";
            this.ORDER_Currency.Name = "ORDER_Currency";
            this.ORDER_Currency.ReadOnly = true;
            this.ORDER_Currency.Width = 90;
            // 
            // ORDER_Amount
            // 
            this.ORDER_Amount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ORDER_Amount.DataPropertyName = "ORDER_Amount";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = null;
            this.ORDER_Amount.DefaultCellStyle = dataGridViewCellStyle3;
            this.ORDER_Amount.HeaderText = "本金";
            this.ORDER_Amount.Name = "ORDER_Amount";
            this.ORDER_Amount.ReadOnly = true;
            this.ORDER_Amount.Width = 90;
            // 
            // ORDER_INTEREST_MONTHLY
            // 
            this.ORDER_INTEREST_MONTHLY.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ORDER_INTEREST_MONTHLY.DataPropertyName = "ORDER_INTEREST_MONTHLY";
            this.ORDER_INTEREST_MONTHLY.HeaderText = "每月派利金額";
            this.ORDER_INTEREST_MONTHLY.Name = "ORDER_INTEREST_MONTHLY";
            this.ORDER_INTEREST_MONTHLY.ReadOnly = true;
            this.ORDER_INTEREST_MONTHLY.Width = 110;
            // 
            // AmountOut
            // 
            this.AmountOut.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.AmountOut.DataPropertyName = "Amount";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N2";
            dataGridViewCellStyle4.NullValue = null;
            this.AmountOut.DefaultCellStyle = dataGridViewCellStyle4;
            this.AmountOut.HeaderText = "提領金額";
            this.AmountOut.Name = "AmountOut";
            this.AmountOut.Width = 90;
            // 
            // OnPathAmount
            // 
            this.OnPathAmount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.OnPathAmount.DataPropertyName = "OnPathAmount";
            this.OnPathAmount.HeaderText = "提領處理中金額";
            this.OnPathAmount.Name = "OnPathAmount";
            this.OnPathAmount.ReadOnly = true;
            this.OnPathAmount.Width = 120;
            // 
            // ACTUAL_AmountOut
            // 
            this.ACTUAL_AmountOut.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ACTUAL_AmountOut.DataPropertyName = "ACTUAL_Amount";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "N2";
            dataGridViewCellStyle5.NullValue = null;
            this.ACTUAL_AmountOut.DefaultCellStyle = dataGridViewCellStyle5;
            this.ACTUAL_AmountOut.HeaderText = "實際提領金額";
            this.ACTUAL_AmountOut.Name = "ACTUAL_AmountOut";
            this.ACTUAL_AmountOut.ReadOnly = true;
            this.ACTUAL_AmountOut.Width = 105;
            // 
            // BALANCE
            // 
            this.BALANCE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.BALANCE.DataPropertyName = "BALANCE";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "N2";
            dataGridViewCellStyle6.NullValue = null;
            this.BALANCE.DefaultCellStyle = dataGridViewCellStyle6;
            this.BALANCE.HeaderText = "餘額";
            this.BALANCE.Name = "BALANCE";
            this.BALANCE.ReadOnly = true;
            this.BALANCE.Width = 80;
            // 
            // IsNet
            // 
            this.IsNet.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.IsNet.DataPropertyName = "WITHDRAWAL_TYPE";
            this.IsNet.HeaderText = "是否網路出金";
            this.IsNet.Name = "IsNet";
            this.IsNet.ReadOnly = true;
            this.IsNet.Width = 120;
            // 
            // IsTransToNewForm
            // 
            this.IsTransToNewForm.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.IsTransToNewForm.DataPropertyName = "BONUS_TSF";
            this.IsTransToNewForm.HeaderText = "利息內轉新單";
            this.IsTransToNewForm.Name = "IsTransToNewForm";
            this.IsTransToNewForm.ReadOnly = true;
            this.IsTransToNewForm.Width = 120;
            // 
            // NewOrderNo
            // 
            this.NewOrderNo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.NewOrderNo.DataPropertyName = "NEW_CONTRACT_NO";
            this.NewOrderNo.HeaderText = "合約編號(利息內轉新單)";
            this.NewOrderNo.Name = "NewOrderNo";
            this.NewOrderNo.ReadOnly = true;
            this.NewOrderNo.Width = 120;
            // 
            // ORDER_ID
            // 
            this.ORDER_ID.DataPropertyName = "ORDER_ID";
            this.ORDER_ID.HeaderText = "ORDER_ID";
            this.ORDER_ID.Name = "ORDER_ID";
            this.ORDER_ID.ReadOnly = true;
            this.ORDER_ID.Visible = false;
            // 
            // SCHEDULE
            // 
            this.SCHEDULE.DataPropertyName = "SCHEDULE";
            this.SCHEDULE.HeaderText = "SCHEDULE";
            this.SCHEDULE.Name = "SCHEDULE";
            this.SCHEDULE.ReadOnly = true;
            this.SCHEDULE.Visible = false;
            // 
            // CUST_ID
            // 
            this.CUST_ID.DataPropertyName = "CUST_ID";
            this.CUST_ID.HeaderText = "CUST_ID";
            this.CUST_ID.Name = "CUST_ID";
            this.CUST_ID.ReadOnly = true;
            this.CUST_ID.Visible = false;
            // 
            // IS_ALL_BALANCE_OUT
            // 
            this.IS_ALL_BALANCE_OUT.DataPropertyName = "IS_ALL_BALANCE_OUT";
            this.IS_ALL_BALANCE_OUT.HeaderText = "IS_ALL_BALANCE_OUT";
            this.IS_ALL_BALANCE_OUT.Name = "IS_ALL_BALANCE_OUT";
            this.IS_ALL_BALANCE_OUT.ReadOnly = true;
            this.IS_ALL_BALANCE_OUT.Visible = false;
            // 
            // BONUS_TSF
            // 
            this.BONUS_TSF.DataPropertyName = "BONUS_TSF";
            this.BONUS_TSF.HeaderText = "BONUS_TSF";
            this.BONUS_TSF.Name = "BONUS_TSF";
            this.BONUS_TSF.ReadOnly = true;
            this.BONUS_TSF.Visible = false;
            // 
            // NEW_CONTRACT_NO
            // 
            this.NEW_CONTRACT_NO.DataPropertyName = "NEW_CONTRACT_NO";
            this.NEW_CONTRACT_NO.HeaderText = "NEW_CONTRACT_NO";
            this.NEW_CONTRACT_NO.Name = "NEW_CONTRACT_NO";
            this.NEW_CONTRACT_NO.ReadOnly = true;
            this.NEW_CONTRACT_NO.Visible = false;
            // 
            // cust_en
            // 
            this.cust_en.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.cust_en.DataPropertyName = "CUST_ENAME";
            this.cust_en.HeaderText = "cust_en";
            this.cust_en.MinimumWidth = 100;
            this.cust_en.Name = "cust_en";
            this.cust_en.Width = 220;
            // 
            // cust_en2
            // 
            this.cust_en2.DataPropertyName = "CUST_ENAME2";
            this.cust_en2.HeaderText = "cust_en2";
            this.cust_en2.Name = "cust_en2";
            this.cust_en2.ReadOnly = true;
            this.cust_en2.Visible = false;
            // 
            // txtACCOUNT
            // 
            this.txtACCOUNT.Enabled = false;
            this.txtACCOUNT.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txtACCOUNT.Location = new System.Drawing.Point(764, 88);
            this.txtACCOUNT.Name = "txtACCOUNT";
            this.txtACCOUNT.Size = new System.Drawing.Size(591, 27);
            this.txtACCOUNT.TabIndex = 146;
            // 
            // txtBRANCH_CNAME
            // 
            this.txtBRANCH_CNAME.Enabled = false;
            this.txtBRANCH_CNAME.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txtBRANCH_CNAME.Location = new System.Drawing.Point(764, 58);
            this.txtBRANCH_CNAME.Name = "txtBRANCH_CNAME";
            this.txtBRANCH_CNAME.Size = new System.Drawing.Size(252, 27);
            this.txtBRANCH_CNAME.TabIndex = 142;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label11.Location = new System.Drawing.Point(425, 8);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(109, 19);
            this.label11.TabIndex = 139;
            this.label11.Text = "銀行名稱(中文)";
            // 
            // tb_BALANCE
            // 
            this.tb_BALANCE.Location = new System.Drawing.Point(911, 458);
            this.tb_BALANCE.Name = "tb_BALANCE";
            this.tb_BALANCE.Size = new System.Drawing.Size(119, 29);
            this.tb_BALANCE.TabIndex = 137;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label10.Location = new System.Drawing.Point(672, 62);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(69, 19);
            this.label10.TabIndex = 141;
            this.label10.Text = "分行名稱";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label9.Location = new System.Drawing.Point(864, 464);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(39, 19);
            this.label9.TabIndex = 136;
            this.label9.Text = "餘額";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.textBox1.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.textBox1.Location = new System.Drawing.Point(429, 58);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(229, 27);
            this.textBox1.TabIndex = 135;
            this.textBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyDown);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label8.Location = new System.Drawing.Point(323, 62);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(99, 19);
            this.label8.TabIndex = 134;
            this.label8.Text = "新單合約號碼";
            // 
            // comboBox1
            // 
            this.comboBox1.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Y",
            "N"});
            this.comboBox1.Location = new System.Drawing.Point(120, 58);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(67, 27);
            this.comboBox1.TabIndex = 131;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label6.Location = new System.Drawing.Point(15, 62);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(99, 19);
            this.label6.TabIndex = 130;
            this.label6.Text = "是否網路出金";
            // 
            // txtAMOUNT_RMB
            // 
            this.txtAMOUNT_RMB.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txtAMOUNT_RMB.Location = new System.Drawing.Point(1276, 592);
            this.txtAMOUNT_RMB.Name = "txtAMOUNT_RMB";
            this.txtAMOUNT_RMB.Size = new System.Drawing.Size(79, 27);
            this.txtAMOUNT_RMB.TabIndex = 129;
            this.txtAMOUNT_RMB.TextChanged += new System.EventHandler(this.txtAMOUNT_USD_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label5.Location = new System.Drawing.Point(1119, 596);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(112, 19);
            this.label5.TabIndex = 128;
            this.label5.Text = "提領金額(RMB)";
            // 
            // txtAMOUNT_NTD
            // 
            this.txtAMOUNT_NTD.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txtAMOUNT_NTD.Location = new System.Drawing.Point(1021, 592);
            this.txtAMOUNT_NTD.Name = "txtAMOUNT_NTD";
            this.txtAMOUNT_NTD.Size = new System.Drawing.Size(79, 27);
            this.txtAMOUNT_NTD.TabIndex = 127;
            this.txtAMOUNT_NTD.TextChanged += new System.EventHandler(this.txtAMOUNT_USD_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label4.Location = new System.Drawing.Point(864, 596);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(110, 19);
            this.label4.TabIndex = 126;
            this.label4.Text = "提領金額(NTD)";
            // 
            // txtAMOUNT_USD
            // 
            this.txtAMOUNT_USD.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txtAMOUNT_USD.Location = new System.Drawing.Point(770, 592);
            this.txtAMOUNT_USD.Name = "txtAMOUNT_USD";
            this.txtAMOUNT_USD.Size = new System.Drawing.Size(79, 27);
            this.txtAMOUNT_USD.TabIndex = 125;
            this.txtAMOUNT_USD.TextChanged += new System.EventHandler(this.txtAMOUNT_USD_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label3.Location = new System.Drawing.Point(614, 596);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(110, 19);
            this.label3.TabIndex = 124;
            this.label3.Text = "提領金額(USD)";
            // 
            // dteAPPROVE_DATE
            // 
            this.dteAPPROVE_DATE.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.dteAPPROVE_DATE.Location = new System.Drawing.Point(1212, 558);
            this.dteAPPROVE_DATE.Name = "dteAPPROVE_DATE";
            this.dteAPPROVE_DATE.Size = new System.Drawing.Size(143, 27);
            this.dteAPPROVE_DATE.TabIndex = 123;
            this.dteAPPROVE_DATE.Visible = false;
            this.dteAPPROVE_DATE.ValueChanged += new System.EventHandler(this.dte_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(1116, 562);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 17);
            this.label2.TabIndex = 122;
            this.label2.Text = "審批日";
            this.label2.Visible = false;
            // 
            // dteCHECK_DATE
            // 
            this.dteCHECK_DATE.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.dteCHECK_DATE.Location = new System.Drawing.Point(957, 558);
            this.dteCHECK_DATE.Name = "dteCHECK_DATE";
            this.dteCHECK_DATE.Size = new System.Drawing.Size(143, 27);
            this.dteCHECK_DATE.TabIndex = 121;
            this.dteCHECK_DATE.Visible = false;
            this.dteCHECK_DATE.ValueChanged += new System.EventHandler(this.dte_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(864, 562);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 19);
            this.label1.TabIndex = 120;
            this.label1.Text = "查核日";
            this.label1.Visible = false;
            // 
            // dteAPPLY_DATE
            // 
            this.dteAPPLY_DATE.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.dteAPPLY_DATE.Location = new System.Drawing.Point(279, 6);
            this.dteAPPLY_DATE.Name = "dteAPPLY_DATE";
            this.dteAPPLY_DATE.Size = new System.Drawing.Size(143, 27);
            this.dteAPPLY_DATE.TabIndex = 119;
            this.dteAPPLY_DATE.ValueChanged += new System.EventHandler(this.dte_ValueChanged);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label28.Location = new System.Drawing.Point(190, 9);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(84, 19);
            this.label28.TabIndex = 118;
            this.label28.Text = "出金申請日";
            // 
            // txtAmount_RMB_Order
            // 
            this.txtAmount_RMB_Order.Enabled = false;
            this.txtAmount_RMB_Order.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txtAmount_RMB_Order.Location = new System.Drawing.Point(1276, 493);
            this.txtAmount_RMB_Order.Name = "txtAmount_RMB_Order";
            this.txtAmount_RMB_Order.Size = new System.Drawing.Size(79, 27);
            this.txtAmount_RMB_Order.TabIndex = 117;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label42.Location = new System.Drawing.Point(1116, 497);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(154, 19);
            this.label42.TabIndex = 116;
            this.label42.Text = "Order Amount(RMB)";
            // 
            // txtAmount_NTD_Order
            // 
            this.txtAmount_NTD_Order.Enabled = false;
            this.txtAmount_NTD_Order.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txtAmount_NTD_Order.Location = new System.Drawing.Point(1021, 493);
            this.txtAmount_NTD_Order.Name = "txtAmount_NTD_Order";
            this.txtAmount_NTD_Order.Size = new System.Drawing.Size(79, 27);
            this.txtAmount_NTD_Order.TabIndex = 115;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label40.Location = new System.Drawing.Point(864, 497);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(152, 19);
            this.label40.TabIndex = 114;
            this.label40.Text = "Order Amount(NTD)";
            // 
            // txtAmount_USD_Order
            // 
            this.txtAmount_USD_Order.Enabled = false;
            this.txtAmount_USD_Order.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txtAmount_USD_Order.Location = new System.Drawing.Point(770, 493);
            this.txtAmount_USD_Order.Name = "txtAmount_USD_Order";
            this.txtAmount_USD_Order.Size = new System.Drawing.Size(79, 27);
            this.txtAmount_USD_Order.TabIndex = 113;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label39.Location = new System.Drawing.Point(614, 497);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(152, 19);
            this.label39.TabIndex = 112;
            this.label39.Text = "Order Amount(USD)";
            // 
            // btnBringOrder
            // 
            this.btnBringOrder.Enabled = false;
            this.btnBringOrder.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnBringOrder.Location = new System.Drawing.Point(797, 460);
            this.btnBringOrder.Name = "btnBringOrder";
            this.btnBringOrder.Size = new System.Drawing.Size(36, 25);
            this.btnBringOrder.TabIndex = 111;
            this.btnBringOrder.Text = "...";
            this.btnBringOrder.UseVisualStyleBackColor = true;
            this.btnBringOrder.Click += new System.EventHandler(this.btnBringOrder_Click);
            // 
            // txtParentOrderNo
            // 
            this.txtParentOrderNo.Enabled = false;
            this.txtParentOrderNo.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txtParentOrderNo.Location = new System.Drawing.Point(705, 460);
            this.txtParentOrderNo.Name = "txtParentOrderNo";
            this.txtParentOrderNo.Size = new System.Drawing.Size(86, 27);
            this.txtParentOrderNo.TabIndex = 110;
            this.txtParentOrderNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtParentOrderNo_KeyDown);
            this.txtParentOrderNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtParentOrderNo_KeyPress);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label37.Location = new System.Drawing.Point(614, 464);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(76, 19);
            this.label37.TabIndex = 109;
            this.label37.Text = "Order No";
            // 
            // btnSubmit
            // 
            this.btnSubmit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSubmit.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnSubmit.Location = new System.Drawing.Point(435, 668);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(259, 42);
            this.btnSubmit.TabIndex = 9;
            this.btnSubmit.Text = "批次送出";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnClose.Location = new System.Drawing.Point(733, 668);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(259, 42);
            this.btnClose.TabIndex = 8;
            this.btnClose.Text = "關閉";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // txtInterestTimes
            // 
            this.txtInterestTimes.Enabled = false;
            this.txtInterestTimes.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txtInterestTimes.Location = new System.Drawing.Point(1276, 688);
            this.txtInterestTimes.Name = "txtInterestTimes";
            this.txtInterestTimes.Size = new System.Drawing.Size(79, 27);
            this.txtInterestTimes.TabIndex = 161;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label21.Location = new System.Drawing.Point(1119, 692);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(69, 19);
            this.label21.TabIndex = 160;
            this.label21.Text = "派息次數";
            // 
            // txtAmount_AUD_Order
            // 
            this.txtAmount_AUD_Order.Enabled = false;
            this.txtAmount_AUD_Order.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txtAmount_AUD_Order.Location = new System.Drawing.Point(1021, 525);
            this.txtAmount_AUD_Order.Name = "txtAmount_AUD_Order";
            this.txtAmount_AUD_Order.Size = new System.Drawing.Size(79, 27);
            this.txtAmount_AUD_Order.TabIndex = 165;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label22.Location = new System.Drawing.Point(864, 529);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(153, 19);
            this.label22.TabIndex = 164;
            this.label22.Text = "Order Amount(AUD)";
            // 
            // txtAmount_EUR_Order
            // 
            this.txtAmount_EUR_Order.Enabled = false;
            this.txtAmount_EUR_Order.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txtAmount_EUR_Order.Location = new System.Drawing.Point(770, 525);
            this.txtAmount_EUR_Order.Name = "txtAmount_EUR_Order";
            this.txtAmount_EUR_Order.Size = new System.Drawing.Size(79, 27);
            this.txtAmount_EUR_Order.TabIndex = 163;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label23.Location = new System.Drawing.Point(614, 529);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(150, 19);
            this.label23.TabIndex = 162;
            this.label23.Text = "Order Amount(EUR)";
            // 
            // txtAMOUNT_AUD
            // 
            this.txtAMOUNT_AUD.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txtAMOUNT_AUD.Location = new System.Drawing.Point(1021, 623);
            this.txtAMOUNT_AUD.Name = "txtAMOUNT_AUD";
            this.txtAMOUNT_AUD.Size = new System.Drawing.Size(79, 27);
            this.txtAMOUNT_AUD.TabIndex = 169;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label24.Location = new System.Drawing.Point(864, 627);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(111, 19);
            this.label24.TabIndex = 168;
            this.label24.Text = "提領金額(AUD)";
            // 
            // txtAMOUNT_EUR
            // 
            this.txtAMOUNT_EUR.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txtAMOUNT_EUR.Location = new System.Drawing.Point(770, 623);
            this.txtAMOUNT_EUR.Name = "txtAMOUNT_EUR";
            this.txtAMOUNT_EUR.Size = new System.Drawing.Size(79, 27);
            this.txtAMOUNT_EUR.TabIndex = 167;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label25.Location = new System.Drawing.Point(614, 627);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(108, 19);
            this.label25.TabIndex = 166;
            this.label25.Text = "提領金額(EUR)";
            // 
            // txtAmount_JPY_Order
            // 
            this.txtAmount_JPY_Order.Enabled = false;
            this.txtAmount_JPY_Order.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txtAmount_JPY_Order.Location = new System.Drawing.Point(1276, 526);
            this.txtAmount_JPY_Order.Name = "txtAmount_JPY_Order";
            this.txtAmount_JPY_Order.Size = new System.Drawing.Size(79, 27);
            this.txtAmount_JPY_Order.TabIndex = 171;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label26.Location = new System.Drawing.Point(1119, 530);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(145, 19);
            this.label26.TabIndex = 170;
            this.label26.Text = "Order Amount(JPY)";
            // 
            // txtAMOUNT_JPY
            // 
            this.txtAMOUNT_JPY.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txtAMOUNT_JPY.Location = new System.Drawing.Point(1276, 623);
            this.txtAMOUNT_JPY.Name = "txtAMOUNT_JPY";
            this.txtAMOUNT_JPY.Size = new System.Drawing.Size(79, 27);
            this.txtAMOUNT_JPY.TabIndex = 173;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label27.Location = new System.Drawing.Point(1119, 627);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(103, 19);
            this.label27.TabIndex = 172;
            this.label27.Text = "提領金額(JPY)";
            // 
            // txtAmount_NZD_Order
            // 
            this.txtAmount_NZD_Order.Enabled = false;
            this.txtAmount_NZD_Order.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txtAmount_NZD_Order.Location = new System.Drawing.Point(770, 558);
            this.txtAmount_NZD_Order.Name = "txtAmount_NZD_Order";
            this.txtAmount_NZD_Order.Size = new System.Drawing.Size(79, 27);
            this.txtAmount_NZD_Order.TabIndex = 175;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label29.Location = new System.Drawing.Point(614, 562);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(153, 19);
            this.label29.TabIndex = 174;
            this.label29.Text = "Order Amount(NZD)";
            // 
            // txtAMOUNT_NZD
            // 
            this.txtAMOUNT_NZD.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txtAMOUNT_NZD.Location = new System.Drawing.Point(1276, 655);
            this.txtAMOUNT_NZD.Name = "txtAMOUNT_NZD";
            this.txtAMOUNT_NZD.Size = new System.Drawing.Size(79, 27);
            this.txtAMOUNT_NZD.TabIndex = 177;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label30.Location = new System.Drawing.Point(1119, 659);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(111, 19);
            this.label30.TabIndex = 176;
            this.label30.Text = "提領金額(NZD)";
            // 
            // AddDepositOut_Batch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.ClientSize = new System.Drawing.Size(1370, 722);
            this.Controls.Add(this.txtAMOUNT_NZD);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.txtAmount_NZD_Order);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.txtAMOUNT_JPY);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.txtAmount_JPY_Order);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.txtAMOUNT_AUD);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.txtAMOUNT_EUR);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.txtAmount_AUD_Order);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.txtAmount_EUR_Order);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.txtInterestTimes);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.lblCurrenct);
            this.Controls.Add(this.txt_Bankename);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.checkBox2);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.txtRemark);
            this.Controls.Add(this.txtSWIFT_CODE);
            this.Controls.Add(this.txtBANK_CNAME);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtBatchID);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.dgv_header);
            this.Controls.Add(this.txtACCOUNT);
            this.Controls.Add(this.txtBRANCH_CNAME);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.tb_BALANCE);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtAMOUNT_RMB);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtAMOUNT_NTD);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtAMOUNT_USD);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dteAPPROVE_DATE);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dteCHECK_DATE);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dteAPPLY_DATE);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.txtAmount_RMB_Order);
            this.Controls.Add(this.label42);
            this.Controls.Add(this.txtAmount_NTD_Order);
            this.Controls.Add(this.label40);
            this.Controls.Add(this.txtAmount_USD_Order);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.btnBringOrder);
            this.Controls.Add(this.txtParentOrderNo);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.btnClose);
            this.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "AddDepositOut_Batch";
            this.ShowIcon = false;
            this.Text = "出金輸入";
            this.Load += new System.EventHandler(this.AddDepositOut_Batch_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_header)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.TextBox txtAmount_RMB_Order;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox txtAmount_NTD_Order;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox txtAmount_USD_Order;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Button btnBringOrder;
        private System.Windows.Forms.TextBox txtParentOrderNo;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.DateTimePicker dteAPPLY_DATE;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.DateTimePicker dteCHECK_DATE;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dteAPPROVE_DATE;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtAMOUNT_USD;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtAMOUNT_NTD;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtAMOUNT_RMB;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label tb_BALANCE;
        private System.Windows.Forms.DataGridView dgv_header;
        private System.Windows.Forms.TextBox txtRemark;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtSWIFT_CODE;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtACCOUNT;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtBRANCH_CNAME;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtBANK_CNAME;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.TextBox txtBatchID;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox txtCustomer;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtAmount1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtAmount3;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtAmount2;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtCustomer_en;
        private System.Windows.Forms.TextBox txt_Bankename;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label lblCurrenct;
        private System.Windows.Forms.TextBox txtAmount2ACTUAL;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtCustomer_en2;
        private System.Windows.Forms.TextBox txtInterestTimes;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtAmount_AUD_Order;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtAmount_EUR_Order;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtAMOUNT_AUD;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtAMOUNT_EUR;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtAmount_JPY_Order;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtAMOUNT_JPY;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.DataGridViewTextBoxColumn No;
        private System.Windows.Forms.DataGridViewCheckBoxColumn check;
        private System.Windows.Forms.DataGridViewLinkColumn RowDel;
        private System.Windows.Forms.DataGridViewTextBoxColumn ORDER_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn END_BALANCE_FLAG;
        private System.Windows.Forms.DataGridViewTextBoxColumn END_BALANCE_FLAG2;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUST_NAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn APPLY_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn CHECK_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn APPROVE_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn ORDER_Currency;
        private System.Windows.Forms.DataGridViewTextBoxColumn ORDER_Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn ORDER_INTEREST_MONTHLY;
        private System.Windows.Forms.DataGridViewTextBoxColumn AmountOut;
        private System.Windows.Forms.DataGridViewTextBoxColumn OnPathAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn ACTUAL_AmountOut;
        private System.Windows.Forms.DataGridViewTextBoxColumn BALANCE;
        private System.Windows.Forms.DataGridViewTextBoxColumn IsNet;
        private System.Windows.Forms.DataGridViewTextBoxColumn IsTransToNewForm;
        private System.Windows.Forms.DataGridViewTextBoxColumn NewOrderNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ORDER_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn SCHEDULE;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUST_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn IS_ALL_BALANCE_OUT;
        private System.Windows.Forms.DataGridViewTextBoxColumn BONUS_TSF;
        private System.Windows.Forms.DataGridViewTextBoxColumn NEW_CONTRACT_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn cust_en;
        private System.Windows.Forms.DataGridViewTextBoxColumn cust_en2;
        private System.Windows.Forms.TextBox txtAmount_NZD_Order;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtAMOUNT_NZD;
        private System.Windows.Forms.Label label30;
    }
}
