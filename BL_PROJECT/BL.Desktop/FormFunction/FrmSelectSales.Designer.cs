﻿namespace MK_DEMO.Destop.FormFunction
{
    partial class FrmSelectSales
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.dgv_header = new System.Windows.Forms.DataGridView();
            this.check = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.EMP_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IB_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EMP_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.txtIB_CODE = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_header)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSubmit
            // 
            this.btnSubmit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSubmit.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.btnSubmit.Location = new System.Drawing.Point(64, 364);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(147, 48);
            this.btnSubmit.TabIndex = 9;
            this.btnSubmit.Text = "確定";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.btnClose.Location = new System.Drawing.Point(217, 364);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(147, 48);
            this.btnClose.TabIndex = 8;
            this.btnClose.Text = "關閉";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // dgv_header
            // 
            this.dgv_header.AllowUserToAddRows = false;
            this.dgv_header.AllowUserToDeleteRows = false;
            this.dgv_header.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_header.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("微軟正黑體", 11F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_header.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_header.ColumnHeadersHeight = 25;
            this.dgv_header.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv_header.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.check,
            this.EMP_CODE,
            this.CNAME,
            this.IB_CODE,
            this.EMP_ID});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("微軟正黑體", 11F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_header.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_header.Location = new System.Drawing.Point(12, 37);
            this.dgv_header.Name = "dgv_header";
            this.dgv_header.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("微軟正黑體", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.RoyalBlue;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Yellow;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_header.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_header.RowHeadersWidth = 25;
            this.dgv_header.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgv_header.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(226)))), ((int)(((byte)(239)))));
            this.dgv_header.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_header.RowTemplate.Height = 24;
            this.dgv_header.Size = new System.Drawing.Size(422, 316);
            this.dgv_header.TabIndex = 10;
            this.dgv_header.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_header_CellClick);
            // 
            // check
            // 
            this.check.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.check.FillWeight = 25F;
            this.check.HeaderText = "選";
            this.check.MinimumWidth = 25;
            this.check.Name = "check";
            this.check.ReadOnly = true;
            this.check.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.check.Width = 25;
            // 
            // EMP_CODE
            // 
            this.EMP_CODE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.EMP_CODE.DataPropertyName = "EMP_CODE";
            this.EMP_CODE.HeaderText = "EMP_CODE";
            this.EMP_CODE.Name = "EMP_CODE";
            this.EMP_CODE.ReadOnly = true;
            this.EMP_CODE.Width = 115;
            // 
            // CNAME
            // 
            this.CNAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.CNAME.DataPropertyName = "CNAME";
            this.CNAME.HeaderText = "業務姓名";
            this.CNAME.Name = "CNAME";
            this.CNAME.ReadOnly = true;
            this.CNAME.Width = 115;
            // 
            // IB_CODE
            // 
            this.IB_CODE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.IB_CODE.DataPropertyName = "IB_CODE";
            this.IB_CODE.HeaderText = "IB_CODE";
            this.IB_CODE.Name = "IB_CODE";
            this.IB_CODE.ReadOnly = true;
            this.IB_CODE.Width = 115;
            // 
            // EMP_ID
            // 
            this.EMP_ID.DataPropertyName = "EMP_ID";
            this.EMP_ID.HeaderText = "EMP_ID";
            this.EMP_ID.Name = "EMP_ID";
            this.EMP_ID.ReadOnly = true;
            this.EMP_ID.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 19);
            this.label1.TabIndex = 11;
            this.label1.Text = "IB_CODE";
            // 
            // txtIB_CODE
            // 
            this.txtIB_CODE.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.txtIB_CODE.Location = new System.Drawing.Point(84, 6);
            this.txtIB_CODE.Name = "txtIB_CODE";
            this.txtIB_CODE.Size = new System.Drawing.Size(127, 27);
            this.txtIB_CODE.TabIndex = 12;
            this.txtIB_CODE.TextChanged += new System.EventHandler(this.txtIB_CODE_TextChanged);
            // 
            // FrmSelectSales
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.ClientSize = new System.Drawing.Size(446, 419);
            this.Controls.Add(this.txtIB_CODE);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgv_header);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.btnClose);
            this.Font = new System.Drawing.Font("微軟正黑體", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Name = "FrmSelectSales";
            this.Text = "選取業務";
            this.Load += new System.EventHandler(this.FrmSelectSales_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_header)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.DataGridView dgv_header;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtIB_CODE;
        private System.Windows.Forms.DataGridViewCheckBoxColumn check;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMP_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn CNAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn IB_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMP_ID;
    }
}
