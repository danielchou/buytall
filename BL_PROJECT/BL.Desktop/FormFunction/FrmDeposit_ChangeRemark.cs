﻿using MK.Demo.Logic;
using MK.Demo.Model;
using MK_DEMO.Destop.FormFunction;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MK.Demo.Utility;
using static MK.Demo.Data.enCommon;

namespace MK_DEMO.Destop.FormFunction
{
    public partial class FrmDeposit_ChangeRemark : MK_DEMO.Destop.BaseForm
    {
        public enFormStatus status;
        private string _BatchNo = "";
        private string _BeforeRemark = "";

        public FrmDeposit_ChangeRemark(string strBatchNo, string strBaforeRemark)
        {
            InitializeComponent();
            this.status = enFormStatus.Cancel;

            this._BatchNo = strBatchNo;
            this._BeforeRemark = strBaforeRemark;
            this.txtRemark_Before.Text = this._BeforeRemark;
        }
        

        private void FrmDeposit_ChangeRemark_Load(object sender, EventArgs e)
        {
           
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.status = enFormStatus.Cancel;
            this.Close();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            if (this.txtRemark_After.Text.Trim() == string.Empty)
            {
                MessageBox.Show("請輸入異動後的Remark");
                return;
            }

            if (base.Cust.UpdateTempDepositRemarkByBatchNo(this._BatchNo, this.txtRemark_After.Text))
            {
                MessageBox.Show("整批號調整remark 作業完成");
                this.status = enFormStatus.OK;
                this.Close();
            }
            else
            {
                MessageBox.Show("整批號調整remark 作業失敗");
            }
        }

    }
}
