﻿namespace MK_DEMO.Destop.FormFunction
{
    partial class AddDepositOut
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.lblInfo = new System.Windows.Forms.Label();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.txtAmount_RMB_Order = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.txtAmount_NTD_Order = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.txtAmount_USD_Order = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.btnBringOrder = new System.Windows.Forms.Button();
            this.txtParentOrderNo = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.dteAPPLY_DATE = new System.Windows.Forms.DateTimePicker();
            this.label28 = new System.Windows.Forms.Label();
            this.dteCHECK_DATE = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.dteAPPROVE_DATE = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.txtAMOUNT_USD = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtAMOUNT_NTD = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtAMOUNT_RMB = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tb_BALANCE = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // lblInfo
            // 
            this.lblInfo.AutoSize = true;
            this.lblInfo.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lblInfo.Location = new System.Drawing.Point(15, 9);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(51, 19);
            this.lblInfo.TabIndex = 10;
            this.lblInfo.Text = "Info：";
            this.lblInfo.Visible = false;
            // 
            // btnSubmit
            // 
            this.btnSubmit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSubmit.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnSubmit.Location = new System.Drawing.Point(168, 203);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(259, 48);
            this.btnSubmit.TabIndex = 9;
            this.btnSubmit.Text = "儲存";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnClose.Location = new System.Drawing.Point(466, 203);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(259, 48);
            this.btnClose.TabIndex = 8;
            this.btnClose.Text = "關閉";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // txtAmount_RMB_Order
            // 
            this.txtAmount_RMB_Order.Enabled = false;
            this.txtAmount_RMB_Order.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txtAmount_RMB_Order.Location = new System.Drawing.Point(701, 64);
            this.txtAmount_RMB_Order.Name = "txtAmount_RMB_Order";
            this.txtAmount_RMB_Order.Size = new System.Drawing.Size(79, 27);
            this.txtAmount_RMB_Order.TabIndex = 117;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label42.Location = new System.Drawing.Point(523, 68);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(154, 19);
            this.label42.TabIndex = 116;
            this.label42.Text = "Order Amount(RMB)";
            // 
            // txtAmount_NTD_Order
            // 
            this.txtAmount_NTD_Order.Enabled = false;
            this.txtAmount_NTD_Order.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txtAmount_NTD_Order.Location = new System.Drawing.Point(430, 64);
            this.txtAmount_NTD_Order.Name = "txtAmount_NTD_Order";
            this.txtAmount_NTD_Order.Size = new System.Drawing.Size(79, 27);
            this.txtAmount_NTD_Order.TabIndex = 115;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label40.Location = new System.Drawing.Point(272, 68);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(152, 19);
            this.label40.TabIndex = 114;
            this.label40.Text = "Order Amount(NTD)";
            // 
            // txtAmount_USD_Order
            // 
            this.txtAmount_USD_Order.Enabled = false;
            this.txtAmount_USD_Order.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txtAmount_USD_Order.Location = new System.Drawing.Point(175, 64);
            this.txtAmount_USD_Order.Name = "txtAmount_USD_Order";
            this.txtAmount_USD_Order.Size = new System.Drawing.Size(79, 27);
            this.txtAmount_USD_Order.TabIndex = 113;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label39.Location = new System.Drawing.Point(15, 68);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(152, 19);
            this.label39.TabIndex = 112;
            this.label39.Text = "Order Amount(USD)";
            // 
            // btnBringOrder
            // 
            this.btnBringOrder.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnBringOrder.Location = new System.Drawing.Point(196, 31);
            this.btnBringOrder.Name = "btnBringOrder";
            this.btnBringOrder.Size = new System.Drawing.Size(36, 25);
            this.btnBringOrder.TabIndex = 111;
            this.btnBringOrder.Text = "...";
            this.btnBringOrder.UseVisualStyleBackColor = true;
            this.btnBringOrder.Click += new System.EventHandler(this.btnBringOrder_Click);
            // 
            // txtParentOrderNo
            // 
            this.txtParentOrderNo.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txtParentOrderNo.Location = new System.Drawing.Point(104, 31);
            this.txtParentOrderNo.Name = "txtParentOrderNo";
            this.txtParentOrderNo.Size = new System.Drawing.Size(86, 27);
            this.txtParentOrderNo.TabIndex = 110;
            this.txtParentOrderNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtParentOrderNo_KeyDown);
            this.txtParentOrderNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtParentOrderNo_KeyPress);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label37.Location = new System.Drawing.Point(15, 35);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(76, 19);
            this.label37.TabIndex = 109;
            this.label37.Text = "Order No";
            // 
            // dteAPPLY_DATE
            // 
            this.dteAPPLY_DATE.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.dteAPPLY_DATE.Location = new System.Drawing.Point(111, 96);
            this.dteAPPLY_DATE.Name = "dteAPPLY_DATE";
            this.dteAPPLY_DATE.Size = new System.Drawing.Size(143, 27);
            this.dteAPPLY_DATE.TabIndex = 119;
            this.dteAPPLY_DATE.ValueChanged += new System.EventHandler(this.dte_ValueChanged);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label28.Location = new System.Drawing.Point(15, 100);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(84, 19);
            this.label28.TabIndex = 118;
            this.label28.Text = "出金申請日";
            // 
            // dteCHECK_DATE
            // 
            this.dteCHECK_DATE.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.dteCHECK_DATE.Location = new System.Drawing.Point(366, 96);
            this.dteCHECK_DATE.Name = "dteCHECK_DATE";
            this.dteCHECK_DATE.Size = new System.Drawing.Size(143, 27);
            this.dteCHECK_DATE.TabIndex = 121;
            this.dteCHECK_DATE.Visible = false;
            this.dteCHECK_DATE.ValueChanged += new System.EventHandler(this.dte_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(272, 100);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 19);
            this.label1.TabIndex = 120;
            this.label1.Text = "查核日";
            this.label1.Visible = false;
            // 
            // dteAPPROVE_DATE
            // 
            this.dteAPPROVE_DATE.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.dteAPPROVE_DATE.Location = new System.Drawing.Point(637, 96);
            this.dteAPPROVE_DATE.Name = "dteAPPROVE_DATE";
            this.dteAPPROVE_DATE.Size = new System.Drawing.Size(143, 27);
            this.dteAPPROVE_DATE.TabIndex = 123;
            this.dteAPPROVE_DATE.Visible = false;
            this.dteAPPROVE_DATE.ValueChanged += new System.EventHandler(this.dte_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(523, 100);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 19);
            this.label2.TabIndex = 122;
            this.label2.Text = "審批日";
            this.label2.Visible = false;
            // 
            // txtAMOUNT_USD
            // 
            this.txtAMOUNT_USD.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txtAMOUNT_USD.Location = new System.Drawing.Point(175, 130);
            this.txtAMOUNT_USD.Name = "txtAMOUNT_USD";
            this.txtAMOUNT_USD.Size = new System.Drawing.Size(79, 27);
            this.txtAMOUNT_USD.TabIndex = 125;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label3.Location = new System.Drawing.Point(15, 134);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(110, 19);
            this.label3.TabIndex = 124;
            this.label3.Text = "提領金額(USD)";
            // 
            // txtAMOUNT_NTD
            // 
            this.txtAMOUNT_NTD.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txtAMOUNT_NTD.Location = new System.Drawing.Point(430, 130);
            this.txtAMOUNT_NTD.Name = "txtAMOUNT_NTD";
            this.txtAMOUNT_NTD.Size = new System.Drawing.Size(79, 27);
            this.txtAMOUNT_NTD.TabIndex = 127;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label4.Location = new System.Drawing.Point(272, 134);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(110, 19);
            this.label4.TabIndex = 126;
            this.label4.Text = "提領金額(NTD)";
            // 
            // txtAMOUNT_RMB
            // 
            this.txtAMOUNT_RMB.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txtAMOUNT_RMB.Location = new System.Drawing.Point(701, 130);
            this.txtAMOUNT_RMB.Name = "txtAMOUNT_RMB";
            this.txtAMOUNT_RMB.Size = new System.Drawing.Size(79, 27);
            this.txtAMOUNT_RMB.TabIndex = 129;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label5.Location = new System.Drawing.Point(523, 134);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(112, 19);
            this.label5.TabIndex = 128;
            this.label5.Text = "提領金額(RMB)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label6.Location = new System.Drawing.Point(15, 165);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(99, 19);
            this.label6.TabIndex = 130;
            this.label6.Text = "是否網路出金";
            // 
            // comboBox1
            // 
            this.comboBox1.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Y",
            "N"});
            this.comboBox1.Location = new System.Drawing.Point(133, 161);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 27);
            this.comboBox1.TabIndex = 131;
            // 
            // comboBox2
            // 
            this.comboBox2.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "Y",
            "N"});
            this.comboBox2.Location = new System.Drawing.Point(388, 161);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(121, 27);
            this.comboBox2.TabIndex = 133;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label7.Location = new System.Drawing.Point(272, 165);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(99, 19);
            this.label7.TabIndex = 132;
            this.label7.Text = "利息內轉新單";
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.textBox1.Location = new System.Drawing.Point(701, 161);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(79, 27);
            this.textBox1.TabIndex = 135;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label8.Location = new System.Drawing.Point(523, 165);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(169, 19);
            this.label8.TabIndex = 134;
            this.label8.Text = "合約編號(利息內轉新單)";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label9.Location = new System.Drawing.Point(272, 35);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(39, 19);
            this.label9.TabIndex = 136;
            this.label9.Text = "餘額";
            // 
            // tb_BALANCE
            // 
            this.tb_BALANCE.Location = new System.Drawing.Point(321, 29);
            this.tb_BALANCE.Name = "tb_BALANCE";
            this.tb_BALANCE.Size = new System.Drawing.Size(83, 29);
            this.tb_BALANCE.TabIndex = 137;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.checkBox1.Location = new System.Drawing.Point(433, 33);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(88, 23);
            this.checkBox1.TabIndex = 138;
            this.checkBox1.Text = "餘額結清";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // AddDepositOut
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.ClientSize = new System.Drawing.Size(826, 263);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.tb_BALANCE);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtAMOUNT_RMB);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtAMOUNT_NTD);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtAMOUNT_USD);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dteAPPROVE_DATE);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dteCHECK_DATE);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dteAPPLY_DATE);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.txtAmount_RMB_Order);
            this.Controls.Add(this.label42);
            this.Controls.Add(this.txtAmount_NTD_Order);
            this.Controls.Add(this.label40);
            this.Controls.Add(this.txtAmount_USD_Order);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.btnBringOrder);
            this.Controls.Add(this.txtParentOrderNo);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.lblInfo);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.btnClose);
            this.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Name = "AddDepositOut";
            this.Text = "出金輸入";
            this.Load += new System.EventHandler(this.AddDepositOut_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Label lblInfo;
        private System.Windows.Forms.TextBox txtAmount_RMB_Order;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox txtAmount_NTD_Order;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox txtAmount_USD_Order;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Button btnBringOrder;
        private System.Windows.Forms.TextBox txtParentOrderNo;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.DateTimePicker dteAPPLY_DATE;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.DateTimePicker dteCHECK_DATE;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dteAPPROVE_DATE;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtAMOUNT_USD;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtAMOUNT_NTD;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtAMOUNT_RMB;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label tb_BALANCE;
        private System.Windows.Forms.CheckBox checkBox1;
    }
}
