﻿namespace MK_DEMO.Destop.FormFunction
{
    partial class frmDepositOut_BatchAmount
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv_header = new System.Windows.Forms.DataGridView();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.No = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.check = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ORDER_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FROM_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.YEAR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ORDER_Currency = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ORDER_Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BALANCE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AmountOut = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ACTUAL_AmountOut = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.END_BALANCE_FLAG = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PKG_INTEREST_TIMES = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ORDER_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SCHEDULE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUST_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IS_ALL_BALANCE_OUT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BONUS_TSF = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NEW_CONTRACT_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cust_en = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_header)).BeginInit();
            this.SuspendLayout();
            // 
            // dgv_header
            // 
            this.dgv_header.AllowUserToAddRows = false;
            this.dgv_header.AllowUserToDeleteRows = false;
            this.dgv_header.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_header.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("新細明體", 11F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_header.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_header.ColumnHeadersHeight = 25;
            this.dgv_header.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv_header.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.No,
            this.check,
            this.ORDER_NO,
            this.FROM_DATE,
            this.YEAR,
            this.ORDER_Currency,
            this.ORDER_Amount,
            this.BALANCE,
            this.AmountOut,
            this.ACTUAL_AmountOut,
            this.END_BALANCE_FLAG,
            this.PKG_INTEREST_TIMES,
            this.ORDER_ID,
            this.SCHEDULE,
            this.CUST_ID,
            this.IS_ALL_BALANCE_OUT,
            this.BONUS_TSF,
            this.NEW_CONTRACT_NO,
            this.cust_en});
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("新細明體", 11F);
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_header.DefaultCellStyle = dataGridViewCellStyle7;
            this.dgv_header.Location = new System.Drawing.Point(12, 12);
            this.dgv_header.Name = "dgv_header";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.RoyalBlue;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Yellow;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_header.RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.dgv_header.RowHeadersWidth = 25;
            this.dgv_header.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgv_header.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(226)))), ((int)(((byte)(239)))));
            this.dgv_header.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_header.RowTemplate.Height = 24;
            this.dgv_header.Size = new System.Drawing.Size(970, 342);
            this.dgv_header.TabIndex = 139;
            this.dgv_header.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_header_CellClick);
            this.dgv_header.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_header_CellEnter);
            this.dgv_header.CellLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_header_CellLeave);
            this.dgv_header.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgv_header_EditingControlShowing);
            this.dgv_header.Sorted += new System.EventHandler(this.dgv_header_Sorted);
            // 
            // btnSubmit
            // 
            this.btnSubmit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSubmit.Font = new System.Drawing.Font("新細明體", 11F);
            this.btnSubmit.Location = new System.Drawing.Point(342, 360);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(221, 42);
            this.btnSubmit.TabIndex = 11;
            this.btnSubmit.Text = "確認";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Font = new System.Drawing.Font("新細明體", 11F);
            this.btnClose.Location = new System.Drawing.Point(624, 360);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(221, 42);
            this.btnClose.TabIndex = 10;
            this.btnClose.Text = "取消";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // No
            // 
            this.No.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.No.HeaderText = "No";
            this.No.Name = "No";
            this.No.ReadOnly = true;
            this.No.Width = 35;
            // 
            // check
            // 
            this.check.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.check.FillWeight = 25F;
            this.check.HeaderText = "選";
            this.check.MinimumWidth = 25;
            this.check.Name = "check";
            this.check.ReadOnly = true;
            this.check.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.check.Visible = false;
            this.check.Width = 25;
            // 
            // ORDER_NO
            // 
            this.ORDER_NO.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ORDER_NO.DataPropertyName = "ORDER_NO";
            this.ORDER_NO.HeaderText = "Order No";
            this.ORDER_NO.Name = "ORDER_NO";
            this.ORDER_NO.ReadOnly = true;
            this.ORDER_NO.Width = 85;
            // 
            // FROM_DATE
            // 
            this.FROM_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.FROM_DATE.DataPropertyName = "FROM_DATE";
            this.FROM_DATE.HeaderText = "合約啟始日";
            this.FROM_DATE.Name = "FROM_DATE";
            this.FROM_DATE.ReadOnly = true;
            this.FROM_DATE.Width = 120;
            // 
            // YEAR
            // 
            this.YEAR.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.YEAR.DataPropertyName = "YEAR";
            this.YEAR.HeaderText = "年期";
            this.YEAR.Name = "YEAR";
            this.YEAR.ReadOnly = true;
            this.YEAR.Width = 90;
            // 
            // ORDER_Currency
            // 
            this.ORDER_Currency.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ORDER_Currency.DataPropertyName = "ORDER_Currency";
            this.ORDER_Currency.HeaderText = "幣別";
            this.ORDER_Currency.Name = "ORDER_Currency";
            this.ORDER_Currency.ReadOnly = true;
            this.ORDER_Currency.Width = 90;
            // 
            // ORDER_Amount
            // 
            this.ORDER_Amount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ORDER_Amount.DataPropertyName = "ORDER_Amount";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "N2";
            dataGridViewCellStyle2.NullValue = null;
            this.ORDER_Amount.DefaultCellStyle = dataGridViewCellStyle2;
            this.ORDER_Amount.HeaderText = "本金";
            this.ORDER_Amount.Name = "ORDER_Amount";
            this.ORDER_Amount.ReadOnly = true;
            this.ORDER_Amount.Width = 90;
            // 
            // BALANCE
            // 
            this.BALANCE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.BALANCE.DataPropertyName = "BALANCE";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = null;
            this.BALANCE.DefaultCellStyle = dataGridViewCellStyle3;
            this.BALANCE.HeaderText = "餘額";
            this.BALANCE.Name = "BALANCE";
            this.BALANCE.ReadOnly = true;
            this.BALANCE.Width = 80;
            // 
            // AmountOut
            // 
            this.AmountOut.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.AmountOut.DataPropertyName = "Amount";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N2";
            dataGridViewCellStyle4.NullValue = null;
            this.AmountOut.DefaultCellStyle = dataGridViewCellStyle4;
            this.AmountOut.HeaderText = "提領金額";
            this.AmountOut.Name = "AmountOut";
            this.AmountOut.Width = 90;
            // 
            // ACTUAL_AmountOut
            // 
            this.ACTUAL_AmountOut.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ACTUAL_AmountOut.DataPropertyName = "ACTUAL_Amount";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "N2";
            dataGridViewCellStyle5.NullValue = null;
            this.ACTUAL_AmountOut.DefaultCellStyle = dataGridViewCellStyle5;
            this.ACTUAL_AmountOut.HeaderText = "實際提領金額";
            this.ACTUAL_AmountOut.Name = "ACTUAL_AmountOut";
            this.ACTUAL_AmountOut.Width = 105;
            // 
            // END_BALANCE_FLAG
            // 
            this.END_BALANCE_FLAG.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.END_BALANCE_FLAG.DataPropertyName = "END_BALANCE_FLAG";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "N2";
            dataGridViewCellStyle6.NullValue = null;
            this.END_BALANCE_FLAG.DefaultCellStyle = dataGridViewCellStyle6;
            this.END_BALANCE_FLAG.HeaderText = "餘額結清";
            this.END_BALANCE_FLAG.Name = "END_BALANCE_FLAG";
            this.END_BALANCE_FLAG.ReadOnly = true;
            this.END_BALANCE_FLAG.Width = 95;
            // 
            // PKG_INTEREST_TIMES
            // 
            this.PKG_INTEREST_TIMES.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.PKG_INTEREST_TIMES.DataPropertyName = "PKG_INTEREST_TIMES";
            this.PKG_INTEREST_TIMES.HeaderText = "派息次數";
            this.PKG_INTEREST_TIMES.MinimumWidth = 80;
            this.PKG_INTEREST_TIMES.Name = "PKG_INTEREST_TIMES";
            this.PKG_INTEREST_TIMES.ReadOnly = true;
            this.PKG_INTEREST_TIMES.Width = 80;
            // 
            // ORDER_ID
            // 
            this.ORDER_ID.DataPropertyName = "ORDER_ID";
            this.ORDER_ID.HeaderText = "ORDER_ID";
            this.ORDER_ID.Name = "ORDER_ID";
            this.ORDER_ID.ReadOnly = true;
            this.ORDER_ID.Visible = false;
            // 
            // SCHEDULE
            // 
            this.SCHEDULE.DataPropertyName = "SCHEDULE";
            this.SCHEDULE.HeaderText = "SCHEDULE";
            this.SCHEDULE.Name = "SCHEDULE";
            this.SCHEDULE.ReadOnly = true;
            this.SCHEDULE.Visible = false;
            // 
            // CUST_ID
            // 
            this.CUST_ID.DataPropertyName = "CUST_ID";
            this.CUST_ID.HeaderText = "CUST_ID";
            this.CUST_ID.Name = "CUST_ID";
            this.CUST_ID.ReadOnly = true;
            this.CUST_ID.Visible = false;
            // 
            // IS_ALL_BALANCE_OUT
            // 
            this.IS_ALL_BALANCE_OUT.DataPropertyName = "IS_ALL_BALANCE_OUT";
            this.IS_ALL_BALANCE_OUT.HeaderText = "IS_ALL_BALANCE_OUT";
            this.IS_ALL_BALANCE_OUT.Name = "IS_ALL_BALANCE_OUT";
            this.IS_ALL_BALANCE_OUT.ReadOnly = true;
            this.IS_ALL_BALANCE_OUT.Visible = false;
            // 
            // BONUS_TSF
            // 
            this.BONUS_TSF.DataPropertyName = "BONUS_TSF";
            this.BONUS_TSF.HeaderText = "BONUS_TSF";
            this.BONUS_TSF.Name = "BONUS_TSF";
            this.BONUS_TSF.ReadOnly = true;
            this.BONUS_TSF.Visible = false;
            // 
            // NEW_CONTRACT_NO
            // 
            this.NEW_CONTRACT_NO.DataPropertyName = "NEW_CONTRACT_NO";
            this.NEW_CONTRACT_NO.HeaderText = "NEW_CONTRACT_NO";
            this.NEW_CONTRACT_NO.Name = "NEW_CONTRACT_NO";
            this.NEW_CONTRACT_NO.ReadOnly = true;
            this.NEW_CONTRACT_NO.Visible = false;
            // 
            // cust_en
            // 
            this.cust_en.DataPropertyName = "CUST_ENAME";
            this.cust_en.HeaderText = "cust_en";
            this.cust_en.MinimumWidth = 100;
            this.cust_en.Name = "cust_en";
            this.cust_en.Visible = false;
            // 
            // frmDepositOut_BatchAmount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.ClientSize = new System.Drawing.Size(994, 414);
            this.Controls.Add(this.dgv_header);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.btnClose);
            this.Name = "frmDepositOut_BatchAmount";
            this.Text = "批次出金";
            this.Load += new System.EventHandler(this.frmDepositOut_BatchAmount_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_header)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.DataGridView dgv_header;
        private System.Windows.Forms.DataGridViewTextBoxColumn No;
        private System.Windows.Forms.DataGridViewCheckBoxColumn check;
        private System.Windows.Forms.DataGridViewTextBoxColumn ORDER_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn FROM_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn YEAR;
        private System.Windows.Forms.DataGridViewTextBoxColumn ORDER_Currency;
        private System.Windows.Forms.DataGridViewTextBoxColumn ORDER_Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn BALANCE;
        private System.Windows.Forms.DataGridViewTextBoxColumn AmountOut;
        private System.Windows.Forms.DataGridViewTextBoxColumn ACTUAL_AmountOut;
        private System.Windows.Forms.DataGridViewTextBoxColumn END_BALANCE_FLAG;
        private System.Windows.Forms.DataGridViewTextBoxColumn PKG_INTEREST_TIMES;
        private System.Windows.Forms.DataGridViewTextBoxColumn ORDER_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn SCHEDULE;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUST_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn IS_ALL_BALANCE_OUT;
        private System.Windows.Forms.DataGridViewTextBoxColumn BONUS_TSF;
        private System.Windows.Forms.DataGridViewTextBoxColumn NEW_CONTRACT_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn cust_en;
    }
}
