﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using MK.Demo.Utility;
using MK.Demo.Model;
using MK.Demo.Logic;
using System.Threading.Tasks;
using System.Linq;
using static MK.Demo.Data.enCommon;

namespace MK_DEMO.Destop.FormFunction
{
    public partial class AddDepositOut : MK_DEMO.Destop.BaseForm
    {
        public enFormStatus status;
        private string strYear;
        private string strMonth;
        
        public AddDepositOut(string Year, string Month)
        {
            InitializeComponent();
            this.strYear = Year;
            this.strMonth = Month;

            this.dteAPPLY_DATE.Value = DateTime.Now;
            this.dteAPPLY_DATE.Text = string.Empty;
            this.dteAPPLY_DATE.Format = DateTimePickerFormat.Custom;
            this.dteAPPLY_DATE.CustomFormat = " ";

            this.dteAPPROVE_DATE.Value = DateTime.Now;
            this.dteAPPROVE_DATE.Text = string.Empty;
            this.dteAPPROVE_DATE.Format = DateTimePickerFormat.Custom;
            this.dteAPPROVE_DATE.CustomFormat = " ";


            this.dteCHECK_DATE.Value = DateTime.Now;
            this.dteCHECK_DATE.Text = string.Empty;
            this.dteCHECK_DATE.Format = DateTimePickerFormat.Custom;
            this.dteCHECK_DATE.CustomFormat = " ";
        }

        public AddDepositOut(string Year, string Month, string strOrderNo) : this(Year, Month)
        {
            this.SetOrderNoDafuleValue(strOrderNo, false);
        }

        private void AddDepositOut_Load(object sender, EventArgs e)
        {
            this.lblInfo.Text = "Info:　Year " + this.strYear + "　Month " + this.strMonth;

            this.comboBox1.SelectedIndex = 1;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            status = enFormStatus.Cancel;
            this.Close();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            if (this.DataValidation())
            {
                var dataSave = this.GetSaveData;

                if (base.Cust.AddNewACCOUNT_TXN_TEMP(dataSave))
                {
                    MessageBox.Show("資料儲存成功");
                    status = enFormStatus.OK;
                    this.Close();
                }
            }
        }


        private BL_ACCOUNT_TXN_TEMP GetSaveData
        {
            get
            {
                var data = new BL_ACCOUNT_TXN_TEMP();
                decimal decTry;
                DateTime dtTry;
                if (decimal.TryParse(this.txtParentOrderNo.Tag.ConvertToString(), out decTry)) data.ORDER_ID = decTry;
                if (decimal.TryParse(this.txtAMOUNT_USD.Text, out decTry)) data.AMOUNT_USD = decTry;
                if (decimal.TryParse(this.txtAMOUNT_NTD.Text, out decTry)) data.AMOUNT_NTD = decTry;
                if (decimal.TryParse(this.txtAMOUNT_RMB.Text, out decTry)) data.AMOUNT_RMB = decTry;
                if (DateTime.TryParse(this.dteAPPLY_DATE.Text, out dtTry)) data.APPLY_DATE = dtTry;
                if (DateTime.TryParse(this.dteCHECK_DATE.Text, out dtTry)) data.CHECK_DATE = dtTry;
                if (DateTime.TryParse(this.dteAPPROVE_DATE.Text, out dtTry)) data.APPROVE_DATE = dtTry;
                data.WITHDRAWAL_TYPE = this.comboBox1.Text;
                data.BONUS_TSF = this.comboBox2.Text;
                data.NEW_CONTRACT_NO = this.textBox1.Text;
                return data;
            }
        }

        private bool DataValidation()
        {
            var msg = string.Empty;
            var result = true;

            if (this.txtParentOrderNo.Text.Trim() == string.Empty)
            {
                msg += "請選擇「Order No」" + Environment.NewLine;
            }

            var data = base.Cust.GetUnExpiredOrderDatabyOrderID(this.txtParentOrderNo.Text.Trim());
            if (data.Count == 0)
            {
                msg += "此「Order No」不存在，請重新輸入" + Environment.NewLine;
            }

            //  出金申請不能大於 餘額 – 本金
            if (this.checkBox1.Checked == false)
            {
                #region 一般出金

                decimal decAmount;
                decimal decOutAmount;
                decimal decBALANCE;
                if (this.txtAMOUNT_USD.Enabled)
                {
                    if
                        (
                            decimal.TryParse(this.txtAmount_USD_Order.Text.Trim(), out decAmount) &&
                            decimal.TryParse(this.txtAMOUNT_USD.Text.Trim(), out decOutAmount) &&
                            decimal.TryParse(this.tb_BALANCE.Text.Trim(), out decBALANCE)
                        )
                    {
                        if (decOutAmount > decBALANCE - decAmount)
                        {
                            msg += "出金申請不能大於 餘額 – 本金" + Environment.NewLine;
                        }
                    }
                    else
                    {
                        msg += "出金申請不能大於 餘額 – 本金" + Environment.NewLine;
                    }
                }
                else if (this.txtAMOUNT_RMB.Enabled)
                {
                    if
                    (
                        decimal.TryParse(this.txtAmount_RMB_Order.Text.Trim(), out decAmount) &&
                        decimal.TryParse(this.txtAMOUNT_RMB.Text.Trim(), out decOutAmount) &&
                        decimal.TryParse(this.tb_BALANCE.Text.Trim(), out decBALANCE)
                    )
                    {
                        if (decOutAmount > decBALANCE - decAmount)
                        {
                            msg += "出金申請不能大於 餘額 – 本金" + Environment.NewLine;
                        }
                    }
                    else
                    {
                        msg += "出金申請不能大於 餘額 – 本金" + Environment.NewLine;
                    }
                }
                else if (this.txtAMOUNT_NTD.Enabled)
                {
                    if
                    (
                        decimal.TryParse(this.txtAmount_NTD_Order.Text.Trim(), out decAmount) &&
                        decimal.TryParse(this.txtAMOUNT_NTD.Text.Trim(), out decOutAmount) &&
                        decimal.TryParse(this.tb_BALANCE.Text.Trim(), out decBALANCE)
                    )
                    {
                        if (decOutAmount > decBALANCE - decAmount)
                        {
                            msg += "出金申請不能大於 餘額 – 本金" + Environment.NewLine;
                        }
                    }
                    else
                    {
                        msg += "出金申請不能大於 餘額 – 本金" + Environment.NewLine;
                    }
                }
                else
                {
                    msg += "出金申請不能大於 餘額 – 本金" + Environment.NewLine;
                }

                #endregion
            }
            //else
            //{
            //    #region 餘額結清

            //    decimal decBalance;
            //    decimal decOutAmount;
            //    if (this.txtAMOUNT_USD.Enabled)
            //    {
            //        if
            //            (
            //                decimal.TryParse(this.txtAMOUNT_USD.Text.Trim(), out decOutAmount) &&
            //                decimal.TryParse(this.tb_BALANCE.Text.Trim(), out decBalance)
            //            )
            //        {
            //            if (decBalance != decOutAmount)
            //            {
            //                msg += "出金申請需等於餘額" + Environment.NewLine;
            //            }
            //        }
            //        else
            //        {
            //            msg += "出金申請需等於餘額" + Environment.NewLine;
            //        }
            //    }
            //    else if (this.txtAMOUNT_NTD.Enabled)
            //    {
            //        if
            //            (
            //                decimal.TryParse(this.txtAMOUNT_NTD.Text.Trim(), out decOutAmount) &&
            //                decimal.TryParse(this.tb_BALANCE.Text.Trim(), out decBalance)
            //            )
            //        {
            //            if (decBalance != decOutAmount)
            //            {
            //                msg += "出金申請需等於餘額" + Environment.NewLine;
            //            }
            //        }
            //        else
            //        {
            //            msg += "出金申請需等於餘額" + Environment.NewLine;
            //        }
            //    }
            //    else if (this.txtAMOUNT_RMB.Enabled)
            //    {
            //        if
            //            (
            //                decimal.TryParse(this.txtAMOUNT_RMB.Text.Trim(), out decOutAmount) &&
            //                decimal.TryParse(this.tb_BALANCE.Text.Trim(), out decBalance)
            //            )
            //        {
            //            if (decBalance != decOutAmount)
            //            {
            //                msg += "出金申請需等於餘額" + Environment.NewLine;
            //            }
            //        }
            //        else
            //        {
            //            msg += "出金申請需等於餘額" + Environment.NewLine;
            //        }
            //    }
            //    else
            //    {
            //        msg += "出金申請需等於餘額" + Environment.NewLine;
            //    }
            //    #endregion
            //}


            if (msg != string.Empty)
            {
                MessageBox.Show(msg);
                result = false;
            }
            return result;
        }

        private void btnBringOrder_Click(object sender, EventArgs e)
        {
            var frm = new frmContractDetail_Deposit(this.strYear, this.strMonth)
            { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
            frm.ShowDialog();
            if (frm.status == MK.Demo.Data.enCommon.enFormStatus.OK)
            {
                this.txtParentOrderNo.Text = frm.strORDER_NO.ConvertToString();
                this.txtParentOrderNo.Tag = frm.strORDER_ID.ConvertToString();
                this.txtAmount_NTD_Order.Text = frm.strAMOUNT_NTD.ConvertToString();
                this.txtAmount_RMB_Order.Text = frm.strAMOUNT_RMB.ConvertToString();
                this.txtAmount_USD_Order.Text = frm.strAMOUNT_USD.ConvertToString();
                //--------------------------------------------------------------
                //get BALANCE
                var bl = new logicCust(Global.IsTestMode).GetBALANCE(frm.strORDER_ID.ConvertToString());
                this.tb_BALANCE.Text = bl.ToString();

                if (this.txtAmount_NTD_Order.Text.Trim() != string.Empty)
                {
                    this.txtAMOUNT_NTD.Enabled = true;
                    this.txtAMOUNT_RMB.Enabled = false;
                    this.txtAMOUNT_USD.Enabled = false;
                }

                if (this.txtAmount_RMB_Order.Text.Trim() != string.Empty)
                {
                    this.txtAMOUNT_NTD.Enabled = false;
                    this.txtAMOUNT_RMB.Enabled = true;
                    this.txtAMOUNT_USD.Enabled = false;
                }

                if (this.txtAmount_USD_Order.Text.Trim() != string.Empty)
                {
                    this.txtAMOUNT_NTD.Enabled = false;
                    this.txtAMOUNT_RMB.Enabled = false;
                    this.txtAMOUNT_USD.Enabled = true;
                }
            }
            frm.Dispose();
            frm = null;
        }

        private void dte_ValueChanged(object sender, EventArgs e)
        {
            (sender as DateTimePicker).Format = DateTimePickerFormat.Long;
        }

        private void txtParentOrderNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Tab)
            {
               
            }
        }

        private void txtParentOrderNo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Tab || e.KeyCode == Keys.Enter)
            {
                this.SetOrderNoDafuleValue(this.txtParentOrderNo.Text.Trim());
            }
        }

        private void SetOrderNoDafuleValue(string strOrderNo, bool IsShowError = true)
        {
            this.txtAMOUNT_NTD.Text = string.Empty;
            this.txtAMOUNT_RMB.Text = string.Empty;
            this.txtAMOUNT_USD.Text = string.Empty;

            //  Do Search
            var data = base.Cust.GetUnExpiredOrderDatabyOrderID(strOrderNo);
            if (data.Count > 0)
            {
                var dataSingle = data[0];


                this.txtParentOrderNo.Text = dataSingle.ORDER_NO.ConvertToString();
                this.txtParentOrderNo.Tag = dataSingle.ORDER_ID == null ? "" : dataSingle.ORDER_ID.Value.ConvertToString();
                this.txtAmount_NTD_Order.Text = dataSingle.AMOUNT_NTD == null ? "" : dataSingle.AMOUNT_NTD.Value.ConvertToString();
                this.txtAmount_RMB_Order.Text = dataSingle.AMOUNT_RMB == null ? "" : dataSingle.AMOUNT_RMB.Value.ConvertToString();
                this.txtAmount_USD_Order.Text = dataSingle.AMOUNT_USD == null ? "" : dataSingle.AMOUNT_USD.Value.ConvertToString();

                var bl = new logicCust(Global.IsTestMode).GetBALANCE(this.txtParentOrderNo.Tag.ConvertToString());
                this.tb_BALANCE.Text = bl.ToString();


                if (this.txtAmount_NTD_Order.Text.Trim() != string.Empty)
                {
                    this.txtAMOUNT_NTD.Enabled = true;
                    this.txtAMOUNT_RMB.Enabled = false;
                    this.txtAMOUNT_USD.Enabled = false;
                }

                if (this.txtAmount_RMB_Order.Text.Trim() != string.Empty)
                {
                    this.txtAMOUNT_NTD.Enabled = false;
                    this.txtAMOUNT_RMB.Enabled = true;
                    this.txtAMOUNT_USD.Enabled = false;
                }

                if (this.txtAmount_USD_Order.Text.Trim() != string.Empty)
                {
                    this.txtAMOUNT_NTD.Enabled = false;
                    this.txtAMOUNT_RMB.Enabled = false;
                    this.txtAMOUNT_USD.Enabled = true;
                }

            }
            else
            {
                //  Clean
                this.txtParentOrderNo.Text = string.Empty;
                this.txtParentOrderNo.Tag = string.Empty;
                this.txtAmount_NTD_Order.Text = string.Empty;
                this.txtAmount_RMB_Order.Text = string.Empty;
                this.txtAmount_USD_Order.Text = string.Empty;
                //--------------------------------------------------------------
                this.tb_BALANCE.Text = string.Empty;

                this.txtAMOUNT_NTD.Enabled = true;
                this.txtAMOUNT_RMB.Enabled = true;
                this.txtAMOUNT_USD.Enabled = true;

                if (IsShowError)
                {
                    MessageBox.Show("此「Order No」不存在，請重新輸入");
                }
            }

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (this.checkBox1.Checked)
            {
                if (this.txtAMOUNT_USD.Enabled)
                {
                    this.txtAMOUNT_USD.Text = this.tb_BALANCE.Text;
                }
                else if (this.txtAMOUNT_NTD.Enabled)
                {
                    this.txtAMOUNT_NTD.Text = this.tb_BALANCE.Text;
                }
                else if (this.txtAMOUNT_RMB.Enabled)
                {
                    this.txtAMOUNT_RMB.Text = this.tb_BALANCE.Text;
                }
            }
            else
            {
               
            }
        }
    }
}
