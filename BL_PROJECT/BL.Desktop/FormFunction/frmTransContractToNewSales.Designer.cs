﻿namespace MK_DEMO.Destop.FormFunction
{
    partial class frmTransContractToNewSales
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnClose = new System.Windows.Forms.Button();
            this.txtCust = new System.Windows.Forms.TextBox();
            this.btnBringCust = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSales = new System.Windows.Forms.TextBox();
            this.btnBringSales = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.dgv_header = new System.Windows.Forms.DataGridView();
            this.check = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ORDER_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUST_CNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUST_ENAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUST_CNAME2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUST_ENAME2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SALES = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ORDER_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUST_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chkALL = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnConfirm = new System.Windows.Forms.Button();
            this.txtIBCodeTo = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.txtSalesTo = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnCleanSelection_Sales = new System.Windows.Forms.Button();
            this.btnCleanSelection_Cust = new System.Windows.Forms.Button();
            this.lblDataCount = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_header)).BeginInit();
            this.dgv_header.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(782, 461);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(181, 48);
            this.btnClose.TabIndex = 7;
            this.btnClose.Text = "關閉";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // txtCust
            // 
            this.txtCust.Enabled = false;
            this.txtCust.Font = new System.Drawing.Font("微軟正黑體", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txtCust.Location = new System.Drawing.Point(145, 4);
            this.txtCust.Name = "txtCust";
            this.txtCust.Size = new System.Drawing.Size(818, 27);
            this.txtCust.TabIndex = 131;
            // 
            // btnBringCust
            // 
            this.btnBringCust.Font = new System.Drawing.Font("微軟正黑體", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnBringCust.Location = new System.Drawing.Point(56, 2);
            this.btnBringCust.Margin = new System.Windows.Forms.Padding(2);
            this.btnBringCust.Name = "btnBringCust";
            this.btnBringCust.Size = new System.Drawing.Size(40, 30);
            this.btnBringCust.TabIndex = 130;
            this.btnBringCust.Text = "...";
            this.btnBringCust.UseVisualStyleBackColor = true;
            this.btnBringCust.Click += new System.EventHandler(this.btnBringCust_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微軟正黑體", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(11, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 19);
            this.label2.TabIndex = 129;
            this.label2.Text = "客戶";
            // 
            // txtSales
            // 
            this.txtSales.Enabled = false;
            this.txtSales.Font = new System.Drawing.Font("微軟正黑體", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txtSales.Location = new System.Drawing.Point(145, 37);
            this.txtSales.Name = "txtSales";
            this.txtSales.Size = new System.Drawing.Size(818, 27);
            this.txtSales.TabIndex = 134;
            // 
            // btnBringSales
            // 
            this.btnBringSales.Font = new System.Drawing.Font("微軟正黑體", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnBringSales.Location = new System.Drawing.Point(56, 35);
            this.btnBringSales.Margin = new System.Windows.Forms.Padding(2);
            this.btnBringSales.Name = "btnBringSales";
            this.btnBringSales.Size = new System.Drawing.Size(40, 30);
            this.btnBringSales.TabIndex = 133;
            this.btnBringSales.Text = "...";
            this.btnBringSales.UseVisualStyleBackColor = true;
            this.btnBringSales.Click += new System.EventHandler(this.btnBringSales_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微軟正黑體", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(11, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 19);
            this.label1.TabIndex = 132;
            this.label1.Text = "業務";
            // 
            // dgv_header
            // 
            this.dgv_header.AllowUserToAddRows = false;
            this.dgv_header.AllowUserToDeleteRows = false;
            this.dgv_header.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_header.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_header.ColumnHeadersHeight = 25;
            this.dgv_header.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv_header.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.check,
            this.ORDER_NO,
            this.CUST_CNAME,
            this.CUST_ENAME,
            this.CUST_CNAME2,
            this.CUST_ENAME2,
            this.SALES,
            this.Column1,
            this.ORDER_ID,
            this.CUST_ID});
            this.dgv_header.Controls.Add(this.chkALL);
            this.dgv_header.Location = new System.Drawing.Point(15, 113);
            this.dgv_header.Name = "dgv_header";
            this.dgv_header.ReadOnly = true;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.RoyalBlue;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Yellow;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_header.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_header.RowHeadersWidth = 25;
            this.dgv_header.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgv_header.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(226)))), ((int)(((byte)(239)))));
            this.dgv_header.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_header.RowTemplate.Height = 24;
            this.dgv_header.Size = new System.Drawing.Size(948, 247);
            this.dgv_header.TabIndex = 135;
            this.dgv_header.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_header_CellClick);
            // 
            // check
            // 
            this.check.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.check.FillWeight = 25F;
            this.check.Frozen = true;
            this.check.HeaderText = "選";
            this.check.MinimumWidth = 25;
            this.check.Name = "check";
            this.check.ReadOnly = true;
            this.check.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.check.Width = 35;
            // 
            // ORDER_NO
            // 
            this.ORDER_NO.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ORDER_NO.DataPropertyName = "ORDER_NO";
            this.ORDER_NO.HeaderText = "合約編號";
            this.ORDER_NO.Name = "ORDER_NO";
            this.ORDER_NO.ReadOnly = true;
            this.ORDER_NO.Width = 90;
            // 
            // CUST_CNAME
            // 
            this.CUST_CNAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.CUST_CNAME.DataPropertyName = "CUST_CNAME";
            this.CUST_CNAME.HeaderText = "姓名";
            this.CUST_CNAME.Name = "CUST_CNAME";
            this.CUST_CNAME.ReadOnly = true;
            this.CUST_CNAME.Width = 130;
            // 
            // CUST_ENAME
            // 
            this.CUST_ENAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.CUST_ENAME.DataPropertyName = "CUST_ENAME";
            this.CUST_ENAME.HeaderText = "英文姓名";
            this.CUST_ENAME.Name = "CUST_ENAME";
            this.CUST_ENAME.ReadOnly = true;
            this.CUST_ENAME.Width = 220;
            // 
            // CUST_CNAME2
            // 
            this.CUST_CNAME2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.CUST_CNAME2.DataPropertyName = "CUST_CNAME2";
            this.CUST_CNAME2.HeaderText = "聯名戶名";
            this.CUST_CNAME2.Name = "CUST_CNAME2";
            this.CUST_CNAME2.ReadOnly = true;
            this.CUST_CNAME2.Width = 150;
            // 
            // CUST_ENAME2
            // 
            this.CUST_ENAME2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.CUST_ENAME2.DataPropertyName = "CUST_ENAME2";
            this.CUST_ENAME2.HeaderText = "聯名戶英文名";
            this.CUST_ENAME2.Name = "CUST_ENAME2";
            this.CUST_ENAME2.ReadOnly = true;
            this.CUST_ENAME2.Width = 220;
            // 
            // SALES
            // 
            this.SALES.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.SALES.DataPropertyName = "ATTRIBUTE1";
            this.SALES.HeaderText = "業務";
            this.SALES.Name = "SALES";
            this.SALES.ReadOnly = true;
            this.SALES.Width = 130;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Column1.DataPropertyName = "IB_CODE";
            this.Column1.HeaderText = "IB_CODE";
            this.Column1.MinimumWidth = 100;
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // ORDER_ID
            // 
            this.ORDER_ID.DataPropertyName = "ORDER_ID";
            this.ORDER_ID.HeaderText = "ORDER_ID";
            this.ORDER_ID.Name = "ORDER_ID";
            this.ORDER_ID.ReadOnly = true;
            this.ORDER_ID.Visible = false;
            // 
            // CUST_ID
            // 
            this.CUST_ID.DataPropertyName = "CUST_ID";
            this.CUST_ID.HeaderText = "CUST_ID";
            this.CUST_ID.Name = "CUST_ID";
            this.CUST_ID.ReadOnly = true;
            this.CUST_ID.Visible = false;
            // 
            // chkALL
            // 
            this.chkALL.AutoSize = true;
            this.chkALL.Location = new System.Drawing.Point(7, 7);
            this.chkALL.Name = "chkALL";
            this.chkALL.Size = new System.Drawing.Size(15, 14);
            this.chkALL.TabIndex = 136;
            this.chkALL.UseVisualStyleBackColor = true;
            this.chkALL.CheckedChanged += new System.EventHandler(this.chkALL_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.btnConfirm);
            this.groupBox1.Controls.Add(this.txtIBCodeTo);
            this.groupBox1.Controls.Add(this.label54);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.txtSalesTo);
            this.groupBox1.Controls.Add(this.label55);
            this.groupBox1.Location = new System.Drawing.Point(15, 366);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(338, 147);
            this.groupBox1.TabIndex = 137;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "合約轉換設定";
            // 
            // btnConfirm
            // 
            this.btnConfirm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConfirm.Location = new System.Drawing.Point(140, 85);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(181, 48);
            this.btnConfirm.TabIndex = 138;
            this.btnConfirm.Text = "確定變更";
            this.btnConfirm.UseVisualStyleBackColor = true;
            this.btnConfirm.Click += new System.EventHandler(this.btnConfirm_Click);
            // 
            // txtIBCodeTo
            // 
            this.txtIBCodeTo.Location = new System.Drawing.Point(104, 54);
            this.txtIBCodeTo.Name = "txtIBCodeTo";
            this.txtIBCodeTo.Size = new System.Drawing.Size(170, 25);
            this.txtIBCodeTo.TabIndex = 138;
            this.txtIBCodeTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtIBCodeTo_KeyDown);
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(11, 57);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(55, 17);
            this.label54.TabIndex = 137;
            this.label54.Text = "IB Code";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(274, 24);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(36, 25);
            this.button1.TabIndex = 136;
            this.button1.Text = "...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtSalesTo
            // 
            this.txtSalesTo.Enabled = false;
            this.txtSalesTo.Location = new System.Drawing.Point(104, 24);
            this.txtSalesTo.Name = "txtSalesTo";
            this.txtSalesTo.Size = new System.Drawing.Size(169, 25);
            this.txtSalesTo.TabIndex = 135;
            this.txtSalesTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSalesTo_KeyDown);
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(11, 27);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(34, 17);
            this.label55.TabIndex = 134;
            this.label55.Text = "業務";
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(782, 70);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(181, 37);
            this.btnSearch.TabIndex = 139;
            this.btnSearch.Text = "搜尋";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnCleanSelection_Sales
            // 
            this.btnCleanSelection_Sales.Font = new System.Drawing.Font("微軟正黑體", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnCleanSelection_Sales.Location = new System.Drawing.Point(100, 35);
            this.btnCleanSelection_Sales.Margin = new System.Windows.Forms.Padding(2);
            this.btnCleanSelection_Sales.Name = "btnCleanSelection_Sales";
            this.btnCleanSelection_Sales.Size = new System.Drawing.Size(40, 30);
            this.btnCleanSelection_Sales.TabIndex = 141;
            this.btnCleanSelection_Sales.Text = "X";
            this.btnCleanSelection_Sales.UseVisualStyleBackColor = true;
            this.btnCleanSelection_Sales.Click += new System.EventHandler(this.btnCleanSelection_Sales_Click);
            // 
            // btnCleanSelection_Cust
            // 
            this.btnCleanSelection_Cust.Font = new System.Drawing.Font("微軟正黑體", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnCleanSelection_Cust.Location = new System.Drawing.Point(100, 2);
            this.btnCleanSelection_Cust.Margin = new System.Windows.Forms.Padding(2);
            this.btnCleanSelection_Cust.Name = "btnCleanSelection_Cust";
            this.btnCleanSelection_Cust.Size = new System.Drawing.Size(40, 30);
            this.btnCleanSelection_Cust.TabIndex = 140;
            this.btnCleanSelection_Cust.Text = "X";
            this.btnCleanSelection_Cust.UseVisualStyleBackColor = true;
            this.btnCleanSelection_Cust.Click += new System.EventHandler(this.btnCleanSelection_Cust_Click);
            // 
            // lblDataCount
            // 
            this.lblDataCount.AutoSize = true;
            this.lblDataCount.Font = new System.Drawing.Font("微軟正黑體", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lblDataCount.ForeColor = System.Drawing.Color.Blue;
            this.lblDataCount.Location = new System.Drawing.Point(18, 88);
            this.lblDataCount.Name = "lblDataCount";
            this.lblDataCount.Size = new System.Drawing.Size(108, 19);
            this.lblDataCount.TabIndex = 142;
            this.lblDataCount.Text = "資料筆數 : 0 筆";
            // 
            // frmTransContractToNewSales
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.ClientSize = new System.Drawing.Size(975, 521);
            this.Controls.Add(this.lblDataCount);
            this.Controls.Add(this.btnCleanSelection_Sales);
            this.Controls.Add(this.btnCleanSelection_Cust);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dgv_header);
            this.Controls.Add(this.txtSales);
            this.Controls.Add(this.btnBringSales);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtCust);
            this.Controls.Add(this.btnBringCust);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnClose);
            this.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Name = "frmTransContractToNewSales";
            this.Text = "合約轉業務";
            this.Load += new System.EventHandler(this.frmTransContractToNewSales_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_header)).EndInit();
            this.dgv_header.ResumeLayout(false);
            this.dgv_header.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.TextBox txtCust;
        private System.Windows.Forms.Button btnBringCust;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtSales;
        private System.Windows.Forms.Button btnBringSales;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgv_header;
        private System.Windows.Forms.DataGridViewCheckBoxColumn check;
        private System.Windows.Forms.DataGridViewTextBoxColumn ORDER_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUST_CNAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUST_ENAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUST_CNAME2;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUST_ENAME2;
        private System.Windows.Forms.DataGridViewTextBoxColumn SALES;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ORDER_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUST_ID;
        private System.Windows.Forms.CheckBox chkALL;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtIBCodeTo;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtSalesTo;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Button btnConfirm;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnCleanSelection_Sales;
        private System.Windows.Forms.Button btnCleanSelection_Cust;
        private System.Windows.Forms.Label lblDataCount;
    }
}
