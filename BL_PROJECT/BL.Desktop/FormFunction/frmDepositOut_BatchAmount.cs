﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using MK.Demo.Utility;
using MK.Demo.Model;
using MK.Demo.Logic;
using System.Threading.Tasks;
using System.Linq;
using static MK.Demo.Data.enCommon;

namespace MK_DEMO.Destop.FormFunction
{
    public partial class frmDepositOut_BatchAmount : MK_DEMO.Destop.BaseForm
    {
        public enFormStatus status;
        private List<BL_ACCOUNT_TXN_TEMP> dataTemp;
        public List<BL_ACCOUNT_TXN_TEMP> dataOut = new List<BL_ACCOUNT_TXN_TEMP>();
        public frmDepositOut_BatchAmount(List<BL_ACCOUNT_TXN_TEMP> data)
        {
            InitializeComponent();

            this.dgv_header.AutoGenerateColumns = false;
            this.dgv_header.MultiSelect = false;
            this.dgv_header.SelectionMode = DataGridViewSelectionMode.FullRowSelect;


            status = enFormStatus.Cancel;
            this.dataTemp = data;
        }

        private void frmDepositOut_BatchAmount_Load(object sender, EventArgs e)
        {
            this.dgv_header.DataBindByList(this.dataTemp);
            this.SetSeq();


            Parallel.ForEach(this.dgv_header.Rows.Cast<DataGridViewRow>(), x =>
            {
                x.Cells["AmountOut"].ReadOnly = false;
                x.Cells["ACTUAL_AmountOut"].ReadOnly = false;
            });

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            status = enFormStatus.OK;
            this.Close();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            this.dataOut = new List<BL_ACCOUNT_TXN_TEMP>();
            this.dgv_header.Rows.Cast<DataGridViewRow>()
                .ForEach(x =>
                {
                    var dataSingle = new BL_ACCOUNT_TXN_TEMP();
                    if (x.Cells["ORDER_ID"].Value.ConvertToString().Trim().IsNumeric())
                    {
                        dataSingle.ORDER_ID = decimal.Parse(x.Cells["ORDER_ID"].Value.ConvertToString());

                        if (x.Cells["AmountOut"].Value.ConvertToString().IsNumeric())
                        {
                            switch (x.Cells["ORDER_Currency"].Value.ConvertToString().Trim())
                            {
                                case "USD":
                                    dataSingle.AMOUNT_USD = decimal.Parse(x.Cells["AmountOut"].Value.ConvertToString());
                                    break;
                                case "NTD":
                                    dataSingle.AMOUNT_NTD = decimal.Parse(x.Cells["AmountOut"].Value.ConvertToString());
                                    break;
                                case "RMB":
                                    dataSingle.AMOUNT_RMB = decimal.Parse(x.Cells["AmountOut"].Value.ConvertToString());
                                    break;
                                case "EUR":
                                    dataSingle.AMOUNT_EUR = decimal.Parse(x.Cells["AmountOut"].Value.ConvertToString());
                                    break;
                                case "AUD":
                                    dataSingle.AMOUNT_AUD = decimal.Parse(x.Cells["AmountOut"].Value.ConvertToString());
                                    break;
                                case "JPY":
                                    dataSingle.AMOUNT_JPY = decimal.Parse(x.Cells["AmountOut"].Value.ConvertToString());
                                    break;
                                case "NZD":
                                    dataSingle.AMOUNT_NZD = decimal.Parse(x.Cells["AmountOut"].Value.ConvertToString());
                                    break;
                                default:
                                    break;
                            }
                        }
                        if (x.Cells["ACTUAL_AmountOut"].Value.ConvertToString().IsNumeric())
                        {
                            switch (x.Cells["ORDER_Currency"].Value.ConvertToString().Trim())
                            {
                                case "USD":
                                    dataSingle.ACTUAL_WITHDRAWAL_AMT_USD = decimal.Parse(x.Cells["ACTUAL_AmountOut"].Value.ConvertToString());
                                    break;
                                case "NTD":
                                    dataSingle.ACTUAL_WITHDRAWAL_AMT_NTD = decimal.Parse(x.Cells["ACTUAL_AmountOut"].Value.ConvertToString());
                                    break;
                                case "RMB":
                                    dataSingle.ACTUAL_WITHDRAWAL_AMT_RMB = decimal.Parse(x.Cells["ACTUAL_AmountOut"].Value.ConvertToString());
                                    break;
                                case "EUR":
                                    dataSingle.ACTUAL_WITHDRAWAL_AMT_EUR = decimal.Parse(x.Cells["ACTUAL_AmountOut"].Value.ConvertToString());
                                    break;
                                case "AUD":
                                    dataSingle.ACTUAL_WITHDRAWAL_AMT_AUD = decimal.Parse(x.Cells["ACTUAL_AmountOut"].Value.ConvertToString());
                                    break;
                                case "JPY":
                                    dataSingle.ACTUAL_WITHDRAWAL_AMT_JPY = decimal.Parse(x.Cells["ACTUAL_AmountOut"].Value.ConvertToString());
                                    break;
                                case "NZD":
                                    dataSingle.ACTUAL_WITHDRAWAL_AMT_NZD = decimal.Parse(x.Cells["ACTUAL_AmountOut"].Value.ConvertToString());
                                    break;
                                default:
                                    break;
                            }
                        }
                        dataSingle.END_BALANCE_FLAG = x.Cells["END_BALANCE_FLAG"].Value.ConvertToString();
                        this.dataOut.Add(dataSingle);
                    }
                });
            status = enFormStatus.OK;
            this.Close();
        }

        private void dgv_header_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1 && this.dgv_header.Rows.Count > 0)
            {
                Parallel.ForEach(this.dgv_header.Rows.Cast<DataGridViewRow>(), x =>
                {
                    x.Cells["check"].Value = false;
                });

                this.dgv_header.Rows[e.RowIndex].Cells["check"].Value = true;


                switch (this.dgv_header.Columns[e.ColumnIndex].Name)
                {
                    case "AmountOut":
                        this.dgv_header.BeginEdit(true);
                        break;
                    case "ACTUAL_AmountOut":
                        this.dgv_header.BeginEdit(true);
                        break;
                    case "END_BALANCE_FLAG":

                        this.dgv_header.Rows[e.RowIndex].Cells["END_BALANCE_FLAG"].Value =
                            this.dgv_header.Rows[e.RowIndex].Cells["END_BALANCE_FLAG"].Value.ConvertToString().ToUpper().Trim() == "Y" ? "N" : "Y";

                        if (this.dgv_header.Rows[e.RowIndex].Cells["END_BALANCE_FLAG"].Value.ConvertToString().ToUpper().Trim() == "Y")
                        {
                            this.dgv_header.Rows[e.RowIndex].Cells["AmountOut"].Value = this.dgv_header.Rows[e.RowIndex].Cells["BALANCE"].Value.ConvertToString();
                        }
                        else
                        {
                            this.dgv_header.Rows[e.RowIndex].Cells["AmountOut"].Value =
                                decimal.Parse(this.dgv_header.Rows[e.RowIndex].Cells["BALANCE"].Value.ConvertToString()) -
                                decimal.Parse(this.dgv_header.Rows[e.RowIndex].Cells["ORDER_Amount"].Value.ConvertToString());
                        }
                        break;
                    default:
                        break;
                }
                
            }
        }

        private void dgv_header_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            var txt = e.Control as TextBox;
            e.Control.TextChanged -= Txt_TextChanged;
            e.Control.TextChanged += Txt_TextChanged;

            if (txt != null)
            {
                txt.TextChanged -= Txt_TextChanged;
                txt.TextChanged += Txt_TextChanged;

            }
        }

        private void Txt_TextChanged(object sender, EventArgs e)
        {
            var txt = sender as TextBox;
            if (txt != null)
            {
                if (txt.Text.IsNumeric())
                {

                }

            }
        }

        private void dgv_header_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1 && e.ColumnIndex != -1)
            {
                if (this.dgv_header.Columns[e.ColumnIndex].Name == "ACTUAL_AmountOut" || this.dgv_header.Columns[e.ColumnIndex].Name == "AmountOut")
                {
                    DataGridViewCellStyle style = new DataGridViewCellStyle();
                    style.BackColor = Color.Yellow;
                    this.dgv_header.CurrentCell.Style.ApplyStyle(style);
                }
            }
        }

        private void dgv_header_CellLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1 && e.ColumnIndex != -1)
            {
                if (this.dgv_header.Columns[e.ColumnIndex].Name == "ACTUAL_AmountOut" || this.dgv_header.Columns[e.ColumnIndex].Name == "AmountOut")
                {
                    DataGridViewCellStyle style = new DataGridViewCellStyle();
                    style.BackColor = Color.White;
                    this.dgv_header.CurrentCell.Style.ApplyStyle(style);
                }
            }
        }

        private void dgv_header_Sorted(object sender, EventArgs e)
        {
            this.SetSeq();
        }
        private void SetSeq()
        {
            int i = 1;
            this.dgv_header.Rows.Cast<DataGridViewRow>().ForEach(x =>
            {
                x.Cells["No"].Value = i.ToString();
                i += 1;
            });
        }


    }
}
