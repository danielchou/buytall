﻿namespace MK_DEMO.Destop.FormFunction
{
    partial class frmContractSearch
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmContractSearch));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.chk_has_order_end = new System.Windows.Forms.CheckBox();
            this.dteORDER_END_E = new System.Windows.Forms.DateTimePicker();
            this.dteORDER_END_S = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.txtCustID = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dteEND_DATE_E = new System.Windows.Forms.DateTimePicker();
            this.dteEND_DATE_S = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.chkALL = new System.Windows.Forms.CheckBox();
            this.btnBatchDepositOut = new System.Windows.Forms.Button();
            this.txtCustomerName = new System.Windows.Forms.TextBox();
            this.txtOrderNo = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnCleanCondition = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.picLoder = new System.Windows.Forms.PictureBox();
            this.dgv_header = new System.Windows.Forms.DataGridView();
            this.btnClose = new System.Windows.Forms.Button();
            this.SEQ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.check = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.DoDepositOut = new System.Windows.Forms.DataGridViewLinkColumn();
            this.ModifyContract = new System.Windows.Forms.DataGridViewLinkColumn();
            this.ORDER_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUST_CNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUST_ENAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUST_CNAME2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUST_ENAME2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SALES = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MT4_ACCOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BALANCE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT_USD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT_NTD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT_RMB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT_EUR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT_AUD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT_JPY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT_NZD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FROM_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.END_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ext_date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ORDER_END = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.YEAR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ORDER_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUST_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.REJECT_REASON = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LAST_UPDATE_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.picLoder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_header)).BeginInit();
            this.SuspendLayout();
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // chk_has_order_end
            // 
            this.chk_has_order_end.AutoSize = true;
            this.chk_has_order_end.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.chk_has_order_end.Location = new System.Drawing.Point(18, 82);
            this.chk_has_order_end.Name = "chk_has_order_end";
            this.chk_has_order_end.Size = new System.Drawing.Size(103, 23);
            this.chk_has_order_end.TabIndex = 147;
            this.chk_has_order_end.Text = "含終止合約";
            this.chk_has_order_end.UseVisualStyleBackColor = true;
            // 
            // dteORDER_END_E
            // 
            this.dteORDER_END_E.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.dteORDER_END_E.Location = new System.Drawing.Point(808, 79);
            this.dteORDER_END_E.Name = "dteORDER_END_E";
            this.dteORDER_END_E.Size = new System.Drawing.Size(175, 27);
            this.dteORDER_END_E.TabIndex = 146;
            this.dteORDER_END_E.ValueChanged += new System.EventHandler(this.dteORDER_END_E_ValueChanged);
            // 
            // dteORDER_END_S
            // 
            this.dteORDER_END_S.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.dteORDER_END_S.Location = new System.Drawing.Point(624, 79);
            this.dteORDER_END_S.Name = "dteORDER_END_S";
            this.dteORDER_END_S.Size = new System.Drawing.Size(171, 27);
            this.dteORDER_END_S.TabIndex = 145;
            this.dteORDER_END_S.ValueChanged += new System.EventHandler(this.dteORDER_END_S_ValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.label6.Location = new System.Drawing.Point(531, 82);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 19);
            this.label6.TabIndex = 144;
            this.label6.Text = "合約終止日";
            // 
            // txtCustID
            // 
            this.txtCustID.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.txtCustID.Location = new System.Drawing.Point(335, 52);
            this.txtCustID.Name = "txtCustID";
            this.txtCustID.Size = new System.Drawing.Size(178, 27);
            this.txtCustID.TabIndex = 143;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.label3.Location = new System.Drawing.Point(273, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 19);
            this.label3.TabIndex = 142;
            this.label3.Text = "客戶 ID";
            // 
            // dteEND_DATE_E
            // 
            this.dteEND_DATE_E.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.dteEND_DATE_E.Location = new System.Drawing.Point(808, 52);
            this.dteEND_DATE_E.Name = "dteEND_DATE_E";
            this.dteEND_DATE_E.Size = new System.Drawing.Size(175, 27);
            this.dteEND_DATE_E.TabIndex = 141;
            this.dteEND_DATE_E.ValueChanged += new System.EventHandler(this.dteEND_DATE_E_ValueChanged);
            // 
            // dteEND_DATE_S
            // 
            this.dteEND_DATE_S.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.dteEND_DATE_S.Location = new System.Drawing.Point(624, 52);
            this.dteEND_DATE_S.Name = "dteEND_DATE_S";
            this.dteEND_DATE_S.Size = new System.Drawing.Size(171, 27);
            this.dteEND_DATE_S.TabIndex = 140;
            this.dteEND_DATE_S.ValueChanged += new System.EventHandler(this.dteEND_DATE_S_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.label2.Location = new System.Drawing.Point(531, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 19);
            this.label2.TabIndex = 139;
            this.label2.Text = "合約結束日";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微軟正黑體", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(88, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(167, 15);
            this.label1.TabIndex = 138;
            this.label1.Text = "(可用 , / - . 號區隔輸入多筆查詢)";
            // 
            // chkALL
            // 
            this.chkALL.AutoSize = true;
            this.chkALL.Location = new System.Drawing.Point(18, 114);
            this.chkALL.Name = "chkALL";
            this.chkALL.Size = new System.Drawing.Size(15, 14);
            this.chkALL.TabIndex = 137;
            this.chkALL.UseVisualStyleBackColor = true;
            this.chkALL.CheckedChanged += new System.EventHandler(this.chkALL_CheckedChanged);
            // 
            // btnBatchDepositOut
            // 
            this.btnBatchDepositOut.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.btnBatchDepositOut.Location = new System.Drawing.Point(1035, 50);
            this.btnBatchDepositOut.Name = "btnBatchDepositOut";
            this.btnBatchDepositOut.Size = new System.Drawing.Size(157, 28);
            this.btnBatchDepositOut.TabIndex = 136;
            this.btnBatchDepositOut.Text = "批次出金作業";
            this.btnBatchDepositOut.UseVisualStyleBackColor = true;
            this.btnBatchDepositOut.Click += new System.EventHandler(this.btnBatchDepositOut_Click);
            // 
            // txtCustomerName
            // 
            this.txtCustomerName.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.txtCustomerName.Location = new System.Drawing.Point(88, 52);
            this.txtCustomerName.Name = "txtCustomerName";
            this.txtCustomerName.Size = new System.Drawing.Size(178, 27);
            this.txtCustomerName.TabIndex = 135;
            this.txtCustomerName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCustomerName_KeyDown);
            // 
            // txtOrderNo
            // 
            this.txtOrderNo.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.txtOrderNo.Location = new System.Drawing.Point(88, 7);
            this.txtOrderNo.Name = "txtOrderNo";
            this.txtOrderNo.Size = new System.Drawing.Size(696, 27);
            this.txtOrderNo.TabIndex = 134;
            this.txtOrderNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtOrderNo_KeyDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.label4.Location = new System.Drawing.Point(14, 55);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 19);
            this.label4.TabIndex = 133;
            this.label4.Text = "客戶名";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.label5.Location = new System.Drawing.Point(14, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 19);
            this.label5.TabIndex = 132;
            this.label5.Text = "Order No";
            // 
            // btnCleanCondition
            // 
            this.btnCleanCondition.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.btnCleanCondition.Location = new System.Drawing.Point(1035, 5);
            this.btnCleanCondition.Name = "btnCleanCondition";
            this.btnCleanCondition.Size = new System.Drawing.Size(157, 29);
            this.btnCleanCondition.TabIndex = 131;
            this.btnCleanCondition.Text = "清除查詢條件";
            this.btnCleanCondition.UseVisualStyleBackColor = true;
            this.btnCleanCondition.Click += new System.EventHandler(this.btnCleanCondition_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.btnSearch.Location = new System.Drawing.Point(882, 5);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(147, 29);
            this.btnSearch.TabIndex = 130;
            this.btnSearch.Text = "查詢";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // picLoder
            // 
            this.picLoder.BackColor = System.Drawing.Color.Transparent;
            this.picLoder.Image = ((System.Drawing.Image)(resources.GetObject("picLoder.Image")));
            this.picLoder.Location = new System.Drawing.Point(477, 243);
            this.picLoder.Name = "picLoder";
            this.picLoder.Size = new System.Drawing.Size(174, 19);
            this.picLoder.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picLoder.TabIndex = 114;
            this.picLoder.TabStop = false;
            this.picLoder.Visible = false;
            // 
            // dgv_header
            // 
            this.dgv_header.AllowUserToAddRows = false;
            this.dgv_header.AllowUserToDeleteRows = false;
            this.dgv_header.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_header.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_header.ColumnHeadersHeight = 25;
            this.dgv_header.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv_header.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SEQ,
            this.check,
            this.DoDepositOut,
            this.ModifyContract,
            this.ORDER_NO,
            this.CUST_CNAME,
            this.CUST_ENAME,
            this.CUST_CNAME2,
            this.CUST_ENAME2,
            this.SALES,
            this.Column1,
            this.MT4_ACCOUNT,
            this.BALANCE,
            this.CURRENCY,
            this.AMOUNT_USD,
            this.AMOUNT_NTD,
            this.AMOUNT_RMB,
            this.AMOUNT_EUR,
            this.AMOUNT_AUD,
            this.AMOUNT_JPY,
            this.AMOUNT_NZD,
            this.FROM_DATE,
            this.END_DATE,
            this.ext_date,
            this.ORDER_END,
            this.YEAR,
            this.ORDER_ID,
            this.CUST_ID,
            this.REJECT_REASON,
            this.LAST_UPDATE_DATE});
            this.dgv_header.Location = new System.Drawing.Point(12, 110);
            this.dgv_header.Name = "dgv_header";
            this.dgv_header.ReadOnly = true;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("微軟正黑體", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.RoyalBlue;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Yellow;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_header.RowHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dgv_header.RowHeadersWidth = 25;
            this.dgv_header.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgv_header.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(226)))), ((int)(((byte)(239)))));
            this.dgv_header.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_header.RowTemplate.Height = 24;
            this.dgv_header.Size = new System.Drawing.Size(1186, 458);
            this.dgv_header.TabIndex = 6;
            this.dgv_header.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_header_CellClick);
            this.dgv_header.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgv_header_DataBindingComplete);
            this.dgv_header.Sorted += new System.EventHandler(this.dgv_header_Sorted);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.btnClose.Location = new System.Drawing.Point(1082, 574);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(110, 36);
            this.btnClose.TabIndex = 5;
            this.btnClose.Text = "關閉";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // SEQ
            // 
            this.SEQ.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.SEQ.Frozen = true;
            this.SEQ.HeaderText = "No.";
            this.SEQ.Name = "SEQ";
            this.SEQ.ReadOnly = true;
            this.SEQ.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.SEQ.Width = 40;
            // 
            // check
            // 
            this.check.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.check.FillWeight = 25F;
            this.check.Frozen = true;
            this.check.HeaderText = "選";
            this.check.MinimumWidth = 25;
            this.check.Name = "check";
            this.check.ReadOnly = true;
            this.check.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.check.Width = 35;
            // 
            // DoDepositOut
            // 
            this.DoDepositOut.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.DoDepositOut.HeaderText = "出金作業";
            this.DoDepositOut.Name = "DoDepositOut";
            this.DoDepositOut.ReadOnly = true;
            this.DoDepositOut.Visible = false;
            this.DoDepositOut.Width = 80;
            // 
            // ModifyContract
            // 
            this.ModifyContract.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ModifyContract.HeaderText = "合約修改";
            this.ModifyContract.Name = "ModifyContract";
            this.ModifyContract.ReadOnly = true;
            this.ModifyContract.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ModifyContract.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ModifyContract.Width = 80;
            // 
            // ORDER_NO
            // 
            this.ORDER_NO.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ORDER_NO.DataPropertyName = "ORDER_NO";
            this.ORDER_NO.HeaderText = "合約編號";
            this.ORDER_NO.Name = "ORDER_NO";
            this.ORDER_NO.ReadOnly = true;
            this.ORDER_NO.Width = 90;
            // 
            // CUST_CNAME
            // 
            this.CUST_CNAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.CUST_CNAME.DataPropertyName = "CUST_CNAME";
            this.CUST_CNAME.HeaderText = "姓名";
            this.CUST_CNAME.Name = "CUST_CNAME";
            this.CUST_CNAME.ReadOnly = true;
            this.CUST_CNAME.Width = 130;
            // 
            // CUST_ENAME
            // 
            this.CUST_ENAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.CUST_ENAME.DataPropertyName = "CUST_ENAME";
            this.CUST_ENAME.HeaderText = "英文姓名";
            this.CUST_ENAME.Name = "CUST_ENAME";
            this.CUST_ENAME.ReadOnly = true;
            this.CUST_ENAME.Width = 220;
            // 
            // CUST_CNAME2
            // 
            this.CUST_CNAME2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.CUST_CNAME2.DataPropertyName = "CUST_CNAME2";
            this.CUST_CNAME2.HeaderText = "聯名戶名";
            this.CUST_CNAME2.Name = "CUST_CNAME2";
            this.CUST_CNAME2.ReadOnly = true;
            this.CUST_CNAME2.Width = 150;
            // 
            // CUST_ENAME2
            // 
            this.CUST_ENAME2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.CUST_ENAME2.DataPropertyName = "CUST_ENAME2";
            this.CUST_ENAME2.HeaderText = "聯名戶英文名";
            this.CUST_ENAME2.Name = "CUST_ENAME2";
            this.CUST_ENAME2.ReadOnly = true;
            this.CUST_ENAME2.Width = 220;
            // 
            // SALES
            // 
            this.SALES.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.SALES.DataPropertyName = "ATTRIBUTE1";
            this.SALES.HeaderText = "業務";
            this.SALES.Name = "SALES";
            this.SALES.ReadOnly = true;
            this.SALES.Width = 130;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Column1.DataPropertyName = "IB_CODE";
            this.Column1.HeaderText = "IB_CODE";
            this.Column1.MinimumWidth = 100;
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // MT4_ACCOUNT
            // 
            this.MT4_ACCOUNT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.MT4_ACCOUNT.DataPropertyName = "MT4_ACCOUNT";
            this.MT4_ACCOUNT.HeaderText = "MT4交易帳戶";
            this.MT4_ACCOUNT.Name = "MT4_ACCOUNT";
            this.MT4_ACCOUNT.ReadOnly = true;
            this.MT4_ACCOUNT.Visible = false;
            this.MT4_ACCOUNT.Width = 160;
            // 
            // BALANCE
            // 
            this.BALANCE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.BALANCE.DataPropertyName = "BALANCE";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle1.Format = "N2";
            dataGridViewCellStyle1.NullValue = null;
            this.BALANCE.DefaultCellStyle = dataGridViewCellStyle1;
            this.BALANCE.HeaderText = "餘額";
            this.BALANCE.Name = "BALANCE";
            this.BALANCE.ReadOnly = true;
            this.BALANCE.Width = 110;
            // 
            // CURRENCY
            // 
            this.CURRENCY.DataPropertyName = "CURRENCY";
            this.CURRENCY.HeaderText = "幣別";
            this.CURRENCY.MinimumWidth = 100;
            this.CURRENCY.Name = "CURRENCY";
            this.CURRENCY.ReadOnly = true;
            // 
            // AMOUNT_USD
            // 
            this.AMOUNT_USD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.AMOUNT_USD.DataPropertyName = "AMOUNT_USD";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "N2";
            dataGridViewCellStyle2.NullValue = null;
            this.AMOUNT_USD.DefaultCellStyle = dataGridViewCellStyle2;
            this.AMOUNT_USD.HeaderText = "入金金額(美元)";
            this.AMOUNT_USD.Name = "AMOUNT_USD";
            this.AMOUNT_USD.ReadOnly = true;
            this.AMOUNT_USD.Width = 150;
            // 
            // AMOUNT_NTD
            // 
            this.AMOUNT_NTD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.AMOUNT_NTD.DataPropertyName = "AMOUNT_NTD";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = null;
            this.AMOUNT_NTD.DefaultCellStyle = dataGridViewCellStyle3;
            this.AMOUNT_NTD.HeaderText = "入金金額(新台幣)";
            this.AMOUNT_NTD.Name = "AMOUNT_NTD";
            this.AMOUNT_NTD.ReadOnly = true;
            this.AMOUNT_NTD.Width = 150;
            // 
            // AMOUNT_RMB
            // 
            this.AMOUNT_RMB.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.AMOUNT_RMB.DataPropertyName = "AMOUNT_RMB";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N2";
            dataGridViewCellStyle4.NullValue = null;
            this.AMOUNT_RMB.DefaultCellStyle = dataGridViewCellStyle4;
            this.AMOUNT_RMB.HeaderText = "入金金額(人民幣)";
            this.AMOUNT_RMB.Name = "AMOUNT_RMB";
            this.AMOUNT_RMB.ReadOnly = true;
            this.AMOUNT_RMB.Width = 150;
            // 
            // AMOUNT_EUR
            // 
            this.AMOUNT_EUR.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.AMOUNT_EUR.DataPropertyName = "AMOUNT_EUR";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "N2";
            this.AMOUNT_EUR.DefaultCellStyle = dataGridViewCellStyle5;
            this.AMOUNT_EUR.HeaderText = "入金金額(歐元)";
            this.AMOUNT_EUR.Name = "AMOUNT_EUR";
            this.AMOUNT_EUR.ReadOnly = true;
            this.AMOUNT_EUR.Width = 150;
            // 
            // AMOUNT_AUD
            // 
            this.AMOUNT_AUD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.AMOUNT_AUD.DataPropertyName = "AMOUNT_AUD";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "N2";
            this.AMOUNT_AUD.DefaultCellStyle = dataGridViewCellStyle6;
            this.AMOUNT_AUD.HeaderText = "入金金額(澳幣)";
            this.AMOUNT_AUD.Name = "AMOUNT_AUD";
            this.AMOUNT_AUD.ReadOnly = true;
            this.AMOUNT_AUD.Width = 150;
            // 
            // AMOUNT_JPY
            // 
            this.AMOUNT_JPY.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.AMOUNT_JPY.DataPropertyName = "AMOUNT_JPY";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Format = "N2";
            this.AMOUNT_JPY.DefaultCellStyle = dataGridViewCellStyle7;
            this.AMOUNT_JPY.HeaderText = "入金金額(日幣)";
            this.AMOUNT_JPY.Name = "AMOUNT_JPY";
            this.AMOUNT_JPY.ReadOnly = true;
            this.AMOUNT_JPY.Width = 150;
            // 
            // AMOUNT_NZD
            // 
            this.AMOUNT_NZD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.AMOUNT_NZD.DataPropertyName = "AMOUNT_NZD";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.Format = "N2";
            this.AMOUNT_NZD.DefaultCellStyle = dataGridViewCellStyle8;
            this.AMOUNT_NZD.HeaderText = "入金金額(紐幣)";
            this.AMOUNT_NZD.Name = "AMOUNT_NZD";
            this.AMOUNT_NZD.ReadOnly = true;
            this.AMOUNT_NZD.Width = 150;
            // 
            // FROM_DATE
            // 
            this.FROM_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.FROM_DATE.DataPropertyName = "FROM_DATE";
            this.FROM_DATE.HeaderText = "合約日期";
            this.FROM_DATE.Name = "FROM_DATE";
            this.FROM_DATE.ReadOnly = true;
            this.FROM_DATE.Width = 140;
            // 
            // END_DATE
            // 
            this.END_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.END_DATE.DataPropertyName = "END_DATE";
            this.END_DATE.HeaderText = "合約結束日期";
            this.END_DATE.Name = "END_DATE";
            this.END_DATE.ReadOnly = true;
            this.END_DATE.Width = 140;
            // 
            // ext_date
            // 
            this.ext_date.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ext_date.DataPropertyName = "EXTEND_DATE";
            this.ext_date.HeaderText = "展延日期";
            this.ext_date.MinimumWidth = 100;
            this.ext_date.Name = "ext_date";
            this.ext_date.ReadOnly = true;
            this.ext_date.Width = 140;
            // 
            // ORDER_END
            // 
            this.ORDER_END.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ORDER_END.DataPropertyName = "ORDER_END";
            this.ORDER_END.HeaderText = "合約終止日";
            this.ORDER_END.Name = "ORDER_END";
            this.ORDER_END.ReadOnly = true;
            this.ORDER_END.Width = 150;
            // 
            // YEAR
            // 
            this.YEAR.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.YEAR.DataPropertyName = "YEAR";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.YEAR.DefaultCellStyle = dataGridViewCellStyle9;
            this.YEAR.HeaderText = "年期";
            this.YEAR.Name = "YEAR";
            this.YEAR.ReadOnly = true;
            this.YEAR.Width = 90;
            // 
            // ORDER_ID
            // 
            this.ORDER_ID.DataPropertyName = "ORDER_ID";
            this.ORDER_ID.HeaderText = "ORDER_ID";
            this.ORDER_ID.Name = "ORDER_ID";
            this.ORDER_ID.ReadOnly = true;
            this.ORDER_ID.Visible = false;
            // 
            // CUST_ID
            // 
            this.CUST_ID.DataPropertyName = "CUST_ID";
            this.CUST_ID.HeaderText = "CUST_ID";
            this.CUST_ID.Name = "CUST_ID";
            this.CUST_ID.ReadOnly = true;
            this.CUST_ID.Visible = false;
            // 
            // REJECT_REASON
            // 
            this.REJECT_REASON.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.REJECT_REASON.DataPropertyName = "REJECT_REASON";
            this.REJECT_REASON.HeaderText = "退件原因";
            this.REJECT_REASON.Name = "REJECT_REASON";
            this.REJECT_REASON.ReadOnly = true;
            this.REJECT_REASON.Visible = false;
            this.REJECT_REASON.Width = 250;
            // 
            // LAST_UPDATE_DATE
            // 
            this.LAST_UPDATE_DATE.DataPropertyName = "LAST_UPDATE_DATE";
            this.LAST_UPDATE_DATE.HeaderText = "LAST_UPDATE_DATE";
            this.LAST_UPDATE_DATE.Name = "LAST_UPDATE_DATE";
            this.LAST_UPDATE_DATE.ReadOnly = true;
            this.LAST_UPDATE_DATE.Visible = false;
            // 
            // frmContractSearch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.ClientSize = new System.Drawing.Size(1205, 622);
            this.Controls.Add(this.chk_has_order_end);
            this.Controls.Add(this.dteORDER_END_E);
            this.Controls.Add(this.dteORDER_END_S);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtCustID);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dteEND_DATE_E);
            this.Controls.Add(this.dteEND_DATE_S);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chkALL);
            this.Controls.Add(this.btnBatchDepositOut);
            this.Controls.Add(this.txtCustomerName);
            this.Controls.Add(this.txtOrderNo);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnCleanCondition);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.picLoder);
            this.Controls.Add(this.dgv_header);
            this.Controls.Add(this.btnClose);
            this.Font = new System.Drawing.Font("微軟正黑體", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Name = "frmContractSearch";
            this.Text = "合約資料";
            this.Load += new System.EventHandler(this.frmContractDetail_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picLoder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_header)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.DataGridView dgv_header;
        private System.Windows.Forms.PictureBox picLoder;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.TextBox txtCustomerName;
        private System.Windows.Forms.TextBox txtOrderNo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnCleanCondition;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnBatchDepositOut;
        private System.Windows.Forms.CheckBox chkALL;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dteEND_DATE_S;
        private System.Windows.Forms.DateTimePicker dteEND_DATE_E;
        private System.Windows.Forms.TextBox txtCustID;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dteORDER_END_E;
        private System.Windows.Forms.DateTimePicker dteORDER_END_S;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox chk_has_order_end;
        private System.Windows.Forms.DataGridViewTextBoxColumn SEQ;
        private System.Windows.Forms.DataGridViewCheckBoxColumn check;
        private System.Windows.Forms.DataGridViewLinkColumn DoDepositOut;
        private System.Windows.Forms.DataGridViewLinkColumn ModifyContract;
        private System.Windows.Forms.DataGridViewTextBoxColumn ORDER_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUST_CNAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUST_ENAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUST_CNAME2;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUST_ENAME2;
        private System.Windows.Forms.DataGridViewTextBoxColumn SALES;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn MT4_ACCOUNT;
        private System.Windows.Forms.DataGridViewTextBoxColumn BALANCE;
        private System.Windows.Forms.DataGridViewTextBoxColumn CURRENCY;
        private System.Windows.Forms.DataGridViewTextBoxColumn AMOUNT_USD;
        private System.Windows.Forms.DataGridViewTextBoxColumn AMOUNT_NTD;
        private System.Windows.Forms.DataGridViewTextBoxColumn AMOUNT_RMB;
        private System.Windows.Forms.DataGridViewTextBoxColumn AMOUNT_EUR;
        private System.Windows.Forms.DataGridViewTextBoxColumn AMOUNT_AUD;
        private System.Windows.Forms.DataGridViewTextBoxColumn AMOUNT_JPY;
        private System.Windows.Forms.DataGridViewTextBoxColumn AMOUNT_NZD;
        private System.Windows.Forms.DataGridViewTextBoxColumn FROM_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn END_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn ext_date;
        private System.Windows.Forms.DataGridViewTextBoxColumn ORDER_END;
        private System.Windows.Forms.DataGridViewTextBoxColumn YEAR;
        private System.Windows.Forms.DataGridViewTextBoxColumn ORDER_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUST_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn REJECT_REASON;
        private System.Windows.Forms.DataGridViewTextBoxColumn LAST_UPDATE_DATE;
    }
}
