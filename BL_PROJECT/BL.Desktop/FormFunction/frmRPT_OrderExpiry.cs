﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MK.Demo.Utility;
using Microsoft.Reporting.WinForms;
using MK.Demo.Logic;
using System.IO;
using MK.Demo.Model;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.xml;
using iTextSharp.text.html;

namespace MK_DEMO.Destop.FormFunction
{
    public partial class frmRPT_OrderExpiry : Form
    {
        public frmRPT_OrderExpiry()
        {
            InitializeComponent();

            this.dgv_New.AutoGenerateColumns = false;
            this.dgv_New.MultiSelect = false;
            this.dgv_New.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            this.dgv_Old.AutoGenerateColumns = false;
            this.dgv_Old.MultiSelect = false;
            this.dgv_Old.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        private void frmRPT_OrderExpiry_Load(object sender, EventArgs e)
        {
            this.InitComboBox();
        }
        private void InitComboBox()
        {
            this.cboYear.Items.Clear();
            this.cboMonth.Items.Clear();
            for (int i = DateTime.Now.Year + 1; i >= DateTime.Now.Year - 3; i--)
            {
                this.cboYear.Items.Add(i.ConvertToString());
            }
            for (int i = 1; i <= 12; i++)
            {
                this.cboMonth.Items.Add(i.ConvertToString());
            }
            this.cboYear.SelectedIndex = this.cboYear.FindStringExact(DateTime.Now.Year.ConvertToString());
            this.cboMonth.SelectedIndex = this.cboMonth.FindStringExact(DateTime.Now.Month.ConvertToString());

        }


        private bool DataValidation()
        {
            var strYear = this.cboYear.Text.Trim();
            var strMonth = this.cboMonth.Text.Trim();

            if (strYear.Trim() == string.Empty || strMonth.Trim() == string.Empty)
            {
                MessageBox.Show("請選擇基準年月");
                return false;
            }

            return true;
        }

        private void GetData(out List<BL_ORDER_Expiry> dataNew, out List<BL_ORDER_Expiry> dataOld, out List<BL_ORDER_Expiry> dataOld_CannotExtend)
        {
            var strYear = this.cboYear.Text.Trim();
            var strMonth = this.cboMonth.Text.Trim();
            var strORG = Global.ORG_CODE;

            var dt = new DateTime(int.Parse(strYear), int.Parse(strMonth), 1);
            var data = new logicCust(Global.IsTestMode).GetRpt_OrderExpirtData(dt, strORG);
            dataNew = data.Where(x => x.ORDER_TYPE.ConvertToString() == "NEW").ToList();
            dataOld = data.Where(x => x.ORDER_TYPE.ConvertToString() == "OLD").ToList();
            dataOld_CannotExtend = data.Where(x => x.ORDER_TYPE.ConvertToString() == "OLD_END").ToList();
            if (dataNew.Count() + dataOld.Count() + dataOld_CannotExtend.Count() == 0)
            {
                MessageBox.Show("無符合資料");
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (this.DataValidation())
            {
                List<BL_ORDER_Expiry> dataNew = new List<BL_ORDER_Expiry>();
                List<BL_ORDER_Expiry> dataOld = new List<BL_ORDER_Expiry>();
                List<BL_ORDER_Expiry> dataOld_CannotExtend = new List<BL_ORDER_Expiry>();

                this.GetData(out dataNew, out dataOld, out dataOld_CannotExtend);
                this.dgv_New.DataBindByList(dataNew);
                this.dgv_Old.DataBindByList(dataOld);
                this.dgv_Old2.DataBindByList(dataOld_CannotExtend);

                this.tabPage1.Text = "新合約 [" + dataNew.Count().ConvertToString() + "]";
                this.tabPage2.Text = "舊合約-可續約 [" + dataOld.Count().ConvertToString() + "]";
                this.tabPage3.Text = "舊合約-不可續約 [" + dataOld_CannotExtend.Count().ConvertToString() + "]";
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            if (this.DataValidation())
            {
                List<BL_ORDER_Expiry> dataNew = new List<BL_ORDER_Expiry>();
                List<BL_ORDER_Expiry> dataOld = new List<BL_ORDER_Expiry>();
                List<BL_ORDER_Expiry> dataOld_CannotExtend = new List<BL_ORDER_Expiry>();

                this.GetData(out dataNew, out dataOld, out dataOld_CannotExtend);

                using (var fbd = new FolderBrowserDialog())
                {
                    DialogResult result = fbd.ShowDialog();
                    if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                    {
                        var logic = new logicCust(Global.IsTestMode);
                        //  存放路徑
                        var strSavePath = fbd.SelectedPath;
                        var dataLog = new List<BL_ORDEREND_MAIL_LOG>();
                        var strJob_ID = logic.GetOrderEndMailJob_ID();

                        dataLog.AddRange(
                            this.ExportPDF
                            (
                                data: dataOld,
                                strRPT_Path: @"RPT\RPT_OrderExpiry_OLD.rdlc",
                                strSavePath: strSavePath,
                                strBatchJobID: strJob_ID
                            ));

                        dataLog.AddRange(
                            this.ExportPDF
                            (
                                data: dataOld_CannotExtend,
                                strRPT_Path: @"RPT\RPT_OrderExpiry_OLD2.rdlc",
                                strSavePath: strSavePath,
                                strBatchJobID: strJob_ID
                            ));

                        dataLog.AddRange(
                            this.ExportPDF
                            (
                                data: dataNew,
                                strRPT_Path: @"RPT\RPT_OrderExpiry_NEW.rdlc",
                                strSavePath: strSavePath,
                                strBatchJobID: strJob_ID
                            ));


                        //  寫入db
                        if (logic.WriteOrderEndMailLog(dataLog, strJob_ID, Global.str_UserName))
                        {
                            MessageBox.Show("檔案已產生完畢" + Environment.NewLine + "本次產生批號 : " + strJob_ID);
                        }
                        else
                        {
                            MessageBox.Show("批次產生失敗");
                        }
                    }
                }
            }
        }

        private List<BL_ORDEREND_MAIL_LOG> ExportPDF(List<BL_ORDER_Expiry> data, string strRPT_Path, string strSavePath, string strBatchJobID)
        {
            var logic = new logicCust(Global.IsTestMode);
            var dataLogs = new List<BL_ORDEREND_MAIL_LOG>();
            var fileEncryption = new List<FILE_ENCRYPTION>();

            string exeFolder = Application.StartupPath;

            data.ForEach(x =>
            {
                var dataSingle = new List<BL_ORDER_Expiry>()
                {
                    x
                };
                var strFileName = string.Empty;
                var strPassword = string.Empty;
                var strSales = string.Empty;
                var strMail = string.Empty;

                #region 檔案參數設定

                //聯名戶要秀出
                var IsUnionCust = false;
                if (x.CUST_CNAME2.ConvertToString() != string.Empty)
                {
                    IsUnionCust = true;
                    strFileName = "合約到期通知書_" + x.CNAME.ConvertToString().Replace("/", "_") + "_" + x.CUST_CNAME2.ConvertToString().Replace("/", "_") + "_" + x.ORDER_NO.ConvertToString();
                    strPassword = x.UNION_ID_NUMBER.ConvertToString().Trim();
                    strMail = x.EMAIL_2.ConvertToString();
                    strSales = x.SALES;
                }
                else
                {
                    strFileName = "合約到期通知書_" + x.CNAME.ConvertToString().Replace("/", "_") + "_" + x.ORDER_NO.ConvertToString();
                    strPassword = x.ID_NUMBER.ConvertToString().Trim();
                    strMail = x.EMAIL_1.ConvertToString();
                    strSales = x.SALES;
                }
                fileEncryption.Add(new FILE_ENCRYPTION()
                {
                    FileName = strFileName,
                    Password = strPassword,
                     Sales_Name =  strSales

                });
                #endregion

                #region 產生Temp檔
                var binding = new BindingSource();
                binding.DataSource = dataSingle;
                string reportPath = System.IO.Path.Combine(exeFolder, strRPT_Path);
                var rpt = new ReportViewer();
                rpt.Reset();
                rpt.LocalReport.ReportPath = reportPath;
                rpt.LocalReport.DataSources.Add(new ReportDataSource("dsData", binding));
                rpt.RefreshReport();

               
                //  Export
                var strFileName_Save = strSavePath + "/" + x.SALES +"_"+ strFileName + "_Temp.pdf";

                Warning[] warnings;
                string[] streamids;
                string mimeType;
                string encoding;
                string filenameExtension;

                byte[] bytes = rpt.LocalReport.Render(
                    "PDF", null, out mimeType, out encoding, out filenameExtension,
                    out streamids, out warnings);

                using (FileStream fs = new FileStream(strFileName_Save, FileMode.Create))
                {
                    fs.Write(bytes, 0, bytes.Length);
                }

                dataLogs.Add(new BL_ORDEREND_MAIL_LOG()
                {
                    JOB_ID = strBatchJobID,
                    CUST_ID = x.CUST_ID,
                    ORDEREND_MONTH = x.ORDEREND_MONTH,
                    ORDERNO_DESC = x.ORDER_NO,
                    PDF_LOCATION = strSavePath + "\\" + strFileName + ".pdf",
                    EMAIL = strMail
                });

                #endregion

            });

            #region 檔案加密

            //  Encryption
            fileEncryption.ForEach(x =>
            {
                PdfReader reader = new PdfReader(strSavePath + "/" +x.Sales_Name+"_"+ x.FileName + "_Temp.pdf");
                using (var os = new FileStream(strSavePath + "/" + x.FileName + ".pdf", FileMode.Create))
                {
                    PdfEncryptor.Encrypt(reader, os, true, x.Password, "", PdfWriter.AllowPrinting);
                }
            });

            #endregion

            return dataLogs;
        }

        private void btnSendEmail_Click(object sender, EventArgs e)
        {
            //  開啟發送視窗
            var frm = new frmRPT_ExpiredOrder_Mail(null) { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
            frm.ShowDialog();
            frm.Dispose();
            frm = null;
        }

    }
}
