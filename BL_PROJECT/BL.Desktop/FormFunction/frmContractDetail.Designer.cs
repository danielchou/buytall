﻿namespace MK_DEMO.Destop.FormFunction
{
    partial class frmContractDetail
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmContractDetail));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.btnOK = new System.Windows.Forms.Button();
            this.picLoder = new System.Windows.Forms.PictureBox();
            this.dgv_header = new System.Windows.Forms.DataGridView();
            this.btnClose = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCustName = new System.Windows.Forms.TextBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.check = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ORDER_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUST_CNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUST_ENAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SALES = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IB_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BALANCE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT_USD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT_NTD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT_RMB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT_EUR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT_AUD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT_JPY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT_NZD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FROM_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.END_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.YEAR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EXTEND_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ORDER_END = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.picLoder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_header)).BeginInit();
            this.SuspendLayout();
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.btnOK.Location = new System.Drawing.Point(565, 424);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(110, 48);
            this.btnOK.TabIndex = 115;
            this.btnOK.Text = "確定";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // picLoder
            // 
            this.picLoder.BackColor = System.Drawing.Color.Transparent;
            this.picLoder.Image = ((System.Drawing.Image)(resources.GetObject("picLoder.Image")));
            this.picLoder.Location = new System.Drawing.Point(314, 194);
            this.picLoder.Name = "picLoder";
            this.picLoder.Size = new System.Drawing.Size(174, 19);
            this.picLoder.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picLoder.TabIndex = 114;
            this.picLoder.TabStop = false;
            this.picLoder.Visible = false;
            // 
            // dgv_header
            // 
            this.dgv_header.AllowUserToAddRows = false;
            this.dgv_header.AllowUserToDeleteRows = false;
            this.dgv_header.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_header.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_header.ColumnHeadersHeight = 25;
            this.dgv_header.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv_header.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.check,
            this.ORDER_NO,
            this.CUST_CNAME,
            this.CUST_ENAME,
            this.SALES,
            this.IB_CODE,
            this.CURRENCY,
            this.BALANCE,
            this.AMOUNT_USD,
            this.AMOUNT_NTD,
            this.AMOUNT_RMB,
            this.AMOUNT_EUR,
            this.AMOUNT_AUD,
            this.AMOUNT_JPY,
            this.AMOUNT_NZD,
            this.FROM_DATE,
            this.END_DATE,
            this.YEAR,
            this.EXTEND_DATE,
            this.ORDER_END});
            this.dgv_header.Location = new System.Drawing.Point(12, 53);
            this.dgv_header.Name = "dgv_header";
            this.dgv_header.ReadOnly = true;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("微軟正黑體", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.RoyalBlue;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Yellow;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_header.RowHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dgv_header.RowHeadersWidth = 25;
            this.dgv_header.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgv_header.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(226)))), ((int)(((byte)(239)))));
            this.dgv_header.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_header.RowTemplate.Height = 24;
            this.dgv_header.Size = new System.Drawing.Size(779, 365);
            this.dgv_header.TabIndex = 6;
            this.dgv_header.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_header_CellClick);
            this.dgv_header.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgv_header_CellFormatting);
            this.dgv_header.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgv_header_DataBindingComplete);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.btnClose.Location = new System.Drawing.Point(681, 424);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(110, 48);
            this.btnClose.TabIndex = 5;
            this.btnClose.Text = "關閉";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 18);
            this.label1.TabIndex = 116;
            this.label1.Text = "客戶姓名";
            // 
            // txtCustName
            // 
            this.txtCustName.Location = new System.Drawing.Point(87, 13);
            this.txtCustName.Name = "txtCustName";
            this.txtCustName.Size = new System.Drawing.Size(212, 25);
            this.txtCustName.TabIndex = 117;
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch.Font = new System.Drawing.Font("微軟正黑體", 11F);
            this.btnSearch.Location = new System.Drawing.Point(305, 13);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(110, 25);
            this.btnSearch.TabIndex = 118;
            this.btnSearch.Text = "搜尋";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // check
            // 
            this.check.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.check.FillWeight = 25F;
            this.check.HeaderText = "選";
            this.check.MinimumWidth = 25;
            this.check.Name = "check";
            this.check.ReadOnly = true;
            this.check.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.check.Width = 25;
            // 
            // ORDER_NO
            // 
            this.ORDER_NO.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ORDER_NO.DataPropertyName = "ORDER_NO";
            this.ORDER_NO.HeaderText = "合約編號";
            this.ORDER_NO.Name = "ORDER_NO";
            this.ORDER_NO.ReadOnly = true;
            this.ORDER_NO.Width = 90;
            // 
            // CUST_CNAME
            // 
            this.CUST_CNAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.CUST_CNAME.DataPropertyName = "CUST_CNAME";
            this.CUST_CNAME.HeaderText = "姓名";
            this.CUST_CNAME.Name = "CUST_CNAME";
            this.CUST_CNAME.ReadOnly = true;
            this.CUST_CNAME.Width = 130;
            // 
            // CUST_ENAME
            // 
            this.CUST_ENAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.CUST_ENAME.DataPropertyName = "CUST_ENAME";
            this.CUST_ENAME.HeaderText = "英文姓名";
            this.CUST_ENAME.Name = "CUST_ENAME";
            this.CUST_ENAME.ReadOnly = true;
            this.CUST_ENAME.Width = 210;
            // 
            // SALES
            // 
            this.SALES.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.SALES.DataPropertyName = "SALES_NAME";
            this.SALES.HeaderText = "業務";
            this.SALES.Name = "SALES";
            this.SALES.ReadOnly = true;
            this.SALES.Width = 130;
            // 
            // IB_CODE
            // 
            this.IB_CODE.DataPropertyName = "IB_CODE";
            this.IB_CODE.HeaderText = "IB_CODE";
            this.IB_CODE.MinimumWidth = 90;
            this.IB_CODE.Name = "IB_CODE";
            this.IB_CODE.ReadOnly = true;
            // 
            // CURRENCY
            // 
            this.CURRENCY.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.CURRENCY.DataPropertyName = "CURRENCY";
            this.CURRENCY.FillWeight = 80F;
            this.CURRENCY.HeaderText = "幣別";
            this.CURRENCY.Name = "CURRENCY";
            this.CURRENCY.ReadOnly = true;
            this.CURRENCY.Width = 80;
            // 
            // BALANCE
            // 
            this.BALANCE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.BALANCE.DataPropertyName = "BALANCE";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle1.Format = "N2";
            dataGridViewCellStyle1.NullValue = null;
            this.BALANCE.DefaultCellStyle = dataGridViewCellStyle1;
            this.BALANCE.HeaderText = "餘額";
            this.BALANCE.Name = "BALANCE";
            this.BALANCE.ReadOnly = true;
            this.BALANCE.Width = 115;
            // 
            // AMOUNT_USD
            // 
            this.AMOUNT_USD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.AMOUNT_USD.DataPropertyName = "AMOUNT_USD";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "N2";
            dataGridViewCellStyle2.NullValue = null;
            this.AMOUNT_USD.DefaultCellStyle = dataGridViewCellStyle2;
            this.AMOUNT_USD.HeaderText = "入金金額(美元)";
            this.AMOUNT_USD.Name = "AMOUNT_USD";
            this.AMOUNT_USD.ReadOnly = true;
            this.AMOUNT_USD.Width = 150;
            // 
            // AMOUNT_NTD
            // 
            this.AMOUNT_NTD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.AMOUNT_NTD.DataPropertyName = "AMOUNT_NTD";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = null;
            this.AMOUNT_NTD.DefaultCellStyle = dataGridViewCellStyle3;
            this.AMOUNT_NTD.HeaderText = "入金金額(新台幣)";
            this.AMOUNT_NTD.Name = "AMOUNT_NTD";
            this.AMOUNT_NTD.ReadOnly = true;
            this.AMOUNT_NTD.Width = 150;
            // 
            // AMOUNT_RMB
            // 
            this.AMOUNT_RMB.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.AMOUNT_RMB.DataPropertyName = "AMOUNT_RMB";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N2";
            dataGridViewCellStyle4.NullValue = null;
            this.AMOUNT_RMB.DefaultCellStyle = dataGridViewCellStyle4;
            this.AMOUNT_RMB.HeaderText = "入金金額(人民幣)";
            this.AMOUNT_RMB.Name = "AMOUNT_RMB";
            this.AMOUNT_RMB.ReadOnly = true;
            this.AMOUNT_RMB.Width = 150;
            // 
            // AMOUNT_EUR
            // 
            this.AMOUNT_EUR.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.AMOUNT_EUR.DataPropertyName = "AMOUNT_EUR";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "N2";
            this.AMOUNT_EUR.DefaultCellStyle = dataGridViewCellStyle5;
            this.AMOUNT_EUR.HeaderText = "入金金額(歐元)";
            this.AMOUNT_EUR.Name = "AMOUNT_EUR";
            this.AMOUNT_EUR.ReadOnly = true;
            this.AMOUNT_EUR.Width = 150;
            // 
            // AMOUNT_AUD
            // 
            this.AMOUNT_AUD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.AMOUNT_AUD.DataPropertyName = "AMOUNT_AUD";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "N2";
            this.AMOUNT_AUD.DefaultCellStyle = dataGridViewCellStyle6;
            this.AMOUNT_AUD.HeaderText = "入金金額(澳幣)";
            this.AMOUNT_AUD.Name = "AMOUNT_AUD";
            this.AMOUNT_AUD.ReadOnly = true;
            this.AMOUNT_AUD.Width = 150;
            // 
            // AMOUNT_JPY
            // 
            this.AMOUNT_JPY.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.AMOUNT_JPY.DataPropertyName = "AMOUNT_JPY";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Format = "N2";
            this.AMOUNT_JPY.DefaultCellStyle = dataGridViewCellStyle7;
            this.AMOUNT_JPY.HeaderText = "入金金額(日幣)";
            this.AMOUNT_JPY.Name = "AMOUNT_JPY";
            this.AMOUNT_JPY.ReadOnly = true;
            this.AMOUNT_JPY.Width = 150;
            // 
            // AMOUNT_NZD
            // 
            this.AMOUNT_NZD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.AMOUNT_NZD.DataPropertyName = "AMOUNT_NZD";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.Format = "N2";
            this.AMOUNT_NZD.DefaultCellStyle = dataGridViewCellStyle8;
            this.AMOUNT_NZD.HeaderText = "入金金額(紐幣)";
            this.AMOUNT_NZD.Name = "AMOUNT_NZD";
            this.AMOUNT_NZD.ReadOnly = true;
            this.AMOUNT_NZD.Width = 150;
            // 
            // FROM_DATE
            // 
            this.FROM_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.FROM_DATE.DataPropertyName = "FROM_DATE";
            this.FROM_DATE.HeaderText = "合約日期";
            this.FROM_DATE.Name = "FROM_DATE";
            this.FROM_DATE.ReadOnly = true;
            this.FROM_DATE.Width = 120;
            // 
            // END_DATE
            // 
            this.END_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.END_DATE.DataPropertyName = "END_DATE";
            this.END_DATE.HeaderText = "合約結束日期";
            this.END_DATE.Name = "END_DATE";
            this.END_DATE.ReadOnly = true;
            this.END_DATE.Width = 150;
            // 
            // YEAR
            // 
            this.YEAR.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.YEAR.DataPropertyName = "YEAR";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.YEAR.DefaultCellStyle = dataGridViewCellStyle9;
            this.YEAR.HeaderText = "年期";
            this.YEAR.Name = "YEAR";
            this.YEAR.ReadOnly = true;
            this.YEAR.Width = 90;
            // 
            // EXTEND_DATE
            // 
            this.EXTEND_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.EXTEND_DATE.DataPropertyName = "EXTEND_DATE";
            this.EXTEND_DATE.HeaderText = "展延日";
            this.EXTEND_DATE.Name = "EXTEND_DATE";
            this.EXTEND_DATE.ReadOnly = true;
            this.EXTEND_DATE.Width = 90;
            // 
            // ORDER_END
            // 
            this.ORDER_END.DataPropertyName = "ORDER_END";
            this.ORDER_END.HeaderText = "ORDER_END";
            this.ORDER_END.Name = "ORDER_END";
            this.ORDER_END.ReadOnly = true;
            this.ORDER_END.Visible = false;
            // 
            // frmContractDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.ClientSize = new System.Drawing.Size(803, 484);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.txtCustName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.picLoder);
            this.Controls.Add(this.dgv_header);
            this.Controls.Add(this.btnClose);
            this.Font = new System.Drawing.Font("微軟正黑體", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Name = "frmContractDetail";
            this.Text = "合約資料";
            this.Load += new System.EventHandler(this.frmContractDetail_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picLoder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_header)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.DataGridView dgv_header;
        private System.Windows.Forms.PictureBox picLoder;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCustName;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.DataGridViewCheckBoxColumn check;
        private System.Windows.Forms.DataGridViewTextBoxColumn ORDER_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUST_CNAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUST_ENAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn SALES;
        private System.Windows.Forms.DataGridViewTextBoxColumn IB_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn CURRENCY;
        private System.Windows.Forms.DataGridViewTextBoxColumn BALANCE;
        private System.Windows.Forms.DataGridViewTextBoxColumn AMOUNT_USD;
        private System.Windows.Forms.DataGridViewTextBoxColumn AMOUNT_NTD;
        private System.Windows.Forms.DataGridViewTextBoxColumn AMOUNT_RMB;
        private System.Windows.Forms.DataGridViewTextBoxColumn AMOUNT_EUR;
        private System.Windows.Forms.DataGridViewTextBoxColumn AMOUNT_AUD;
        private System.Windows.Forms.DataGridViewTextBoxColumn AMOUNT_JPY;
        private System.Windows.Forms.DataGridViewTextBoxColumn AMOUNT_NZD;
        private System.Windows.Forms.DataGridViewTextBoxColumn FROM_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn END_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn YEAR;
        private System.Windows.Forms.DataGridViewTextBoxColumn EXTEND_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn ORDER_END;
    }
}
