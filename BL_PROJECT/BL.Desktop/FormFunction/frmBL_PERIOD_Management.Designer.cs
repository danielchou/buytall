﻿namespace MK_DEMO.Destop.FormFunction
{
    partial class frmBL_PERIOD_Management
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cboYear = new System.Windows.Forms.ComboBox();
            this.dgv_header = new System.Windows.Forms.DataGridView();
            this.YEAR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MON = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.STATUS = new System.Windows.Forms.DataGridViewComboBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_header)).BeginInit();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnClose.Location = new System.Drawing.Point(181, 363);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(141, 54);
            this.btnClose.TabIndex = 5;
            this.btnClose.Text = "關閉";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSubmit
            // 
            this.btnSubmit.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnSubmit.Location = new System.Drawing.Point(26, 363);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(141, 54);
            this.btnSubmit.TabIndex = 4;
            this.btnSubmit.Text = "更新";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 19);
            this.label1.TabIndex = 6;
            this.label1.Text = "年度";
            // 
            // cboYear
            // 
            this.cboYear.FormattingEnabled = true;
            this.cboYear.Location = new System.Drawing.Point(57, 6);
            this.cboYear.Name = "cboYear";
            this.cboYear.Size = new System.Drawing.Size(121, 27);
            this.cboYear.TabIndex = 7;
            this.cboYear.SelectedIndexChanged += new System.EventHandler(this.cboYear_SelectedIndexChanged);
            // 
            // dgv_header
            // 
            this.dgv_header.AllowUserToAddRows = false;
            this.dgv_header.AllowUserToDeleteRows = false;
            this.dgv_header.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_header.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_header.ColumnHeadersHeight = 25;
            this.dgv_header.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv_header.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.YEAR,
            this.MON,
            this.STATUS});
            this.dgv_header.Location = new System.Drawing.Point(12, 37);
            this.dgv_header.Name = "dgv_header";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("微軟正黑體", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.RoyalBlue;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Yellow;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_header.RowHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_header.RowHeadersWidth = 25;
            this.dgv_header.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgv_header.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(226)))), ((int)(((byte)(239)))));
            this.dgv_header.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_header.RowTemplate.Height = 24;
            this.dgv_header.Size = new System.Drawing.Size(322, 320);
            this.dgv_header.TabIndex = 139;
            this.dgv_header.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_header_CellClick);
            this.dgv_header.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgv_header_CellMouseClick);
            this.dgv_header.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgv_header_DataError);
            // 
            // YEAR
            // 
            this.YEAR.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.YEAR.DataPropertyName = "YEAR";
            this.YEAR.HeaderText = "YEAR";
            this.YEAR.Name = "YEAR";
            this.YEAR.ReadOnly = true;
            this.YEAR.Width = 80;
            // 
            // MON
            // 
            this.MON.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.MON.DataPropertyName = "MON";
            this.MON.HeaderText = "MON";
            this.MON.Name = "MON";
            this.MON.ReadOnly = true;
            this.MON.Width = 80;
            // 
            // STATUS
            // 
            this.STATUS.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.STATUS.DataPropertyName = "STATUS";
            this.STATUS.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.STATUS.HeaderText = "STATUS";
            this.STATUS.Items.AddRange(new object[] {
            "OPEN",
            "CLOSED",
            "FUTURE"});
            this.STATUS.Name = "STATUS";
            this.STATUS.Width = 105;
            // 
            // frmBL_PERIOD_Management
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.ClientSize = new System.Drawing.Size(346, 429);
            this.Controls.Add(this.dgv_header);
            this.Controls.Add(this.cboYear);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSubmit);
            this.Font = new System.Drawing.Font("微軟正黑體", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Name = "frmBL_PERIOD_Management";
            this.Text = "開關帳月份設定";
            this.Load += new System.EventHandler(this.frmBL_PERIOD_Management_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_header)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboYear;
        private System.Windows.Forms.DataGridView dgv_header;
        private System.Windows.Forms.DataGridViewTextBoxColumn YEAR;
        private System.Windows.Forms.DataGridViewTextBoxColumn MON;
        private System.Windows.Forms.DataGridViewComboBoxColumn STATUS;
    }
}
