﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using MK.Demo.Utility;
using MK.Demo.Model;
using MK.Demo.Logic;
using System.Threading.Tasks;
using System.Linq;
using Newtonsoft.Json;

namespace MK_DEMO.Destop.FormFunction
{
    public partial class frmTransContractToNewSales : MK_DEMO.Destop.BaseForm
    {
        private List<string> liCustID = new List<string>();
        private List<BL_CUST> liCust_ALL = new List<BL_CUST>();
        private List<BL_EMP> liSales_ALL = new List<BL_EMP>();

        public frmTransContractToNewSales()
        {
            InitializeComponent();

            this.chkALL.BringToFront();
        }


        private void frmTransContractToNewSales_Load(object sender, EventArgs e)
        {

        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnBringCust_Click(object sender, EventArgs e)
        {
            var frm = new FrmSelectCust(this.liCustID) { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
            frm.ShowDialog();
            if (frm.status == MK.Demo.Data.enCommon.enFormStatus.OK)
            {
                this.txtCust.Text = frm.strDisplay;
                this.liCustID = frm.liCustID;
                this.liCust_ALL = frm.liCust;
            }
            frm.Dispose();
            frm = null;
        }

        private void btnBringSales_Click(object sender, EventArgs e)
        {
            var frm = new FrmSelectSales(true, true, Global.ORG_CODE) { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
            frm.ShowDialog();
            if (frm.status == MK.Demo.Data.enCommon.enFormStatus.OK)
            {
                this.txtSales.Text = frm.strCNAME_ALL;
                this.liSales_ALL = frm.liEmp;
            }
            frm.Dispose();
            frm = null;
        }

        private void dgv_header_CellClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void chkALL_CheckedChanged(object sender, EventArgs e)
        {
            this.dgv_header.Rows.Cast<DataGridViewRow>()
                .ForEach(x =>
                {
                    x.Cells["check"].Value = this.chkALL.Checked;
                });
        }

        #region 設定新業務
        
        private void button1_Click(object sender, EventArgs e)
        {
            var frm = new FrmSelectSales
                (
                    strORG_CODE: Global.ORG_CODE
                )
            { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
            frm.ShowDialog();
            if (frm.status == MK.Demo.Data.enCommon.enFormStatus.OK)
            {
                this.txtIBCodeTo.Text = frm.strIB_CODE.ConvertToString();
                this.txtSalesTo.Text = frm.strCNAME.ConvertToString();
                this.txtSalesTo.Tag = frm.strEMP_CODE.ConvertToString();
            }
            frm.Dispose();
            frm = null;
        }
        
        private void txtSalesTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                // this.txtIBCode.Text = string.Empty;
                this.txtSalesTo.Text = string.Empty;
                this.txtSalesTo.Tag = string.Empty;

                var data = base.Emp.GetBL_EMP();
                data = data.Where(x => x.IB_CODE.ConvertToString().Trim() != string.Empty).ToList();
                data = data
                 .Where(x => x.IB_CODE.ConvertToString().ToUpper().IndexOf(this.txtIBCodeTo.Text.Trim().ToUpper()) != -1)
                 .ToList();
                if (data.Count != 0)
                {
                    this.txtIBCodeTo.Text = data[0].IB_CODE.ConvertToString();
                    this.txtSalesTo.Text = data[0].CNAME.ConvertToString();
                    this.txtSalesTo.Tag = data[0].EMP_CODE.ConvertToString();
                }
            }
        }

        private void txtIBCodeTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                // this.txtIBCode.Text = string.Empty;
                this.txtSalesTo.Text = string.Empty;
                this.txtSalesTo.Tag = string.Empty;

                var data = base.Emp.GetBL_EMP();
                data = data.Where(x => x.IB_CODE.ConvertToString().Trim() != string.Empty).ToList();
                data = data
                 .Where(x => x.IB_CODE.ConvertToString().ToUpper().IndexOf(this.txtIBCodeTo.Text.Trim().ToUpper()) != -1)
                 .ToList();
                if (data.Count != 0)
                {
                    this.txtIBCodeTo.Text = data[0].IB_CODE.ConvertToString();
                    this.txtSalesTo.Text = data[0].CNAME.ConvertToString();
                    this.txtSalesTo.Tag = data[0].EMP_CODE.ConvertToString();
                }
            }
        }

        private void btnConfirm_Click(object sender, EventArgs e)
        {
            //  Todo > 檢核 and 寫入資料
            MessageBox.Show("系統開發中...");
        }

        #endregion

        private void btnSearch_Click(object sender, EventArgs e)
        {
            var data = base.Cust.GetOrderDataByTransSales(this.liCust_ALL, this.liSales_ALL);
            this.dgv_header.DataBindByList(data);
            this.lblDataCount.Text = "資料筆數 : " + data.Count().ConvertToString() + " 筆";
        }

        private void btnCleanSelection_Cust_Click(object sender, EventArgs e)
        {
            this.txtCust.Text = string.Empty;
            this.liCustID = new List<string>();
            this.liCust_ALL = new List<BL_CUST>();
        }

        private void btnCleanSelection_Sales_Click(object sender, EventArgs e)
        {
            this.txtSales.Text = string.Empty;
            this.liSales_ALL = new List<BL_EMP>();
        }
    }
}
