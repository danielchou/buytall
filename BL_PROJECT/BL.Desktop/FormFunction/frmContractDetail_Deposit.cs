﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using MK.Demo.Utility;
using MK.Demo.Model;
using MK.Demo.Logic;
using System.Threading.Tasks;
using System.Linq;
using static MK.Demo.Data.enCommon;

namespace MK_DEMO.Destop.FormFunction
{
    public partial class frmContractDetail_Deposit : MK_DEMO.Destop.BaseForm
    {
        private string strYear;
        private string strMonth;
        public enFormStatus status;

        public string strORDER_NO;
        public string strORDER_ID;
        public string strAMOUNT_USD;
        public string strAMOUNT_NTD;
        public string strAMOUNT_RMB;
        public string strAMOUNT_EUR;
        public string strAMOUNT_AUD;
        public string strAMOUNT_JPY;
        public string strAMOUNT_NZD;


        public frmContractDetail_Deposit(string Year, string Month)
        {
            InitializeComponent();

            this.strYear = Year;
            this.strMonth = Month;

            this.dgv_header.AutoGenerateColumns = false;
            this.dgv_header.MultiSelect = false;
            this.dgv_header.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }
        

        private void frmContractDetail_Deposit_Load(object sender, EventArgs e)
        {
            this.BindData();
        }

        private void BindData()
        {
            this.picLoder.Visible = true;
            this.picLoder.BringToFront();
            if (this.backgroundWorker1.IsBusy == false)
            {
                this.backgroundWorker1.RunWorkerAsync(this.txtCustName.Text);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            status = enFormStatus.Cancel;
            this.Close();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            System.Threading.Thread.Sleep(50);
            var data = base.Cust.GetUnExpiredOrderData(this.strYear, this.strMonth, e.Argument.ConvertToString());
            e.Result = data;
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.dgv_header.DataSource = new List<BL_ORDER>();
            var data = e.Result as List<BL_ORDER>;
            if (data != null)
            {
                this.dgv_header.DataSource = data;
            }
            this.picLoder.Visible = false;
        }

        private void dgv_header_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Parallel.ForEach(this.dgv_header.Rows.Cast<DataGridViewRow>(), x =>
                {
                    x.Cells["Qcheck"].Value = false;
                });
                this.dgv_header.Rows[e.RowIndex].Cells["Qcheck"].Value = true;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            var selection = this.dgv_header.Rows.Cast<DataGridViewRow>().Where(x => (bool?)x.Cells["Qcheck"].Value == true).ToList();
            if (selection.Count == 1)
            {
                this.strORDER_NO = selection[0].Cells["QORDER_NO"].Value.ConvertToString();
                this.strAMOUNT_USD = selection[0].Cells["QAMOUNT_USD"].Value.ConvertToString();
                this.strAMOUNT_NTD = selection[0].Cells["QAMOUNT_NTD"].Value.ConvertToString();
                this.strAMOUNT_RMB = selection[0].Cells["QAMOUNT_RMB"].Value.ConvertToString();
                this.strAMOUNT_EUR = selection[0].Cells["QAMOUNT_EUR"].Value.ConvertToString();
                this.strAMOUNT_AUD = selection[0].Cells["QAMOUNT_AUD"].Value.ConvertToString();
                this.strAMOUNT_JPY = selection[0].Cells["QAMOUNT_JPY"].Value.ConvertToString();
                this.strAMOUNT_NZD = selection[0].Cells["QAMOUNT_NZD"].Value.ConvertToString();
                this.strORDER_ID = selection[0].Cells["QORDER_ID"].Value.ConvertToString();

                this.status = enFormStatus.OK;
                this.Close();
            }
            else
            {
                MessageBox.Show("請單選");
            }

        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.BindData();
        }
    }
}
