﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MK.Demo.Utility;
using Microsoft.Reporting.WinForms;
using MK.Demo.Logic;

namespace MK_DEMO.Destop.FormFunction
{
    public partial class frmRPT_SalesPerformByCust : Form
    {
        private List<string> liCustID = new List<string>();

        public frmRPT_SalesPerformByCust()
        {
            InitializeComponent();
        }

        private void frmRPT_SalesPerformByCust_Load(object sender, EventArgs e)
        {
            this.InitComboBox();
            //this.BindReport();
        }

        private void InitComboBox()
        {
            this.dteDATE_S.Value = DateTime.Parse(DateTime.Now.ToString("yyyy/MM/01"));
            this.dteDATE_E.Value = DateTime.Now;
        }



        private void button2_Click(object sender, EventArgs e)
        {
            this.BindReport();
        }


        private void BindReport()
        {

            if (this.dteDATE_S.Text.IsDate() == false || this.dteDATE_E.Text.IsDate() == false)
            {

                MessageBox.Show("請選擇業績計算區間");
                return;
            }

            var DATE_S = DateTime.Parse(this.dteDATE_S.Value.ToString("yyyy/MM/dd"));
            var DATE_E = DateTime.Parse(this.dteDATE_E.Value.ToString("yyyy/MM/dd"));
            var strORG = Global.ORG_CODE;

            string exeFolder = Application.StartupPath;
            string reportPath = System.IO.Path.Combine(exeFolder, @"RPT\RPT_SALES_PERFORMANCE_BY_CUST.rdlc");
            var data = new logicCust(Global.IsTestMode).GetRPT_SALES_PERFORMANCE_BY_CUST(DATE_S, DATE_E, strORG);

            var binding = new BindingSource();
            binding.DataSource = data;
            this.reportViewer2.Reset();

            this.reportViewer2.LocalReport.ReportPath = reportPath;
            this.reportViewer2.LocalReport.DataSources.Add(new ReportDataSource("dsReport", binding));
            this.reportViewer2.RefreshReport();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

    




    }
}
