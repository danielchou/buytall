﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MK.Demo.Model;
using MK.Demo.Utility;
using static MK.Demo.Data.enCommon;

namespace MK_DEMO.Destop.FormFunction
{
    public partial class frmSearchCustCondition : BaseForm
    {
        public enFormStatus status;
        public QueryCust QueryCondition;

        public frmSearchCustCondition(QueryCust Condition)
        {
            InitializeComponent();
            status = enFormStatus.Cancel;
            this.QueryCondition = Condition;
        }

        private void frmSearchCustCondition_Load(object sender, EventArgs e)
        {
            this.txtCUST_CNAME.Text = this.QueryCondition.CUST_NAME.ConvertToString();
            this.txtID_NUMBER.Text = this.QueryCondition.ID_NUMBER.ConvertToString();
            this.txtID_NUMBER2.Text = this.QueryCondition.ID_NUMBER2.ConvertToString();
            this.txtPASSPORT.Text = this.QueryCondition.PASSPORT.ConvertToString();
            this.txtSALES.Text = this.QueryCondition.SALES.ConvertToString();
            if (this.QueryCondition.CONTRACT_COUNT != null && this.drpCONTRACT_COUNT_CONDITION.FindString(this.QueryCondition.CONTRACT_COUNT_CONDITION) != -1)
            {
                this.txtCONTRACT_COUNT.Text = this.QueryCondition.CONTRACT_COUNT.Value.ConvertToString();
                this.drpCONTRACT_COUNT_CONDITION.SelectedIndex = this.drpCONTRACT_COUNT_CONDITION.FindString(this.QueryCondition.CONTRACT_COUNT_CONDITION);
            }
            this.chkUnSendMail.Checked = this.QueryCondition.IschkUnSendMail.ConvertToString() == "Y";
            this.chkIsShowOrderEnd.Checked = this.QueryCondition.IsIncOrderEnd.ConvertToString() == "Y";
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            var txtInputCount = 0;
            var groupInputCount = 0;

            txtInputCount += this.txtCUST_CNAME.Text.Trim() != string.Empty ? 1 : 0;
            txtInputCount += this.txtID_NUMBER.Text.Trim() != string.Empty ? 1 : 0;
            txtInputCount += this.txtID_NUMBER2.Text.Trim() != string.Empty ? 1 : 0;
            txtInputCount += this.txtPASSPORT.Text.Trim() != string.Empty ? 1 : 0;
            txtInputCount += this.txtSALES.Text.Trim() != string.Empty ? 1 : 0;

            if (this.txtCONTRACT_COUNT.Text.IsNumeric() && this.drpCONTRACT_COUNT_CONDITION.Text.ConvertToString() != string.Empty)
            {
                groupInputCount += 1;
            }
            if (this.chkUnSendMail.Checked)
            {
                txtInputCount += 1;
            }
            if (this.chkIsShowOrderEnd.Checked)
            {
                txtInputCount += 1;
            }
            if (txtInputCount + groupInputCount == 0)
            {
                MessageBox.Show("請至少輸入一個條件");
                return;
            }

        

            this.QueryCondition.CUST_NAME = this.txtCUST_CNAME.Text;
            this.QueryCondition.ID_NUMBER = this.txtID_NUMBER.Text;
            this.QueryCondition.ID_NUMBER2 = this.txtID_NUMBER2.Text;
            this.QueryCondition.PASSPORT = this.txtPASSPORT.Text;
            this.QueryCondition.SALES = this.txtSALES.Text;

            this.QueryCondition.CONTRACT_COUNT = (int?)null;
            this.QueryCondition.CONTRACT_COUNT_CONDITION = string.Empty;
            this.QueryCondition.IschkUnSendMail = this.chkUnSendMail.Checked ? "Y" : "N";
            this.QueryCondition.IsIncOrderEnd = this.chkIsShowOrderEnd.Checked ? "Y" : "N";
            int iTry;
            if (this.txtCONTRACT_COUNT.Text.IsNumeric() && this.drpCONTRACT_COUNT_CONDITION.Text.ConvertToString() != string.Empty)
            {
                if (int.TryParse(this.txtCONTRACT_COUNT.Text.Trim(), out iTry))
                {
                    this.QueryCondition.CONTRACT_COUNT = iTry;
                    this.QueryCondition.CONTRACT_COUNT_CONDITION = this.drpCONTRACT_COUNT_CONDITION.Text.ConvertToString();
                }
            }
    
            
            this.status = enFormStatus.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.status = enFormStatus.Cancel;
            this.Close();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            this.txtCUST_CNAME.Text = string.Empty;
            this.txtID_NUMBER.Text = string.Empty;
            this.txtID_NUMBER2.Text = string.Empty;
            this.txtPASSPORT.Text = string.Empty;
            this.txtSALES.Text = string.Empty;
            this.txtCONTRACT_COUNT.Text = string.Empty;
            this.drpCONTRACT_COUNT_CONDITION.SelectedIndex = 0;
            this.chkUnSendMail.Checked = false;
            this.chkIsShowOrderEnd.Checked = false;
        }

        private void txtCUST_CNAME_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.btnSearch_Click(this.btnSearch, new EventArgs());
            }
        }

        private void txtID_NUMBER_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.btnSearch_Click(this.btnSearch, new EventArgs());
            }
        }

        private void txtID_NUMBER2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.btnSearch_Click(this.btnSearch, new EventArgs());
            }
        }

        private void txtPASSPORT_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.btnSearch_Click(this.btnSearch, new EventArgs());
            }
        }

        private void txtSALES_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.btnSearch_Click(this.btnSearch, new EventArgs());
            }
        }

        private void txtCONTRACT_COUNT_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.btnSearch_Click(this.btnSearch, new EventArgs());
            }
        }


    }
}
