﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using MK.Demo.Utility;
using MK.Demo.Model;
using MK.Demo.Logic;
using System.Threading.Tasks;
using System.Linq;
using static MK.Demo.Data.enCommon;
using Microsoft.Reporting.WinForms;
using System.IO;

namespace MK_DEMO.Destop.FormFunction
{
    public partial class frmContractApprove : MK_DEMO.Destop.BaseForm
    {
        public enFormStatus status;

        /// <summary>
        /// HK or TWN
        /// </summary>
        private string WHO;

        public frmContractApprove(string strWho)
        {
            InitializeComponent();

            this.WHO = strWho;

            this.dgv_header.AutoGenerateColumns = false;
            this.dgv_header.MultiSelect = false;
            this.dgv_header.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            ////  Todo > 
            ////  Approve Button For HK
            //this.button1.Visible = false;
            ////  Audit Button For TWN
            //this.btnBatchDepositOut.Visible = false;
            //switch (this.WHO.ConvertToString().Trim().ToUpper())
            //{
            //    case "HK":
            //        this.button1.Visible = true;
            //        break;
            //    case "TWN":
            //        this.btnBatchDepositOut.Visible = true;
            //        break;
            //    default:
            //        break;
            //}

            //

            
            
            


            string HK = "HK_ADMIN";
            if (Global.str_UserName == "RAININGCHAN")
            { Global.str_UserName = HK; }
            if (Global.str_UserName == "MAGGIELIN")
            { Global.str_UserName = HK; }
            if (Global.str_UserName == "MONIQUECHOY")
            { Global.str_UserName = HK; }
            if (Global.str_UserName == "OLIVIAHUNG")
            { Global.str_UserName = HK; }


            switch (Global.str_UserName)
            {
                case "NICOLE":
                    this.button1.Visible = false;
                    this.btnBatchDepositOut.Visible = false;
                    this.btnBatchReject.Visible = false;
                    this.btnBatchVoid.Visible = false;
                    break;
                case "JENNIFER":
                    this.button1.Visible = true;
                    this.btnBatchDepositOut.Visible = true;
                    this.btnBatchReject.Visible = true;
                    this.btnBatchVoid.Visible = true;
                    break;
                case "ADMIN":
                case "AMYHK":
                    this.button1.Visible = false;   //  沒有審批功能
                    this.btnBatchDepositOut.Visible = true;
                    this.btnBatchReject.Visible = true;
                    this.btnBatchVoid.Visible = true;
                    break;
                case "HK_ADMIN":
                case "MAGGIELIU":
                case "MONIQUECHOY":
                case "OLIVIAHUNG":
                    this.button1.Visible = true;
                    this.btnBatchDepositOut.Visible = false;
                    this.btnBatchReject.Visible = true;
                    this.btnBatchReject.Text = "批次退件作業(香港)";
                    this.btnBatchReject.Location = new Point(602, 6);
                    this.btnBatchReject.Width = 317;
                    this.btnBatchVoid.Visible = false;
                    break;
                default:
                    break;
            }

            if (Global.UserRole.ConvertToString() == "50")
            {
                //  HK Admin
                this.button1.Visible = true;
                this.btnBatchDepositOut.Visible = false;
                this.btnBatchReject.Visible = true;
                this.btnBatchReject.Text = "批次退件作業(香港)";
                this.btnBatchReject.Location = new Point(602, 6);
                this.btnBatchReject.Width = 317;
                this.btnBatchVoid.Visible = false;
            }
            if (Global.UserRole.ConvertToString() == "40")
            {
                this.button1.Visible = true;
                this.btnBatchDepositOut.Visible = true;
                this.btnBatchReject.Visible = true;
                this.btnBatchVoid.Visible = true;
            }
        }

        private void frmContractDetail_Load(object sender, EventArgs e)
        {
            this.cboApproveStatus.Items.Clear();
            //switch (this.WHO.ConvertToString().Trim().ToUpper())
            //{
            //    case "HK":
            //        this.cboApproveStatus.Items.Add("待審核");
            //        this.cboApproveStatus.SelectedIndex = 0;
            //        this.cboApproveStatus.Enabled = false;
            //        break;
            //    case "TWN":
            //        this.cboApproveStatus.Items.Add("全部");
            //        this.cboApproveStatus.Items.Add("待查核");
            //        this.cboApproveStatus.Items.Add("待審核");
            //        this.cboApproveStatus.Items.Add("已退件");
            //        break;
            //    default:
            //        break;
            //}

            this.cboApproveStatus.Items.Add("全部");
            this.cboApproveStatus.Items.Add("待查核");
            this.cboApproveStatus.Items.Add("待審批");
            this.cboApproveStatus.Items.Add("已退件");
            this.cboApproveStatus.Items.Add("已作廢");

            this.CleacCondition();
            this.BindData();
        }

        private void BindData()
        {
            this.picLoder.Visible = true;
            this.picLoder.BringToFront();
            if (this.backgroundWorker1.IsBusy == false)
            { 
                var strParam = new string[]
                {
                    this.txtOrderNo.Text.Trim(),
                    this.txtCustomerName.Text.Trim(),
                    this.cboApproveStatus.Text.Trim()
                };

                this.backgroundWorker1.RunWorkerAsync(strParam);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            status = enFormStatus.Cancel;
            this.Close();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            System.Threading.Thread.Sleep(50);
            var strPara = e.Argument as string[];
            if (strPara != null)
            {
                if (strPara.Length >= 3)
                {
                    var strORG = Global.ORG_CODE;
                    var data = base.Cust.GetUnApproveDataByOrderNo(strPara[0], strPara[1], strPara[2], strORG);

                    
                    e.Result = data;
                }
            }
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //this.dgv_header.DataSource = new List<BL_ORDER>();
            this.dgv_header.DataClean();
            var data = e.Result as List<BL_ORDER>;
            if (data != null)
            {
                //this.dgv_header.DataSource = data;
                this.dgv_header.DataBindByList(data);
                this.dgv_header.Rows.Cast<DataGridViewRow>()
                    .Where(x => x.Cells["ORG_CODE"].Value.ConvertToString().Trim() == "IND")
                    .ForEach(x =>
                    {
                        x.Cells["ORDER_NO"].Style.ForeColor = Color.DarkGreen;
                    });
            }
            this.picLoder.Visible = false;
        }

        private void dgv_header_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1 && e.ColumnIndex != -1)
            {
                //Parallel.ForEach(this.dgv_header.Rows.Cast<DataGridViewRow>(), x =>
                //{
                //    x.Cells["check"].Value = false;
                //});
                if ((bool?)this.dgv_header.Rows[e.RowIndex].Cells["check"].Value == true)
                {
                    this.dgv_header.Rows[e.RowIndex].Cells["check"].Value = false;
                }
                else
                {
                    this.dgv_header.Rows[e.RowIndex].Cells["check"].Value = true;
                }

                if (this.dgv_header.Columns[e.ColumnIndex].Name == "CUST_CNAME")
                {
                    //  開啟客戶詳細訂單資訊
                    var strOrderID = this.dgv_header.Rows[e.RowIndex].Cells["ORDER_ID"].Value.ConvertToString();
                    var strCustID = this.dgv_header.Rows[e.RowIndex].Cells["CUST_ID"].Value.ConvertToString();

                    var frm = new frmAddContract(strCustID, strOrderID, true) { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
                    frm.ShowDialog();
                    frm.Dispose();
                    frm = null;
                }
                if (this.dgv_header.Columns[e.ColumnIndex].Name == "CONTRACT_STATUS")
                {
                    if (this.dgv_header.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ConvertToString().Trim() == "已退件")
                    {
                        //  已退件資料, 可以修改重送
                        var strOrderID = this.dgv_header.Rows[e.RowIndex].Cells["ORDER_ID"].Value.ConvertToString();
                        var strCustID = this.dgv_header.Rows[e.RowIndex].Cells["CUST_ID"].Value.ConvertToString();
                        
                        var frm = new frmAddContract(strCustID, strOrderID, false, true) { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
                        frm.ShowDialog();
                        frm.Dispose();
                        frm = null;
                        this.BindData();
                    }
                }
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.BindData();
        }

       
        private void btnCleanCondition_Click(object sender, EventArgs e)
        {
            this.CleacCondition();
        }

        private void CleacCondition()
        {
            this.txtCustomerName.Text = string.Empty;
            this.txtOrderNo.Text = string.Empty;

            this.cboApproveStatus.SelectedIndex = 0;
        }

        private void dgv_header_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            Parallel.ForEach(this.dgv_header.Rows.Cast<DataGridViewRow>(), x =>
            {
               
            });
        }

        /// <summary>
        /// 批次查核作業(行政)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnBatchDepositOut_Click(object sender, EventArgs e)
        {
            var selectionItem = this.dgv_header.Rows.Cast<DataGridViewRow>()
                .Where(x => (bool?)x.Cells["check"].Value == true).ToList();

            if (selectionItem.Count() > 0)
            {
                //  在送出前, 先全部撈一次
                var dataSubmit = base.Cust.GetOrderDatabyOrderList(this.GetSubmitData);
                if (dataSubmit.Count > 0)
                {
                    //  查核只能抓取狀態為1的資料
                    if (dataSubmit.Where(x => (x.STATUS_CODE == null ? "" : x.STATUS_CODE.Value.ConvertToString()) == "1").ToList().Count() == dataSubmit.Count())
                    {
                        //  Todo > 
                        if (base.Cust.BatchAuditOrder(dataSubmit, Global.str_UserName))
                        {
                            MessageBox.Show("批次查核完成");
                            this.BindData();
                        }
                        else
                        {
                            MessageBox.Show("批次查核失敗");
                        }
                    }
                    else
                    {
                        MessageBox.Show("請選取尚未查核(待查核)的資料");
                        this.BindData();
                    }

                }
                else
                {
                    MessageBox.Show("選取的資料無符合此作業的執行條件");
                }

            }
            else
            {
                MessageBox.Show("請選擇要批次查核的資料");
            }
        }

        private List<BL_ORDER> GetSubmitData
        {
            get
            {
                var data = new List<BL_ORDER>();


                this.dgv_header.Rows.Cast<DataGridViewRow>()
                    .Where(x => (bool?)x.Cells["check"].Value == true)
                    .ToList()
                    .ForEach(x =>
                    {
                        //decimal decOrderID, decStatus, decOrderNo;
                        //DateTime dtFromDate;

                        //if (DateTime.TryParse(x.Cells["colFROM_DATE"].Value.ConvertToString(), out dtFromDate) &&
                        //    decimal.TryParse(x.Cells["ORDER_ID"].Value.ConvertToString(), out decOrderID) &&
                        //    decimal.TryParse(x.Cells["STATUS_CODE"].Value.ConvertToString(), out decStatus) &&
                        //    decimal.TryParse(x.Cells["ORDER_NO"].Value.ConvertToString(), out decOrderNo))
                        //{
                        //    data.Add(new BL_ORDER()
                        //    {
                        //        ORDER_ID = decOrderID,
                        //        STATUS_CODE = decStatus,
                        //        FROM_DATE = dtFromDate,
                        //        ORDER_NO= decOrderNo
                        //    });
                        //}


                        //  2019/04/18 Order_No 調整回文字
                        decimal decOrderID, decStatus;
                        DateTime dtFromDate;

                        if (DateTime.TryParse(x.Cells["colFROM_DATE"].Value.ConvertToString(), out dtFromDate) &&
                            decimal.TryParse(x.Cells["ORDER_ID"].Value.ConvertToString(), out decOrderID) &&
                            decimal.TryParse(x.Cells["STATUS_CODE"].Value.ConvertToString(), out decStatus))
                        {
                            data.Add(new BL_ORDER()
                            {
                                ORDER_ID = decOrderID,
                                STATUS_CODE = decStatus,
                                FROM_DATE = dtFromDate,
                                ORDER_NO = x.Cells["ORDER_NO"].Value.ConvertToString()
                            });
                        }

                    });


                return data;
            }
        }

        private void chkALL_CheckedChanged(object sender, EventArgs e)
        {
            Parallel.ForEach(this.dgv_header.Rows.Cast<DataGridViewRow>(), x =>
            {
                x.Cells["check"].Value = this.chkALL.Checked;
            });
        }

        private void btnBatchReject_Click(object sender, EventArgs e)
        {
            var selectionItem = this.dgv_header.Rows.Cast<DataGridViewRow>()
              .Where(x => (bool?)x.Cells["check"].Value == true).ToList();

            if (selectionItem.Count() > 0)
            {
                var dataSubmit = this.GetSubmitData;
                if (dataSubmit.Count > 0)
                {
                    var strRejectReason = string.Empty;
                    if (Global.str_UserName == "HK_ADMIN" || Global.UserRole.ConvertToString() == "50")
                    {
                        DialogResult dResult;

                        while (true)
                        {
                            dResult = InputBox("請輸入退件原因", "退件原因", ref strRejectReason);
                            if (dResult == DialogResult.OK)
                            {
                                if (strRejectReason != string.Empty)
                                {
                                    break;
                                }
                            }
                            else
                            {
                                return;
                            }
                        }
                        if (strRejectReason == string.Empty)
                        {
                            return;
                        }
                    }

                    //  審批只能抓取狀態為3的資料
                    if (dataSubmit.Where
                        (
                            x => 
                                (x.STATUS_CODE == null ? "" : x.STATUS_CODE.Value.ConvertToString()) == "3" ||
                                (x.STATUS_CODE == null ? "" : x.STATUS_CODE.Value.ConvertToString()) == "1"
                        ).ToList().Count() == dataSubmit.Count())
                    {
                        if (base.Cust.BatchRejectOrder(dataSubmit, strRejectReason, Global.str_UserName))
                        {
                            if (Global.str_UserName == "HK_ADMIN" || Global.UserRole.ConvertToString() == "50")
                            {
                                var strMailSubject = "合約審批退件通知";
                                var strMailTo = GetConfigValueByKey("MailAudit").Split(new string[] { ";" }, StringSplitOptions.None).ToList();
                                var strMailBody = @"
Dear <br/><br/>
　　合約審批退件通知, <br/>
　　退件原因：" + strRejectReason + @"<br/>
　　以下Order No為被退件之資料<br/>
<table>
<tr><th>Order No</th></tr>
";
                                dataSubmit.ForEach(x =>
                                {
                                    strMailBody += @"
<tr><td>" + (x.ORDER_NO.ConvertToString()) + "</td></tr>";
                                });
                                strMailBody += "</table>";

                                SendMail(strMailTo, strMailSubject, strMailBody);
                            }

                            MessageBox.Show("批次退件完成");
                            this.BindData();
                        }
                        else
                        {
                            MessageBox.Show("批次退件失敗");
                        }
                    }
                    else
                    {
                        MessageBox.Show("請選取尚未審批/查核的資料");
                    }

                    //if (dataSubmit.Where(x => (x.STATUS_CODE == null ? "" : x.STATUS_CODE.Value.ConvertToString()) != "1").ToList().Count() == 0)
                    //{
                    //    if (base.Cust.BatchRejectOrder(dataSubmit))
                    //    {
                    //        MessageBox.Show("批次退件完成");
                    //        this.BindData();
                    //    }
                    //    else
                    //    {
                    //        MessageBox.Show("批次退件失敗");
                    //    }
                    //}
                    //else
                    //{
                    //    MessageBox.Show("請選取尚未審批/查核的資料");
                    //}

                }
                else
                {
                    MessageBox.Show("選取的資料無符合此作業的執行條件");
                }

            }
            else
            {
                MessageBox.Show("請選擇要批次退件的資料");
            }
        }

        /// <summary>
        /// 批次Approve作業(香港)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {

            var selectionItem = this.dgv_header.Rows.Cast<DataGridViewRow>()
                .Where(x => (bool?)x.Cells["check"].Value == true).ToList();

            if (selectionItem.Count() > 0)
            {
                var dataSubmit = base.Cust.GetOrderDatabyOrderList(this.GetSubmitData);
                if (dataSubmit.Count > 0)
                {
                    //  審批只能抓取狀態為3的資料
                    if (dataSubmit.Where(x => (x.STATUS_CODE == null ? "" : x.STATUS_CODE.Value.ConvertToString()) == "3").ToList().Count() == dataSubmit.Count())
                    {
                        if (base.Cust.BatchApproveOrder(dataSubmit, Global.str_UserName))
                        {
                            MessageBox.Show("批次審批完成");
                            this.BindData();
                        }
                        else
                        {
                            MessageBox.Show("批次審批失敗");
                        }
                    }
                    else
                    {
                        MessageBox.Show("請選取尚未審批(待審批)的資料");
                        this.BindData();
                    }
                }
                else
                {
                    MessageBox.Show("選取的資料無符合此作業的執行條件");
                }

            }
            else
            {
                MessageBox.Show("請選擇要批次審批的資料");
            }
        }



        public static DialogResult InputBox(string title, string promptText, ref string value)
        {
            Form form = new Form();
            Label label = new Label();
            TextBox textBox = new TextBox();
            Button buttonOk = new Button();
            Button buttonCancel = new Button();

            form.Text = title;
            form.Font = new Font("微軟正黑體", 11);
            label.Text = promptText;
            textBox.Text = value;
            textBox.Multiline = true;

            buttonOk.Text = "OK";
            buttonCancel.Text = "Cancel";
            buttonOk.DialogResult = DialogResult.OK;
            buttonCancel.DialogResult = DialogResult.Cancel;

            label.SetBounds(9, 12, 372, 13);
            textBox.SetBounds(12, 36, 372, 80);
            buttonOk.SetBounds(228, 172, 75, 23);
            buttonCancel.SetBounds(309, 172, 75, 23);

            label.AutoSize = true;
            textBox.Anchor = textBox.Anchor | AnchorStyles.Right;
            buttonOk.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            buttonCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;

            form.ClientSize = new Size(396, 207);
            form.Controls.AddRange(new Control[] { label, textBox, buttonOk, buttonCancel });
            form.ClientSize = new Size(Math.Max(300, label.Right + 10), form.ClientSize.Height);
            form.FormBorderStyle = FormBorderStyle.FixedDialog;
            form.StartPosition = FormStartPosition.CenterScreen;
            form.MinimizeBox = false;
            form.MaximizeBox = false;
            //form.AcceptButton = buttonOk;
            //form.CancelButton = buttonCancel;

            DialogResult dialogResult = form.ShowDialog();
            value = textBox.Text;
            return dialogResult;
        }

        private void cboApproveStatus_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnBatchVoid_Click(object sender, EventArgs e)
        {
            var selectionItem = this.dgv_header.Rows.Cast<DataGridViewRow>()
                            .Where(x => (bool?)x.Cells["check"].Value == true).ToList();

            if (selectionItem.Count() > 0)
            {
                //  在送出前, 先全部撈一次
                var dataSubmit = base.Cust.GetOrderDatabyOrderList(this.GetSubmitData);
                if (dataSubmit.Count > 0)
                {
                    if (MessageBox.Show("請確認是否要執行批次作廢作業？", "系統訊息", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        //  查核只能抓取狀態為1的資料
                        if (dataSubmit.Where(x => (x.STATUS_CODE == null ? "" : x.STATUS_CODE.Value.ConvertToString()) == "1").ToList().Count() == dataSubmit.Count())
                        {
                            //  Todo > 
                            if (base.Cust.BatchVoidOrder(dataSubmit, Global.str_UserName))
                            {
                                MessageBox.Show("批次作廢完成");
                                this.BindData();
                            }
                            else
                            {
                                MessageBox.Show("批次作廢失敗");
                            }
                        }
                        else
                        {
                            MessageBox.Show("請選取尚未查核(待查核)的資料進行作廢");
                            this.BindData();
                        }
                    }

                }
                else
                {
                    MessageBox.Show("選取的資料無符合此作業的執行條件");
                }

            }
            else
            {
                MessageBox.Show("請選擇要批次作廢的資料");
            }
        }

        private void btn_IND_Contract_Notic_Click(object sender, EventArgs e)
        {
            var selectionItem = this.dgv_header.Rows.Cast<DataGridViewRow>()
                .Where(x => (bool?)x.Cells["check"].Value == true).ToList();

            if (selectionItem.Count() > 0)
            {
                var cntIND_Contract = selectionItem.Where(x => x.Cells["ORG_CODE"].Value.ConvertToString().Trim() == "IND").Count(); 
                if (cntIND_Contract == selectionItem.Count)
                {
                    using (var fbd = new FolderBrowserDialog())
                    {
                        DialogResult result = fbd.ShowDialog();
                        if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                        {
                            //  存放路徑
                            var strSavePath = fbd.SelectedPath;

                            string exeFolder = Application.StartupPath;
                            string reportPath = System.IO.Path.Combine(exeFolder, @"RPT\RPT_Notificationletter.rdlc");

                            selectionItem
                                .ForEach(x =>
                                {
                                    var strCustName = x.Cells["CUST_CNAME"].Value.ConvertToString();
                                    var strOrderNo = x.Cells["ORDER_NO"].Value.ConvertToString();
                                    var strCustMail = x.Cells["EMAIL_1"].Value.ConvertToString().Trim();
                                    var strSalesMail = x.Cells["SALES_EMAIL"].Value.ConvertToString().Trim();

                                    ReportViewer rptExport = new ReportViewer();
                                    rptExport.Reset();
                                    rptExport.LocalReport.ReportPath = reportPath;
                                    rptExport.LocalReport.SetParameters(new ReportParameter("CUST_NAME", strCustName));
                                    rptExport.LocalReport.SetParameters(new ReportParameter("ORDER_NO", strOrderNo));
                                    rptExport.RefreshReport();

                                    //  Export
                                    var strFileName = strSavePath + "/" + strOrderNo + "_" + strCustName + ".pdf";

                                    Warning[] warnings;
                                    string[] streamids;
                                    string mimeType;
                                    string encoding;
                                    string filenameExtension;
                                    
                                    // 

                                    byte[] bytes = rptExport.LocalReport.Render(
                                        "PDF", null, out mimeType, out encoding, out filenameExtension,
                                        out streamids, out warnings);

                                    using (System.IO.FileStream fs = new System.IO.FileStream(strFileName, System.IO.FileMode.Create))
                                    {
                                        fs.Write(bytes, 0, bytes.Length);
                                    }

                                    //  Send Mail
                                    //  Todo > Alex > Testing and Change Body content.
                                    var strMailSubject = "Sincerely thank you for becoming our valued wealth management client";
                                    var strMailTo = strCustMail;
                                    string strMailBody = @"<html>" +
                                    "<body style=\"font-family: Calisto MT;font-size: 14px;line-height: 1.25;\">" +
                                    "<p>Dear " + strCustName + "：</p>" +
                                    "<p>Thank you for your trust in Best Leader Markets PTY Ltd., <br/> we will give you the contract number: " + strOrderNo + " <br/>to complete the necessary administrative matters, <br/>because the operation process must be completed overseas, <br/>there will be a waiting time, please also be patient If you have any questions, <br/>you can contact your service business and we will provide you with professional services.</p>" +
                                    "<br/>" +
                                    "Thank you." +
                                    "<br/><br/>" +
                                    "Best Leader Markets PTY Ltd" +
                                    "</body>" +
                                    "</html>";


                                    var strCC = strSalesMail + ";customerservice@bestleader-service.com";
                                    var strBCC = "levy.chang@best-rich.com;customerservice@bestleader-service.com;daniel0422@gmail.com;" + strSalesMail;
                                    SendMail
                                    (
                                        MailTo: new List<string>() { strMailTo },
                                        MailSubject: strMailSubject,
                                        MailBody: strMailBody,
                                      
                                        strFileName: strFileName,
                                        strBCC: strBCC,
                                        strCC: strCC
                                    );
                                });
                        }
                    }
                }
                else
                {
                    MessageBox.Show("請取消選取選取非印尼合約的資料列");
                }
            }
            else
            {
                MessageBox.Show("請選擇要發送通知的印尼合約");
            }
        }
        
        private void btn_Welcome_Contract_Notic_Click(object sender, EventArgs e)
        {
            frmContractApproveDetail f = new frmContractApproveDetail(
                    this.txtOrderNo.Text.Trim(),
                    this.txtCustomerName.Text.Trim(),
                    "待查核"
                    //this.cboApproveStatus.Text.Trim()
                );
            f.ShowDialog();
        }

        private void btn_Welcome_Contract_Notic_Click2(object sender, EventArgs e)
        {
            var selectionItem = this.dgv_header.Rows.Cast<DataGridViewRow>()
                .Where(x => (bool?)x.Cells["check"].Value == true).ToList();
            if (selectionItem.Count() > 0)
            {
                //using (var fbd = new FolderBrowserDialog())
                //{
                //    DialogResult result = fbd.ShowDialog();
                //    if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                //    {
                //        //  存放路徑
                //        var strSavePath = fbd.SelectedPath;

                //        string exeFolder = Application.StartupPath;
                //        string reportPath = System.IO.Path.Combine(exeFolder, @"RPT\RPT_Notificationletter_Welcome.rdlc");

                        selectionItem
                            .ForEach(x =>
                            {
                                var strCustName = x.Cells["CUST_CNAME"].Value.ConvertToString();
                                var strCustName_Eng = x.Cells["CUST_ENAME"].Value.ConvertToString();
                                var strOrderNo = x.Cells["ORDER_NO"].Value.ConvertToString();
                                var strCustMail = x.Cells["EMAIL_1"].Value.ConvertToString().Trim();
                                var strSalesMail = x.Cells["SALES_EMAIL"].Value.ConvertToString().Trim();

                                //ReportViewer rptExport = new ReportViewer();
                                //rptExport.Reset();
                                //rptExport.LocalReport.ReportPath = reportPath;
                                //rptExport.LocalReport.SetParameters(new ReportParameter("CUST_CNAME", strCustName));
                                //rptExport.LocalReport.SetParameters(new ReportParameter("CUST_ENAME", strCustName_Eng));
                                //rptExport.LocalReport.SetParameters(new ReportParameter("ORDER_NO", strOrderNo));
                                //rptExport.RefreshReport();

                                ////  Export
                                //var strFileName = strSavePath + "/" + strOrderNo + "_" + strCustName + ".pdf";

                                //Warning[] warnings;
                                //string[] streamids;
                                //string mimeType;
                                //string encoding;
                                //string filenameExtension;

                                //// 

                                //byte[] bytes = rptExport.LocalReport.Render(
                                //"PDF", null, out mimeType, out encoding, out filenameExtension,
                                //out streamids, out warnings);

                                //using (System.IO.FileStream fs = new System.IO.FileStream(strFileName, System.IO.FileMode.Create))
                                //{
                                //    fs.Write(bytes, 0, bytes.Length);
                                //}

                                //  Send Mail
                                //  Todo > Alex > Testing and Change Body content.
                                var strMailSubject = "Sincerely thank you for becoming our valued wealth management client";
                                var strMailTo = strCustMail;
                                string strMailBody = @"<html>" +
                                    "<body style=\"font-family: Calisto MT;font-size: 14px;line-height: 1.25;\">" +
                                    "<p>Dear " + strCustName + "：</p>" +
                                    "<p>Thank you for your trust in Best Leader Markets PTY Ltd., <br/> we will give you the contract number: " + strOrderNo + " <br/>to complete the necessary administrative matters, <br/>because the operation process must be completed overseas, <br/>there will be a waiting time, please also be patient If you have any questions, <br/>you can contact your service business and we will provide you with professional services.</p>" +
                                    "<br/>" +
                                    "Thank you." +
                                    "<br/><br/>" +
                                    "Best Leader Markets PTY Ltd" +
                                    "</body>" +
                                    "</html>";

                                strMailBody = @"<html>" +
                                    "<body style=\"font-family: Calisto MT;font-size: 14px;line-height: 1.25;\">" +
                                    "<p>尊敬的" + strCustName + "，</p>" +
                                    "<p>謝謝您的支持與愛護，我們已收到您的合約 " + strOrderNo + "</p>" +
                                    "<p>合約會送至國外用印，這會需要一段時間，請耐心等待。</p>" +
                                    "<p>如有任何問題請與您的專員聯絡，我們將會提供您專業的服務。</p>" +
                                    "<p>&nbsp;</p>" +
                                    "<p>謝謝</p>" +
                                    "<p>&nbsp;</p>" +
                                    "<p>Best Leader Markets PTY Ltd</p>" +
                                    "<p>&nbsp;</p>" +
                                    "<p>&nbsp;</p>" +
                                    "<p>&nbsp;</p>" +
                                    "<p>Dear " + strCustName_Eng + "：</p>" +
                                    "<p>Thank you for your trust in Best Leader Markets PTY Ltd.,</p>" +
                                    "<p>we will give you the contract number: " + strOrderNo + "</p>" +
                                    "<p>to complete the necessary administrative matters,</p>" +
                                    "<p>because the operation process must be completed overseas,</p>" +
                                    "<p>there will be a waiting time, please also be patient .</p>" +
                                    "<p>If you have any questions, there is always welcome to contact to your</p>" +
                                    "<p>service business and we will provide you with professional services.</p>" +
                                    "<p>&nbsp;</p>" +
                                    "<p>Thank you.</p>" +
                                    "<p>&nbsp;</p>" +
                                    "<p>Best Leader Markets PTY Ltd</p>" +
                                    "</body>" +
                                    "</html>";

                                var strCC = strSalesMail + ";customerservice@bestleader-service.com";
                                var strBCC = string.Empty;

                                SendMail
                                (
                                    MailTo: new List<string>() { strMailTo },
                                    MailSubject: strMailSubject,
                                    MailBody: strMailBody,

                                    strFileName: string.Empty,
                                    strBCC: strBCC,
                                    strCC: strCC
                                );

                                MessageBox.Show("Welcome Letter寄出成功");
                            });
                //    }
                //}
            }
            else
            {
                MessageBox.Show("請選擇要發送Welcome Letter通知的合約");
            }
        }



    }
}
