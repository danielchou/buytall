﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using MK.Demo.Utility;
using MK.Demo.Model;
using MK.Demo.Logic;
using System.Threading.Tasks;
using System.Linq;

namespace MK_DEMO.Destop.FormFunction
{
    public partial class frmCustDataModifyHist : MK_DEMO.Destop.BaseForm
    {
        private string CUST_ID;
        public frmCustDataModifyHist(string strCustID)
        {
            InitializeComponent();

            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            this.CUST_ID = strCustID;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmCustDataModifyHist_Load(object sender, EventArgs e)
        {
            var data = base.Cust.GetLogDataByCustID(this.CUST_ID);
            this.dataGridView1.DataBindByList(data);
        }

    }
}
