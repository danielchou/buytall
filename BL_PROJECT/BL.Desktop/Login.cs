﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MK.Demo.Utility;
using MK.Demo.Logic;
using System.Net;
using MK.Demo.Model;

namespace MK_DEMO.Destop
{
    public partial class Login : BaseForm
    {
        public Login()
        {
            InitializeComponent();
        }
        protected override bool IsHiddenCloseButton()
        {
            return true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var username = this.txtUsername.Text.Trim().ToUpper();
            var password = this.txtPassword.Text.Trim().ToUpper();
            if (
                (username == "ADMIN" & password == "1318")  //  台灣行政, 只能查詢TW1資料
                || (username == "HK_ADMIN" & password == "1234")
                || (username == "RAININGCHAN" & password == "1234")
                || (username == "MAGGIELIU" & password == "1234")
                || (username == "MONIQUECHOY" & password == "1234")
                || (username == "OLIVIAHUNG" & password == "1234")
                || (username == "AMYHK" & password == "1234")   //  香港行政, 只能查詢HK1資料
                || (username == "AMYTW2" & password == "1234")
                || (username == "MAGGIELIU" & password == "1234")
                || (username == "MONIQUECHOY" & password == "1234")
                || (username == "OLIVIAHUNG" & password == "1234")
                )
            {

                Global.str_UserName = username;
                Global.IsTestMode = this.chkTestMode.Checked;

                //var dataEmp = base.Emp.GetBL_EMP();

                var data = new EMP_MANAGEMENT(this.chkTestMode.Checked).SelectEmpByAccount(Global.str_UserName);
                //var data = base.Emp.SelectEmpByAccount(Global.str_UserName);
                if (data.Count > 0)
                {
                    Global.UserRole = data[0].ROLE_NO.ConvertToString();
                    Global.ORG_CODE = data[0].ORG_CODE.ConvertToString();
                }
                if
                    (
                        Global.UserRole.ConvertToString().Trim() == string.Empty &&
                        (
                            username == "HK_ADMIN" ||
                            username == "MAGGIELIU" ||
                            username == "MONIQUECHOY" ||
                            username == "OLIVIAHUNG"
                        )
                    )
                {
                    Global.UserRole = "50";
                    Global.ORG_CODE = string.Empty;
                }
                if (Global.UserRole.ConvertToString().Trim() == string.Empty && username == "RAININGCHAN")
                {
                    Global.UserRole = "50";
                }
                if (Global.UserRole.ConvertToString().Trim() == string.Empty && username == "MAGGIELIU")
                {
                    Global.UserRole = "50";
                }
                if (Global.UserRole.ConvertToString().Trim() == string.Empty && username == "MONIQUECHOY")
                {
                    Global.UserRole = "50";
                }
                if (Global.UserRole.ConvertToString().Trim() == string.Empty && username == "OLIVIAHUNG")
                {
                    Global.UserRole = "50";
                }

                #region Set Org Code

                switch (Global.str_UserName.ConvertToString().ToUpper().Trim())
                {
                    case "ADMIN":
                        Global.ORG_CODE = "TW1";
                        break;
                    case "AMYTW2":
                        Global.ORG_CODE = "TW2";
                        break;
                    case "AMYHK":
                        Global.ORG_CODE = "HK1";
                        break;
                    default:
                        //  原本有Hard Code 行政 = TW1, 目前都改為抓DB的即可
                        break;
                }

                #endregion
                
                this.WriteLoginInfo(username);
                var frm = new Main(this);
                frm.Show();
                this.Hide();
            }
            else
            {
                var data = new EMP_MANAGEMENT(this.chkTestMode.Checked).SelectEmpByAccount(username);
                //var data = base.Emp.SelectEmpByAccount(username);
                if (data.Count == 1)
                {
                    if (password == data[0].LOGIN_PASSWORD.ConvertToString().ToUpper())
                    {
                        if
                            (
                                (this.chkTestMode.Checked && data[0].IS_USE_TEST.ConvertToString().Trim() == "Y") ||
                                (this.chkTestMode.Checked == false && data[0].IS_USE_PROD.ConvertToString().Trim() == "Y")
                            )
                        {
                            Global.str_UserName = username;
                            Global.IsTestMode = this.chkTestMode.Checked;
                            Global.UserRole = data[0].ROLE_NO.ConvertToString();
                            Global.ORG_CODE = data[0].ORG_CODE.ConvertToString();

                            this.WriteLoginInfo(username);
                            var frm = new Main(this);
                            frm.Show();
                            this.Hide();
                        }
                        else
                        {
                            MessageBox.Show("帳號密碼輸入有錯誤");
                        }
                    }
                    else
                    {
                        MessageBox.Show("帳號密碼輸入有錯誤");
                    }
                }
                else
                {
                    MessageBox.Show("帳號密碼輸入有錯誤");
                }
            }
        }

        private void Login_Load(object sender, EventArgs e)
        {
            this.chkTestMode.Checked = false;


            if (System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed)
            {
                System.Deployment.Application.ApplicationDeployment ad = System.Deployment.Application.ApplicationDeployment.CurrentDeployment;
                Version ver = ad.CurrentVersion;
                this.label1.Text = "Ver " + ver.ConvertToString();
            }
            else
            {
                this.label1.Text = "Debug mode.";
            }

        }

        private void WriteLoginInfo(string strAccount)
        {
            IPAddress[] localIPs = Dns.GetHostAddresses(Dns.GetHostName());
            var strIPAddress = string.Empty;
            if (localIPs.Where(x => x.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork).ToList().Count() > 0)
            {
                strIPAddress = localIPs.Where(x => x.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork).ToList()[0].ConvertToString();
            }

            Global.BL_BACKEND_LOGIN_LOG_SN = base.Cust.GetNextSEQ("BL_BACKEND_LOGIN_LOG_S1");

            var dataLog = new BL_BACKEND_LOGIN_LOG()
            {
                LOGIN_IP = strIPAddress,
                LOGIN_ACCOUNT = strAccount
            };

            new logicCust(this.chkTestMode.Checked).WriteLog(Global.BL_BACKEND_LOGIN_LOG_SN, strAccount, strIPAddress);
        }

        private void txtUsername_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Tab)
            {
                this.txtPassword.Focus();
            }
        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.button1_Click(this.button1, new EventArgs());
            }
        }

    }
}
