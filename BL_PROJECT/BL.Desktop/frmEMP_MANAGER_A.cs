﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using MK.Demo.Utility;
using MK.Demo.Model;
using MK.Demo.Logic;
using System.Threading.Tasks;
using System.Linq;
using static MK.Demo.Data.enCommon;


namespace MK_DEMO.Destop
{
    public partial class frmEMP_MANAGER_A : MK_DEMO.Destop.BaseForm
    {
        private string EMP_ID = "";
        public enFormStatus status;

        public frmEMP_MANAGER_A(string strEMP_ID = "")
        {
            InitializeComponent();

            this.EMP_ID = strEMP_ID;

            var dte = new List<DateTimePicker>()
            {
                this.dteDATE_OF_BIRTH,
                this.dteHIRE_END_DATE,
                this.dteHIRE_START_DATE
            };
            dte.ForEach(x =>
            {
                x.Value = DateTime.Now;
                x.Text = string.Empty;
                x.Format = DateTimePickerFormat.Custom;
                x.CustomFormat = " ";
            });
      

        }

        private void frmEMP_MANAGER_A_Load(object sender, EventArgs e)
        {
            this.rdoTW1.Visible = false;
            this.rdoTW1.Checked = false;
            this.rdoTW2.Visible = false;
            this.rdoTW2.Checked = false;
            this.rdoHK1.Visible = false;
            this.rdoHK1.Checked = false;

            switch (Global.ORG_CODE)
            {
                case "TW1":
                    this.rdoTW1.Visible = true;
                    break;
                case "TW2":
                    this.rdoTW2.Visible = true;
                    break;
                case "HK1":
                    this.rdoHK1.Visible = true;
                    break;
                default:
                    this.rdoTW1.Visible = true;
                    this.rdoTW2.Visible = true;
                    this.rdoHK1.Visible = true;
                    break;
            }

            //var dataDept = base.Cust.GetDeptCodeData();
            //this.drpDEPT_VALUE.DataBindByList(dataDept, "DEPT_NAME", "DEPT_CODE", null); ;
            //this.drpDEPT_VALUE.Items.Add("行政管理部");
            //this.drpDEPT_VALUE.Items.Add("V1");
            //this.drpDEPT_VALUE.Items.Add("V2");
            //this.drpDEPT_VALUE.Items.Add("V3");
            //this.drpDEPT_VALUE.Items.Add("V5");
            //this.drpDEPT_VALUE.Items.Add("法人關係部");
            //this.drpDEPT_VALUE.Items.Add("通路部");
            //this.drpDEPT_VALUE.Items.Add("HK");

            this.cboRole.Items.Add("申請人員"); //  10
            this.cboRole.Items.Add("查核人員"); //  20
            this.cboRole.Items.Add("審批人員"); //  30
            this.cboRole.Items.Add("Admin");    //  40
            this.cboRole.Items.Add("HK-Admin");    //  50

            this.panAdmin.Visible = false;
            if
                (
                    Global.str_UserName.ToUpper() == "ADMIN" ||
                    Global.UserRole.ConvertToString() == "10" ||
                    Global.UserRole.ConvertToString() == "40"
                )
            {
                this.panAdmin.Visible = true;
            }

            if (this.EMP_ID.Trim() != string.Empty)
            {
                this.BindInitData();
            }
            else
            {
                //  新增沒有刪除鈕
                this.btnDel.Visible = false;
                this.txtEmpCode.Text = base.Cust.GetEmpCode;

                switch (Global.ORG_CODE)
                {
                    case "TW1":
                        this.rdoTW1.Checked = true;
                        this.SetDeptVal("TW1");
                        break;
                    case "TW2":
                        this.rdoTW2.Checked = true;
                        this.SetDeptVal("TW2");
                        break;
                    case "HK1":
                        this.rdoHK1.Checked = true;
                        this.SetDeptVal("HK1");
                        break;
                    default:
                        break;
                }
            }
        }

        private void BindInitData()
        {
            this.rdoTW1.Checked = false;
            this.rdoTW2.Checked = false;
            this.rdoHK1.Checked = false;
            //  修改用的
            var data = base.Cust.SelectBL_EMPByEMP_ID(this.EMP_ID.ConvertToString());
            if (data != null)
            {
                this.btnDel.Visible = true;
                this.txtEmpCode.Text = data.EMP_CODE.ConvertToString();
                this.txtCNAME.Text = data.CNAME.ConvertToString();
                this.txtENAME.Text = data.ENAME.ConvertToString();
                this.txtID_NUMBER.Text = data.ID_NUMBER.ConvertToString();
                this.txtTITLE.Text = data.TITLE.ConvertToString();
                this.txtSUPERVISOR_ID.Text = data.SUPERVISOR_NAME.ConvertToString();
                this.txtSUPERVISOR_ID.Tag = data.SUPERVISOR_ID.ConvertToString();
                this.txtIB_CODE.Text = data.IB_CODE.ConvertToString();
                this.txtEMAIL.Text = data.EMAIL.ConvertToString();

                this.drpSEX.SelectedIndex = this.drpSEX.FindString(data.SEX.ConvertToString());
                switch (data.ORG_CODE.ConvertToString())
                {
                    case "TW1":
                        this.SetDeptVal(data.ORG_CODE.ConvertToString());
                        this.rdoTW1.Checked = true;
                        break;
                    case "TW2":
                        this.SetDeptVal(data.ORG_CODE.ConvertToString());
                        this.rdoTW2.Checked = true;
                        break;
                    case "HK1":
                        this.SetDeptVal(data.ORG_CODE.ConvertToString());
                        this.rdoHK1.Checked = true;
                        break;
                    default:
                        break;
                }
                //switch (data.DS_DEPT.ConvertToString())
                //{
                //    case "行政":
                //        this.drpDEPT_VALUE.SelectedIndex = this.drpDEPT_VALUE.FindStringExact("行政管理部");
                //        break;
                //    case "V1":
                //        this.drpDEPT_VALUE.SelectedIndex = this.drpDEPT_VALUE.FindStringExact("V1");
                //        break;
                //    case "V2":
                //        this.drpDEPT_VALUE.SelectedIndex = this.drpDEPT_VALUE.FindStringExact("V2");
                //        break;
                //    case "V3":
                //        this.drpDEPT_VALUE.SelectedIndex = this.drpDEPT_VALUE.FindStringExact("V3");
                //        break;
                //     case "V5":
                //        this.drpDEPT_VALUE.SelectedIndex = this.drpDEPT_VALUE.FindStringExact("V5");
                //        break;
                //    case "法人":
                //        this.drpDEPT_VALUE.SelectedIndex = this.drpDEPT_VALUE.FindStringExact("法人關係部");
                //        break;
                //    case "通路":
                //        this.drpDEPT_VALUE.SelectedIndex = this.drpDEPT_VALUE.FindStringExact("通路部");
                //        break;
                //    case "HK":
                //        this.drpDEPT_VALUE.SelectedIndex = this.drpDEPT_VALUE.FindStringExact("HK");
                //        break;
                //    default:
                //        break;
                //}
                this.drpDEPT_VALUE.SelectedIndex = this.drpDEPT_VALUE.FindStringExact(data.DS_DEPT.ConvertToString());

                if (data.HIRE_START_DATE != null)
                {
                    this.dteHIRE_START_DATE.Format = DateTimePickerFormat.Long;
                    this.dteHIRE_START_DATE.Value = data.HIRE_START_DATE.Value;
                    this.dteHIRE_START_DATE.Text = data.HIRE_START_DATE.Value.ToString("yyyy/MM/dd");
                }
                else
                {
                    this.dteHIRE_START_DATE.Value = DateTime.Now;
                    this.dteHIRE_START_DATE.Text = string.Empty;
                    this.dteHIRE_START_DATE.Format = DateTimePickerFormat.Custom;
                    this.dteHIRE_START_DATE.CustomFormat = " ";
                }

                if (data.HIRE_END_DATE != null)
                {
                    this.dteHIRE_END_DATE.Format = DateTimePickerFormat.Long;
                    this.dteHIRE_END_DATE.Value = data.HIRE_END_DATE.Value;
                    this.dteHIRE_END_DATE.Text = data.HIRE_END_DATE.Value.ToString("yyyy/MM/dd");
                }
                else
                {
                    this.dteHIRE_END_DATE.Value = DateTime.Now;
                    this.dteHIRE_END_DATE.Text = string.Empty;
                    this.dteHIRE_END_DATE.Format = DateTimePickerFormat.Custom;
                    this.dteHIRE_END_DATE.CustomFormat = " ";
                }


                if (data.DATE_OF_BIRTH != null)
                {
                    this.dteDATE_OF_BIRTH.Format = DateTimePickerFormat.Long;
                    this.dteDATE_OF_BIRTH.Value = data.DATE_OF_BIRTH.Value;
                    this.dteDATE_OF_BIRTH.Text = data.DATE_OF_BIRTH.Value.ToString("yyyy/MM/dd");
                }
                else
                {
                    this.dteDATE_OF_BIRTH.Value = DateTime.Now;
                    this.dteDATE_OF_BIRTH.Text = string.Empty;
                    this.dteDATE_OF_BIRTH.Format = DateTimePickerFormat.Custom;
                    this.dteDATE_OF_BIRTH.CustomFormat = " ";
                }

                this.txtLOGIN_PASSWORD.Text = data.LOGIN_PASSWORD.ConvertToString();
                this.txtSALES_LOGIN_PASSWORD.Text = data.SALES_LOGIN_PASSWORD.ConvertToString();
                this.chkProdMode.Checked = data.IS_USE_PROD.ConvertToString().Trim() == "Y";
                this.chkTestMode.Checked = data.IS_USE_TEST.ConvertToString().Trim() == "Y";
                switch (data.ROLE_NO.ConvertToString().Trim())
                {
                    case "10":
                        this.cboRole.SelectedIndex = this.cboRole.FindStringExact("申請人員");
                        break;
                    case "20":
                        this.cboRole.SelectedIndex = this.cboRole.FindStringExact("查核人員");
                        break;
                    case "30":
                        this.cboRole.SelectedIndex = this.cboRole.FindStringExact("審批人員");
                        break;
                    case "40":
                        this.cboRole.SelectedIndex = this.cboRole.FindStringExact("Admin");
                        break;
                    case "50":
                        this.cboRole.SelectedIndex = this.cboRole.FindStringExact("HK-Admin");
                        break;
                    default:
                        break;
                }

                this.txtINTERNAL_IB_CODE.Text = data.INTERNAL_IB_CODE.ConvertToString().Trim();
                this.txtPARENT_IB_CODE.Text = data.PARENT_SALES_NAME.ConvertToString();
                this.txtPARENT_IB_CODE.Tag = data.PARENT_IB_CODE.ConvertToString();

                this.txtPARENT_INTERNAL_IB_CODE.Text = data.PARENT_INTERNAL_SALES_NAME.ConvertToString();
                this.txtPARENT_INTERNAL_IB_CODE.Tag = data.PARENT_INTERNAL_IB_CODE.ConvertToString();
            }
            else
            {
                this.btnDel.Visible = false;
            }
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void btnSave_Click(object sender, EventArgs e)
        {
            if (this.DataValidation())
            {
                var dataSave = this.GetSaveData;

                if (this.EMP_ID.ConvertToString().Trim() == string.Empty)
                {
                    this.EMP_ID = base.Cust.AddNewEmp(dataSave).ConvertToString();
                    if (this.EMP_ID.ConvertToString().Trim() == string.Empty)
                    {
                        MessageBox.Show("儲存失敗");
                    }
                    else
                    {
                        MessageBox.Show("儲存成功");
                        this.BindInitData();
                        this.Close();
                    }
                }
                else
                {
                    //  修改的
                    if (base.Cust.UpdateEmp(dataSave, this.EMP_ID))
                    {
                        MessageBox.Show("儲存成功");
                        this.BindInitData();
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("儲存失敗");
                    }
                }
            }
        }

        private bool DataValidation()
        {
            var result = true;
            var msg = string.Empty;

            if (this.txtEmpCode.Text.Trim() == string.Empty)
            {
                msg += "無法取得「EMP CODE」，請聯絡系統管理者" + Environment.NewLine;
            }
            if (this.txtCNAME.Text.Trim() == string.Empty)
            {
                msg += "請輸入「中文姓名」" + Environment.NewLine;
            }
            //if (this.txtENAME.Text.Trim() == string.Empty)
            //{
            //    msg += "請輸入「英文姓名」" + Environment.NewLine;
            //}

            if (this.txtID_NUMBER.Text.Trim() == string.Empty)
            {
                msg += "請輸入「身分證號」" + Environment.NewLine;
            }
            if (this.drpSEX.Text.Trim() == string.Empty)
            {
                msg += "請選擇「性別」" + Environment.NewLine;
            }
            if (this.dteDATE_OF_BIRTH.Text.IsDate() == false)
            {
                msg += "請輸入「生日」" + Environment.NewLine;
            }
            //if (this.txtTITLE.Text.Trim() == string.Empty)
            //{
            //    msg += "請輸入「職稱」" + Environment.NewLine;
            //}

            //if (this.txtIB_CODE.Text.Trim() == string.Empty)
            //{
            //    msg += "請輸入「IB_CODE」" + Environment.NewLine;
            //}
            if (this.rdoTW1.Checked == false && this.rdoTW2.Checked == false && this.rdoHK1.Checked == false)
            {
                msg += "請選擇「ORG_CODE」" + Environment.NewLine;
            }

            if (this.txtEMAIL.Text.Trim() == string.Empty)
            {
                msg += "請輸入「EMAIL」" + Environment.NewLine;
            }


            if (this.txtINTERNAL_IB_CODE.Text.Trim() == string.Empty)
            {
                if (this.dteHIRE_START_DATE.Text.IsDate() == false)
                {
                    msg += "請輸入「到職日」" + Environment.NewLine;
                }
                else
                {
                    if (this.dteHIRE_END_DATE.Text.IsDate())
                    {
                        if (DateTime.Parse(this.dteHIRE_END_DATE.Value.ToString("yyyy/MM/dd")) < DateTime.Parse(this.dteHIRE_START_DATE.Value.ToString("yyyy/MM/dd")))
                        {
                            msg += "「到職日」不可小於「離職日」" + Environment.NewLine;
                        }
                    }
                }
                if (this.drpDEPT_VALUE.Text.Trim() == string.Empty)
                {
                    msg += "請選擇「部門」" + Environment.NewLine;
                }
            }
            else
            {
                if (this.txtPARENT_IB_CODE.Tag.ConvertToString().Trim() == string.Empty)
                {
                    msg += "請選擇「直屬業務」";
                }
            }

            if (msg != string.Empty)
            {
                result = false;
                MessageBox.Show(msg);
            }

            return result;
        }


        private BL_EMP GetSaveData
        {
            get
            {
                var data = new BL_EMP()
                {
                    EMP_CODE = this.txtEmpCode.Text.Trim(),
                    CNAME = this.txtCNAME.Text.Trim(),
                    ENAME = this.txtENAME.Text.Trim(),
                    HIRE_START_DATE = this.dteHIRE_START_DATE.Text.IsDate() ? DateTime.Parse(this.dteHIRE_START_DATE.Value.ToString("yyyy/MM/dd")) : (DateTime?)null,
                    HIRE_END_DATE = this.dteHIRE_END_DATE.Text.IsDate() ? DateTime.Parse(this.dteHIRE_END_DATE.Value.ToString("yyyy/MM/dd")) : (DateTime?)null,
                    ID_NUMBER = this.txtID_NUMBER.Text,
                    SEX = this.drpSEX.Text,
                    DATE_OF_BIRTH = this.dteDATE_OF_BIRTH.Text.IsDate() ? DateTime.Parse(this.dteDATE_OF_BIRTH.Value.ToString("yyyy/MM/dd")) : (DateTime?)null,
                    TITLE = this.txtTITLE.Text.Trim(),
                    SUPERVISOR_ID = this.txtSUPERVISOR_ID.Tag.ConvertToString().Trim(),
                    IB_CODE = this.txtIB_CODE.Text.Trim(),
                    EMAIL = this.txtEMAIL.Text.Trim(),
                    CREATED_BY = Global.str_UserName.ConvertToString(),
                    LAST_UPDATED_BY = Global.str_UserName.ConvertToString(),
                    LOGIN_PASSWORD = this.txtLOGIN_PASSWORD.Text.Trim(),
                    SALES_LOGIN_PASSWORD = this.txtSALES_LOGIN_PASSWORD.Text.Trim(),
                    IS_USE_PROD = this.chkProdMode.Checked ? "Y" : "N",
                    IS_USE_TEST = this.chkTestMode.Checked ? "Y" : "N",
                    ROLE_NO = string.Empty,
                    INTERNAL_IB_CODE = this.txtINTERNAL_IB_CODE.Text,
                    PARENT_IB_CODE = this.txtPARENT_IB_CODE.Tag.ConvertToString(),
                    PARENT_INTERNAL_IB_CODE = this.txtPARENT_INTERNAL_IB_CODE.Tag.ConvertToString()
                };

                if (this.rdoTW1.Checked)
                {
                    data.ORG_CODE = "TW1";
                }
                if (this.rdoTW2.Checked)
                {
                    data.ORG_CODE = "TW2";
                }
                if (this.rdoHK1.Checked)
                {
                    data.ORG_CODE = "HK1";
                }
                //switch (this.drpDEPT_VALUE.Text.Trim())
                //{
                //    case "行政管理部":
                //        data.DEPT_VALUE = "行政";
                //        break;
                //    case "V1":
                //        data.DEPT_VALUE = "V1";
                //        break;
                //    case "V2":
                //        data.DEPT_VALUE = "V2";
                //        break;
                //    case "V3":
                //        data.DEPT_VALUE = "V3";
                //        break;
                //    case "V5":
                //        data.DEPT_VALUE = "V5";
                //        break;
                //    case "法人關係部":
                //        data.DEPT_VALUE = "法人";
                //        break;
                //    case "通路部":
                //        data.DEPT_VALUE = "通路";
                //        break;
                //    default:
                //        data.DEPT_VALUE = string.Empty;
                //        break;
                //}
                data.DEPT_VALUE = this.drpDEPT_VALUE.SelectedValue.ConvertToString();

                switch (this.cboRole.Text.Trim())
                {
                    case "申請人員":
                        data.ROLE_NO = "10";
                        break;
                    case "查核人員":
                        data.ROLE_NO = "20";
                        break;
                    case "審批人員":
                        data.ROLE_NO = "30";
                        break;
                    case "Admin":
                        data.ROLE_NO = "40";
                        break;
                    case "HK-Admin":
                        data.ROLE_NO = "50";
                        break;
                    default:
                        break;
                }

                return data;
            }
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            if (this.EMP_ID.IsNumeric())
            {

                if (MessageBox.Show("請確定是否要刪除此筆資料?", "系統訊息", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    if (base.Cust.DeleteBL_EMPByEMP_ID(this.EMP_ID))
                    {
                        MessageBox.Show("刪除成功");
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("刪除失敗");
                    }
                }
            }
            else
            {
                MessageBox.Show("不符合刪除的條件");
            }
        }

        private void dte_ValueChanged(object sender, EventArgs e)
        {
            (sender as DateTimePicker).Format = DateTimePickerFormat.Long;
        }

        private void btnDelHIRE_START_DATE_Click(object sender, EventArgs e)
        {
            this.dteHIRE_START_DATE.Value = DateTime.Now;
            this.dteHIRE_START_DATE.Text = string.Empty;
            this.dteHIRE_START_DATE.Format = DateTimePickerFormat.Custom;
            this.dteHIRE_START_DATE.CustomFormat = " ";
        }

        private void btnDelHIRE_END_DATE_Click(object sender, EventArgs e)
        {
            this.dteHIRE_END_DATE.Value = DateTime.Now;
            this.dteHIRE_END_DATE.Text = string.Empty;
            this.dteHIRE_END_DATE.Format = DateTimePickerFormat.Custom;
            this.dteHIRE_END_DATE.CustomFormat = " ";
        }

        private void btnDelDATE_OF_BIRTH_Click(object sender, EventArgs e)
        {
            this.dteDATE_OF_BIRTH.Value = DateTime.Now;
            this.dteDATE_OF_BIRTH.Text = string.Empty;
            this.dteDATE_OF_BIRTH.Format = DateTimePickerFormat.Custom;
            this.dteDATE_OF_BIRTH.CustomFormat = " ";
        }

        private void btnOpenEmp_Click(object sender, EventArgs e)
        {
            if (this.rdoTW1.Checked == false && this.rdoTW2.Checked == false && this.rdoHK1.Checked == false)
            {
                MessageBox.Show("請先選擇ORG_CODE");
                return;
            }
            var strORG_CODE = string.Empty;
            if (this.rdoTW1.Checked) strORG_CODE = "TW1";
            if (this.rdoTW2.Checked) strORG_CODE = "TW2";
            if (this.rdoHK1.Checked) strORG_CODE = "HK1";
            var frm = new FormFunction.FrmSelectSales
                (
                    IsShowALL: true,
                    strORG_CODE: strORG_CODE
                )
            { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
            frm.ShowDialog();
            if (frm.status == enFormStatus.OK)
            {
                this.txtSUPERVISOR_ID.Text = frm.strCNAME.ConvertToString().Trim();
                this.txtSUPERVISOR_ID.Tag = frm.strEMP_ID.ConvertToString().Trim();
            }
            frm.Dispose();
            frm = null;
        }

        private void drpDEPT_VALUE_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void rdoTW1_CheckedChanged(object sender, EventArgs e)
        {
            this.SetDeptVal("TW1");
        }

        private void rdoTW2_CheckedChanged(object sender, EventArgs e)
        {
            this.SetDeptVal("TW2");
        }

        private void rdoHK1_CheckedChanged(object sender, EventArgs e)
        {
            this.SetDeptVal("HK1");
        }

        private void SetDeptVal(string strORG_CODE)
        {
            var dataDept = base.Cust.GetDeptCodeData(strORG_CODE);
            this.drpDEPT_VALUE.DataBindByList(dataDept, "DEPT_NAME", "DEPT_CODE", null); ;
        }

        private void btnOpenSales_Click(object sender, EventArgs e)
        {
            if (this.rdoTW1.Checked == false && this.rdoTW2.Checked == false && this.rdoHK1.Checked == false)
            {
                MessageBox.Show("請先選擇ORG_CODE");
                return;
            }
            var strORG_CODE = string.Empty;
            if (this.rdoTW1.Checked) strORG_CODE = "TW1";
            if (this.rdoTW2.Checked) strORG_CODE = "TW2";
            if (this.rdoHK1.Checked) strORG_CODE = "HK1";
            var frm = new FormFunction.FrmSelectSales
                (
                    IsShowALL: true,
                    strORG_CODE: strORG_CODE
                )
            { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
            frm.ShowDialog();
            if (frm.status == enFormStatus.OK)
            {
                this.txtPARENT_IB_CODE.Text = frm.strCNAME.ConvertToString().Trim();
                this.txtPARENT_IB_CODE.Tag = frm.strIB_CODE.ConvertToString().Trim();
            }
            frm.Dispose();
            frm = null;
        }

        private void btnOpenInternalParentSales_Click(object sender, EventArgs e)
        {
            if (this.rdoTW1.Checked == false && this.rdoTW2.Checked == false && this.rdoHK1.Checked == false)
            {
                MessageBox.Show("請先選擇ORG_CODE");
                return;
            }
            var strORG_CODE = string.Empty;
            if (this.rdoTW1.Checked) strORG_CODE = "TW1";
            if (this.rdoTW2.Checked) strORG_CODE = "TW2";
            if (this.rdoHK1.Checked) strORG_CODE = "HK1";
            
            var frm = new FormFunction.FrmSelectSales_Internal
                (
                    IsShowALL: true,
                    strORG_CODE: strORG_CODE
                )
            { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
            frm.ShowDialog();
            if (frm.status == enFormStatus.OK)
            {
                this.txtPARENT_INTERNAL_IB_CODE.Text = frm.strCNAME.ConvertToString().Trim();
                this.txtPARENT_INTERNAL_IB_CODE.Tag = frm.strIB_CODE.ConvertToString().Trim();
            }
            frm.Dispose();
            frm = null;
        }

        private void panAdmin_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
