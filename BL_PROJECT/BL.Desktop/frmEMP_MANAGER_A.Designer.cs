﻿namespace MK_DEMO.Destop
{
    partial class frmEMP_MANAGER_A
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEMP_MANAGER_A));
            this.drpDEPT_VALUE = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.btnOpenEmp = new System.Windows.Forms.Button();
            this.txtSUPERVISOR_ID = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtEMAIL = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.drpSEX = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtIB_CODE = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtTITLE = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btnDelDATE_OF_BIRTH = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.dteDATE_OF_BIRTH = new System.Windows.Forms.DateTimePicker();
            this.txtID_NUMBER = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnDelHIRE_END_DATE = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.dteHIRE_END_DATE = new System.Windows.Forms.DateTimePicker();
            this.btnDelHIRE_START_DATE = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.dteHIRE_START_DATE = new System.Windows.Forms.DateTimePicker();
            this.txtENAME = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCNAME = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtEmpCode = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnDel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.txtLOGIN_PASSWORD = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.chkProdMode = new System.Windows.Forms.CheckBox();
            this.chkTestMode = new System.Windows.Forms.CheckBox();
            this.cboRole = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.panAdmin = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.txtSALES_LOGIN_PASSWORD = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.rdoTW1 = new System.Windows.Forms.RadioButton();
            this.rdoTW2 = new System.Windows.Forms.RadioButton();
            this.rdoHK1 = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnOpenInternalParentSales = new System.Windows.Forms.Button();
            this.txtPARENT_INTERNAL_IB_CODE = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.btnOpenSales = new System.Windows.Forms.Button();
            this.txtINTERNAL_IB_CODE = new System.Windows.Forms.TextBox();
            this.txtPARENT_IB_CODE = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.panAdmin.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // drpDEPT_VALUE
            // 
            this.drpDEPT_VALUE.FormattingEnabled = true;
            this.drpDEPT_VALUE.Location = new System.Drawing.Point(413, 245);
            this.drpDEPT_VALUE.Name = "drpDEPT_VALUE";
            this.drpDEPT_VALUE.Size = new System.Drawing.Size(152, 27);
            this.drpDEPT_VALUE.TabIndex = 181;
            this.drpDEPT_VALUE.SelectedIndexChanged += new System.EventHandler(this.drpDEPT_VALUE_SelectedIndexChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(322, 249);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(39, 19);
            this.label13.TabIndex = 180;
            this.label13.Text = "部門";
            // 
            // btnOpenEmp
            // 
            this.btnOpenEmp.Location = new System.Drawing.Point(571, 177);
            this.btnOpenEmp.Name = "btnOpenEmp";
            this.btnOpenEmp.Size = new System.Drawing.Size(31, 28);
            this.btnOpenEmp.TabIndex = 179;
            this.btnOpenEmp.Text = "...";
            this.btnOpenEmp.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.btnOpenEmp.UseVisualStyleBackColor = true;
            this.btnOpenEmp.Click += new System.EventHandler(this.btnOpenEmp_Click);
            // 
            // txtSUPERVISOR_ID
            // 
            this.txtSUPERVISOR_ID.Enabled = false;
            this.txtSUPERVISOR_ID.Location = new System.Drawing.Point(413, 178);
            this.txtSUPERVISOR_ID.Name = "txtSUPERVISOR_ID";
            this.txtSUPERVISOR_ID.Size = new System.Drawing.Size(152, 27);
            this.txtSUPERVISOR_ID.TabIndex = 178;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(322, 182);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(69, 19);
            this.label12.TabIndex = 177;
            this.label12.Text = "直屬主管";
            // 
            // txtEMAIL
            // 
            this.txtEMAIL.Location = new System.Drawing.Point(105, 282);
            this.txtEMAIL.Name = "txtEMAIL";
            this.txtEMAIL.Size = new System.Drawing.Size(460, 27);
            this.txtEMAIL.TabIndex = 176;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(10, 286);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 19);
            this.label11.TabIndex = 175;
            this.label11.Text = "EMAIL";
            // 
            // drpSEX
            // 
            this.drpSEX.FormattingEnabled = true;
            this.drpSEX.Items.AddRange(new object[] {
            "男",
            "女"});
            this.drpSEX.Location = new System.Drawing.Point(413, 111);
            this.drpSEX.Name = "drpSEX";
            this.drpSEX.Size = new System.Drawing.Size(152, 27);
            this.drpSEX.TabIndex = 174;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(322, 115);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(39, 19);
            this.label10.TabIndex = 173;
            this.label10.Text = "性別";
            // 
            // txtIB_CODE
            // 
            this.txtIB_CODE.Location = new System.Drawing.Point(105, 210);
            this.txtIB_CODE.Name = "txtIB_CODE";
            this.txtIB_CODE.Size = new System.Drawing.Size(152, 27);
            this.txtIB_CODE.TabIndex = 172;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(10, 214);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(70, 19);
            this.label9.TabIndex = 171;
            this.label9.Text = "IB_CODE";
            // 
            // txtTITLE
            // 
            this.txtTITLE.Location = new System.Drawing.Point(105, 177);
            this.txtTITLE.Name = "txtTITLE";
            this.txtTITLE.Size = new System.Drawing.Size(152, 27);
            this.txtTITLE.TabIndex = 170;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(10, 181);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(39, 19);
            this.label8.TabIndex = 169;
            this.label8.Text = "職稱";
            // 
            // btnDelDATE_OF_BIRTH
            // 
            this.btnDelDATE_OF_BIRTH.Image = ((System.Drawing.Image)(resources.GetObject("btnDelDATE_OF_BIRTH.Image")));
            this.btnDelDATE_OF_BIRTH.Location = new System.Drawing.Point(263, 143);
            this.btnDelDATE_OF_BIRTH.Name = "btnDelDATE_OF_BIRTH";
            this.btnDelDATE_OF_BIRTH.Size = new System.Drawing.Size(31, 28);
            this.btnDelDATE_OF_BIRTH.TabIndex = 168;
            this.btnDelDATE_OF_BIRTH.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.btnDelDATE_OF_BIRTH.UseVisualStyleBackColor = true;
            this.btnDelDATE_OF_BIRTH.Click += new System.EventHandler(this.btnDelDATE_OF_BIRTH_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 147);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(39, 19);
            this.label7.TabIndex = 166;
            this.label7.Text = "生日";
            // 
            // dteDATE_OF_BIRTH
            // 
            this.dteDATE_OF_BIRTH.Location = new System.Drawing.Point(105, 144);
            this.dteDATE_OF_BIRTH.Name = "dteDATE_OF_BIRTH";
            this.dteDATE_OF_BIRTH.Size = new System.Drawing.Size(152, 27);
            this.dteDATE_OF_BIRTH.TabIndex = 167;
            this.dteDATE_OF_BIRTH.ValueChanged += new System.EventHandler(this.dte_ValueChanged);
            // 
            // txtID_NUMBER
            // 
            this.txtID_NUMBER.Location = new System.Drawing.Point(105, 111);
            this.txtID_NUMBER.Name = "txtID_NUMBER";
            this.txtID_NUMBER.Size = new System.Drawing.Size(152, 27);
            this.txtID_NUMBER.TabIndex = 165;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 115);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 19);
            this.label6.TabIndex = 164;
            this.label6.Text = "身分證號";
            // 
            // btnDelHIRE_END_DATE
            // 
            this.btnDelHIRE_END_DATE.Image = ((System.Drawing.Image)(resources.GetObject("btnDelHIRE_END_DATE.Image")));
            this.btnDelHIRE_END_DATE.Location = new System.Drawing.Point(571, 77);
            this.btnDelHIRE_END_DATE.Name = "btnDelHIRE_END_DATE";
            this.btnDelHIRE_END_DATE.Size = new System.Drawing.Size(31, 28);
            this.btnDelHIRE_END_DATE.TabIndex = 163;
            this.btnDelHIRE_END_DATE.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.btnDelHIRE_END_DATE.UseVisualStyleBackColor = true;
            this.btnDelHIRE_END_DATE.Click += new System.EventHandler(this.btnDelHIRE_END_DATE_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(324, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 19);
            this.label4.TabIndex = 161;
            this.label4.Text = "離職日";
            // 
            // dteHIRE_END_DATE
            // 
            this.dteHIRE_END_DATE.Location = new System.Drawing.Point(413, 78);
            this.dteHIRE_END_DATE.Name = "dteHIRE_END_DATE";
            this.dteHIRE_END_DATE.Size = new System.Drawing.Size(152, 27);
            this.dteHIRE_END_DATE.TabIndex = 162;
            this.dteHIRE_END_DATE.ValueChanged += new System.EventHandler(this.dte_ValueChanged);
            // 
            // btnDelHIRE_START_DATE
            // 
            this.btnDelHIRE_START_DATE.Image = ((System.Drawing.Image)(resources.GetObject("btnDelHIRE_START_DATE.Image")));
            this.btnDelHIRE_START_DATE.Location = new System.Drawing.Point(263, 77);
            this.btnDelHIRE_START_DATE.Name = "btnDelHIRE_START_DATE";
            this.btnDelHIRE_START_DATE.Size = new System.Drawing.Size(31, 28);
            this.btnDelHIRE_START_DATE.TabIndex = 160;
            this.btnDelHIRE_START_DATE.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.btnDelHIRE_START_DATE.UseVisualStyleBackColor = true;
            this.btnDelHIRE_START_DATE.Click += new System.EventHandler(this.btnDelHIRE_START_DATE_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 19);
            this.label3.TabIndex = 158;
            this.label3.Text = "到職日";
            // 
            // dteHIRE_START_DATE
            // 
            this.dteHIRE_START_DATE.Location = new System.Drawing.Point(105, 78);
            this.dteHIRE_START_DATE.Name = "dteHIRE_START_DATE";
            this.dteHIRE_START_DATE.Size = new System.Drawing.Size(152, 27);
            this.dteHIRE_START_DATE.TabIndex = 159;
            this.dteHIRE_START_DATE.ValueChanged += new System.EventHandler(this.dte_ValueChanged);
            // 
            // txtENAME
            // 
            this.txtENAME.Location = new System.Drawing.Point(121, 16);
            this.txtENAME.Name = "txtENAME";
            this.txtENAME.Size = new System.Drawing.Size(123, 27);
            this.txtENAME.TabIndex = 157;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(109, 38);
            this.label2.TabIndex = 156;
            this.label2.Text = "英文姓名\r\n(系統登入帳號)";
            // 
            // txtCNAME
            // 
            this.txtCNAME.Location = new System.Drawing.Point(105, 45);
            this.txtCNAME.Name = "txtCNAME";
            this.txtCNAME.Size = new System.Drawing.Size(152, 27);
            this.txtCNAME.TabIndex = 155;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 19);
            this.label1.TabIndex = 154;
            this.label1.Text = "中文姓名";
            // 
            // txtEmpCode
            // 
            this.txtEmpCode.Enabled = false;
            this.txtEmpCode.Location = new System.Drawing.Point(105, 12);
            this.txtEmpCode.Name = "txtEmpCode";
            this.txtEmpCode.Size = new System.Drawing.Size(152, 27);
            this.txtEmpCode.TabIndex = 153;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 19);
            this.label5.TabIndex = 152;
            this.label5.Text = "EMP CODE";
            // 
            // btnDel
            // 
            this.btnDel.Location = new System.Drawing.Point(14, 569);
            this.btnDel.Name = "btnDel";
            this.btnDel.Size = new System.Drawing.Size(180, 36);
            this.btnDel.TabIndex = 147;
            this.btnDel.Text = "刪除";
            this.btnDel.UseVisualStyleBackColor = true;
            this.btnDel.Click += new System.EventHandler(this.btnDel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(241, 569);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(180, 36);
            this.btnSave.TabIndex = 146;
            this.btnSave.Text = "儲存";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(468, 569);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(180, 36);
            this.btnClose.TabIndex = 145;
            this.btnClose.Text = "關閉";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // txtLOGIN_PASSWORD
            // 
            this.txtLOGIN_PASSWORD.Location = new System.Drawing.Point(429, 16);
            this.txtLOGIN_PASSWORD.Name = "txtLOGIN_PASSWORD";
            this.txtLOGIN_PASSWORD.Size = new System.Drawing.Size(123, 27);
            this.txtLOGIN_PASSWORD.TabIndex = 184;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(311, 20);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(99, 19);
            this.label14.TabIndex = 183;
            this.label14.Text = "系統登入密碼";
            // 
            // chkProdMode
            // 
            this.chkProdMode.AutoSize = true;
            this.chkProdMode.Location = new System.Drawing.Point(92, 49);
            this.chkProdMode.Name = "chkProdMode";
            this.chkProdMode.Size = new System.Drawing.Size(205, 23);
            this.chkProdMode.TabIndex = 185;
            this.chkProdMode.Text = "使用正式環境(Production)";
            this.chkProdMode.UseVisualStyleBackColor = true;
            // 
            // chkTestMode
            // 
            this.chkTestMode.AutoSize = true;
            this.chkTestMode.Location = new System.Drawing.Point(313, 49);
            this.chkTestMode.Name = "chkTestMode";
            this.chkTestMode.Size = new System.Drawing.Size(179, 23);
            this.chkTestMode.TabIndex = 186;
            this.chkTestMode.Text = "使用測試環境(Testing)";
            this.chkTestMode.UseVisualStyleBackColor = true;
            // 
            // cboRole
            // 
            this.cboRole.FormattingEnabled = true;
            this.cboRole.Location = new System.Drawing.Point(92, 72);
            this.cboRole.Name = "cboRole";
            this.cboRole.Size = new System.Drawing.Size(152, 27);
            this.cboRole.TabIndex = 188;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(3, 76);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(39, 19);
            this.label15.TabIndex = 187;
            this.label15.Text = "角色";
            // 
            // panAdmin
            // 
            this.panAdmin.BackColor = System.Drawing.Color.LightPink;
            this.panAdmin.Controls.Add(this.label16);
            this.panAdmin.Controls.Add(this.txtSALES_LOGIN_PASSWORD);
            this.panAdmin.Controls.Add(this.label2);
            this.panAdmin.Controls.Add(this.cboRole);
            this.panAdmin.Controls.Add(this.txtENAME);
            this.panAdmin.Controls.Add(this.label15);
            this.panAdmin.Controls.Add(this.label14);
            this.panAdmin.Controls.Add(this.chkTestMode);
            this.panAdmin.Controls.Add(this.txtLOGIN_PASSWORD);
            this.panAdmin.Controls.Add(this.chkProdMode);
            this.panAdmin.Location = new System.Drawing.Point(12, 456);
            this.panAdmin.Name = "panAdmin";
            this.panAdmin.Size = new System.Drawing.Size(636, 106);
            this.panAdmin.TabIndex = 189;
            this.panAdmin.Paint += new System.Windows.Forms.PaintEventHandler(this.panAdmin_Paint);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(311, 77);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(111, 19);
            this.label16.TabIndex = 189;
            this.label16.Text = "業務密碼(Web)";
            // 
            // txtSALES_LOGIN_PASSWORD
            // 
            this.txtSALES_LOGIN_PASSWORD.Location = new System.Drawing.Point(429, 73);
            this.txtSALES_LOGIN_PASSWORD.Name = "txtSALES_LOGIN_PASSWORD";
            this.txtSALES_LOGIN_PASSWORD.Size = new System.Drawing.Size(123, 27);
            this.txtSALES_LOGIN_PASSWORD.TabIndex = 190;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(10, 250);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(90, 19);
            this.label17.TabIndex = 190;
            this.label17.Text = "ORG_CODE";
            // 
            // rdoTW1
            // 
            this.rdoTW1.AutoSize = true;
            this.rdoTW1.Location = new System.Drawing.Point(3, 5);
            this.rdoTW1.Name = "rdoTW1";
            this.rdoTW1.Size = new System.Drawing.Size(59, 23);
            this.rdoTW1.TabIndex = 191;
            this.rdoTW1.TabStop = true;
            this.rdoTW1.Text = "TW1";
            this.rdoTW1.UseVisualStyleBackColor = true;
            this.rdoTW1.CheckedChanged += new System.EventHandler(this.rdoTW1_CheckedChanged);
            // 
            // rdoTW2
            // 
            this.rdoTW2.AutoSize = true;
            this.rdoTW2.Location = new System.Drawing.Point(76, 5);
            this.rdoTW2.Name = "rdoTW2";
            this.rdoTW2.Size = new System.Drawing.Size(59, 23);
            this.rdoTW2.TabIndex = 192;
            this.rdoTW2.TabStop = true;
            this.rdoTW2.Text = "TW2";
            this.rdoTW2.UseVisualStyleBackColor = true;
            this.rdoTW2.CheckedChanged += new System.EventHandler(this.rdoTW2_CheckedChanged);
            // 
            // rdoHK1
            // 
            this.rdoHK1.AutoSize = true;
            this.rdoHK1.Location = new System.Drawing.Point(149, 5);
            this.rdoHK1.Name = "rdoHK1";
            this.rdoHK1.Size = new System.Drawing.Size(56, 23);
            this.rdoHK1.TabIndex = 193;
            this.rdoHK1.TabStop = true;
            this.rdoHK1.Text = "HK1";
            this.rdoHK1.UseVisualStyleBackColor = true;
            this.rdoHK1.CheckedChanged += new System.EventHandler(this.rdoHK1_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.panel1.Controls.Add(this.rdoTW1);
            this.panel1.Controls.Add(this.rdoHK1);
            this.panel1.Controls.Add(this.rdoTW2);
            this.panel1.Location = new System.Drawing.Point(105, 242);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(208, 34);
            this.panel1.TabIndex = 194;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.LightBlue;
            this.groupBox1.Controls.Add(this.btnOpenInternalParentSales);
            this.groupBox1.Controls.Add(this.txtPARENT_INTERNAL_IB_CODE);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.btnOpenSales);
            this.groupBox1.Controls.Add(this.txtINTERNAL_IB_CODE);
            this.groupBox1.Controls.Add(this.txtPARENT_IB_CODE);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Location = new System.Drawing.Point(12, 317);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(636, 133);
            this.groupBox1.TabIndex = 195;
            this.groupBox1.TabStop = false;
            // 
            // btnOpenInternalParentSales
            // 
            this.btnOpenInternalParentSales.Location = new System.Drawing.Point(268, 83);
            this.btnOpenInternalParentSales.Name = "btnOpenInternalParentSales";
            this.btnOpenInternalParentSales.Size = new System.Drawing.Size(31, 28);
            this.btnOpenInternalParentSales.TabIndex = 201;
            this.btnOpenInternalParentSales.Text = "...";
            this.btnOpenInternalParentSales.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.btnOpenInternalParentSales.UseVisualStyleBackColor = true;
            this.btnOpenInternalParentSales.Click += new System.EventHandler(this.btnOpenInternalParentSales_Click);
            // 
            // txtPARENT_INTERNAL_IB_CODE
            // 
            this.txtPARENT_INTERNAL_IB_CODE.Enabled = false;
            this.txtPARENT_INTERNAL_IB_CODE.Location = new System.Drawing.Point(104, 84);
            this.txtPARENT_INTERNAL_IB_CODE.Name = "txtPARENT_INTERNAL_IB_CODE";
            this.txtPARENT_INTERNAL_IB_CODE.Size = new System.Drawing.Size(158, 27);
            this.txtPARENT_INTERNAL_IB_CODE.TabIndex = 200;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(7, 78);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(89, 38);
            this.label20.TabIndex = 199;
            this.label20.Text = "外圍主管\r\n(IB_CODE2)";
            // 
            // btnOpenSales
            // 
            this.btnOpenSales.Location = new System.Drawing.Point(558, 31);
            this.btnOpenSales.Name = "btnOpenSales";
            this.btnOpenSales.Size = new System.Drawing.Size(31, 28);
            this.btnOpenSales.TabIndex = 198;
            this.btnOpenSales.Text = "...";
            this.btnOpenSales.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.btnOpenSales.UseVisualStyleBackColor = true;
            this.btnOpenSales.Click += new System.EventHandler(this.btnOpenSales_Click);
            // 
            // txtINTERNAL_IB_CODE
            // 
            this.txtINTERNAL_IB_CODE.Location = new System.Drawing.Point(104, 32);
            this.txtINTERNAL_IB_CODE.Name = "txtINTERNAL_IB_CODE";
            this.txtINTERNAL_IB_CODE.Size = new System.Drawing.Size(158, 27);
            this.txtINTERNAL_IB_CODE.TabIndex = 197;
            // 
            // txtPARENT_IB_CODE
            // 
            this.txtPARENT_IB_CODE.Enabled = false;
            this.txtPARENT_IB_CODE.Location = new System.Drawing.Point(400, 32);
            this.txtPARENT_IB_CODE.Name = "txtPARENT_IB_CODE";
            this.txtPARENT_IB_CODE.Size = new System.Drawing.Size(152, 27);
            this.txtPARENT_IB_CODE.TabIndex = 197;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(318, 26);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(80, 38);
            this.label19.TabIndex = 196;
            this.label19.Text = "直屬業務\r\n(IB_CODE)";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(7, 26);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(89, 38);
            this.label18.TabIndex = 196;
            this.label18.Text = "外圍業務\r\n(IB_CODE2)";
            // 
            // frmEMP_MANAGER_A
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.ClientSize = new System.Drawing.Size(660, 661);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.panAdmin);
            this.Controls.Add(this.drpDEPT_VALUE);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.btnOpenEmp);
            this.Controls.Add(this.txtSUPERVISOR_ID);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtEMAIL);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.drpSEX);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtIB_CODE);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtTITLE);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.btnDelDATE_OF_BIRTH);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.dteDATE_OF_BIRTH);
            this.Controls.Add(this.txtID_NUMBER);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnDelHIRE_END_DATE);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.dteHIRE_END_DATE);
            this.Controls.Add(this.btnDelHIRE_START_DATE);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dteHIRE_START_DATE);
            this.Controls.Add(this.txtCNAME);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtEmpCode);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnDel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnClose);
            this.Font = new System.Drawing.Font("微軟正黑體", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Name = "frmEMP_MANAGER_A";
            this.Text = "員工資料-編輯";
            this.Load += new System.EventHandler(this.frmEMP_MANAGER_A_Load);
            this.panAdmin.ResumeLayout(false);
            this.panAdmin.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnDel;
        private System.Windows.Forms.TextBox txtEmpCode;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtCNAME;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtENAME;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnDelHIRE_START_DATE;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dteHIRE_START_DATE;
        private System.Windows.Forms.Button btnDelHIRE_END_DATE;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dteHIRE_END_DATE;
        private System.Windows.Forms.TextBox txtID_NUMBER;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnDelDATE_OF_BIRTH;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker dteDATE_OF_BIRTH;
        private System.Windows.Forms.TextBox txtTITLE;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtIB_CODE;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox drpSEX;
        private System.Windows.Forms.TextBox txtEMAIL;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtSUPERVISOR_ID;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnOpenEmp;
        private System.Windows.Forms.ComboBox drpDEPT_VALUE;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtLOGIN_PASSWORD;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.CheckBox chkProdMode;
        private System.Windows.Forms.CheckBox chkTestMode;
        private System.Windows.Forms.ComboBox cboRole;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel panAdmin;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtSALES_LOGIN_PASSWORD;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.RadioButton rdoTW1;
        private System.Windows.Forms.RadioButton rdoTW2;
        private System.Windows.Forms.RadioButton rdoHK1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtINTERNAL_IB_CODE;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button btnOpenSales;
        private System.Windows.Forms.TextBox txtPARENT_IB_CODE;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button btnOpenInternalParentSales;
        private System.Windows.Forms.TextBox txtPARENT_INTERNAL_IB_CODE;
        private System.Windows.Forms.Label label20;
    }
}
