﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MK.Demo.Logic;
using System.Net;
using System.Net.Mail;
using MK.Demo.Utility;

namespace MK_DEMO.Destop
{

    public partial class BaseForm : Form
    {

        private const int CP_NOCLOSE_BUTTON = 0x200;

        private logicCust _Cust;
        public logicCust Cust
        {
            get
            {
                return this._Cust ?? (this._Cust = new logicCust(Global.IsTestMode));
            }
        }



        private EMP_MANAGEMENT _Emp;
        public EMP_MANAGEMENT Emp
        {
            get
            {
                return this._Emp ?? (this._Emp = new EMP_MANAGEMENT(Global.IsTestMode));
            }
        }

        public BaseForm()
        {
            InitializeComponent();
        }

        protected virtual bool IsHiddenCloseButton()
        {
            return false;
        }
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                if (IsHiddenCloseButton() == false)
                    myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }

        public static string GetConfigValueByKey(string strKey)
        {
            return System.Configuration.ConfigurationManager.AppSettings[strKey].ConvertToString().Trim();
        }

        public static bool SendMail
            (
                List<string> MailTo,
                string MailSubject,
                string MailBody,
                string strFileName = "",
                bool IsShowError = true,
                string strBCC = "",
                string strCC = ""
            )
        {
            var strFileNames = new List<string>();
            if (strFileName.Trim() != string.Empty)
            {
                strFileNames.Add(strFileName);
            }

            return SendMail(MailTo, MailSubject, MailBody, strFileNames, IsShowError, strBCC, strCC);
        }


        public static bool SendMail
            (
                List<string> MailTo,
                string MailSubject,
                string MailBody,
                List<string> strFileNames,
                bool IsShowError = true,
                string strBCC = "",
                string strCC = ""
            )
        {
            try
            {
                MailMessage msg = new MailMessage();
                msg.To.Add(string.Join(",", MailTo.ToArray()));
                //   msg.CC.Add(string.Join(",", GetConfigValueByKey("MailSystemAdmin").Split(new string[] { ";" }, StringSplitOptions.None)));
                msg.Bcc.Add(string.Join(",", GetConfigValueByKey("MailBCC").Split(new string[] { ";" }, StringSplitOptions.None)));
                //  Todo > 2019/04/29 > 原本就有固定CC對象, 是不是直接把那個參數放到BCC就好 ? 
                if (strBCC != string.Empty)
                {
                    //msg.Bcc.Add(strBCC);
                    strBCC.Split(new string[] { ";" }, StringSplitOptions.None).ToList()
                        .ForEach(x =>
                        {
                            msg.Bcc.Add(x);
                        });
                }
                if (strCC != string.Empty)
                {
                    strCC.Split(new string[] { ";" }, StringSplitOptions.None).ToList()
                        .ForEach(x =>
                        {
                            msg.CC.Add(x);
                        });
                }
                msg.From = new MailAddress(
                    GetConfigValueByKey("MailFromAddress"),
                    "百麗客服部",
                    System.Text.Encoding.UTF8);
                msg.Subject = MailSubject;
                msg.SubjectEncoding = System.Text.Encoding.UTF8;
                msg.Body = "<div style='font-family:微軟正黑體;'>" + MailBody + "</div>";
                msg.IsBodyHtml = true;
                msg.BodyEncoding = System.Text.Encoding.UTF8;
                msg.Priority = MailPriority.Normal;


                strFileNames.ForEach(x =>
                {
                    if (x.ConvertToString().Trim() != string.Empty)
                    {
                        Attachment data = new Attachment(x);
                        msg.Attachments.Add(data);
                    }
                });

               SmtpClient mySmtp = new SmtpClient("124.9.9.173", 25);  //for 家裡測試用@20200506
                  
               // SmtpClient mySmtp = new SmtpClient("192.168.0.99", 25);  //for 企業用

               // SmtpClient mySmtp = new SmtpClient("168.95.4.150", 25);


                mySmtp.Send(msg);

                //this.txtFromAddress.Text = "customerservice@bestleader-service.com";
                //this.txtSMTP_HOST.Text = "124.9.9.173";
                //this.txtSMTP_PORT.Text = "25";
                ////this.txtSMTP_HOST.Text = "relay-hosting.secureserver.net";
                ////this.txtSMTP_PORT.Text = "25";
                //this.txtAccount.Text = "customerservice@bestleader-service.com";
                //this.txtPassword.Text = "BestLeader@@8";


                //this.txtToAddress.Text = "rosa7650@gmail.com";B

                //this.txtSubject.Text = "System Test";
                //this.txtBody.Text = "Testing...";

                //mySmtp.Send(msg);
                return true;

            }
            catch (Exception ex)
            {
                if (IsShowError)
                {
                    MessageBox.Show(ex.Message.ToString());
                }
                //new Pub.ErrorPage().WriteErrorLog(ex);
                return false;
            }
        }
    }

    public static class Global
    {
        private static string _str_UserName = "";
        private static bool _IsTestMode = false;
        private static string _Role = "";
        private static long _BL_BACKEND_LOGIN_LOG_SN = -1;

        public static string str_UserName
        {
            get { return _str_UserName; }
            set { _str_UserName = value; }
        }
        public static bool IsTestMode
        {
            get { return _IsTestMode; }
            set { _IsTestMode = value; }
        }
        public static string UserRole
        {
            get { return _Role; }
            set { _Role = value; }
        }

        public static long BL_BACKEND_LOGIN_LOG_SN
        {
            get { return _BL_BACKEND_LOGIN_LOG_SN; }
            set { _BL_BACKEND_LOGIN_LOG_SN = value; }
        }

        public static string ORG_CODE { get; set; }
        //public static string ORG_CODE
        //{
        //    get
        //    {
        //        var strORG = string.Empty;

        //        switch (str_UserName.ConvertToString())
        //        {
        //            case "ADMIN":
        //                strORG = "TW1";
        //                break;
        //            case "AMYTW2":
        //                strORG = "TW2";
        //                break;
        //            case "AMYHK":
        //                strORG = "HK1";
        //                break;
        //            default:
        //                switch (UserRole.ConvertToString())
        //                {
        //                    case "20":
        //                        //  行政查核人員
        //                        strORG = "TW1";
        //                        break;
        //                    default:
        //                        break;
        //                }
        //                break;
        //        }

        //        return strORG;
        //    }

        //}

    }
}
