﻿using MK.Demo.Logic;
using MK.Demo.Model;
using MK_DEMO.Destop.FormFunction;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MK.Demo.Utility;

namespace MK_DEMO.Destop
{
    public partial class Main : BaseForm
    {
        #region Var and Property

        private Form _Parent;

        private enum enCurrentMenu
        {
            穩健型商品客戶,
            出金作業,
            金流表異動作業,
            報表統計,
            合約查詢,
            合約審核
        }

        private enCurrentMenu _CurrentMenu;
        private enCurrentMenu GetCurrentMenu
        {
            get
            {
                return this._CurrentMenu;
            }
            set
            {
                this._CurrentMenu = value;


                this.toolScriptCust.BackColor = SystemColors.Control;
                this.toolScriptDeposit.BackColor = SystemColors.Control;
                this.toolScriptManager.BackColor = SystemColors.Control;
                this.toolScriptReport.BackColor = SystemColors.Control;
                this.toolScriptContract.BackColor = SystemColors.Control;
                this.toolStripContractApprove.BackColor = SystemColors.Control;

                switch (this._CurrentMenu)
                {
                    case enCurrentMenu.穩健型商品客戶:
                        this.toolScriptCust.BackColor = Color.Pink;
                        this.BindCustData();
                        break;
                    case enCurrentMenu.出金作業:
                        this.toolScriptDeposit.BackColor = Color.Pink;
                        var frm = new frmDepositOut() { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
                        frm.ShowDialog();
                        frm.Dispose();
                        frm = null;
                        break;
                    case enCurrentMenu.金流表異動作業:
                        this.toolScriptManager.BackColor = Color.Pink;
                        break;
                    //case enCurrentMenu.設定管理_系統設定:
                    //    this.toolScriptManager.BackColor = Color.Pink;
                    //    break;
                    case enCurrentMenu.報表統計:
                        this.toolScriptReport.BackColor = Color.Pink;
                        break;
                    case enCurrentMenu.合約查詢:
                        this.toolScriptContract.BackColor = Color.Pink;
                        var frm_Control_Search = new frmContractSearch() { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
                        frm_Control_Search.ShowDialog();
                        frm_Control_Search.Dispose();
                        frm_Control_Search = null;
                        break;
                    case enCurrentMenu.合約審核:
                        this.toolStripContractApprove.BackColor = Color.Pink;

                        var frm_Contract_Approve = new frmContractApprove("TWN") { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
                        frm_Contract_Approve.ShowDialog();
                        frm_Contract_Approve.Dispose();
                        frm_Contract_Approve = null;

                        this.BindCustData();
                        break;
                    default:
                        break;
                }
            }
        }

        private QueryCust QueryCondition;

        #endregion

        #region Form Event

        public Main(Form formParent)
        {
            InitializeComponent();

            this._Parent = formParent;
            this.QueryCondition = new QueryCust();

            this.dgv_header.AutoGenerateColumns = false;
            this.dgv_header.MultiSelect = false;
            this.dgv_header.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            this.lblTestMode.Visible = Global.IsTestMode;
        }


        private void Main_Load(object sender, EventArgs e)
        {
            //  Default Show 客戶頁籤
            this.GetCurrentMenu = enCurrentMenu.穩健型商品客戶;
            var strORG = Global.ORG_CODE;
            if (strORG != string.Empty) strORG = " (" + strORG + ") ";
            //  var test = Global.str_UserName;
            this.lb_login_user.Text = "HI ~ " + Global.str_UserName + strORG + " 歡迎登入系統";

            switch (Global.str_UserName)
            {
                case "HK_ADMIN":
                case "MAGGIELIU":
                case "MONIQUECHOY":
                case "OLIVIAHUNG":
                    this.toolScriptDeposit.Visible = false;
                    this.toolStripContractApprove.Visible = false;
                    this.toolStripButton1.Visible = true;
                    this.toolStripButton2.Visible = true;
                    this.toolScriptManager.Visible = false;
                    //this.dgv_header.Columns["ViewDetail"].Visible = false;
                    this.dgv_header.Columns["AddContract"].Visible = false;
                    btnAddNewContract.Visible = false;
                    this.toolStripButton3.Visible = false;
                    break;
                default:
                    this.toolScriptDeposit.Visible = true;
                    this.toolStripContractApprove.Visible = true;
                    this.toolStripButton1.Visible = false;
                    this.toolStripButton2.Visible = false;
                    toolScriptManager.Visible = true;
                    btnAddNewContract.Visible = true;
                    this.toolStripButton3.Visible = false;
                    break;
            }
            if (Global.UserRole.ConvertToString() == "50")
            {
                //  HK Admin
                this.toolScriptDeposit.Visible = false;
                this.toolStripContractApprove.Visible = false;
                this.toolStripButton1.Visible = true;
                this.toolStripButton2.Visible = true;
                toolScriptManager.Visible = false;
                //this.dgv_header.Columns["ViewDetail"].Visible = false;
                this.dgv_header.Columns["AddContract"].Visible = false;
                btnAddNewContract.Visible = false;
                this.toolStripButton3.Visible = false;
            }

            if
                (
                    Global.str_UserName == "HK_ADMIN" ||
                    Global.str_UserName == "MAGGIELIU" ||
                    Global.str_UserName == "MONIQUECHOY" ||
                    Global.str_UserName == "OLIVIAHUNG"
                )
            {
                //  2020/04/03 > HK_ADMIN 可以編輯員工資料
                this.toolScriptManager.Visible = true;
                for (int i = 0; i < this.toolScriptManager.DropDownItems.Count; i++)
                {
                    this.toolScriptManager.DropDownItems[i].Visible = false;
                }
                this.ToolStripEmpManager.Visible = true;
            }

            var dataSystemVersion = base.Cust.GetSystemVersionData();
            this.pictureBox1.Visible = dataSystemVersion.Count() > 0;
            if (dataSystemVersion.Count() > 0)
            {
                var MaxMSG_SN = dataSystemVersion.Max(x => x.BL_MSG_DATA_SN);
                var dataMsgChk = base.Cust.GetMSG_CHECK_DATA(Global.str_UserName, MaxMSG_SN);
                if (dataMsgChk.Count() == 0)
                {
                    var frm = new frmSystemVersionInfo() { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
                    frm.ShowDialog();
                    frm.Dispose();
                    frm = null;
                }
            }

            if
                (
                    Global.str_UserName.ToUpper() == "ADMIN" ||
                    Global.UserRole.ConvertToString() == "40"   //  Admin
                )
            {
                this.btnTransContractToNewSales.Visible = true;
            }
            else
            {
                this.btnTransContractToNewSales.Visible = false;
            }
        }

        private void Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (this._Parent != null)
            {
                this._Parent.Close();
            }
        }


        protected override bool IsHiddenCloseButton()
        {
            return true;
        }

        #endregion


        #region Tool Menu Event


        
        /// <summary>
        /// 穩健型商品客戶
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolScriptCust_Click(object sender, EventArgs e)
        {
            this.GetCurrentMenu = enCurrentMenu.穩健型商品客戶;
        }

        /// <summary>
        /// 出金作業
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolScriptDeposit_Click(object sender, EventArgs e)
        {
            this.GetCurrentMenu = enCurrentMenu.出金作業;
        }

    
        private void toolScriptEmpManager_Click(object sender, EventArgs e)
        {
            if 
                (
                    Global.str_UserName == "ADMIN" ||
                    Global.UserRole.ConvertToString() == "10" ||
                    Global.UserRole.ConvertToString() == "40"
                )
            {
                this.GetCurrentMenu = enCurrentMenu.金流表異動作業;

                var frm = new frmAdminSentTxn()
                { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
                frm.ShowDialog();
            }
            else
            {
                MessageBox.Show("管理員才可進入"); 

            } 


        }

        /// <summary>
        /// 系統設定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolScriptSystemSetting_Click(object sender, EventArgs e)
        {
            //this.GetCurrentMenu = enCurrentMenu.設定管理_系統設定;
        }

        #endregion


        #region Footer Control Event



        /// <summary>
        /// 查詢
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnQuery_Click(object sender, EventArgs e)
        {
            var frm = new frmSearchCustCondition(this.QueryCondition) { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
            frm.ShowDialog();
            if (frm.status == MK.Demo.Data.enCommon.enFormStatus.OK)
            {
                this.BindCustData(frm.QueryCondition);
            }
            frm.Dispose();
            frm = null;
        }

        private void btnAddNewContract_Click(object sender, EventArgs e)
        {
            var frm = new frmAddContract(string.Empty)
            { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
            frm.ShowDialog();
            frm.Dispose();
            frm = null;
            this.BindCustData();
        }

        /// <summary>
        /// 列印
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnReport_Click(object sender, EventArgs e)
        {

        }

        #endregion


        #region Function

        private void BindContractData()
        {

        }

        private void BindCustData(QueryCust _Condition = null)
        {
            this.picLoder.Visible = true;
            this.picLoder.BringToFront();
            if (this.backgroundWorker1.IsBusy == false)
            {
                this.backgroundWorker1.RunWorkerAsync(_Condition);
            }
        }


        #endregion

        #region Main Grid Event
        private void dgv_header_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                //Parallel.ForEach(this.dgv_header.Rows.Cast<DataGridViewRow>(), x =>
                //{
                //    x.Cells["check"].Value = false;
                //});
                //this.dgv_header.Rows[e.RowIndex].Cells["check"].Value = true;
                if ((bool?)this.dgv_header.Rows[e.RowIndex].Cells["check"].Value == true)
                {
                    this.dgv_header.Rows[e.RowIndex].Cells["check"].Value = false;
                }
                else
                {
                    this.dgv_header.Rows[e.RowIndex].Cells["check"].Value = true;
                }

                var strCustID = this.dgv_header.Rows[e.RowIndex].Cells["CUST_ID"].Value.ConvertToString();
                if (e.ColumnIndex == this.dgv_header.Columns["ViewDetail"].Index)
                {
                    ////  Show 詳細
                    //var frm = new frmCustDetail(strCustID)
                    //{ AutoScroll = false, StartPosition = FormStartPosition.CenterParent };

                    var frm = new frmAddContract(strCustID, true) { 
                        AutoScroll = false, StartPosition = FormStartPosition.CenterParent
                    };
                    var is_order_end = QueryCondition.IsIncOrderEnd;
                    if (is_order_end == "Y")
                    {
                        frm.IsOrderend = true ;
                    }

                   // frm.IsOrderend
                    frm.ShowDialog();
                    frm.Dispose();
                    frm = null;

                }

                if (e.ColumnIndex == this.dgv_header.Columns["ORDER_CNT"].Index)
                {
                    //  Show 合約
                    var frm = new frmContractDetail(strCustID)
                    { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
                    frm.ShowDialog();
                    frm.Dispose();
                    frm = null;
                }

                if (e.ColumnIndex == this.dgv_header.Columns["AddContract"].Index)
                {
                    //  新增合約
                    var frm = new frmAddContract(strCustID)
                    { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
                    frm.ShowDialog();
                    frm.Dispose();
                    frm = null;
                    this.BindCustData();
                }
            }
        }

        private void dgv_header_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {

            Parallel.ForEach(this.dgv_header.Rows.Cast<DataGridViewRow>(), x =>
            {
                x.Cells["AddContract"].Value = "新增合約";
                x.Cells["ViewDetail"].Value = "客戶詳細";
            });

        }

        #endregion

        #region Background Handle


        #region Background Task 1 - Bind 客戶資料


        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            System.Threading.Thread.Sleep(50);
            var strORG = Global.ORG_CODE;

            if ((e.Argument as QueryCust) != null)
            {
                e.Result = base.Cust.GetCustData("", (e.Argument as QueryCust), strORG);
            }
            else
            {
                e.Result = base.Cust.GetCustData("", null, strORG);
            }

        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //this.dgv_header.DataSource = null;
            this.dgv_header.DataClean();
            if (e.Result != null)
            {
                if ((e.Result as List<BL_CUST>) != null)
                {
                    var data = e.Result as List<BL_CUST>;

                    this.dgv_header.DataBindByList(data);
             
                    this.dgv_header.Rows.Cast<DataGridViewRow>().ForEach(x =>
                    {
                        x.Cells["AddContract"].Value = "新增合約";
                        x.Cells["ViewDetail"].Value = "客戶詳細";
                    });
                }
            }

            this.picLoder.Visible = false;
        }


        #endregion

        #endregion

        private void toolScriptContract_Click(object sender, EventArgs e)
        {
            this.GetCurrentMenu = enCurrentMenu.合約查詢;
        }

        private void 每月新合約統計報表ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //var frm = new Form2() { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
            //frm.ShowDialog();
            //frm.Dispose();
            //frm = null;
            var frm = new frmRPT_SalesPerformByCust() { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
            frm.ShowDialog();
            frm.Dispose();
            frm = null;
        }

        private void toolStripContractApprove_Click(object sender, EventArgs e)
        {
            this.GetCurrentMenu = enCurrentMenu.合約審核;
        }

        private void 本月到期合約清單ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var frm = new frmRPT_ExpiredOrder() { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
            frm.ShowDialog();
            frm.Dispose();
            frm = null;
        }

        /// <summary>
        /// 月對帳單
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripRPTMonthlyBill_Click(object sender, EventArgs e)
        {
            var frm = new frmRPT_MonthlyBill() { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
            frm.ShowDialog();
            frm.Dispose();
            frm = null;
        }

        private void ToolStripEmpManager_Click(object sender, EventArgs e)
        {
            var frm = new frmEMP_MANAGER_Q() { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
            frm.ShowDialog();
            frm.Dispose();
            frm = null;
        }

        /// <summary>
        /// 合約審批[香港]
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            var frm_Contract_Approve = new frmContractApprove("HK") { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
            frm_Contract_Approve.ShowDialog();
            frm_Contract_Approve.Dispose();
            frm_Contract_Approve = null;

            this.BindCustData();
        }

        /// <summary>
        /// 出金審批[香港]
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            this.toolScriptDeposit.BackColor = Color.Pink;
            var frm = new frmDepositOut() { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
            frm.ShowDialog();
            frm.Dispose();
            frm = null;
        }

        private void 每月新客戶列表ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var frm = new frmRPT_SalesPerformByMonth() { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
            frm.ShowDialog();
            frm.Dispose();
            frm = null;
        }

        /// <summary>
        /// 客人WebApp登入資訊通知
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripAlertToCustomer_Click(object sender, EventArgs e)
        {
            
            var selectionData = this.dgv_header.Rows.Cast<DataGridViewRow>()
                .Where(x => (bool?)x.Cells["check"].Value == true)
                .ToList();
            if (selectionData.Count > 0)
            {
                var data = selectionData.Select(x => x.Cells["CUST_ID"].Value.ConvertToString()).ToList();

                var frm = new frmAlertToCustomer(data) { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
                frm.ShowDialog();
                frm.Dispose();
                frm = null;
            }
            else
            {
                MessageBox.Show("請先選取客戶");
                return;
            }
            
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            this.dgv_header.Rows.Cast<DataGridViewRow>()
                .ForEach(x =>
                {
                    x.Cells["check"].Value = this.checkBox1.Checked;
                });
        }

        private void 月對帳總表ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var frm = new frmRPT_MonthlyBill_Summary() { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
            frm.ShowDialog();
            frm.Dispose();
            frm = null;
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            this.GetCurrentMenu = enCurrentMenu.金流表異動作業;

            var frm = new frmAdminSentTxn()
            { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
            frm.ShowDialog();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            var frm = new frmSystemVersionInfo() { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
            frm.ShowDialog();
            frm.Dispose();
            frm = null;
        }

        private void 每日出金表ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var frm = new frmRPT_DailyBill() { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
            frm.ShowDialog();
            frm.Dispose();
            frm = null;
        }

        private void toolMenu_BL_PERIOD_Management_Click(object sender, EventArgs e)
        {
            var frm = new frmBL_PERIOD_Management() { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
            frm.ShowDialog();
            frm.Dispose();
            frm = null;
        }

        /// <summary>
        /// 台幣提領執行作業
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolMenu_NTD_Payment_Job_Click(object sender, EventArgs e)
        {
            var frm = new frmNTD_Payment_Job() { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
            frm.ShowDialog();
            frm.Dispose();
            frm = null;
        }

        /// <summary>
        /// 到期通知書
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolMenu_OrderExpiry_Click(object sender, EventArgs e)
        {
            var frm = new frmRPT_OrderExpiry() { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
            frm.ShowDialog();
            frm.Dispose();
            frm = null;
        }

        private void btnTransContractToNewSales_Click(object sender, EventArgs e)
        {
            var frm = new frmTransContractToNewSales() { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
            frm.ShowDialog();
            frm.Dispose();
            frm = null;

            this.BindCustData();
        }

        private void 每月出金統計表ToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void ToolStripAlertToCustomer_DepositOnLine_Click(object sender, EventArgs e)
        {
            var frm = new frmAlertToCustomer_DepositOnLine() { AutoScroll = false, StartPosition = FormStartPosition.CenterParent };
            frm.ShowDialog();
            frm.Dispose();
            frm = null;
        }
    }
}
