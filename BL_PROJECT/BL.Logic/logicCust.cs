﻿using MK.Demo.Data;
using MK.Demo.Model;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MK.Demo.Utility;
using Newtonsoft.Json;

namespace MK.Demo.Logic
{

    public class logicCust : ConnectionDataBase
    {
        public logicCust(bool IsTestMode)
        {
            base.IsTestMode = IsTestMode;
        }

        public long GetNextSEQ(string strSEQ_NAME)
        {
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                return new Da_Common(cn).GetNextSeq(strSEQ_NAME);
            }
        }

        public List<BL_CUST> GetCustData(string strCustID = "", QueryCust _Condition = null, string strORG = "")
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_CUST(cn).GetData(strCustID, _Condition, strORG, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public List<BL_CUST> GetData(List<string> CustID)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_CUST(cn).GetData(CustID, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public List<BL_CUST> GetData_CanOnlineDeposit()
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_CUST(cn).GetData_CanOnlineDeposit(out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public List<BL_CUST> GetCustDataByID_Number(string strID_Number, string strID_Number2)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_CUST(cn).GetDataByID_Number(strID_Number, strID_Number2, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public List<BL_ORDER> GetContractDataByCustID(string CustID, string strCustName = "")
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_ORDER(cn).GetDataByCustID(CustID, strCustName, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public List<BL_ORDER> GetDataByOrderNo(string ORDER_NO, string CustName)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_ORDER(cn).GetDataByOrderNo(ORDER_NO, CustName, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public List<BL_ORDER> GetDataByOrderNo
            (
                List<string> ORDER_NO,
                string CustName,
                DateTime? END_DATE_S,
                DateTime? END_DATE_E,
                string strCustID,
                DateTime? ORDER_END_S,
                DateTime? ORDER_END_E,
                string strORG
            )
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_ORDER(cn).GetDataByOrderNo
                    (
                        ORDER_NO,
                        CustName,
                        END_DATE_S,
                        END_DATE_E,
                        strCustID,
                        ORDER_END_S,
                        ORDER_END_E,
                        strORG,
                        out exError
                    );
                if (exError != null) throw exError;
                return data;
            }
        }


        public List<BL_ORDER> GetUnApproveDataByOrderNo(string ORDER_NO, string CustName, string strApproveStatus, string strOrg)
        {
            Exception exError = null;

            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_ORDER(cn).GetUnApproveDataByOrderNo(ORDER_NO, CustName, strApproveStatus, strOrg, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public List<BL_ORDER> GetUnApproveDataByOrderNo_NewCustomer(string ORDER_NO, string CustName, string strApproveStatus, string strOrg)
        {
            Exception exError = null;

            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_ORDER(cn).GetUnApproveDataByOrderNo_NewCustomer(ORDER_NO, CustName, strApproveStatus, strOrg, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }


        public List<BL_CUST_BANK> GetBankDataByCustID(string strCust_ID)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_CUST_BANK(cn).GetDataByCustID(strCust_ID, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }


        public int GetBALANCE(string order_ID)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_Common(cn).GetBALANCE(order_ID);
                if (exError != null) throw exError;
                return data;
            }
            //  return 0;   //  Todo > Ian Remark
        }


        public string SaveNewContract
            (
                BL_CUST dataCust,
                BL_CUST_BANK dataBank,
                BL_CUST_BANK dataBank2,
                BL_CUST_BANK dataBank3,
                BL_CUST_BANK dataBank4,
                BL_CUST_BANK dataBank5,
                string strCustID,
                BL_ORDER dataOrder,
                string strUserName,
                BL_CUST_BANK dataBank3_2,
                BL_CUST_BANK dataBank6,
                BL_CUST_BANK dataBank7
            )
        {
            var strResult = string.Empty;
            var strOrderID = string.Empty;
            Exception exError = null;
            if (strCustID != string.Empty)
            {
                ////  Update
                //using (var cn=new OracleConnection(this.ConnectionString ) )
                //{
                //    new Da_BL_CUST(cn).UpdateContractData(dataCust, dataBank, strCustID, out exError);
                //    if (exError != null) throw exError;
                //}
                //  TBD > 合約沒有綁訂單, 是綁客戶, 這樣更新會有問題, 先不處理
                //using (var CN = new OracleConnection(this.ConnectionString))
                //{
                //    new Da_BL_CUST(CN).UpdateContractData(dataCust, strCustID, out exError);
                //    if (exError != null) throw exError;
                //}
                strResult = strCustID;
            }
            else
            {
                //  Insert
                using (var cn = new OracleConnection(this.ConnectionString))
                {
                    strResult = new Da_Common(cn).GetNextSeq("BL_CUST_S1").ConvertToString();
                    var strBankID = new Da_Common(cn).GetNextSeq("BL_CUST_BANK_S1").ConvertToString(); // BR2.BL_CUST_BANK_S1.NEXTVAL()
                    var strBankID2 = new Da_Common(cn).GetNextSeq("BL_CUST_BANK_S1").ConvertToString(); // BR2.BL_CUST_BANK_S1.NEXTVAL()
                    var strBankID3 = new Da_Common(cn).GetNextSeq("BL_CUST_BANK_S1").ConvertToString(); // BR2.BL_CUST_BANK_S1.NEXTVAL()
                    var strBankID3_2 = new Da_Common(cn).GetNextSeq("BL_CUST_BANK_S1").ConvertToString(); // BR2.BL_CUST_BANK_S1.NEXTVAL()
                    var strBankID4 = new Da_Common(cn).GetNextSeq("BL_CUST_BANK_S1").ConvertToString(); // BR2.BL_CUST_BANK_S1.NEXTVAL()
                    var strBankID5 = new Da_Common(cn).GetNextSeq("BL_CUST_BANK_S1").ConvertToString(); // BR2.BL_CUST_BANK_S1.NEXTVAL()
                    var strBankID6 = new Da_Common(cn).GetNextSeq("BL_CUST_BANK_S1").ConvertToString(); // BR2.BL_CUST_BANK_S1.NEXTVAL()
                    var strBankID7 = new Da_Common(cn).GetNextSeq("BL_CUST_BANK_S1").ConvertToString(); // BR2.BL_CUST_BANK_S1.NEXTVAL()
                    new Da_BL_CUST(cn).InsertContractData(dataCust, dataBank, dataBank2, dataBank3, dataBank3_2, dataBank4, dataBank5, dataBank6, dataBank7, strResult, strBankID, strBankID2, strBankID3, strBankID3_2, strBankID4, strBankID5, strBankID6, strBankID7, strUserName, out exError);
                    if (exError != null) throw exError;
                }
            }

            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var seqOrder = new Da_Common(cn).GetNextSeq("BL_ORDER_S1").ConvertToString();
                new Da_BL_ORDER(cn).InsertOrder(strResult, dataOrder, seqOrder, strUserName, out exError);
                if (exError != null) throw exError;
                strOrderID = seqOrder;
            }

            //  新增合約時, 不Call Package, 待審定後才Call
            ////  Call Package
            //if (strOrderID != string.Empty)
            //{
            //    using (var cn = new OracleConnection(this.ConnectionString))
            //    {
            //        var cmd = new OracleCommand();
            //        cmd.Connection = cn;
            //        cmd.CommandText = "BL_INTEREST_PKG.BEGIN_BALANCE_TXN";
            //        cmd.CommandType = System.Data.CommandType.StoredProcedure;
            //        cmd.Parameters.Add("X_DATE", dataOrder.FROM_DATE);
            //        cmd.Parameters.Add("X_ORDER_ID", strOrderID);
            //        try
            //        {
            //            cn.Open();
            //            var reader = cmd.ExecuteReader();
            //        }
            //        catch (Exception ex)
            //        {
            //            throw ex;
            //        }
            //    }
            //}

            return strResult;
        }

        public bool BatchApproveOrder(List<BL_ORDER> dataOrder, string strUserName)
        {
            Exception exError = null;
            var result = true;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                result = new Da_BL_ORDER(cn).BatchApproveOrder(dataOrder, strUserName, out exError);
                if (exError != null) throw exError;
            }

            //  Call Package
            dataOrder.ForEach(x =>
            {
                if (x.ORDER_ID != null)
                {
                    using (var cn = new OracleConnection(this.ConnectionString))
                    {
                        var cmd = new OracleCommand();
                        cmd.Connection = cn;
                        cmd.CommandText = "BL_INTEREST2_PKG.BEGIN_BALANCE_TXN";
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.Add("X_DATE", x.FROM_DATE);
                        cmd.Parameters.Add("X_ORDER_ID", x.ORDER_ID.Value);
                        try
                        {
                            cn.Open();
                            var reader = cmd.ExecuteReader();
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }

                    if (x.ORG_CODE.ConvertToString().Trim() == "IND")
                    {
                        using (var cn = new OracleConnection(this.ConnectionString))
                        {
                            var cmd = new OracleCommand();
                            cmd.Connection = cn;
                            cmd.CommandText = "BL_INTEREST2_PKG.DISCOUNT_AUTO_WITHDRAWAL";
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd.Parameters.Add("X_ORDER_ID", x.ORDER_ID.Value);
                            cmd.Parameters.Add("X_TXN_DATE", x.FROM_DATE);
                            cmd.Parameters.Add("X_DISCOUNT_AMOUNT", x.DISCOUNT_AMOUNT == null ? 0 : x.DISCOUNT_AMOUNT.Value);
                            try
                            {
                                cn.Open();
                                var reader = cmd.ExecuteReader();
                            }
                            catch (Exception ex)
                            {
                                throw ex;
                            }
                        }
                    }
                }
            });


            return result;
        }

        public bool BatchAuditOrder(List<BL_ORDER> dataOrder, string strUserName)
        {
            Exception exError = null;
            var result = true;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                result = new Da_BL_ORDER(cn).BatchAuditOrder(dataOrder, strUserName, out exError);
                if (exError != null) throw exError;
            }
            return result;
        }

        public bool BatchRejectOrder(List<BL_ORDER> dataOrder, string strRejectReason, string strUserName)
        {
            Exception exError = null;
            var result = true;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                result = new Da_BL_ORDER(cn).BatchRejectOrder(dataOrder, strRejectReason, strUserName, out exError);
                if (exError != null) throw exError;
            }

            return result;
        }

        public bool UpdateOrderData
           (
               string strOrderID,
               string strCustID,
               BL_CUST dataCust,
               BL_CUST_BANK dataBank,
               BL_CUST_BANK dataBank2,
               BL_CUST_BANK dataBank3,
               BL_CUST_BANK dataBank4,
               BL_CUST_BANK dataBank5,
               BL_ORDER dataOrder,
               string strUserName,
               bool IsResend,
               BL_CUST_BANK dataBank3_2,
               BL_CUST_BANK dataBank6,
               BL_CUST_BANK dataBank7,
               bool IsRejectByHKtoResend = false
           )
        {
            Exception exError = null;
            
            int count = 0;
            using (var cn = new OracleConnection(this.ConnectionString))
            {

                var data = new Da_BL_CUST_BANK(cn).GetDataByCustID(strCustID, out exError);
                count = data.Count;
                if (exError != null) throw exError;

            }

            if (dataBank.BANK_ID == null)
            {
                using (var cn = new OracleConnection(this.ConnectionString))
                {
                    var strBankID = new Da_Common(cn).GetNextSeq("BL_CUST_BANK_S1");
                    dataBank.BANK_ID = strBankID;
                    new Da_BL_CUST_BANK(cn).InsertCustBank(strCustID, dataBank, out exError);
                    if (exError != null) throw exError;
                }
            }
            if (dataBank2.BANK_ID == null)
            {
                using (var cn = new OracleConnection(this.ConnectionString))
                {
                    var strBankID = new Da_Common(cn).GetNextSeq("BL_CUST_BANK_S1");
                    dataBank2.BANK_ID = strBankID;
                    new Da_BL_CUST_BANK(cn).InsertCustBank(strCustID, dataBank2, out exError);
                    if (exError != null) throw exError;
                }
            }
            if (dataBank3.BANK_ID == null)
            {
                using (var cn = new OracleConnection(this.ConnectionString))
                {
                    var strBankID = new Da_Common(cn).GetNextSeq("BL_CUST_BANK_S1");
                    dataBank3.BANK_ID = strBankID;
                    new Da_BL_CUST_BANK(cn).InsertCustBank(strCustID, dataBank3, out exError);
                    if (exError != null) throw exError;
                }
            }
            if (dataBank3_2.BANK_ID == null)
            {
                using (var cn = new OracleConnection(this.ConnectionString))
                {
                    var strBankID = new Da_Common(cn).GetNextSeq("BL_CUST_BANK_S1");
                    dataBank3_2.BANK_ID = strBankID;
                    new Da_BL_CUST_BANK(cn).InsertCustBank(strCustID, dataBank3_2, out exError);
                    if (exError != null) throw exError;
                }
            }
            if (dataBank4.BANK_ID == null)
            {
                using (var cn = new OracleConnection(this.ConnectionString))
                {
                    var strBankID = new Da_Common(cn).GetNextSeq("BL_CUST_BANK_S1");
                    dataBank4.BANK_ID = strBankID;
                    new Da_BL_CUST_BANK(cn).InsertCustBank(strCustID, dataBank4, out exError);
                    if (exError != null) throw exError;
                }
            }
            if (dataBank5.BANK_ID == null)
            {
                using (var cn = new OracleConnection(this.ConnectionString))
                {
                    var strBankID = new Da_Common(cn).GetNextSeq("BL_CUST_BANK_S1");
                    dataBank5.BANK_ID = strBankID;
                    new Da_BL_CUST_BANK(cn).InsertCustBank(strCustID, dataBank5, out exError);
                    if (exError != null) throw exError;
                }
            }
            if (dataBank6.BANK_ID == null)
            {
                using (var cn = new OracleConnection(this.ConnectionString))
                {
                    var strBankID = new Da_Common(cn).GetNextSeq("BL_CUST_BANK_S1");
                    dataBank6.BANK_ID = strBankID;
                    new Da_BL_CUST_BANK(cn).InsertCustBank(strCustID, dataBank6, out exError);
                    if (exError != null) throw exError;
                }
            }
            if (dataBank7.BANK_ID == null)
            {
                using (var cn = new OracleConnection(this.ConnectionString))
                {
                    var strBankID = new Da_Common(cn).GetNextSeq("BL_CUST_BANK_S1");
                    dataBank7.BANK_ID = strBankID;
                    new Da_BL_CUST_BANK(cn).InsertCustBank(strCustID, dataBank7, out exError);
                    if (exError != null) throw exError;
                }
            }

            //using (var cn = new OracleConnection(this.ConnectionString))
            //{

            //    if (count <= 0)
            //    {
            //        new Da_BL_CUST_BANK(cn).InsertCustBank(strCustID, dataBank, out exError);
            //    }

            //    if (exError != null) throw exError;

            //}

            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_ORDER(cn).UpdateOrderData
                    (
                        strOrderID: strOrderID,
                        strCustID: strCustID,
                        dataCust: dataCust,
                        dataBank: dataBank,
                        dataBank2: dataBank2,
                        dataBank3: dataBank3,
                        dataBank3_2: dataBank3_2,
                        dataBank4: dataBank4,
                        dataBank5: dataBank5,
                        dataBank6: dataBank6,
                        dataBank7: dataBank7,
                        dataOrder: dataOrder,
                        strUserName: strUserName,
                        IsResend: IsResend,
                        IsRejectByHKtoResend: IsRejectByHKtoResend,
                        exError: out exError
                    );
                if (exError != null) throw exError;
                return data;
            }
        }


        public List<BL_ACCOUNT_TXN_TEMP> GetDepositData(string strYear, string strMonth)
        {
            Exception exError = null;

            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_ACCOUNT_TXN_TEMP(cn).GetDepositData(strYear, strMonth, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public List<BL_ACCOUNT_TXN_TEMP> GetDepositData
          (
              string OrderNo,
              string strCustName,
              DateTime? dteCreateDate_S, DateTime? dteCreateDate_E,
              DateTime? dteApplyDate_S, DateTime? dteApplyDate_E,
              string BatchNo,
              string strOrg
          )
        {
            Exception exError = null;

            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_ACCOUNT_TXN_TEMP(cn).GetDepositData(OrderNo, strCustName, dteCreateDate_S, dteCreateDate_E, dteApplyDate_S, dteApplyDate_E, BatchNo, strOrg, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }


        public List<BL_ACCOUNT_TXN_V1> GetDepositData_Approved
            (
                string OrderNo,
                string strCustName,
                DateTime? dteCreateDate_S, DateTime? dteCreateDate_E,
                DateTime? dteApplyDate_S, DateTime? dteApplyDate_E,
                string BatchNo,
                string strORG
            )
        {
            Exception exError = null;

            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_ACCOUNT_TXN_V1(cn).GetDepositData(OrderNo, strCustName, dteCreateDate_S, dteCreateDate_E, dteApplyDate_S, dteApplyDate_E, BatchNo, strORG, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }
        public BL_ACCOUNT_TXN_V1 GetDepositData_Approved_ByBatchID(string strOrderID, string BatchID)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_ACCOUNT_TXN_V1(cn).GetDepositData_Approved_ByBatchID(strOrderID, BatchID, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }


        public List<BL_ACCOUNT_TXN_TEMP> GetDepositData_UnApprove(string strORG)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_ACCOUNT_TXN_TEMP(cn).GetDepositData_UnApprove(strORG, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public List<BL_ACCOUNT_TXN_ERRLOG> GetDepositErrData(string strORG)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_ACCOUNT_TXN_TEMP(cn).GetDepositErrData(strORG, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public List<BL_ACCOUNT_TXN_TEMP> GetDepositData_NoTempRecord(string strORG)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_ACCOUNT_TXN_TEMP(cn).GetDepositData_NoTempRecord(strORG, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public List<BL_ORDER> GetUnExpiredOrderData(string strYear, string strMonth, string strCustName)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_ORDER(cn).GetUnExpiredOrderData(strYear, strMonth, strCustName, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }
        public List<BL_ORDER> GetUnExpiredOrderDatabyOrderID(string strOrderid)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_ORDER(cn).GetUnExpiredOrderDatabyOrderID(strOrderid, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }





        public List<BL_ORDER> GetOrderDatabyOrderID(string strOrderID)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_ORDER(cn).GetDatabyOrderID(strOrderID, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public List<BL_ORDER> GetOrderDatabyOrderList(List<BL_ORDER> data)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var result = new Da_BL_ORDER(cn).GetDatabyOrderList(data, out exError);
                if (exError != null) throw exError;
                return result;
            }
        }


        public bool AddNewACCOUNT_TXN_TEMP(BL_ACCOUNT_TXN_TEMP dataSave)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_ACCOUNT_TXN_TEMP(cn).AddNewData(dataSave, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public bool AddNewDataByList(List<BL_ACCOUNT_TXN_TEMP> dataSave)
        {
            Exception exError = null;

            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_ACCOUNT_TXN_TEMP(cn).AddNewDataByList(dataSave, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public bool ResendDeposit(BL_ACCOUNT_TXN_TEMP dataSave)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_ACCOUNT_TXN_TEMP(cn).ResendDeposit(dataSave, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public bool DataSubmit_ACCOUNT_TXN_TEMP(List<string> dataOrderID, string username)
        {
            Exception exError = null;
            var result = true;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                result = new Da_BL_ACCOUNT_TXN_TEMP(cn).DataSubmit_TEMPID(dataOrderID, username, out exError);
                if (exError != null) throw exError;
            }

            var dataOrder = new List<BL_ACCOUNT_TXN_TEMP>();
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                dataOrder = new Da_BL_ACCOUNT_TXN_TEMP(cn).GetDataByTempID(dataOrderID, out exError);
                if (exError != null) throw exError;
            }

            dataOrder.ForEach(x =>
            {

                //X_BATCH_ID NUMBER, X_ORDER_ID VARCHAR2,X_CAL_DATE DATE, X_INTEREST_NTD NUMBER,X_INTEREST_RMB NUMBER, X_INTEREST_USD NUMBER,
                //         X_APPLY_USER VARCHAR2, X_APPLY_DATE DATE,
                //         X_CHECK_USER VARCHAR2, X_CHECK_DATE DATE,X_CHECKED_FLAG VARCHAR2,
                //         X_APPROVER   VARCHAR2, X_APPROVE_DATE DATE, X_APPROVED_FLAG VARCHAR2,
                //         X_BONUS_TSF VARCHAR2,/*Y/N是否獲利轉新單*/
                //         X_NEW_CONTRACT_NO VARCHAR2, /*新單號碼*/
                //         X_REMARK VARCHAR2,
                //         X_WITHDRAWAL_TYPE VARCHAR2,/*INTERNAL-行政 EXTERNAL-網路*/ 
                //         X_RESULT OUT VARCHAR2
                var strError = string.Empty;

                if (x.END_BALANCE_FLAG == "Y")
                {
                    using (var cn = new OracleConnection(this.ConnectionString))
                    {
                        var cmd = new OracleCommand();
                        cmd.Connection = cn;
                        cmd.CommandText = "BL_INTEREST2_PKG.END_BALANCE_TXN2";
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        decimal amount = 0;
                        if (x.AMOUNT_NTD.GetValueOrDefault(0) > 0)
                        {
                            amount = x.AMOUNT_NTD.GetValueOrDefault(0);
                        }
                        if (x.AMOUNT_RMB.GetValueOrDefault(0) > 0)
                        {
                            amount = x.AMOUNT_RMB.GetValueOrDefault(0);
                        }
                        if (x.AMOUNT_USD.GetValueOrDefault(0) > 0)
                        {
                            amount = x.AMOUNT_USD.GetValueOrDefault(0);
                        }
                        if (x.AMOUNT_EUR.GetValueOrDefault(0) > 0)
                        {
                            amount = x.AMOUNT_EUR.GetValueOrDefault(0);
                        }
                        if (x.AMOUNT_AUD.GetValueOrDefault(0) > 0)
                        {
                            amount = x.AMOUNT_AUD.GetValueOrDefault(0);
                        }
                        if (x.AMOUNT_JPY.GetValueOrDefault(0) > 0)
                        {
                            amount = x.AMOUNT_JPY.GetValueOrDefault(0);
                        }
                        if (x.AMOUNT_NZD.GetValueOrDefault(0) > 0)
                        {
                            amount = x.AMOUNT_NZD.GetValueOrDefault(0);
                        }
                        cmd.Parameters.Add("X_BALANCE_AMOUNT", amount);
                        cmd.Parameters.Add("X_TXN_DATE", x.TXN_DATE);
                        cmd.Parameters.Add("X_ORDER_ID", x.ORDER_ID);
                        cmd.Parameters.Add("X_BATCH_ID", x.BATCH_WITHDRAWAL_ID);


                        //cmd.Parameters.Add("X_CAL_DATE", DateTime.Now);
                        //cmd.Parameters.Add("X_INTEREST_NTD", x.AMOUNT_NTD);
                        //cmd.Parameters.Add("X_INTEREST_RMB", x.AMOUNT_RMB);
                        //cmd.Parameters.Add("X_INTEREST_USD", x.AMOUNT_USD);
                        cmd.Parameters.Add("X_APPLY_USER", username);
                        cmd.Parameters.Add("X_APPLY_DATE", x.APPLY_DATE);


                        cmd.Parameters.Add("X_CHECK_USER", x.CHECK_USER);
                        cmd.Parameters.Add("X_CHECK_DATE", x.CHECK_DATE);
                        cmd.Parameters.Add("X_CHECKED_FLAG", x.CHECKED_FLAG);
                        cmd.Parameters.Add("X_APPROVER", x.APPROVER);
                        cmd.Parameters.Add("X_APPROVE_DATE", x.APPROVE_DATE);
                        cmd.Parameters.Add("X_APPROVED_FLAG", x.APPROVED_FLAG);

                        cmd.Parameters.Add("X_ACTUAL_WITHDRAWAL_AMT_USD", x.ACTUAL_WITHDRAWAL_AMT_USD);
                        cmd.Parameters.Add("X_ACTUAL_WITHDRAWAL_AMT_RMB", x.ACTUAL_WITHDRAWAL_AMT_RMB);
                        cmd.Parameters.Add("X_ACTUAL_WITHDRAWAL_AMT_NTD", x.ACTUAL_WITHDRAWAL_AMT_NTD);
                        cmd.Parameters.Add("X_ACTUAL_WITHDRAWAL_AMT_EUR", x.ACTUAL_WITHDRAWAL_AMT_EUR);
                        cmd.Parameters.Add("X_ACTUAL_WITHDRAWAL_AMT_GBP", (decimal?)null);
                        cmd.Parameters.Add("X_ACTUAL_WITHDRAWAL_AMT_JPY", x.ACTUAL_WITHDRAWAL_AMT_JPY);
                        cmd.Parameters.Add("X_ACTUAL_WITHDRAWAL_AMT_AUD", x.ACTUAL_WITHDRAWAL_AMT_AUD);
                        cmd.Parameters.Add("X_ACTUAL_WITHDRAWAL_AMT_NZD", x.ACTUAL_WITHDRAWAL_AMT_NZD);

                        //cmd.Parameters.Add("X_BONUS_TSF", x.BONUS_TSF);
                        //cmd.Parameters.Add("X_NEW_CONTRACT_NO", x.NEW_CONTRACT_NO);
                        //cmd.Parameters.Add("X_REMARK", x.REMARK);

                        //cmd.Parameters.Add("X_WITHDRAWAL_TYPE", x.WITHDRAWAL_TYPE);
                        cmd.Parameters.Add("X_RESULT", OracleDbType.Varchar2, 50).Direction = System.Data.ParameterDirection.Output;

                        try
                        {
                            cn.Open();
                            var reader = cmd.ExecuteReader();
                            var str_result = string.Empty;
                            if (cmd.Parameters["X_RESULT"].Value != null)
                            {
                                str_result = cmd.Parameters["X_RESULT"].Value.ToString();

                                strError = str_result;
                            }
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
                }
                else
                {

                    using (var cn = new OracleConnection(this.ConnectionString))
                    {
                        var cmd = new OracleCommand();
                        cmd.Connection = cn;
                        cmd.CommandText = "BL_INTEREST2_PKG.USD_APPLY3";
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;

                        cmd.Parameters.Add("X_BATCH_ID", x.BATCH_WITHDRAWAL_ID);

                        cmd.Parameters.Add("X_ORDER_ID", x.ORDER_ID);
                        cmd.Parameters.Add("X_CAL_DATE", x.TXN_DATE);
                        cmd.Parameters.Add("X_INTEREST_NTD", x.AMOUNT_NTD);
                        cmd.Parameters.Add("X_INTEREST_RMB", x.AMOUNT_RMB);
                        cmd.Parameters.Add("X_INTEREST_USD", x.AMOUNT_USD);
                        cmd.Parameters.Add("X_INTEREST_EUR", x.AMOUNT_EUR);
                        cmd.Parameters.Add("X_INTEREST_GBP", (decimal?)null);
                        cmd.Parameters.Add("X_INTEREST_JPY", x.AMOUNT_JPY);
                        cmd.Parameters.Add("X_INTEREST_AUD", x.AMOUNT_AUD);
                        cmd.Parameters.Add("X_INTEREST_NZD", x.AMOUNT_NZD);
                        cmd.Parameters.Add("X_APPLY_USER", username);
                        cmd.Parameters.Add("X_APPLY_DATE", x.APPLY_DATE);

                        //  Todo > Call Package加參數_2018_11_20
                        cmd.Parameters.Add("X_CHECK_USER", x.CHECK_USER);
                        cmd.Parameters.Add("X_CHECK_DATE", x.CHECK_DATE);
                        cmd.Parameters.Add("X_CHECKED_FLAG", x.CHECKED_FLAG);
                        cmd.Parameters.Add("X_APPROVER", x.APPROVER);
                        cmd.Parameters.Add("X_APPROVE_DATE", x.APPROVE_DATE);
                        cmd.Parameters.Add("X_APPROVED_FLAG", x.APPROVED_FLAG);
                        cmd.Parameters.Add("X_BONUS_TSF", x.BONUS_TSF);
                        cmd.Parameters.Add("X_NEW_CONTRACT_NO", x.NEW_CONTRACT_NO);
                        cmd.Parameters.Add("X_REMARK", x.REMARK);

                        cmd.Parameters.Add("X_WITHDRAWAL_TYPE", x.WITHDRAWAL_TYPE);
                        cmd.Parameters.Add("X_ACTUAL_WITHDRAWAL_AMT_USD", x.ACTUAL_WITHDRAWAL_AMT_USD);
                        cmd.Parameters.Add("X_ACTUAL_WITHDRAWAL_AMT_RMB", x.ACTUAL_WITHDRAWAL_AMT_RMB);
                        cmd.Parameters.Add("X_ACTUAL_WITHDRAWAL_AMT_NTD", x.ACTUAL_WITHDRAWAL_AMT_NTD);
                        cmd.Parameters.Add("X_ACTUAL_WITHDRAWAL_AMT_EUR", x.ACTUAL_WITHDRAWAL_AMT_EUR);
                        cmd.Parameters.Add("X_ACTUAL_WITHDRAWAL_AMT_GBP", (decimal?)null);
                        cmd.Parameters.Add("X_ACTUAL_WITHDRAWAL_AMT_JPY", x.ACTUAL_WITHDRAWAL_AMT_JPY);
                        cmd.Parameters.Add("X_ACTUAL_WITHDRAWAL_AMT_AUD", x.ACTUAL_WITHDRAWAL_AMT_AUD);
                        cmd.Parameters.Add("X_ACTUAL_WITHDRAWAL_AMT_NZD", x.ACTUAL_WITHDRAWAL_AMT_NZD);

                        cmd.Parameters.Add("X_RESULT", OracleDbType.Varchar2, 50).Direction = System.Data.ParameterDirection.Output;

                        try
                        {
                            cn.Open();
                            var reader = cmd.ExecuteReader();

                            var str_result = string.Empty;
                            if (cmd.Parameters["X_RESULT"].Value != null)
                            {
                                str_result = cmd.Parameters["X_RESULT"].Value.ToString();
                                strError = str_result;
                            }
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
                }

                //strError = "ERROR";
                if (strError.IndexOf("ERROR") != -1)
                {
                    using (var cn = new OracleConnection(this.ConnectionString))
                    {
                        var ERROR_ID = new Da_Common(cn).GetNextSeq("BL_ERR_S1");
                        var dataError = new Da_BL_ACCOUNT_TXN_ERRLOG(cn).WriteErrorLog(ERROR_ID, x, out exError);
                        if (exError != null) throw exError;
                    }
                }
            });

            //  送出後就刪除
            //using (var cn = new OracleConnection(this.ConnectionString))
            //{
            //    result = new Da_BL_ACCOUNT_TXN_TEMP(cn).DataDelete(dataOrderID, out exError);
            //    if (exError != null) throw exError;
            //}

            return result;
        }

        public bool DataSubmit_ACCOUNT_TXN_TEMP_END_BAL(List<string> dataOrderID, string username)
        {
            Exception exError = null;
            var result = true;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                result = new Da_BL_ACCOUNT_TXN_TEMP(cn).DataSubmit(dataOrderID, username, out exError);
                if (exError != null) throw exError;
            }

            var dataOrder = new List<BL_ACCOUNT_TXN_TEMP>();
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                dataOrder = new Da_BL_ACCOUNT_TXN_TEMP(cn).GetDataByOrderID(dataOrderID, out exError);
                if (exError != null) throw exError;
            }

            dataOrder.ForEach(x =>
            {

                //           --每日批次執行END BALANCE(或該單執行ORDER_END填值後, 呼叫此PROCEDURE, 執行INSERT至TXN TABLE)
                //PROCEDURE END_BALANCE_TXN(X_BALANCE_AMOUNT NUMBER/*傳入結清餘額*/ , X_TXN_DATE DATE/*傳入結清日期TXN_DATE*/ ,
                //                          X_ORDER_ID NUMBER, X_BATCH_ID NUMBER,
                //                          X_APPLY_USER VARCHAR2, X_APPLY_DATE DATE,
                //                          X_CHECK_USER VARCHAR2, X_CHECK_DATE DATE, X_CHECKED_FLAG VARCHAR2,
                //                          X_APPROVER VARCHAR2, X_APPROVE_DATE DATE, X_APPROVED_FLAG VARCHAR2,
                //                          X_RESULT OUT VARCHAR2) IS

                using (var cn = new OracleConnection(this.ConnectionString))
                {
                    var cmd = new OracleCommand();
                    cmd.Connection = cn;
                    cmd.CommandText = "BL_INTEREST2_PKG.END_BALANCE_TXN";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add("X_BALANCE_AMOUNT", x.AMOUNT_USD);
                    cmd.Parameters.Add("X_TXN_DATE", DateTime.Now);
                    cmd.Parameters.Add("X_ORDER_ID", x.ORDER_ID);
                    cmd.Parameters.Add("X_BATCH_ID", x.BATCH_WITHDRAWAL_ID);


                    //cmd.Parameters.Add("X_CAL_DATE", DateTime.Now);
                    //cmd.Parameters.Add("X_INTEREST_NTD", x.AMOUNT_NTD);
                    //cmd.Parameters.Add("X_INTEREST_RMB", x.AMOUNT_RMB);
                    //cmd.Parameters.Add("X_INTEREST_USD", x.AMOUNT_USD);
                    cmd.Parameters.Add("X_APPLY_USER", username);
                    cmd.Parameters.Add("X_APPLY_DATE", x.APPLY_DATE);

                    //  Todo > Call Package加參數_2018_11_20
                    cmd.Parameters.Add("X_CHECK_USER", x.CHECK_USER);
                    cmd.Parameters.Add("X_CHECK_DATE", x.CHECK_DATE);
                    cmd.Parameters.Add("X_CHECKED_FLAG", x.CHECKED_FLAG);
                    cmd.Parameters.Add("X_APPROVER", x.APPROVER);
                    cmd.Parameters.Add("X_APPROVE_DATE", x.APPROVE_DATE);
                    cmd.Parameters.Add("X_APPROVED_FLAG", x.APPROVED_FLAG);
                    //cmd.Parameters.Add("X_BONUS_TSF", x.BONUS_TSF);
                    //cmd.Parameters.Add("X_NEW_CONTRACT_NO", x.NEW_CONTRACT_NO);
                    //cmd.Parameters.Add("X_REMARK", x.REMARK);

                    //cmd.Parameters.Add("X_WITHDRAWAL_TYPE", x.WITHDRAWAL_TYPE);
                    cmd.Parameters.Add("X_RESULT", OracleDbType.Varchar2, 50).Direction = System.Data.ParameterDirection.Output;


                    try
                    {
                        cn.Open();
                        var reader = cmd.ExecuteReader();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

            });

            //  送出後就刪除
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                result = new Da_BL_ACCOUNT_TXN_TEMP(cn).DataDelete(dataOrderID, out exError);
                if (exError != null) throw exError;
            }

            return result;
        }



        public bool DataAudit(List<string> dataOrderID, string strUserName)
        {
            Exception exError = null;
            var result = true;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                result = new Da_BL_ACCOUNT_TXN_TEMP(cn).DataAudit(dataOrderID, strUserName, out exError);
                if (exError != null) throw exError;
            }
            return result;
        }

        public bool DataConfirm_SalesApply(List<string> dataOrderID, string strUserName)
        {
            Exception exError = null;
            var result = true;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                result = new Da_BL_ACCOUNT_TXN_TEMP(cn).DataConfirm_SalesApply(dataOrderID, strUserName, out exError);
                if (exError != null) throw exError;
            }
            return result;
        }
        
        public bool DataDelete_ACCOUNT_TXN_TEMP(List<string> dataOrderID)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var result = new Da_BL_ACCOUNT_TXN_TEMP(cn).DataDelete(dataOrderID, out exError);
                if (exError != null) throw exError;
                return result;
            }
        }

        public void SaveFREE_TXN(decimal? strOrderID, string TxnType, string InOut, string WITHDRAWAL_TYPE, string Amount, DateTime dt, string strRemark, string strAdjustType)
        {

            //  X_DATE DATE, X_ORDER_ID NUMBER ,X_FLOW_TYPE VARCHAR2, X_TXN_TYPE VARCHAR2,X_AMOUNT_USD NUMBER, X_WITHDRAWAL_TYPE VARCHAR2
            //  Call 

            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var cmd = new OracleCommand();
                cmd.Connection = cn;
                cmd.CommandText = "BL_INTEREST2_PKG.ADJUST_TXN";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("X_DATE", dt);
                cmd.Parameters.Add("X_ORDER_ID", strOrderID);
                cmd.Parameters.Add("X_TXN_TYPE", InOut);
                cmd.Parameters.Add("X_FLOW_TYPE", TxnType);
                cmd.Parameters.Add("X_AMOUNT_USD", Amount);
                cmd.Parameters.Add("X_WITHDRAWAL_TYPE", WITHDRAWAL_TYPE);
                cmd.Parameters.Add("X_REMAR", strRemark);
                cmd.Parameters.Add("X_ADJUSTMENT_TYPE", strAdjustType);



                try
                {
                    cn.Open();
                    var reader = cmd.ExecuteReader();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

        }

        public List<BL_ACCOUNT_TXN_TEMP> GetBatchDepositDataByListOrderID(List<decimal> liOrderID)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_ACCOUNT_TXN_TEMP(cn).GetBatchDepositDataByListOrderID(liOrderID, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public List<BL_ACCOUNT_TXN_TEMP> GetTemp_OnPathDataByOrderID(decimal? Order_Id)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_ACCOUNT_TXN_TEMP(cn).GetOnPathDataByOrderID(Order_Id, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public List<BL_ACCOUNT_TXN_TEMP> GetTempDataBySchedule(decimal OrderID)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_ACCOUNT_TXN_TEMP(cn).GetTempDataBySchedule(OrderID, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public bool UpdateTempDepositRemarkByBatchNo(string strBatchNo, string strAfterRemark)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var result = new Da_BL_ACCOUNT_TXN_TEMP(cn).UpdateRemarkByBatchNo(strBatchNo, strAfterRemark, out exError);
                if (exError != null) throw exError;
                return result;
            }
        }

        public List<BL_ACCOUNT_TXN_TEMP> GetDepositTempUnApproceData(List<decimal> liOrderID)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_ACCOUNT_TXN_TEMP(cn).GetTempUnApproceData(liOrderID, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public BL_ACCOUNT_TXN_TEMP GetBatchDeposit_ResendData(string strOrderID, string BatchID)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_ACCOUNT_TXN_TEMP(cn).GetResendData(strOrderID, BatchID, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public BL_ACCOUNT_TXN_ERRLOG GetDepositData_Error_ByBatchID(string strOrderID, string BatchID)
        {
            Exception exError = null;
            using (var cn=new OracleConnection(this.ConnectionString) )
            {
                var data = new Da_BL_ACCOUNT_TXN_TEMP(cn).GetDepositData_Error_ByBatchID(strOrderID, BatchID, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public List<BL_ORDER> GetExpiredOrderDataByYM(string strYear, string strMonth, string strORG)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_ORDER(cn).GetExpiredDataByYM(strYear, strMonth, strORG, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }


        public bool BatchVoidOrder(List<BL_ORDER> dataOrder, string strUserName)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_ORDER(cn).BatchVoidOrder(dataOrder, strUserName, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }




        public List<BL_RPT_MONTHLY_BILL> GetRPT_MONTHLY_BILLDataByBatchSN(long ReportSN)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_RPT_MONTHLY_BILL(cn).GetDataByBatchReportSN(ReportSN, out exError);
                if (exError != null) throw exError;

                //  2019/03/09 排除台幣資料
                data = data.Where(x => x.CURRENCY.ConvertToString() != "NTD").ToList();
                return data;
            }
        }

        public long InsertRPT_MONTHLY_BILLData(DateTime strRepoetEndDate, List<long> liCustID)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                long BL_RPT_MONTHLY_BILL_SN = new Da_Common(cn).GetNextSeq("BL_RPT_MONTHLY_BILL_SN");
                var data = new Da_BL_RPT_MONTHLY_BILL(cn).InsertReportData(BL_RPT_MONTHLY_BILL_SN, strRepoetEndDate, liCustID, out exError);
                if (exError != null) throw exError;
                return BL_RPT_MONTHLY_BILL_SN;
            }
        }


        public List<BL_RPT_MONTHLY_BILL> GetRPT_MONTHLY_BILLData(DateTime strRepoetEndDate, List<long> liCustID)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_RPT_MONTHLY_BILL(cn).GetReportData(strRepoetEndDate, liCustID, out exError);
                if (exError != null) throw exError;

                //  計算分群
                data.Select(x => new
                {
                    x.CUST_CNAME,
                    x.CURRENCY
                }).Distinct()
                .ForEach(x =>
                {
                    var dataMainGroup = data.Where(y => y.CUST_CNAME == x.CUST_CNAME && y.CURRENCY == x.CURRENCY).ToList();
                    int iCount = 0;
                    int iGroupCount = 10;
                    int iTemp = 1;
                    if (dataMainGroup.Count() % iGroupCount == 0) iGroupCount = 9;
                    int iTotalPage = dataMainGroup.Count() / iGroupCount;
                    if (iTotalPage * iGroupCount < dataMainGroup.Count()) iTotalPage += 1;

                    dataMainGroup
                        .OrderBy(y => y.FROM_DATE)
                        .ThenBy(y => y.ORDER_NO)
                        .ForEach(y =>
                        {
                            iCount += 1;
                            y.DATA_GROUP = iTemp.ConvertToString();
                            y.Is_Last = "N";
                            if (iTemp == iTotalPage) y.Is_Last = "Y";
                            if (iCount >= iGroupCount)
                            {
                                iTemp += 1;
                                iCount = 0;
                            }
                        });
                });
                
                return data;
            }
        }



        public List<BL_CUST> GetCustComboData(string yyyymm, string strORG)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_CUST(cn).GetCustComboData(yyyymm, strORG, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public List<BL_CUST> GetCustComboData_All(string strORG)
        {
            //get
            //{
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_CUST(cn).GetCustComboData_All(strORG, out exError);
                if (exError != null) throw exError;
                return data;
            }
            // }
        }

        public List<BL_CUST> GetCustComboData_Active(string yyyymm)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_CUST(cn).GetCustComboData_active(yyyymm, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }


        public bool UpdateCustData
            (
                string strCustID,
                BL_CUST dataCust,
                BL_CUST_BANK dataBank,
                BL_CUST_BANK dataBank2,
                BL_CUST_BANK dataBank3,
                BL_CUST_BANK dataBank4,
                BL_CUST_BANK dataBank5,
                string strUserName,
                BL_CUST_BANK dataBank3_2,
                BL_CUST_BANK dataBank6,
                BL_CUST_BANK dataBank7
            )
        {
            Exception exError = null;
            
            int count = 0;
            using (var cn = new OracleConnection(this.ConnectionString))
            {

                var data = new Da_BL_CUST_BANK(cn).GetDataByCustID(strCustID, out exError);
                count = data.Count;
                if (exError != null) throw exError;

            }

            if (dataBank.BANK_ID == null)
            {
                using (var cn = new OracleConnection(this.ConnectionString))
                {
                    var strBankID = new Da_Common(cn).GetNextSeq("BL_CUST_BANK_S1");
                    dataBank.BANK_ID = strBankID;
                    new Da_BL_CUST_BANK(cn).InsertCustBank(strCustID, dataBank, out exError);
                    if (exError != null) throw exError;
                }
            }
            if (dataBank2.BANK_ID == null)
            {
                using (var cn = new OracleConnection(this.ConnectionString))
                {
                    var strBankID = new Da_Common(cn).GetNextSeq("BL_CUST_BANK_S1");
                    dataBank2.BANK_ID = strBankID;
                    new Da_BL_CUST_BANK(cn).InsertCustBank(strCustID, dataBank2, out exError);
                    if (exError != null) throw exError;
                }
            }
            if (dataBank3.BANK_ID == null)
            {
                using (var cn = new OracleConnection(this.ConnectionString))
                {
                    var strBankID = new Da_Common(cn).GetNextSeq("BL_CUST_BANK_S1");
                    dataBank3.BANK_ID = strBankID;
                    new Da_BL_CUST_BANK(cn).InsertCustBank(strCustID, dataBank3, out exError);
                    if (exError != null) throw exError;
                }
            }
            if (dataBank3_2.BANK_ID == null)
            {
                using (var cn = new OracleConnection(this.ConnectionString))
                {
                    var strBankID = new Da_Common(cn).GetNextSeq("BL_CUST_BANK_S1");
                    dataBank3_2.BANK_ID = strBankID;
                    new Da_BL_CUST_BANK(cn).InsertCustBank(strCustID, dataBank3_2, out exError);
                    if (exError != null) throw exError;
                }
            }
            if (dataBank4.BANK_ID == null)
            {
                using (var cn = new OracleConnection(this.ConnectionString))
                {
                    var strBankID = new Da_Common(cn).GetNextSeq("BL_CUST_BANK_S1");
                    dataBank4.BANK_ID = strBankID;
                    new Da_BL_CUST_BANK(cn).InsertCustBank(strCustID, dataBank4, out exError);
                    if (exError != null) throw exError;
                }
            }
            if (dataBank5.BANK_ID == null)
            {
                using (var cn = new OracleConnection(this.ConnectionString))
                {
                    var strBankID = new Da_Common(cn).GetNextSeq("BL_CUST_BANK_S1");
                    dataBank5.BANK_ID = strBankID;
                    new Da_BL_CUST_BANK(cn).InsertCustBank(strCustID, dataBank5, out exError);
                    if (exError != null) throw exError;
                }
            }
            if (dataBank6.BANK_ID == null)
            {
                using (var cn = new OracleConnection(this.ConnectionString))
                {
                    var strBankID = new Da_Common(cn).GetNextSeq("BL_CUST_BANK_S1");
                    dataBank6.BANK_ID = strBankID;
                    new Da_BL_CUST_BANK(cn).InsertCustBank(strCustID, dataBank6, out exError);
                    if (exError != null) throw exError;
                }
            }
            if (dataBank7.BANK_ID == null)
            {
                using (var cn = new OracleConnection(this.ConnectionString))
                {
                    var strBankID = new Da_Common(cn).GetNextSeq("BL_CUST_BANK_S1");
                    dataBank7.BANK_ID = strBankID;
                    new Da_BL_CUST_BANK(cn).InsertCustBank(strCustID, dataBank7, out exError);
                    if (exError != null) throw exError;
                }
            }

            //using (var cn = new OracleConnection(this.ConnectionString))
            //{

            //    if (count <= 0)
            //    {
            //        dataBank.CREATED_BY = strUserName;
            //        dataBank.LAST_UPDATED_BY = strUserName;
            //        var strBankID = new Da_Common(cn).GetNextSeq("BL_CUST_BANK_S1");
            //        dataBank.BANK_ID = strBankID;
            //        new Da_BL_CUST_BANK(cn).InsertCustBank(strCustID, dataBank, out exError);
            //    }

            //    if (exError != null) throw exError;

            //}


            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_CUST(cn).UpdateCustData(strCustID, dataCust, dataBank, dataBank2, dataBank3, dataBank3_2, dataBank4, dataBank5, dataBank6, dataBank7, strUserName, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }



        #region 員工管理

        public string GetEmpCode
        {
            get
            {
                Exception exError = null;
                using (var cn = new OracleConnection(this.ConnectionString))
                {
                    var data = new Da_BL_EMP(cn).GetEmpCode(out exError);
                    if (exError != null) throw exError;
                    return data.ConvertToString();
                }
            }
        }

        public List<BL_EMP> SelectBL_EMP
            (
                string EmpCode,
                string EmpName,
                string IB_CODE,
                string ID_NO,
                DateTime? HIRE_DATE_S,
                DateTime? HIRE_DATE_E,
                DateTime? QUIT_DATE_S,
                DateTime? QUIT_DATE_E,
                string ORG_CODE
            )
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_EMP(cn).SelectBL_EMP
                    (
                        EmpCode,
                        EmpName,
                        IB_CODE,
                        ID_NO,
                        HIRE_DATE_S,
                        HIRE_DATE_E,
                        QUIT_DATE_S,
                        QUIT_DATE_E,
                        ORG_CODE,
                        out exError
                    );
                if (exError != null) throw exError;
                return data;
            }
        }

        public BL_EMP SelectBL_EMPByEMP_ID(string EMP_ID)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_EMP(cn).SelectBL_EMPByEMP_ID(EMP_ID, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public long AddNewEmp(BL_EMP dataSave)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var EMP_ID = new Da_Common(cn).GetNextSeq("BL_EMP_S1");
                var data = new Da_BL_EMP(cn).AddNewEmp(dataSave, EMP_ID, out exError);
                if (exError != null) throw exError;
                return EMP_ID;
            }
        }

        public bool UpdateEmp(BL_EMP dataSave, string EMP_ID)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_EMP(cn).UpdateEmp(dataSave, EMP_ID, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public bool DeleteBL_EMPByEMP_ID(string EMP_ID)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_EMP(cn).DeleteBL_EMPByEMP_ID(EMP_ID, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        #endregion

        public List<RPT_SALES_PERFORMANCE_BY_MONTH> GetRPT_SALES_PERFORMANCE_BY_MONTH(string strBaseYM,string strORG)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_RPT_SALES_PERFORMANCE_BY_MONTH(cn).GetReportDataByMonth(strBaseYM, strORG, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public List<RPT_SALES_PERFORMANCE_BY_CUST> GetRPT_SALES_PERFORMANCE_BY_CUST(DateTime Date_S, DateTime Date_E,string strORG)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_RPT_SALES_PERFORMANCE_BY_CUST(cn).GetReportDataByRange(Date_S, Date_E, strORG, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public bool UpdateCustWebPwd(List<BL_CUST> dataPWD)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_CUST(cn).UpdateWebPwd(dataPWD, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }
        public bool UpdateWebGU_ID(List<BL_CUST> dataGU_ID)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_CUST(cn).UpdateWebGU_ID(dataGU_ID, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public string GetNewUnionIdNumber()
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_CUST(cn).GetNewUnionIdNumber(out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public bool UpdateUnionIdNumber(BL_CUST data)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var result = new Da_BL_CUST(cn).UpdateUnionIdNumber(data, out exError);
                if (exError != null) throw exError;
                return result;
            }
        }
        
        public bool UpdateCustomer_IsOldCustomer(string strCustID)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var result = new Da_BL_CUST(cn).UpdateCustomer_IsOldCustomer(strCustID, out exError);
                if (exError != null) throw exError;
                return result;
            }
        }

        public bool TransOrder(string ORDER_ID, string CUST_ID_ori, string CUST_ID_new, string USER_NAME)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_ORDER(cn).TransOrder(ORDER_ID, CUST_ID_ori, CUST_ID_new, USER_NAME, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }


        public List<RPT_MonthlyBill_Summary> GetRpt_RPT_MonthlyBill_Summary_Data(DateTime OrderEndDate,string strORG)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_RPT_MonthlyBill_Summary(cn).GetRptData(OrderEndDate, strORG, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public List<BL_MSG_DATA> GetSystemVersionData()
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_MSG_DATA(cn).GetSystemVersionData(out exError);
                if (exError != null) throw exError;
                return data;
            }
        }


        public List<RPT_DailyBill_Summary> GetDailyBillReportData(DateTime txt_date,string strORG)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_RPT_DailyBill_Summary(cn).GetReportData(txt_date, strORG, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public bool ACCOUNT_TXT_TEMP_Reject_TEMPID(List<string> dataTempID, string strUserName, string strRejectReason)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_ACCOUNT_TXN_TEMP(cn).DataReject_TEMPID(dataTempID, strUserName, strRejectReason, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        #region Log

        public string GetCustLogData_Json(string strCustID)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_CUST_EDIT_LOG(cn).GetCustLogData(strCustID, out exError);
                if (exError != null) throw exError;
                return JsonConvert.SerializeObject(data);
            }
        }

        public string GetCustBankLogData_Json(string strCust_ID)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_CUST_EDIT_LOG(cn).GetCustBankLogData(strCust_ID, out exError);
                if (exError != null) throw exError;
                return JsonConvert.SerializeObject(data);
            }
        }

        public bool WriteJsonLog(List<BL_CUST_EDIT_LOG> data)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var result = new Da_BL_CUST_EDIT_LOG(cn).WriteLog(data, out exError);
                if (exError != null) throw exError;
                return result;
            }
        }

        public List<BL_CUST_EDIT_LOG> GetLogDataByCustID(string strCust_ID)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_CUST_EDIT_LOG(cn).GetLogDataByCustID(strCust_ID, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }


        #endregion

        public bool WriteLog(long LogSN, string strAccout, string strIPAddress)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var result = new Da_BL_BACKEND_LOGIN_LOG(cn).WriteLog(LogSN, strAccout, strIPAddress, out exError);
                if (exError != null) throw exError;
                return result;
            }
        }
        public List<BL_BACKEND_LOGIN_LOG> GetMSG_CHECK_DATA(string strAccount, decimal MaxMsgSN)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_BACKEND_LOGIN_LOG(cn).GetMSG_CHECK_DATA(strAccount, MaxMsgSN, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public bool UpdateMSG_CheckLog(decimal MsgSN, long LogSN)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_BACKEND_LOGIN_LOG(cn).UpdateMSG_CheckLog(MsgSN, LogSN, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public BL_PERIOD GetPeriodMaxDate()
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_PERIOD(cn).GetMaxDate(out exError);
                if (exError != null) throw exError;
                return data;
            }
        }


        public bool WriteBillMailLog(List<BL_BILL_MAIL_LOG> data, string strJobID, string strUserName)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var result = new Da_BL_BILL_MAIL_LOG(cn).WriteMailLog(data, strJobID, strUserName, out exError);
                if (exError != null) throw exError;
                return result;
            }
        }

        public string GetBillMailJob_ID()
        {
            var SN = string.Empty;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                SN = new Da_Common(cn).GetNextSeq("BL_BILL_MAIL_LOG_S1").ConvertToString();
            }

            return DateTime.Now.ToString("yyyyMMdd") + SN.PadLeft(3, '0');
        }

        public string GetOrderEndMailJob_ID()
        {
            var SN = string.Empty;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                SN = new Da_Common(cn).GetNextSeq("BL_ORDEREND_MAIL_LOG_S1").ConvertToString();
            }

            return DateTime.Now.ToString("yyyyMMdd") + SN.PadLeft(3, '0');
        }

        public bool WriteOrderEndMailLog(List<BL_ORDEREND_MAIL_LOG> data, string strJobID, string strUserName)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var result = new Da_BL_ORDEREND_MAIL_LOG(cn).WriteMailLog(data, strJobID, strUserName, out exError);
                if (exError != null) throw exError;
                return result;
            }
        }


        public List<BL_BILL_MAIL_LOG> GetAllBillBatchNoByUser(string strUserName)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_BILL_MAIL_LOG(cn).GetAllBatchNoByUser(strUserName, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public List<BL_ORDEREND_MAIL_LOG> GetAllOrderEndBatchNoByUser(string strUserName)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_ORDEREND_MAIL_LOG(cn).GetAllBatchNoByUser(strUserName, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public List<BL_BILL_MAIL_LOG> GetBillMailDataByBatchNo(string strBatchNo)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_BILL_MAIL_LOG(cn).GetDataByBatchNo(strBatchNo, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public List<BL_ORDEREND_MAIL_LOG> GetOrderEndMailDataByBatchNo(string strBatchNo)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_ORDEREND_MAIL_LOG(cn).GetDataByBatchNo(strBatchNo, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public bool UpdateBillMailSendFlag(List<BL_BILL_MAIL_LOG> data, string strUserName)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var result = new Da_BL_BILL_MAIL_LOG(cn).UpdateSendFlag(data, strUserName, out exError);
                if (exError != null) throw exError;
                return result;
            }
        }

        public bool UpdateOrderEndMailSendFlag(List<BL_ORDEREND_MAIL_LOG> data, string strUserName)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var result = new Da_BL_ORDEREND_MAIL_LOG(cn).UpdateSendFlag(data, strUserName, out exError);
                if (exError != null) throw exError;
                return result;
            }
        }

        public List<BL_BILL_MAIL_LOG> GetSalesInfoByBatchNo(List<string> liBatchNo)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_BILL_MAIL_LOG(cn).GetSalesInfoByBatchNo(liBatchNo, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public List<BL_PERIOD> GetBL_PERIOD_DataByYear(string strYear)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_PERIOD(cn).GetDataByYear(strYear, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public bool UpdateBL_PERIOD_Data(List<BL_PERIOD> dataSave, string strUserName)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_PERIOD(cn).UpdateData(dataSave, strUserName, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public bool ExecNTD_Payment_Job(string strUserName)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_Common(cn).ExecNTD_Payment_Job(strUserName, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public bool WriteOrderUpdateLog(BL_ORDER_UPDATE_LOG data, string strUserName)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var result = new Da_BL_ORDER_UPDATE_LOG(cn).WriteLog(data, strUserName, out exError);
                if (exError != null) throw exError;
                return result;
            }
        }


        public List<BL_ORDER_Expiry> GetRpt_OrderExpirtData(DateTime dt,string strORG)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_ORDER_Expiry(cn).GetData(dt, strORG, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public List<BL_ORDER> GetOrderDataByTransSales
            (
                List<BL_CUST> liCust,
                List<BL_EMP> liSales
            )
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_ORDER(cn).GetDataByTransSales(liCust, liSales, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public BL_PROCESS_LOG GetLastExecuteNTD_Payment_Info()
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_PROCESS_LOG(cn).GetLastInfo(out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public List<BL_DEPT> GetDeptCodeData(string strORG_CODE)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString) )
            {
                var data = new Da_BL_DEPT(cn).GetData(strORG_CODE, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public IND_DISCOUNT_AMOUNT GetIND_DISCOUNT_AMOUNT(string strCurrency, string strAmount)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_IND_DISCOUNT_AMOUNT(cn).GetIND_DISCOUNT_AMOUNT(strCurrency, strAmount, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }


    }
}
