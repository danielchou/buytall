﻿using MK.Demo.Data;
using MK.Demo.Model;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MK.Demo.Logic
{
    public class EMP_MANAGEMENT :ConnectionDataBase
    {
        public EMP_MANAGEMENT(bool IsTestMode)
        {
            base.IsTestMode = IsTestMode;
        }

        public List<BL_EMP> GetBL_EMP(string strORG_CODE = "")
        {
            Exception exError = null;
            using (var connection = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_EMP(connection).SelectBL_EMP(strORG_CODE, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }

        public List<BL_EMP> SelectEmpByAccount(string AccountName)
        {
            Exception exError = null;
            using (var cn = new OracleConnection(this.ConnectionString))
            {
                var data = new Da_BL_EMP(cn).SelectEmpByAccount(AccountName, out exError);
                if (exError != null) throw exError;
                return data;
            }
        }
    }
}
