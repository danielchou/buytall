#UBT Development  #
百拓資訊公司專案管理使用。

# 系統架構 #
	1. 資料庫：SQL Server 2016
	2. 報表：Reporting Service (RDLC)
	3. ASP.NET MVC 5.2
	4. DOT NET Framework 4.6.1
	5. Windows Form

# 程式架構 #
### 1. Winform Supervisor ###
Windows Application (Windows Form), 提供給所有Agent使用。

### 2. Web ###
#### 前端主要元件 ####
- jQuery (version 3.0?)
- Vue.js (v2.2.2) 
- Vuetify
- axios

#### 前端打包工具 ####
- Webpack2

#### 後端架構 ####
	- ASP.NET MVC 5.2 
	- JSON Result (View/Model/Controller)
	- 商業邏輯層 (BLL) 
	- 資料處理層 (DAL)
	- Entity Framework6

### 4.開發環境 ###
	1. Version Control
	   - [Git](https://git-scm.com/)
	   - [Bitbuket](bitbucket.org)
	2. Visual Studio 2015
	3. [Visual Studio Code](https://code.visualstudio.com/)
	4. Chrome
	5. [Selenium IDE](http://docs.seleniumhq.org/projects/ide/)